<? include_once("admin2_logica.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Geelbe - Partners</title>
<link href="css/initial.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jscript.js"></script>

</head>

<body>
<div id="bigone">
        <div id="header">
            <a class="logout" href="close.php">SALIR</a>
        </div>
      <div id="content">
            <h1>Bienvenido, <?=$_SESSION["partners"]["Nombre"]?> <?=$_SESSION["partners"]["Apellido"]?> (<?=$_SESSION["partners"]["NombrePartner"]?>)</h1>
        <!--<h2>El c&oacute;digo de invitaci&oacute;n se encuentra activo desde el 12/11/08<br />
                Hay <span class="number">25</span> miembros y <span class="number">4</span> compras nuevas desde su última visita, sumando un total de <span class="number">452</span>.</h2>-->
            
            <div id="reportlogo">
            </div>
            <div id="filter">
              <div class="content">
			  <form method="get">
			  <input type="hidden" name="col" value="<?=$orden["col"]?>"/>
			  <input type="hidden" name="sentido" value="<?=$orden["sentido"]?>"/>	
				<label>
                	Desde
                    <input class="input" type="text" name="fechadesde" id="fechadesde" value="<?=mysql2fecha($fechaini)?>" />
		      	</label>
                <img src="images/n_spacer.jpg" alt="" width="5" />
                <label>
                    Hasta
                    <input class="input" type="text" name="fechahasta" id="fechahasta" value="<?=mysql2fecha($fechafin)?>" />
                </label>
                <? if(count($lista_codigos)>1){?>
                <!-- Empieza el if de multicombo -->
                <img src="images/n_spacer.jpg" alt="" width="5" />
              	<label>
                	Con c&oacute;digo
                    <select class="select" name="c[]" size="1" multiple="multiple" id="select">
                      <?=load_select($lista_codigos,$codigo)?>
                  </select>
                   <img src="images/n_spacer.jpg" alt="" width="110" />
       	        </label>
                <!-- Termina el if de multicombo -->
                <? 	}?>
				
           	    <img src="images/n_spacer.jpg" alt="" width="4" />
                <label>Con estado</label>
				<select name="compro" class="miniselect">
					<?=load_select($compro_opciones,$compro)?>
				</select>
                <img src="images/n_spacer.jpg" alt="" width="7" />
                <input type="submit" class="boton" value="Ver reportes" />
                </form>
                </div>
            </div>
        <div id="results">
		
		<div class="titular">Se encontraron <span class="strong"><?=$totales["compras"]?></span> compras realizadas y <span class="strong"><?=$totales["registros"]?></span> usuarios registrados entre el <span class="strong"><?=mysql2fecha($fechaini)?></span> y el <span class="strong"><?=mysql2fecha($fechafin)?></span> (codigos:<span class="strong"><?=$totales["codigos"]?></span>)</div>
		  
        </div>
            <table id="tableresults" width="92%" border="0" align="center" cellpadding="4" cellspacing="0">
              <thead>
                  <tr>
                    <th width="20%" height="38" align="left"><a href="<?=g_link($fechaini, $fechafin, $orden, 0, $codigo, $compro, $pagina)?>" <?=gen_class_orden($orden,0)?>>Nombre completo</a></th>
                    <th width="16%" align="left"><a href="<?=g_link($fechaini, $fechafin, $orden, 1, $codigo, $compro, $pagina)?>" <?=gen_class_orden($orden,1)?>>Fecha de registro</a></th>
                    <th width="18%" align="left"><a href="<?=g_link($fechaini, $fechafin, $orden, 2, $codigo, $compro, $pagina)?>" <?=gen_class_orden($orden,2)?>>Código</a></th>
                    <th width="10%" align="left"><a href="<?=g_link($fechaini, $fechafin, $orden, 3, $codigo, $compro, $pagina)?>" <?=gen_class_orden($orden,3)?>>Compró</a></th>
					<th width="20%" align="left"><a href="<?=g_link($fechaini, $fechafin, $orden, 4, $codigo, $compro, $pagina)?>" <?=gen_class_orden($orden,4)?>>Estado</a></th>
                    <th width="16%" align="left"><a href="<?=g_link($fechaini, $fechafin, $orden, 5, $codigo, $compro, $pagina)?>" <?=gen_class_orden($orden,5)?>>Fecha de compra</a></th>
                </tr>
              </thead>
              <tbody>
			  <? foreach($compras as $linea){ ?>
				<tr>
                    <td height="37" align="left"><?=$linea["nombre"]?></td>
                    <td align="left"><?=mysql2fecha($linea["fecha_de_registro"])?></td>
                    <td align="left"><?=$linea["codigo"]?></td>
                    <td align="left"><?=$linea["compro"]?></td>
					<td align="left"><?=$linea["estado"]?$linea["estado"]:"&nbsp;"?></td>
                    <td align="left"><?=$linea["fecha_de_compra"]?(mysql2fecha($linea["fecha_de_compra"])):"&nbsp;"?></td>
                </tr>
				<?} ?>
				</tbody>
            </table>
			
        <div id="pager">
       	  
          	<div class="pages">
			
			
			<? if ($show['rr']): ?>
			<a href="<?=g_link($fechaini, $fechafin, $orden, false, $codigo, $compro, 1)?>" class="direccion">Primera</a>
			<a href="<?=g_link($fechaini, $fechafin, $orden, false, $codigo, $compro, $pagina-1)?>" class="direccion">Anterior</a>
			<? endif; ?>	
			<? if ($show['pi']): ?>
			<span class="text">...</span>
			<? endif; ?>	
			<? for($n=1;$n<=9;$n++): ?>
			<? if($show['num'][$n]):?>
			<a href="<?=g_link($fechaini, $fechafin, $orden, false, $codigo, $compro, $show['p'][$n])?>"  class="<?=($n==5)?'active':'off'?>"><?=$show['p'][$n];?></a>
			<? endif; endfor;?>
			<? if ($show['pf']): ?>
			<span class="text">...</span>
			<? endif; ?>
			<? if ($show['ff']): ?>
			<a href="<?=g_link($fechaini, $fechafin, $orden, false, $codigo, $compro, $pagina+1)?>" class="direccion">Siguiente</a>
			<a href="<?=g_link($fechaini, $fechafin, $orden, false, $codigo, $compro, $totalpages)?>" class="direccion">Ultima</a>
			<? endif; ?>	
			
			
			
			<?/*=g_link($fechaini, $fechafin, $orden, 1, $codigo, $compro, $pagina)*/?>
			
			
		  </div>
           
            <a href="generar_cvs.php" class="export"></a>
          </div>
  </div>
        
        <div id="footer">
        	Geelbe © 2008<!-- | Términos y Condiciones | Contacto -->
			<?//=$_SESSION["partners"]["query_todos"];?>
        </div>
    </div>
</body>
</html>