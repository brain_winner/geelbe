<?
set_time_limit(300);
include_once("funciones.php");
session_start();
if(!is_array($_SESSION["partners"]["reporte"])){
	echo "No hay datos para exportar. Tiempo excedido/Informe Vacio./r/n";
	var_dump($_SESSION);
	exit();
}

/**
*reporte_compras_xls: Genera una tabla compatible con xls.
*
*@param array $compras los datos de la tabla. espera: Array[$n]["nombre, fecha_de_registro, codigo, compro, estado, fecha_de_compra"]
*@return string el html-table para hacer xls.
*/
function reporte_compras_xls($compras){
	
	//inicializo variable
	$compras = (is_array($compras)) ? $compras : array();

	//Seteo los estilos.
	$style_th = "font-size:13px;background-color:#dddddd;color:#333;";
	$style_td = "border-bottom-style:solid;border-bottom-width:3px;border-bottom-color:#c1c1c1;";
	$style_tr = "color:#666;font-size:13px;";
	
	//Genero los encabezados
	$cxls = '<table border="0" align="center" style="font-family:sans-serif;cellpadding:4;"  cellspacing="0">';
	$cxls .= '<tr align="left" valign="middle">';
	$cxls .= '<th align="left" style="'.$style_th.'">Nombre completo</th>';
	$cxls .= '<th align="left" style="'.$style_th.'">Fecha de registro</th>';
	$cxls .= '<th align="left" style="'.$style_th.'">Código</th>';
	$cxls .= '<th align="left" style="'.$style_th.'">Compró</th>';
	$cxls .= '<th align="left" style="'.$style_th.'">Estado</th>';
	$cxls .= '<th align="left" style="'.$style_th.'">Fecha de compra</th>';
	$cxls .= '</tr>';
	
	//Genero las lineas
	foreach($compras as $linea){
		$cxls .= '<tr style="'.$style_tr.'">';
		$cxls .= '<td align="left" style="'.$style_td.'">' . $linea["nombre"] . '</td>';
		$cxls .= '<td align="left" style="'.$style_td.'">' . $linea["fecha_de_registro"] . '</td>';
		$cxls .= '<td align="left" style="'.$style_td.'">' . $linea["codigo"] . '</td>';	
		$cxls .= '<td align="left" style="'.$style_td.'">' . $linea["compro"] . '</td>';
		$cxls .= '<td align="left"style="'.$style_td.'">' . $linea["estado"] . '</td>';
		$cxls .= '<td align="left" style="'.$style_td.'">' . $linea["fecha_de_compra"] . '</td>';	
		$cxls .= '</tr>';
	}

	//Cierro tabla
	$cxls .= '</table>';
	return $cxls;
}

$filename = "Reporte_".date('d-m-y_H:m:s').".xls";
header('Content-Transfer-Encoding: Binary');
header('Content-Type: application/vnd.ms-excel');
header('Expires: 0');
header('Cache-Control: post-check=-1, pre-check=-1', false);
header('Content-Disposition: attachment; filename="' . $filename . '"');

echo iconv("UTF-8","ISO-8859-1",reporte_compras_xls($_SESSION["partners"]["reporte"]));
?>