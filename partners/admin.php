<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="css/initial.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jscript.js"></script>

</head>

<body>
<div id="bigone">
        <div id="header">
            <a class="logout" href="#">SALIR</a>
        </div>
      <div id="content">
            <h1>Bienvenido, Terra Argentina</h1>
        <h2>El c&oacute;digo de invitaci&oacute;n se encuentra activo desde el 12/11/08<br />
                Hay <span class="number">25</span> miembros y <span class="number">4</span> compras nuevas desde su última visita, sumando un total de <span class="number">452</span>.</h2>
            
            <div id="filter">
              <div class="content">
		      	<label>
                	Desde
                    <input class="input" type="text" name="textfield" id="fechadesde" />
		      	</label>
                <img src="images/n_spacer.jpg" alt="" width="5" />
                <label>
                    Hasta
                    <input class="input" type="text" name="textfield" id="fechahasta" />
                </label>
                
                <!-- Empieza el if de multicombo -->
                <img src="images/n_spacer.jpg" alt="" width="5" />
              	<label>
                	Con c&oacute;digo
                    <select class="select" name="select" size="1" multiple="multiple" id="select">
                      <option>One</option>
                      <option>Two</option>
                      <option>Three</option>
                  </select>
                   <img src="images/n_spacer.jpg" alt="" width="110" />
       	        </label>
                <!-- Termina el if de multicombo -->
                
           	    <img src="images/n_spacer.jpg" alt="" width="4" />
                <input type="button" class="boton" value="Ver" />
                
                <script type="text/javascript"> 
					function customRange(input) { 
						return {minDate: (input.id == "fechahasta" ? $("#fechadesde").datepicker("getDate") : null), 
							maxDate: (input.id == "fechadesde" ? $("#fechahasta").datepicker("getDate") : null)}; 
					} 
				</script>

                </div>
            </div>
        <div id="results">
       	  <div class="titular">Se encontraron <span class="strong">4</span> compras realizadas entre el 12/11/07 y el 05/12/08.</div>
        </div>
            <table id="tableresults" width="92%" border="0" align="center" cellpadding="4" cellspacing="0">
              <thead>
                  <tr>
                    <th width="20%" height="38" align="left">Nombre completo&nbsp;</th>
                    <th width="20%" align="left">Fecha de registro&nbsp;</th>
                    <th width="20%" align="left">Código&nbsp;</th>
                    <th width="20%" align="left">Estado&nbsp;</th>
                    <th width="20%" align="left">Fecha de compra&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                    <td height="37" align="left">Juan Martín</td>
                    <td align="left">12/11/08</td>
                    <td align="left">marcelopetra</td>
                    <td align="left">Confirmado</td>
                    <td align="left">13/11/08</td>
                </tr>
                  <tr>
                    <td height="37" align="left">Juan Martín</td>
                    <td align="left">12/11/08</td>
                    <td align="left">marcelopetra</td>
                    <td align="left">Confirmado</td>
                    <td align="left">13/11/08</td>
                  </tr>
                  <tr>
                    <td height="37" align="left">Juan Martín</td>
                    <td align="left">12/11/08</td>
                    <td align="left">marcelopetra</td>
                    <td align="left">Confirmado</td>
                    <td align="left">13/11/08</td>
                  </tr>
                  <tr>
                    <td height="37" align="left">Juan Martín</td>
                    <td align="left">12/11/08</td>
                    <td align="left">marcelopetra</td>
                    <td align="left">Confirmado</td>
                    <td align="left">13/11/08</td>
                  </tr>
                  <tr>
                    <td height="37" align="left">Juan Martín</td>
                    <td align="left">12/11/08</td>
                    <td align="left">marcelopetra</td>
                    <td align="left">Confirmado</td>
                    <td align="left">13/11/08</td>
                  </tr>
           	  </tbody>
            </table>
        <div id="pager">
       	  <!--
          	<div class="pages">
                	<span class="text">P&aacute;ginas</span>
                    <span class="active">1</span>
                    <a href="#" class="off">2</a>
                    <a href="#" class="off">3</a>
                    <a href="#" class="off">4</a>
                    <a href="#" class="off">5</a>
                    <a href="#" class="off">6</a>
            </div>
            -->
            <a href="#" class="export"></a>
          </div>
  </div>
        
        <div id="footer">
        	Geelbe © 2008 | Términos y Condiciones | Contacto
        </div>
    </div>
</body>
</html>