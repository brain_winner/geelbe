<?
set_time_limit(300);
include_once("funciones.php");
session_start();
if(!$_SESSION["partners"]["query_todos"]){
	echo "No hay datos para exportar. Tiempo excedido/Informe Vacio.\r\n";
	//var_dump($_SESSION);
	exit();
}

$vlink = mysql_connect("localhost", "geelbe_lch43", "Olcfk300Z");
mysql_query("SET NAMES 'utf8'",$vlink);
mysql_select_db("geelbe_launch", $vlink);
//echo($_SESSION["partners"]["query_todos"]);
$sql = mysql_query($_SESSION["partners"]["query_todos"],$vlink);
$compras = "";
while($row = mysql_fetch_assoc($sql)){
		if($row["nombre"]){
			$row["nombre"] = ucwords(strtolower($row["nombre"]));
		}
		if($row["fecha_de_registro"]){
			$row["fecha_de_registro"] = mysql2fecha($row["fecha_de_registro"]);
		}
		if($row["fecha_de_compra"]){
			$row["fecha_de_compra"] = mysql2fecha($row["fecha_de_compra"]);
		}
		$compras[] = $row;
}
mysql_close($vlink);

/**
*reporte_compras_xls: Genera una tabla compatible con xls.
*
*@param array $compras los datos de la tabla. espera: Array[$n]["nombre, fecha_de_registro, codigo, compro, estado, fecha_de_compra"]
*@return string el html-table para hacer xls.
*/
function reporte_compras_xls($compras){
	
	//inicializo variable
	$compras = (is_array($compras)) ? $compras : array();

	//Seteo los estilos.
	$style_th = "font-size:13px;background-color:#dddddd;color:#333;";
	$style_td = "border-bottom-style:solid;border-bottom-width:3px;border-bottom-color:#c1c1c1;";
	$style_tr = "color:#666;font-size:13px;";
	
	//Genero los encabezados
	$cxls = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
	$cxls .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
	$cxls .= '<html>';
	$cxls .= '<head>';
	$cxls .= '<meta http-equiv="Content-type" content="text/html;charset=iso-8859-1" />';
	$cxls .= '<style id="Classeur1_16681_Styles"></style>';
	$cxls .= '</head>';
	$cxls .= '<body>';
	$cxls .= '<div id="Classeur1_16681" align=center x:publishsource="Excel">';
	$cxls .= '<table x:str border=0 style="font-family:sans-serif;" cellpadding=4  cellspacing=0>';
	$cxls .= '<tr align="left" valign="middle">';
	$cxls .= '<th align="left" style="'.$style_th.'">Nombre completo</th>';
	$cxls .= '<th align="left" style="'.$style_th.'">Fecha de registro</th>';
	$cxls .= '<th align="left" style="'.$style_th.'">Código</th>';
	$cxls .= '<th align="left" style="'.$style_th.'">Compró</th>';
	$cxls .= '<th align="left" style="'.$style_th.'">Estado</th>';
	$cxls .= '<th align="left" style="'.$style_th.'">Fecha de compra</th>';
	$cxls .= '</tr>';
	
	//Genero las lineas
	foreach($compras as $linea){
		$cxls .= '<tr style="'.$style_tr.'">';
		$cxls .= '<td align="left" style="'.$style_td.'">' . $linea["nombre"] . '</td>';
		$cxls .= '<td align="left" style="'.$style_td.'">' . $linea["fecha_de_registro"] . '</td>';
		$cxls .= '<td align="left" style="'.$style_td.'">' . $linea["codigo"] . '</td>';	
		$cxls .= '<td align="left" style="'.$style_td.'">' . $linea["compro"] . '</td>';
		$cxls .= '<td align="left"style="'.$style_td.'">' . $linea["estado"] . '</td>';
		$cxls .= '<td align="left" style="'.$style_td.'">' . $linea["fecha_de_compra"] . '</td>';	
		$cxls .= '</tr>';
	}

	//Cierro tabla
	$cxls .= '</table>';

	$cxls .='</div>';
	$cxls .='</body>';
	$cxls .='</html>';

	return $cxls;
}

$filename = "Reporte_".date('d-m-y_H:m:s').".xls";
header('Content-Transfer-Encoding: Binary');
header('Content-Type: application/vnd.ms-excel');
header('Expires: 0');
header('Cache-Control: post-check=-1, pre-check=-1', false);
header('Content-Disposition: attachment; filename="' . $filename . '"');

//imprimo el contendio. Lo converito a ISO-8859-1
echo iconv("UTF-8","ISO-8859-1",reporte_compras_xls($compras));
?>