<? include_once("reportes_logica.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Geelbe - Partners</title>
<link href="css/initial.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jscript.js"></script>

</head>

<body>
<div id="bigone">
        <div id="header">
            <a class="logout" href="close.php">SALIR</a>
        </div>
      <div id="content">
            <h1>Bienvenido, <?=$_SESSION["partners"]["Apellido"]?> (<?=$_SESSION["partners"]["NombrePartner"]?>)</h1>
        <!--<h2>El c&oacute;digo de invitaci&oacute;n se encuentra activo desde el 12/11/08<br />
                Hay <span class="number">25</span> miembros y <span class="number">4</span> compras nuevas desde su última visita, sumando un total de <span class="number">452</span>.</h2>-->
            
            <div id="reportlogo">
            </div>
            <div id="filter">
              <div class="content">
			  <form method="post">
				<label>
                	Desde
                    <input class="input" type="text" name="fechadesde" id="fechadesde" value="<?=mysql2fecha($fechaini)?>" />
		      	</label>
                <img src="images/n_spacer.jpg" alt="" width="5" />
                <label>
                    Hasta
                    <input class="input" type="text" name="fechahasta" id="fechahasta" value="<?=mysql2fecha($fechafin)?>" />
                </label>
                
                <!-- Empieza el if de multicombo -->
                <img src="images/n_spacer.jpg" alt="" width="5" />
              	<label>
                	Con c&oacute;digo
                    <select class="select" name="codigo[]" size="1" multiple="multiple" id="select">
                      <?=load_select($lista_codigos,$codigo)?>
                  </select>
                   <img src="images/n_spacer.jpg" alt="" width="110" />
       	        </label>
                <!-- Termina el if de multicombo -->
                
           	    <img src="images/n_spacer.jpg" alt="" width="4" />
                <label>Con estado</label>
				<select name="compro" class="miniselect">
					<?=load_select($compro_opciones,$compro)?>
				</select>
                <img src="images/n_spacer.jpg" alt="" width="7" />
                <input type="submit" class="boton" value="Ver reportes" />
                </form>
                </div>
            </div>
        <div id="results">
		<!--
       	  <div class="titular">Se encontraron <span class="strong">4</span> compras realizadas entre el 12/11/07 y el 05/12/08.</div>
		  -->
        </div>
            <table id="tableresults" width="92%" border="0" align="center" cellpadding="4" cellspacing="0">
              <thead>
                  <tr>
                    <th width="20%" height="38" align="left">Nombre completo</th>
                    <th width="16%" align="left">Fecha de registro</th>
                    <th width="18%" align="left">Código</th>
                    <th width="10%" align="left">Compró</th>
					<th width="22%" align="left">Estado</th>
                    <th width="14%" align="left">Fecha de compra</th>
                </tr>
              </thead>
              <tbody>
			  <? foreach($compras as $linea){ ?>
				<tr>
                    <td height="37" align="left"><?=$linea["nombre"]?></td>
                    <td align="left"><?=mysql2fecha($linea["fecha_de_registro"])?></td>
                    <td align="left"><?=$linea["codigo"]?></td>
                    <td align="left"><?=$linea["compro"]?></td>
					<td align="left"><?=$linea["estado"]?></td>
                    <td align="left"><?=mysql2fecha($linea["fecha_de_compra"])?></td>
                </tr>
				<?} ?>
				</tbody>
            </table>
			
        <div id="pager">
       	  <!--
          	<div class="pages">
                	<span class="text">P&aacute;ginas</span>
                    <span class="active">1</span>
                    <a href="#" class="off">2</a>
                    <a href="#" class="off">3</a>
                    <a href="#" class="off">4</a>
                    <a href="#" class="off">5</a>
                    <a href="#" class="off">6</a>
            </div>
            -->
            <a href="generar_cvs.php" class="export"></a>
          </div>
  </div>
        
        <div id="footer">
        	Geelbe © 2008<!-- | Términos y Condiciones | Contacto -->
			<?//=$query."\r\n";?>
        </div>
    </div>
</body>
</html>