<?
function redirect($extra){
	
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	/*header("location: http://$host$uri/$extra");*/
		
		exit("location: http://$host$uri/$extra");
}

function fecha2mysql($date){
	$date_ar = explode("/",$date);
	$new = $date_ar[2]."-".$date_ar[1]."-".$date_ar[0];
	if(!($date_ar[2] && $date_ar[1] && $date_ar[0] && checkdate($date_ar[1],$date_ar[0],$date_ar[2]))){
		$new = false;
	}
	return $new;
}

function mysql2fecha($date){
	$date_ar = explode("-",$date);
	$new = $date_ar[2]."/".$date_ar[1]."/".$date_ar[0];
	if(!($date_ar[2] && $date_ar[1] && $date_ar[0] && checkdate($date_ar[1],$date_ar[2],$date_ar[0]))){
		$new = false;
	}
	return $new;
}

function mysql2url($date){
	$date_ar = explode("-",$date);
	$new = $date_ar[2]."%2F".$date_ar[1]."%2F".$date_ar[0];
	if(!($date_ar[2] && $date_ar[1] && $date_ar[0] && checkdate($date_ar[1],$date_ar[2],$date_ar[0]))){
		$new = false;
	}
	return $new;
}


function make_table($array){
	if(!is_array($array[0])){
		return false;
	}
	$tabla = "";
	$tabla .="<table>\r\n";
	$tabla .="<tr>\r\n";
	foreach($array[0] as $titulo => $nada){
		$tabla .="<th>\r\n".$titulo."</th>\r\n";
	}
	$tabla .="</tr>\r\n";
	
	foreach($array as $num_fila => $fila){
		$tabla .="<tr>\r\n";
		
		foreach($fila as $header => $valor){
			$tabla .="<td>".$valor."</td>\r\n";
		}
		
		$tabla .="</tr>\r\n";
	}

	$tabla .="</table>\r\n";
	
	return $tabla;
}



/**
*comprar: Compara una string contra un array para saber si pertenece a �l.
*
*@param string $id es la string a comparar.
*@param array string $codigos es el array contra el cual quiero comparar.
*@return boolean devuelve true si pertenece.
*/
function comparar($id, $codigos){
	$coincide = false;
	$codigos = (is_array($codigos)) ? $codigos : array($codigos);
	foreach($codigos as $cod){
		if($id == $cod){
			$coincide = true;
		}
	}
	return $coincide;
}


/**
*Load Select: Genera un listado de Options a partir de 2 arrays.
*
*@param array $array es el listado de las opciones: value => label
*@param array $codigo es el listado de las opciones que van selected=selected.
*@return string el listado de las options.
*/
function load_select($array,$codigo){
	$select = "";
	foreach($array as $id => $cod){
		$select .="<option value=\"".$id."\"".((comparar($id, $codigo))? " selected=\"selected\"" : "" ).">".$cod."</option>\r\n";
	}
	return $select;
}


function make_cvs($array){
	if(!is_array($array[0])){
		return false;
	}
	$cvs = "";
	$i=0;
	foreach($array[0] as $titulo => $nada){
		if($i!=0){$cvs.=";";}
		$cvs .=$titulo;
		$i++;
	}
	$cvs .="\r\n";
	
	foreach($array as $num_fila => $fila){
		$i=0;
		foreach($fila as $header => $valor){
			if($i!=0){$cvs.=";";}
			$cvs .=$valor;
			$i++;
		}
		$cvs .="\r\n";
	}
	return $cvs;
}

function gen_class_orden($orden,$id_columna){
	if($orden["col"] == $id_columna){
		if($orden["sentido"] == 1){
			return 'class="arriba"';
		}elseif($orden["sentido"] == 2){
			return 'class="abajo"';
		}
	}else{
		return 'class="nada"';
	}		
}


/**
*g_link() genera el str para el link GET
*
*@param string $fechaini
*@param string $fechafin
*@param array $orden_actual
*@param string $orden_nuevo
*@param string $codigos
*@retrun string string para el get.
*/

function g_link($fechaini, $fechafin, $orden_actual, $orden_nuevo, $codigos, $compro){
		
	$orden_get["col"]=$orden_nuevo;
	
	if($orden_nuevo==$orden_actual["col"]){
		$orden_get["sentido"] = ($orden_actual["sentido"]==1)? 2 : 1;
	}else{
		$orden_get["sentido"] = 1;
	}	
		
	$codigo_get = "";
	foreach($codigos as $codigo){
	$codigo_get .= "&c[]=".$codigo;
	}
		
	$link = "admin2.php?";
	$link .='fechadesde='.mysql2url($fechaini).'&fechahasta='.mysql2url($fechafin).$codigo_get.'&col='.$orden_get["col"].'&sentido='.$orden_get["sentido"].'&compro='.$compro;
	
	return $link;
}



	

?>