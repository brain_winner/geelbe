<?php 

	$code = $_REQUEST["code"];
	$IdProducto = $_REQUEST["IdProducto"];
	$IdCampania = $_REQUEST["IdCampania"];
	$IdCategoria = $_REQUEST["IdCategoria"];
	$codact = $_REQUEST["codact"];
	$countryParam = $_REQUEST["country"];

	if($countryParam == 'ar')
	 	require_once 'includes/config-ar.php';
	else
	 	require_once 'includes/config-co.php';

	$redirect_params = "";
	if (isset($IdProducto)) {
		$redirect_params .= "&IdProducto=".$IdProducto;
	}
	if (isset($IdCampania)) {
		$redirect_params .= "&IdCampania=".$IdCampania;
	}
	if (isset($IdCategoria)) {
		$redirect_params .= "&IdCategoria=".$IdCategoria;
	}
	if (isset($codact)) {
		$redirect_params .= "&codact=".urlencode($codact);
	}
	if (isset($countryParam)) {
		$redirect_params .= "&country=".$countryParam;
	}
	
    if(empty($code)) {
        $dialog_url = "http://www.facebook.com/dialog/oauth?client_id=" 
            . API_ID . "&redirect_uri=" . urlencode("http://".$_SERVER["SERVER_NAME"]."/appfacebook/register_no_popup.php?".substr($redirect_params, 1))
            . "&display=popup&scope=user_birthday,email,publish_stream,offline_access,rsvp_event";

        echo("<script> top.location.href='" . $dialog_url . "'</script>");
    } else {
    
	    $token_url = "https://graph.facebook.com/oauth/access_token?client_id="
	        . API_ID . "&redirect_uri=" . urlencode("http://".$_SERVER["SERVER_NAME"]."/appfacebook/register_no_popup.php?".substr($redirect_params, 1)) . "&client_secret="
	        . API_SECRET . "&code=" . $code;

	    $access_token = file_get_contents($token_url);
	    
	    $graph_url = "https://graph.facebook.com/me?" . $access_token;
	
	    $user = json_decode(file_get_contents($graph_url));
	    $user->uid = $user->id;

	  	$ch = curl_init(CO_REGISTRY_SERVICE);
	  	//DEV TESTING
	 	//$ch = curl_init('http://www.dev.geelbe.com/ar/api/coregistro-facebook.php');
	    curl_setopt ($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    
	    define('REGISTRY_HASH', 'i98DjvxF7YfSS8lDJhj');
	    curl_setopt ($ch, CURLOPT_POSTFIELDS, "key=" . REGISTRY_HASH . "&codact=" . $codact . "&user_data=" . json_encode($user));
	    
	    $response = curl_exec ($ch);
	    curl_close ($ch);


	    if (!$response) {
	      header("Location: http://".$_SERVER['SERVER_NAME'].'/'.$_GET['country']);
	      exit;
	    }
	    $data = json_decode($response);

	    if (!$data) {
	      throw new Exception("ERROR - curl not parse json response");
	    }

	    if ($data->error == "true") {
	  	  header("Location: http://".$_SERVER['SERVER_NAME'].'/'.$_GET['country']);
	  	  exit;
	    }
	    
	    echo "<script>window.location.href='".$data->url_redirect.$redirect_params."';</script>";
    }
    
?>
