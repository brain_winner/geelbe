<?php
    session_start();
    if(!$_SESSION['username'] || !$_SESSION['password']){header("location:index.php");}
    require_once "includes/config.php";
    require_once "includes/functions.php";
?>
<?php include "includes/head.php"; ?>
<body>
    <?php include "includes/menu.php"; ?>
    <div id="wrap">
    	<h1>
            Gifts
            <input type="button" value="Export CSV" class="mainActionButton export" onClick="document.location.href='includes/csv_generator.php?export=gifts';" />
            <div class="paginacion">
            	<div class="labelExt">Show</div>
            	<?php echo paginacion($_REQUEST['orderby'], $_REQUEST['orderdir'], $_REQUEST['from'], 'gifts'); ?>
            </div>
            <div class="totales">
            	<div class="labelExt">Stats</div>
                <span><strong>Total:</strong> <?php echo getTableRows("gifts"); ?></span>
            </div>
        </h1>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr class="order">
                <th><a href="?orderby=post_id&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Post ID</a></th>
                <th><a href="?orderby=campaign_id&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Campaign ID</a></th>
                <th><a href="?orderby=campaign_name&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Campaign Name</a></th>
                <th><a href="?orderby=product_id&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Product ID</a></th>
                <th><a href="?orderby=product_name&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Product Name</a></th>
                <th><a href="?orderby=from_uid&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Sender User</a></th>
                <th><a href="?orderby=to_uid&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Receiver User</a></th>
                <th><a href="?orderby=country&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Country</a></th>
                <th><a href="?orderby=date&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Date</a></th>
            </tr>
            <?php echo selectGifts($_GET['orderby'], $_GET['orderdir'], $_GET['from']); ?>
            <tr>
                <td colspan="9" class="shadow"><img src="images/shadow.gif" /></td>
            </tr>
        </table>
        <div class="clearBoth"></div>
    </div>
</body>
</html>