<?php
    session_start();
    if(!$_SESSION['username'] || !$_SESSION['password']){header("location:index.php");}
    require_once "includes/config.php";
    require_once "includes/functions.php";
?>
<?php include "includes/head.php"; ?>
<body>
    <?php include "includes/menu.php"; ?>
    <div id="wrap">
    	<h1>
            Likes
            <input type="button" value="Export CSV" class="mainActionButton export" onClick="document.location.href='includes/csv_generator.php?export=likes';" />
            <div class="paginacion">
            	<div class="labelExt">Show</div>
            	<?php echo paginacion($_REQUEST['orderby'], $_REQUEST['orderdir'], $_REQUEST['from'], 'likes'); ?>
            </div>
            <div class="totales">
            	<div class="labelExt">Stats</div>
                <span><strong>Total:</strong> <?php echo getTableRows("likes"); ?></span>
            </div>
        </h1>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr class="order">
                <th><a href="?orderby=campaign_id&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Campaign ID</a></th>
                <th><a href="?orderby=campaign_name&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Campaign Name</a></th>
                <th><a href="?orderby=uid&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">User</a></th>
                <th><a href="?orderby=country&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Country</a></th>
                <th><a href="?orderby=date&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Date</a></th>
            </tr>
            <?php echo selectLikes($_GET['orderby'], $_GET['orderdir'], $_GET['from']); ?>
            <tr>
                <td colspan="5" class="shadow"><img src="images/shadow.gif" /></td>
            </tr>
        </table>
        <div class="clearBoth"></div>
    </div>
</body>
</html>