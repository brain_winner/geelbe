<?php
	session_start();
	if(!$_SESSION['username'] || !$_SESSION['password']){header("location:index.php");}
	require_once "includes/config.php";
	require_once "includes/functions.php";
?>
<?php include "includes/head.php"; ?>
<body>
    <?php include "includes/menu.php"; ?>
    <div id="wrap">
    	<h1>
            Invites
            <input type="button" value="Export CSV" class="mainActionButton export" onClick="document.location.href='includes/csv_generator.php?export=invites';" />
            <div class="paginacion">
            	<div class="labelExt">Show</div>
            	<?php echo paginacion($_REQUEST['orderby'], $_REQUEST['orderdir'], $_REQUEST['from'], 'invites'); ?>
            </div>
            <div class="totales">
            	<div class="labelExt">Stats</div>
                <span><strong>Total:</strong> <?php echo getTableRows("invites"); ?></span>
                <span><strong>Invited:</strong> <?php echo getTableRows("invites WHERE status='invited'"); ?></span>
                <span><strong>Confirmed:</strong> <?php echo getTableRows("invites WHERE status='confirmed'"); ?></span>
                <span><strong>Installed:</strong> <?php echo getTableRows("invites WHERE status='installed'"); ?></span>
                <span><strong>Removed:</strong> <?php echo getTableRows("invites WHERE status='removed'"); ?></span>
            </div>
        </h1>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr class="order">
                <th><a href="?orderby=uid&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">User</a></th>
                <th><a href="?orderby=inviter&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Inviter User</a></th>
                <th><a href="?orderby=status&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Status</a></th>
                <th><a href="?orderby=country&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Country</a></th>
                <th><a href="?orderby=promo_id&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Promo ID</a></th>
                <th><a href="?orderby=date_created&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Date Created</a></th>
                <th><a href="?orderby=date_modified&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Date Modified</a></th>
            </tr>
            <?php echo selectInvites($_GET['orderby'], $_GET['orderdir'], $_GET['from']); ?>
            <tr>
                <td colspan="7" class="shadow"><img src="images/shadow.gif" /></td>
            </tr>
        </table>
        <div class="clearBoth"></div>
    </div>
</body>
</html>