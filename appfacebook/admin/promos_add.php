<?php
    session_start();
    if(!$_SESSION['username'] || !$_SESSION['password']){header("location:index.php");}
    require_once "includes/config.php";
    require_once "includes/functions.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/appfacebook/includes/classes/upload.php";

    $maxFileSize = 1024*250;
    $theresAnError = false;
    $imageUpload = new file_upload;

    $imageUpload->upload_dir = $_SERVER['DOCUMENT_ROOT']."/appfacebook/upload/";
    $imageUpload->extensions = array(".png", ".jpg", ".jpeg", ".gif");
    $imageUpload->max_length_filename = 200;
    $imageUpload->rename_file = true;

    if(isset($_POST['Submit'])) {
        $imageUpload->the_temp_file = $_FILES['image']['tmp_name'];
        $imageUpload->the_file = $_FILES['image']['name'];
        $imageUpload->http_error = $_FILES['image']['error'];
        $imageUpload->replace = "y";
        $imageUpload->do_filename_check = "n";

        if ($imageUpload->upload(uniqid())) {
            $full_path = $imageUpload->upload_dir.$imageUpload->file_copy;
            $imageError = $imageUpload->show_error_string();

            if($imageError == 'uploaded') {
              $imageFilename = $imageUpload->file_copy;

              $is_active = isset($_REQUEST['is_active']) ? $_REQUEST['is_active'] : 0;

              db("INSERT INTO promos (title, description, image, show_winners, is_active, duration, country, date_created) VALUES ('".$_REQUEST['title']."', '".$_REQUEST['description']."', '".$imageFilename."', '0', '".$is_active."', '".$_REQUEST['duration_from']." / ".$_REQUEST['duration_to']."', '".$_REQUEST['country']."', NOW());");
              @header("Location: promos.php");

            } else {
              echo "<div class='error' onclick='this.style.display = \"none\";'>";
              echo "  <div>";
              echo      $imageError;
              echo "  </div>";
              echo "</div>";
            }
        }
    }
?>
<?php include "includes/head.php"; ?>
<body>
  <script type="text/javascript" src="js/calendarDateInput.js"></script>
    <?php include "includes/menu.php"; ?>
    <div id="wrap">
      <h1>Add Promo</h1>
      <?php
        if (isset($_REQUEST['error'])) {
          echo "<div class='error' onclick='this.style.display = \"none\"; document.getElementById(\"".(isset($_REQUEST['focus_on_field']) ? $_REQUEST['focus_on_field'] : 'title')."\").focus();'>";
          echo "  <div>";
          echo      urldecode($_REQUEST['error']);
          echo "  </div>";
          echo "</div>";
        }
      ?>
      <form name="form1" enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $maxFileSize; ?>">
        <table border="0" cellspacing="0" cellpadding="0" class="form">
          <tr>
            <td class="label">Title</td>
            <td><input type="text" name="title" id="title" value="<?php echo isset($title) ? $title : ''?>"></td>
          </tr>
          <tr>
            <td class="label">Description</td>
            <td><textarea name="description" id="description" cols="" rows=""><?php echo isset($description) ? $description : ''; ?></textarea></td>
          </tr>
          <tr>
            <td class="label">Image <br><small>(473x148)</small></td>
            <td>
              <input type="file" name="image">
            </td>
          </tr>
          <tr>
            <td class="label">Duration</td>
            <td class="calendar"><span class="labeltext">from</span><script>DateInput('duration_from', true, 'DD-MM-YYYY');</script><span class="labeltext">to</span><script>DateInput('duration_to', true, 'DD-MM-YYYY');</script></td>
          </tr>
          <tr>
            <td class="label">Country</td>
            <td>
              <select name="country" id="country">
                <option value="AR">Argentina</option>
                <option value="MX">M&eacute;xico</option>
                <option value="CO">Colombia</option>
              </select>
            </td>
          </tr>
          <tr>
            <?php
              $result = db("SELECT is_active FROM promos WHERE is_active = 1 AND country='".$country."';");
              $theresAnActivePromo = mysql_num_rows($result);
            ?>
            <td class="label">Is Active</td>
            <td><input value="1" type="checkbox" name="is_active" id="is_active" class="checkbox" <?php echo $theresAnActivePromo ? 'disabled="disabled"' : '';?>></td>
          </tr>
          <tr>
            <td colspan="3" class="button noHover">
              <input value="Add" type="Submit" name="Submit" class="true">
              <input type="button" value="Cancel" class="false" onclick="document.location.href='promos.php';">
            </td>
          </tr>
          <tr>
            <td colspan="2" class="shadow"><img src="images/shadow.gif" /></td>
          </tr>
        </table>
      </form>
      <div class="clearBoth"></div>
    </div>
    <script type="text/javascript">
      document.getElementById('<?php echo isset($_REQUEST['focus_on_field']) ? $_REQUEST['focus_on_field'] : 'title'; ?>').focus();
    </script>
</body>
</html>