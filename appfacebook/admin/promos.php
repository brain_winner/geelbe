<?php
  session_start();
  if(!$_SESSION['username'] || !$_SESSION['password']){header("location:index.php");}
  require_once "includes/config.php";
  require_once "includes/functions.php";
?>
<?php include "includes/head.php"; ?>
<body>
  <?php include "includes/menu.php"; ?>
  <div id="wrap">
    <h1>
      Promos
      <input type="button" value="Export CSV" class="mainActionButton export" onClick="document.location.href='includes/csv_generator.php?export=promos';" />
      <input type="button" value="Add Promo" class="mainActionButton agregar" onClick="document.location.href='promos_add.php';" />
      <div class="paginacion">
        <div class="labelExt">Show</div>
        <?php echo paginacion($_REQUEST['orderby'], $_REQUEST['orderdir'], $_REQUEST['from'], 'promos'); ?>
      </div>
      <div class="totales">
        <div class="labelExt">Stats</div>
        <span><strong>Total:</strong> <?php echo getTableRows("promos"); ?></span>
      </div>
    </h1>
    <table border="0" cellspacing="0" cellpadding="0" class="promos">
      <tr class="order">
        <th><a href="?orderby=title&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Title</a></th>
        <th><a href="?orderby=description&orderdir=<?php echo $_REQUEST['orderdir'] == 'ASC' ? 'DESC' : 'ASC' ?>&from=<?php echo $_REQUEST['from'] ? $_REQUEST['from'] : '0' ?>">Description</a></th>
        <th>Image</th>
        <th>Show&nbsp;Winners</th>
        <th>Active</th>
        <th style="width:130px;">Duration</th>
        <th>Country</th>
        <th>Invites</th>
        <th style="width:100px;">Actions</th>
      </tr>
      <?php echo selectPromos($_GET['orderby'], $_GET['orderdir'], $_GET['from']); ?>
      <tr>
        <td colspan="9" class="shadow"><img src="images/shadow.gif" /></td>
      </tr>
    </table>
    <div class="clearBoth"></div>
  </div>
</body>
</html>