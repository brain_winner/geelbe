<?php
    $url = $_SERVER['REQUEST_URI'];
    if(strpos($url, "index.php")){$selected = "index";}
    else if(strpos($url, "invites.php")){$selected = "invites";}
    else if(strpos($url, "likes.php")){$selected = "likes";}
    else if(strpos($url, "gifts.php")){$selected = "gifts";}
    else if(strpos($url, "winners.php")){$selected = "winners";}
    else if(strpos($url, "promos.php") || strpos($url, "promos_add.php") || strpos($url, "promos_edit.php") || strpos($url, "promos_remove.php")){$selected = "promos";}
    else {$selected = "";}

    $uadmin = $selected=='usuarios_admin' ? 'selected' : '';
?>
<ul id="menu">
    <li class="title">Geelbe Admin</li>
<?php
    if(isset($_SESSION['username']) && isset($_SESSION['password']))
    {
?>
    <li class="<?php echo $selected=="promos" ? "selected" : "" ?>"><a href="promos.php" title="Promos">Promos</a></li>
    <li class="<?php echo $selected=="invites" ? "selected" : "" ?>"><a href="invites.php?orderby=id&orderdir=ASC&from=0" title="Invites">Invites</a></li>
    <li class="<?php echo $selected=="winners" ? "selected" : "" ?>"><a href="winners.php" title="Winners">Winners</a></li>
    <li class="<?php echo $selected=="likes" ? "selected" : "" ?>"><a href="likes.php?orderby=id&orderdir=ASC&from=0" title="Likes">Likes</a></li>
    <li class="<?php echo $selected=="gifts" ? "selected" : "" ?>"><a href="gifts.php?orderby=id&orderdir=ASC&from=0" title="Gifts">Gifts</a></li>
<?php
    }

    if($selected != "index")
    {
        echo("<li class='right'><span>".(isset($_SESSION['username']) ? $_SESSION["username"] : '')."</span><a href='includes/checklogin.php?action=logout' title='Logout'>Logout</a></li>");
    }
?>
</ul>