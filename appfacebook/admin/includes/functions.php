<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/appfacebook/includes/facebook/facebook.php';

function getUsersNames($uids) {
  $facebook = new Facebook('78c610bf2c314b23dcadc925b8df07c0', 'bcd1e30c04c9a0cf1dfdd36d586d6f2d');
  $userData = $facebook->api_client->users_getInfo($uids, 'name');
  return $userData;
}

function db($query) {
  ${'conn' . intval(microtime())} = mysql_connect(DB_HOST, DB_USER, DB_PASS);
  if (!${'conn' . intval(microtime())}) {
    die(mysql_error());
  }
  mysql_select_db(DB_NAME);

  $retval = mysql_query($query, ${'conn' . intval(microtime())});
  if (!$retval) {
    die(mysql_error());
  } else if ($retval) {
    return($retval);
  }

  mysql_close(${'conn' . intval(microtime())});
}

function selectInvites($orderBy, $orderDir, $from) {
  $orderBy ? $orderBy = $orderBy : $orderBy = 'id';
  $orderDir ? $orderDir = $orderDir : $orderDir = 'DESC';
  $from ? $from = $from : $from = '0';
  $itemsPerPage = 250;
  $paginacion = "";
  $totalItems = getTableRows('invites');
  $totalPages = ceil($totalItems / $itemsPerPage);

  $content = "";
  $paginacion = "";
  $count = 0;

  $uids = array();
  $usersNames = array();
  $result = db("SELECT * FROM invites ORDER BY " . $orderBy . " " . $orderDir . " LIMIT " . $from . ", " . $itemsPerPage);
  while ($row = mysql_fetch_array($result)) {
    array_push($uids, strval($row['uid']));
    if ($row['inviter'] != 'FANPAGE') {
      array_push($uids, strval($row['inviter']));
    }
  }
  array_unique($uids);
  $names = getUsersNames($uids);
  foreach ($names as $name) {
    $usersNames[$name['uid']] = $name['name'];
  }

  $retval = db("SELECT * FROM invites ORDER BY " . $orderBy . " " . $orderDir . " LIMIT " . $from . ", " . $itemsPerPage);
  while ($row = mysql_fetch_array($retval)) {
    $content .= "<tr class='row" . ($count % 2) . "'>";
    $content .= "<td><a href='http://www.facebook.com/profile.php?id=" . $row['uid'] . "' target='blank' class='name'>" . (isset($usersNames[$row['uid']]) ? htmlentities(utf8_decode($usersNames[$row['uid']])) : $row['uid']) . "</a></td>";
    $content .= "<td><a href='http://www.facebook.com/profile.php?id=" . $row['inviter'] . "' target='blank' class='name'>" . (is_numeric($row['inviter']) || is_int($row['inviter']) ? htmlentities(utf8_decode($usersNames[$row['inviter']])) : $row['inviter']) . "</a></td>";
    $content .= "<td>" . $row['status'] . "</td>";
    $content .= "<td>" . $row['country'] . "</td>";
    $content .= "<td>" . $row['promo_id'] . "</td>";
    $content .= "<td>" . $row['date_created'] . "</td>";
    $content .= "<td>" . $row['date_modified'] . "</td>";
    $content .= "</tr>";
    $count++;
  }

  return($content);
}

function selectLikes($orderBy, $orderDir, $from) {
  $orderBy ? $orderBy = $orderBy : $orderBy = 'id';
  $orderDir ? $orderDir = $orderDir : $orderDir = 'DESC';
  $from ? $from = $from : $from = '0';
  $itemsPerPage = 250;
  $paginacion = "";
  $totalItems = getTableRows('invites');
  $totalPages = ceil($totalItems / $itemsPerPage);

  $content = "";
  $paginacion = "";
  $count = 0;

  $uids = array();
  $usersNames = array();
  $result = db("SELECT * FROM likes ORDER BY " . $orderBy . " " . $orderDir . " LIMIT " . $from . ", " . $itemsPerPage);
  while ($row = mysql_fetch_array($result)) {
    if (!empty($row['uid']) && is_numeric($row['uid'])) {
      array_push($uids, (string) $row['uid']);
    }
  }
  array_unique($uids);
  $names = getUsersNames($uids);
  foreach ($names as $name) {
    $usersNames[$name['uid']] = $name['name'];
  }

  $retval = db("SELECT * FROM likes ORDER BY " . $orderBy . " " . $orderDir . " LIMIT " . $from . ", " . $itemsPerPage);
  while ($row = mysql_fetch_array($retval)) {
    $content .= "<tr class='row" . ($count % 2) . "'>";
    $content .= "<td>" . $row['campaign_id'] . "</td>";
    $content .= "<td>" . $row['campaign_name'] . "</td>";
    $content .= "<td><a href='http://www.facebook.com/profile.php?id=" . $row['uid'] . "' target='blank' class='name'>" . (isset($usersNames[$row['uid']]) ? htmlentities(utf8_decode($usersNames[$row['uid']])) : $row['uid']) . "</a></td>";
    $content .= "<td>" . $row['country'] . "</td>";
    $content .= "<td>" . $row['date'] . "</td>";
    $content .= "</tr>";
    $count++;
  }

  return($content);
}

function selectGifts($orderBy, $orderDir, $from) {
  $orderBy ? $orderBy = $orderBy : $orderBy = 'id';
  $orderDir ? $orderDir = $orderDir : $orderDir = 'DESC';
  $from ? $from = $from : $from = '0';
  $itemsPerPage = 250;
  $paginacion = "";
  $totalItems = getTableRows('invites');
  $totalPages = ceil($totalItems / $itemsPerPage);

  $content = "";
  $paginacion = "";
  $count = 0;

  $uids = array();
  $usersNames = array();
  $result = db("SELECT * FROM gifts ORDER BY " . $orderBy . " " . $orderDir . " LIMIT " . $from . ", " . $itemsPerPage);
  while ($row = mysql_fetch_array($result)) {
    if (!empty($row['from_uid']) && !empty($row['to_uid']) && is_numeric($row['from_uid']) && is_numeric($row['to_uid'])) {
      array_push($uids, (string) $row['from_uid']);
      array_push($uids, (string) $row['to_uid']);
    }
  }
  array_unique($uids);
  $names = getUsersNames($uids);
  foreach ($names as $name) {
    $usersNames[$name['uid']] = $name['name'];
  }

  $retval = db("SELECT * FROM gifts ORDER BY " . $orderBy . " " . $orderDir . " LIMIT " . $from . ", " . $itemsPerPage);
  while ($row = mysql_fetch_array($retval)) {
    $content .= "<tr class='row" . ($count % 2) . "'>";
    $content .= "<td>" . $row['post_id'] . "</td>";
    $content .= "<td>" . $row['campaign_id'] . "</td>";
    $content .= "<td>" . $row['campaign_name'] . "</td>";
    $content .= "<td>" . $row['product_id'] . "</td>";
    $content .= "<td>" . $row['product_name'] . "</td>";
    $content .= "<td><a href='http://www.facebook.com/profile.php?id=" . $row['from_uid'] . "' target='blank' class='name'>" . (isset($usersNames[$row['from_uid']]) ? htmlentities(utf8_decode($usersNames[$row['from_uid']])) : $row['from_uid']) . "</a></td>";
    $content .= "<td><a href='http://www.facebook.com/profile.php?id=" . $row['to_uid'] . "' target='blank' class='name'>" . (isset($usersNames[$row['to_uid']]) ? htmlentities(utf8_decode($usersNames[$row['to_uid']])) : $row['to_uid']) . "</a></td>";
    $content .= "<td>" . $row['country'] . "</td>";
    $content .= "<td>" . $row['date'] . "</td>";
    $content .= "</tr>";
    $count++;
  }

  return($content);
}

function selectPromos($orderBy, $orderDir, $from) {
  $orderBy ? $orderBy = $orderBy : $orderBy = 'id';
  $orderDir ? $orderDir = $orderDir : $orderDir = 'ASC';
  $from ? $from = $from : $from = '0';
  $itemsPerPage = 250;
  $paginacion = "";
  $totalItems = getTableRows('promos');
  $totalPages = ceil($totalItems / $itemsPerPage);

  $content = "";
  $paginacion = "";
  $count = 0;
  $retval = db("SELECT * FROM promos ORDER BY " . $orderBy . " " . $orderDir . " LIMIT " . $from . ", " . $itemsPerPage);
  while ($row = mysql_fetch_array($retval)) {
    if ($row['id'] != 999999998 && $row['id'] != 999999999 && $row['id'] != 999999997) {
      $content .= "<tr class='row" . ($count % 2) . "'>";
      $content .= "<td>" . $row['title'] . "</td>";
      $content .= "<td>" . substr($row['description'], 0, 110) . (strlen($row['description']) <= 110 ? '' : '...') . "</td>";
      $content .= "<td>" . ($row['image'] != '' ? '<img src="images/icons/true.png">' : '<img src="images/icons/false.png">') . "</td>";
      $content .= "<td class='alignCenter'><img src='images/icons/" . ($row['show_winners'] == 1 ? 'true' : 'false') . ".png'></td>";
      $content .= "<td class='alignCenter'><img src='images/icons/" . ($row['is_active'] == 1 ? 'true' : 'false') . ".png'></td>";
      $content .= "<td>" . $row['duration'] . "</td>";
      $content .= "<td>" . $row['country'] . "</td>";
      $result = db("SELECT * FROM invites WHERE promo_id=" . $row['id'] . ";");
      $content .= "<td>" . mysql_num_rows($result) . "</td>";
      $content .= "<td class='button'>";
      $content .= '<input value="" type="button" class="editar noText" onClick="document.location.href=\'promos_edit.php?id=' . $row['id'] . '\';" />';
      $content .= '<input value="" type="button" class="borrar noText" onClick="document.location.href=\'promos_remove.php?id=' . $row['id'] . '\';" />';
      $content .= "</td>";
      $content .= "</tr>";
      $count++;
    }
  }

  return($content);
}

function getTableRows($table) {
  $count = 0;
  $retval = db("SELECT * FROM " . $table . ";");
  return(mysql_num_rows($retval));
}

function paginacion($orderBy, $orderDir, $from, $table) {
  $orderBy ? $orderBy = $orderBy : $orderBy = 'id';
  $orderDir ? $orderDir = $orderDir : $orderDir = 'DESC';
  $from ? $from = $from : $from = '0';
  $itemsPerPage = 250;
  $result = "";
  $options = "";
  $totalItems = getTableRows($table);
  $totalPages = ceil($totalItems / $itemsPerPage);


  for ($p = 0; $p < $totalPages; $p++) {
    $numFrom = $p * $itemsPerPage;
    $numTo = $p == ($totalPages - 1) ? $totalItems : ($p * $itemsPerPage) + $itemsPerPage;
    $selected = $_REQUEST['from'] == $numFrom ? 'selected="selected" style="font-weight:bold;"' : '';
    if ($_REQUEST['from'] == $numFrom) {
      $next = ($p + 1) * $itemsPerPage;
      $prev = ($p - 1) * $itemsPerPage;
    }
    $options .= "<option value=\"?orderby=" . $orderBy . "&orderdir=ASC&from=" . $numFrom . "\" " . $selected . ">" . $numFrom . "-" . $numTo . "</a>";
  }

  $result .= $prev >= 0 ? "<a href='?orderby=id&orderdir=ASC&from=" . $prev . "'>&lt; Anterior</a>" : '';
  $result .= "<select onchange=\"document.location.href = this.value;\">";
  $result .= $options;
  $result .= "</select>";
  $result .= $next <= $totalItems ? "<a href='?orderby=id&orderdir=ASC&from=" . $next . "'>Siguiente &gt;</a>" : '';

  return($result);
}

function selectWinners($country, $promo_id) {
  $count = 1;
  $uids = array();
  $usersNames = array();
  $result = db("SELECT COUNT(*) AS cnt, inviter, promo_id FROM invites WHERE country = '" . $country . "' AND promo_id = " . $promo_id . " AND (status = 'invited' OR status = 'confirmed') GROUP BY inviter HAVING cnt >1 ORDER BY cnt DESC;");
  $howMany = mysql_num_rows($result);
  if ($howMany > 0) {
    while ($row = mysql_fetch_array($result)) {
      array_push($uids, strval($row['inviter']));
    }
    array_unique($uids);
    $names = getUsersNames($uids);
    foreach ($names as $name) {
      $usersNames[$name['uid']] = $name['name'];
    }

    $invalidatedUIDs = array();
    $qinvalids = "SELECT uid FROM users_invalid;";
    $rinvalids = db($qinvalids);
    while($invalid = mysql_fetch_assoc($rinvalids)) {
      array_push($invalidatedUIDs, $invalid['uid']);
    }

    $firstThree = array();
    $result = db("SELECT * FROM promos_winners WHERE country = '{$country}' AND promo_id = '{$promo_id}' ORDER BY invites DESC;");
    if (mysql_num_rows($result) > 0) {
      while ($row = mysql_fetch_array($result)) {
        if(!in_array($row['uid'], $invalidatedUIDs)) {
          echo '<tr>';
          echo '<td' . ($count <= 3 ? ' style="font-weight:bold;"' : '') . '>';
          echo '<a href="http://www.facebook.com/profile.php?id=' . $row['uid'] . '" target="_blank" class="name">' . (isset($usersNames[$row['uid']]) ? htmlentities(utf8_decode($usersNames[$row['uid']])) : $row['uid']) . '</a>';
          echo '</td>';
          echo '<td>' . $row['invites'] . '</td>';
          echo '<td><a style="color:#333;" href="javascript:void(0)" onclick="invalidate(\'' . $row['uid'] . '\', \'promos_winners\');">Invalidate User</a></td>';
          echo '</tr>';
          array_push($firstThree, $row['uid']);
          $count++;
        }
      }
    } else {
      echo '<tr><td colspan="3" style="text-align:center;font-weight:bold;">Winners have not been updated yet, click on the above button to do it.</td></tr>';
    }
    $count2 = 1;
    $result = db("SELECT COUNT(*) AS cnt, inviter, promo_id FROM invites WHERE country = '" . $country . "' AND promo_id = " . $promo_id . " AND (status = 'invited' OR status = 'confirmed') GROUP BY inviter HAVING cnt >1 ORDER BY cnt DESC;");
    while ($row = mysql_fetch_array($result)) {
      if(!in_array($row['inviter'], $invalidatedUIDs)) {
        if ($count2 > 3 && $count2 <= 10) {
          if (!in_array($row['inviter'], $firstThree)) {
            echo '<tr>';
            echo '<td>';
            echo '<a href="http://www.facebook.com/profile.php?id=' . $row['inviter'] . '" target="_blank" class="name">' . (isset($usersNames[$row['inviter']]) ? htmlentities(utf8_decode($usersNames[$row['inviter']])) : $row['inviter']) . '</a>';
            echo '</td>';
            echo '<td>' . $row['cnt'] . '</td>';
            echo '<td><a style="color:#333;" href="javascript:void(0)" onclick="$(\'body, body a, body input\').css(\'cursor\', \'wait\'); invalidate(\'' . $row['inviter'] . '\', \'invites\');">Invalidate User</a></td>';
            echo '</tr>';
          } else {
            $count2--;
          }
        }
        $count2++;
      }
    }
  } else {
    echo '<tr><td colspan="2">No winners found.</td></tr>';
  }
}

function invalidateUser($uid, $table) {
  $uidFieldName = '';
  if ($table == 'promos_winners') {
    $query = "DELETE FROM promos_winners WHERE uid = '{$uid}' LIMIT 1;";
    db($query);
  }

  $query = "INSERT INTO users_invalid (`uid`) VALUES ('{$uid}');";
  db($query);
  @header("Location: winners.php");
}