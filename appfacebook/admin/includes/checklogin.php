<?php
    require_once "config.php";
    require_once "functions.php";

    if($_REQUEST['action'] == 'login')
    {
        $userOk = false;
        $passOk = false;

        $retval = db("SELECT * FROM users_admin;");
        while($row = mysql_fetch_array($retval))
        {
            if($row['username'] == $_POST['username'])
            {
                $userOk=true;

                if($row['password'] == md5($_POST['password']))
                {
                    $passOk=true;
                    break;
                }
            }
        }

        if($userOk && $passOk)
        {
            session_start();
            $_SESSION["username"] = $_POST['username'];
            $_SESSION["password"] = $_POST['password'];
            header("Location: ../promos.php?orderby=title&orderdir=ASC&from=0");
        }
        else if(!$userOk || !$passOk)
        {
            header("Location: ../index.php?error=loginIncorrect");
        }
    }
    else if($_REQUEST['action'] == 'logout')
    {
        session_start();
        unset($_SESSION["username"]);
        unset($_SESSION["password"]);
        session_destroy();
        header("Location: ../index.php");
    }
?>