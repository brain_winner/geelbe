<?php
	header("Expires:0");
	header("Cache-control:private");
	header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
	header("Content-Description:File Transfer");
	header("Content-Type:application/vnd.ms-excel");
	header("Content-disposition:attachment; filename=".$_REQUEST['export'].".csv");
	
	require_once("config.php");
    require_once("functions.php");
	
	$delimiter = ";";
	$break = "\n";
		
	if($_REQUEST['export']=='invites')
	{

		echo "id; inviter; uid; status; country; promo_id; date_created; date_modified".$break;
		$retval = db("SELECT * FROM invites ORDER BY id ASC");
		while($row = mysql_fetch_array($retval))
		{
			echo $row['id'].$delimiter;
			echo $row['inviter'].$delimiter;
			echo $row['uid'].$delimiter;
			echo $row['status'].$delimiter;
            echo $row['country'].$delimiter;
            echo $row['promo_id'].$delimiter;
			echo $row['date_created'].$delimiter;
			echo $row['date_modified'].$break;
		}
	}

        if($_REQUEST['export']=='likes')
	{

		echo "id; campaign_id; uid; country; date".$break;
		$retval = db("SELECT * FROM likes ORDER BY id ASC");
		while($row = mysql_fetch_array($retval))
		{
			echo $row['id'].$delimiter;
			echo $row['campaign_id'].$delimiter;
			echo $row['uid'].$delimiter;
            echo $row['country'].$delimiter;
			echo $row['date'].$break;
		}
	}

    if($_REQUEST['export']=='gifts')
	{

		echo "id; post_id; campaign_id; product_id; from_uid; to_uid; country; date".$break;
		$retval = db("SELECT * FROM gifts ORDER BY id ASC");
		while($row = mysql_fetch_array($retval))
		{
			echo $row['id'].$delimiter;
			echo $row['post_id'].$delimiter;
            echo $row['campaign_id'].$delimiter;
            echo $row['product_id'].$delimiter;
			echo $row['from_uid'].$delimiter;
            echo $row['to_uid'].$delimiter;
            echo $row['country'].$delimiter;
			echo $row['date'].$break;
		}
	}

    if($_REQUEST['export']=='promos')
	{

		echo "id; title; description; image; show_winners; is_active; duration; country; date_created; date_modified".$break;
		$retval = db("SELECT * FROM promos ORDER BY id ASC");
		while($row = mysql_fetch_array($retval))
		{
			echo $row['id'].$delimiter;
			echo $row['title'].$delimiter;
            echo $row['description'].$delimiter;
            echo $row['image'].$delimiter;
            echo $row['show_winners'].$delimiter;
			echo $row['is_active'].$delimiter;
            echo $row['duration'].$delimiter;
            echo $row['country'].$delimiter;
            echo $row['date_created'].$delimiter;
            echo $row['date_modified'].$break;
		}
	}
?>