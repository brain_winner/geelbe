<?php
    session_start();
    if(!$_SESSION['username'] || !$_SESSION['password']){header("location:index.php");}
    require_once "includes/config.php";
    require_once "includes/functions.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/appfacebook/includes/classes/upload.php";

    $result = db("SELECT * FROM promos WHERE id=".$_REQUEST['id'].' LIMIT 1;');
    $promoData = mysql_fetch_array($result);
    $title = $promoData['title'];
    $description = $promoData['description'];
    $image = $promoData['image'];
    $duration = $promoData['duration'] != '' ? explode(" / ", $promoData['duration']) : array(date("d-m-Y"), date("d-m-Y"));
    $duration_from = $duration[0];
    $duration_to = $duration[1];
    $country = $promoData['country'];
    $show_winners = $promoData['show_winners'];
    $is_active = $promoData['is_active'];

    $maxFileSize = 1024*250;
    $theresAnError = false;
    $imageUpload = new file_upload;

    if(isset($_POST['delete_image'])) {
      $result = db("SELECT image FROM promos WHERE id=".$_REQUEST['id']." LIMIT 1;");
      $image = mysql_fetch_assoc($result);
      unlink($_SERVER['DOCUMENT_ROOT'].'/appfacebook/upload/'.$image['image']);
      db("UPDATE promos SET image = '' WHERE id = ".$_REQUEST['id']." LIMIT 1;");
      $image = '';
    }

    $imageUpload->upload_dir = $_SERVER['DOCUMENT_ROOT']."/appfacebook/upload/";
    $imageUpload->extensions = array(".png", ".jpg", ".jpeg", ".gif");
    $imageUpload->max_length_filename = 200;
    $imageUpload->rename_file = true;

    if(isset($_POST['Submit'])) {
      if(isset($_FILES['image'])) {
        $imageUpload->the_temp_file = $_FILES['image']['tmp_name'];
        $imageUpload->the_file = $_FILES['image']['name'];
        $imageUpload->http_error = $_FILES['image']['error'];
        $imageUpload->replace = "y";
        $imageUpload->do_filename_check = "n";
      }

      if ($imageUpload->upload(uniqid())) {
          $full_path = $imageUpload->upload_dir.$imageUpload->file_copy;
          $imageError = $imageUpload->show_error_string();
      }

      if(isset($imageError)) {
        if($imageError == 'uploaded') {
          $imageFilename = $imageUpload->file_copy;
        } else {
          echo "<div class='error' onclick='this.style.display = \"none\";'>";
          echo "  <div>";
          echo      $imageError;
          echo "  </div>";
          echo "</div>";
        }
      } else {
        $imageError = null;
      }

      $show_winners = isset($_REQUEST['show_winners']) ? $_REQUEST['show_winners'] : 0;
      $is_active = isset($_REQUEST['is_active']) ? $_REQUEST['is_active'] : 0;

      db("UPDATE promos SET title='".$_REQUEST['title']."', description='".$_REQUEST['description']."', image='".($imageError == 'uploaded' ? $imageFilename : $image)."', show_winners='".(isset($_REQUEST['show_winners']) ? $_REQUEST['show_winners'] : 0)."', is_active='".(isset($_REQUEST['is_active']) ? $_REQUEST['is_active'] : 0)."', duration='".$_REQUEST['duration_from']." / ".$_REQUEST['duration_to']."', country='".$_REQUEST['country']."' WHERE id = ".$_REQUEST['id']." LIMIT 1;");
      @header("Location: promos.php");
    }
?>
<?php include "includes/head.php"; ?>
<body>
  <script type="text/javascript" src="js/calendarDateInput.js"></script>
    <?php include "includes/menu.php"; ?>
    <div id="wrap">
      <h1>Add Promo</h1>
      <?php
        if (isset($_REQUEST['error'])) {
          echo "<div class='error' onclick='this.style.display = \"none\"; document.getElementById(\"".(isset($_REQUEST['focus_on_field']) ? $_REQUEST['focus_on_field'] : 'title')."\").focus();'>";
          echo "  <div>";
          echo      urldecode($_REQUEST['error']);
          echo "  </div>";
          echo "</div>";
        }
      ?>
      <form name="form1" enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $maxFileSize; ?>">
        <input type="hidden" name="id" id="id" value="<?php echo $_REQUEST['id']; ?>">
        <table border="0" cellspacing="0" cellpadding="0" class="form">
          <tr>
            <td class="label">Title</td>
            <td><input type="text" name="title" id="title" value="<?php echo isset($title) ? $title : ''?>"></td>
          </tr>
          <tr>
            <td class="label">Description</td>
            <td><textarea name="description" id="description" cols="" rows=""><?php echo isset($description) ? $description : ''; ?></textarea></td>
          </tr>
          <tr>
            <td class="label">Image <br><small>(473x148)</small></td>
            <td>
              <?php
                if(!isset($image) || empty($image)) {
              ?>
                <input type="file" name="image">
              <?php
                } else {
              ?>
                <img src="../upload/<?php echo $image;?>" />
            </td>
          </tr>
          <tr>
            <td class="label">Delete Image?</td>
            <td>
              <input type="checkbox" class="checkbox" name="delete_image" id="delete_image" value="1">
              <?php
                }
              ?>
            </td>
          </tr>
          <tr>
            <td class="label">Duration</td>
            <td class="calendar">
              <span class="labeltext">from</span>
              <script>DateInput('duration_from', true, 'DD-MM-YYYY'<?php echo isset($duration_from) ? ', "'.$duration_from.'"' : ''; ?>);</script>
              <span class="labeltext">to</span>
              <script>DateInput('duration_to', true, 'DD-MM-YYYY'<?php echo isset($duration_to) ? ', "'.$duration_to.'"' : ''; ?>);</script>
            </td>
          </tr>
          <tr>
            <td class="label">Country</td>
            <td>
              <select name="country" id="country">
                <option value="AR"<?php echo $country == 'AR' ? ' selected="selected"' : ''; ?>>Argentina</option>
                <option value="MX"<?php echo $country == 'MX' ? ' selected="selected"' : ''; ?>>M&eacute;xico</option>
                <option value="CO"<?php echo $country == 'CO' ? ' selected="selected"' : ''; ?>>Colombia</option>
              </select>
            </td>
          </tr>
          <tr>
            <td class="label">Show Winners</td>
            <td><input value="1" type="checkbox" name="show_winners" id="show_winners" class="checkbox"<?php echo isset($show_winners) && $show_winners == 1 ? ' checked' : ''; ?>></td>
          </tr>
          <tr>
            <?php
              $result = db("SELECT is_active, id FROM promos WHERE is_active = 1 AND country='".$country."';");
              $theresAnActivePromo = mysql_num_rows($result);
              $activePromo = mysql_fetch_array($result);
            ?>
            <td class="label">Is Active</td>
            <td><input value="1" type="checkbox" name="is_active" id="is_active" class="checkbox"<?php echo $theresAnActivePromo && $activePromo['id']!= $_REQUEST['id'] ? ' disabled="disabled"' : '';?><?php echo isset($is_active) && $is_active == 1 ? ' checked' : '';?>></td>
          </tr>
          <tr>
            <td colspan="3" class="button noHover">
              <input value="Edit" type="Submit" name="Submit" class="true">
              <input type="button" value="Cancel" class="false" onclick="document.location.href='promos.php';">
            </td>
          </tr>
          <tr>
            <td colspan="2" class="shadow"><img src="images/shadow.gif" /></td>
          </tr>
        </table>
      </form>
      <div class="clearBoth"></div>
    </div>
    <script type="text/javascript">
      document.getElementById('<?php echo isset($_REQUEST['focus_on_field']) ? $_REQUEST['focus_on_field'] : 'title'; ?>').focus();
    </script>
</body>
</html>