<?php
    session_start();
    if(!$_SESSION['username'] || !$_SESSION['password']){header("location:index.php");}
    require_once "includes/config.php";
    require_once "includes/functions.php";

    if(isset($_REQUEST['remove']) && isset($_REQUEST['id'])) {
      $result = db("SELECT image FROM promos WHERE id=".$_REQUEST['id']." LIMIT 1;");
      $image = mysql_fetch_assoc($result);
      if($image['image']) {
        unlink($_SERVER['DOCUMENT_ROOT'].'/appfacebook/upload/'.$image['image']);
      }
      db("DELETE FROM promos WHERE id=".$_REQUEST['id']);
      @header("Location: promos.php");
    }
?>
<?php include "includes/head.php"; ?>
<body>
    <?php include "includes/menu.php"; ?>
    <div id="wrap">
      <h1>Remove Promo</h1>
      <form action="promos_remove.php">
        <input type="hidden" name="remove" value="true">
        <table border="0" cellspacing="0" cellpadding="0" class="form">
          <tr>
            <?php
              $result = db("SELECT title FROM promos WHERE id=".$_REQUEST['id']." LIMIT 1;");
              $videoTitle = mysql_fetch_assoc($result);
            ?>
            <td class="noBorder">Are you sure you want to remove <?php echo $videoTitle['title']; ?>?</td>
          </tr>
          <tr>
            <td colspan="3" class="button noHover">
              <input value="<?php echo $_REQUEST['id']; ?>" name="id" type="hidden">
              <input value="Remove" type="Submit" class="true">
              <input type="button" value="Cancel" class="false" onclick="document.location.href='promos.php';">
            </td>
          </tr>
          <tr>
            <td colspan="2" class="shadow"><img src="images/shadow.gif" /></td>
          </tr>
        </table>
      </form>
      <div class="clearBoth"></div>
    </div>
</body>
</html>