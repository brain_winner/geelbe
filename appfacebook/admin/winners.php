<?php
error_reporting(E_ALL | E_STRICT);
ini_set("display_errors", 1);
session_start();
if (!$_SESSION['username'] || !$_SESSION['password']) {
  header("location:index.php");
}
require_once "includes/config.php";
require_once "includes/functions.php";

if (isset($_REQUEST['invalidate_uid']) && !empty($_REQUEST['invalidate_uid'])) {
  invalidateUser($_REQUEST['invalidate_uid'], $_REQUEST['table']);
}
?>
<?php include "includes/head.php"; ?>
<body>
  <?php include "includes/menu.php"; ?>
  <div id="wrap">
    <script type="text/javascript">
      function updateWinners() {
        $.ajax({
          cache: false,
          dataType: 'raw',
          url: '/appfacebook/promo/includes/cron_winners.php',
          type: 'GET',
          success: function(data) {
            document.location.href = '/appfacebook/admin/winners.php?output=' + base64_encode(data);
          }
        });
      }

      function invalidate(uid, table) {
        var invalidate = confirm("Are you sure you want to invalidate user with id " + uid + "? You should update winners afterwards.");
        if(invalidate) {
          document.location.href = 'winners.php?invalidate_uid=' + uid + '&table=' + table;
        }
      }

      $(document).ready(function() {
        <?php
        if(isset($_REQUEST['output'])) {
        ?>
            var output_data = eval(base64_decode("<?php echo $_REQUEST['output'];?>"));
            if(output_data.length > 0) {
//              $('#output').fadeIn('slow');
//              $('#output_button').fadeOut('slow');
              for(var i in output_data) {
                $('#output').append(output_data[i].uid + " won promo " + output_data[i].promo_id + " for " + output_data[i].country + " with " + output_data[i].invites + " invites<br />");
              }
            } else {
              alert("Winners table is already up to date.");
            }
        <?php
        }
        ?>
      });
    </script>
    <div title="Click here to close this dialog." id="output" onclick="$(this).fadeOut('slow');"></div>
    <div style="width:100%;float:left;padding:15px 0;font-size:12px;">
      <a href="javascript:void(0)" style="border:1px solid #000;font-weight:bold;text-decoration:none;background:#ccc;padding:5px 15px;float:right;color:#000;" onclick="$('body, body a, body input').css('cursor', 'wait'); updateWinners();" id="output_button">Update Winners</a>
    </div>
    <h1>
      Winners AR
      <select>
        <?php
        $result = db("SELECT * FROM promos WHERE country = 'AR' AND duration NOT LIKE 'Default';");
        while ($promo = mysql_fetch_assoc($result)) {
          echo '<option value="' . $promo['id'] . '" onclick="document.location.href = \'winners.php?promo_ar_id=' . $promo['id'] . '\';"' . ($promo['id'] == $_REQUEST['promo_ar_id'] ? ' selected="selected"' : '') . '>';
          echo $promo['id'] . ': ' . $promo['title'];
          echo '</option>';
        }
        ?>
      </select>
    </h1>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th>User</th>
        <th>Invites</th>
        <th>Actions</th>
      </tr>
      <?php echo selectWinners('AR', (isset($_REQUEST['promo_ar_id']) ? $_REQUEST['promo_ar_id'] : '6')); ?>
        <tr>
          <td colspan="3" class="shadow"><img src="images/shadow.gif" /></td>
        </tr>
      </table>
      <h1>
        Winners MX
        <select>
        <?php
        $result = db("SELECT * FROM promos WHERE country = 'MX' AND duration NOT LIKE 'Default';");
        while ($promo = mysql_fetch_assoc($result)) {
          echo '<option value="' . $promo['id'] . '" onclick="document.location.href = \'winners.php?promo_mx_id=' . $promo['id'] . '\';"' . ($promo['id'] == $_REQUEST['promo_mx_id'] ? ' selected="selected"' : '') . '>';
          echo $promo['id'] . ': ' . $promo['title'];
          echo '</option>';
        }
        ?>
      </select>
    </h1>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th>User</th>
        <th>Invites</th>
        <th>Actions</th>
      </tr>
      <?php echo selectWinners('MX', (isset($_REQUEST['promo_mx_id']) ? $_REQUEST['promo_mx_id'] : '12')); ?>
        <tr>
          <td colspan="3" class="shadow"><img src="images/shadow.gif" /></td>
        </tr>
      </table>
      <h1>
        Winners CO
        <select>
        <?php
        $result = db("SELECT * FROM promos WHERE country = 'CO' AND duration NOT LIKE 'Default';");
        while ($promo = mysql_fetch_assoc($result)) {
          echo '<option value="' . $promo['id'] . '" onclick="document.location.href = \'winners.php?promo_co_id=' . $promo['id'] . '\';"' . ($promo['id'] == $_REQUEST['promo_co_id'] ? ' selected="selected"' : '') . '>';
          echo $promo['id'] . ': ' . $promo['title'];
          echo '</option>';
        }
        ?>
      </select>
    </h1>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th>User</th>
        <th>Invites</th>
        <th>Actions</th>
      </tr>
      <?php echo selectWinners('CO', (isset($_REQUEST['promo_co_id']) ? $_REQUEST['promo_co_id'] : '13')); ?>
      <tr>
        <td colspan="3" class="shadow"><img src="images/shadow.gif" /></td>
      </tr>
    </table>
    <div class="clearBoth"></div>
  </div>
</body>
</html>