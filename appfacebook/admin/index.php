<?php
	require_once "includes/config.php";
	require_once "includes/functions.php";
?>
<?php include "includes/head.php"; ?>
<body>
	<?php include "includes/menu.php"; ?>
	<div id="wrap">
    	<h1>Login</h1>
    	<form name="form1" method="post" action="includes/checklogin.php">
            <input type="hidden" name="action" value="login" />
            <table border="0" cellspacing="0" cellpadding="0" class="login">
                <tr class="noHover">
                    <td class="label">User:</td>
                    <td><input name="username" type="text" id="username"></td>
                </tr>
                <tr class="noHover">
                    <td class="label">Password:</td>
                    <td><input name="password" type="password" id="password"></td>
                </tr>
                <tr>
                    <td colspan="2" class="shadow"><img src="images/shadow_mini.gif" /></td>
            	</tr>
                <tr>
                    <td colspan="2" class="button noHover"><input type="submit" name="Submit" class="noIcon" value="Login"></td>
                </tr>
            </table>
        </form>
        <div class="error" onClick="this.style.display = 'none';" style="display:<?php echo isset($_REQUEST['error']) ? 'block' : 'none'; ?>;"><div>Usuario o Contraseņa incorrectos.</div></div>
    	<div class="clearBoth"></div>
    </div>
</body>
</html>