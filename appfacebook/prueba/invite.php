<?php 
	$_GET["app_id"] = "210368798980001";
	require_once '../includes/config.php';
	require_once '../includes/classes/db.php';
	require_once '../includes/classes/dao/InvitationDAO.php';
	
	$code = $_REQUEST["code"];
	
    $message = "Unite a Geelbe, dale .. vos podes!";
    if(empty($code)) {
    	   $dialog_url = "http://www.facebook.com/dialog/oauth?client_id=" 
            . CONTEST_AUTHORIZE_APP_ID . "&redirect_uri=" . urlencode(CONTEST_INVITE_URL)
            . "&display=popup&scope=user_birthday,email,publish_stream,offline_access,rsvp_event";

        echo("<script> window.location.href='" . $dialog_url . "'</script>");
    } else {
	    $token_url = "https://graph.facebook.com/oauth/access_token?client_id="
	        . CONTEST_AUTHORIZE_APP_ID . "&redirect_uri=" . urlencode(CONTEST_INVITE_URL) . "&client_secret="
	        . CONTEST_AUTHORIZE_APP_SECRET . "&code=" . $code;
	
	    $access_token = file_get_contents($token_url);
	    
	    $graph_url = "https://graph.facebook.com/me?" . $access_token;
	
	    $user = json_decode(file_get_contents($graph_url));
	    
	    $requests_url = "http://www.facebook.com/dialog/apprequests?app_id=" 
	    	. $app_id . "&redirect_uri=" . urlencode(CONTEST_FINISH_URL)
	        . "&message=" . $message . "&display=popup";
	
		if (empty($_REQUEST["request_ids"])) {
	    	echo("<script> top.location.href='" . $requests_url . "'</script>");
		} else {
			$invitationDAO = new InvitationDAO();
	    	foreach($_REQUEST["request_ids"] as $request_id) {
	    		$invitationDAO->saveRequest(INVITATION_PROMO_ID, $request_id, $user->id);
	    	}
		}
    }
?>