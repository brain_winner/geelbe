 <?php 
	$_GET["app_id"] = "210368798980001";
 	require_once '../includes/config.php';

	$code = $_REQUEST["code"];

	
    if(empty($code)) {
        $dialog_url = "http://www.facebook.com/dialog/oauth?client_id=" 
            . CONTEST_AUTHORIZE_APP_ID . "&redirect_uri=" . urlencode(CONTEST_AUTHORIZE_URL)
            . "&display=popup&scope=user_birthday,email,publish_stream,offline_access,rsvp_event";

        echo("<script> top.location.href='" . $dialog_url . "'</script>");
    } else {
	    $token_url = "https://graph.facebook.com/oauth/access_token?client_id="
	        . CONTEST_AUTHORIZE_APP_ID . "&redirect_uri=" . urlencode(CONTEST_AUTHORIZE_URL) . "&client_secret="
	        . CONTEST_AUTHORIZE_APP_SECRET . "&code=" . $code;
	
	    $access_token = file_get_contents($token_url);
	    
	    $graph_url = "https://graph.facebook.com/me?" . $access_token;
	
	    $user = json_decode(file_get_contents($graph_url));
	    $user->uid = $user->id;

	    $ch = curl_init(CO_REGISTRY_SERVICE);
	    curl_setopt ($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt ($ch, CURLOPT_POSTFIELDS, "key=" . REGISTRY_HASH . "&user_data=" . json_encode($user));
	    
	    $response = curl_exec ($ch);
	    curl_close ($ch);
	    if (!$response) {
	      throw new Exception("ERROR - curl response null");
	    }
	
	    $data = json_decode($response);
	
	    if (!$data) {
	      throw new Exception("ERROR - curl not parse json response");
	    }
	    //Devolvemos la url del usuario autologueado
	    if(!isset($data->url_redirect) || empty($data->url_redirect)) {
	      throw new Exception("ERROR - url no viene");
	    }
	    echo "<script>window.opener.location.href='".CONTEST_HOME_URL."';window.close();</script>";
    }
    
 ?>