<script type="text/javascript">
  var activeBlink = false;

  function startPasswordMagic(targetID) {
    var target = document.getElementById(targetID);
    var targetFake = document.getElementById(targetID+'Fake');

    setInterval(function() {
      var magicString = "";
      for(var i=0; i<target.getValue().length; i++) {
        magicString += "*";
      }
      targetFake.setValue(magicString);
    }, 150);
  }

  function register() {

    Facebook.showPermissionDialog('email,user_birthday', function(permissions) {
        if (permissions == null) {
          var dialog = new Dialog();
          dialog.showMessage('Alerta', 'Debes aceptar los permisos extendidos para poder registrate en Geelbe.');
          return false;
        }

        var ajax = new Ajax();
        ajax.responseType = Ajax.FBML;
        ajax.ondone = function (data) {
          console.log(data);
          var dialog = new Dialog();          
          dialog.showMessage('Geelbe', data);
        }

        ajax.onerror = function () {
          console.log(data);
          var dialog = new Dialog();
          dialog.showMessage('Hubo un problema', 'Ha habido un problema al procesar el registro. Por favor intente nuevamente m�s tarde.');
        }

        ajax.post('<?php echo APP_CALLBACK_URL;?>registro/services/register.php');
      }, false);
  }

  function showMessage(type, message) {
    switch (type) {
      case "error":
        document.getElementById('messenger').setStyle('backgroundColor', '#FFA8A8');
        document.getElementById('messenger').setStyle('border', '5px solid #7A0000');
        document.getElementById('messenger').setStyle('color', '#7A0000');
      break;
      case "gracias":
        document.getElementById('messenger').setStyle('backgroundColor', '#ccc');
        document.getElementById('messenger').setStyle('border', '5px solid #333');
        document.getElementById('messenger').setStyle('color', '#333');
      break;
    }

    document.getElementById('messenger').setTextValue(message);
    document.getElementById('messenger').setStyle('display', 'block');
  }
</script>