<?php

if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();} 

class InvitationDAO {

	public function saveRequest($promo_id = null, $request_id = null, $user_id = null) {
		$db = new db();
		$db->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		
		$query = "insert into geelbe_invitations (promo_id, fcb_request_id, user_uid, creation_date) 
				  values (\"".htmlspecialchars(mysql_real_escape_string($promo_id))."\", 
					  	  \"".htmlspecialchars(mysql_real_escape_string($request_id))."\", 
						  \"".htmlspecialchars(mysql_real_escape_string($user_id))."\",
						  NOW())";

		$db->query($query);
		$db->disconnect();
	}

	public function acceptRequest($request_id = null, $invited_user_id = null, $accepted = false) {
		$db = new db();
		$db->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		
		$query = "update geelbe_invitations SET 
						response_date = NOW(), 
						accepted = ".htmlspecialchars(mysql_real_escape_string($accepted)).", 
						invited_user_id = \"".htmlspecialchars(mysql_real_escape_string($invited_user_id))."\" 
				  where request_id = \"".htmlspecialchars(mysql_real_escape_string($request_id))."\"";

		$db->query($query);
		$db->disconnect();
	}
}

?>