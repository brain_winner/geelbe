<?php
    class db {
        function connect($host, $user, $pass, $name = null) {
            ${'conn'.intval(microtime())} = mysql_connect($host, $user, $pass);
            if(!${'conn'.intval(microtime())}) {
                die(mysql_error());
            }
            if($name != null) {
                mysql_select_db($name);
            }
        }

        function query($query) {
            $result = mysql_query($query);
            if(!$result){
                die(mysql_error());
            } else {
                return($result);
            }
        }

        function insert_id() {
          return mysql_insert_id();
        }
        
        function disconnect($con) {
        	mysql_close($con);
        }
    }
?>
