<?php

		ini_set('display_errors', '0');
    require_once("../includes/config.php");
    require_once("../includes/facebook/facebook.php");
		require_once ("../includes/classes/db.php");
	  require_once ("../includes/classes/json_getter_and_decoder.php");
		require_once("../tab/includes/functions.php");

		$facebook = new Facebook(API_KEY, API_SECRET);

		$db = new db();
		$dbConnection = $db->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		$json = new jsonGetterAndDecoder();
		$decodedjson = $json->get_and_decode(JSON_URL);
		$campaigns = $decodedjson->campaigns;
		$gmt = $decodedjson->regional[0]->gmt;
		$campaignsSchedule = array();
		
		for($i=0; $i<count($campaigns); $i++) {
			// echo "Analizing Campaign \"{$campaigns[$i]->brand_name}\" id={$campaigns[$i]->id}: start: " . $campaigns[$i]->start_date . " === end: " . $campaigns[$i]->expire_date . "<br />";

//			if($campaigns[$i]->id == 631) {
//				$campaigns[$i]->expire_date = '2011-04-01 18:00:00';
//			}

			$startDate = parseTimeFromString($campaigns[$i]->start_date);
			$expireDate = parseTimeFromString($campaigns[$i]->expire_date);

			// PUBLICAMOS TODOS LOS FEEDS DE COMIENZO DE CAMPANIA
			if (time() >= $startDate + (2*3600) && time() <= $expireDate - (6*3600)) {			

				$query = "SELECT *, es.id as susc_id
									FROM events_subscription as es
									LEFT JOIN campaign_events as ce ON es.campaign_id = ce.campaign_id and es.country = ce.country
									LEFT JOIN users_calendar as uc ON es.uid = uc.uid and es.country = uc.country
									WHERE es.campaign_id = {$campaigns[$i]->id} and
												es.published_campaign_start = 0 and
												es.country like '" . mysql_real_escape_string(COUNTRY) . "'";
				$result = $db->query($query) or die(mysql_error() . __LINE__);			

				while ($res = mysql_fetch_array($result)) {

					$facebook->clear_cookie_state();
					$facebook->set_user($res['uid'], $res['session_key']);
					// echo "se publica feed de comienzo de campania para el usuario {$res["uid"]} <br />";

					$name = "Comienza la campa�a de {$res["campaign_name"]} en Geelbe. No te la pierdas!";
					if(!mb_check_encoding($name, 'UTF-8') 
				        OR !($name === mb_convert_encoding(mb_convert_encoding($name, 'UTF-32', 'UTF-8' ), 'UTF-8', 'UTF-32'))) { 
				
				        $name = mb_convert_encoding($name, 'UTF-8'); 
			        }

					$description = $campaigns[$i]->campaign_landing_text;
					if(!mb_check_encoding($description, 'UTF-8') 
				        OR !($description === mb_convert_encoding(mb_convert_encoding($description, 'UTF-32', 'UTF-8' ), 'UTF-8', 'UTF-32'))) { 
				
				        $description = mb_convert_encoding($description, 'UTF-8'); 
			        }
					
					try{
						$attachments = array(	"href" => $campaigns[$i]->campaign_url,
							"name" => $name ,
						    "description" => $description,
							"media" => array(
								array("type" => "image",
											"src"  => $res["campaign_picture"],
											"href" => $campaigns[$i]->campaign_url
											)
									)
						);
						$actionLinks =  array(array("text" => mb_convert_encoding("Ver Campa�a", 'UTF-8'),
																			"href" => $campaigns[$i]->campaign_url));

						$response = $facebook->api_client->stream_publish(null,
								json_encode($attachments),
								json_encode($actionLinks));
						$db->query("UPDATE events_subscription
												SET published_campaign_start = 1
												WHERE id = ".mysql_real_escape_string($res["susc_id"])." 
												LIMIT 1") or die(mysql_error() . __LINE__);

					} catch(Exception $e ) {
						echo "Problema: " . $e->getMessage() . "({$e->getCode()})";
					}
				}
			}

			// PUBLICAMOS TODOS LOS FEEDS DE FINALIZACION DE CAMPANIA
			if (time() >= $expireDate - (6*3600) && time() <= $expireDate) {				
				$query = "SELECT *, es.id as susc_id
									FROM events_subscription as es
									LEFT JOIN campaign_events as ce ON es.campaign_id = ce.campaign_id and es.country = ce.country
									LEFT JOIN users_calendar as uc ON es.uid = uc.uid and es.country = uc.country
									WHERE es.campaign_id = ".mysql_real_escape_string($campaigns[$i]->id)." and
												es.published_campaign_end = 0 and
												es.country like '" . mysql_real_escape_string(COUNTRY) . "'";

				$result = $db->query($query) or die(mysql_error() . __LINE__);				

				while ($resF = mysql_fetch_array($result)) {

					$facebook->clear_cookie_state();
					$facebook->set_user($resF['uid'], $resF['session_key']);
					// echo "se publica feed de finalizaci�n para el usuario {$resF["uid"]} <br />";
					try{
						
						$name = "La campa�a de {$resF["campaign_name"]} en Geelbe pronto finalizar�. No te la pierdas!";
						if(!mb_check_encoding($name, 'UTF-8') 
					        OR !($name === mb_convert_encoding(mb_convert_encoding($name, 'UTF-32', 'UTF-8' ), 'UTF-8', 'UTF-32'))) { 
					
					        $name = mb_convert_encoding($name, 'UTF-8'); 
				        }
						
						$description = $campaigns[$i]->campaign_landing_text;
						if(!mb_check_encoding($description, 'UTF-8') 
					        OR !($description === mb_convert_encoding(mb_convert_encoding($description, 'UTF-32', 'UTF-8' ), 'UTF-8', 'UTF-32'))) { 
					
					        $description = mb_convert_encoding($description, 'UTF-8'); 
				        }
				        
						$attachments = array(	"href" => $campaigns[$i]->campaign_url,
								"name" => $name ,
								"description" => $description,
								"media" => array(
										array("type" => "image",
													"src"  => $resF["campaign_picture"],
													"href" => $campaigns[$i]->campaign_url
													)
											)
								);																	
						$actionLinks =  array(array("text" => mb_convert_encoding("Ver Campa�a", 'UTF-8'),
													"href" => $campaigns[$i]->campaign_url));
						$response = $facebook->api_client->stream_publish(null,
										json_encode($attachments),
										json_encode($actionLinks));												

						$db->query("UPDATE events_subscription
												SET published_campaign_end = 1
												WHERE id = ".mysql_real_escape_string($resF["susc_id"])." 
												LIMIT 1") or die(mysql_error() . __LINE__);

					} catch(FacebookRestClientException $e ) {
						echo "Problema: " . $e->getMessage() . "({$e->getCode()})";
					} catch(Exception $e ) {
						echo "Problema: " . $e->getMessage() . "({$e->getCode()})";
					}
				}
			}


			//ACTUALIZAMOS EVENTOS
			$query = "SELECT *
									FROM campaign_events as ce
									WHERE ce.campaign_id = ".mysql_real_escape_string($campaigns[$i]->id)." and (
												ce.campaign_start_date <> '".mysql_real_escape_string($campaigns[$i]->start_date)."' or
												ce.campaign_end_date <> '".mysql_real_escape_string($campaigns[$i]->expire_date)."' ) and
												ce.country like '" . mysql_real_escape_string(COUNTRY) . "'
									LIMIT 1";

			$result = $db->query($query) or die(mysql_error() . __LINE__);
			if ($resE = mysql_fetch_array($result)) {
				$query = "UPDATE campaign_events as ce
									SET ce.campaign_start_date = '".mysql_real_escape_string($campaigns[$i]->start_date)."',
											ce.campaign_end_date = '".mysql_real_escape_string($campaigns[$i]->expire_date)."'
									WHERE ce.campaign_id like '".mysql_real_escape_string($campaigns[$i]->id)."' and
												ce.country like '" . mysql_real_escape_string(COUNTRY) . "'
									LIMIT 1";

				mysql_query($query) or die(mysql_error() . __LINE__);
				
				$facebook->set_user($res['uid'], $res['session_key']);

				try {

					$start = $startDate + $timeDiffence*3600;
					$end = $expireDate + $timeDiffence*3600;
					$accessToken = COUNTRY_ACCESS_TOKEN;
					$ch = curl_init("https://graph.facebook.com/" . $resE["fb_event_id"]);
					curl_setopt ($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
					curl_setopt ($ch, CURLOPT_POSTFIELDS, "access_token={$accessToken}&start_time={$start}&end_time={$end}");
					$response = curl_exec ($ch);
					curl_close ($ch);
					echo "<br />Event updated: " . $response . "<br />";
				} catch(Exception $e) {
						echo $e->getMessage();
				}
				
			}
		}



?>