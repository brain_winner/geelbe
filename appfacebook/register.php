<?php 
	$server = $_SERVER["SERVER_NAME"];
	if ($server == "localhost") {
		$_GET["app_id"] = "214371551958574";
	} else if ($server == "www.dev.geelbe.com") {
		$_GET["app_id"] = "210368798980001";
	} else {
		if($_GET["country"] == "ar") {
			$_GET["app_id"] = "109174705776408";
		} else if($_GET["country"] == "co") {
			$_GET["app_id"] = "130962696927517";
		} else {
			$_GET["app_id"] = "109174705776408";
		}
	}
 	require_once 'includes/config.php';

	$code = $_REQUEST["code"];
	$IdProducto = $_REQUEST["IdProducto"];
	$IdCampania = $_REQUEST["IdCampania"];
	$IdCategoria = $_REQUEST["IdCategoria"];
	$codact = $_REQUEST["codact"];
	$countryParam = $_REQUEST["country"];
	
	$redirect_params = "";
	if (isset($IdProducto)) {
		$redirect_params .= "&IdProducto=".$IdProducto;
	}
	if (isset($IdCampania)) {
		$redirect_params .= "&IdCampania=".$IdCampania;
	}
	if (isset($IdCategoria)) {
		$redirect_params .= "&IdCategoria=".$IdCategoria;
	}
	if (isset($codact)) {
		$redirect_params .= "&codact=".urlencode($codact);
	}
	if (isset($countryParam)) {
		$redirect_params .= "&country=".$countryParam;
	}
	
    if(empty($code)) {
        $dialog_url = "http://www.facebook.com/dialog/oauth?client_id=" 
            . API_ID . "&redirect_uri=" . urlencode("http://".$_SERVER["SERVER_NAME"]."/appfacebook/register.php?".substr($redirect_params, 1))
            . "&display=popup&scope=user_birthday,email,publish_stream,offline_access,rsvp_event";

        echo("<script> top.location.href='" . $dialog_url . "'</script>");
    } else {
	    $token_url = "https://graph.facebook.com/oauth/access_token?client_id="
	        . API_ID . "&redirect_uri=" . urlencode("http://".$_SERVER["SERVER_NAME"]."/appfacebook/register.php?".substr($redirect_params, 1)) . "&client_secret="
	        . API_SECRET . "&code=" . $code;
	
	    $access_token = file_get_contents($token_url);
	    
	    $graph_url = "https://graph.facebook.com/me?" . $access_token;
	
	    $user = json_decode(file_get_contents($graph_url));
	    $user->uid = $user->id;
	    
	    $ch = curl_init(CO_REGISTRY_SERVICE);
	    curl_setopt ($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt ($ch, CURLOPT_POSTFIELDS, "key=" . REGISTRY_HASH . "&codact=" . $codact . "&user_data=" . json_encode($user));
	    
	    $response = curl_exec ($ch);
	    curl_close ($ch);

	    if (!$response) {
	      echo "<p>co-registry response null</p><script>window.close();</script>";
	      exit;
	    }
	    $data = json_decode($response);

	    if (!$data) {
	      throw new Exception("ERROR - curl not parse json response");
	    }

	    if ($data->error == "true") {
	  	  echo "<p>".$data->error_type."</p><script>window.close();</script>";
	  	  exit;
	    }
	    echo "<script>window.opener.location.href='".$data->url_redirect.$redirect_params."';window.close();</script>";
    }
    
?>
