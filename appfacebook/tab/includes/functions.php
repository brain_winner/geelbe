<?php
    require_once ("../includes/classes/json_getter_and_decoder.php");
    require_once ("../includes/classes/db.php");
    require_once ("../includes/facebook/facebook.php");

    $db = new db();
    $dbConnection = $db->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		function getAccessToken($sessionKey, $appId, $appSecret) {

			$post = array(
				'type' => 'client_cred',
				'client_id' => $appId,
				'client_secret' => $appSecret,
				'sessions' => $sessionKey
			);

			$options = array(
				CURLOPT_POST => 1,
				CURLOPT_HEADER => 0,
				CURLOPT_URL => 'https://graph.facebook.com/oauth/exchange_sessions',
				CURLOPT_FRESH_CONNECT => 1,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_POSTFIELDS => http_build_query($post)
			);

			$ch = curl_init();
			curl_setopt_array($ch, $options);
			$result = curl_exec($ch);
			$result = json_decode($result);
			curl_close($ch);
			return $result[0]->access_token;
		}

    function parseTimeFromString($string) {
        $year = intval(substr($string, 0, 4));
        $month = intval(substr($string, 5, 2));
        $day = intval(substr($string, 8, 2));
        $hour = intval(substr($string, 11, 2));
        $minute = intval(substr($string, 14, 2));
        $second = intval(substr($string, 17, 2));
        return(mktime($hour, $minute, $second, $month, $day, $year));
    }
		

    function makeCampaignList() {
        $facebook = new Facebook(API_KEY, API_SECRET);
        $json = new jsonGetterAndDecoder();
        $decodedjson = $json->get_and_decode(JSON_URL);
        $campaigns = $decodedjson->campaigns;
        //$gmt = $decodedjson->regional[0]->gmt;
        $gmt = 0;
		
        $campaignTypes = array('active', 'inactive');
        $timers = array();
        
        for($c=0; $c<count($campaignTypes); $c++) {

            $type = $campaignTypes[$c];
            $kind = ($type == 'inactive') ? false : true;

            if($type=='active') {
                echo "<ul class='campaigns'><h1>Ofertas disponibles<div class='iconHelp' onclick='showPopHelp(\"campaigns\");'></div></h1>";
            } else if($type=='inactive') {
                echo "<h1 style='padding:20px 0 0;'>Campa&ntilde;as Pr&oacute;ximos 7 D&iacute;as</h1>";
            }

            for($i=0; $i<count($campaigns); $i++) {
              
                $startDate = parseTimeFromString($campaigns[$i]->start_date);
                $expireDate = parseTimeFromString($campaigns[$i]->expire_date);
                
                $isActive = ($campaigns[$i]->is_active==0 ? false : true) ? ($campaigns[$i]->is_active==0 ? false : true) : ($startDate>604800 ? false : true);

                $tempDays = abs(($isActive ? $expireDate : $startDate)-(time()+($gmt*3600)))/86400;
                $days = floor($tempDays);
                $tempHours = (abs(floor($tempDays) - $tempDays)*86400)/3600;
                $hours = floor($tempHours);
                $tempMins = (abs(floor($tempHours) - $tempHours)*3600)/60;
                $mins = floor($tempMins);
                $tempSecs = abs(floor($tempMins) - $tempMins)*60;
                $secs = ceil($tempSecs);
                
                $campaignBanner = ($campaigns[$i]->campaign_banner || $campaigns[$i]->campaign_banner_inactive) ? ($isActive ? $campaigns[$i]->campaign_banner : $campaigns[$i]->campaign_banner_inactive) : TAB_CALLBACK_URL.'images/banner_default.gif';

                /*if($i==0) {
                  function save_image($url) {
                    $img = $_SERVER['DOCUMENT_ROOT'].'/appfacebook/upload/cache/test.jpg';
                    $data = file_get_contents($url);
                    var_dump($data);
                  }

                  save_image($campaignBanner);
                }*/
                
                $campaignTime = $isActive ? $days." d&iacute;as ".sprintf("%02d",$hours).":".sprintf("%02d",$mins).":".sprintf("%02d",$secs) : date("d/m/Y h:i", $startDate);
                $campaignID = $campaigns[$i]->id;
                $campaignBrandName = htmlentities(utf8_decode($campaigns[$i]->brand_name));
                $campaignBrandLogo = $isActive ? $campaigns[$i]->brand_logo : $campaigns[$i]->brand_logo_inactive;
                $campaignDescription = htmlentities(utf8_decode($campaigns[$i]->description));
                $campaignURL = $campaigns[$i]->campaign_url;

                if($kind == $isActive) {
                    $products = $campaigns[$i]->products;
                    if(count($products)!=0 && $isActive) {
                        echo '<div id="gift_store_'.$campaignID.'_container">';
                        echo '<fb:dialog id="gift_store_'.$campaignID.'">';
                        echo '    <fb:dialog-title>Enviar Regalo</fb:dialog-title>';
                        echo '    <fb:dialog-content>';
                        echo '        <form id="gift_form_'.$campaignID.'" class="giftForm">';
                        echo '            <div class="bmclearfix giftsContainer" id="gifts_container_'.$campaignID.'">';
                        echo '                <h2>Selecciona Tu Regalo:</h2>';

                        $count = 1;
                        foreach($products as $product) {
                            $giftID = $product->product_id;
                            $giftImage = $product->image;
                            $giftThumb = $product->thumb_image;
                            $giftName = htmlentities(utf8_decode($product->name));
                            $giftDescription = htmlentities(utf8_decode($product->description));

                            if($count==1) {
                              echo '<script>';
                              echo '  var firstGift_'.$campaignID.' = '.$giftID;
                              echo '</script>';
                              echo '        <input type="hidden" id="giftId_'.$campaignID.'" value="'.$giftID.'" />';
                              echo '        <input type="hidden" id="giftImg_'.$campaignID.'" value="'.$giftImage.'" />';
                              echo '        <input type="hidden" id="giftThumb_'.$campaignID.'" value="'.$giftThumb.'" />';
                              echo '        <input type="hidden" id="giftName_'.$campaignID.'" value="'.$giftName.'" />';
                              echo '        <input type="hidden" id="giftDesc_'.$campaignID.'" value="'.$giftDescription.'" />';
                            }
                            
                            echo '            <div class="giftBox'.($count%6 == 0 ? ' noBorder' : ($count == count($products) ? ' noBorder' : '')).'"  title="'.$giftName.'"  id="gift_'.$campaignID.'_'.$giftID.'" onclick="setGiftValues(\''.$campaignID.'\', \''.$giftID.'\', \''.$giftImage.'\', \''.$giftThumb.'\', \''.$giftName.'\', \''.$giftDescription.'\');">';
                            echo '                <img src="'.$giftThumb.'" class="gift_th" alt="'.$giftName.'" title="'.$giftName.'" />';
                            echo '                <div class="frame"></div>';
                            echo '            </div>';

                            $count++;
                        }

                        echo '            </div>';
                        echo '            <div class="clearfix">';
                        echo '                <div class="messageAndButtons">';
                        echo '                    <h2>Para:</h2>';
                        echo '                    <fb:friend-selector name="uid" idname="friend_sel" /><br />';
                        echo '                    <h2>Mensaje:</h2>';
                        echo '                    <textarea cols="" rows="" name="message" id="message_'.$campaignID.'"></textarea>';
                        echo '                    <input type="button" id="send_button_'.$campaignID.'" value="Enviar" class="inputbutton" onclick="sendGift(\''.$campaignID.'\',\''.$campaignURL.'&utm_source=facebook-app&utm_medium=feeds&utm_content=regalo&utm_campaign='.urlencode($campaignBrandName).'\', \''.$campaignBrandName.'\');" />';
                        echo '                    <fb:dialog-button type="button" close_dialog="true" value="Cerrar" />';
                        echo '                </div>';
                        echo '            </div>';
                        echo '        </form>';
                        echo '    </fb:dialog-content>';
                        echo '</fb:dialog>';
                        echo '</div>';
                    }

                    $likeAction = "like(\"likeIcon_".$campaignID."\", \"".$campaignID."\", \"".$campaignURL.'&utm_source=facebook-app&utm_medium=feeds&utm_content=megusta&utm_campaign='.urlencode($campaignBrandName)."\", \"".$campaignBrandName."\", \"".$campaignDescription."\", \"".$campaignBanner."\");";
									$calendarAction = "addCalendarEvent(\"calendarIcon_".$campaignID."\", \"".$campaignID."\", \"".$campaignURL.'&utm_source=facebook-app&utm_medium=feeds&utm_content=megusta&utm_campaign='.urlencode($campaignBrandName)."\", \"".$campaignBrandName."\", \"".$campaignDescription."\", \"".$campaignBrandLogo."\",{$startDate},{$expireDate});";

                    echo "<div class='campaignTop".($isActive ? "" : "Inactive")."'></div>";
                    echo "<li".($isActive ? "" : " class='inactive'").">";
                    echo $isActive ? "<a href='".$campaignURL."&utm_source=facebook-app&utm_medium=vidriera&utm_content=imagen&utm_campaign=".urlencode($campaignBrandName)."' target='_blank'>" : "";
                    echo "    <img src='".$campaignBanner."' class='banner'>";
                    echo $isActive ? "</a>" : "";
                    echo "    <span>Esta campa&ntilde;a ".($isActive ? 'finaliza' : 'empieza')." en:</span>";
                    echo "    <strong id='timer".$i."'>".$campaignTime."</strong>";
                    echo "    <h2>";
                    echo $isActive ? "<a href='".$campaignURL."' target='_blank'>".$campaignBrandName . "</a>" : $campaignBrandName;
                    echo "    </h2>";
                    echo "    <p>".$campaignDescription."</p>";
                    echo "    <div class='buttons'>";
                    echo $isActive ? "<a href='".$campaignURL."&utm_source=facebook-app&utm_medium=vidriera&utm_content=boton&utm_campaign=".urlencode($campaignBrandName)."' target='_blank' class='red'>Ver Campa&ntilde;a</a>" : "&nbsp;";
                    echo $isActive ? "<a class='blue' clicktoshowdialog='gift_store_".$campaignID."' onclick='waitForUid(".$campaignID."); toggleGiftSelected(\"gift_".$campaignID."_\"+firstGift_".$campaignID.");'>Enviar Regalo</a>" : "&nbsp;";
                    echo $isActive ? "<a class='like' title='Me Gusta' id='likeIcon_".$campaignID."' onclick='".$likeAction."'></a>" : "&nbsp;";                    
                    echo "    </div>";
                    echo $isActive ? "<a href='".$campaignURL."' target='_blank'>" : "";
                    echo "    <img src='".$campaignBrandLogo."' class='logo'>";
                    echo $isActive ? "</a>" : "";                    
                    echo "</li>";
                    echo "<div class='campaignBottom".($isActive ? "" : "Inactive")."'>";
                    echo $isActive ? "" : "<a class='calendar' title='Agregar a mi calendario' id='calendarIcon_".$campaignID."' onclick='".$calendarAction."'></a>";
                    echo "</div>";
                }

                $currentTimer = array();
                $currentTimer['type'] = $isActive ? "active" : "inactive";
                $currentTimer['target'] = 'timer'.$i;
                $currentTimer['expire'] = $expireDate;

                array_push($timers, $currentTimer);
            }
            if($type=='inactive') {
                echo "</ul>";
                echo '<script type="text/javascript">';
                echo '    var timers = '.json_encode($timers).';';
                echo '</script>';
            }
        }
    }

    function selectActiveUsers() {
      global $db;
        $excludeIDs = '';
        $result = $db->query("SELECT uid FROM invites WHERE status='confirmed' OR status='installed';");
        while($row = mysql_fetch_array($result)) {
            $excludeIDs .= $row['uid'].', ';
        }

        return(substr($excludeIDs, 0, -2));
    }

    function includeBanner($position, $category) {
        $json = new jsonGetterAndDecoder();
        $decodedjson = $json->get_and_decode(JSON_URL);
        $banners = isset($decodedjson->banners) ? $decodedjson->banners : null;

        if($banners) {
            $url = null;
            $href = null;
            $title = null;

            foreach($banners as $banner) {
                if($banner->category == $category) {
                    if($position == 'top') {
                        $url = $banner->banner_top_url;
                        $href = $banner->banner_top_href;
                        $title = $banner->banner_top_title;
                    } else if($position == 'bottom') {
                        $url = $banner->banner_bottom_url;
                        $href = $banner->banner_bottom_href;
                        $title = $banner->banner_bottom_title;
                    }
                    
                    if($url!='' && $href!='') {
                        echo '<div class="banner '.($position == 'top' ? "bannerTop" : "bannerBottom").'">';
                        echo '    <a href="'.$href.'" title="'.$title.'"><img src="'.$url.'" title="'.$title.'" alt="'.$title.'" /></a>';
                        echo '</div>';
                    }
                }
            }
        }
    }

    function decrypter($clave) {   
   		try {
	        $Enc = new textEncrypter();
            $NClave = $Enc->decode($clave);
            return $NClave;
        } catch(exception $e) {
        	throw $e;
        }
	}
?>