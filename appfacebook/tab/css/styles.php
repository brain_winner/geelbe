<style type="text/css">
/* COMMONLY USED */
.floatLeft {float:left !important;}
.floatRight {float:right !important;}
.clearBoth {clear:both !important;}
.noBorder {border:0 !important;}

/* BUTTONS */
.button {font-size:11px; cursor:pointer; display:block;}
.button span {margin:0 -5px 0 0; padding:8px 15px 7px; display:block;}
.button:hover {background-position:left bottom;}
.button:hover span {background-position:right bottom;}

.btGris {background:url(<?php echo TAB_CALLBACK_URL; ?>images/bt_gris_l.png) no-repeat left top; color:#535353; text-decoration:none;}
.btGris span {background:url(<?php echo TAB_CALLBACK_URL; ?>images/bt_gris_r.png) no-repeat right top;}

.fbButton {border-style:solid; border-color:#d9dfea #0e1f5b #0e1f5b #d9dfea; border-width:1px; padding:2px 15px 3px; background:#3b5998; color:#fff; font-family:"Lucida Grande",tahoma,verdana,arial,sans-serif; font-size:11px; text-align:center;}
.fbButton:hover {text-decoration:none !important;}

/* GENERAL LAYOUT & STYLING */
#wrap {width:520px; float:left;}
#header {width:520px; height:52px; float:left; background:url(<?php echo TAB_CALLBACK_URL; ?>images/new_header.jpg?v=1) no-repeat 0 0;}
#container { width:774px; padding-bottom:52px; background:url(<?php echo TAB_CALLBACK_URL; ?>images/wrap_bg_mod.png) repeat;}

/* MENU */
#menu {width:750px; height:40px; float:left; padding:0 0 0 10px; background:#ccc; border-bottom:1px solid #fff;}
#menu li {height:30px; line-height:30px; float:left; display:inline; margin:0 0 0 10px; padding:5px 10px; background:#fff; position:relative; top:-10px; border:1px solid #ddd; font-size:14px; font-weight:bold; color:#3B5998; text-align:center; cursor:pointer;}
#menu li.selected {background:#3B5998; color:#fff;}


/* CAMPAIGNS LIST */
.content {width:520px; float:left; background:#efefef;}
.content .banner {width:520px; float:left; text-align:center;}
.content .bannerTop {padding:15px 0 5px;}
.content .bannerBottom {padding:5px 0 15px;}
.content .campaigns {width:492px; height:475px; float:left; overflow:auto; padding:10px 20px 10px 8px;}
.content .campaigns h1 {color:#b40e10; font-size:18px;}

.content .campaigns .campaignTop, .content .campaigns .campaignTopInactive {width:490px; height:5px; float:left; vertical-align:bottom; margin:10px 0 0; background:url(<?php echo TAB_CALLBACK_URL; ?>images/sprite_wide_img.png) no-repeat;}
.content .campaigns .campaignBottom, .content .campaigns .campaignBottomInactive {width:490px; height:5px; float:left; vertical-align:top; background:url(<?php echo TAB_CALLBACK_URL; ?>images/sprite_wide_img.png) no-repeat;}
.content .campaigns .campaignTop {background-position:0 -105px;}
.content .campaigns .campaignBottom {background-position:0 -110px;}
.content .campaigns .campaignTopInactive {background-position:0 -115px;}
.content .campaigns .campaignBottomInactive {background-position:0 -120px;}
.content .campaigns .campaignBottomInactive .calendar {float:left; margin-bottom:0px;margin-top: -35px;margin-left: 221px;width:130px; height:40px; padding:0; border:0; background:url(<?php echo TAB_CALLBACK_URL; ?>images/fb_calendar.png) 0 0;}

.content .campaigns li {width:488px; float:left; padding:5px 0; background:#fff url(<?php echo TAB_CALLBACK_URL; ?>images/li_bg.gif) no-repeat bottom right; border-left:1px solid #ccc; border-right:1px solid #ccc;}
.content .campaigns li .banner {width:200px; height:122px; float:left; margin:0 10px 0; padding:0;}
.content .campaigns li span {width:258px; float:left; font-size:12px;}
.content .campaigns li strong {width:258px; float:left; color:#df3131; font-size:14px;}
.content .campaigns li h2 {width:258px; float:left; font-size:15px; padding:10px 0 0;}
.content .campaigns li h2 a {color:#333;}
.content .campaigns li p {width:258px; height:21px; float:left; margin:0; padding:3px 0;}
.content .campaigns li .logo {width:100px; height:51px; margin:0 0 -51px -100px; float:left; position:relative; z-index:99; top:-65px; right:0;}

.content .campaigns li .buttons {width:258px; float:left; text-align:right;}
.content .campaigns li .buttons a {height:32px; text-decoration:none; float:left; color:#fff; text-align:center; line-height:32px; font-size:12px;}

.content .campaigns li .buttons .red {width:105px; padding:0 0 0 15px; background:url(<?php echo TAB_CALLBACK_URL; ?>images/sprite_bts.png) no-repeat 0 0;}
.content .campaigns li .buttons .red:hover {background-position:0 -32px; text-decoration:none;}

.content .campaigns li .buttons .blue {width:101px; margin:0 8px; background:url(<?php echo TAB_CALLBACK_URL; ?>images/sprite_bts.png) no-repeat -124px 0;}
.content .campaigns li .buttons .blue:hover {background-position:-124px -32px; text-decoration:none;}

.content .campaigns li .buttons .like {width:16px; height:14px; margin:9px 2px 4px 2px; padding:0; border:0; background:url(<?php echo TAB_CALLBACK_URL; ?>images/sprite_likes.png) 0 0;}
.content .campaigns li .buttons .like:hover {background-position:0 -14px;}
.content .campaigns li .buttons .like.selected {background-position:0 -28px;}


.content .campaigns li.inactive {background:#c0c0c0 url(<?php echo TAB_CALLBACK_URL; ?>images/li_bg_inactive.gif) no-repeat bottom right;}
.content .campaigns li.inactive strong {color:#333;}

/* FOOTER */
#footer {width:520px; margin:0 auto; background:url(<?php echo TAB_CALLBACK_URL; ?>images/footer_mod_bg.gif) repeat-x top;}
#footer h1 {width:492px; float:left; font-size:18px; padding:15px 20px 8px 8px; color:#B40E10; border-bottom:1px solid #eee; background:#eee;}

#footer .friends {width:100%; float:left;}
#footer .friends .mask {width:452px; float:left; height:70px; overflow:hidden; margin:0 0 0 8px;}
#footer .friends .next, #footer .friends .prev {width:26px; height:70px; cursor:pointer;}
#footer .friends .next div, #footer .friends .prev div {width:26px; height:42px; float:left; margin:14px 0 0; background:url(<?php echo TAB_CALLBACK_URL; ?>images/sprite_carrousel.png) no-repeat;}
#footer .friends .prev div {background-position:0 0;}
#footer .friends .prev:hover div {background-position:-52px 0;}
#footer .friends .prev:active div {background-position:-104px 0;}
#footer .friends #friendsCarrouselBtnPrev.disabled div {background-position:-156px 0;}
#footer .friends .next div {background-position:-26px 0;}
#footer .friends .next:hover div {background-position:-78px 0;}
#footer .friends .next:active div {background-position:-130px 0;}
#footer .friends #friendsCarrouselBtnNext.disabled div {background-position:-182px 0;}
#footer .friends a.disabled {cursor:default !important;}

#footer .friends ul {height:50px; float:left; padding:10px 0; position:relative; left:-22px;}
#footer .friends ul li {width:50px; float:left; display:inline; padding:0 0 0 12px; margin:0 0 0 10px; background:url(<?php echo TAB_CALLBACK_URL; ?>images/footer_li_bg.gif) no-repeat left;}
#footer .friends ul li.first {background:none !important;}
#footer .friends ul li .frame {width:50px; height:45px; margin:0 0 -50px -50px; position:relative; left:50px; top:-54px; background:url(<?php echo TAB_CALLBACK_URL; ?>images/footer_frame.png);}

#friendsGrabber {text-align:center; vertical-align:middle; height:82px; }
.friendsBg {background:url(<?php echo TAB_CALLBACK_URL; ?>images/bottom.jpg) no-repeat; }
#getUserFriendsButton {margin-left:189px; margin-top:25px;}
/* OTHERS */
h1 {width:100%; float:left; font-size:20px;}
h1 img {margin:0 6px -6px 0;}
h1 .iconHelp {width:24px; height:24px; float:left; position:relative; left:205px; margin:0 0 -4px -24px; cursor:pointer; background:url(<?php echo TAB_CALLBACK_URL; ?>images/sprite_icons.png) 0 0;}
h1 .fbButton {float:right; margin:7px 0 0;}

.giftsContainer, .yourGift, .messageAndButtons {width:100%; float:left;}
.giftBox {width:49px; float:left; padding:5px 10px 5px 5px; cursor:pointer; border-right:1px solid #ccc; margin:10px 0 0 5px;}
.giftBox .frame {width:50px; height:50px; position:relative; background:url(<?php echo TAB_CALLBACK_URL; ?>images/sprite_gifts_frame_3b5998.png) no-repeat top;}
.giftBox.selected .frame {background-position:bottom !important;}
.giftBox img {width:50px; float:left;}

.messageAndButtons textarea {width:415px; float:left; overflow:auto;}
.messageAndButtons .inputbutton {position:relative; top:43px; left:265px; display:none; z-index:500 !important;}
.messageAndButtons .inputtext {width:415px; float:left;}

/* HELP POPUP */
.popHelp {width:250px; height:80px; float:left; padding:5px; position:relative; margin:0 0 -100px -250px; z-index:100; left:475px; top:15px; background:#ddd url(<?php echo TAB_CALLBACK_URL; ?>images/icon_close.png) no-repeat 245px 3px; border:5px solid #ccc; cursor:pointer;}
.popHelp p {margin:10px 0 0;}
.popHelp .popTip {width:12px; height:8px; margin:0 0 -8px -12px; position:absolute; top:-5px; left:-5px; background:url(<?php echo TAB_CALLBACK_URL; ?>images/sprite_icons.png) 0 -60px;}
.popHelp .iconInfo {width:24px; height:24px; float:left; margin:0 2px -10px; background:url(<?php echo TAB_CALLBACK_URL; ?>images/sprite_icons.png) 0 -24px;}

/* FACEBOOK CSS STEP-ON */
.email_section, .inputaux {display:none !important;}
.num_cols_4 {width:580px !important; margin:0 !important;}
.num_cols_4 li {margin:1px !important;}
#fb_multi_friend_selector_wrapper {width:100%;}
</style>