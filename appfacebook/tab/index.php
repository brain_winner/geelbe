<?php
  require_once("../includes/config.php");
  require_once("includes/functions.php");

  include_once("css/styles.php");
  include_once("js/scripts.php");
  
?>
<div id="container">
  <div id="wrap" onclick="startApp();">
    <div id="debug" style="font-size:30px;"></div>
    <div id="header">
      <?php include_once("includes/menu.php"); ?>
    </div>
    <div class="content" style="opacity:1; display:block;">
      <?php
        includeBanner('top', 'home');
        include_once("includes/popups/help.php");
      ?>
      <?php
        makeCampaignList();
        includeBanner('bottom', 'home');
      ?>
    </div>
    <?php include ("includes/footer.php"); ?>
    <div class="clearBoth"></div>
  </div>
  <br clear="all" />
  <?php
   // echo '<input type="button" onclick="getToken()" value="Get Token"/>';
  ?> 
</div>
<script type="text/javascript">
  var currentGift = '';
  var timersWorking = false;
  var firstClick = false;
  var installed = undefined;
  var currentUserID = undefined;
  var currentCarrouselPos = -22;
</script>
