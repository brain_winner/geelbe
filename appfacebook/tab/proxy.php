<?php
  require_once ("../includes/config.php");
  require_once ("includes/functions.php");
  require_once ("../includes/facebook/facebook.php");
  require_once ("../includes/classes/db.php");
  require_once ("../includes/classes/json_getter_and_decoder.php");
  require_once ("../includes/classes/class.xhttp.php");
	
  
  // TODO: Buscarle un lugar un poco mas feliz a esta funcion
  function _convert($content) { 
    if(!mb_check_encoding($content, 'UTF-8') 
        OR !($content === mb_convert_encoding(mb_convert_encoding($content, 'UTF-32', 'UTF-8' ), 'UTF-8', 'UTF-32'))) { 

        $content = mb_convert_encoding($content, 'UTF-8'); 

        if (mb_check_encoding($content, 'UTF-8')) { 
            // log('Converted to UTF-8'); 
        } else { 
            // log('Could not converted to UTF-8'); 
        } 
    } 
    return $content; 
  } 

  $json = new jsonGetterAndDecoder();
  $decodedjson = $json->get_and_decode(JSON_URL);
  $campaigns = $decodedjson->campaigns;
  $regional = $decodedjson->regional;
  $gmt = $regional[0]->gmt;
  $country = $regional[0]->country;

  $db = new db();
  $dbConnection = $db->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
  
  $facebook = new Facebook(API_KEY, API_SECRET);

  if(isset($_REQUEST['fb_sig_user'])) {
    $user_id = $_REQUEST['fb_sig_user'];		
    $facebook->set_user($user_id, $_REQUEST['fb_sig_session_key']);
  } elseif (isset($_REQUEST["fb_sig_api_key"]) && isset($_REQUEST[$_REQUEST["fb_sig_api_key"] . '_user'])) {
    $user_id =  $_REQUEST[$_REQUEST["fb_sig_api_key"] . '_user'];
  } else if(isset($_REQUEST['uid'])) {
      $user_id = $_REQUEST['uid'];
  } else {
    die();
  }

  // APP FRIENDS
  if(isset($_REQUEST['get_friends'])) {
    $query = "SELECT uid, name, pic_square, profile_url FROM user WHERE is_app_user = 1 AND uid IN (SELECT uid2 FROM friend WHERE uid1 = $user_id)";
    $friends = $facebook->api_client->fql_query($query);
    $friendsCount = count($friends);
    $ulWidth = $friendsCount*72;

    echo "<a class='prev floatLeft disabled' id='friendsCarrouselBtnPrev' onclick='moveCarrousel(\"prev\", ".$ulWidth.");'><div></div></a>";
    echo "<div class='mask'>";
    echo "<ul id='friendsCarrousel' style='width:".$ulWidth."px;'>";
    if(is_array($friends)) {
      for($i=0; $i<count($friends); $i++) {
        $uid = $friends[$i]['uid'];
        $name = $friends[$i]['name'];
        $pic = $friends[$i]['pic_square'] ? $friends[$i]['pic_square'] : TAB_CALLBACK_URL.'images/pic_square_default.gif';
        $url = $friends[$i]['profile_url'];

        echo $i==0 ? "<li class='first'>" : "<li>";
        echo "    <a href='".$url."' title='".$name."' target='_blank'>";
        echo "        <img src='".$pic."'>";
        echo "        <div class='frame'></div>";
        echo "    </a>";
        echo "</li>";
      }
    }
    echo "</ul>";
    echo "</div>";
    echo "<a class='next floatRight ".($friendsCount>=7 ? "" : "disabled")."' id='friendsCarrouselBtnNext' onclick='moveCarrousel(\"next\", ".$ulWidth.");'><div></div></a>";
  }

  // GIFTS
  if(isset($_REQUEST['save_gift_sent'])) {
    $postID = $_REQUEST['post_id'] ? $_REQUEST['post_id'] : 'no post';
    $db->query("INSERT INTO gifts (post_id, campaign_id, campaign_name, product_id, product_name, from_uid, to_uid, country, date) VALUES ('".mysql_real_escape_string($postID)."', '".mysql_real_escape_string($_REQUEST['campaign_id'])."', '".mysql_real_escape_string(urldecode($_REQUEST['campaign_name']))."', '".mysql_real_escape_string($_REQUEST['product_id'])."', '".mysql_real_escape_string(urldecode($_REQUEST['gift_name']))."', '".mysql_real_escape_string($user_id)."', '".mysql_real_escape_string($_REQUEST['to_uid'])."', '".mysql_real_escape_string($country)."', NOW());");
  }

  // LIKES
  if(isset($_REQUEST['save_campaign_like'])) {
    $db->query("INSERT INTO likes (campaign_id, campaign_name, uid, country) VALUES ('".mysql_real_escape_string($_REQUEST['campaign'])."', '".mysql_real_escape_string(urldecode($_REQUEST['title']))."', '".mysql_real_escape_string($user_id)."', '".mysql_real_escape_string($country)."');");
  }

  if(isset($_REQUEST['delete_campaign_like'])) {
    $db->query("DELETE FROM likes WHERE campaign_id='".mysql_real_escape_string($_REQUEST['campaign'])."' AND uid='".mysql_real_escape_string($user_id)."';");
  }

  if(isset($_REQUEST['get_user_likes'])) {
    $campaignIDs = '';
    $result = $db->query("SELECT campaign_id FROM likes WHERE uid='".mysql_real_escape_string($user_id)."' AND country='".mysql_real_escape_string($country)."';");
    while($row = mysql_fetch_array($result)) {
      $campaignIDs .= $row['campaign_id'].",";
    }
    echo substr($campaignIDs, 0, -1);
  }


  // EVENTS

	if(isset($_REQUEST['get_token'])) {
		$sessionKey = trim($_REQUEST["fb_sig_session_key"]);
		echo getAccessToken($sessionKey, $appID, API_SECRET);
	}

	if(isset($_REQUEST['add_to_calendar'])) {

		$result = $db->query("SELECT *
								FROM users_calendar as uc
								LEFT JOIN users_info as ui on uc.uid = ui.uid and ui.country like ui.country
								WHERE uc.uid='".mysql_real_escape_string($user_id)."' AND uc.country='".mysql_real_escape_string($country)."'
								LIMIT 1;") or die(mysql_error() .  __LINE__);
    	//creamos el usuario si no est�
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result);
			$accessToken = $row["access_token"];
			
			// just in case actualizo siempre el access token
			if (isset($_REQUEST["access_token"])) {
				$accessToken = $_REQUEST["access_token"];
			} else {
				$sessionKey = trim($_REQUEST["fb_sig_session_key"]);
				$accessToken = getAccessToken($sessionKey, $appID, API_SECRET);
			}
			$db->query("update users_info SET access_token = '".mysql_real_escape_string($accessToken)."' WHERE uid = ".mysql_real_escape_string($user_id). " and country like '".mysql_real_escape_string($country)."';") or die(mysql_error() .  __LINE__);
		} else {
			$sessionKey = trim($_REQUEST["fb_sig_session_key"]);
	      	$db->query("INSERT INTO users_calendar (uid, country, created, updated, session_key) VALUES (".mysql_real_escape_string($user_id). ", '".mysql_real_escape_string($country)."', NOW(),NOW(),'".mysql_real_escape_string($sessionKey)."');") or die(mysql_error() .  __LINE__);
			if (!isset($_REQUEST["access_token"])) {
				$accessToken = getAccessToken($sessionKey, $appID, API_SECRET);
			} else {
				$accessToken = $_REQUEST["access_token"];
			}
			$db->query("INSERT INTO users_info (uid, access_token, country) VALUES (".mysql_real_escape_string($user_id). ", '".mysql_real_escape_string($accessToken)."', '".mysql_real_escape_string($country)."');") or die(mysql_error() .  __LINE__);
	    }

	    $sessionKey = trim($_REQUEST["fb_sig_session_key"]);
	    //actualizamos la session del usuario siempre.
	
	    $db->query("UPDATE users_calendar SET session_key = '".mysql_real_escape_string($sessionKey)."', updated = NOW()  WHERE uid = ".mysql_real_escape_string($user_id). "  and country like '".mysql_real_escape_string($country)."' LIMIT 1;") or die(mysql_error() .  __LINE__);
	
		//buscamos el evento en la DB
	    $result = $db->query("SELECT * FROM campaign_events WHERE campaign_id like '".mysql_real_escape_string($_REQUEST['campaign'])."' and country like '".mysql_real_escape_string($country)."';") or die(mysql_error() .  __LINE__);
	
	    $eventCreated = false;
	    $idSetted = false;
	    if(mysql_num_rows($result) > 0) {
				//levantamos el id del evento en FB para poder hacer RSVP
				$campaignEvent = mysql_fetch_array($result);
				$fbEventId = $campaignEvent["fb_event_id"];
				
				if($fbEventId != null && trim($fbEventId) != '') {
					$idSetted = true;
				}
				$eventCreated = true;
				$eventId = $campaignEvent["id"];
		}
			
	if(!$idSetted) {
		if(!$eventCreated) {
			  //CREAMOS EL EVENTO en la db
		      $startDate = date("Y-m-d H:i:s", $_REQUEST["campaign_start_date"]);
		      $endDate = date("Y-m-d H:i:s", $_REQUEST["campaign_end_date"]);
		      $db->query("INSERT INTO campaign_events
		                          (country, created, updated, campaign_id, 
															 campaign_start_date, campaign_end_date,campaign_name,
															 campaign_url, campaign_picture)
		                  VALUES  ('".mysql_real_escape_string($country)."', NOW(),NOW(),
															 '".mysql_real_escape_string($_REQUEST["campaign"])."','".mysql_real_escape_string($startDate)."',
															 '".mysql_real_escape_string($endDate)."','".mysql_real_escape_string($_REQUEST["title"])."','".mysql_real_escape_string($_REQUEST["url"])."',
															 '".mysql_real_escape_string($_REQUEST["img"])."');") or die(mysql_error() .  __LINE__);
															 
		      $eventId = $db->insert_id();    
		} 

			$data = array();
			$data['post'] = array(
			 'access_token' => COUNTRY_ACCESS_TOKEN,
			 'name' => _convert("Campa�a de venta de {$_REQUEST["title"]} en Geelbe"),
			 'description' => "http://www.geelbe.com/". strtolower(COUNTRY) . "/",
			 'privacy_type' => 'OPEN', # OPEN, CLOSED, SECRET
			 'start_time' => $_REQUEST["campaign_start_date"] + $timeDiffence*3600,
			 'end_time' =>  $_REQUEST["campaign_end_date"] + $timeDiffence*3600,
			 );
			$data['files'] = array(
			 'picture' => $_REQUEST["img"],
			);

			$response = xhttp::fetch(FANPAGE_PATH, $data);

			if($response['successful']) {
			 $var = json_decode($response['body'], true);			
			 $fbEventId = $var[id];
			} else {
			 $error = "Unable to create event: {$response[body]}";
			 echo "ERROR: " . $error;
			 die;
			}
			
			//guardamos el event id de facebook en la tabla.
			$db->query("  UPDATE campaign_events
										SET fb_event_id = '".mysql_real_escape_string($fbEventId)."'
										WHERE id = ".mysql_real_escape_string($eventId)." 
										LIMIT 1;") or die(mysql_error() .  __LINE__);
    }
		
		//buscamos si hay suscripci�n realizada ya
		$result = $db->query("SELECT COUNT(*) as total
													FROM events_subscription
													WHERE campaign_id = '".mysql_real_escape_string($_REQUEST['campaign'])."' and
																uid = ".mysql_real_escape_string($user_id)." and
																country='".mysql_real_escape_string($country)."';") or die(mysql_error() .  __LINE__);
    $row = mysql_fetch_array($result);

    if ($row["total"] == '0') {
			//agregamos la suscripci�n a la db
      $db->query("INSERT INTO events_subscription
                          (uid, country, created, updated, campaign_id)
                  VALUES  (".mysql_real_escape_string($user_id). ", '".mysql_real_escape_string($country)."', NOW(),NOW(),
													 '".mysql_real_escape_string($_REQUEST["campaign"])."');") or die(mysql_error() .  __LINE__);
      try {
				//hacemos RSVP del evento en fb

				$data = array();
				$data['post'] = array(
					'access_token' => $accessToken
				 );
				
				$url = "https://graph.facebook.com/{$fbEventId}/attending";

				$response = xhttp::fetch($url, $data);

				if($response['successful']) {
				 $var = json_decode($response['body'], true);				 
				} else {
				 echo "ERROR: " . $response["body"];
				}

      } catch(Exception $e) {
        echo $e->getMessage();
      }

      echo "OK";
    } else {
      echo "ALREADY";
    }
  }


  // INSTALL
  if(isset($_REQUEST['install_user']) && isset($_REQUEST['uid'])) {
    $isInstalled = false;
    $result = $db->query("SELECT * FROM invites WHERE uid = ".mysql_real_escape_string($_REQUEST['uid']).";");
    while($row = mysql_fetch_array($result)) {
        var_dump($row);
      if($row['uid'] == $_REQUEST['uid']) {
        $isInstalled = true;
        break;
      }
    }
    
    if(!$isInstalled) {
      $db->query("INSERT INTO invites (id, inviter, uid, status, country, promo_id, date_created) VALUES ('".uniqid()."', 'FANPAGE', ".mysql_real_escape_string($_REQUEST['uid']).", 'installed', '".mysql_real_escape_string($country)."', 0, NOW());");
    }
  }

  // TIME
  if(isset($_REQUEST['get_current_time'])) {
    echo (time()+($gmt*3600));
  }
?>