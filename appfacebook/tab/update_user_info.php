<?php
  require_once ("../includes/config.php");
  require_once ("includes/functions.php");
  require_once ("../includes/facebook/facebook.php");
  require_once ("../includes/classes/db.php");
  require_once ("../includes/classes/json_getter_and_decoder.php");
  require_once ("../includes/classes/class.xhttp.php");
  
  $accessToken = $_REQUEST["access_token"];
  $geelbe_user_id = $_REQUEST["user_id"];
  $hash = $_REQUEST["hash"];
  $country = strtolower(COUNTRY);
  
  $db = new db();
  $dbConnection = $db->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
  
  $facebook = new Facebook(API_KEY, API_SECRET);
  
  if(isset($_REQUEST['fb_sig_user'])) {
    $user_id = $_REQUEST['fb_sig_user'];		
    $facebook->set_user($user_id, $_REQUEST['fb_sig_session_key']);
  } elseif (isset($_REQUEST["fb_sig_api_key"]) && isset($_REQUEST[$_REQUEST["fb_sig_api_key"] . '_user'])) {
    $user_id =  $_REQUEST[$_REQUEST["fb_sig_api_key"] . '_user'];
  } else if(isset($_REQUEST['uid'])) {
      $user_id = $_REQUEST['uid'];
  } else {
  	echo "Unable to obtain user id.";
    die();
  }
  
  $result = $db->query("SELECT *
	  FROM users_info as ui
	  WHERE ui.uid='".mysql_real_escape_string($user_id)."' AND ui.country='".mysql_real_escape_string($country)."'
	  LIMIT 1;") or die(mysql_error() .  __LINE__);
      
  if (mysql_num_rows($result) > 0) {
	  $row = mysql_fetch_array($result);
	  
	  if($accessToken != $row["access_token"]) {
	    $db->query("update users_info set access_token = '".mysql_real_escape_string($accessToken)."' where uid = '".mysql_real_escape_string($user_id)."' and country = '".mysql_real_escape_string($country)."'") or die(mysql_error().__LINE__);	  	
	  }
  } else {
	  $db->query("INSERT INTO users_info (uid, access_token, country) VALUES (".mysql_real_escape_string($user_id). ", '".mysql_real_escape_string($accessToken)."', '".mysql_real_escape_string($country)."');") or die(mysql_error().__LINE__);
  }
  
  $ch = curl_init("https://graph.facebook.com/me?access_token=".$accessToken);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  $response = curl_exec($ch);
  curl_close ($ch);
  $user = json_decode($response);
  $user->uid = $user->id;

  $ch = curl_init(REGISTRY_UPDATER_SERVICE);
  curl_setopt ($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt ($ch, CURLOPT_POSTFIELDS, "user_id=".$geelbe_user_id."&hash=".$hash."&user_data=" . json_encode($user));
  $response = curl_exec ($ch);
  curl_close ($ch);
  
  if($_REQUEST["redirection"] == "micuenta") {
  	header("Location: /".$country."/front/micuenta/mi-perfil.php");
  } else if($_REQUEST["redirection"] == "connect") {
  	header("Location: /".$country."/front/micuenta/post-connect.php");
  }
  
?>

