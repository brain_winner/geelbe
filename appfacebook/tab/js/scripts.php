<?php
    require_once("../includes/config.php");
?>
<script type="text/javascript">
<!--
    function showPopHelp(id) {
        Animation(document.getElementById("popHelp_"+id)).to("height", "80px").from("0px").to("width", "250px").from("0px").to("opacity", 1).from(0).blind().show().go();
        setTimeout(function() {
            document.getElementById("popHelp_"+id).setStyle('overflow', 'visible');
        }, 500);
    }

    function hidePopHelp(id) {
        document.getElementById("popHelp_"+id).setStyle('overflow', 'hidden');
        Animation(document.getElementById("popHelp_"+id)).to('height', '0px').to('width', '0px').to('opacity', 0).blind().hide().go();
    }

    function like(handler, campaign_id, href, title, description, img) {
        var icon = document.getElementById(handler);
        icon.toggleClassName("selected");

        if(icon.hasClassName("selected")) {
            var message = 'Me gusta la campa\u00f1a ' + title + ' que acabo de ver en Geelbe!';
            var attachment = {
            'name': title + " -  " + description + " en Geelbe <?php echo isset($i18n['country']) ? $i18n['country'] : ''; ?>.",
            'href': href,
            'description': 'Geelbe, el primer club privado de compras por Internet. Convi&eacute;rtete en socio de Geelbe y tendr&aacute;s acceso a ofertas exclusivas de las mejores marcas a precios muy especiales.',
            'media':[{'type':'image','src':img, 'href': href}]};
            var actionLinks = [{'text':'Ver Campa&ntilde;a', 'href': href}];

            var post = Facebook.streamPublish(message, attachment, actionLinks);

            var ajax = new Ajax();
            ajax.responseType = Ajax.RAW;
						ajax.requireLogin = true;
            ajax.post('<?php echo TAB_CALLBACK_URL ?>proxy.php?save_campaign_like=true&campaign='+campaign_id+'&title='+escape(title)+'&uid='+currentUserID);
        } else if(!icon.hasClassName("selected")) {
            var ajax = new Ajax();
            ajax.responseType = Ajax.RAW;
						ajax.requireLogin = true;
            ajax.post('<?php echo TAB_CALLBACK_URL ?>proxy.php?delete_campaign_like=true&campaign='+campaign_id+'&uid='+currentUserID);
        }
    }
    
    function addCalendarEvent(handler, campaign_id, href, title, description, img,startDate,endDate) {
      Facebook.showPermissionDialog('user_birthday,email,publish_stream,offline_access,rsvp_event', function (data){					
        if(data == null) {
          var dialog = new Dialog();
          dialog.showMessage('Acceso a datos', "Para poder crear un evento con la informaci�n de la campana necesitamos que autorice el pedido de permisos especiales. Intente nuevamente por favor.");
          return false;
        }
        
        var ajax = new Ajax();
        ajax.responseType = Ajax.RAW;
        ajax.ondone = function(data) {
//            var dialog = new Dialog();
//            dialog.showMessage('test', data);
//            return false;
          if(data == 'OK') {
            var dialog = new Dialog();
            dialog.showMessage('Evento agendado!', "El evento fue agregado a tu calendario. Cuando comience la campa\u00f1a de ventas, se publicar\u00e1 un mensaje en tu muro para avisarte.");
          } else {
            if (data == "ALREADY") {
              var dialog = new Dialog();
              dialog.showMessage('Evento ya agendado!', "El evento ya se encontraba agendado en tu calendario.");
            } else {
              var dialog = new Dialog();
              dialog.showMessage('test', data);
            }
          }
        };
        
        ajax.post('<?php echo TAB_CALLBACK_URL ?>proxy.php?add_to_calendar=true&campaign=' +
                  campaign_id+'&title='+escape(title)+'&uid='+currentUserID +
                  '&campaign_start_date=' + startDate + '&campaign_end_date=' + endDate +
                  '&url=' + escape(href) + "&img=" + img + "&description=" + description);
      });
    }

		function getToken() {
      Facebook.showPermissionDialog('publish_stream,offline_access,rsvp_event,user_events,create_event,manage_pages', function (data){
        if(data == null) {
          var dialog = new Dialog();
          dialog.showMessage('Acceso a datos', "Para poder crear un evento con la informaci�n de la campana necesitamos que autorice el pedido de permisos especiales. Intente nuevamente por favor.");
          return false;
        }

        var ajax = new Ajax();
        ajax.responseType = Ajax.RAW;
        ajax.ondone = function(data) {
            var dialog = new Dialog();
            dialog.showMessage('Token Request', data);
            return false;
        };

        ajax.post('<?php echo TAB_CALLBACK_URL ?>proxy.php?get_token=true');
      });
    }

    function countdown(target, expire)
    {
        var tempDays = expire/86400;
        var days = Math.floor(tempDays);
        var restDays = Math.abs(Math.floor(tempDays) - tempDays);

        var tempHours = (restDays*86400)/3600;
        var hours = Math.floor(tempHours);
        var restHours = Math.abs(Math.floor(tempHours) - tempHours);

        var tempMins = (restHours*3600)/60;
        var mins = Math.floor(tempMins);
        var restMins = Math.abs(Math.floor(tempMins) - tempMins);

        var tempSecs = restMins*60;
        var secs = Math.ceil(tempSecs);
        setInterval(function() {
            if(days == 0 && hours == 0 && mins == 0 && secs == 0) {
                document.getElementById(target).setTextValue("Finalizada");
                clearInterval(this);
                return;
            }

            secs--;

            if(secs==0 && mins!=0){mins--; secs=59}
            if(mins==0 && hours!=0){hours--; mins=59}
            if(hours==0 && days!=0){days--; hours=23}

            var finalHours = /^\d{1}$/.test(hours) ? "0"+hours : hours;
            var finalMins = /^\d{1}$/.test(mins) ? "0"+mins : mins;
            var finalSecs = /^\d{1}$/.test(secs) ? "0"+secs : secs;

            document.getElementById(target).setTextValue(days+" d\u00edas "+finalHours+":"+finalMins+":"+finalSecs);
        }, 1000);
    }

    function toggleGiftSelected(id) {
        if(currentGift) {document.getElementById(currentGift).removeClassName("selected");}
        document.getElementById(id).addClassName("selected");
        currentGift = id;
    }

    function setGiftValues(campaign_id, product_id, img, thumb, name, desc) {
        toggleGiftSelected('gift_'+campaign_id+'_'+product_id);
        document.getElementById('giftId_'+campaign_id).setValue(product_id);
        document.getElementById('giftImg_'+campaign_id).setValue(escape(img));
        document.getElementById('giftThumb_'+campaign_id).setValue(escape(thumb));
        document.getElementById('giftName_'+campaign_id).setValue(name);
        document.getElementById('giftDesc_'+campaign_id).setValue(desc);

        var checkToSend = setInterval(function() {
            var params = document.getElementById("gift_form_"+campaign_id).serialize();
            if(params.friend_sel!="" && params.friend_sel!=undefined) {
                document.getElementById('send_button_'+campaign_id).setStyle('display', 'block');
                clearInterval(checkToSend);
            }
        }, 300);
    }

    function waitForUid(campaign_id) {
      var checkToSend = setInterval(function() {
          var params = document.getElementById("gift_form_"+campaign_id).serialize();
          if(params.friend_sel!="" && params.friend_sel!=undefined) {
              document.getElementById('send_button_'+campaign_id).setStyle('display', 'block');
              clearInterval(checkToSend);
          }
      }, 300);
    }

    function sendGift(campaign_id, campaign_url, campaign_name) {

        var product_id = document.getElementById('giftId_'+campaign_id).getValue();
        var img =  document.getElementById('giftImg_'+campaign_id).getValue();
        var name =  document.getElementById('giftName_'+campaign_id).getValue();
        var desc =  document.getElementById('giftDesc_'+campaign_id).getValue();

        var appUrl= "<?php echo TAB_URL; ?>";
        var message = document.getElementById("message_"+campaign_id).getValue();
        var params = document.getElementById("gift_form_"+campaign_id).serialize();

        var attachment = {
        'name': "Nuevo regalo Geelbe",
        'href': campaign_url,
        'caption': "{*actor*} te envi� " + name + " a trav�s de la Fan Page de Geelbe <?php echo isset($i18n['country']) ? $i18n['country'] : ''; ?>.",
        'description': 'Tu tambi�n puedes enviar regalos a tus amigos ingresando a la Fan Page de Geelbe <?php echo isset($i18n['country']) ? $i18n['country'] : ''; ?>.',
        'media':[{'type':'image','src': unescape(img), 'href': campaign_url}]};
        var actionLinks = [{'text':'Enviar Regalos', 'href': appUrl}];

        Facebook.streamPublish(message, attachment, actionLinks, params.friend_sel, null, function(post_id) {
            if(post_id != 'null' && post_id != null) {
                var ajax = new Ajax();
                ajax.responseType = Ajax.RAW;
                ajax.post('<?php echo TAB_CALLBACK_URL ?>proxy.php?save_gift_sent=true&post_id='+post_id+'&campaign_id='+campaign_id+'&product_id='+product_id+'&to_uid='+params.friend_sel+'&uid='+currentUserID+'&gift_name='+escape(name)+'&campaign_name='+escape(campaign_name));
            }

            toggleGiftSelected('gift_'+campaign_id+'_'+product_id);
        });
    }

    function startApp() {
      var isUser = Facebook.getUser();

      if(isUser == null) {
//        Facebook.requireLogin(function() {
//          document.setLocation('<?php echo TAB_URL;?>');
//        });
      } else {
        currentUserID = isUser;
        if(installed == undefined) {
          var ajax = new Ajax();
          ajax.responseType = Ajax.RAW;
          ajax.post('<?php echo TAB_CALLBACK_URL ?>proxy.php?install_user=true&uid='+isUser);
          installed = isUser;
        }
      }

      if(!firstClick) {
        if(isUser != null) {
          getUserFriends();
          getUserLikes();
        }
        if(!timersWorking) {
          getTimersCountdown();
          timersWorking = true;
        }
        firstClick = true;
      }
    }

    function getUserLikes() {
        var ajax = new Ajax();
        ajax.responseType = Ajax.RAW;
        ajax.ondone = function(data) {
            var campaigns = data.split(",");

            for(var i=0; i<campaigns.length; i++) {
                document.getElementById('likeIcon_'+campaigns[i]) ? document.getElementById('likeIcon_'+campaigns[i]).addClassName('selected') : '';
            }
        }
        ajax.post('<?php echo TAB_CALLBACK_URL ?>proxy.php?get_user_likes=true');
    }

    function getUserFriends() {
        Facebook.requireLogin( function() {
            if(document.getElementById('getUserFriendsButton')) {
              document.getElementById('getUserFriendsButton').setStyle('display', 'none');
            }

            var ajax = new Ajax();
            ajax.responseType = Ajax.FBML;
            ajax.ondone = function(data) {
              document.getElementById('friendsGrabber').setInnerFBML(data);
              document.getElementById('friendsGrabber').removeClassName('friendsBg');
              document.getElementById('friendsGrabber').setStyle('background', '#eee');
            }
            ajax.post('<?php echo TAB_CALLBACK_URL ?>proxy.php?get_friends=true');
            });
    }

    function getTimersCountdown() {
        var ajax = new Ajax();
        ajax.responseType = Ajax.RAW;
        ajax.ondone = function(data) {
            for(var i=0; i<timers.length; i++) {
                if(timers[i].type == 'active') {
                    countdown(timers[i].target, Math.abs(timers[i].expire-parseFloat(data)));
                }
            }
        }
        ajax.post('<?php echo TAB_CALLBACK_URL ?>proxy.php?get_current_time=true');
    }

    function moveCarrousel(direction, maxWidth) {
        var target = document.getElementById('friendsCarrousel');
        var liSize = 72;

        if(direction == 'next') {
            if(currentCarrouselPos>((-maxWidth)+410)) {
                if(document.getElementById('friendsCarrouselBtnPrev').hasClassName('disabled')) {
                    document.getElementById('friendsCarrouselBtnPrev').removeClassName('disabled');
                }
                if(currentCarrouselPos == (((-maxWidth)+410)+liSize)) {
                    document.getElementById('friendsCarrouselBtnNext').addClassName('disabled');
                }
                currentCarrouselPos = currentCarrouselPos-liSize;
                Animation(target).to('left', currentCarrouselPos).duration(300).go();
            } else {
                document.getElementById('friendsCarrouselBtnNext').addClassName('disabled');
            }
        } else if(direction == 'prev') {
            if(currentCarrouselPos<-22) {
                if(document.getElementById('friendsCarrouselBtnNext').hasClassName('disabled')) {
                    document.getElementById('friendsCarrouselBtnNext').removeClassName('disabled');
                }
                if(currentCarrouselPos == (-22-liSize)) {
                    document.getElementById('friendsCarrouselBtnPrev').addClassName('disabled');
                }
                currentCarrouselPos = currentCarrouselPos+liSize;
                Animation(target).to('left', currentCarrouselPos).duration(300).go();

            } else {
                document.getElementById('friendsCarrouselBtnPrev').addClassName('disabled');
            }
        }
    }
-->
</script>