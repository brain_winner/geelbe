<?php

  require_once '../includes/config.php';
  require_once '../includes/facebook/facebook.php';

  $facebook = new Facebook(API_KEY, API_SECRET);
  $uid = $facebook->require_login();
  $facebook->require_frame();

  $query = "SELECT name, profile_url FROM user WHERE uid = ".$uid.";";
  $userData = $facebook->api_client->fql_query($query);
  
  $query = "SELECT uid FROM user WHERE is_app_user = 1 AND uid IN (SELECT uid2 FROM friend WHERE uid1 = ".$uid.");";
  $friends = $facebook->api_client->fql_query($query);

  if($friends) {
    $excludeIds = '';
    foreach($friends as $friend) {
      $excludeIds .= $friend['uid'].', ';
    }
    $excludeIds = substr($excludeIds, 0, -2);
  }
  $invitationID = uniqid();
  $invitationContent = "Aceptando esta invitaci�n le estas otorgando una chance extra a <a href='".$userData[0]['profile_url']."' target='_blank'>".$userData[0]['name']."</a> de ganar un cup�n de descuento! <fb:req-choice url='".APP_URL."promo/proxy.php?action=confirm&invitation_id=".$invitationID."' label='Aceptar'>";

  include_once 'css/styles.php';
  include_once 'js/scripts.php';
?>
<div id="wrap">
  <div id="header"></div>
  <div id="content">
    <h1 class="multi">
      Invit&aacute; a tus amigos y sum&aacute; chances de ganar!<br>
      <small>Record&aacute; que por cada amigo que invites, sum&aacute;s una chance extra de <strong>ganar un cup&oacute;n de descuento</strong> todas las semanas !!!</small>
    </h1>
    <div class="invite">
      <fb:request-form action="<?php echo APP_URL; ?>promo/proxy.php?action=invite&invitation_id=<?php echo $invitationID; ?>" method="POST" invite="true" type="Geelbe" content="<?php echo htmlentities(utf8_decode($invitationContent)); ?>">
        <fb:multi-friend-selector actiontext="Invit&aacute; a tus amigos a ganar premios con Geelbe" email_invite="false" rows="4" cols="3" exclude_ids="<?php echo $excludeIds; ?>" />
      </fb:request-form>
    </div>
  </div>
  <?php include_once 'includes/footer.php'; ?>
</div>