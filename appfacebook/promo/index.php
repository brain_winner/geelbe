<?php
require_once("../includes/config.php");
require_once("../includes/facebook/facebook.php");
require_once("../includes/classes/db.php");
include_once("css/styles.php");

$facebook = new Facebook(API_KEY, API_SECRET);

$db = new db();
$dbConnection = $db->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

$query = "SELECT *
             FROM promos
             WHERE is_active=1
             AND country='" . mysql_real_escape_string(COUNTRY) . "'
             LIMIT 1;";
$resultActive = $db->query($query);
$activePromo = mysql_fetch_array($resultActive);
$alreadyWon = array();

if (!isset($activePromo['id'])) {
  if (COUNTRY == 'AR') {
    $promo_id = 999999998;
  } else if (COUNTRY == 'MX') {
    $promo_id = 999999999;
  } else if (COUNTRY == 'CO') {
    $promo_id = 999999997;
  }
  $query = "SELECT *
               FROM promos
               WHERE id=" . mysql_real_escape_string($promo_id) . "
               AND country='" . mysql_real_escape_string(COUNTRY) . "'
               LIMIT 1;";
  $resultActive = $db->query($query);
  $activePromo = mysql_fetch_array($resultActive);
}

$promosToShowWinners = array();
$query = "SELECT *
           FROM promos
           WHERE show_winners=1
           AND country='" . mysql_real_escape_string(COUNTRY) . "'
           ORDER BY id ASC;";
$resultWinners = $db->query($query);
while ($row = mysql_fetch_array($resultWinners)) {
  if ($row['id'] != $activePromo['id']) {
    array_push($promosToShowWinners, $row);
  }
}
?>
<script type="text/javascript">
  function checkFan() {
<?php
if (isset($_REQUEST["fb_sig_is_fan"]) && $_REQUEST["fb_sig_is_fan"] == "1") {
  echo "return true";
} else {
  echo 'var dialog = new Dialog().showMessage("Atenci�n", "Para poder invitar amigos primero debes apretar Me Gusta en la parte superior de �sta p�gina");';
  echo "return false";
}
?>;
      }
</script>
<div id="container">
  <div id="wrap" >
    <div id="debug" style="font-size:30px;"></div>
    <div id="header"></div>
    <div id="content">
      <div id="centralBlock">
        <div id="headerText">
<?php echo utf8_encode($activePromo['title']); ?>
        </div>
        <div id="promoDetail" style="background:url(<?php echo APP_CALLBACK_URL . 'upload/' . $activePromo['image']; ?>) no-repeat center;"></div>
        <div id="likeText">
<?php echo utf8_encode($activePromo['description']); ?>
        </div>
        <div class="buttons">
          <a href="<?php echo APP_URL; ?>promo/invite.php" onclick="return checkFan();" id="inviteButton"></a>
        </div>
      </div>
      <div id="ganadores">
<?php
if (empty($promosToShowWinners)) {
  echo '<img src="' . TAB_CALLBACK_URL . 'images/ganadores.jpg">';
} else {
  echo '<h1>Ganadores de las promos anteriores</h1>';
  foreach ($promosToShowWinners as $promo) {
    $query = "SELECT * FROM promos_winners WHERE country = '" . mysql_real_escape_string(COUNTRY) . "' AND promo_id = '".mysql_real_escape_string($promo['id'])."' ORDER BY invites DESC;";
    $result = $db->query($query);
    
    if (mysql_num_rows($result) != 0) {
      $count = 1;

      if ($count == 1) {
        $dateArray = explode(" / ", $promo['duration']);
        echo '<h4>Promo del ' . substr($dateArray[0], 0, 5) . ' al ' . substr($dateArray[1], 0, 5) . '</h4>';
        echo '<ul>';
      }

      while ($winners = mysql_fetch_assoc($result)) {
        echo '<li>';
        echo '  <div><p>' . $count . '<sup>&deg;</sup></p><fb:profile-pic uid="' . $winners['uid'] . '" size="square" /></div>';
        echo '  <h5><fb:name uid="' . $winners['uid'] . '" useyou="false" /></h5>';
        echo '  <span style="display:none;">'.$winners['invites'].'</span>';
        echo '</li>';
        $count++;
      }
      echo '</ul>';
    }
  }
}
?>
      </div>
    </div>
    <div class="bases">
      <a href="<?php echo $i18n['url_bases_promo']; ?>" target="_blank">Bases y condiciones de la promoci&oacute;n</a>
    </div>
    <div class="clearBoth"></div>
  </div>
</div>