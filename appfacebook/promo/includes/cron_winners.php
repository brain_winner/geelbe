<?php

require_once("../../includes/config.php");
require_once("../../includes/facebook/facebook.php");
require_once("../../includes/classes/db.php");

$facebook = new Facebook(API_KEY, API_SECRET);

$db = new db();
$dbConnection = $db->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
$db->query("TRUNCATE promos_winners;");
$countries = array("AR", "MX", "CO");
$return = array();

foreach ($countries as $country) {
  $query = "SELECT *
             FROM promos
             WHERE is_active=1
             AND country='" . mysql_real_escape_string($country) . "'
             LIMIT 1;";

  $resultActive = $db->query($query);
  $activePromo = mysql_fetch_array($resultActive);
  $alreadyWon = array();

  if (!isset($activePromo['id'])) {
    if ($country == 'AR') {
      $promo_id = 999999998;
    } else if ($country == 'MX') {
      $promo_id = 999999999;
    } else if ($country == 'CO') {
      $promo_id = 999999997;
    }

    $query = "SELECT *
               FROM promos
               WHERE id=" . mysql_real_escape_string($promo_id) . "
               AND country='" . mysql_real_escape_string($country) . "'
               LIMIT 1;";

    $resultActive = $db->query($query);
    $activePromo = mysql_fetch_array($resultActive);
  }

  $promos = array();
  $query = "SELECT *
             FROM promos
             WHERE country='" . mysql_real_escape_string($country) . "'
             ORDER BY id ASC;";

  $resultWinners = $db->query($query);
  while ($row = mysql_fetch_array($resultWinners)) {
    if ($row['id'] != $activePromo['id']) {
      array_push($promos, $row);
    }
  }

  foreach ($promos as $promo) {
    $query = "SELECT COUNT(*) AS cnt, inviter, country
               FROM invites
               WHERE country = '" . $country . "'
               AND (status = 'invited' OR status = 'confirmed')
               AND promo_id = '" . mysql_real_escape_string($promo['id']) . "'
               GROUP BY inviter
               HAVING cnt >1
               ORDER BY cnt DESC;";

    $result = $db->query($query);
    $countWinners = 1;
    if (mysql_num_rows($result) != 0) {
      while ($winners = mysql_fetch_assoc($result)) {
        $queryIfPromoHasAlready3 = "SELECT id
                                        FROM promos_winners
                                        WHERE promo_id = ".mysql_real_escape_string($promo['id'])." 
                                        AND country = '".mysql_real_escape_string($country)."'";
        $resultIfPromoHasAlready3 = $db->query($queryIfPromoHasAlready3);
        if (mysql_num_rows($resultIfPromoHasAlready3) < 3) {
          $queryIfUIDExists = "SELECT id
                                  FROM promos_winners
                                  WHERE uid = '".mysql_real_escape_string($winners['inviter'])."'
                                  AND country = '".mysql_real_escape_string($country)."'
                                  LIMIT 1;";

          $resultIfUIDExists = $db->query($queryIfUIDExists);

          $queryIfUIDInvalid = "SELECT id
                                   FROM users_invalid
                                   WHERE uid = '".mysql_real_escape_string($winners['inviter'])."'
                                   LIMIT 1;";

          $resultIfUIDInvalid = $db->query($queryIfUIDInvalid);

          if (mysql_num_rows($resultIfUIDExists) == 0 && mysql_num_rows($resultIfUIDInvalid) == 0) {
            if ($countWinners <= 3) {
              $query = "INSERT INTO promos_winners
                         (`promo_id`, `uid`, `invites`, `country`, `date_created`)
                         VALUES ('".mysql_real_escape_string($promo['id'])."', '".mysql_real_escape_string($winners['inviter'])."', '".mysql_real_escape_string($winners['cnt'])."', '".mysql_real_escape_string($country)."', NOW());";
              $db->query($query);
              array_push($return, array(
                  'uid' => $winners['inviter'],
                  'promo_id' => $promo['id'],
                  'invites' => $winners['cnt'],
                  'country' => $winners['country']
              ));
            }
            $countWinners++;
          }
        }
      }
    }
  }
}
echo json_encode($return);
