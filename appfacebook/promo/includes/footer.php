<?php
  $query = "SELECT uid, name, profile_url, pic_square FROM user WHERE is_app_user = 1 AND uid IN (SELECT uid2 FROM friend WHERE uid1 = ".$uid.");";
  $friendsData = $facebook->api_client->fql_query($query);
  $ulWidth = (count($friendsData)*72)-16
?>
<div id="footer">
  <h1><img src="<?php echo APP_CALLBACK_URL; ?>promo/images/txt_footer.jpg" /></h1>
  <div id="carrousel">
    <a id="friendsCarrouselBtnPrev" class="prev disabled" onclick="moveCarrousel('prev', <?php echo $ulWidth; ?>);"><div></div></a>
    <div class="mask">
      <ul style="width:<?php echo $ulWidth; ?>px;" id="friendsCarrousel">
        <?php
          if($friendsData) {
            $count = 0;
            foreach($friendsData as $friendData) {
        ?>
              <li<?php echo $count==0 ? ' class="first"' : ''; ?>>
                <a href="<?php echo $friendData['profile_url']; ?>" target="_blank" title="<?php echo $friendData['name']; ?>"  style="background:url(<?php echo $friendData['pic_square']; ?>);"></a>
                <div class="frame"></div>
              </li>
        <?php
              $count++;
            }
          }
        ?>
      </ul>
    </div>
    <a id="friendsCarrouselBtnNext" class="next<?php echo count($friendsData)<=7 ? ' disabled' : '';?>" onclick="moveCarrousel('next', <?php echo $ulWidth; ?>);"><div></div></a>
  </div>
  <div class="clearBoth"></div>
</div>
<script type="text/javascript">
  var currentCarrouselPos = 0;
</script>