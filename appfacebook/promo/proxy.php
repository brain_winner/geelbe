<?php
  require_once '../includes/config.php';
  require_once '../includes/facebook/facebook.php';
  require_once '../includes/classes/db.php';

  $facebook = new Facebook(API_KEY, API_SECRET);
  $uid = $facebook->require_login();

  $db = new db();
  $dbConnection = $db->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  $result = $db->query("SELECT * FROM promos WHERE is_active=1 AND country='".mysql_real_escape_string(COUNTRY)."';");
  $activePromo = mysql_fetch_array($result);

  if(!isset($activePromo['id'])) {
    if(COUNTRY == 'AR') {
      $promo_id = 999999998;
    } else if(COUNTRY == 'MX') {
      $promo_id = 999999999;
    } else if(COUNTRY == 'CO') {
      $promo_id = 999999997;
    }
    $resultActive = $db->query("SELECT * FROM promos WHERE id=".mysql_real_escape_string($promo_id)." AND country='".mysql_real_escape_string(COUNTRY)."' LIMIT 1;");
    $activePromo = mysql_fetch_array($resultActive);
  }

  switch($_REQUEST['action']) {
    case 'invite':
      if(isset($_REQUEST['ids']) && isset($_REQUEST['invitation_id'])) {
        foreach($_REQUEST['ids'] as $invited) {
          $db->query("INSERT INTO invites (id, inviter, uid, status, country, promo_id, date_created) VALUES ('".mysql_real_escape_string($_REQUEST['invitation_id'])."', ".mysql_real_escape_string($_REQUEST["fb_sig_user"]).", ".mysql_real_escape_string($invited).", 'invited', '".mysql_real_escape_string(COUNTRY)."', ".mysql_real_escape_string($activePromo['id']).", NOW());");
        }
      }
      echo '<fb:redirect url="'.APP_URL.'promo/gracias.php?invited='.count($_REQUEST['ids']).'">';
    break;

    case 'confirm':
      if(isset($_REQUEST['invitation_id'])) {
        $db->query("UPDATE invites SET status='confirmed' WHERE uid='".mysql_real_escape_string($uid)."' AND id='".mysql_real_escape_string($_REQUEST['invitation_id'])."' AND promo_id = ".mysql_real_escape_string($activePromo['id'])." AND country='".mysql_real_escape_string(COUNTRY)."';");
      }
      echo '<fb:redirect url="'.TAB_URL.'">';
      
    break;
  }
?>
