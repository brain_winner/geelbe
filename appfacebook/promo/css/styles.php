<style type="text/css">
  #wrap {width:520px; padding:0 0 20px; float:left; margin:0 0 20px; background:url(<?php echo APP_CALLBACK_URL ?>promo/images/wrap_bg_mod.png);}
  .bases {width:520px; padding:20px 0 0; float:left; text-align:center; font-size:11px; font-weight:bold;}
  .bases a {color:#fff; text-decoration:none;}
  .bases a:hover {text-decoration:underline;}
  #header {width:520px; height:52px; float:left; background:url(<?php echo TAB_CALLBACK_URL ?>images/new_header.jpg) no-repeat;}
  #content {width:520px; padding:20px 0; float:left; background:#efefef;}
  #content h1, #footer h1 {width:500px; float:left; padding:0 0 10px; margin:0 10px; border-bottom:2px solid #ddd;}
  #content h1.multi {color:#b40e10; font-size:16px;}
  #content h1.multi small {font-weight:normal; color:#666;}
  #content h1 img {float:left;}
  #content .invite, #content .gracias {width:500px; float:left; margin:10px 10px 0;}
  #content .invite .inputaux {display:none !important;}
  #content .gracias {padding:0 0 20px; background:url(<?php echo APP_CALLBACK_URL; ?>promo/images/sombra.jpg) no-repeat center bottom;}
  #content .gracias .chances {width:97px; height:82px; float:left; background:url(<?php echo APP_CALLBACK_URL; ?>promo/images/chances_bg.jpg) no-repeat; color:#fff; text-align:center; font-weight:bold; font-size:36px; line-height:57px;}
  #content .gracias .txt {width:403px; height:62px; padding:20px 0 0; float:left; font-size:11px; line-height:20px;}
  #content .gracias .txt strong {font-size:12px;}
  #content .gracias .txt strong span {color:#999;}

  #content .btInvitar, #content .btVolver {text-decoration:none; text-align:center; line-height:35px; font-size:14px;}
  #content .btInvitar {width:163px; height:35px; float:left; margin:0 0 0 90px; background:url(<?php echo APP_CALLBACK_URL; ?>promo/images/bt_gris.jpg) no-repeat top; color:#333;}
  #content .btVolver {width:163px; height:35px; float:left; margin:0 0 0 20px; background:url(<?php echo APP_CALLBACK_URL; ?>promo/images/bt_celeste.jpg) no-repeat top; color:#fff;}
  #content .btInvitar:hover, #content .btVolver:hover {background-position:bottom;}
  
  #footer {width:520px; float:left; background:#efefef;}

  #carrousel {width:520px; height:70px; float:left;}
  #carrousel .mask {width:446px; float:left; height:70px; overflow:hidden; margin:0 11px;}
  #carrousel .next, #carrousel .prev {width:26px; height:70px; cursor:pointer; float:left;}
  #carrousel .next div, #carrousel .prev div {width:26px; height:42px; float:left; margin:14px 0 0; background:url(<?php echo TAB_CALLBACK_URL; ?>images/sprite_carrousel.png) no-repeat;}
  #carrousel .prev div {background-position:0 0;}
  #carrousel .prev:hover div {background-position:-52px 0;}
  #carrousel .prev:active div {background-position:-104px 0;}
  #carrousel #friendsCarrouselBtnPrev.disabled div {background-position:-156px 0;}
  #carrousel .next div {background-position:-26px 0;}
  #carrousel .next:hover div {background-position:-78px 0;}
  #carrousel .next:active div {background-position:-130px 0;}
  #carrousel #friendsCarrouselBtnNext.disabled div {background-position:-182px 0;}
  #carrousel a.disabled {cursor:default !important;}

  #carrousel ul {height:50px; float:left; padding:10px 0; position:relative; left:0;}
  #carrousel ul li {width:50px; height:50px; float:left; display:inline; padding:0 0 0 9px; margin:0 0 0 7px; background:url(<?php echo APP_CALLBACK_URL; ?>promo/images/footer_li_bg.gif) no-repeat left;}
  #carrousel ul li a {width:50px; height:50px; float:left; background-repeat:no-repeat;}
  #carrousel ul li.first {background:none !important; padding:0; margin:0;}
  #carrousel ul li .frame {width:50px; height:50px; margin:0 0 -50px -50px; position:relative; left:50px; top:0; background:url(<?php echo APP_CALLBACK_URL; ?>promo/images/frame_efefef.png);}

  #centralBlock {background:url(<?php echo TAB_CALLBACK_URL;?>images/fondo.png) no-repeat center; height:315px; width:520px; float:left;}
  #headerText {width:520px; float:left; margin:15px 0 0; text-align:center; color:#1E4777; font-family:Arial; font-size:16px; font-weight:bold;}
  #promoDetail {width:520px; float:left; height:148px; margin:11px 0 0;}
  #likeText {width:440px; float:left; padding:10px 40px 0; color:#fff; font-family:Arial; font-size:12px;}
  .buttons {width:520px; float:left; padding:18px 0 0; margin:0 0 0 196px; display:inline;}
  #inviteButton {background:url(<?php echo TAB_CALLBACK_URL;?>images/bot_invitar.png) no-repeat 0 -35px; width:128px; height:35px; float:left; cursor:pointer; display:block; text-decoration:none;}
  #inviteButton:hover {background-position:0 0;}
  #likeButton {background: url(<?php echo TAB_CALLBACK_URL;?>images/bot_megusta.png); width:87px; height:24px; cursor:pointer; position:relative; top:-40px; left:295px; margin:0 0 -24px -87px; float:left;}

  #ganadores {width:500px; float:left; padding:10px 0 0 20px;}
  #ganadores h1 {width:465px !important; color:#801616; font-size:16px;}
  #ganadores h4 {width:465px; float:left; padding:0 0 5px; border-bottom:1px solid #ddd; color:#515151; font-size:14px; margin:10px;}
  #ganadores ul {width:465px; float:left; margin:0 10px;}
  #ganadores ul li {width:145px; float:left; padding:0 0 10px 10px;}
  #ganadores ul li div {width:50px; height:50px; float:left;}
  #ganadores ul li div p {width:50px; height:50px; float:left; margin:0 0 -50px -50px; position:relative; top:0; left:50px; font-size:30px; line-height:50px; text-align:center; opacity:0.4; text-shadow:0px 0px 5px #000; color:#fff; font-weight:bold;}
  #ganadores ul li div p sup {font-size:16px;}
  #ganadores ul li h5 {width:85px; height:40px; float:left; padding:5px; font-size:11px;}
  #ganadores ul li h5 a, #ganadores ul li h5 span {width:110px; color:#515151; font-size:11px !important; text-align:left !important;}
  #ganadores ul li span {width:50px; height:40px; padding:5px 0; float:left; font-size:16px; text-align:center; font-weight:bold; color:#515151;}
</style>