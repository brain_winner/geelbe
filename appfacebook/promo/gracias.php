<?php
  require_once '../includes/config.php';
  require_once '../includes/facebook/facebook.php';
  require_once '../includes/classes/db.php';

  $facebook = new Facebook(API_KEY, API_SECRET);
  $facebook->require_frame();
  $uid = $facebook->require_login();

  $db = new db();
  $dbConnection = $db->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  $resultActive = $db->query("SELECT * FROM promos WHERE is_active=1 AND country='".mysql_real_escape_string(COUNTRY)."' LIMIT 1;");
  $activePromo = mysql_fetch_array($resultActive);

  if(!isset($activePromo['id'])) {
    if(COUNTRY == 'AR') {
      $promo_id = 999999998;
    } else if(COUNTRY == 'MX') {
      $promo_id = 999999999;
    } else if(COUNTRY == 'CO') {
      $promo_id = 999999997;
    }
    $resultActive = $db->query("SELECT * FROM promos WHERE id=".mysql_real_escape_string($promo_id)." AND country='".mysql_real_escape_string(COUNTRY)."' LIMIT 1;");
    $activePromo = mysql_fetch_array($resultActive);
  }

  $result = $db->query("SELECT * FROM invites WHERE inviter='".mysql_real_escape_string($uid)."' AND promo_id=".mysql_real_escape_string($activePromo['id'])." AND country='".mysql_real_escape_string(COUNTRY)."';");
  $confirmedInvitations = mysql_num_rows($result);

  include_once 'css/styles.php';
  include_once 'js/scripts.php';
?>
<div id="wrap">
  <div id="header"></div>
  <div id="content">
    <h1>
      <img src="<?php echo APP_CALLBACK_URL; ?>promo/images/txt_gracias.jpg" />
    </h1>
    <div class="gracias">
      <div class="chances">
        <?php echo $confirmedInvitations; ?>
      </div>
      <div class="txt">
        <strong>Invitaste <?php echo $_REQUEST['invited']; ?> amigo<?php echo intval($_REQUEST['invited'])>1 ? 's' : '';?> para la promo <span><?php echo $activePromo['duration'];?>.</span></strong><br>
        Record&aacute; que pod&eacute;s seguir invitando todos los d&iacute;as!
      </div>
    </div>
    <a href="<?php echo APP_URL; ?>promo/invite.php" class="btInvitar">Invitar m&aacute;s amigos</a>
    <a href="<?php echo TAB_URL; ?>" class="btVolver"><?php echo $i18n['txt_bt_volver_gracias']; ?></a>
  </div>
  <?php include_once 'includes/footer.php'; ?>
</div>