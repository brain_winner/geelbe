<script type="text/javascript">
function moveCarrousel(direction, maxWidth) {
  var target = document.getElementById('friendsCarrousel');
  var liSize = 66;

  if(direction == 'next') {
    if(currentCarrouselPos>((-maxWidth)+446)) {
      if(document.getElementById('friendsCarrouselBtnPrev').hasClassName('disabled')) {
        document.getElementById('friendsCarrouselBtnPrev').removeClassName('disabled');
      }
      if(currentCarrouselPos == (((-maxWidth)+446)+48+liSize)) {
        document.getElementById('friendsCarrouselBtnNext').addClassName('disabled');
      }
      currentCarrouselPos = currentCarrouselPos-liSize;
      Animation(target).to('left', currentCarrouselPos).duration(300).go();
    } else {
      document.getElementById('friendsCarrouselBtnNext').addClassName('disabled');
    }
  } else if(direction == 'prev') {
    if(currentCarrouselPos<0) {
      if(document.getElementById('friendsCarrouselBtnNext').hasClassName('disabled')) {
        document.getElementById('friendsCarrouselBtnNext').removeClassName('disabled');
      }
      if(currentCarrouselPos == -liSize) {
        document.getElementById('friendsCarrouselBtnPrev').addClassName('disabled');
      }
      currentCarrouselPos = currentCarrouselPos+liSize;
      Animation(target).to('left', currentCarrouselPos).duration(300).go();
    } else {
      document.getElementById('friendsCarrouselBtnPrev').addClassName('disabled');
    }
  }
}
</script>