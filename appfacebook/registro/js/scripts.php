<script type="text/javascript">
  var activeBlink = false;

  function startPasswordMagic(targetID) {
    var target = document.getElementById(targetID);
    var targetFake = document.getElementById(targetID+'Fake');

    setInterval(function() {
      var magicString = "";
      for(var i=0; i<target.getValue().length; i++) {
        magicString += "*";
      }
      targetFake.setValue(magicString);
    }, 150);
  }

  function register() {

    Facebook.showPermissionDialog('email,user_birthday', function(permissions) {
        if (permissions == null) {
          var dialog = new Dialog();
          dialog.showMessage('Alerta', 'Debes aceptar los permisos extendidos para poder registrate en Geelbe.');
          return false;
        }

        if(!document.getElementById('acepto2').getChecked()) {
          var dialog = new Dialog();
          dialog.showMessage('Alerta', 'Debes aceptar los t�rminos y condiciones para poder registrarte en Geelbe.');
          return false;
        } 
        
        var ajax = new Ajax();
        ajax.responseType = Ajax.FBML;
        ajax.ondone = function (data) {
          console.log(data);
          var dialog = new Dialog();          
          dialog.showMessage('Geelbe', data);
        }

        ajax.onerror = function () {
          console.log(data);
          var dialog = new Dialog();
          dialog.showMessage('Hubo un problema', 'Ha habido un problema al procesar el registro. Por favor intente nuevamente m�s tarde.');
        }

        ajax.post('<?php echo APP_CALLBACK_URL;?>registro/services/register.php');
      }, false);
  }

  function validateForm() {
    var params = {
      "txtNombre": document.getElementById('txtNombre').getValue(),
      "txtApellido": document.getElementById('txtApellido').getValue(),
      "txtEmail": document.getElementById('txtEmail').getValue(),
      "txtPadrino": document.getElementById('txtPadrino').getValue(),
      "txtClave": document.getElementById('txtClave').getValue(),
      "txtConfClave": document.getElementById('txtConfClave').getValue(),
      "cbxIdTratamiento": document.getElementById('sexo_1').getChecked() ? document.getElementById('sexo_1').getValue() : (document.getElementById('sexo_2').getChecked() ? document.getElementById('sexo_2').getValue() : ''),
      "cmb_nacimientoDia": document.getElementById('cmb_nacimientoDia').getValue(),
      "cmb_nacimientoMes": document.getElementById('cmb_nacimientoMes').getValue(),
      "cmb_nacimientoAnio": document.getElementById('cmb_nacimientoAnio').getValue(),
      "acepto2": document.getElementById('acepto2').getValue()
    };

    var ajax = new Ajax();
    ajax.responseType = Ajax.JSON;
    ajax.ondone = function(data) {
      var status = data.estado;
      var error = data.error_key;

      if(status != 1 && error != '') {
        switch(error) {
          case "NOMBRE": showMessage("error", "El campo nombre es requerido."); break;
          case "APELLIDO": showMessage("error", "El campo apellido es requerido."); break;
          case "MAIL_INCORRECTO": showMessage("error", "El mail ingresado no tiene un formato correcto."); break;
          case "CLAVE_INCORRECTO": showMessage("error", "El campo clave es requerido y debe tener m�s de seis caracteres."); break;
          case "REPETIR_CLAVE_INCORRECTO": showMessage("error", "El campo confirmar clave es requerido."); break;
          case "CLAVE_DIFERENTES": showMessage("error", "La contrase�a debe coincidir con confirmar contrase�a."); break;
          case "FECHA DE NACIMIENTO": showMessage("error", "La Fecha de nacimiento es requerida."); break;
          case "FECHA INVALIDA": showMessage("error", "La fecha de nacimiento es inv�lida."); break;
          case "MENOR DE EDAD": showMessage("error", "Para registrarse debe ser mayor de edad."); break;
          case "TERMINOS": showMessage("error", "Debe aceptar los terminos."); break;
          case "USUARIO_EXISTENTE": showMessage("error", "El email ingresado ya existe en nuestra base de datos."); break;
          case "ERROR_GENERAL": showMessage("error", "Hubo problemas al realizar el registro, por favor intente nuevamente y si el problema persiste cont�ctese con atenci�n al cliente."); break;
          default : showMessage("error", "Hubo problemas al realizar el registro, por favor intente nuevamente y si el problema persiste cont�ctese con atenci�n al cliente."); break;
        }
      } else {
        showMessage("gracias", "Te enviamos un e-mail a tu casilla para que actives tu cuenta.");
        setTimeout(function() {
          document.setLocation("<?php echo TAB_URL;?>");
        }, 5000);
      }
    }
    ajax.post("<?php echo $i18n['form_registro_url']; ?>", params);
  }

  function showMessage(type, message) {
    switch (type) {
      case "error":
        document.getElementById('messenger').setStyle('backgroundColor', '#FFA8A8');
        document.getElementById('messenger').setStyle('border', '5px solid #7A0000');
        document.getElementById('messenger').setStyle('color', '#7A0000');
      break;
      case "gracias":
        document.getElementById('messenger').setStyle('backgroundColor', '#ccc');
        document.getElementById('messenger').setStyle('border', '5px solid #333');
        document.getElementById('messenger').setStyle('color', '#333');
      break;
    }

    document.getElementById('messenger').setTextValue(message);
    document.getElementById('messenger').setStyle('display', 'block');
  }
</script>