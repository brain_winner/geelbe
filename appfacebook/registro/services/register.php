<?php

  try {
    
    require_once("../../includes/config.php");
    require_once("../../includes/facebook/facebook.php");
    $facebook = new Facebook(API_KEY_REGISTRO, API_SECRET_REGISTRO);
     $facebook->set_user($_REQUEST["fb_sig_user"], $_REQUEST["fb_sig_session_key"]);

    if ($facebook->user == null) {
      throw new Exception('No se pudo setear el usuario');
    }
    $userData = $facebook->api_client->users_getInfo($facebook->user, array('uid', 'email', 'first_name', 'last_name', 'birthday_date', 'sex'));
    if ($userData == "") {
      throw new Exception("ERROR - could not extract user data");
    }
    //Mandamos la data al Registry Updater    
    $ch = curl_init(CO_REGISTRY_SERVICE);
    curl_setopt ($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt ($ch, CURLOPT_POSTFIELDS, "key=" . REGISTRY_HASH . "&user_data=" . json_encode($userData[0]));

    $response = curl_exec ($ch);
    curl_close ($ch);
    if (!$response) {
      throw new Exception("ERROR - curl response null");
    }

    $data = json_decode($response);

    if (!$data) {
      throw new Exception("ERROR - curl not parse json response");
    }
    //Devolvemos la url del usuario autologueado
    if(!isset($data->url_redirect) || empty($data->url_redirect)) {
      throw new Exception("ERROR - url no viene");
    }
    echo "<div>
            <center>
              <span>Bienvenido a Geelbe!! Te has registrado correctamente.</span><br /><br />
              <a href='{$data->url_redirect}' target='_blank' class='irageelbe'></a>
            </center>
          </div>";

  } catch(Exception $e) {
    echo "<div>
            <h3>Hubo un problema en el registro. Por favor, intentelo nuevamente mas tarde.</h3>
          </div>";
    //@todo: loguear error $e->getMessage().
  }
?>