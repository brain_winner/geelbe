<style type="text/css">
  #wrap {width:520px; float:left; margin:0 0 20px; background:url(<?php echo APP_CALLBACK_URL ?>promo/images/wrap_bg_mod.png);}
  #header {width:520px; height:52px; float:left; background:url(<?php echo TAB_CALLBACK_URL ?>images/new_header.jpg) no-repeat;}
  #content {width:520px; padding:20px 0; float:left; background:#efefef;}

  #container {width:520px; height:374px; float:left; background:url(<?php echo APP_CALLBACK_URL ?>/registro/images/registry-bg.jpg) no-repeat;}
  
  #container.AR {background:url(<?php echo APP_CALLBACK_URL ?>/registro/images/registry-bg.jpg) no-repeat;}
  
  #container.MX {background:url(<?php echo APP_CALLBACK_URL ?>/registro/images/registry-bg-mx.jpg) no-repeat;}
  
  #container.CO {background:url(<?php echo APP_CALLBACK_URL ?>/registro/images/registry-bg-co.jpg) no-repeat;}
  
  h1 {width:500px; margin:0 0 0 10px; padding:0 0 10px; float:left; border-bottom:2px solid #ddd; color:#b40e10; font-weight:bold; font-size:16px;}

  form {width:520px; float:left; background:url(<?php echo $i18n['form_registro_bg'] ?>) no-repeat 310px 10px;}
  
  table {width:500px; margin:10px 0 0 10px; float:left;}

  td {padding:5px;}
  td.label {width:70px; text-align:right !important; font-size:12px;}

  label {font-weight:normal !important;}

  td small {padding:2px; color:#b40e10;}

  input {width:200px; float:left; margin:0; padding:2px; z-index:10; position:relative; border:1px solid #999;}
  input.hidden {left:206px; margin:0 0 0 -206px; z-index:20; color:#fff; background:none; text-indent:-9999px;}
  input.radio, input.checkbox {width:auto; border:0; padding:0; float:none; vertical-align:middle;}

  .terms {text-indent:40px;}
  .terms a {color:#b40e10;}

  a.registrarme {
    width:114px;
    height:35px;
    float:left;
    text-align:center;
    font-size:14px;
    color:#fff;
    background:url(<?php echo APP_CALLBACK_URL ?>registro/images/bt_red.png) no-repeat top;
    text-decoration:none;
    line-height:35px;
    margin:10px 0 10px 190px;
  }
  a.registrarme:hover {background-position:bottom !important;}

  a.irageelbe {
    display:block;
    width:114px;
    height:35px;
    background:url(<?php echo APP_CALLBACK_URL ?>registro/images/ir-a-geelbe.png) no-repeat top;
  }
  a.irageelbe:hover {background-position:bottom !important;}

  a.register {
    display:block;
    width:194px;
    height:45px;
    margin:145px auto 0 auto;
    background:url(<?php echo APP_CALLBACK_URL ?>registro/images/registrarme.png) no-repeat top;
  }
  a.register:hover {background-position:bottom !important;}


  img.logos {float:left; margin:0 0 0 20px;}

  #messenger {
    width:430px;
    height:60px;
    margin:0 0 -40px -440px;
    float:left;
    border:5px solid #333;
    position:relative;
    left:480px;
    top:75px;
    background-color:#ccc;
    background-image:url(<?php echo APP_CALLBACK_URL ?>registro/images/close_x.png);
    background-repeat:no-repeat;
    background-position:410px 10px;
    z-index:21;
    display:none;
    text-align:center;
    line-height:62px;
    cursor:pointer;
    font-size:13px;
    -moz-border-radius:10px;
  }
  #terminos {
    margin-left:162px;
    margin-top:7px;
}
</style>