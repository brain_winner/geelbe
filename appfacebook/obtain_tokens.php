<?php

	header("Cache-Control: no-cache");
	header("Pragma: no-cache");

	if($_REQUEST["country"] == "ar") {
		$_REQUEST["app_id"] = "109174705776408";
	} else if($_REQUEST["country"] == "co") {
		$_REQUEST["app_id"] = "130962696927517";
	} else {
		echo "Specify country.";
		die;
	}
	
	require_once 'includes/config.php';

	$code = $_REQUEST["code"];
	$countryParam = $_REQUEST["country"];
	
	$redirect_params = "";
	if (isset($countryParam)) {
		$redirect_params .= "&country=".$countryParam;
	}
	
    if(empty($code)) {
        $dialog_url = "http://www.facebook.com/dialog/oauth?client_id=" 
            . API_ID . "&redirect_uri=" . urlencode("http://www.geelbe.com/appfacebook/obtain_tokens.php?".substr($redirect_params, 1))
            . "&display=popup&scope=publish_stream,offline_access,rsvp_event,user_events,create_event,manage_pages";

        echo("<script> top.location.href='" . $dialog_url . "'</script>");
    } else {
	    $token_url = "https://graph.facebook.com/oauth/access_token?client_id="
	        . API_ID . "&redirect_uri=" . urlencode("http://".$_SERVER["SERVER_NAME"]."/appfacebook/obtain_tokens.php?".substr($redirect_params, 1)) . "&client_secret="
	        . API_SECRET . "&code=" . $code;
	
	    $access_token = file_get_contents($token_url);
	    
	    $accounts_url = "https://graph.facebook.com/me/accounts?" . $access_token;
  
	    $accounts = file_get_contents($accounts_url);
	    echo $accounts;
	    
    }
    
?>
