<?php
    
    require_once("includes/config.php");
    require_once("includes/facebook/facebook.php");
    $facebook = new Facebook(API_KEY_REGISTRO, API_SECRET_REGISTRO);
    if ($facebook->user == null) {
      $loginUrl = $facebook->get_login_url( APP_REGISTRO_URL . "redirector.php?user_id={$_REQUEST["user_id"]}&url={$_REQUEST['url']}", true, 'email,user_birthday');
      $facebook->redirect($loginUrl);
    }

    //Obtenemos la información del usuario:    

    $userData = $facebook->api_client->users_getInfo($facebook->user, array('uid', 'email', 'first_name', 'last_name', 'birthday_date', 'sex'));
//    var_dump($userData);
//    die();
    $userId = $_REQUEST["user_id"];
    $hash = md5($userId . HASH_SALT);

    //Mandamos la data al Registry Updater 
    
    $ch = curl_init(REGISTRY_UPDATER_SERVICE);
    curl_setopt ($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt ($ch, CURLOPT_POSTFIELDS, "user_id={$userId}&hash={$hash}&user_data=" . json_encode($userData[0]));
    $response = curl_exec ($ch);
    curl_close ($ch);
    //@todo: loguear respuesta del servicio.
//    var_dump($response);

    //Lo Mandamos a la pagina del compose
    $facebook->redirect(str_replace('{*invite_link*}',$_REQUEST["url"],INVITE_LINK));

?>