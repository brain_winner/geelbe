<?php
    class dmBonos
    {
        /**
        * @desc Modifica el estado de un bono.
        * @param int. IdBono a modificar.
        * @param int. Bandera para el nuevo estado.
        */
        public static function EstablecerEstado($IdBono, $NuevoEstado)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE bonos_x_usuarios SET IdEstadoBono=" .$NuevoEstado . " WHERE IdBono=" . $IdBono);
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch(MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }

        public static function getBonosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(*) as total 
									  FROM(	SELECT 	A.`BU.IdBono` as `A.IdBono`, 
													A.`TR.Descripcion` as `A.TR.Descripcion`, 
													A.`FechaCaducidad` as `A.FechaCaducidad`, 
													A.`RN.Aplica1` as `A.Aplica1`, 
													A.`BU.IdEstadoBono` as `A.IdEstadoBono`, 
													A.`DUP.Apellido` as `A.Apellido`, 
													A.`DUP.Nombre` as `A.Nombre`, 
													A.`U.IdUsuario` as `A.IdUsuario`, 
													A.`U.NombreUsuario` as `A.NombreUsuario`, 
													A.`EB.Descripcion` as `A.EB.Descripcion`, 
													IF(SUM(BP.Importe) IS NULL, 0, SUM(BP.Importe)) as `Importe` 
											FROM ( 	SELECT 	BU.IdBono as `BU.IdBono`, 
															TR.Descripcion as `TR.Descripcion`, 
															UNIX_TIMESTAMP( BU.Fecha ) + RN.Caducidad * ( 24 *60 *60 ) as `FechaCaducidad`, 
															RN.Aplica1 as `RN.Aplica1`,  
															BU.IdEstadoBono as `BU.IdEstadoBono`, 
															DUP.Apellido as `DUP.Apellido`, 
															DUP.Nombre as `DUP.Nombre`, 
															U.IdUsuario as `U.IdUsuario`, 
															U.NombreUsuario as `U.NombreUsuario`, 
															EB.Descripcion as `EB.Descripcion` 
													FROM bonos_x_usuarios BU
													INNER JOIN reglas_aplicadas RA ON BU.IdReglaAplicada = RA.IdReglaAplicada
													INNER JOIN reglasnegocio RN ON RA.IdRegla = RN.IdRegla
													INNER JOIN tiposbeneficios TB ON TB.IdBeneficio = RN.Beneficio1
													INNER JOIN tiposreglas TR ON TR.IdTipoRegla = RN.IdTipoRegla
													INNER JOIN usuarios U ON BU.IdUsuario = U.IdUsuario
													LEFT JOIN datosusuariospersonales DUP ON DUP.IdUsuario = U.IdUsuario
													INNER JOIN estadosbonos EB ON EB.IdEstadoBono = BU.IdEstadoBono
													WHERE ($filtersSQL) 
											) A 
											LEFT JOIN bonos_x_pedidos BP ON BP.IdBono = A.`BU.IdBono`
											GROUP BY A.`BU.IdBono`, A.`TR.Descripcion`, A.`FechaCaducidad`, A.`RN.Aplica1`, A.`BU.IdEstadoBono` 
									  ) B ");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch(MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }  
        }
		
        public static function getBonosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT 	B.`A.IdBono` as `B.IdBono`,
												IF(B.`A.Aplica1`-B.`Importe`=0, -1, B.`A.IdEstadoBono`) as `B.IdEstadoBono`, 
												B.`A.IdUsuario` as `B.IdUsuario`, 
												B.`A.NombreUsuario` as `B.NombreUsuario`, 
												B.`A.Apellido` as `B.Apellido`, 
												B.`A.Nombre` as `B.Nombre`, 
												B.`A.TR.Descripcion` as `B.TR.Descripcion`, 
												FROM_UNIXTIME(B.`A.FechaCaducidad`, '%d/%m/%Y') as `B.FechaCaducidad`,  
												B.`A.Aplica1` as `B.Aplica1`, 
												IF(B.`A.Aplica1`-B.`Importe`=0, 'Consumido', B.`A.EB.Descripcion`) AS `B.Estado`
									  FROM(	SELECT 	A.`BU.IdBono` as `A.IdBono`, 
													A.`TR.Descripcion` as `A.TR.Descripcion`, 
													A.`FechaCaducidad` as `A.FechaCaducidad`, 
													A.`RN.Aplica1` as `A.Aplica1`, 
													A.`BU.IdEstadoBono` as `A.IdEstadoBono`, 
													A.`DUP.Apellido` as `A.Apellido`, 
													A.`DUP.Nombre` as `A.Nombre`, 
													A.`U.IdUsuario` as `A.IdUsuario`, 
													A.`U.NombreUsuario` as `A.NombreUsuario`, 
													A.`EB.Descripcion` as `A.EB.Descripcion`, 
													IF(SUM(BP.Importe) IS NULL, 0, SUM(BP.Importe)) as `Importe` 
											FROM ( 	SELECT 	BU.IdBono as `BU.IdBono`, 
															TR.Descripcion as `TR.Descripcion`, 
															UNIX_TIMESTAMP( BU.Fecha ) + RN.Caducidad * ( 24 *60 *60 ) as `FechaCaducidad`, 
															RN.Aplica1 as `RN.Aplica1`,  
															BU.IdEstadoBono as `BU.IdEstadoBono`, 
															DUP.Apellido as `DUP.Apellido`, 
															DUP.Nombre as `DUP.Nombre`, 
															U.IdUsuario as `U.IdUsuario`, 
															U.NombreUsuario as `U.NombreUsuario`, 
															EB.Descripcion as `EB.Descripcion` 
													FROM bonos_x_usuarios BU
													INNER JOIN reglas_aplicadas RA ON BU.IdReglaAplicada = RA.IdReglaAplicada
													INNER JOIN reglasnegocio RN ON RA.IdRegla = RN.IdRegla
													INNER JOIN tiposbeneficios TB ON TB.IdBeneficio = RN.Beneficio1
													INNER JOIN tiposreglas TR ON TR.IdTipoRegla = RN.IdTipoRegla
													INNER JOIN usuarios U ON BU.IdUsuario = U.IdUsuario
													LEFT JOIN datosusuariospersonales DUP ON DUP.IdUsuario = U.IdUsuario
													INNER JOIN estadosbonos EB ON EB.IdEstadoBono = BU.IdEstadoBono
													WHERE ($filtersSQL) 
											) A 
											LEFT JOIN bonos_x_pedidos BP ON BP.IdBono = A.`BU.IdBono`
											GROUP BY A.`BU.IdBono`, A.`TR.Descripcion`, A.`FechaCaducidad`, A.`RN.Aplica1`, A.`BU.IdEstadoBono` 
									  ) B  
									  ORDER BY $sortSQL 
									  LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
									  
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                ColumnaTimestamp_to_date(&$Tabla, "Fecha de caducidad");
                ColumnaMoneda(&$Tabla, "Valor");              
                return $Tabla;                
            }
            catch(MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }  
        }
		
		
    }
?>
