<?
    class dmMarcas
    {
        public static function save(MySQL_Marcas $objMarca)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objMarca->getInsert());
                $oConexion->EjecutarQuery();
                if($objMarca->getIdMarca() == 0)
                {
                    $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                    $ID = $oConexion->DevolverQuery();
                    $objMarca->setIdMarca($ID[0]["Id"]);
                }
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function delete(MySQL_Marcas $objMarca)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objMarca->getDelete());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getByIdMarca($Id)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $objMarca = new MySQL_Marcas();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objMarca->getById($Id));
                $Tabla = $oConexion->DevolverQuery();
                foreach($Tabla as $fila)
                {
                    foreach ($fila as $atributo => $valor)
                    {
                        $objMarca->__set($valor, $atributo);
                    }
                }
                $oConexion->Cerrar_Trans();
                return $objMarca;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getMarcas($columnaNro, $columnaSentido, $filter="")
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdMarca AS Codigo, Nombre AS Marca, IF(Habilitado = 1, 'Si', 'No') AS Habilitado FROM marcasproductos WHERE Nombre LIKE \"%".$filter."%\" ORDER BY ".$columnaNro." ".$columnaSentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>