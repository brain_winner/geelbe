<?
    class MySQL_Marcas extends marcas
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_idmarca")
                {   
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=($valor != 0) ? chr(34).mysql_real_escape_string($valor).chr(34).", " : "NULL, ";
                }
                else
                {   
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
            }
            if($this->getIdMarca()==0)
                return "INSERT INTO marcasproductos (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") -- ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
            else
                return "UPDATE marcasproductos SET ".substr($strCamposU, 0, strlen($strCamposU)-2)." WHERE IdMarca = ".$this->getIdMarca();
        }
        public function getDelete()
        {
            return "DELETE FROM marcasproductos WHERE IdMarca = ".$this->getIdMarca();
           // return "UPDATE marcasproductos SET Habilitado = 0 WHERE IdMarca = ".$this->getIdMarca();
        }
        public function getById($Id)
        {
            $strCampos="";
            $Me = $this->__toArray();                
            foreach ($Me as $propiedad => $valor)
            {
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM marcasproductos WHERE IdMarca = ".mysql_real_escape_string($Id);
        }
    }
?>