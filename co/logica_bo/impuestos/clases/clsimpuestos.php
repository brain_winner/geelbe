<?
    class MySQL_Impuestos extends impuestos
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_idimpuesto")
                {   
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=($valor != 0) ? chr(34).mysql_real_escape_string($valor).chr(34).", " : "NULL, ";
                }
                else
                {   
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
            }
            return "INSERT INTO impuestos (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getDelete()
        {
            return "DELETE FROM impuestos WHERE IdImpuesto = ".$this->getIdImpuesto();
        }
        public function getById($Id)
        {
            $strCampos="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM impuestos WHERE IdImpuesto = ".mysql_real_escape_string($Id);
        }
    }
    class MySQL_ImpuestosXProvincias extends impuestos_x_provincias
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_idimpuesto" || $propiedad == "_idprovincia")
                {
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
                else
                {
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
            }
            return "INSERT INTO impuestos_x_provincias (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getDelete()
        {
            return "DELETE FROM impuestos_x_provincias WHERE IdImpuesto = ".$this->getIdImpuesto()." AND IdProvincia = ".$this->getIdProvincia();
        }
        public function getById($IdImpuesto, $IdProvincia)
        {
            $strCampos="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM impuestos_x_provincias WHERE IdImpuesto = ".mysql_real_escape_string($IdImpuesto)." AND IdProvincia = ". mysql_real_escape_string($IdProvincia);
        }
    }
?>