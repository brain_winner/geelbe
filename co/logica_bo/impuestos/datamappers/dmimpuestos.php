<?
    class dmImpuestos
    {
        public static function save(MySQL_Impuestos $objImpuesto)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objImpuesto->getInsert());
                $oConexion->EjecutarQuery();
                if($objImpuesto->getIdImpuesto() == 0)
                {
                    $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                    $ID = $oConexion->DevolverQuery();
                    $objImpuesto->setIdImpuesto($ID[0]["Id"]);
                }
                foreach($objImpuesto->getImpuestosXProvincias() as $ip)
                {
                    $ip->setIdImpuesto($objImpuesto->getIdImpuesto());
                    $oConexion->setQuery($ip->getInsert());
                    $oConexion->EjecutarQuery();
                }
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function delete(MySQL_Impuestos $objImpuesto)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objImpuesto->getDelete());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getByIdImpuesto($Id)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $objImpuesto = new MySQL_Impuestos();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objImpuesto->getById($Id));
                $Tabla = $oConexion->DevolverQuery();
                foreach($Tabla as $fila)
                {
                    foreach ($fila as $atributo => $valor)
                    {
                        $objImpuesto->__set($valor, $atributo);
                    }
                }
                $oConexion->Cerrar_Trans();
                return $objImpuesto;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getImpuestos($columnaNro, $columnaSentido, $filter = "")
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdImpuesto AS Codigo, Nombre AS Impuesto, IF(Habilitado = 1, 'Activo','Eliminado') AS Estado FROM impuestos WHERE Nombre LIKE \"%".$filter."%\" ORDER BY ".$columnaNro." ".$columnaSentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getProvinciasByIdImpuesto($IdImpuesto)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT P.IdProvincia, Nombre, Valor FROM provincias P LEFT JOIN impuestos_x_provincias IP ON P.IdProvincia = IP.IdProvincia WHERE IdImpuesto = ".mysql_real_escape_string($IdImpuesto)." AND P.IdPais = 15 UNION SELECT P.IdProvincia, Nombre, NULL FROM provincias P WHERE NOT P.IdProvincia IN(SELECT P.IdProvincia FROM provincias P LEFT JOIN impuestos_x_provincias IP ON P.IdProvincia = IP.IdProvincia WHERE IdImpuesto = ".mysql_real_escape_string($IdImpuesto).")");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>