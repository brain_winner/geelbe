<?php
    if($_POST)
    {
        ob_start();
        extract($_POST);
        try
        {   
            $objImpuesto = new MySQL_Impuestos();
            $objImpuesto->setIdImpuesto($txtIdImpuesto);
            $objImpuesto->setNombre($txtNombre);
            $objImpuesto->setOperador("");
            $objImpuesto->setTipo($optTipo);
            $objImpuesto->setHabilitado(isset($chkHabilitado)?1:0);
            if(isset($frmImpuestos))
            {
                foreach($frmImpuestos as $imp)
                {
                    $objIP = new MySQL_ImpuestosXProvincias();
                    $objIP->setIdProvincia($imp["IdProvincia"]);
                    $objIP->setValor($imp["Valor"]);
                    $objImpuesto->addImpuestoXProvincia($objIP);
                }
            }
            dmImpuestos::save($objImpuesto);
            ?>
                <script type="text/javascript">
                    window.irVolver();
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            print_r($e);
            ?>
                <script>
                    window.ErrorMSJ("<?=$e->getNumeroSQLERROR()?>", "");
                </script>
            <?
        }
        ob_flush();
    }
?>