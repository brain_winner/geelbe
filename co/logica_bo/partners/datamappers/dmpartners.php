<?php
    class dmPartners
    {
        public static function getAll($columna = 'nombre', $sentido = 'asc', $filtro="")
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->setQuery("SELECT IdPartner, NombrePartner as Nombre FROM partners ".($filtro != '' ? "WHERE NombrePartner LIKE \"%".$filtro."%\"" : '')." ORDER BY " . $columna . " " . $sentido);
                $Tabla = $oConexion->DevolverQuery();
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        public static function save($oLocal)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                if ($oLocal->getIdPartner() == "") {
                    $oConexion->setQuery("INSERT INTO partners (NombrePartner, PixelesRegistro, PixelesActivacion) VALUES('".htmlspecialchars(mysql_real_escape_string($oLocal->getNombre()), ENT_QUOTES)."', '".htmlspecialchars(mysql_real_escape_string($oLocal->getPixelesRegistro()), ENT_QUOTES)."', '".htmlspecialchars(mysql_real_escape_string($oLocal->getPixelesActivacion()), ENT_QUOTES)."')");
                } else
                    $oConexion->setQuery("UPDATE partners SET NombrePartner = '".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($oLocal->getNombre())), ENT_QUOTES)."', PixelesRegistro = '".htmlspecialchars(mysql_real_escape_string($oLocal->getPixelesRegistro()), ENT_QUOTES)."', PixelesActivacion = '".htmlspecialchars(mysql_real_escape_string($oLocal->getPixelesActivacion()), ENT_QUOTES)."' WHERE IdPartner = " . $oLocal->getIdPartner());
                $oConexion->EjecutarQuery();
                
                if($oLocal->getIdPartner() == "")
                	$oLocal->setIdPartner($oConexion->LastId());

                return $oLocal->getIdPartner();
            }
            catch(MySQLException $e)
            {
                throw $e;
            }            
        }
        public static function getById($idLocal)
        {
            $oConexion = Conexion::nuevo();
            $oLocal = new Partners();
            try
            {
                $oConexion->setQuery("SELECT * FROM partners WHERE IdPartner = " . $idLocal);
                $Fila = $oConexion->DevolverQuery();
                
                 $oLocal->setIdPartner($Fila[0]["IdPartner"]);
                 $oLocal->setNombre($Fila[0]["NombrePartner"]);
                 $oLocal->setPixelesRegistro($Fila[0]["PixelesRegistro"]);
                 $oLocal->setPixelesActivacion($Fila[0]["PixelesActivacion"]);

                }
                catch(MySQLException $e)
                {
                    throw $e;
                }
                return $oLocal;
            }
            
       	public static function delete($obj)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->setQuery("DELETE FROM partners WHERE IdPartner = ".$obj->getIdPartner());
                $oConexion->EjecutarQuery();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>
