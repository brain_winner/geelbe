<?php
    class Partners
    {
        private $_idpartner;
        private $_nombrepartner;
        private $_pixelesregistro;
        private $_pixelesactivacion;
       
        public function __construct()
        {
            $this->_idpartner = "";
            $this->_nombrepartner = "";
            $this->_pixelesregistro = "";
        	$this->_pixelesactivacion = "";
        }
        public function getIdPartner()
        {
            return $this->_idpartner;
        }
        public function setIdPartner($idlocal)
        {
            $this->_idpartner = $idlocal;
        }
        public function getNombre()
        {
            return $this->_nombrepartner;
        }
        public function setNombre($descripcion)
        {
            $this->_nombrepartner = $descripcion;
        }
        public function getPixelesRegistro()
        {
            return $this->_pixelesregistro;
        }
        public function setPixelesRegistro($pixeles)
        {
            $this->_pixelesregistro = $pixeles;
        }
        public function getPixelesActivacion()
        {
            return $this->_pixelesactivacion;
        }
        public function setPixelesActivacion($pixeles)
        {
            $this->_pixelesactivacion = $pixeles;
        }
    }
?>
