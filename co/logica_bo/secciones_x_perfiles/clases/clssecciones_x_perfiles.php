<?
/**
*****************************************************************************************************
* CLASES																							*
* ATBK																								*
* www.atbk.com.ar																					*
*****************************************************************************************************
*/
/**
* Clase [%Nombre%]
* @author ATBK
* @copyright Copyright (c) 2007
* @license http://www.atbk.com.ar
*/
class Secciones_x_perfiles
{
	/*Datos Privados*/
	private $_idseccion;	private $_idperfil;
	/*Constructor*/
/***@var idseccion*@var idperfil*/
	function __construct($idseccion=0, $idperfil=0)
	{
		$this->_idseccion=$idseccion;		$this->_idperfil=$idperfil;

	}
	/*Metodos Publicos*/
	/**	* Set idSeccion	* @var idSeccion	*/	function setidSeccion($idseccion)	{		$this->_idseccion =$idseccion;	}	/**	* Get idSeccion	* @return idSeccion	*/	function getidSeccion()	{		return $this->_idseccion;	}	/**	* Set idPerfil	* @var idPerfil	*/	function setidPerfil($idperfil)	{		$this->_idperfil =$idperfil;	}	/**	* Get idPerfil	* @return idPerfil	*/	function getidPerfil()	{		return $this->_idperfil;	}	
	/*Destructor*/
	function __destruct()
	{
	}
}
?>