<?
    class dmSecciones_x_perfiles
    {
	    /**
	    *Realiza el INSERT en la base de datos
	    *@param class oSecciones_x_perfiles
	    *@return 1 si ejecuto con exito, 0 si ocurrio un error.
	    */
	    public static function Insert($osecciones_x_perfiles)
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir_Trans();
			    $cn->setQuery("INSERT INTO secciones_x_perfiles (idSeccion, idPerfil) VALUES (". $osecciones_x_perfiles->getidSeccion() .", ". $osecciones_x_perfiles->getidPerfil() .")");
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return 1;
		    }
		    catch(exception $e)
		    {
			    $cn->RollBack();
			   throw $e;
		    }		
	    }
	    /**
	    *Realiza el UPDATE en la base de datos
	    *@param class osecciones_x_perfiles
	    *@return 1 si ejecuto con exito, 0 si ocurrio un error.
	    */
	    public static function Update($osecciones_x_perfiles)
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir_Trans();
			    $cn->setQuery("UPDATE secciones_x_perfiles SET idSeccion = ". $osecciones_x_perfiles->getidSeccion() .", idPerfil = ". $osecciones_x_perfiles->getidPerfil() ." WHERE ");
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return 1;
		    }
		    catch(exception $e)
		    {
			    $cn->RollBack();
			    throw $e;
		    }		
	    }
	    /**
	    *Realiza el DELETE en la base de datos
	    *@param object osecciones_x_perfiles
	    *@return 1 si ejecuto con exito, 0 si ocurrio un error.
	    */
	    public static function Delete($osecciones_x_perfiles)
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir_Trans();
			    $cn->setQuery("DELETE FROM secciones_x_perfiles WHERE idPerfil = " . $osecciones_x_perfiles);
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return 1;
		    }
		    catch(exception $e)
		    {
			    $cn->RollBack();
			    throw $e;
		    }		
	    }
	    /**
	    *Realiza un SELECT que devuelve todos los registros de secciones_x_perfiles
	    *@return array Assoc, si ocurrio un error devuelve array()
	    */
	    public static function getAll()
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir();
			    $cn->setQuery("SELECT * FROM secciones_x_perfiles");
			    $Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();
			    return $Tabla;
		    }
		    catch(exception $e)
		    {			
			    throw $e;                   
		    }		
	    }
        /**
        *Realiza un SELECT que devuelve todos los registros de secciones_x_perfiles
        *@return array Assoc, si ocurrio un error devuelve array()
        */
        public static function getAllObj()
        {        
            $cn = Conexion::nuevo();
            try
            {
                $cn->Abrir();
                $cn->setQuery("SELECT * FROM secciones_x_perfiles");
                $Tabla = $cn->DevolverQuery();
                $osecciones_x_perfiles[] = array();
                foreach($Tabla as $Fila)
                {
                    $osecciones_x_perfiles[] = new Secciones_x_perfiles();
                    				$osecciones_x_perfiles[count($oSecciones_x_perfiles)-1]->setidSeccion($Fila['idSeccion']);
				$osecciones_x_perfiles[count($oSecciones_x_perfiles)-1]->setidPerfil($Fila['idPerfil']);

                }
                $cn->Cerrar();
                return $osecciones_x_perfiles;
            }
            catch(exception $e)
            {            
                throw $e;                  
            }        
        }
	    /**
	    *Realiza un SELECT que devuelve UN registros de secciones_x_perfiles por 
	    *@param  condicion a buscar
	    *@return class clssecciones_x_perfiles, si ocurrio un error devuelve class clssecciones_x_perfiles vacio
	    */
        public static function getById()
        {        
            $cn = Conexion::nuevo();
            $osecciones_x_perfiles = new Secciones_x_perfiles();
            try
            {
                $cn->Abrir();
                $cn->setQuery("SELECT * FROM secciones_x_perfiles WHERE ");
                $Tabla = $cn->DevolverQuery();
                if ($Tabla)
                {
                    $osecciones_x_perfiles->setidSeccion($Tabla[0]['idSeccion']);
                $osecciones_x_perfiles->setidPerfil($Tabla[0]['idPerfil']);

                
                }
                $cn->Cerrar();            
            }
            catch(exception $e)
            {            
                throw $e;
            }
            return $osecciones_x_perfiles;
        }
        public static function getByIdPerfil($IdPerfil)
        {        
            $cn = Conexion::nuevo();
            try
            {
                $cn->Abrir();
                $cn->setQuery("SELECT idSeccion FROM secciones_x_perfiles WHERE idPerfil = " . $IdPerfil);
                $Tabla = $cn->DevolverQuery();
                $cn->Cerrar();            
            }
            catch(exception $e)
            {            
                throw $e;
            }
            return $Tabla;
        }        
    }
?>
