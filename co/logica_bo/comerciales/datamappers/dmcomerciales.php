<?php
    class dmComerciales
    {
        public static function getAll($columna = 'nombre', $sentido = 'asc', $filtro="")
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT b.IdComercial, b.Nombre FROM comerciales b ".($filtro != '' ? "WHERE b.nombre LIKE \"%".$filtro."%\"" : '')." ORDER BY " . $columna . " " . $sentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }


        public static function save($oLocal)
        {
            $oConexion = Conexion::nuevo();
            try
            {                                    
                $oConexion->Abrir_Trans();
                if ($oLocal->getIdComercial() == "") {
                    $oConexion->setQuery("INSERT INTO comerciales (nombre) VALUES('".htmlspecialchars(mysql_real_escape_string($oLocal->getNombre()), ENT_QUOTES)."')");
                } else
                    $oConexion->setQuery("UPDATE comerciales SET nombre = '".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($oLocal->getNombre())), ENT_QUOTES)."' WHERE idcomercial = " . $oLocal->getIdComercial());
                $oConexion->EjecutarQuery();
                                
                if($oLocal->getIdComercial() == "")               	
                	$id = $oConexion->LastId();
                else
                	$id = $oLocal->getIdComercial();
             
                $oConexion->Cerrar_Trans();
                return $id;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }            
        }
        
                
        public static function getById($idLocal)
        {
            $oConexion = Conexion::nuevo();
            $oLocal = new Comerciales();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM comerciales WHERE idComercial = " . $idLocal);
                $Fila = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                
                    $oLocal->setIdComercial($Fila[0]["IdComercial"]);
                    $oLocal->setNombre($Fila[0]["Nombre"]);

                }
                catch(MySQLException $e)
                {
                    throw $e;
                }
                return $oLocal;
            }
            
       	public static function delete($obj)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("DELETE FROM comerciales WHERE id = ".$obj->getIdComercial());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                
                
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>
