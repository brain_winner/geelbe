<?php
    class Comerciales
    {
        private $_idcomercial;
        private $_nombre;
       
        public function __construct()
        {
            $this->_idcomercial = "";
            $this->_nombre = "";
        }
        public function getIdComercial()
        {
            return $this->_idcomercial;
        }
        public function setIdComercial($idlocal)
        {
            $this->_idcomercial = $idlocal;
        }
        public function getNombre()
        {
            return $this->_nombre;
        }
        public function setNombre($idlocal)
        {
            $this->_nombre = $idlocal;
        }
    }
?>
