<html>
    <head>
    </head>
    <body>
<?php
    if($_POST)
    {	
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/comerciales/clases/clscomerciales.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/comerciales/datamappers/dmcomerciales.php");
        extract($_POST);
        try
        {   
            $oLocal = new Comerciales();

            if(isset($idComercial) && is_numeric($idComercial))
            	$oLocal->setIdComercial($idComercial);
            	
            $oLocal->setNombre($txtNombre);
            $id = dmComerciales::save($oLocal);
            ?>
                <script type="text/javascript">
                    window.location.href = '../../bo/comerciales/index.php';
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                    window.parent.ErrorMSJ("<?=$e->getNumeroSQLERROR()?>", "");
                </script>
            <?
        }
        ob_flush();
    }
?>
    </body>
</html>
