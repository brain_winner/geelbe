<html>
<head>
</head>
<body>
<?php
    if($_GET)
    {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/comerciales/clases/clscomerciales.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/comerciales/datamappers/dmcomerciales.php");
        extract($_GET);
        try
        {   
            $objCampania = new Comerciales();
            $objCampania->setIdComercial($IdComercial);
            dmComerciales::delete($objCampania);
            ?>
                <script type="text/javascript">
                    window.open("../../../bo/comerciales/index.php?ok=1", "_self");
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                    window.open("../../../bo/comerciales/index.php?error=<?=$e->getNumeroSQLERROR()?>", "_self");
                </script>
            <?
        }
        ob_flush();
    }
?>
</body>
</html>