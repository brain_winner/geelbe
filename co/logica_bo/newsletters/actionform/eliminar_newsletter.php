<html>
<head>
</head>
<body>
<?php
    if($_GET)
    {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/newsletters/clases/clsnewsletters.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/newsletters/datamappers/dmnewsletters.php");
        extract($_GET);
        try
        {   
            $objCampania = new Newsletters();
            $objCampania->setIdNewsletter($IdNewsletter);
            dmNewsletters::delete($objCampania);
            ?>
                <script type="text/javascript">
                    window.open("../../../bo/newsletters/index.php?ok=1", "_self");
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                    window.open("../../../bo/newsletters/index.php?error=<?=$e->getNumeroSQLERROR()?>", "_self");
                </script>
            <?
        }
        ob_flush();
    }
?>
</body>
</html>