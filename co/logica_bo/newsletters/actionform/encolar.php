<?php
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/newsletters/clases/clsnewsletters.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/newsletters/datamappers/dmnewsletters.php");
	      require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
		  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
		  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
        
        try
        {   
	        $oConexion = Conexion::nuevo();
			try {
				$oConexion->Abrir_Trans();
				$oConexion->setQuery("SELECT id FROM newsletters WHERE enviado = 0 AND fechaenvio <= NOW()");
				$news = $oConexion->DevolverQuery();
				$oConexion->Cerrar_Trans();
			}
			catch (MySQLException $e) {
				$oConexion->RollBack();
				throw $e;
			}

			foreach($news as $n) {
			
	           $oLocal = dmNewsletters::getById($n['id']);
	           $sent = EmailSender::sendNewsletter($oLocal);
	           
	        }
	           
        }
        catch(MySQLException $e)
        {
            ?>
		<html>
		    <head>
		    </head>
		    <body>test
                <script>
                    window.parent.ErrorMSJ("<?=$e->getNumeroSQLERROR()?>", "");
                </script>
		    </body>
		</html>
            <?
        }
?>
