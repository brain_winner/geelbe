<html>
    <head>
    </head>
    <body>
<?php
    if($_POST)
    {	
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/newsletters/clases/clsnewsletters.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/newsletters/datamappers/dmnewsletters.php");
        extract($_POST);
        try
        {   
            $oLocal = new Newsletters();

            if(isset($idNewsletter) && is_numeric($idNewsletter))
            	$oLocal->setIdNewsletter($idNewsletter);
            else
            	$oLocal->setCreado(date("Y-m-d H:i:s"));
            	
            $oLocal->setAsunto(stripslashes($txtAsunto));
            $oLocal->setCuerpo(stripslashes($txtCuerpo));
            $oLocal->setFechaEnvio($txtFechaEnvio);
            $oLocal->setTest(isset($_POST['test']));
            $oLocal->setHuerfanos(isset($_POST['huerfanos']));
            $oLocal->setInactivos(isset($_POST['inactivos']));
            $oLocal->setEnviado(!isset($_POST['enviar']));
            
            if(isset($_POST['enviar']))
            	$oLocal->setMaxUserId(null);
            	
            $id = dmNewsletters::save($oLocal);
            ?>
                <script type="text/javascript">
                    window.location.href = '../../bo/newsletters/index.php';
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                    window.parent.ErrorMSJ("<?=$e->getNumeroSQLERROR()?>", "");
                </script>
            <?
        }
        ob_flush();
    }
?>
    </body>
</html>
