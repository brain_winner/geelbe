<?php
    class dmNewsletters
    {
        public static function getAll($columna = 'nombre', $sentido = 'asc', $filtro="")
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT n.id, n.Asunto, n.fechaenvio 'Envio', n.Creado, IF(n.enviado = 0, 'Si', 'No') Enviar FROM newsletters n ".($filtro != '' ? "WHERE n.asunto LIKE \"%".$filtro."%\"" : '')." ORDER BY " . $columna . " " . $sentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }

        public static function save($oLocal)
        {
            $oConexion = Conexion::nuevo();
            try
            {                                    
                $oConexion->Abrir_Trans();
                if ($oLocal->getIdNewsletter() == "") {
                    $oConexion->setQuery("INSERT INTO newsletters (asunto, cuerpo, creado, fechaenvio, enviado, test, huerfanos, inactivos) VALUES('".mysql_real_escape_string($oLocal->getAsunto())."', '".mysql_real_escape_string($oLocal->getCuerpo())."', '".mysql_real_escape_string($oLocal->getCreado())."', '".mysql_real_escape_string($oLocal->getFechaEnvio())."', '".$oLocal->getEnviado()."', '".$oLocal->getTest()."', '".$oLocal->getHuerfanos()."', '".$oLocal->getInactivos()."')");
                } else
                    $oConexion->setQuery("UPDATE newsletters SET asunto = '".mysql_real_escape_string($oLocal->getAsunto())."', cuerpo = '".mysql_real_escape_string($oLocal->getCuerpo())."', fechaenvio = '".mysql_real_escape_string($oLocal->getFechaEnvio())."', enviado = '".$oLocal->getEnviado()."', test = '".$oLocal->getTest()."', huerfanos = '".$oLocal->getHuerfanos()."', inactivos = '".$oLocal->getInactivos()."' ".($oLocal->getMaxUserId() != null ? ", IdUsuarioMax = '".$oLocal->getMaxUserId()."'" : ', IdUsuarioMax = NULL')." WHERE id = " . $oLocal->getIdNewsletter());
                $oConexion->EjecutarQuery();
                                
                if($oLocal->getIdNewsletter() == "")               	
                	$id = $oConexion->LastId();
                else
                	$id = $oLocal->getIdNewsletter();

                $oConexion->Cerrar_Trans();
                return $id;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }            
        }
        
                
        public static function getById($idLocal)
        {
            $oConexion = Conexion::nuevo();
            $oLocal = new Newsletters();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM newsletters WHERE id = " . $idLocal);
                $Fila = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                
                    $oLocal->setIdNewsletter($Fila[0]["id"]);
                    $oLocal->setAsunto($Fila[0]["asunto"]);
                    $oLocal->setCuerpo($Fila[0]["cuerpo"]);
                    $oLocal->setCreado($Fila[0]["creado"]);
                    $oLocal->setFechaEnvio($Fila[0]["fechaenvio"]);
                    $oLocal->setEnviado($Fila[0]["enviado"]);
                    $oLocal->setMaxUserId($Fila[0]["IdUsuarioMax"]);
                    $oLocal->setTest($Fila[0]["test"]);
                    $oLocal->setHuerfanos($Fila[0]["huerfanos"]);
                    $oLocal->setInactivos($Fila[0]["inactivos"]);

                }
                catch(MySQLException $e)
                {
                    throw $e;
                }
                return $oLocal;
            }
            
       	public static function delete($obj)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("DELETE FROM newsletters WHERE id = ".$obj->getIdNewsletter());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                                
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>
