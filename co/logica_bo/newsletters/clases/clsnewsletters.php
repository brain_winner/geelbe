<?php
    class Newsletters
    {
        private $_idnewsletter;
        private $_asunto;
        private $_cuerpo;
        private $_fechaenvio;
        private $_creado;
        private $_enviado;
        private $_maxuserid;
        private $_test;
        private $_inactivos;
        private $_huerfanos;
       
        public function __construct()
        {
            $this->_idnewsletter = "";
            $this->_ausnto = "";
            $this->_cuerpo = "";
            $this->_fechaenvio = "";
            $this->_creado = "";
            $this->_enviado = 0;
            $this->_test = 1;
            $this->_huerfanos = 0;
            $this->_inactivos = 0;
            $this->_maxuserid = null;
        }
        public function getIdNewsletter()
        {
            return $this->_idnewsletter;
        }
        public function setIdNewsletter($idlocal)
        {
            $this->_idnewsletter = $idlocal;
        }
        public function getMaxUserId()
        {
            return $this->_maxuserid;
        }
        public function setMaxUserId($idlocal)
        {
            $this->_maxuserid = $idlocal;
        }
        public function getTest()
        {
            return $this->_test;
        }
        public function setTest($idlocal)
        {
            $this->_test = $idlocal;
        }
        public function getHuerfanos()
        {
            return $this->_huerfanos;
        }
        public function setHuerfanos($idlocal)
        {
            $this->_huerfanos = $idlocal;
        }
        public function getInactivos()
        {
            return $this->_inactivos;
        }
        public function setInactivos($idlocal)
        {
            $this->_inactivos = $idlocal;
        }
        public function getAsunto()
        {
            return $this->_asunto;
        }
        public function setAsunto($idlocal)
        {
            $this->_asunto = $idlocal;
        }
        public function getCuerpo()
        {
            return $this->_cuerpo;
        }
        public function setCuerpo($descripcion)
        {
            $this->_cuerpo = $descripcion;
        }
        public function getFechaEnvio()
        {
            return $this->_fechaenvio;
        }
        public function setFechaEnvio($descripcion)
        {
            $this->_fechaenvio = $descripcion;
        }
        public function getCreado()
        {
            return $this->_creado;
        }
        public function setCreado($descripcion)
        {
            $this->_creado = $descripcion;
        }
        public function getEnviado()
        {
            return $this->_enviado;
        }
        public function setEnviado($descripcion)
        {
            $this->_enviado = $descripcion;
        }
    }
?>
