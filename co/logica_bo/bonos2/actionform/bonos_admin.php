
<?php
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("bonos2"));
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
	
	    try {
	        ValidarUsuarioLogueadoBo(29);
		}
		catch(Exception $e) {
			header('Location: /'.$confRoot[1].'/bo');
			exit;
	    }	
	    

        try
        {   
        	$c = Conexion::nuevo();
        	$c->Abrir_Trans();
        	$c->setQuery('SELECT IdUsuario FROM usuarios WHERE NombreUsuario = "'.$_REQUEST["email"].'"');
        	$datos = $c->DevolverQuery();
        	$c->Cerrar_Trans();

        	if(count($datos) == 0) {
        		echo 'Error: usuario inexistente.';
        		die;
        	}
        
            $oLocal = new Bonos();
            $oLocal->setIdBono($idBono);
            $oLocal->setIdUsuario($datos[0]['IdUsuario']);
            $new_bonos = $_REQUEST['valor'];
			for ( $i = 1 ; $i <= $new_bonos ; $i ++) {
				dmBonos::save($oLocal, $_REQUEST["idRegla"]);
			}
    	    
            //Invalido la cache del credito de usuario
            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		    $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
		    $cache->remove('cache_credito_usuario_'.$datos[0]['IdUsuario']);
            
            ?>
                Bonos cargados satisfactoriamente.
            <?
        }
        catch(MySQLException $e)
        {
            echo $e;
        }
?>