
<?php
	set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
	require_once 'Zend/Cache.php'; 

    if($_POST)
    {	
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("bonos2"));
        extract($_POST);
        try
        {   
        	$c = Conexion::nuevo();
        	$c->Abrir_Trans();
        	$c->setQuery('SELECT IdUsuario FROM usuarios WHERE NombreUsuario = "'.$txtIdUsuario.'"');
        	$datos = $c->DevolverQuery();
        	$c->Cerrar_Trans();

        	if(count($datos) == 0) {
        		echo 'Error: usuario inexistente.';
        		die;
        	}
        
            $oLocal = new Bonos();
            $oLocal->setIdBono($idBono);
            $oLocal->setIdUsuario($datos[0]['IdUsuario']);
            $new_bonos = $_POST['valor'];
            
            $bonos50000 = ((int) ($new_bonos / 50000));
            $resto50000 = (($new_bonos - ($bonos50000*50000)) < 0)?0:$new_bonos - ($bonos50000*50000);
            
            $bonos5000 = ((int) ($resto50000 / 5000));
            $resto5000 = (($resto50000 - ($bonos5000*5000)) < 0)?0:$resto50000 - ($bonos5000*5000);
            
            $bonos1000 = ((int) ($resto5000 / 1000));
            $resto1000 = (($resto5000 - ($bonos1000*1000)) < 0)?0:$resto5000 - ($bonos1000*1000);
            
            $bonos100 = ((int) ($resto1000 / 100));
            $resto100 = (($resto1000 - ($bonos100*100)) < 0)?0:$resto1000 - ($bonos100*100);
            
			for ( $i = 1 ; $i <= $bonos50000 ; $i++) {
				dmBonos::save($oLocal, 7);
			}
            
			for ( $i = 1 ; $i <= $bonos5000 ; $i++) {
				dmBonos::save($oLocal, 6);
			}
            
			for ( $i = 1 ; $i <= $bonos1000 ; $i++) {
				dmBonos::save($oLocal, 5);
			}
            
			for ( $i = 1 ; $i <= $bonos100 ; $i++) {
				dmBonos::save($oLocal, 4);
			}

			for ( $i = 1 ; $i <= $resto100; $i ++) {
				dmBonos::save($oLocal);
			}
    	    
            //Invalido la cache del credito de usuario
            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		    $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
		    $cache->remove('cache_credito_usuario_'.$datos[0]['IdUsuario']);
            
            ?>
                <script type="text/javascript">
                    window.irVolver();
                </script>
            <?
        }
        catch(MySQLException $e) {
            echo $e;
            exit;
        }
        ob_flush();
    }
?>