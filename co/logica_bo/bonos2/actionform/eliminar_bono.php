<html>
<head>
</head>
<body>
<?php
    if($_GET)
    {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("bonos2"));
        
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        
        extract($_GET);
        try
        {   
            $objCampania = new Bonos();
            $objCampania->setIdBono($IdBono);
            dmBonos::delete($objCampania);
    	    
            //Invalido la cache del credito de usuario
            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		    $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
		    $cache->remove('cache_credito_usuario_'.$_GET['IdUsuario']);
            
            ?>
                <script type="text/javascript">
                    window.open("../../../bo/bonos2/index.php?ok=1", "_self");
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                    window.open("../../../bo/bonos2/index.php?error=<?=$e->getNumeroSQLERROR()?>", "_self");
                </script>
            <?
        }
        ob_flush();
    }
?>
</body>
</html>