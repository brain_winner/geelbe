<?    
	 
abstract class clsMailer{

    	protected $arrEmail;
    	protected $msj;
    	protected $id;
    	protected $arrDatos;
    	protected $nombreEmail;


     	protected function __construct($email, $id){
     		$this->nombreEmail = $email;
     		$this->id = $id;
     		$this->nombreMail($email);
			$this->obtenerDatosUsuario();
			$this->reemplazarString();
		//	$this->verificarMailUsuario();
     	}

     	protected function nombreMail($email){
			$this->arrEmail = Aplicacion::getEmail($email);
     	}
		abstract protected function obtenerDatosUsuario(){

		}
		abstract protected function reemplazarString(){

		}
//		protected function verificarMailUsuario(){}

		abstract protected function enviarMail(){

		}

     	public function enviar(){
			$this->enviarMail();
    	}

    }

	class clsMailerPedidos extends clsMailer{
		protected function obtenerDatosUsuario(){
			$this->arrDatos = dmPedidos::getDatosMailByIdPedido($this->id);

		}		

		protected function reemplazarString(){
	 		$oTexto = new Texto(Aplicacion::getRoot() . $this->arrEmail["url"]);
	 		$oTexto->Read();
			$oTexto->ReemplazarString("%NombreVidriera%",$this->arrDatos["nombreVidriera"]);
			$oTexto->ReemplazarString("%NumeroPedido%",str_pad($this->arrDatos["nroPedido"], 8, 0, 0));
			$oTexto->ReemplazarString("%EstadoPedido%",$this->arrDatos["estadoPedido"]);
			$oTexto->ReemplazarString("%numerotrackingdhl%", $this->arrDatos["numeroTrackingDhl"]);
			$oTexto->ReemplazarString("%NombreUsuario%", $this->arrDatos["nombreUsuario"]);
			$oTexto->ReemplazarString("%ApellidoUsuario%", $this->arrDatos["apellidoUsuario"]);
			$oTexto->ReemplazarString("%EmailUsuario%", $this->arrDatos["emailUsuario"]);
			$oTexto->ReemplazarString("%NombrePadrino%", $this->arrDatos["nombrePadrino"]);
			$oTexto->ReemplazarString("%ApellidoPadrino%", $this->arrDatos["apellidoPadrino"]);
			$oTexto->ReemplazarString("%EmailPadrino%", $this->arrDatos["emailPadrino"]);
			$oTexto->ReemplazarString("%Direccion%", $this->arrDatos["direccion"]);
			$oTexto->ReemplazarString("%Departamento%", $this->arrDatos["departamento"]);
			$oTexto->ReemplazarString("%Ciudad%", $this->arrDatos["ciudad"]);
			$oTexto->ReemplazarString("%TiempoEntregaFn%", date("d/m", strtotime($this->arrDatos["TiempoEntregaFn"])));
			$oTexto->ReemplazarString("%TiempoEntregaIn%", date("d/m", strtotime($this->arrDatos["TiempoEntregaIn"])));
			$oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
			$oTexto->ReemplazarString("%year%", date("Y"));
			$this->msj = $oTexto->getText();
			str_ireplace("%NombreUsuario%",$this->arrDatos["nombreUsuario"],$this->arrEmail["asunto"]);
			str_ireplace("%NombrePadrino%",$this->arrDatos["nombrePadrino"],$this->arrEmail["asunto"]);
	     }


	    protected function enviarMail(){
	    
            $emailBuilder = new EmailBuilder();
	        $emailData = $emailBuilder->generateAnEmail($arrDatos["emailUsuario"], $this->msj, $this->nombreEmail, $arrEmail["asunto"]);
	        
			$emailSender = new EmailSender();
			$emailSender->sendEmail($emailData);
	    	
			return true;
	 
		}
		
		public function enviarPadrino($email){
			$this->nombreMail($email);
			$this->reemplarString();

			$emailBuilder = new EmailBuilder();
	        $emailData = $emailBuilder->generateAnEmail($arrDatos["emailPadrino"], $this->msj, $this->nombreEmail, $arrEmail["asunto"]);
	        
			$emailSender = new EmailSender();
			$emailSender->sendEmail($emailData);
	    	
			return true;
		}
	}
	?>