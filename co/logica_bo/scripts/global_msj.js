var arrErrores = new Array();
arrErrores["USUARIOS"]=new Array();
arrErrores["USUARIOS"]["MAIL_INCORRECTO"]="El mail ingresado no tiene un formato correcto.";
arrErrores["USUARIOS"]["SELECCIONE"]="Elija un registro por favor.";
arrErrores["USUARIOS"]["NOMBRE"]="Ingrese un nombre.";
arrErrores["USUARIOS"]["APELLIDO"]="Ingrese un apellido.";
arrErrores["USUARIOS"]["CLAVE_INCORRECTO"]= "Su Clave es Incorrecta. Debe contener de 8 a 16 digitos.";
arrErrores["USUARIOS"]["CLAVE_DIFERENTES"] = "La contrase�a debe coincidir con repetir contrase�a.";
arrErrores["USUARIOS"]["MAIL_DIFERENTES"] = "Los emails deben coincidir.";
arrErrores["USUARIOS"]["PERFIL"] = "Seleccione un perfil.";
arrErrores["USUARIOS"]["1452"] = "El email ingresado ya se ecuentra registrado.\nPor favor ingrese otro.";
arrErrores["USUARIOS"]["1062"] = "El email ingresado ya se ecuentra registrado.\nPor favor ingrese otro.";
arrErrores["USUARIOS"]["EXITO"] = "Los datos se han actualizado correctamente.";
arrErrores["USUARIOS"]["ELIMINAR"] = "ADVERTENCIA:Los datos eliminados no podran ser recuperados.\n�Desea continuar?";
arrErrores["USUARIOS"]["FECHA DE NACIMIENTO"] = "La Fecha de nacimiento es requerida.";

arrErrores["CAMPANIAS"]=new Array();
arrErrores["CAMPANIAS"]["SELECCIONE"] = "Elija una campa�a por favor.";
arrErrores["CAMPANIAS"]["NOMBRE"]="Ingrese un nombre.";
arrErrores["CAMPANIAS"]["FECHA DE INICIO"] = "La Fecha de inicio es requerida.";
arrErrores["CAMPANIAS"]["FECHA DE FIN"] = "La Fecha de fin es requerida.";
arrErrores["CAMPANIAS"]["VISIBLE DESDE"] = "La Fecha visible desde es requerida.";
arrErrores["CAMPANIAS"]["MAXPROD"] = "Maximos productos a vender debe ser mayor a cero.";
arrErrores["CAMPANIAS"]["1452"] = "El email ingresado ya se ecuentra registrado.\nPor favor ingrese otro.";
arrErrores["CAMPANIAS"]["1451"] = "La campa�a seleccionada no se puede eliminar porque contiene datos relacionados.";
arrErrores["CAMPANIAS"]["1062"] = "El email ingresado ya se ecuentra registrado.\nPor favor ingrese otro.";
arrErrores["CAMPANIAS"]["EXITO"] = "Los datos se han actualizado correctamente.";
arrErrores["CAMPANIAS"]["ELIMINAR"] = "ADVERTENCIA:Los datos eliminados no podran ser recuperados.\n�Desea continuar?";

arrErrores["CATEGORIAS"]=new Array();
arrErrores["CATEGORIAS"]["NOMBRE"]="Ingrese un nombre.";
arrErrores["CATEGORIAS"]["ORDEN"]="Ingrese un numero de orden para la categoria.";
arrErrores["CATEGORIAS"]["SELECCIONE"] = "Elija una categoria por favor.";
arrErrores["CATEGORIAS"]["1452"] = "El email ingresado ya se ecuentra registrado.\nPor favor ingrese otro.";
arrErrores["CATEGORIAS"]["1062"] = "El email ingresado ya se ecuentra registrado.\nPor favor ingrese otro.";
arrErrores["CATEGORIAS"]["EXITO"] = "Los datos se han actualizado correctamente.";
arrErrores["CATEGORIAS"]["ELIMINAR"] = "ADVERTENCIA:Los datos eliminados no podran ser recuperados.\n�Desea continuar?";
arrErrores["CATEGORIAS"]["SELECCIONE_PRODUCTO"] = "Seleccione un producto.";
arrErrores["CATEGORIAS"]["PRODUCTO_EXISTE"]= "El producto seleccionado ya se agrego.";
arrErrores["CATEGORIAS"]["ELIMINAR_PRODUCTOS"] = "ADVERTENCIA:�Desea realmente eliminar el producto de la categoria?"

arrErrores["MARCAS"]=new Array();
arrErrores["MARCAS"]["NOMBRE"]="Ingrese un nombre.";
arrErrores["MARCAS"]["SELECCIONE"] = "Elija una marca por favor.";
arrErrores["MARCAS"]["1452"] = "La marca ingresada ya se encuetra registrada.\nPor favor ingrese otra.";
arrErrores["MARCAS"]["1062"] = "La marca ingresada ya se encuetra registrada.\nPor favor ingrese otra.";
arrErrores["MARCAS"]["EXITO"] = "Los datos se han actualizado correctamente.";
arrErrores["MARCAS"]["ELIMINAR"] = "ADVERTENCIA:Los datos eliminados no podran ser recuperados.\n�Desea continuar?";

arrErrores["PROVEEDORES"]=new Array();
arrErrores["PROVEEDORES"]["NOMBRE"]="Ingrese un nombre.";
arrErrores["PROVEEDORES"]["SELECCIONE"] = "Elija un proveedor por favor.";
arrErrores["PROVEEDORES"]["1452"] = "La marca ingresada ya se encuetra registrada.\nPor favor ingrese otra.";
arrErrores["PROVEEDORES"]["1062"] = "La marca ingresada ya se encuetra registrada.\nPor favor ingrese otra.";
arrErrores["PROVEEDORES"]["EXITO"] = "Los datos se han actualizado correctamente.";
arrErrores["PROVEEDORES"]["ELIMINAR"] = "ADVERTENCIA:Los datos eliminados no podran ser recuperados.\n�Desea continuar?";

arrErrores["CLASES"]=new Array();
arrErrores["CLASES"]["NOMBRE"]="Ingrese un nombre.";
arrErrores["CLASES"]["ELIMINAR_ATRIBUTO"]="ADVERTENCIA:Los datos eliminados no podran ser recuperados.\n�Desea continuar?";
arrErrores["CLASES"]["IMPUESTO"]="Seleccione un impuesto.";
arrErrores["CLASES"]["ATRIBUTOS_0"]="Ingrese al menos un atributo.";
arrErrores["CLASES"]["IMPUESTOS_0"]="Ingrese al menos un impuesto.";
arrErrores["CLASES"]["PARAMETRO_NOMBRE"]="Ingrese un nombre.";
arrErrores["CLASES"]["PARAMETRO_NOMBRE_EXISTE"]="El nombre ingresado ya existe.";
arrErrores["CLASES"]["SELECCIONE"] = "Elija un proveedor por favor.";
arrErrores["CLASES"]["SELECCIONE_IMPUESTO"] = "Elija un impuesto por favor.";
arrErrores["CLASES"]["IMPUESTO_EXISTE"]="El impuesto ya existe.";
arrErrores["CLASES"]["1452"] = "La marca ingresada ya se encuetra registrada.\nPor favor ingrese otra.";
arrErrores["CLASES"]["1062"] = "La marca ingresada ya se encuetra registrada.\nPor favor ingrese otra.";
arrErrores["CLASES"]["EXITO"] = "Los datos se han actualizado correctamente.";
arrErrores["CLASES"]["ELIMINAR"] = "ADVERTENCIA:Los datos eliminados no podran ser recuperados.\n�Desea continuar?";

arrErrores["IMPUESTOS"]=new Array();
arrErrores["IMPUESTOS"]["NOMBRE"]="Ingrese un nombre.";
arrErrores["IMPUESTOS"]["TIPO"]="Seleccione el tipo de impuesto.";
arrErrores["IMPUESTOS"]["SELECCIONE"] = "Elija una impuesto por favor.";
arrErrores["IMPUESTOS"]["1452"] = "La impuesto ingresada ya se encuetra registrada.\nPor favor ingrese otra.";
arrErrores["IMPUESTOS"]["1062"] = "La impuesto ingresada ya se encuetra registrada.\nPor favor ingrese otra.";
arrErrores["IMPUESTOS"]["EXITO"] = "Los datos se han actualizado correctamente.";
arrErrores["IMPUESTOS"]["ELIMINAR"] = "ADVERTENCIA:Los datos eliminados no podran ser recuperados.\n�Desea continuar?";

arrErrores["BONOS"]=new Array();
arrErrores["BONOS"]["SELECCIONE"]="Seleccione el bono.";

arrErrores["PRODUCTOS"]=new Array();
arrErrores["PRODUCTOS"]["SELECCIONE_PROVEEDOR"]="Seleccione un proveedor.";
arrErrores["PRODUCTOS"]["NOMBRE"]="Ingrese un nombre.";
arrErrores["PRODUCTOS"]["INTERNO"]="Por favor ingrese el stock del producto.";
arrErrores["PRODUCTOS"]["MARCA"]="Seleccione una marca.";
arrErrores["PRODUCTOS"]["CLASE"]="Seleccione una clase.";
arrErrores["PRODUCTOS"]["ATRIBUTOS_VALOR_VACIO"]=" debe tener un valor.";
arrErrores["PRODUCTOS"]["ELIMINAR_ATRIBUTOS"]="El atributo seleccionado sera eliminado y no podra recuperar los datos.\n�Desea continuar?";
arrErrores["PRODUCTOS"]["ELIMINAR_ATRIBUTOS_CONFIRMADO"]="El atributo ha sido eliminado. Para confirmar la eliminacion debe guardar los datos.\nTenga en cuenta que si el atributo tiene movimientos asociados, este no sera eliminado.";

arrErrores["PEDIDOS"]=new Array();
arrErrores["PEDIDOS"]["FECHA_ENVIO"] = "Fecha de envio incorrecta.";
arrErrores["PEDIDOS"]["NROENVIO"] = "Ingrese un numero de envio.";
arrErrores["PEDIDOS"]["ANULAR"] = "�Realmente desea anular el pedido?\nADVERTENCIA:Una vez anulado no se podra realizar ninguna operacion mas.";
arrErrores["PEDIDOS"]["ENTREGAR"] = "ADVERTENCIA: Si elige entregar el pedido no se podra realizar ninguna modificacion.\n�Desea continuar?";
arrErrores["PEDIDOS"]["MOTIVO"] = "Ingrese el motivo.";
arrErrores["PEDIDOS"]["FECHAINVALIDA"] = "La Fecha Ingresada es invalida";