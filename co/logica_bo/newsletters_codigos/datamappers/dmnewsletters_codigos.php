<?php
    class dmNewsletterCodigos
    {
        public static function getAll($columna = 'nombre', $sentido = 'asc', $filtro="")
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT id, Nombre FROM newsletter_codigos ".($filtro != '' ? "WHERE nombre LIKE \"%".$filtro."%\"" : '')." ORDER BY " . $columna . " " . $sentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                /*$arrLocales = array();
                foreach($Tabla as $Fila)
                {
                    $arrLocales[] = new Locales();
                    $arrLocales[count($arrLocales) - 1]->setIdLocal($Fila["idLocal"]);
                    $arrLocales[count($arrLocales) - 1]->setDescripcion($Fila["Descripcion"]);
                    $arrLocales[count($arrLocales) - 1]->setDireccion($Fila["Direccion"]);
                    $arrLocales[count($arrLocales) - 1]->setIdProvincia($Fila["idProvincia"]);
                }
                return $arrLocales;*/
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        public static function save($oLocal)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                if ($oLocal->getIdCodigo() == "")
                    $oConexion->setQuery("INSERT INTO newsletter_codigos (Nombre) VALUES('".htmlspecialchars(mysql_real_escape_string($oLocal->getNombre()), ENT_QUOTES)."')");
                else
                    $oConexion->setQuery("UPDATE newsletter_codigos SET Nombre = '".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($oLocal->getNombre())), ENT_QUOTES)."' WHERE id = " . $oLocal->getIdCodigo());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }            
        }
        public static function getById($idLocal)
        {
            $oConexion = Conexion::nuevo();
            $oLocal = new NewsletterCodigo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM newsletter_codigos WHERE id = " . $idLocal);
                $Fila = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                
                    $oLocal->setIdCodigo($Fila[0]["id"]);
                    $oLocal->setNombre($Fila[0]["nombre"]);

            }
            catch(MySQLException $e)
            {
                throw $e;
            }
            return $oLocal;
        }
       
        public static function getLinks($idLocal)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("
                
                SELECT c.id as id, c.nombre, v.visitas, v.IdCodigo, c.id as IdCampo FROM newsletter_codigos_campos c 
				LEFT JOIN newsletter_codigos_visitas v ON c.Id = v.IdCampo
				WHERE v.idCodigo = $idLocal

				UNION
				
				SELECT c.id as id, c.nombre, 0 as visitas, $idLocal as IdCodigo, c.id as IdCampo FROM newsletter_codigos_campos c 
				
				ORDER BY id ASC, visitas DESC");

                $oLocal = $oConexion->DevolverQuery();

            }
            catch(MySQLException $e)
            {
                throw $e;
            }
            return $oLocal;
        }
            
       	public static function delete($obj)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("DELETE FROM newsletter_codigos WHERE id = ".$obj->getIdCodigo());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function visitar($codigo, $campo) {
        	$oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                                
                $oConexion->setQuery("SELECT id FROM newsletter_codigos_campos WHERE id = ".$campo);
                $data = $oConexion->DevolverQuery();
                if(count($data) == 0) return false;
                
                $oConexion->setQuery("SELECT id FROM newsletter_codigos WHERE id = ".$codigo);
                $data = $oConexion->DevolverQuery();
                if(count($data) == 0) return false;
                
                $oConexion->setQuery("SELECT id FROM newsletter_codigos_visitas WHERE idCodigo = ".$codigo." AND IdCampo = ".$campo);
                $data = $oConexion->DevolverQuery();
                
                if(count($data) == 0) {
               		$oConexion->setQuery("INSERT INTO newsletter_codigos_visitas VALUES ('', ".$codigo.", ".$campo.", 1)");
                } else {
                	$oConexion->setQuery("UPDATE newsletter_codigos_visitas SET visitas = visitas + 1 WHERE idCodigo = ".$codigo." AND IdCampo = ".$campo);
                }
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>
