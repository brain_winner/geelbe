<?php
    class NewsletterCodigo
    {
        private $_idcodigo;
        private $_nombre;
       
        public function __construct()
        {
            $this->_idcodigo = "";
            $this->_nombre = "";
        }
        public function getIdCodigo()
        {
            return $this->_idcodigo;
        }
        public function setIdCodigo($idlocal)
        {
            $this->_idcodigo = $idlocal;
        }
        public function getNombre()
        {
            return $this->_nombre;
        }
        public function setNombre($descripcion)
        {
            $this->_nombre = $descripcion;
        }
    }
?>
