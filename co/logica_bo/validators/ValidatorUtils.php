<?

class ValidatorUtils {

	public static function validateNumber($value)  {
		return is_numeric($value);
	} 

	public static function validateEmail($value)  {
		return self::check_email_address($value);
	} 
	
	// Input Format: yyyy-mm-dd
	public static function validateDate($date) {
		list($year,$month,$day) = explode("-",$date);
		return checkdate($month, $day, $year);
	}
	
	// Input Format: yyyy-mm-dd
	public static function validteOlderUser($date, $ageLimit) {
	    list($year,$month,$day) = explode("-",$date);
	    $year_diff  = date("Y") - $year;
	    $month_diff = date("m") - $month;
	    $day_diff   = date("d") - $day;
	    if ($day_diff < 0 || $month_diff < 0)
	      $year_diff--;
	    return $year_diff >= $ageLimit;
	}
	
	public static function is_valid_email($email) {
		$regexp = '/^[^\x00-\x20()<>@,;:\\".[\]\x7f-\xff]+(?:\.[^\x00-\x20()<>@,;:\\".[\]\x7f-\xff]+)*\@[^\x00-\x20()<>@,;:\\".[\]\x7f-\xff]+(?:\.[^\x00-\x20()<>@,;:\\".[\]\x7f-\xff]+)+$/i';
		if (preg_match($regexp, $email)) {
			return true;
		}
		return false;
	} 
  
   public static function check_email_address($email) {
	    // First, we check that there's one @ symbol, and that the lengths are right
	    if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
	        // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
	        return false;
	    }
	    // Split it into sections to make life easier
	    $email_array = explode("@", $email);
	    $local_array = explode(".", $email_array[0]);
	    for ($i = 0; $i < sizeof($local_array); $i++) {
	         if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
	            return false;
	        }
	    }    
	    if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
	        $domain_array = explode(".", $email_array[1]);
	        if (sizeof($domain_array) < 2) {
	                return false; // Not enough parts to domain
	        }
	        for ($i = 0; $i < sizeof($domain_array); $i++) {
	            if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
	                return false;
	            }
	        }
	    }
	    return true;
	}
	
	public static function checkFields($fields, $where, $redirect = true) {
		//Checks all fields in array
		try {
		
			foreach($fields as $i => $field) {

				//Get field name
				$start = strpos($i, '[');
				if($start !== false) { //field is inside array
					$key1 = substr($i, 0, $start);
					$key2 = substr($i, $start+1, -1);
					
					if(!isset($where[$key1]) || !isset($where[$key1][$key2]))
						throw new Exception($key2.' not defined');
					else					
						$value = $where[$key1][$key2];
						
				} elseif(!isset($where[$i]))
					throw new Exception($i.' not defined');
				else {
					$value = $where[$i];				
					$key2 = $i;
				}

				//Content validation
				if(!ValidatorUtils::check($value, $field['check']))
					throw new Exception($field['exception']);
			
			}	
			
		} catch(Exception $e) {

			if($redirect) {
				header('HTTP/1.1 404 Not Found');
				include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
				exit;
			} else {
				echo '<script>
                    window.parent.msjError("'.$field['exception'].'", "Verifique los datos ingresados");
                </script>';
                exit;
			}

		}
		
	}
	
	public static function check($value, $type) {

		$ok = true;
		switch($type) {
		
			case 'numeric':
			case 'dni':
			case 'year':
				$ok = ValidatorUtils::validateNumber($value);
			break;
			
			case 'month':
				$ok = (ValidatorUtils::validateNumber($value) && $value >= 1 && $value <= 12);
			break;
			
			case 'day':
				$ok = (ValidatorUtils::validateNumber($value) && $value >= 1 && $value <= 31);
			break;
			
			case 'email':
				$ok = ValidatorUtils::validateEmail($value);
			break;			
			
		}

		return $ok;
			
	}

}

?>