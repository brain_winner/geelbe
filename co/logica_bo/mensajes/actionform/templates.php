<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    
    require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php";
    
    if ($_POST) {
        try {
            extract($_POST);
            
            if (strlen($txtTitle)==0) {
            	header('Location: '.Aplicacion::getRootUrl().'bo/templates/crear.php?error=title');
				exit;
            }
        	if (strlen($txtMensaje)==0) {
            	header('Location: '.Aplicacion::getRootUrl().'bo/templates/crear.php?error=message');
				exit;
            }
        
            if(isset($idTemplate)) {
            	dmMensajes::updateTemplate($idTemplate, $txtTitle, $txtMensaje, $txtTipo);
            } 
            else {
            	dmMensajes::getAddTemplate($txtTitle, $txtMensaje, $txtTipo);
            }
            
            header('Location: '.Aplicacion::getRootUrl().'bo/templates/index.php');
			exit;
  
        }
        catch(exception $e) {
            print_r($e);
        }
    }
?>
