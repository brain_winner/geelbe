<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/mensajes/datamappers/dmmensajes.php");
    try {
        ValidarUsuarioLogueadoBo(14);
    } catch(ACCESOException $e) {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
	
	$userId = Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]);
	$saludo = dmMensajes::getSaludosMensajes($userId);
  
  if($saludo == null) {
       $oConexion = Conexion::nuevo();
	   $oConexion->setQuery("INSERT INTO mensajes_saludo (IdUsuario, MensajeSaludo) VALUES (".mysql_real_escape_string($userId).",\"".mysql_real_escape_string($_REQUEST["saludo"])."\")");
	   $oConexion->EjecutarQuery();
  } else {
       $oConexion = Conexion::nuevo();
	   $oConexion->setQuery("UPDATE mensajes_saludo SET MensajeSaludo = \"".mysql_real_escape_string($_REQUEST["saludo"])."\" WHERE IdUsuario=".mysql_real_escape_string($userId));
	   $oConexion->EjecutarQuery();  
  }
	header("Location: /".$confRoot[1]."/bo/mensajes/leer.php?idMensaje=".$_REQUEST["idMensaje"]);
?>