<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    
    require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php";
    
	
	if (isset($_REQUEST["botonResponder"]) || isset($_REQUEST["botonResponderYSiguiente"]) || isset($_REQUEST['botonResponderYCerrar'])) {
		if ($_POST) {
	        try {
			    $userId = Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]);
				$saludo = dmMensajes::getSaludosMensajes($userId);
	        	
	            extract($_POST);
	            $oMensaje = new Mensajes();
	            $oMensaje->setidMensajePadre($txtIdMensajePadre);
	            $oMensaje->setasunto($txtAsunto);
	            $oMensaje->setmensaje($txtMensaje."\n\n".$saludo);
	            $oMensaje->setidmotivo(0);
	            $oMensaje->setrespondido("false");
	            $oMensaje->setcustomercare("true");
	            $oMensaje->setfecha(date("Y-m-d H:i:s"));
	            $oMensaje->setemail("no-reply@geelbe.com");
	            
	            $oMensajeAnt = dmMensajes::getById($txtIdMensajePadre);
	            $oMensajeAnt->setrespondido("true");
				
				if(isset($_REQUEST['botonResponderYCerrar'])) {
	            	$oMensajeAnt->setcerrado("true");
		            $oMensaje->setcerrado("true");
				} else {
	            	$oMensajeAnt->setcerrado("false");
		            $oMensaje->setcerrado("false");
		        }
	        
	            if (isset($_SESSION["BO"]["User"])) {
	            	$oMensaje->setboUserId(Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]));
	            	$oMensajeAnt->setboUserId(Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]));
	            }
	            
	            
	            if (is_null($oMensajeAnt->getboUserId())) {
	            	$oMensajeAnt->setboUserId("null");
	            }
	            
	            dmMensajes::Insert($oMensaje);
	            dmMensajes::Update($oMensajeAnt);
	            
	    		
                $emailBuilder = new EmailBuilder();
	            $emailData = $emailBuilder->generateRespuestaBOEmail($oMensaje, $oMensajeAnt, $strMensajeText);
	            
				$emailSender = new EmailSender();
				$emailSender->sendEmail($emailData);   
	        }
	        catch(exception $e) {
	            print_r($e);
	        }
	    }
	}
	
	if (isset($_REQUEST["botonCerrar"]) || isset($_REQUEST["botonCerrarYSiguiente"])) {
		if ($_POST) {
	        try {
	        	extract($_POST);
	        	
	            $txtMensaje = " --- MOTIVO DE CERRADO --- ".$txtMensaje." --- MOTIVO DE CERRADO --- ";
	            
	            $oMensaje = new Mensajes();
	            $oMensaje->setidMensajePadre($txtIdMensajePadre);
	            $oMensaje->setasunto($txtAsunto);
	            $oMensaje->setmensaje($txtMensaje);
	            $oMensaje->setidmotivo(0);
	            $oMensaje->setrespondido("false");
	            $oMensaje->setcerrado("true");
	            $oMensaje->setcustomercare("true");
	            $oMensaje->setfecha(date("Y-m-d H:i:s"));
	            $oMensaje->setemail("no-reply@geelbe.com");
	            
	            $oMensajeAnt = dmMensajes::getById($txtIdMensajePadre);
	            $oMensajeAnt->setrespondido("false");
	            $oMensajeAnt->setcerrado("true");
	        
	            if (isset($_SESSION["BO"]["User"])) {
	            	$oMensaje->setboUserId(Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]));
	            	$oMensajeAnt->setboUserId(Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]));
	            }
	            
	            if (is_null($oMensajeAnt->getboUserId())) {
	            	$oMensajeAnt->setboUserId("null");
	            }
	            
	            dmMensajes::Insert($oMensaje);
	            dmMensajes::Update($oMensajeAnt);  
	        }
	        catch(exception $e) {
	            print_r($e);
	        }
	    }
	}
	
	
	if (isset($_REQUEST["botonReabrir"])) {
		if ($_POST) {
	        try {
	        	extract($_POST);
	        	
	            $txtMensaje = " --- REABIERTO ".date("Y-m-d H:i:s")." --- ";
	            
	            $oMensaje = new Mensajes();
	            $oMensaje->setidMensajePadre($txtIdMensajePadre);
	            $oMensaje->setasunto($txtAsunto);
	            $oMensaje->setmensaje($txtMensaje);
	            $oMensaje->setidmotivo(0);
	            $oMensaje->setrespondido("false");
	            $oMensaje->setcerrado("false");
	            $oMensaje->setcustomercare("true");
	            $oMensaje->setfecha(date("Y-m-d H:i:s"));
	            $oMensaje->setemail("no-reply@geelbe.com");
	            
	            $cn = Conexion::nuevo();
			    $cn->Abrir();
                $cn->setQuery("UPDATE mensajes SET cerrado = 0 WHERE IdMensaje = ".$txtIdMensajePadre." OR IdMensajePadre = ".$txtIdMensajePadre);
                $cn->EjecutarQuery();
	            	        
	            if (isset($_SESSION["BO"]["User"])) {
	            	$oMensaje->setboUserId(Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]));
	            }
	            
	            dmMensajes::Insert($oMensaje);
	        }
	        catch(exception $e) {
	            print_r($e);
	        }
	    }
	}
	

	if (isset($_REQUEST["botonResponderYSiguiente"]) || isset($_REQUEST["botonCerrarYSiguiente"]) || isset($_REQUEST["botonSiguiente"]) || isset($_REQUEST['botonResponderYCerrar'])) {
		if ($_POST) {
	        try {
	            extract($_POST);
	        	$idMensajeSiguiente = dmMensajes::getNextMensajeId($txtIdMensajePadre, Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]));
	        	
	        	if(!isset($idMensajeSiguiente)) {
	        		$idMensajeSiguiente = dmMensajes::getNextMensajeId(0, Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]));
	        	}
	        	
	        	if(!isset($idMensajeSiguiente)) {
	        		?>
	                <script>
	                    alert("No hay mas mensajes asignados!");
	                </script>
	
	                <script type="text/javascript">
	                    window.location.href="../../../bo/mensajes/index.php?messageType=noasign"; 
	                </script>            
	            <?  
	        	}
	        	
	            ?> 
	                <script type="text/javascript">
	                    window.location.href="../../../bo/mensajes/leer.php?idMensaje=<?=$idMensajeSiguiente;?>"; 
	                </script>            
	            <?     
	        }
	        catch(exception $e) {
	            print_r($e);
	        }
	    }
	}
	
	if (isset($_REQUEST["botonResponder"])) {
		if ($_POST) {
	        try {
	            ?>
	                <script>
	                    alert("se guardaron los datos ok !");
	                </script>
	
	                <script type="text/javascript">
	                    window.location.href="../../../bo/mensajes/index.php"; 
	                </script>            
	            <?     
	        }
	        catch(exception $e) {
	            print_r($e);
	        }
	    }
	}
	
	if (isset($_REQUEST["botonCerrar"])) {
		if ($_POST) {
	        try {
	            ?>
	                <script>
	                    alert("El mensaje se ha cerrado!");
	                </script>
	
	                <script type="text/javascript">
	                    window.location.href="../../../bo/mensajes/index.php"; 
	                </script>            
	            <?     
	        }
	        catch(exception $e) {
	            print_r($e);
	        }
	    }
	}
	
	
	if (isset($_REQUEST["botonReabrir"])) {
		if ($_POST) {
	        try {
	            ?>
	                <script>
	                    alert("El mensaje se ha reabierto!");
	                </script>
	
	                <script type="text/javascript">
	                    window.location.href="../../../bo/mensajes/index.php"; 
	                </script>            
	            <?     
	        }
	        catch(exception $e) {
	            print_r($e);
	        }
	    }
	}
?>
