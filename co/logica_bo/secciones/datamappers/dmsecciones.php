<?
    class dmSecciones
    {
    	  
    	  public static function save($oLocal)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                
                if ($oLocal->getIdSeccion() == 0)
                    $oConexion->setQuery("INSERT INTO secciones (seccion, idSeccionPadre, href) VALUES('".htmlspecialchars(mysql_real_escape_string($oLocal->getSeccion()), ENT_QUOTES)."', '".$oLocal->getIdSeccionPadre()."', '".htmlspecialchars(mysql_real_escape_string($oLocal->getHREF()), ENT_QUOTES)."')");
                else
                    $oConexion->setQuery("UPDATE secciones SET seccion = '".$oLocal->getSeccion()."', idSeccionPadre = '".$oLocal->getIdSeccionPadre()."', href = '".$oLocal->getHREF()."' WHERE idSeccion = " . $oLocal->getIdSeccion());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }            
        }

    
	    /**
	    *Realiza el INSERT en la base de datos
	    *@param class oSecciones
	    *@return 1 si ejecuto con exito, 0 si ocurrio un error.
	    */
	    public static function Insert($osecciones)
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir_Trans();
			    $cn->setQuery("INSERT INTO secciones (seccion) VALUES ('". htmlspecialchars(mysql_real_escape_string($osecciones->getseccion()), ENT_QUOTES) ."')");
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return 1;
		    }
		    catch(exception $e)
		    {
			    $cn->RollBack();
			   throw $e;
		    }		
	    }
	    /**
	    *Realiza el UPDATE en la base de datos
	    *@param class osecciones
	    *@return 1 si ejecuto con exito, 0 si ocurrio un error.
	    */
	    public static function Update($osecciones)
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir_Trans();
			    $cn->setQuery("UPDATE secciones SET seccion = '". htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($osecciones->getseccion())), ENT_QUOTES) ."' WHERE idSeccion = ". $osecciones->getidSeccion() ."");
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return 1;
		    }
		    catch(exception $e)
		    {
			    $cn->RollBack();
			    throw $e;
		    }		
	    }
	    /**
	    *Realiza el DELETE en la base de datos
	    *@param object osecciones
	    *@return 1 si ejecuto con exito, 0 si ocurrio un error.
	    */
	    public static function Delete($osecciones)
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir_Trans();
			    $cn->setQuery("DELETE FROM secciones WHERE idSeccion = ". $osecciones->getidSeccion() ."");
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return 1;
		    }
		    catch(exception $e)
		    {
			    $cn->RollBack();
			    throw $e;
		    }		
	    }
	    /**
	    *Realiza un SELECT que devuelve todos los registros de secciones
	    *@return array Assoc, si ocurrio un error devuelve array()
	    */
	    public static function getAll()
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir();
			    $cn->setQuery("SELECT * FROM secciones");
			    $Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();
			    return $Tabla;
		    }
		    catch(exception $e)
		    {			
			    throw $e;                   
		    }		
	    }
	    public static function getAllNice()
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir();
			    $cn->setQuery("SELECT s.idSeccion, s.seccion as Nombre, p.seccion as Padre FROM secciones s LEFT JOIN seccionesPadre p ON p.idSeccionPadre = s.idSeccionPadre ORDER BY Padre ASC, Nombre ASC");
			    $Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();
			    return $Tabla;
		    }
		    catch(exception $e)
		    {			
			    throw $e;                   
		    }		
	    }
        /**
        *Realiza un SELECT que devuelve todos los registros de secciones
        *@return array Assoc, si ocurrio un error devuelve array()
        */
        public static function getAllObj()
        {        
            $cn = Conexion::nuevo();
            try
            {
                $cn->Abrir();
                $cn->setQuery("SELECT * FROM secciones");
                $Tabla = $cn->DevolverQuery();
                $osecciones[] = array();
                foreach($Tabla as $Fila)
                {
                    $osecciones[] = new Secciones();
                    				$osecciones[count($oSecciones)-1]->setidSeccion($Fila['idSeccion']);
				$osecciones[count($oSecciones)-1]->setseccion($Fila['seccion']);
                }
                $cn->Cerrar();
                return $osecciones;
            }
            catch(exception $e)
            {            
                throw $e;                  
            }        
        }
	    /**
	    *Realiza un SELECT que devuelve UN registros de secciones por $idSeccion
	    *@param $idSeccion condicion a buscar
	    *@return class clssecciones, si ocurrio un error devuelve class clssecciones vacio
	    */
	    public static function getById($idSeccion)
	    {		
		    $cn = Conexion::nuevo();
		    $osecciones = new Secciones();
		    try
		    {
			    $cn->Abrir();
			    $cn->setQuery("SELECT * FROM secciones WHERE idSeccion = ". $idSeccion ." ");
			    $Tabla = $cn->DevolverQuery();
			    if ($Tabla)
			    {
    				$osecciones->setidSeccion($Tabla[0]['idSeccion']);
					$osecciones->setseccion($Tabla[0]['seccion']);
					$osecciones->sethref($Tabla[0]['href']);
					$osecciones->setidseccionpadre($Tabla[0]['idSeccionPadre']);
			    
			    }
			    $cn->Cerrar();			
		    }
		    catch(exception $e)
		    {			
			    throw $e;
		    }
		    return $osecciones;
	    }
    }
?>
