<?

class Secciones
{
	/*Datos Privados*/
	private $_idseccion;
	private $_seccion;

	/*Constructor*/
/**
*@var idseccion
*@var seccion
*/
	function __construct($idseccion=0, $seccion='')
	{
		$this->_idseccion=$idseccion;
		$this->_seccion=$seccion;
		$this->_href;
		$this->_idseccionpadre;

	}
	/*Metodos Publicos*/
	/**
	* Set idSeccion
	* @var idSeccion
	*/
	function setidSeccion($idseccion)
	{
		$this->_idseccion =$idseccion;
	}
	/**
	* Get idSeccion
	* @return idSeccion
	*/
	function getidSeccion()
	{
		return $this->_idseccion;
	}
	/**
	* Set seccion
	* @var seccion
	*/
	function setseccion($seccion)
	{
		$this->_seccion =$seccion;
	}
	/**
	* Get seccion
	* @return seccion
	*/
	function getseccion()
	{
		return $this->_seccion;
	}

	function sethref($seccion)
	{
		$this->_href =$seccion;
	}
	function gethref()
	{
		return $this->_href;
	}

	function setidseccionpadre($seccion)
	{
		$this->_idseccionpadre =$seccion;
	}
	function getidseccionpadre()
	{
		return $this->_idseccionpadre;
	}
	
	/*Destructor*/
	function __destruct()
	{
	}
}
?>
