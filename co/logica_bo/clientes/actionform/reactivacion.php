<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mailer"));
    
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
    
    Includes::Scripts();
    if ($_GET["checks"])
    {
        try
        {
            $arrCodigos_x_activar = explode(",", $_GET["checks"]);
            $cant=0;
            for($i=0; $i<count($arrCodigos_x_activar); $i++)
            {
                $MAIL = Aplicacion::getEmail("reactivacion");
                $oCliente = dmCliente::getByIdUsuario($arrCodigos_x_activar[$i]);
                if ($oCliente->getIdUsuario())
                {
                    $cant++;
                    
                    $emailBuilder = new EmailBuilder();
	                $emailData = $emailBuilder->generateRecordatorioActivacionEmail($oCliente->getDatos()->getNombre(), $oCliente->getIdUsuario(), $oCliente->getDatos()->getNombre());
	                
					$emailSender = new EmailSender();
					$emailSender->sendEmail($emailData);
                }
            }
            ?>
                <script>
                    alert("<?=$cant?> mails de reactivación enviados correctamente.");
                </script>
            <?
        }
        catch(exception $e)
        {
            throw $e;
        }
    }
?>
<script>
    window.location = DIRECTORIO_URL_BO + "clientes/index.php";
</script>
