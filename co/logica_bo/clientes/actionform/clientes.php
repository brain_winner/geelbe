<?php
if($_POST) {
	ob_start();
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
	extract($_POST);
	try {
		$IdUsuario = ($frmU["txtIdUsuario"] == "0") ? 0 : Aplicacion::Decrypter($frmU["txtIdUsuario"]);
		$objCliente = new MySQL_Cliente();
		$objDatos = new DatosUsuariosPersonales();
		$objDatos->setApellido(ucwords(strtolower($frmDaU["txtApellido"])));
		$objDatos->setIdTipoUsuario(1);

		$objDatos->setIdTratamiento($frmDaU["optTratamiento"]);
		$objDatos->setNombre(ucwords(strtolower($frmDaU["txtNombre"])));
		$objDatos->setNroDoc($frmDaU["txtNroDoc"]);
		$objDatos->setFechaNacimiento($frmDaU["cbxAnio"]."-".$frmDaU["cbxMes"]."-".$frmDaU["cbxDia"]);
		$objDatos->setCodTelefono($frmDaU["txtCodTelefono"]);
		$objDatos->setTelefono($frmDaU["txtTelefono"]);
		$objDatos->setceluempresa($frmDaU["cmb_CelularComp"]);
		$objDatos->setcelucodigo($frmDaU["txt_CelularCodigo"]);
		$objDatos->setcelunumero($frmDaU["txt_CelularNumero"]);
		$objDatos->esCompleto();
		$objCliente->setDatos($objDatos);
		if($IdUsuario==0) {
			$objCliente->setClave($frmU["txtClaveNuevo1"]);
			$objCliente->setes_Activa(0);
			$objCliente->setes_Provisoria(1);
			$objCliente->setFechaIngreso(date("Y-m-d", time()));
		}
		else {
			if($frmU["txtClaveNuevo1"]!="") {
				$objCliente->setClave($frmU["txtClaveNuevo1"]);
			}
		}
		$objCliente->setNombreUsuario($frmU["txtEmailNuevo1"]);
		$objDireccion = new DatosUsuariosDirecciones();
		$objDireccion->setDomicilio($frmDi1["txtDomicilio"]);
		$objDireccion->setNumero($frmDi1["txtNumero"]);
		$objDireccion->setPiso($frmDi1["txtPiso"]);
		$objDireccion->setPuerta($frmDi1["txtPuerta"]);
		$objDireccion->setPoblacion($frmDi1["txtPoblacion"]);
		$objDireccion->setIdProvincia($frmDi1["cbxProvincia"]);
		$objDireccion->setIdCiudad($frmDi1["cbxCiudad"]);
		$objDireccion->setCP($frmDi1["txtCodigoPostal"]);
		$objDireccion->setIdPais($frmDi1["cbxPais"]);
		$objDireccion->setIdUsuario($IdUsuario);
		$objDireccion->setTipoDireccion(0);
		$objDireccion->esCompleto();
		$objCliente->setDirecciones(0, $objDireccion);
		$objCliente->setIdPerfil(1);
		$objCliente->setIdUsuario($IdUsuario);
		dmCliente::save($objCliente);
		?>
				<script type="text/javascript">
                    window.parent.irVolver();
                </script>
		<?
	}
	catch(MySQLException $e)
	{
		?>
				<script>
                   window.parent.ErrorMSJ("<?=$e->getNumeroSQLERROR()?>");
                </script>
		<?
	}
	ob_flush();
}
?>