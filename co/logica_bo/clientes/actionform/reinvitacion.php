<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados"));
    
    
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
    
    Includes::Scripts();
    if ($_GET["checks"]) {
        try {
            $arrCodigos_x_activar = explode(",", $_GET["checks"]);
            $cant=0;
            for($i=0; $i<count($arrCodigos_x_activar); $i++) {
                $MAIL = Aplicacion::getEmail("reinvitacion");
                $oCliente = dmCliente::getByIdUsuario($arrCodigos_x_activar[$i]);
                if ($oCliente->getIdUsuario()) {
                    $Ahijados = dmReferenciados::ObtenerReferenciados_NoRegistrados($oCliente->getIdUsuario());
                    foreach($Ahijados as $Ahijado) {
                        $cant++;
                        
				   		$emailBuilder = new EmailBuilder();
				   		$emailData = $emailBuilder->generateInviteEmail($oCliente, $Ahijado["Email"]);
				   
						$emailSender = new EmailSender();
						$emailSender->sendEmail($emailData);
						
					    $conexion = Conexion::nuevo();
					    $conexion->setQuery("insert geelbe_sent_invitations (invitation_from, invitation_to, invitation_date) values ('".$oCliente->getNombreUsuario()."','".$Ahijado["Email"]."', NOW());");
					    $conexion->EjecutarQuery();
                    }
                }
            }
            ?>
                <script>
                    alert("<?=$cant?> mails de reinvitación enviados correctamente.");
                </script>
            <?
        }
        catch(exception $e) {
            throw $e;
        }
    }
?>
<script>
    window.location = DIRECTORIO_URL_BO + "clientes/index.php";
</script>