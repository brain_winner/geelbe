<?
    if($_POST) {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto", "dm"));
        $arrayHTML = Aplicacion::getIncludes("onlyget", "htmlRecomendacion");
        try {
            $arrDatos = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));
            $collAhijados = array();
            foreach($_POST["frmEmail"] as $email) {
                $esRegistrado = dmReferenciados::EsUsuarioRegistrado($email,1);
                if(($email != "" && $email != $arrDatos["Email"]) || $esRegistrado == 1) {
                    $objAhijados = new MySQL_Ahijados();
                    $objAhijados->setEmail($email);
                    $objAhijados->setIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
                    array_push($collAhijados, $objAhijados);
                    if($esRegistrado == 1) {
                    	dmReferenciados::actualizarPadrinoInvitaciones($email);
                    }
                }
            }

	        $objUsuario = dmUsuario::getByIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
		    dmReferenciados::Invitacion($collAhijados, $objUsuario, $arrDatos);
	        dmReferenciados::InvitacionHistorico($collAhijados, $arrDatos);
            ?>
                <script>window.parent.msjInfo("Se han enviado las invitaciones","Referenciar", "<?=Aplicacion::getRootUrl()?>front/referenciados/");</script>
            <?
        }
        catch(MySQLException $e) {
            die($e->getMessage());
        }
        ob_flush();
    }
?>