<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/clases/clsusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/historico/datamappers/dmhistoricologin.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    
    try {
        ValidarUsuarioLogueadoBo(3);
    } catch(ACCESOException $e) {
	    ?>
	        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
	    <?
        exit;
    }
	
	$email = $_POST["email"];
	$body = nl2br($_POST["body"]);
	$type =  "Email directo";
	$subject = $_POST["subject"];
	$idUsuario = $_POST["IdUsuario"];
	
	$oConexion = Conexion::nuevo();
    try
    {
        $oConexion->Abrir_Trans();
        $oConexion->setQuery("INSERT INTO mensajes_bo (`idMensaje`, `idUsuario`, `fecha` , `asunto` , `mensaje`, `boUserId`) VALUES (0, $idUsuario, NOW(), '$subject', '$body', ".Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]).")");
        $oConexion->EjecutarQuery();
        $oConexion->Cerrar_Trans();
    }
    catch (MySQLException $e)
    {
        $oConexion->RollBack();
        throw $e;
    }
		
	$emailData = EmailBuilder::generateAnEmail($email, $body, $type, $subject);
	
	// TODO: agregarlo en el archivo de configuracion
	$emailData->setReplyTo("info.co@corp.geelbe.com"); 
	$emailSender = new EmailSender();
	$emailSender->sendEmail($emailData);
	
		if ($_POST) {
	        try {
	            ?>
	                <script>
	                    alert("El mail se envio correctamente");
	                </script>
	
	                <script type="text/javascript">
	                    window.location.href="../../../bo/clientes/Ver.php?IdUsuario=<?=$idUsuario?>"; 
	                </script>            
	            <?     
	        }
	        catch(exception $e) {
	            print_r($e);
	        }
	    }

?>