<?
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
    class MySQL_Cliente extends usuarios
    {
        public function getInsert()
        {
            if($this->_idusuario==0)
            {
                return "INSERT INTO usuarios (idusuario, nombreusuario, clave, es_provisoria, fechaingreso, idperfil, es_activa) VALUES (NULL, '" . htmlspecialchars(mysql_real_escape_string($this->_nombreusuario), ENT_QUOTES) ."', '".Hash::getHash($this->_clave)."', ".$this->_es_provisoria.", '".$this->_fechaingreso."',".$this->_idperfil.",".$this->_es_activa.")";
            }
            else
            {
                if($this->_clave)
                {
                    return "UPDATE usuarios SET clave = '". Hash::getHash($this->_clave) . "' WHERE IdUsuario = " . $this->_idusuario;
                }
                else
                {
                    return "SELECT 1";
                }
            }
            
        }
        public function getDelete()
        {
            return "DELETE FROM usuarios WHERE IdUsuario = ".$this->getIdUsuario();
        }
        public function getInsertDatosPersonales()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->getDatos()->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad != "_idusuario")
                {   
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
                                
            }
            return "INSERT INTO datosusuariospersonales (IdUsuario, ".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".htmlspecialchars(mysql_real_escape_string($this->getIdUsuario()), ENT_QUOTES).", ".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getInsertDatosDirecciones()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->getDirecciones(0)->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_idusuario" || $propiedad == "_idprovincia" || $propiedad == "_idpais")
                {
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=(($valor == 0)?"NULL":chr(34).mysql_real_escape_string($valor).chr(34)).", ";
                    if($propiedad != "_idusuario")
                    {
                        $strCamposU .=substr($propiedad,1,strlen($propiedad))." = ".(($valor == 0)?"NULL":chr(34).mysql_real_escape_string($valor).chr(34)).", ";
                    }
                }
                else
                {
                    if($propiedad != "_tipodireccion")
                        $strCamposU .=substr($propiedad,1,strlen($propiedad))." = ".chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }

            }
            return "INSERT INTO datosusuariosdirecciones (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getById($Id)
        {
             return "SELECT idusuario, nombreusuario, clave, es_provisoria, padrino, DATE_FORMAT(fechaingreso, '%e/%m/%Y') AS FechaIngreso, idperfil, es_activa, motivobaja, usuariobaja, mail_suscription FROM usuarios WHERE IdUsuario = ".mysql_real_escape_string($Id);
        }
        public function getDatosById($Id)
        {
            $strCampos="";
            $Me = $this->getDatos()->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_fechanacimiento")
                {
                    $strCampos .="DATE_FORMAT(".substr($propiedad,1,strlen($propiedad)).", '%e/%c/%Y') AS FechaNacimiento, ";
                }
                else
                {
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                }
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM datosusuariospersonales WHERE IdUsuario = ".mysql_real_escape_string($Id);
        }
        public function getDireccionesById($Id)
        {
            $strCampos="";
            $Me = $this->getDirecciones(0)->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM datosusuariosdirecciones WHERE IdUsuario = ".mysql_real_escape_string($Id);
        }
    }
?>