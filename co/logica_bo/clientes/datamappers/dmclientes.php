<?
    class dmCliente
    {
        public static function save(MySQL_Cliente $objCliente)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objCliente->getInsert());
                $oConexion->EjecutarQuery();
                if($objCliente->getIdUsuario() == 0)
                {
                    $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                    $ID = $oConexion->DevolverQuery();
                    $objCliente->setIdUsuario($ID[0]["Id"]);
                }
                $oConexion->setQuery($objCliente->getInsertDatosPersonales());
                $oConexion->EjecutarQuery();
                $oConexion->setQuery($objCliente->getInsertDatosDirecciones());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function delete(MySQL_Cliente $objCliente)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                //$oConexion->setQuery($objCliente->getDelete());
                $oConexion->setQuery("UPDATE usuarios SET es_activa = 1, es_provisoria = 1 WHERE idUsuario = " . $objCliente->getIdUsuario());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getByIdUsuario($Id)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $objCliente = new MySQL_Cliente();
                $oConexion->Abrir_Trans();

                $oConexion->setQuery($objCliente->getById($Id));
                $Tabla = $oConexion->DevolverQuery();
                foreach($Tabla as $fila)
                {
                    foreach ($fila as $atributo => $valor)
                    {
                        $objCliente->__set($valor, $atributo);
                    }
                    $oConexion->setQuery($objCliente->getDatosById($Id));
                    $Datos = $oConexion->DevolverQuery();
                    foreach ($Datos as $fila2)
                    {
                        foreach ($fila2 as $atributo2 => $valor2)
                        {
                            $objCliente->getDatos()->__set($valor2, $atributo2);
                        }
                    }
                    $oConexion->setQuery($objCliente->getDireccionesById($Id));
                    $Datos = $oConexion->DevolverQuery();
                    $i=0;
                    foreach ($Datos as $fila2)
                    {
                        foreach ($fila2 as $atributo2 => $valor2)
                        {
                            $objCliente->getDirecciones($i)->__set($valor2, $atributo2);
                        }
                        $i++;
                    }
                }
                $oConexion->Cerrar_Trans();
                return $objCliente;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }

		public static function getClientesCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);

            $oConexion = Conexion::nuevo();
            try
            {
				$oConexion->Abrir_Trans();
				$oConexion->setQuery("SELECT count(*) as total 
				                      FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE ($filtersSQL) AND U.IdPerfil = 1");
				$Tabla = $oConexion->DevolverQuery();
				$oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }		
		
		public static function getClientesPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		    $filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
				$oConexion->setQuery("SELECT 	U.IdUsuario as 'U.IdUsuario', 
												U.NombreUsuario as 'U.NombreUsuario', 
												U.Padrino as 'U.Padrino', 
												DUP.Apellido as 'DUP.Apellido',
												DUP.Nombre AS 'DUP.Nombre', 
												IF(U.mail_suscription=false, 'Desuscripto', IF(es_activa=0, 'Suscripto', IF(es_Provisoria = 0, 'Desuscripto', 'No Aplica'))) as 'MailSuscription', 
												U.LandingUrl as 'U.LandingUrl',
												DATE_FORMAT(U.FechaIngreso, '%d/%m/%Y') AS 'FechaIngreso', 
												IF(es_activa=0,'Activo',IF(es_Provisoria = 0, 'Desuscripto', IF(Padrino = 'Huerfano', 'Huerfano', 'Inactivo'))) as 'Estado'  
									  FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE ($filtersSQL) AND U.IdPerfil = 1 
									  ORDER BY $sortSQL 
									  LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
		
		public static function getClientesActivosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);

            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(*) as total 
				                      FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE ($filtersSQL) AND U.es_activa = 0 AND U.IdPerfil = 1 ");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
		
        public static function getClientesActivosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		    $filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT 	U.IdUsuario as 'U.IdUsuario', 
												U.NombreUsuario as 'U.NombreUsuario', 
												U.Padrino as 'U.Padrino', 
												DUP.Apellido as 'DUP.Apellido',
												DUP.Nombre AS 'DUP.Nombre', 
												IF(U.mail_suscription=false, 'Desuscripto', IF(es_activa=0, 'Suscripto', IF(es_Provisoria = 0, 'Suscripto', 'No Aplica'))) as 'MailSuscription',  
												U.LandingUrl as 'U.LandingUrl',
												DATE_FORMAT(U.FechaIngreso, '%d/%m/%Y') as 'FechaIngreso', 
												'Activo' as 'Estado'  
									  FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE ($filtersSQL) AND U.es_activa = 0 AND U.IdPerfil = 1 
									  ORDER BY $sortSQL 
									  LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }

		public static function getClientesInactivosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			
            $oConexion = Conexion::nuevo();
			
			try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(*) as total 
				                      FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE ($filtersSQL) AND U.es_activa = 1 AND U.es_Provisoria = 1 AND U.Padrino <> 'Huerfano' AND U.IdPerfil = 1");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
		
		public static function getClientesInactivosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		    $filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
            $oConexion = Conexion::nuevo();
			
			try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT 	U.IdUsuario as 'U.IdUsuario', 
												U.NombreUsuario as 'U.NombreUsuario', 
												U.Padrino as 'U.Padrino', 
												DUP.Apellido as 'DUP.Apellido',
												DUP.Nombre AS 'DUP.Nombre', 
												IF(U.mail_suscription=false, 'Desuscripto', IF(es_activa=0, 'Suscripto', IF(es_Provisoria = 0, 'Suscripto', 'No Aplica'))) as 'MailSuscription',  
												U.LandingUrl as 'U.LandingUrl',
												DATE_FORMAT(U.FechaIngreso, '%d/%m/%Y') as 'FechaIngreso', 
												'Inactivo' as 'Estado' 
									  FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE ($filtersSQL) AND U.es_activa = 1 AND U.es_Provisoria = 1 AND U.Padrino <> 'Huerfano' AND U.IdPerfil = 1 
									  ORDER BY $sortSQL 
									  LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
		
		public static function getClientesHuerfanosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(*) as total 
									  FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE ($filtersSQL) AND U.es_activa = 1 AND U.es_Provisoria = 1  AND U.Padrino = 'Huerfano' AND U.IdPerfil = 1");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }  	
		
        public static function getClientesHuerfanosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		    $filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT 	U.IdUsuario as 'U.IdUsuario', 
												U.NombreUsuario as 'U.NombreUsuario', 
												U.Padrino as 'U.Padrino', 
												DUP.Apellido as 'DUP.Apellido',
												DUP.Nombre AS 'DUP.Nombre', 
												IF(U.mail_suscription=false, 'Desuscripto', IF(es_activa=0, 'Suscripto', IF(es_Provisoria = 0, 'Suscripto', 'No Aplica'))) as 'MailSuscription',  
												U.LandingUrl as 'U.LandingUrl',
												DATE_FORMAT(U.FechaIngreso, '%d/%m/%Y') as 'FechaIngreso', 
												'Huerfano' as 'Estado' 
									  FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE ($filtersSQL) AND U.es_activa = 1 AND U.es_Provisoria = 1 AND U.Padrino = 'Huerfano' AND U.IdPerfil = 1 
									  ORDER BY $sortSQL 
									  LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }	

        public static function getClientesDesuscriptosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(*) as total 
				                      FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE ($filtersSQL) AND U.es_activa = 1 AND U.es_Provisoria = 0 AND U.IdPerfil = 1");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }    
		
		public static function getClientesDesuscriptosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		    $filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT 	U.IdUsuario as 'U.IdUsuario', 
												U.NombreUsuario as 'U.NombreUsuario', 
												U.Padrino as 'U.Padrino', 
												DUP.Apellido as 'DUP.Apellido',
												DUP.Nombre AS 'DUP.Nombre', 
												IF(U.mail_suscription=false, 'Desuscripto', IF(es_activa=0, 'Suscripto', IF(es_Provisoria = 0, 'Suscripto', 'No Aplica'))) as 'MailSuscription',  
												U.LandingUrl as 'U.LandingUrl',
												DATE_FORMAT(U.FechaIngreso, '%d/%m/%Y') as 'FechaIngreso', 
												'Desuscripto' as 'Estado' 
									  FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE ($filtersSQL) AND U.es_activa = 1 AND U.es_Provisoria = 0 AND U.IdPerfil = 1 
									  ORDER BY $sortSQL 
									  LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
		
		public static function getClientesRegistradosPorFechaCount($codigo) {
			$oConexion = Conexion::nuevo();
            try
            {
           	 if($codigo != '0')
           	 	$codigo = 'AND U.Padrino LIKE "'.$codigo.'%"';
           	 else
            	$codigo = '';
            
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(distinct date(U.FechaIngreso)) as total 
				                      FROM usuarios U
									  WHERE U.IdPerfil = 1 ".$codigo);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return$Tabla[0]['total'];            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
			
		public static function getClientesRegistradosPorFechaPaginados ($offset=0, $itemsPerPage=10, $codigo) {
			$oConexion = Conexion::nuevo();
            try
            {
            
            if($codigo != '0')
            	$codigo = 'AND U.Padrino LIKE "'.$codigo.'%"';
            else
            	$codigo = '';
            
                $oConexion->Abrir_Trans();
				$oConexion->setQuery("SELECT date(U.FechaIngreso) as FechaIngreso,
												count(U.IdUsuario) as Users
												FROM usuarios U 
												WHERE U.IdPerfil = 1 ".$codigo."
												GROUP BY date(U.FechaIngreso)
												ORDER BY date(U.FechaIngreso) desc
												LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));

				$Tabla = $oConexion->DevolverQuery();

                $oConexion->Cerrar_Trans();
                return $Tabla;
            }

            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
		
		public static function getClientesActivosxFecha ($Fecha, $codigo) {
            $oConexion = Conexion::nuevo();
            try
            {

            if($codigo != '0')
            	$codigo = 'AND U.Padrino LIKE "'.$codigo.'%"';
            else
            	$codigo = '';
            
                $oConexion->Abrir_Trans();
				$oConexion->setQuery("SELECT date(U.FechaIngreso) as FechaIngreso,
											count(U.IdUsuario) as UsersAct
											FROM usuarios U 
											WHERE U.es_activa = 0 AND U.IdPerfil = 1 ".$codigo." AND U.FechaIngreso LIKE '".$Fecha."%'
											LIMIT 1");
												
				$Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
		
		public static function getClientesRegyActxFecha ($offset, $itemsPerPage, $codigo) {
			$users = self::getClientesRegistradosPorFechaPaginados($offset, $itemsPerPage, $codigo);
			foreach ($users as $key=>$elemento){
				$datosActivos = self::getClientesActivosxFecha($elemento["FechaIngreso"], $codigo);
				$users[$key]["UsersAct"] = $datosActivos[0]["UsersAct"];
				$users[$key]["Percent"] = round((($datosActivos[0]["UsersAct"]*100)/$elemento["Users"]),2)."%";
			}
			return $users;
		}
		
        public static function GetPaises()
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();

                $oConexion->setQuery("SELECT * FROM paises ORDER BY Nombre");
                $DT = $oConexion->DevolverQuery();

                $oConexion->Cerrar();
                return $DT;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function GetPaisesById($Id)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();

                $oConexion->setQuery("SELECT * FROM paises WHERE IdPais = ".mysql_real_escape_string($Id)." ORDER BY Nombre");
                $DT = $oConexion->DevolverQuery();

                $oConexion->Cerrar();
                return $DT;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function GetProvincias()
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();

                $oConexion->setQuery("SELECT * FROM provincias ORDER BY Nombre");
                $DT = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                return $DT;
            }
            catch (MySQLException $e)
            {
                throw $e;
            }
        }
        public static function GetCiudadById($Id)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();

                $oConexion->setQuery("SELECT * FROM ciudades WHERE IdCiudad = ".mysql_real_escape_string($Id)." ORDER BY Descripcion");
                $DT = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                return $DT;
            }
            catch (MySQLException $e)
            {
                throw $e;
            }
        }
        public static function GetProvinciasById($Id)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();

                $oConexion->setQuery("SELECT * FROM provincias WHERE IdProvincia = ".mysql_real_escape_string($Id)." ORDER BY Nombre");
                $DT = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                return $DT;
            }
            catch (MySQLException $e)
            {
                throw $e;
            }
        }
        public static function getIdPadrino($email)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();

                $oConexion->setQuery("SELECT IdUsuario FROM usuarios WHERE NombreUsuario = '".$email."'");
                $dt = $oConexion->DevolverQuery();

                $oConexion->Cerrar();
                return $dt;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        
    	public static function getClientesInactivosSegundoIntentoPaginados ($offset=0, $itemsPerPage=100) {
			$oConexion = Conexion::nuevo();
            try {
                $oConexion->Abrir_Trans();
				$oConexion->setQuery("SELECT U.IdUsuario
FROM usuarios U 
WHERE U.es_activa = 1 
AND U.es_Provisoria = 1 
AND U.Padrino <> 'Huerfano' 
AND U.IdPerfil = 1
AND U.second_activation_send = false 
AND U.third_activation_send = false
AND U.FechaIngreso < (NOW() - INTERVAL 15 DAY)
ORDER BY U.FechaIngreso ASC 
LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
												
				$Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }

            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
    
    	public static function getClientesInactivosTercerIntentoPaginados ($offset=0, $itemsPerPage=100) {
			$oConexion = Conexion::nuevo();
            try {
                $oConexion->Abrir_Trans();
				$oConexion->setQuery("SELECT U.IdUsuario 
FROM usuarios U 
WHERE U.es_activa = 1 
AND U.es_Provisoria = 1 
AND U.Padrino <> 'Huerfano' 
AND U.IdPerfil = 1
AND U.second_activation_send = true 
AND U.third_activation_send = false
AND U.FechaIngreso < (NOW() - INTERVAL 30 DAY)
ORDER BY U.FechaIngreso ASC 
LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
												
				$Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }

            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
    public static function getUsuariosBOParaMensajes($perfiles = "") {
			$oConexion = Conexion::nuevo();
            try {
                $oConexion->Abrir_Trans();
				$oConexion->setQuery("SELECT U.IdUsuario, U.NombreUsuario
                                      FROM usuarios U 
                                      WHERE U.IdPerfil in ($perfiles)");
												
				$Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }

            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>