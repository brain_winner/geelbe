<?php
    class dmLandings
    {
        public static function getAll($columna = 'nombre', $sentido = 'asc', $filtro="")
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT id, Nombre, Texto FROM landings ".($filtro != '' ? "WHERE nombre LIKE \"%".$filtro."%\"" : '')." ORDER BY " . $columna . " " . $sentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                /*$arrLocales = array();
                foreach($Tabla as $Fila)
                {
                    $arrLocales[] = new Locales();
                    $arrLocales[count($arrLocales) - 1]->setIdLocal($Fila["idLocal"]);
                    $arrLocales[count($arrLocales) - 1]->setDescripcion($Fila["Descripcion"]);
                    $arrLocales[count($arrLocales) - 1]->setDireccion($Fila["Direccion"]);
                    $arrLocales[count($arrLocales) - 1]->setIdProvincia($Fila["idProvincia"]);
                }
                return $arrLocales;*/
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        public static function save($oLocal)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                if ($oLocal->getIdLanding() == "") {
                    $oConexion->setQuery("INSERT INTO landings (Nombre, Texto) VALUES('".htmlspecialchars(mysql_real_escape_string($oLocal->getNombre()), ENT_QUOTES)."', '".htmlspecialchars(mysql_real_escape_string($oLocal->getTexto()), ENT_QUOTES)."')");
                } else
                    $oConexion->setQuery("UPDATE landings SET Nombre = '".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($oLocal->getNombre())), ENT_QUOTES)."', Texto = '".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($oLocal->getTexto())), ENT_QUOTES)."' WHERE id = " . $oLocal->getIdLanding());
                $oConexion->EjecutarQuery();
                
                if($oLocal->getIdLanding() == "") {
                	$ruta = dirname(__FILE__).'/../../../front/landings/';
                	$ruta .= $oConexion->LastId();
                	mkdir($ruta);
                	
                	$id = $oConexion->LastId();
                } else {
                	$id = $oLocal->getIdLanding();
                }
                
                if(isset($_FILES['imgs'])) {
                
                	foreach($_FILES['imgs']['tmp_name'] as $i => $name) {
                		if(is_uploaded_file($name)) {
                			
                			$ext = pathinfo($_FILES['imgs']['name'][$i]);
                			$ext = strtolower($ext['extension']);

                			if($ext == 'jpeg' || $ext == 'jpg' || $ext == 'gif' || $ext == 'png')                			
	                			dmLandings::upload($id, $name, ($i+1), $ext);
                		}               		
                	}
                
                }
                
                if(isset($_FILES['logo']) && is_uploaded_file($_FILES['logo']['tmp_name'])) {
                	
                	$ext = pathinfo($_FILES['logo']['name']);
        			$ext = strtolower($ext['extension']);

        			if($ext == 'jpeg' || $ext == 'jpg' || $ext == 'gif' || $ext == 'png') {
            			$ruta = dirname(__FILE__).'/../../../front/landings/'.$id.'/';
			        	move_uploaded_file($_FILES['logo']['tmp_name'], $ruta.'logo.'.$ext);
			        }
                	
                }
                
                $oConexion->Cerrar_Trans();
                return $id;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }            
        }
        
        public static function upload($id, $img, $name, $ext) {
        
        	$ruta = dirname(__FILE__).'/../../../front/landings/'.$id.'/';
        	move_uploaded_file($img, $ruta.$name.'.'.$ext);
        	
        	dmLandings::thumbnail($ruta.$name.'.'.$ext, $ruta.'th_'.$name.'.'.$ext);
        	
        }
        
        public static function thumbnail($path, $to, $width = 64, $height = 64) {
        
        	$types = array(1 => "gif", "jpeg", "png", "swf", "psd", "wbmp"); // used to determine image type 

			if (!($size = getimagesize($path)) || !isset($types[$size[2]]))  
				return false; // image doesn't exist
						
			$x = 0;
			$y = 0;
			
			$oWidth = $width;
			$oHeight = $height;
				
			if($width > 0 && $height == 0)
		 		$height = ceil($width / ($size[0]/$size[1]));
		 	elseif($height > 0 && $width == 0)
		 		$width = ceil(($size[0]/$size[1]) * $height);
			elseif($width != 0 && $height != 0) {
			
				$newRatio = $width/$height;
				$origRatio = $size[0]/$size[1];
				
				if($origRatio > $newRatio) {
				
					$width = ceil(($size[0]/$size[1]) * $height);
					$x = ($width/2) - ($oWidth/2);
					
				} else {
				
					$height = ceil($width / ($size[0]/$size[1]));
					$y = ($height/2) - ($oHeight/2);			
					
				}
			
			}	
			
			$image = call_user_func('imagecreatefrom'.$types[$size[2]], $path); 
			$new = imagecreatetruecolor($oWidth, $oHeight);
			
			imagecopyresampled($new, $image, 0, 0, $x, $y, $width, $height, $size[0], $size[1]);
			return imagejpeg($new, $to);
        }
        
        public static function getById($idLocal)
        {
            $oConexion = Conexion::nuevo();
            $oLocal = new Landings();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM landings WHERE id = " . $idLocal);
                $Fila = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                
                    $oLocal->setIdLanding($Fila[0]["id"]);
                    $oLocal->setNombre($Fila[0]["nombre"]);
                    $oLocal->setTexto($Fila[0]["texto"]);

                }
                catch(MySQLException $e)
                {
                    throw $e;
                }
                return $oLocal;
            }
            
       	public static function delete($obj)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("DELETE FROM landings WHERE id = ".$obj->getIdLanding());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                
                $ruta = dirname(__FILE__).'/../../../front/landings/'.$obj->getIdLanding();
                $dir = opendir($ruta);
                while($f = readdir($dir))
                	unlink($ruta.'/'.$f);

				rmdir($ruta);
                
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>
