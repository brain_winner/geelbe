<?php
    class Landings
    {
        private $_idlanding;
        private $_nombre;
        private $_texto;
       
        public function __construct()
        {
            $this->_idlanding = "";
            $this->_nombre = "";
            $this->_texto = "";
        }
        public function getIdLanding()
        {
            return $this->_idlanding;
        }
        public function setIdLanding($idlocal)
        {
            $this->_idlanding = $idlocal;
        }
        public function getNombre()
        {
            return $this->_nombre;
        }
        public function setNombre($descripcion)
        {
            $this->_nombre = $descripcion;
        }
        public function getTexto()
        {
            return $this->_texto;
        }
        public function setTexto($descripcion)
        {
            $this->_texto = $descripcion;
        }
    }
?>
