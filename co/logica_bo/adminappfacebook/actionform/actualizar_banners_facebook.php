<?
    if($_POST) {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/adminappfacebook/datamappers/dmappfacebook.php");
	
		$banners = dmAdminAppFacebook::getAllBanners();
		
		foreach($banners as $banner) {
			$activo = $_POST[$banner["seccion"]]["activo"];
			if(isset($activo) && ($activo == "on" || $activo == "true" || $activo == "1")) {
				$activoBanner = "1";
			} else {
				$activoBanner = "0";
			}

			// Subo la imagen seleccionada para top ..
			$bannerDescriptor = $banner["seccion"]."Top";
			if(isset($_FILES[$bannerDescriptor."File"]) && is_uploaded_file($_FILES[$bannerDescriptor."File"]['tmp_name'])) {
				$destFolder = $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/archivos/";
		       	move_uploaded_file($_FILES[$bannerDescriptor."File"]["tmp_name"], $destFolder.$banner["seccion"]."top.jpg");
		    }
			
		    // Obtengo datos para top ..
			$hrefBannerTop = $_POST[$bannerDescriptor]["hrefBanner"];
			$titleBannerTop = $_POST[$bannerDescriptor]["titleBanner"];

			// Subo la imagen seleccionada para bottom ..
			$bannerDescriptor = $banner["seccion"]."Bottom";
			if(isset($_FILES[$bannerDescriptor."File"]) && is_uploaded_file($_FILES[$bannerDescriptor."File"]['tmp_name'])) {
				$destFolder = $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/archivos/";
		       	move_uploaded_file($_FILES[$bannerDescriptor."File"]["tmp_name"], $destFolder.$banner["seccion"]."bottom.jpg");
		    }
			
		    // Obtengo datos para bottom ..
			$hrefBannerBottom = $_POST[$bannerDescriptor]["hrefBanner"];
			$titleBannerBottom = $_POST[$bannerDescriptor]["titleBanner"];

			// Guardo ..
			dmAdminAppFacebook::updateBannerInfo($banner["seccion"], $hrefBannerTop, $titleBannerTop, $hrefBannerBottom, $titleBannerBottom, $activoBanner);
		}
 
 		dmAdminAppFacebook::saveAppData($_POST['app_id'], $_POST['app_secret']);
   }
?>