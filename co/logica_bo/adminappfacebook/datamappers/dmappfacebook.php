<?php
    class dmAdminAppFacebook {
    	public static function getAllBanners() {
            $oConexion = Conexion::nuevo();
            try {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT seccion, title_banner_top, href_banner_top, title_banner_bottom, href_banner_bottom, activo from banners_app_facebook");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(MySQLException $e) {
                throw $e;
            }
        }
        
        public static function updateBannerInfo($seccion, $hrefBannerTop, $titleBannerTop, $hrefBannerBottom, $titleBannerBottom, $activoBanner) {
        	$oConexion = Conexion::nuevo();
            try {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery(
                	"UPDATE banners_app_facebook 
                		SET title_banner_top = '".$titleBannerTop."', href_banner_top = '".$hrefBannerTop."', title_banner_bottom = '".$titleBannerBottom."', href_banner_bottom = '".$hrefBannerBottom."',activo = ".$activoBanner." 
                	 WHERE seccion = '".$seccion."'");
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch(MySQLException $e) {
                throw $e;
            }            
        }
        public static function getAppData() {
        	$oConexion = Conexion::nuevo();
            try {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM facebook LIMIT 1");
                $data = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                
                return current($data);
            }
            catch(MySQLException $e) {
                throw $e;
            }            
        }

        
        public static function saveAppData($appId, $appSecret) {
        	$oConexion = Conexion::nuevo();
            try {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("DELETE FROM facebook");
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("INSERT INTO facebook VALUES ('".$appId."', '".$appSecret."')");
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch(MySQLException $e) {
                throw $e;
            }            
        }

    }
?>