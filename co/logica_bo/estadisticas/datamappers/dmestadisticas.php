<?php
	set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
	require_once 'Zend/Cache.php'; 

    class dmEstadisticas
    {                
        private $Clientes=null;
        private $Invitaciones=null;
        private $Logins=null;
        private $Registros = null;
        
        public function __construct()
        {
            $this->Clientes = new EstadisticasUsuarios();
            $this->Invitaciones = new EstadisticasInvitaciones();
            $this->Logins = new Logins();
            $this->Registros = new Registros();
        }
        /**
        * @desc 
        * @return EstadisticasUsuarios
        */
        public function Cliente()
        {
            return $this->Clientes;
        }
        public function Invitaciones()
        {
            return $this->Invitaciones;
        }
        public function Logins()
        {
            return $this->Logins;
        }
        public function Registros()
        {
            return $this->Registros;
        }
    }  
    class EstadisticasUsuarios
    {
        const  TODOS_LOS_USUARIOS = 0;
        const USUARIOS_ACTIVOS = 1;
        const USUARIOS_INACTIVOS = 2;
        private function CantidadUsuarios($flag=0)
        {            
            $oConexion = Conexion::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
                 
                 switch($flag)
                 {
                     case self::TODOS_LOS_USUARIOS:
                        $sql = "SELECT COUNT(idUsuario) AS cant FROM usuarios WHERE idPerfil = 1";
                     break;
                     case self::USUARIOS_ACTIVOS:
                        $sql = "SELECT COUNT(idUsuario) AS cant FROM usuarios WHERE es_activa = 0 AND idPerfil = 1";
                     break;
                     case self::USUARIOS_INACTIVOS:
                        $sql = "SELECT COUNT(idUsuario) AS cant FROM usuarios WHERE es_activa = 1 AND idPerfil = 1";
                     break;
                     
                 }                 
                 $oConexion->setQuery($sql);
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 return $Tabla[0]["cant"];                 
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function TablaEstadistica()
        {
            try
            {
                $DatosEstadisticos = array();
                $DatosEstadisticos[0]["id"] = 0;
                $DatosEstadisticos[0]["Descripcion"] = "Total clientes";
                $DatosEstadisticos[0]["Total"] = self::CantidadUsuarios(self::TODOS_LOS_USUARIOS);
                $DatosEstadisticos[0]["%"] = "100%";
                
                $DatosEstadisticos[1]["id"] = 1;
                $DatosEstadisticos[1]["Descripcion"] = "Clientes activos";
                $DatosEstadisticos[1]["Total"] = self::CantidadUsuarios(self::USUARIOS_ACTIVOS);
                $DatosEstadisticos[1]["%"] = round((self::CantidadUsuarios(self::USUARIOS_ACTIVOS)*100)/self::CantidadUsuarios(self::TODOS_LOS_USUARIOS),2). "%";
                
                $DatosEstadisticos[2]["id"] = 2;
                $DatosEstadisticos[2]["Descripcion"] = "Clientes inactivos";
                $DatosEstadisticos[2]["Total"] = self::CantidadUsuarios(self::USUARIOS_INACTIVOS);
                $DatosEstadisticos[2]["%"] = round((self::CantidadUsuarios(self::USUARIOS_INACTIVOS)*100)/self::CantidadUsuarios(self::TODOS_LOS_USUARIOS),2). "%";
                return $DatosEstadisticos;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function UsuariosAhijados($id, $columna, $sentido)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT A.Email as 'Nombre de usuario', IF(ISNULL(CONCAT(CONCAT(DUP.Apellido, ', '), DUP.Nombre)), 'Usuario no registrado', CONCAT(CONCAT(DUP.Apellido, ', '), DUP.Nombre)) AS 'Apellido y nombre' FROM ahijados A LEFT JOIN usuarios U ON A.IdUsuarioReg = U.IdUsuario LEFT JOIN datosusuariospersonales DUP ON DUP.idUsuario = U.idUsuario WHERE A.idUsuario = ".mysql_real_escape_string($id)." ORDER BY ".$columna." ".$sentido);
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 return $Tabla;
            }
            catch(exception $e)
            {
                throw $e;
            }            
        }
        public function CountUsuariosAhijados($id)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                 $oConexion->setQuery("SELECT count(A.Email) as Total FROM ahijados A LEFT JOIN usuarios U ON A.IdUsuarioReg = U.IdUsuario LEFT JOIN datosusuariospersonales DUP ON DUP.idUsuario = U.idUsuario WHERE A.idUsuario = ".mysql_real_escape_string($id));
                 $Tabla = $oConexion->DevolverQuery();
                 return $Tabla[0]["Total"];
            }
            catch(exception $e)
            {
                throw $e;
            }            
        }
        public function UsuariosAhijadosCompradores($email)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
						$oConexion->setQuery("SELECT DISTINCT U.IdUsuario, U.NombreUsuario FROM usuarios U 
												INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario  
												WHERE U.Padrino = '".$email."'");
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 return $Tabla;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function CountUsuariosAhijadosCompradores($email)
        {
            $oConexion = Conexion::nuevo();
            try
            {
						$oConexion->setQuery("SELECT count(DISTINCT U.IdUsuario) as Total FROM usuarios U 
												INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario  
												WHERE U.Padrino = '".$email."'");
                 $Tabla = $oConexion->DevolverQuery();
                 return $Tabla[0]["Total"];
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function UsuariosAhijadosRegistrados($id, $columna, $sentido)
        {
            $oConexion = Conexion::nuevo();
            try
            {
            	
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT u.NombreUsuario as 'Nombre de usuario', CONCAT(CONCAT(dp.Apellido, ', '), dp.Nombre) AS 'Apellido y nombre' FROM usuarios u LEFT JOIN datosusuariospersonales dp ON dp.idUsuario = u.idUsuario WHERE u.Padrino = (SELECT NombreUsuario FROM usuarios u2 WHERE u2.IdUsuario = ".mysql_real_escape_string($id).") ORDER BY ".$columna." ".$sentido);
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 return $Tabla;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function CountUsuariosAhijadosRegistrados($id)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                 $oConexion->setQuery("SELECT count(u.NombreUsuario) as Total FROM usuarios u LEFT JOIN datosusuariospersonales dp ON dp.idUsuario = u.idUsuario WHERE u.Padrino = (SELECT NombreUsuario FROM usuarios u2 WHERE u2.IdUsuario = ".mysql_real_escape_string($id).")");
                 $Tabla = $oConexion->DevolverQuery();
                 return $Tabla[0]["Total"];
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function BonosDisponibles($Creditos)
        {
            $disponibles = 0;
            foreach($Creditos as $fila)
            {                                    
                $disponibles += abs($fila["Valor"] - $fila["Importe"]);
            }
            return $disponibles;
        }
        public function BonosUtilizados($idUsuario)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT IFNULL(SUM(Importe),0) AS BonosUtilizados FROM bonos_x_pedidos BP 
INNER JOIN bonos_x_usuarios BU ON BP.idBono = BU.idBono
WHERE BU.idUsuario = " . $idUsuario);
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 return $Tabla[0]["BonosUtilizados"];
            }
            catch(exception $e)
            {
                throw $e;
            }            
        }
        public function BonosPotenciales($PotencialesCreditos)
        {
            $potenciales = 0;
            foreach($PotencialesCreditos as $key => $fila)
            {                                    
                $potenciales += $fila["Bono"];
            }
            return $potenciales;
        }
    }
    class EstadisticasInvitaciones
    {
        public function InvitacionesEnviadas()
        {
            $oConexion = Conexion::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT * FROM ahijados");                 
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 return $Tabla;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function CantidadInvitacionesEnviadas()
        {
            return count(self::InvitacionesEnviadas());
        }
        public function UsuarioQueMasInvitaron($cantidad, $columna, $sentido)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT CONCAT(CONCAT(DUP.Apellido, ', '), DUP.Nombre) AS \"Nombre y apellido\", U.NombreUsuario as \"Nombre de usuario\", count(A.idUsuario) as Invitaciones FROM ahijados A
INNER JOIN datosusuariospersonales DUP ON DUP.idUsuario = A.idUsuario
INNER JOIN usuarios U ON A.idUsuario = U.idUsuario
GROUP BY \"Nombre y apellido\", U.NombreUsuario
ORDER BY ".$columna." ".$sentido."
LIMIT 0," . $cantidad);                 
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 return $Tabla;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
		
		
		public function UsuariosQueInvitaronCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);

            $oConexion = Conexion::nuevo();
            try
            {
				$oConexion->Abrir_Trans();
				$oConexion->setQuery("SELECT count(distinct A.idUsuario) as total 
									  FROM ahijados A
									  INNER JOIN datosusuariospersonales DUP ON DUP.idUsuario = A.idUsuario
									  INNER JOIN usuarios U ON A.idUsuario = U.idUsuario
									  WHERE ($filtersSQL) ");
				$Tabla = $oConexion->DevolverQuery();
				$oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
		
		public function UsuariosQueInvitaronPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		    $filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
            $oConexion = Conexion::nuevo();
            try
            {
			
                $oConexion->Abrir_Trans();
				$oConexion->setQuery("SELECT 	A.idUsuario as 'A.idUsuario', 
												DUP.Apellido as 'DUP.Apellido', 
												DUP.Nombre as 'DUP.Nombre',
												U.NombreUsuario as 'U.NombreUsuario', 
												count(A.idUsuario) as 'Invitaciones' 
									  FROM ahijados A
									  INNER JOIN datosusuariospersonales DUP ON DUP.idUsuario = A.idUsuario
									  INNER JOIN usuarios U ON A.idUsuario = U.idUsuario
									  WHERE ($filtersSQL) 
									  GROUP BY A.idUsuario 
									  ORDER BY $sortSQL 
									  LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }

        public function UsuariosQueInvitaron($columna, $sentido)
        {
            
			$oQuery = "SELECT A.idUsuario, CONCAT(CONCAT(DUP.Apellido, ', '), DUP.Nombre) AS \"Nombre y apellido\", U.NombreUsuario as \"Nombre de usuario\", count(A.idUsuario) as Invitaciones 
                       FROM ahijados A
                       INNER JOIN datosusuariospersonales DUP ON DUP.idUsuario = A.idUsuario
                       INNER JOIN usuarios U ON A.idUsuario = U.idUsuario
                       GROUP BY A.idUsuario
                       ORDER BY ".$columna." ".$sentido;
			$cacheId = "QueryUsuariosQueInvitaron".$columna.$sentido;
			
			$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
	        $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 28800, 'automatic_serialization' => true), array('cache_dir' => $cacheDir));

			if(!$contenidoCache = $cache->load($cacheId)) {
				$oConexion = Conexion::nuevo();
	            try
	            {
	                $oConexion->Abrir_Trans();
	                $oConexion->setQuery($oQuery);
	                $Tabla = $oConexion->DevolverQuery();
	                $oConexion->Cerrar_Trans();

					$cache->save($Tabla, $cacheId);
	                $contenidoCache = $Tabla;
	            }
	            catch(exception $e)
	            {
	                throw $e;
	            }				
			}
			return $contenidoCache;
        }

        public function ObtenerPromedioInvitaciones()
        {
			$oQuery = "SELECT AVG(Cantidad) as Promedio 
			           FROM ( SELECT idUsuario, COUNT(idUsuario) as Cantidad 
					          FROM ahijados GROUP BY idUsuario ) B";
			$cacheId = "QueryObtenerPromedioInvitaciones";

			$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
	        $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 28800, 'automatic_serialization' => true), array('cache_dir' => $cacheDir));

			if(!$contenidoCache = $cache->load($cacheId)) {
	            $oConexion = Conexion::nuevo();
	            try
	            {
	                $oConexion->Abrir_Trans();
	                $oConexion->setQuery($oQuery);
	                $Tabla = $oConexion->DevolverQuery();
	                $oConexion->Cerrar_Trans();
					 
					$cache->save($Tabla[0]["Promedio"], $cacheId);
	                $contenidoCache = $Tabla[0]["Promedio"];
	            }
	            catch(exception $e)
	            {
	                throw $e;
	            }
			}
			return $contenidoCache;
        }
		
        public function ObtenerDesvioEstandarInvitaciones()
        {
			$oQuery = "SELECT STDDEV(Cantidad) as DesvioEstandar 
			           FROM ( SELECT idUsuario, COUNT(idUsuario) as Cantidad 
					          FROM ahijados GROUP BY idUsuario ) B";
			$cacheId = "QueryObtenerDesvioEstandarInvitaciones";

			$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
	        $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 28800, 'automatic_serialization' => true), array('cache_dir' => $cacheDir));

			if(!$contenidoCache = $cache->load($cacheId)) {
	            $oConexion = Conexion::nuevo();
	            try
	            {
	                 $oConexion->Abrir_Trans();
	                 $oConexion->setQuery($oQuery);                 
	                 $Tabla = $oConexion->DevolverQuery();
	                 $oConexion->Cerrar_Trans();
					 
					$cache->save($Tabla[0]["DesvioEstandar"], $cacheId);
	                $contenidoCache = $Tabla[0]["DesvioEstandar"];
	            }
	            catch(exception $e)
	            {
	                throw $e;
	            }
			}
			return $contenidoCache;
        }
    }
	
    class Logins
    {
        public function TablaEstadistica()
        {
             try
            {
                $DatosEstadisticos = array();
                $DatosEstadisticos[0]["id"] = 0;
                $DatosEstadisticos[0]["Descripcion"] = "D�a con m�s logins";
                    $DiaMaxLogs = self::getDiaMaxLogins();
                $DatosEstadisticos[0]["D�a"] = $DiaMaxLogs["DiaMax"];
                $DatosEstadisticos[0]["Cantidad"] = $DiaMaxLogs["Cantidad"];
                
                $DatosEstadisticos[1]["id"] = 1;
                    $LogsHoy = self::getCantidadLoginsHoy();                                                        
                $DatosEstadisticos[1]["Descripcion"] = "Cantidad de logins";
                $DatosEstadisticos[1]["D�a"] = $LogsHoy["Dia"];
                $DatosEstadisticos[1]["Cantidad"] = $LogsHoy["Cantidad"];
                
                $DatosEstadisticos[2]["id"] = 2;
                    $PxD = self::getPromedioLogins_x_Dia();
                $DatosEstadisticos[2]["Descripcion"] = "Promedio de logins por d�a";
                $DatosEstadisticos[2]["D�a"] = "-";
                $DatosEstadisticos[2]["Cantidad"] = $PxD["PxD"];
/*                
                $DatosEstadisticos[1]["id"] = 1;
                $DatosEstadisticos[1]["Descripcion"] = "Clientes activos";
                $DatosEstadisticos[1]["Total"] = self::CantidadUsuarios(self::USUARIOS_ACTIVOS);
                $DatosEstadisticos[1]["%"] = round((self::CantidadUsuarios(self::USUARIOS_ACTIVOS)*100)/self::CantidadUsuarios(self::TODOS_LOS_USUARIOS),2). "%";
                
                $DatosEstadisticos[2]["id"] = 2;
                $DatosEstadisticos[2]["Descripcion"] = "Clientes inactivos";
                $DatosEstadisticos[2]["Total"] = self::CantidadUsuarios(self::USUARIOS_INACTIVOS);
                $DatosEstadisticos[2]["%"] = round((self::CantidadUsuarios(self::USUARIOS_INACTIVOS)*100)/self::CantidadUsuarios(self::TODOS_LOS_USUARIOS),2). "%";*/
                return $DatosEstadisticos;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function getDiaMaxLogins()
        {
            $oConexion = ConexionHistorico::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT DATE(Fecha) as DiaMax, count(idLogin) as Cantidad FROM ".Aplicacion::getParametros("conexionH","base").".logins
GROUP BY DATE(Fecha)
ORDER BY 2 DESC
LIMIT 0,1");
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 return $Tabla[0];
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function getCantidadLoginsHoy()
        {
            $oConexion = ConexionHistorico::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT DATE(Fecha) as Dia, count(idLogin) as Cantidad FROM ".Aplicacion::getParametros("conexionH","base").".logins WHERE DATE(Fecha) = DATE(NOW())
GROUP BY DATE(Fecha)");
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 if (!$Tabla[0]["Dia"])
                    $Tabla[0]["Dia"] = date("Y-m-d");
                 if (!$Tabla[0]["Cantidad"])
                    $Tabla[0]["Cantidad"] = 0;
                 return $Tabla[0];
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function getPromedioLogins_x_Dia()
        {
            $oConexion = ConexionHistorico::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT AVG (CANT) AS PxD FROM (SELECT DATE(Fecha), COUNT(idLogin) AS CANT FROM ".Aplicacion::getParametros("conexionH","base").".logins GROUP BY DATE(Fecha)) AS T");
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 return $Tabla[0];
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
    }
    class Registros
    {
        public function TablaEstadistica()
        {
			
            $DatosEstadisticos = array();
			/*
                $DatosEstadisticos[3]["id"] = 0;
                $DatosEstadisticos[3]["Descripcion"] = "D�a con m�s invitaciones";
                    $DiaMaxRef = self::getDiaMaxRegistrados();
                $DatosEstadisticos[3]["D�a"] = $DiaMaxRef["DiaMax"];
                $DatosEstadisticos[3]["Cantidad"] = $DiaMaxRef["Cantidad"];
               
			   
                $DatosEstadisticos[4]["id"] = 1;
                    $RefHoy = self::getCantidadRegistradosHoy();                                                        
                $DatosEstadisticos[4]["Descripcion"] = "Cantidad de invitaciones";
                $DatosEstadisticos[4]["D�a"] = $RefHoy["Dia"];
                $DatosEstadisticos[4]["Cantidad"] = $RefHoy["Cantidad"];
               
				
                $DatosEstadisticos[5]["id"] = 2;
                    $RxD = self::getPromedioRegistrados_x_Dia();
                $DatosEstadisticos[5]["Descripcion"] = "Promedio de invitaciones por d�a";
                $DatosEstadisticos[5]["D�a"] = "-";
                $DatosEstadisticos[5]["Cantidad"] = $RxD["PxD"];
                 */
				 
                return $DatosEstadisticos;
            
        }
        public function getCantidadRegistradosHoy()
        {
            $oConexion = ConexionHistorico::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT DATE(Fecha) AS Dia, COUNT(id) AS Cantidad FROM ".Aplicacion::getParametros("conexionH","base").".referenciados WHERE DATE(Fecha) = DATE(NOW()) GROUP BY DATE(Fecha) ");
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 if (!$Tabla[0]["Dia"])
                    $Tabla[0]["Dia"] = date("Y-m-d");
                 if (!$Tabla[0]["Cantidad"])
                    $Tabla[0]["Cantidad"] = 0;
                 return $Tabla[0];
            }
            catch(exception $e)
            {
                throw $e;
            }            
        }    
        public function getPromedioRegistrados_x_Dia()
        {
            $oConexion = ConexionHistorico::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT AVG (CANT) AS PxD FROM (SELECT DATE(Fecha), COUNT(id) AS CANT FROM ".Aplicacion::getParametros("conexionH","base").".referenciados GROUP BY DATE(Fecha)) AS T");
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 return $Tabla[0];
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function getDiaMaxRegistrados()
        {
            $oConexion = ConexionHistorico::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT DATE(Fecha) as DiaMax, count(id) as Cantidad FROM ".Aplicacion::getParametros("conexionH","base").".referenciados
GROUP BY DATE(Fecha)
ORDER BY 2 DESC
LIMIT 0,1");
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->Cerrar_Trans();
                 return $Tabla[0];
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
    }
?>
