<?php

class dmEstadisticasGenerales {

    public function getUsuariosActivosPorFecha($dia, $mes, $anio, $offset, $itemsPorPagina, $paramArray = array(), $sortArray = array()) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
		$sortSQL = ListadoUtils::generateSortSQL($sortArray);
		$limitSQL = $offset.",".$itemsPorPagina;

        $oConexion = Conexion::nuevo();
        try {
             $oConexion->Abrir_Trans();
             $oConexion->setQuery(
        	     "SELECT `usuarios`.`IdUsuario` as IdUsuario, `usuarios`.`NombreUsuario` as NombreUsuario, `usuarios`.`Padrino` as Padrino, `usuarios`.`FechaIngreso` as FechaIngreso, datos.`Nombre`, datos.`Apellido` as Apellido, datos.`Email` as Email, TIME(usuarios.FechaIngreso) as Hora,
        	     IF(es_activa=0,'Activo',IF(es_Provisoria = 0, 'Desuscripto', IF(Padrino = 'Huerfano', 'Huerfano', 'Inactivo'))) as 'Estado'
				  FROM `usuarios`
				  LEFT JOIN `datosusuariospersonales` datos ON `usuarios`.IdUsuario = datos.IdUsuario
				  WHERE `usuarios`.`FechaIngreso` BETWEEN '".$anio."-".$mes."-".$dia." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:'')." 
				  ORDER BY ".$sortSQL."
			 	  LIMIT ".$limitSQL);
             $Tabla = $oConexion->DevolverQuery();
             $oConexion->Cerrar_Trans();
             return $Tabla;
        } catch(exception $e) {
           throw $e;
        }
    }
    
    public function getUsuariosActivosPorFechaCount($dia, $mes, $anio, $paramArray = array()) {
    	$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);

        $oConexion = Conexion::nuevo();
        try {
            $oConexion->Abrir_Trans();
            $oConexion->setQuery(
        		"SELECT count(usuarios.IdUsuario) as numberOfRows
				 FROM `usuarios`
				 LEFT JOIN `datosusuariospersonales` datos ON `usuarios`.IdUsuario = datos.IdUsuario
				 WHERE `usuarios`.`FechaIngreso` BETWEEN '".$anio."-".$mes."-".$dia."-"." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:''));
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            return $Tabla[0];
        } catch(exception $e) {
        	throw $e;
        }
    }
    
    public function getLoginsPorFecha($dia, $mes, $anio, $offset, $itemsPorPagina, $paramArray = array(), $sortArray = array()) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
		$sortSQL = ListadoUtils::generateSortSQL($sortArray);
		$limitSQL = $offset.",".$itemsPorPagina;
    	
        $oConexion = ConexionHistorico::nuevo();
        try {
             $oConexion->Abrir_Trans();
             $oConexion->setQuery(
             	"SELECT idLogin as IdLogin, NombreUsuario as NombreUsuario, Nombre as Nombre, Apellido as Apellido, TIME(Fecha) as Hora
				 FROM ".Aplicacion::getParametros("conexionH","base").".logins
	             WHERE `Fecha` BETWEEN '".$anio."-".$mes."-".$dia." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:'')." 
	             ORDER BY ".$sortSQL."
				 LIMIT ".$limitSQL);
             $Tabla = $oConexion->DevolverQuery();
             $oConexion->Cerrar_Trans();
             return $Tabla;
        } catch(exception $e) {
           throw $e;
        }
    }
    
    public function getLoginsPorFechaCount($dia, $mes, $anio, $paramArray = array()) {
    	$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
    	
        $oConexion = ConexionHistorico::nuevo();
        try {
            $oConexion->Abrir_Trans();
            $oConexion->setQuery(
            	"SELECT count(idLogin) as numberOfRows
				 FROM ".Aplicacion::getParametros("conexionH","base").".logins
				 WHERE `Fecha` BETWEEN '".$anio."-".$mes."-".$dia." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:''));
                 
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            return $Tabla[0];
        } catch(exception $e) {
        	throw $e;
        }
    }
    
    public function getInvitacionesPorFecha($dia, $mes, $anio, $offset, $itemsPorPagina, $paramArray = array(), $sortArray = array()) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
		$sortSQL = ListadoUtils::generateSortSQL($sortArray);
		$limitSQL = $offset.",".$itemsPorPagina;
    	
        $oConexion = Conexion::nuevo();

        try {
             $oConexion->Abrir_Trans();
             $oConexion->setQuery(
             	"SELECT invitation_from, invitation_to, TIME(invitation_date) as Hora
				 FROM `geelbe_sent_invitations`
				 WHERE invitation_date BETWEEN '".$anio."-".$mes."-".$dia." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:'')."
				 ORDER BY ".$sortSQL."
			     LIMIT ".$limitSQL);
             $Tabla = $oConexion->DevolverQuery();
             $oConexion->Cerrar_Trans();
             return $Tabla;
        } catch(exception $e) {
           throw $e;
        }
    }
    
    public function getInvitacionesPorFechaCount($dia, $mes, $anio, $paramArray = array()) {
    	$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
    	
        $oConexion = Conexion::nuevo();
        try {
            $oConexion->Abrir_Trans();
            $oConexion->setQuery(
            	"SELECT count(id) as numberOfRows
				 FROM `geelbe_sent_invitations`
				 WHERE invitation_date BETWEEN '".$anio."-".$mes."-".$dia." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:''));
                 
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            return $Tabla[0];
        } catch(exception $e) {
        	throw $e;
        }
    }
    
	public function getUsuarioActivoPorFecha($dia, $mes, $anio, $offset, $itemsPorPagina, $paramArray = array(), $sortArray = array()) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
		$sortSQL = ListadoUtils::generateSortSQL($sortArray);
		$limitSQL = $offset.",".$itemsPorPagina;

        $oConexion = Conexion::nuevo();
        try {
             $oConexion->Abrir_Trans();
             $oConexion->setQuery(
        	     "SELECT `usuarios`.`IdUsuario` as IdUsuario, `usuarios`.`NombreUsuario` as NombreUsuario, `usuarios`.`Padrino` as Padrino, `usuarios`.`FechaIngreso` as FechaIngreso, datos.`Nombre`, datos.`Apellido` as Apellido, datos.`Email` as Email, TIME(usuarios.FechaIngreso) as Hora,
        	     IF(es_activa=0,'Activo',IF(es_Provisoria = 0, 'Desuscripto', IF(Padrino = 'Huerfano', 'Huerfano', 'Inactivo'))) as 'Estado'
				  FROM `usuarios`
				  LEFT JOIN `datosusuariospersonales` datos ON `usuarios`.IdUsuario = datos.IdUsuario
				  WHERE `usuarios`.`es_activa` = 0 AND `usuarios`.`IdPerfil` = 1 AND `usuarios`.`FechaIngreso` BETWEEN '".$anio."-".$mes."-".$dia." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:'')." 
				  ORDER BY ".$sortSQL."
			 	  LIMIT ".$limitSQL);
             $Tabla = $oConexion->DevolverQuery();
             $oConexion->Cerrar_Trans();
             return $Tabla;
        } catch(exception $e) {
           throw $e;
        }
    }
    
 	public function getUsuarioActivoPorFechaCount($dia, $mes, $anio, $paramArray = array()) {
    	$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);

        $oConexion = Conexion::nuevo();
        try {
            $oConexion->Abrir_Trans();
            $oConexion->setQuery(
        		"SELECT count(usuarios.IdUsuario) as numberOfRows
				 FROM `usuarios`
				 LEFT JOIN `datosusuariospersonales` datos ON `usuarios`.IdUsuario = datos.IdUsuario
				 WHERE `usuarios`.`es_activa` = 0 AND `usuarios`.`IdPerfil` = 1 AND `usuarios`.`FechaIngreso` BETWEEN '".$anio."-".$mes."-".$dia."-"." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:''));
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            return $Tabla[0];
        } catch(exception $e) {
        	throw $e;
        }
    }
    
	public function getUsuarioInactivoPorFecha($dia, $mes, $anio, $offset, $itemsPorPagina, $paramArray = array(), $sortArray = array()) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
		$sortSQL = ListadoUtils::generateSortSQL($sortArray);
		$limitSQL = $offset.",".$itemsPorPagina;

        $oConexion = Conexion::nuevo();
        try {
             $oConexion->Abrir_Trans();
             $oConexion->setQuery(
        	     "SELECT `usuarios`.`IdUsuario` as IdUsuario, `usuarios`.`NombreUsuario` as NombreUsuario, `usuarios`.`Padrino` as Padrino, `usuarios`.`FechaIngreso` as FechaIngreso, datos.`Nombre`, datos.`Apellido` as Apellido, datos.`Email` as Email, TIME(usuarios.FechaIngreso) as Hora,
        	     IF(es_activa=0,'Activo',IF(es_Provisoria = 0, 'Desuscripto', IF(Padrino = 'Huerfano', 'Huerfano', 'Inactivo'))) as 'Estado'
				  FROM `usuarios`
				  LEFT JOIN `datosusuariospersonales` datos ON `usuarios`.IdUsuario = datos.IdUsuario
				  WHERE `usuarios`.`es_activa` = 1 AND `usuarios`.`es_Provisoria` = 1 AND `usuarios`.`Padrino` <> 'Huerfano' AND `usuarios`.`IdPerfil` = 1 AND `usuarios`.`FechaIngreso` BETWEEN '".$anio."-".$mes."-".$dia." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:'')." 
				  ORDER BY ".$sortSQL."
			 	  LIMIT ".$limitSQL);
             $Tabla = $oConexion->DevolverQuery();
             $oConexion->Cerrar_Trans();
             return $Tabla;
        } catch(exception $e) {
           throw $e;
        }
    }
    
 	public function getUsuarioInactivoPorFechaCount($dia, $mes, $anio, $paramArray = array()) {
    	$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);

        $oConexion = Conexion::nuevo();
        try {
            $oConexion->Abrir_Trans();
            $oConexion->setQuery(
        		"SELECT count(usuarios.IdUsuario) as numberOfRows
				 FROM `usuarios`
				 LEFT JOIN `datosusuariospersonales` datos ON `usuarios`.IdUsuario = datos.IdUsuario
				 WHERE `usuarios`.`es_activa` = 1 AND `usuarios`.`es_Provisoria` = 1 AND `usuarios`.`Padrino` <> 'Huerfano' AND `usuarios`.`IdPerfil` = 1 AND `usuarios`.`FechaIngreso` BETWEEN '".$anio."-".$mes."-".$dia."-"." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:''));

            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            return $Tabla[0];
        } catch(exception $e) {
        	throw $e;
        }
    }
    
	public function getUsuarioHuerfanoPorFecha($dia, $mes, $anio, $offset, $itemsPorPagina, $paramArray = array(), $sortArray = array()) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
		$sortSQL = ListadoUtils::generateSortSQL($sortArray);
		$limitSQL = $offset.",".$itemsPorPagina;

        $oConexion = Conexion::nuevo();
        try {
             $oConexion->Abrir_Trans();
             $oConexion->setQuery(
        	     "SELECT `usuarios`.`IdUsuario` as IdUsuario, `usuarios`.`NombreUsuario` as NombreUsuario, `usuarios`.`Padrino` as Padrino, `usuarios`.`FechaIngreso` as FechaIngreso, datos.`Nombre`, datos.`Apellido` as Apellido, datos.`Email` as Email, TIME(usuarios.FechaIngreso) as Hora,
        	     IF(es_activa=0,'Activo',IF(es_Provisoria = 0, 'Desuscripto', IF(Padrino = 'Huerfano', 'Huerfano', 'Inactivo'))) as 'Estado'
				  FROM `usuarios`
				  LEFT JOIN `datosusuariospersonales` datos ON `usuarios`.IdUsuario = datos.IdUsuario
				  WHERE `usuarios`.`es_activa` = 1 AND `usuarios`.`es_Provisoria` = 1 AND `usuarios`.`Padrino` = 'Huerfano' AND `usuarios`.`IdPerfil` = 1 AND `usuarios`.`FechaIngreso` BETWEEN '".$anio."-".$mes."-".$dia." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:'')." 
				  ORDER BY ".$sortSQL."
			 	  LIMIT ".$limitSQL);
             $Tabla = $oConexion->DevolverQuery();
             $oConexion->Cerrar_Trans();
             return $Tabla;
        } catch(exception $e) {
           throw $e;
        }
    }
    
 	public function getUsuarioHuerfanoPorFechaCount($dia, $mes, $anio, $paramArray = array()) {
    	$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);

        $oConexion = Conexion::nuevo();
        try {
            $oConexion->Abrir_Trans();
            $oConexion->setQuery(
        		"SELECT count(usuarios.IdUsuario) as numberOfRows
				 FROM `usuarios`
				 LEFT JOIN `datosusuariospersonales` datos ON `usuarios`.IdUsuario = datos.IdUsuario
				 WHERE `usuarios`.`es_activa` = 1 AND `usuarios`.`es_Provisoria` = 1 AND `usuarios`.`Padrino` = 'Huerfano' AND `usuarios`.`IdPerfil` = 1 AND `usuarios`.`FechaIngreso` BETWEEN '".$anio."-".$mes."-".$dia."-"." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:''));																					
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            return $Tabla[0];
        } catch(exception $e) {
        	throw $e;
        }
    }
    
	public function getUsuarioDesuscriptoPorFecha($dia, $mes, $anio, $offset, $itemsPorPagina, $paramArray = array(), $sortArray = array()) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
		$sortSQL = ListadoUtils::generateSortSQL($sortArray);
		$limitSQL = $offset.",".$itemsPorPagina;

        $oConexion = Conexion::nuevo();
        try {
             $oConexion->Abrir_Trans();
             $oConexion->setQuery(
        	     "SELECT `usuarios`.`IdUsuario` as IdUsuario, `usuarios`.`NombreUsuario` as NombreUsuario, `usuarios`.`Padrino` as Padrino, `usuarios`.`FechaIngreso` as FechaIngreso, datos.`Nombre`, datos.`Apellido` as Apellido, datos.`Email` as Email, TIME(usuarios.FechaIngreso) as Hora,
        	     IF(es_activa=0,'Activo',IF(es_Provisoria = 0, 'Desuscripto', IF(Padrino = 'Huerfano', 'Huerfano', 'Inactivo'))) as 'Estado'
				  FROM `usuarios`
				  LEFT JOIN `datosusuariospersonales` datos ON `usuarios`.IdUsuario = datos.IdUsuario
				  WHERE `usuarios`.`es_activa` = 1 AND `usuarios`.`es_Provisoria` = 0 AND `usuarios`.`IdPerfil` = 1 AND `usuarios`.`FechaIngreso` BETWEEN '".$anio."-".$mes."-".$dia." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:'')." 
				  ORDER BY ".$sortSQL."
			 	  LIMIT ".$limitSQL);
             $Tabla = $oConexion->DevolverQuery();
             $oConexion->Cerrar_Trans();
             return $Tabla;
        } catch(exception $e) {
           throw $e;
        }
    }
    
 	public function getUsuarioDesuscriptoPorFechaCount($dia, $mes, $anio, $paramArray = array()) {
    	$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);

        $oConexion = Conexion::nuevo();
        try {
            $oConexion->Abrir_Trans();
            $oConexion->setQuery(
        		"SELECT count(usuarios.IdUsuario) as numberOfRows
				 FROM `usuarios`
				 LEFT JOIN `datosusuariospersonales` datos ON `usuarios`.IdUsuario = datos.IdUsuario
				 WHERE `usuarios`.`es_activa` = 1 AND `usuarios`.`es_Provisoria` = 0 AND `usuarios`.`IdPerfil` = 1 AND `usuarios`.`FechaIngreso` BETWEEN '".$anio."-".$mes."-".$dia."-"." 00:00:00' AND '".$anio."-".$mes."-".$dia." 23:59:59' ".($filtersSQL!=''?"AND ".$filtersSQL:''));
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            return $Tabla[0];
        } catch(exception $e) {
        	throw $e;
        }
    }
}