<?php
    class PartnersCod
    {
        private $_idcodigobase;
        private $_idpartner;
        private $_codigo;
        private $_cantidad;
        private $_auto;
        private $_pixel_registro;
        private $_pixel_activacion;
       
        public function __construct()
        {
            $this->_idcodigobase = "";
            $this->_idpartner = "";
            $this->_codigo = "";
            $this->_cantidad = "";
            $this->_auto = "";
            $this->_pixel_registro = "";
            $this->_pixel_activacion = "";
        }
        public function getIdCodigoBase()
        {
            return $this->_idcodigobase;
        }
        public function setIdCodigoBase($idlocal)
        {
            $this->_idcodigobase = $idlocal;
        }
        public function getIdPartner()
        {
            return $this->_idpartner;
        }
        public function setIdPartner($idlocal)
        {
            $this->_idpartner = $idlocal;
        }
        public function getCodigo()
        {
            return $this->_codigo;
        }
        public function setCodigo($descripcion)
        {
            $this->_codigo = $descripcion;
        }
        public function getCantidad()
        {
            return $this->_cantidad;
        }
        public function setCantidad($descripcion)
        {
            $this->_cantidad = $descripcion;
        }
        public function getAuto()
        {
            return $this->_auto;
        }
        public function setAuto($descripcion)
        {
            $this->_auto = $descripcion;
        }
        public function getPixelRegistro()
        {
            return $this->_pixel_registro;
        }
        public function setPixelRegistro($descripcion)
        {
            $this->_pixel_registro = $descripcion;
        }
        public function getPixelActivacion()
        {
            return $this->_pixel_activacion;
        }
        public function setPixelActivacion($descripcion)
        {
            $this->_pixel_activacion = $descripcion;
        }
        
        static function pixel($pixel, $IdUsuario, $NombreUsuario) {
	        
	        $pixel = str_replace('[[[IdUsuario]]]', $IdUsuario, $pixel);
	        $pixel = str_replace('[[[NombreUsuario]]]', $NombreUsuario, $pixel);
	        return $pixel;
	        
        }
    }
?>
