<?php
    class dmPartnersCod
    {
        public static function getAll($columna = 'codigo', $sentido = 'asc', $filtro="")
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdCodigoBase, IdPartner, Codigo, Cantidad, Auto FROM partners_codigos_base ".($filtro != '' ? "WHERE Codigo LIKE \"%".$filtro."%\"" : '')." ORDER BY " . $columna . " " . $sentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                /*$arrLocales = array();
                foreach($Tabla as $Fila)
                {
                    $arrLocales[] = new Locales();
                    $arrLocales[count($arrLocales) - 1]->setIdLocal($Fila["idLocal"]);
                    $arrLocales[count($arrLocales) - 1]->setDescripcion($Fila["Descripcion"]);
                    $arrLocales[count($arrLocales) - 1]->setDireccion($Fila["Direccion"]);
                    $arrLocales[count($arrLocales) - 1]->setIdProvincia($Fila["idProvincia"]);
                }
                return $arrLocales;*/
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        public static function save($oLocal)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                if ($oLocal->getIdCodigoBase() == "") {
                    $oConexion->setQuery("INSERT INTO partners_codigos_base VALUES('', '".htmlspecialchars(mysql_real_escape_string($oLocal->getCodigo()), ENT_QUOTES)."', '".$oLocal->getCantidad()."', '".$oLocal->getIdPartner()."', '".$oLocal->getAuto()."', NOW(), '".$oLocal->getPixelRegistro()."', '".$oLocal->getPixelActivacion()."')");
                } else
                    $oConexion->setQuery("UPDATE partners_codigos_base SET Codigo = '".$oLocal->getCodigo()."', Cantidad = '".$oLocal->getCantidad()."', IdPartner = '".$oLocal->getIdPartner()."', Auto = '".$oLocal->getAuto()."', pixel_registro = '".$oLocal->getPixelRegistro()."', pixel_activacion = '".$oLocal->getPixelActivacion()."' WHERE IdCodigoBase = " . $oLocal->getIdCodigoBase());
                $oConexion->EjecutarQuery();
                
                if($oLocal->getIdCodigoBase() == "")
                	$oLocal->setIdCodigoBase($oConexion->LastId());

                $oConexion->Cerrar_Trans();
                	
                return $oLocal->getIdCodigoBase();
            }
            catch(MySQLException $e)
            {
                throw $e;
            }            
        }
        public static function getById($idLocal)
        {
            $oConexion = Conexion::nuevo();
            $oLocal = new PartnersCod();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM partners_codigos_base WHERE IdCodigoBase = " . $idLocal);
                $Fila = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                
                    $oLocal->setIdCodigoBase($Fila[0]["IdCodigoBase"]);
                    $oLocal->setCodigo($Fila[0]["Codigo"]);
                    $oLocal->setIdPartner($Fila[0]["IdPartner"]);
                    $oLocal->setAuto($Fila[0]["Auto"]);
                    $oLocal->setCantidad($Fila[0]["Cantidad"]);
                    $oLocal->setPixelRegistro($Fila[0]["pixel_registro"]);
                    $oLocal->setPixelActivacion($Fila[0]["pixel_activacion"]);

                }
                catch(MySQLException $e)
                {
                    throw $e;
                }
                return $oLocal;
            }
            
       	public static function delete($obj)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("DELETE FROM partners_codigos_base WHERE IdCodigoBase = ".$obj->getIdCodigoBase());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>
