<?
	
	include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/datamappers/dmbackground.php";
	require_once 'Zend/Cache.php';
	
	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
	$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 900), array('cache_dir' => $cacheDir));
	
	$hournow  = strtotime(date("H:i:s"));

	if(!$cacheBackground = $cache->load('geelbe_dayhour_background')) {
		$cacheBackground = dmBackground::getBackground_ById("day"); ;
		$cache->save($cacheBackground, 'geelbe_dayhour_background');
	}
	$dayhour = $cacheBackground;

	if(!$cacheBackground = $cache->load('geelbe_nighthour_background')) {
		$cacheBackground = dmBackground::getBackground_ById("night");
		$cache->save($cacheBackground, 'geelbe_nighthour_background');
	}
	$nighthour = $cacheBackground;
		
	if(!$textColorDay = $cache->load('geelbe_day_text_color')) {
		$textColorDay = dmBackground::getTextColor_ById("day");
		if($textColorDay != null) {
			$cache->save($textColorDay, 'geelbe_day_text_color');
		}
	}
		
	if(!$textColorNight = $cache->load('geelbe_night_text_color')) {
		$textColorNight = dmBackground::getTextColor_ById("night");
		if($textColorNight != null) {
			$cache->save($textColorNight, 'geelbe_night_text_color');
		}
	}

	$day = strtotime($dayhour.":00:00");
	$night = strtotime($nighthour.":00:00");
	
	IF ($hournow >= $day && $hournow <= $night){
	echo 
		'<style type="text/css">
		body {
			background-image:url('.UrlResolver::getImgBaseUrl("front/archivos/background_day.jpg").');
			background-position: top center;
			background-attachment: fixed;
		}
		</style>';

		if($textColorDay != null) {
			echo 
				'<style type="text/css">
				#titular {
					color:'.$textColorDay.';
				}
				</style>';
		}
	}else{
	echo 
		'<style type="text/css">
		body {
			background-image:url('.UrlResolver::getImgBaseUrl("front/archivos/background_night.jpg").');
			background-position: top center;
			background-attachment: fixed;
		}
		</style>';
	
		if($textColorNight != null) {
			echo 
				'<style type="text/css">
				#titular {
					color:'.$textColorNight.';
				}
				</style>';
		}
	}
	

?>