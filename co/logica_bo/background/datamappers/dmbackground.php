<?
 class dmBackground {
		
		public static function Insert($hour = 0, $type = null, $textColor = "") 
		{		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir_Trans();
			    $cn->setQuery("UPDATE backgrounds SET 
				                   Start = ". mysql_real_escape_string($hour) .", text_color = \"".mysql_real_escape_string($textColor)."\"
								   WHERE IdBackground = '".$type."'");
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return 1;
		    }
		    catch(exception $e)
		    {
			    $cn->RollBack();
			    throw $e;
		    }		
	    }
  
		public static function getBackground_ById($type = null){
			$cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
                $cn->setQuery("SELECT Start as Start from backgrounds where IdBackground = '".mysql_real_escape_string($type)."'");
				$Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();
			    return $Tabla[0]['Start'];
		    }
		    catch(exception $e) {			
			    throw $e;                   
		    }		
	    }
  
		public static function getTextColor_ById($type = null){
			$cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
                $cn->setQuery("SELECT text_color as text_color from backgrounds where IdBackground = '".mysql_real_escape_string($type)."'");
				$Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();
			    return $Tabla[0]['text_color'];
		    }
		    catch(exception $e) {			
			    throw $e;                   
		    }		
	    }
		
		public static function Background_save_img($filename = null, $type = null, $country = null){
			if ($type == "day"){
				$imgname = "background_day";
			} else if ($type == "night"){
				$imgname = "background_night";
			} else  {
				$imgname = "";
			}
		
			if(isset($_FILES[$filename]) && is_uploaded_file($_FILES[$filename]['tmp_name'])) {
				$destFolder = $_SERVER['DOCUMENT_ROOT']."/".$country."/front/archivos/";
		       	move_uploaded_file($_FILES[$filename]["tmp_name"], $destFolder.$imgname.".jpg");
		    }
		}
}
?>