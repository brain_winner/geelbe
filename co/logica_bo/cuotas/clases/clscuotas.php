<?
    class MySQL_Cuotas extends cuotas
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_idtarjeta")
                {   
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=($valor != 0) ? chr(34).mysql_real_escape_string($valor).chr(34).", " : "NULL, ";
                }
                else
                {   
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
            }
            return "INSERT INTO cuotas (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getDelete()
        {
            return "DELETE FROM cuotas WHERE IdCuota = ".$this->getIdCuota();
        }        
        public function getById($Id)
        {
            $strCampos="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM cuotas WHERE IdCuota = ".mysql_real_escape_string($Id);
        }
        public function getByIdTarjeta($Id)
        {
            $strCampos="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM cuotas WHERE IdTarjeta = ".mysql_real_escape_string($Id);
        }
    }
?>