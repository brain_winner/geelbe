<?php
    if($_POST)
    {
        ob_start();
        extract($_POST);
        try
        {   
            $IdCuota = ($txtIdCuota == "0") ? 0 : $txtIdCuota;
            $objProveedor = new MySQL_Cuotas();
            $objProveedor->setIdCuota($IdCuota);
            $objProveedor->setCuota($txtCuota);
            $objProveedor->setIdTarjeta($txtIdTarjeta);
            $objProveedor->setPorcentaje($txtIntereses);
            dmCuota::save($objProveedor);
            ?>
                <script type="text/javascript">
                    window.irVolver();
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                   window.ErrorMSJ("<?=$e->getNumeroSQLERROR()?>", "");
                </script>
            <?
        }
        ob_flush();
    }
?>