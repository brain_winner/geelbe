<?
    class dmCuota
    {
        public static function save(MySQL_Cuotas $objProveedor)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objProveedor->getInsert());
                $oConexion->EjecutarQuery();
                if($objProveedor->getIdCuota() == 0)
                {
                    $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                    $ID = $oConexion->DevolverQuery();
                    $objProveedor->setIdCuota($ID[0]["Id"]);
                }
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function delete(MySQL_Cuotas $objProveedor)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objProveedor->getDelete());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getByIdCuota($Id)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $objProveedor = new MySQL_Cuotas();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objProveedor->getById($Id));
                $Tabla = $oConexion->DevolverQuery();
                foreach($Tabla as $fila)
                {
                    foreach ($fila as $atributo => $valor)
                    {	                    		
                        $objProveedor->__set($valor, $atributo);
                    }                    
                }
                $oConexion->Cerrar_Trans();
                return $objProveedor;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getCuotas($columnaNro, $columnaSentido, $filtro = "")
        {
            $oConexion = Conexion::nuevo();
            try
            {  
            
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT bxt.idCuota, bxt.Cuota, bxt.porcentaje as Intereses, t.Nombre as Tarjeta
                FROM cuotas bxt 
                LEFT JOIN tarjetas t ON bxt.IdTarjeta = t.idTarjeta
                ORDER BY ".$columnaNro." ".$columnaSentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>