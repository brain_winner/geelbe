<?php
    class dmProductos
    {
    	
    	public static function getProductosAx($IdCampania)
        {
     		$IdCampania = intval($IdCampania);
        	
            $Cnn = Conexion::nuevo();
            try
            {
                $Cnn->Abrir();
                
                $Cnn->setQuery("SET NAMES utf8");
				$Cnn->EjecutarQuery();
				
                $Cnn->setQuery("SELECT p.IdProducto, p.Nombre, ROUND((p.PCompra * 1.1),2) AS PInterno, PVenta, c.Nombre as Campania, p.Descripcion
								FROM productos AS p
								JOIN productos_x_categorias as pxc ON p.IdProducto = pxc.IdProducto
								JOIN categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
								JOIN campanias AS c ON c.IdCampania = cat.IdCampania
								WHERE c.FechaFin >= NOW() 
								AND p.Habilitado = 1
								AND c.IdCampania = ".mysql_real_escape_string($IdCampania));
                $Tabla = $Cnn->DevolverQuery();
                return $Tabla;
            }        
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        
    
        public static function getProductosDeCampa�a($IdCampania)
        {
     		$IdCampania = intval($IdCampania);
        	
            $Cnn = Conexion::nuevo();
            try
            {
                $Cnn->Abrir();
                
                $Cnn->setQuery("SET NAMES utf8");
				$Cnn->EjecutarQuery();
				
                $Cnn->setQuery("SELECT distinct(p.IdProducto) as IdProducto, p.Nombre as Nombre, p.Descripcion as Descripcion, p.pVenta as PrecioGeelbe, p.PVP as PrecioLista
								FROM productos AS p
								JOIN productos_x_categorias as pxc ON p.IdProducto = pxc.IdProducto
								JOIN categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
								JOIN campanias AS c ON c.IdCampania = cat.IdCampania
								WHERE p.Habilitado = 1
								AND c.IdCampania = ".mysql_real_escape_string($IdCampania));
                $Tabla = $Cnn->DevolverQuery();
                return $Tabla;
            }        
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        
        /**
         * trae los atributos para Ajax. solo campanias con 3 dias. de cerrado max.
         *
         * @param unknown_type $IdProducto
         * @return unknown
         */
    	public static function getAtributosAx($IdProducto)
        {
     		$IdProducto = intval($IdProducto);
        	
            $Cnn = Conexion::nuevo();
            try
            {
                $Cnn->Abrir();
                
                $Cnn->setQuery("SET NAMES utf8");
				$Cnn->EjecutarQuery();
                
                $Cnn->setQuery("SELECT PP.IdCodigoProdInterno, P.Nombre as NombreProducto, P.IdProducto, IF(attr.Nombre IS NULL,'Unico',attr.Nombre) AS Nombre, IF(PA.IdAtributoClase IS NULL,'1',PA.IdAtributoClase) AS IdAtributoClase, IF(PA.Valor IS NULL,'Unico',PA.Valor) AS Valor, PP.Stock, P.PVenta, PP.Incremento
								FROM productos_x_atributos PA
								RIGHT JOIN productos_x_proveedores PP ON PA.IdCodigoProdInterno = PP.IdCodigoProdInterno
								JOIN productos P ON PP.IdProducto = P.IdProducto
								JOIN productos_x_categorias as pxc ON P.IdProducto = pxc.IdProducto
								JOIN categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
								JOIN campanias AS c ON c.IdCampania = cat.IdCampania
               					LEFT JOIN atributos_x_clases AS axc ON PA.IdAtributoClase = axc.idAtributoClase
								LEFT JOIN atributos AS attr ON attr.IdAtributo = axc.IdAtributo
								WHERE PP.IdProducto = ".mysql_real_escape_string($IdProducto)." 
								AND c.FechaFin >= NOW() 
								AND P.Habilitado = 1
								ORDER BY PA.IdCodigoProdInterno");
                $Tabla = $Cnn->DevolverQuery();
                return $Tabla;
            }        
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        
    	
        
        public static function getIdCampaniaByIdCodigoProdInterno($IdCodigoProdInterno)
        {
        	
     		$IdCodigoProdInterno = intval($IdCodigoProdInterno);
        	
            $Cnn = Conexion::nuevo();
            try
            {
                $Cnn->Abrir();
                
                $Cnn->setQuery("SET NAMES utf8");
				$Cnn->EjecutarQuery();
				
                $Cnn->setQuery("SELECT c.IdCampania
                				FROM productos_x_proveedores  as pxp 
                				JOIN  productos AS p ON pxp.IdProducto = p.IdProducto
								JOIN productos_x_categorias as pxc ON p.IdProducto = pxc.IdProducto
								JOIN categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
								JOIN campanias AS c ON c.IdCampania = cat.IdCampania
								WHERE c.FechaFin >= NOW()
								AND pxp.IdCodigoProdInterno = ".mysql_real_escape_string($IdCodigoProdInterno));
                
                $Tabla = $Cnn->DevolverQuery();
                
                if($Tabla[0]["IdCampania"]){
                	return $Tabla[0]["IdCampania"];
                }else{
                	return false;
                }
                
            }        
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
       
        
        
        public static function  save(MySQL_Productos $objProducto) {
            $Cnn = Conexion::nuevo();
            
            try {
                $Cnn->Abrir_Trans();
                $Cnn->setQuery($objProducto->getInsert());
                $Cnn->EjecutarQuery();
                
                if($objProducto->getIdProducto() == 0) {
                    $Cnn->setQuery("SELECT LAST_INSERT_ID() AS Id");
                    $ID = $Cnn->DevolverQuery();
                    $objProducto->setIdProducto($ID[0]["Id"]);
                }
                
                $Cnn->setQuery("DELETE FROM locales_x_producto WHERE IdProducto = " . $objProducto->getIdProducto());
                $Cnn->EjecutarQuery();
                foreach($objProducto->getLocales() as $Local) {
                    $Cnn->setQuery("INSERT INTO locales_x_producto (IdProducto, IdLocal) VALUES(".$objProducto->getIdProducto().",".$Local[0].");");
                    $Cnn->EjecutarQuery();
                }
                
                $objProducto->CrearDirectorio();
                $objProducto->Upload();
                $Cnn->Cerrar_Trans();
                return $objProducto->getIdProducto();
            }
            catch(Exception $e) {
            	echo "ROLLBACK: $e";
				$Cnn->RollBack();
                throw $e;
            }
        }
        
    
        public static function  saveCategoriasProducto($IdProducto, $categorias) {
            $Cnn = Conexion::nuevo();
            
            try {
                $Cnn->Abrir_Trans();
                $Cnn->setQuery("delete from productos_x_categorias where IdProducto = ".mysql_real_escape_string($IdProducto));
                $Cnn->EjecutarQuery();
                
                foreach($categorias as $IdCategoria) {
	                $Cnn->setQuery("INSERT INTO productos_x_categorias (IdProducto, IdCategoria) VALUES (".mysql_real_escape_string($IdProducto).", ".mysql_real_escape_string($IdCategoria).")");
    	            $Cnn->EjecutarQuery();
                }
                $Cnn->Cerrar_Trans();
            }
            catch(Exception $e) {
            	echo "ROLLBACK: $e";
				$Cnn->RollBack();
                throw $e;
            }
        }
        
        public static function saveVisit($Id)
        {
        	if(!is_numeric($Id))
        		return false;
        
            $oConexion = Conexion::nuevo();
            try
            {  
                
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE productos SET Visitas = Visitas + 1 WHERE IdProducto = ".mysql_real_escape_string($Id));
                $Tabla = $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    
        public static function deleteStockNew(MySQL_ProductosProveedores $objPP) {
            $Cnn = Conexion::nuevo();
            try {
                $Cnn->Abrir_Trans();           

                $Cnn->setQuery("delete from productos_x_atributos where IdCodigoProdInterno = ".$objPP->getIdCodigoProdInterno());
                $Cnn->EjecutarQuery();
                
                $Cnn->setQuery("delete from productos_x_proveedores where IdCodigoProdInterno = ".$objPP->getIdCodigoProdInterno());
                $Cnn->EjecutarQuery();
                
                $Cnn->Cerrar_Trans();
            }
            catch(MySQLException $e) {
                $Cnn->RollBack();
                throw $e;
            }
        }
        
    
        public static function agregarStockNew(MySQL_ProductosProveedores $objPP) {
            $Cnn = Conexion::nuevo();
            try {
                $Cnn->Abrir_Trans();
                $Cnn->setQuery($objPP->getInsert());
                $Cnn->EjecutarQuery();

                if($objPP->getIdCodigoProdInterno()==0) {
                	$Cnn->setQuery("SELECT LAST_INSERT_ID() AS Id");
                    $ID = $Cnn->DevolverQuery();
                    $objPP->setIdCodigoProdInterno($ID[0]["Id"]);
                }                    

                $Cnn->setQuery("delete from productos_x_atributos where IdCodigoProdInterno = ".$objPP->getIdCodigoProdInterno());
                $Cnn->EjecutarQuery();
                
                foreach($objPP->getAtributos() as $atributo) {
                	$atributo->setIdCodigoProdInterno($objPP->getIdCodigoProdInterno());
                	$Cnn->setQuery($atributo->getInsert());
                    $Cnn->EjecutarQuery();
                }
                $Cnn->Cerrar_Trans();
            }
            catch(MySQLException $e) {
                $Cnn->RollBack();
                throw $e;
            }
        }
        
        public static function agregarStock(MySQL_Productos $objProducto) {
            $Cnn = Conexion::nuevo();
            try {
                $Cnn->Abrir_Trans();
                $Cnn->setQuery("SELECT * FROM productos_x_proveedores WHERE IdProducto = ".$objProducto->getIdProducto());
                $dtPP = $Cnn->DevolverQuery();
                foreach($dtPP as $i=>$fila) {
                    foreach($objProducto->getProductosProveedores() as $objPP) {
                        if($fila["IdCodigoProdInterno"] == $objPP->getIdCodigoProdInterno()) {
                            unset($dtPP[$i]);
                        }
                    }
                }
                foreach($dtPP as $fila) {
                    $Cnn->setQuery("DELETE FROM productos_x_proveedores WHERE IdCodigoProdInterno = ".$fila["IdCodigoProdInterno"]);
                    $Cnn->EjecutarQuery();
                }
                foreach($objProducto->getProductosProveedores() as $objPP) {
                    $objPP->setIdProducto($objProducto->getIdProducto());
                    $Cnn->setQuery($objPP->getInsert());
                    $Cnn->EjecutarQuery();
                    if($objPP->getIdCodigoProdInterno()==0) {
                        $Cnn->setQuery("SELECT LAST_INSERT_ID() AS Id");
                        $ID = $Cnn->DevolverQuery();
                        $objPP->setIdCodigoProdInterno($ID[0]["Id"]);
                    }                    
                    foreach($objPP->getAtributos() as $atributo) {
                        $atributo->setIdCodigoProdInterno($objPP->getIdCodigoProdInterno());
                        $Cnn->setQuery($atributo->getInsert());
                        $Cnn->EjecutarQuery();
                    }
                }
                $Cnn->Cerrar_Trans();
            }
            catch(MySQLException $e) {
                $Cnn->RollBack();
                throw $e;
            }
        }
        /**
        * @desc 
        */
        public static function  ValoresAtributos_x_IdProducto($IdProducto)
        {
            $Cnn = Conexion::nuevo();
            try
            {
                $Cnn->Abrir();
                $Cnn->setQuery("SELECT PA.IdAtributoClase, PA.IdCodigoProdInterno, PA.Valor, PP.Stock, P.PVenta, PP.Incremento FROM productos_x_atributos PA 
                                                    INNER JOIN productos_x_proveedores PP ON PA.IdCodigoProdInterno = PP.IdCodigoProdInterno
                                                    INNER JOIN productos P ON PP.IdProducto = P.IdProducto
                                                     WHERE PA.IdCodigoProdInterno IN 
                                                     ( SELECT _PP.IdCodigoProdInterno FROM productos_x_proveedores _PP WHERE _PP.IdProducto = ".mysql_real_escape_string($IdProducto).")");
                $Tabla = $Cnn->DevolverQuery();
                $Cnn->Cerrar();
                for($i=0; $i<count($Tabla); $i++)
                {
                    if(strpos($Tabla[$i]["Incremento"], "*") !== FALSE)
                    {
                        $Tabla[$i]["Incremento"]=substr($Tabla[$i]["Incremento"],1,strlen($Tabla[$i]["Incremento"]));
                        $Tabla[$i]["PVenta"]=$Tabla[$i]["PVenta"]*$Tabla[$i]["Incremento"];
                    }
                    else if(strpos($Tabla[$i]["Incremento"], "+")!==FALSE)
                    {
                        $Tabla[$i]["Incremento"]=substr($Tabla[$i]["Incremento"],1,strlen($Tabla[$i]["Incremento"]));
                        $Tabla[$i]["PVenta"]=$Tabla[$i]["PVenta"]+$Tabla[$i]["Incremento"];
                    }
                }
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        /**
        * @desc Obtiene IdAtributoClase y Nombre de los Atributos de un IdProducto
        */
        public static function AtributosByIdProducto($IdProducto)
        {
            $Cnn = Conexion::nuevo();
            try
            {
                $Cnn->Abrir();
                $Cnn->setQuery("SELECT AC.IdAtributoClase, A.Nombre FROM atributos A INNER JOIN atributos_x_clases AC ON AC.IdAtributo = A.IdAtributo INNER JOIN productos P ON P.IdClase = AC.IdClase  WHERE P.IdProducto = " . $IdProducto ." ORDER BY AC.IdAtributoClase");
                $Tabla = $Cnn->DevolverQuery();
                $Cnn->Cerrar();
                return $Tabla;
            }        
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        public static function getProductosInternos($IdProducto)
        {
            $Cnn = Conexion::nuevo();
            try
            {
                $Cnn->Abrir();
                $Cnn->setQuery("SELECT PP.*, P.PVenta FROM productos_x_proveedores PP INNER JOIN productos P ON P.IdProducto = PP.IdProducto WHERE P.Habilitado = 1 AND PP.Habilitado = 1 AND P.IdProducto = ".mysql_real_escape_string($IdProducto));
                $Tabla = $Cnn->DevolverQuery();
                $Cnn->Cerrar();
                return $Tabla;
            }        
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        public static function getProductos($dtCategorias)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $collProductos = array();
                $oOrdenamiento = new clsOrdenamientos();
                foreach($dtCategorias as $Categoria)
                {
                    $oOrdenamiento = dmOrdenamientos::getById($Categoria["IdOrdenamiento"]);
                    $oConexion->Abrir_Trans();
                    $oConexion->setQuery("SELECT P.IdProducto, P.Nombre, P.Descripcion, P.IdMarca, P.IdClase, P.PCompra, P.PVenta, P.PVP, P.Peso, P.Referencia, (SELECT IF(SUM(Stock) IS NULL,0,SUM(Stock)) FROM productos_x_proveedores PP WHERE PP.IdProducto = P.IdProducto) AS StockMax, P.Profundidad, P.Ancho, P.Alto, P.ultimos FROM productos P WHERE P.IdProducto IN (SELECT IdProducto FROM productos_x_categorias WHERE IdCategoria =".$Categoria["IdCategoria"].") ORDER BY " . $oOrdenamiento->getCampo() . " " . $oOrdenamiento->getOrdenamiento());
                    $Tabla = $oConexion->DevolverQuery();
                    foreach($Tabla as $fila)
                    {
                        $collProductos[$fila["IdProducto"]] = $fila;
                    }
                    $oConexion->Cerrar_Trans();
                    $AuxCollProd = dmProductos::getProductos($Categoria["subCategoria"]);
                    foreach($AuxCollProd as $id=>$fila)
                    {
                        $collProductos[$id] = $fila;
                    }
                }
                return $collProductos;
            }
            catch (MySQLException $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
        }
	    public static function getProductosCount($paramArray = array()) {
				$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
					
			    $oConexion = Conexion::nuevo();
			    try
			    {
			        $oConexion->Abrir();
			        $oConexion->setQuery("SELECT count(distinct P.IdProducto) as total 
			        					  FROM productos P 
										  INNER JOIN marcasproductos M ON M.IdMarca = P.IdMarca 
										  INNER JOIN claseproductos C ON C.IdClase = P.IdClase 
										  LEFT JOIN productos_x_proveedores PP ON PP.IdProducto = P.IdProducto AND PP.Habilitado = 1 
										  LEFT JOIN productos_x_categorias PC ON P.IdProducto = PC.IdProducto 
										  LEFT JOIN categorias CA ON PC.IdCategoria = CA.IdCategoria 
										  WHERE ($filtersSQL)");
			        $Tabla = $oConexion->DevolverQuery();
			        //ColumnaMoneda(&$Tabla, "Monto total");
			        $oConexion->Cerrar_Trans();
			        return $Tabla[0]['total'];
			    }
			    catch(MySQLException $e)
			    {
			        $oConexion->RollBack();
			        throw $e;
			    }
			}
			
    
	    public static function getProductosCampaniaCount($IdCampania, $paramArray = array()) {
				$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
					
			    $oConexion = Conexion::nuevo();
			    try
			    {
			        $oConexion->Abrir();
			        $oConexion->setQuery("SELECT count(distinct P.IdProducto) as total 
			        					  FROM productos P 
										  INNER JOIN marcasproductos M ON M.IdMarca = P.IdMarca 
										  INNER JOIN claseproductos C ON C.IdClase = P.IdClase 
										  LEFT JOIN productos_x_proveedores PP ON PP.IdProducto = P.IdProducto AND PP.Habilitado = 1 
										  LEFT JOIN productos_x_categorias PC ON P.IdProducto = PC.IdProducto 
										  LEFT JOIN categorias CA ON PC.IdCategoria = CA.IdCategoria 
										  INNER JOIN campanias CM ON CA.IdCampania = CM.IdCampania 
										  WHERE CA.IdCampania = $IdCampania 
										  AND ($filtersSQL)");
			        $Tabla = $oConexion->DevolverQuery();
			        //ColumnaMoneda(&$Tabla, "Monto total");
			        $oConexion->Cerrar_Trans();
			        return $Tabla[0]['total'];
			    }
			    catch(MySQLException $e)
			    {
			        $oConexion->RollBack();
			        throw $e;
			    }
		}
		
	    public static function getProductosCampaniaPaginadas($IdCampania, $paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
				$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
				$sortSQL = ListadoUtils::generateSortSQL($sortArray);
					
				$oConexion = Conexion::nuevo();
				try
				{
					$oConexion->Abrir();
					$oConexion->setQuery("SELECT P.IdProducto AS 'P.IdProducto', 
												 P.Nombre AS 'P.Nombre', 
												 P.Habilitado AS 'P.Habilitado', 
												 P.Oculto AS 'P.Oculto',
												 P.Referencia AS 'P.Referencia',
												 M.Nombre AS 'M.Nombre', 
												 CM.Nombre AS 'CM.Nombre', 
												 C.Nombre AS 'C.Nombre', 
												 (SELECT SUM(sPP.Stock)
												   FROM productos sP
												   INNER JOIN marcasproductos sM ON sM.IdMarca = sP.IdMarca
												   INNER JOIN claseproductos sC ON sC.IdClase = sP.IdClase
												   LEFT JOIN productos_x_proveedores sPP ON sPP.IdProducto = sP.IdProducto AND sPP.Habilitado = 1
												   LEFT JOIN productos_x_categorias sPC ON sP.IdProducto = sPC.IdProducto
												   LEFT JOIN categorias sCAT ON sPC.IdCategoria = sCAT.IdCategoria
												   LEFT JOIN campanias sCAM ON sCAM.IdCampania = sCAT.IdCampania
												   WHERE  P.IdProducto=sP.IdProducto 
												   and (sPC.IdCategoria IS NULL OR (sPC.IdCategoria IS NOT NULL and (sCAT.IdCategoria IN (select MIN(ssPC.IdCategoria) from productos_x_categorias ssPC where ssPC.IdProducto = sP.IdProducto)) AND (DATE_FORMAT( now(), '%Y-%m-%d' ) >= DATE_FORMAT( sCAM.VisibleDesde, '%Y-%m-%d' ) and DATE_FORMAT( now(), '%Y-%m-%d' ) <= DATE_FORMAT( sCAM.FechaFin, '%Y-%m-%d' ))))
												   GROUP BY sP.IdProducto, sP.Nombre, sM.Nombre, sC.Nombre) as Stock, 
											     SUM(Regalo) AS 'SUM.Regalo', 
											     IF(P.Habilitado =1, 'Si', 'No')AS 'IF.Habilitado', 
											     PC.IdCategoria AS 'PC.IdCategoria', 
											     CA.IdCampania AS 'CA.IdCampania'
											FROM productos P 
											INNER JOIN marcasproductos M ON M.IdMarca = P.IdMarca
											INNER JOIN claseproductos C ON C.IdClase = P.IdClase
											LEFT JOIN productos_x_proveedores PP ON PP.IdProducto = P.IdProducto AND PP.Habilitado = 1
											LEFT JOIN productos_x_categorias PC ON P.IdProducto = PC.IdProducto
											LEFT JOIN categorias CA ON PC.IdCategoria = CA.IdCategoria
										    INNER JOIN campanias CM ON CA.IdCampania = CM.IdCampania 
											WHERE CA.IdCampania = $IdCampania 
										    AND ($filtersSQL)
											GROUP BY P.IdProducto, P.Nombre, M.Nombre, C.Nombre 
											ORDER BY $sortSQL 
											LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
				                	              	
					$Tabla = $oConexion->DevolverQuery();
					ColumnaMoneda(&$Tabla, "Monto total");
					$oConexion->Cerrar_Trans();
					return $Tabla;
				}
				catch(MySQLException $e)
				{
					$oConexion->RollBack();
					throw $e;
				}
			} 
		
			
	    public static function getProductosPaginadas($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
				$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
				$sortSQL = ListadoUtils::generateSortSQL($sortArray);
					
				$oConexion = Conexion::nuevo();
				try
				{
					$oConexion->Abrir();
					$oConexion->setQuery("SELECT P.IdProducto AS 'P.IdProducto', 
												 P.Nombre AS 'P.Nombre', 
												 M.Nombre AS 'M.Nombre', 
												 C.Nombre AS 'C.Nombre', 
												 P.Referencia AS 'P.Referencia',
												 (SELECT SUM(sPP.Stock)
												   FROM productos sP
												   INNER JOIN marcasproductos sM ON sM.IdMarca = sP.IdMarca
												   INNER JOIN claseproductos sC ON sC.IdClase = sP.IdClase
												   LEFT JOIN productos_x_proveedores sPP ON sPP.IdProducto = sP.IdProducto AND sPP.Habilitado = 1
												   LEFT JOIN productos_x_categorias sPC ON sP.IdProducto = sPC.IdProducto
												   LEFT JOIN categorias sCAT ON sPC.IdCategoria = sCAT.IdCategoria
												   LEFT JOIN campanias sCAM ON sCAM.IdCampania = sCAT.IdCampania
												   WHERE  P.IdProducto=sP.IdProducto 
												   and (sPC.IdCategoria IS NULL OR (sPC.IdCategoria IS NOT NULL and (sCAT.IdCategoria IN (select MIN(ssPC.IdCategoria) from productos_x_categorias ssPC where ssPC.IdProducto = sP.IdProducto)) AND (DATE_FORMAT( now(), '%Y-%m-%d' ) >= DATE_FORMAT( sCAM.VisibleDesde, '%Y-%m-%d' ) and DATE_FORMAT( now(), '%Y-%m-%d' ) <= DATE_FORMAT( sCAM.FechaFin, '%Y-%m-%d' ))))
												   GROUP BY sP.IdProducto, sP.Nombre, sM.Nombre, sC.Nombre) as Stock, 
											     SUM(Regalo) AS 'SUM.Regalo', 
											     IF(P.Habilitado =1, 'Si', 'No')AS 'IF.Habilitado', 
											     PC.IdCategoria AS 'PC.IdCategoria', 
											     CA.IdCampania AS 'CA.IdCampania',
											     P.Oculto AS 'P.Oculto'
											FROM productos P 
											INNER JOIN marcasproductos M ON M.IdMarca = P.IdMarca
											INNER JOIN claseproductos C ON C.IdClase = P.IdClase
											LEFT JOIN productos_x_proveedores PP ON PP.IdProducto = P.IdProducto AND PP.Habilitado = 1
											LEFT JOIN productos_x_categorias PC ON P.IdProducto = PC.IdProducto
											LEFT JOIN categorias CA ON PC.IdCategoria = CA.IdCategoria
											WHERE ($filtersSQL)
											GROUP BY P.IdProducto, P.Nombre, M.Nombre, C.Nombre 
											ORDER BY $sortSQL 
											LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
				                	              	
					$Tabla = $oConexion->DevolverQuery();
					ColumnaMoneda(&$Tabla, "Monto total");
					$oConexion->Cerrar_Trans();
					return $Tabla;
				}
				catch(MySQLException $e)
				{
					$oConexion->RollBack();
					throw $e;
				}
			} 
        public static function getProductosOrdenados($dtCategorias) {
            $oConexion = Conexion::nuevo();
            
            $categorias = '';
			foreach($dtCategorias as $Categoria) {
            	$categorias .= $Categoria["IdCategoria"].', ';
            	foreach($Categoria['subCategoria'] as $Sub) {
                  	$categorias .= $Sub["IdCategoria"].', ';
            		foreach($Sub['subCategoria'] as $SubZero) {
                    	$categorias .= $SubZero["IdCategoria"].', ';
                   	}
            	}
			} 
            $categorias = substr($categorias, 0, -2);
                
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT P.IdProducto, P.Nombre, P.Descripcion, P.IdMarca, P.IdClase, P.PCompra, P.PVenta, P.PVP, P.ultimos, P.Peso, P.Referencia, (SELECT IF(SUM(Stock) IS NULL,0,SUM(Stock)) FROM productos_x_proveedores PP WHERE PP.IdProducto = P.IdProducto) AS StockMax, P.Profundidad, P.Ancho, P.Alto, IF(P.ultimos != 0 AND P.ultimos >= (SELECT SUM(Stock) FROM productos_x_proveedores PP WHERE PP.IdProducto = P.IdProducto), 1, 0) as QuedanPocos FROM productos P WHERE P.IdProducto IN (SELECT IdProducto FROM productos_x_categorias WHERE IdCategoria IN (".$categorias.")) ORDER BY QuedanPocos DESC, StockMax DESC");
                 
                 $Tabla = $oConexion->DevolverQuery();
                 foreach($Tabla as $fila)
                 {
                     $collProductos[] = $fila;
                 }
                 $oConexion->Cerrar_Trans();

                return $collProductos;
        }
        
        public static function getProductosTotal($dtCategorias) {
            $oConexion = Conexion::nuevo();
            
            $categorias = '';
			foreach($dtCategorias as $Categoria) {
            	$categorias .= $Categoria["IdCategoria"].', ';
            	foreach($Categoria['subCategoria'] as $Sub) {
                  	$categorias .= $Sub["IdCategoria"].', ';
            		foreach($Sub['subCategoria'] as $SubZero) {
                    	$categorias .= $SubZero["IdCategoria"].', ';
                   	}
            	}
			} 
            $categorias = substr($categorias, 0, -2);

            $oConexion->Abrir_Trans();
            $oConexion->setQuery("
            	SELECT         	
	            	COUNT(P.IdProducto) as Total
            	FROM productos P
            	WHERE 
            		P.IdProducto IN (SELECT IdProducto FROM productos_x_categorias WHERE IdCategoria IN (".$categorias."))");
                 
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();

			return $Tabla[0]['Total'];
        }

        public static function getProductosOrdenadosPaginados($dtCategorias, $page, $limit, $order, $dir) {
            $oConexion = Conexion::nuevo();
            
            $categorias = '';
			foreach($dtCategorias as $Categoria) {
            	$categorias .= $Categoria["IdCategoria"].', ';
            	foreach($Categoria['subCategoria'] as $Sub) {
                  	$categorias .= $Sub["IdCategoria"].', ';
            		foreach($Sub['subCategoria'] as $SubZero) {
                    	$categorias .= $SubZero["IdCategoria"].', ';
                   	}
            	}
			} 
            $categorias = substr($categorias, 0, -2);

            $oConexion->Abrir_Trans();
            $oConexion->setQuery("
            	SELECT 
            	
            	P.IdProducto, P.Nombre, P.Descripcion, P.DescripcionAmpliada, P.IdMarca, P.IdClase,
            	P.PCompra, P.PVenta, P.PVP, P.ultimos, P.Peso, P.Referencia, P.Profundidad, P.Ancho, P.Alto,
            	IF(SUM(PP.Stock) IS NULL, 0, SUM(PP.Stock)) AS StockMax, 
            	IF(P.ultimos != 0 AND P.ultimos >= SUM(PP.Stock), 1, 0) as QuedanPocos,
            	IF(SUM(PP.Stock) = 0, 1, 0) as Agotado
            	
            	FROM productos P
            	LEFT JOIN productos_x_proveedores PP ON PP.IdProducto = P.IdProducto
            	WHERE 
            		P.IdProducto IN (SELECT IdProducto FROM productos_x_categorias WHERE IdCategoria IN (".$categorias.")) 
            		AND P.oculto = 0
            	GROUP BY
            		P.IdProducto
            	ORDER BY Agotado ASC, QuedanPocos DESC, ".$order." ".$dir
            	.($page != false && $limit != false ? " LIMIT ".(($page-1)*$limit).", ".$limit : ''));
            	
            $Tabla = $oConexion->DevolverQuery();
            	
            $oConexion->Cerrar_Trans();

			return $Tabla;
        }
        
        public static function getDataById($id) {
            $oConexion = Conexion::nuevo();
            
            $oConexion->Abrir_Trans();
            $oConexion->setQuery("
            	SELECT 
            	
            	P.IdProducto, P.Nombre, P.Descripcion, P.DescripcionAmpliada, P.IdMarca, P.IdClase,
            	P.PCompra, P.PVenta, P.PVP, P.ultimos, P.Peso, P.Referencia, P.Profundidad, P.Ancho, P.Alto,
            	IF(SUM(PP.Stock) IS NULL, 0, SUM(PP.Stock)) AS StockMax, 
            	IF(P.ultimos != 0 AND P.ultimos >= SUM(PP.Stock), 1, 0) as QuedanPocos,
            	IF(SUM(PP.Stock) = 0, 1, 0) as Agotado
            	
            	FROM productos P
            	LEFT JOIN productos_x_proveedores PP ON PP.IdProducto = P.IdProducto
            	WHERE 
            		P.IdProducto = ".$id."
            		AND P.oculto = 0
            	GROUP BY
            		P.IdProducto");
            	
            $Tabla = current($oConexion->DevolverQuery());
            	
            $oConexion->Cerrar_Trans();

			return $Tabla;
        }


        public static function getProductosAll($columnaNro, $columnaSentido, $filter="")
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT P.IdProducto, P.Nombre AS Producto, M.Nombre AS Marca, C.Nombre AS Clase, (SELECT SUM(sPP.Stock)
				    FROM productos sP
				    INNER JOIN marcasproductos sM ON sM.IdMarca = sP.IdMarca
				    INNER JOIN claseproductos sC ON sC.IdClase = sP.IdClase
				    LEFT JOIN productos_x_proveedores sPP ON sPP.IdProducto = sP.IdProducto AND sPP.Habilitado = 1
				    LEFT JOIN productos_x_categorias sPC ON sP.IdProducto = sPC.IdProducto
				    LEFT JOIN categorias sCAT ON sPC.IdCategoria = sCAT.IdCategoria
				    LEFT JOIN campanias sCAM ON sCAM.IdCampania = sCAT.IdCampania
				    WHERE  P.IdProducto=sP.IdProducto 
				    and (sPC.IdCategoria IS NULL OR (sPC.IdCategoria IS NOT NULL and (sCAT.IdCategoria IN (select MIN(ssPC.IdCategoria) from productos_x_categorias ssPC where ssPC.IdProducto = sP.IdProducto)) AND (DATE_FORMAT( now(), '%Y-%m-%d' ) >= DATE_FORMAT( sCAM.VisibleDesde, '%Y-%m-%d' ) and DATE_FORMAT( now(), '%Y-%m-%d' ) <= DATE_FORMAT( sCAM.FechaFin, '%Y-%m-%d' ))))
				    GROUP BY sP.IdProducto, sP.Nombre, sM.Nombre, sC.Nombre) as Stock, SUM(Regalo) AS Regalo, IF(P.Habilitado =1, 'Si', 'No')AS Habilitado, productos_x_categorias.IdCategoria, categorias.IdCampania 
				FROM productos P 
				INNER JOIN marcasproductos M ON M.IdMarca = P.IdMarca
				INNER JOIN claseproductos C ON C.IdClase = P.IdClase
				LEFT JOIN productos_x_proveedores PP ON PP.IdProducto = P.IdProducto AND PP.Habilitado = 1
				LEFT Join productos_x_categorias ON P.IdProducto = productos_x_categorias.IdProducto
				LEFT Join categorias ON productos_x_categorias.IdCategoria = categorias.IdCategoria
				WHERE P.Nombre LIKE \"%".$filter."%\" OR M.Nombre LIKE \"%".$filter."%\" OR C.Nombre LIKE \"%".$filter."%\"
				GROUP BY P.IdProducto, P.Nombre, M.Nombre, C.Nombre
                ORDER BY ".$columnaNro." ".$columnaSentido."
                limit 200");
                
                
                
				
				/*$oConexion->setQuery("SELECT P.IdProducto, P.Nombre AS Producto, M.Nombre AS Marca, C.Nombre AS Clase, IFNULL(SUM(Stock),0) AS Stock, IFNULL(SUM(Regalo),0) AS Regalo, IF(P.Habilitado =1, 'Si', 'No')AS Habilitado, productos_x_categorias.IdCategoria, categorias.IdCampania FROM productos P 
                INNER JOIN marcasproductos M ON M.IdMarca = P.IdMarca
                INNER JOIN claseproductos C ON C.IdClase = P.IdClase
                LEFT JOIN productos_x_proveedores PP ON PP.IdProducto = P.IdProducto AND PP.Habilitado = 1
                Inner Join productos_x_categorias ON P.IdProducto = productos_x_categorias.IdProducto
                Inner Join categorias ON productos_x_categorias.IdCategoria = categorias.IdCategoria
                WHERE P.Nombre LIKE \"%".$filter."%\" OR M.Nombre LIKE \"%".$filter."%\" OR C.Nombre LIKE \"%".$filter."%\"
                GROUP BY P.IdProducto, P.Nombre, M.Nombre, C.Nombre
                ORDER BY ".$columnaNro." ".$columnaSentido);*/

                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
		
		public static function isCampaignProduct($IdProducto, $IdCampania)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("select count(*) as total from productos_x_categorias pc, categorias c where pc.IdCategoria = c.IdCategoria and pc.IdProducto = ".mysql_real_escape_string($IdProducto)." and c.IdCampania = ".mysql_real_escape_string($IdCampania));
				$Tabla = $oConexion->DevolverQuery();
                $objProductos = new MySQL_Productos();
				
				$totalProductos = $Tabla[0]['total'];
				return ($totalProductos>0);
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
		
        public static function getById($IdProducto)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT P.IdProducto, P.Nombre, P.Descripcion, P.DescripcionAmpliada, P.IdMarca, P.IdClase, P.PCompra, P.PVenta, P.PVP, P.Peso, P.Referencia, P.Profundidad, P.Ancho, P.Alto, P.Habilitado, P.ultimos, P.oculto FROM productos P WHERE P.Habilitado = 1 AND P.IdProducto  =".mysql_real_escape_string($IdProducto));
                $Tabla = $oConexion->DevolverQuery();
                $objProductos = new MySQL_Productos();
                if(isset($Tabla[0]))
                    $objProductos->__setByArray($Tabla[0]);
                $oConexion->setQuery("SELECT IdProducto, IdProveedor, IdCodigoProdInterno, Stock, Regalo, Incremento, Comentarios, Habilitado FROM productos_x_proveedores WHERE IdProducto = ".mysql_real_escape_string($IdProducto));
                $Tabla = $oConexion->DevolverQuery();
                foreach($Tabla as $fila)
                {
                    $objPP = new MySQL_ProductosProveedores();
                    $objPP->__setByArray($fila);
                    $objProductos->addProductosProveedores($objPP);
                }
                $oConexion->Cerrar_Trans();
                return $objProductos;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function updateHabilitado($IdProducto, $value) {
        	
        	$oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE productos SET Habilitado = ".mysql_real_escape_string($value)." WHERE IdProducto = ".mysql_real_escape_string($IdProducto));
                $Tabla = $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function updateOculto($IdProducto, $value) {
        	
        	$oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE productos SET Oculto = ".mysql_real_escape_string($value)." WHERE IdProducto = ".mysql_real_escape_string($IdProducto));
                $Tabla = $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function getByIdCodigoProdInterno($IdCodigoProdInterno)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdProducto, IdProveedor, IdCodigoProdInterno, Stock, Regalo, Incremento, Comentarios FROM productos_x_proveedores  WHERE IdCodigoProdInterno  =".mysql_real_escape_string($IdCodigoProdInterno));
                $Tabla = $oConexion->DevolverQuery();
                $objPP = new MySQL_ProductosProveedores();
                $objPP->__setByArray($Tabla[0]);
                $oConexion->Cerrar_Trans();
                return $objPP;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }        
        public static function getCategoria_byIdProducto($IdProducto, $IdCampania)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT C.IdCategoria FROM productos_x_categorias PC INNER JOIN categorias C ON C.IdCategoria = PC.IdCategoria WHERE PC.IdProducto = ".mysql_real_escape_string($IdProducto)." AND C.IdCampania =". mysql_real_escape_string($IdCampania));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]["IdCategoria"];
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }            
        }
        public static function getAtributosByIdCodigoProdInterno($IdCodigoProdInterno)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT A.*, PA.*, AC.* FROM productos_x_atributos PA INNER JOIN atributos_x_clases AC ON AC.idAtributoClase = PA.idAtributoClase INNER JOIN atributos A ON A.IdAtributo = AC.IdAtributo INNER JOIN claseproductos C ON AC.IdClase = C.IdClase WHERE C.Habilitado = 1 AND PA.IdCodigoProdInterno  = ".mysql_real_escape_string($IdCodigoProdInterno));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getProductosFilrados($IdProveedor, $IdMarca, $IdClase)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $strSQL ="SELECT P.IdProducto AS Codigo, CONCAT(P.Nombre, '(Stock:', IF(SUM(Stock) IS NULL,0,SUM(Stock)), ' Regalo:', IF(SUM(Regalo) IS NULL,0,SUM(Regalo)), ')') AS Producto FROM productos P INNER JOIN productos_x_proveedores PP ON PP.IdProducto = P.IdProducto WHERE 1=1";
                if($IdProveedor >0)
                    $strSQL .=" AND PP.IdProveedor = " .mysql_real_escape_string($IdProveedor);
                if($IdMarca >0)
                    $strSQL .=" AND P.IdMarca= " .mysql_real_escape_string($IdMarca);
                if($IdMarca >0)
                    $strSQL .=" AND P.IdClase = " .mysql_real_escape_string($IdClase);
                $strSQL .=" GROUP BY P.IdProducto , P.Nombre ORDER BY P.Nombre";
                $oConexion->setQuery($strSQL);
                $Tabla = $oConexion->DevolverQuery();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
        }
        public static function getProductosFilradosCamp($idCampania=0)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $strSQL ="SELECT P.IdProducto AS Codigo, CONCAT(P.Nombre, '(Stock:', Stock, ' Regalo:', Regalo, ') (Ref: ',P.Referencia,')') AS Producto, PP.IdCodigoProdInterno FROM productos P INNER JOIN productos_x_proveedores PP ON PP.IdProducto = P.IdProducto WHERE 1=1";
                if($idCampania >0)
                    $strSQL .=" AND P.IdProducto IN (
                    	SELECT c.IdProducto
                    	FROM productos_x_categorias c
                    	JOIN categorias cc ON cc.IdCategoria = c.IdCategoria
                    	WHERE cc.IdCampania=".mysql_real_escape_string($idCampania)."
                    )";
                $strSQL .=" ORDER BY P.Nombre";
                $oConexion->setQuery($strSQL);
                $Tabla = $oConexion->DevolverQuery();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
        }
        public static function getLocales($IdProducto)
        {
            $Cnn = Conexion::nuevo();
            try
            {
                $Cnn->Abrir();
                $Cnn->setQuery("SELECT  LP.IdProducto, LP.IdLocal, L.Descripcion, L.Direccion, L.IdProvincia FROM locales_x_producto LP INNER JOIN locales L ON LP.IdLocal = L.IdLocal WHERE IdProducto = " . $IdProducto);
                $Tabla = $Cnn->DevolverQuery();
                $Cnn->Cerrar();
                return $Tabla;
            }        
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        public static function getIdCodigoProdInternoActual($IdProducto) {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdCodigoProdInterno as IdCodigoProdInterno from productos_x_proveedores where IdProducto=".mysql_real_escape_string($IdProducto));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]["IdCodigoProdInterno"];
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            } 
        }
    
		public static function hayStock($IdCodigoProdInterno, $Cantidad)
		{
		    $oConexion = Conexion::nuevo();
	        $oConexion->setQuery("SELECT Stock
				FROM productos_x_proveedores
				WHERE IdCodigoProdInterno = ".mysql_real_escape_string($IdCodigoProdInterno));
	        $Tabla = $oConexion->DevolverQuery();
	        
	        return ($Tabla[0]["Stock"] >= $Cantidad);
		}
		
		public static function ImportarCSV($csv_path) {

			$_SESSION['resultados'] = array();
			$_SESSION['resultadosErrorClase'] = array();
			$_SESSION['resultadosErrorCategoria'] = array();

			//Categor�a padre para la campa�a
	        $IdCampania = $_GET['IdCampania'];
			$oConexion = Conexion::nuevo();
	        $oConexion->setQuery("SELECT IdCategoria FROM categorias WHERE IdCampania = ".$IdCampania." AND IdPadre IS NULL");
	        $Tabla = $oConexion->DevolverQuery();
	        $oConexion->Cerrar_Trans();
	        $categoriaPadreId = $Tabla[0]['IdCategoria'];
	        
	        $handle = fopen($csv_path, "r");
	        
	        if(!$handle)
	        	die('No se pudo abrir el archivo.');
	        
	        $first = true;
			while(($data = fgetcsv($handle, 0, ";")) !== FALSE) {

				//Skip header
				if($first) {
					$first = false;
					continue;
				}

				$data = array(
					'clase' => $data[0],
					'cat' => $data[1],
					'ref' => $data[2],
					'nombre' => $data[3],
					'desc' => nl2br($data[4]),
					'stock' => $data[5],
					'atributo' => $data[6],
					'atributoNombre' => 'Talla',
					'venta' => $data[7],
					'costo' => $data[8],
					'mercado' => $data[9],
					'peso' => $data[10],
					'alto' => $data[11],
					'ancho' => $data[12],
					'profundidad' => $data[13],
					'ultimos' => $data[14],
					'proveedor' => $data[15],
					//'campania' => $data[16],
					'marca' => $data[16],
					'catGlobal' => $data[17],			
					'iva' => $data[18]			
				);
				
				
				if($data['nombre'] == '')
					continue;
				
				/*foreach($data as $i => $d)
					$data[$i] = utf8_decode($d);*/

				//Campa�a
				/*$oConexion = Conexion::nuevo();
		        $oConexion->setQuery("SELECT IdCampania	FROM campanias WHERE nombre = '".mysql_real_escape_string($data['campania'])."'");
		        $Tabla = $oConexion->DevolverQuery();
		        $oConexion->Cerrar_Trans();
		        
		        if(count($Tabla) == 0) {
					$oConexion = Conexion::nuevo();
			        $oConexion->setQuery("INSERT INTO campanias (nombre) VALUES ('".mysql_real_escape_string($data['campania'])."')");
			        $oConexion->EjecutarQuery();
			        $IdCampania = mysql_insert_id();
			        $oConexion->Cerrar_Trans();
		        } else {
		        	$IdCampania = $Tabla[0]['IdCampania'];
		        }*/
		        
		        //Proveedor
				$oConexion = Conexion::nuevo();
		        $oConexion->setQuery("SELECT IdProveedor FROM proveedores WHERE nombre = '".mysql_real_escape_string($data['proveedor'])."'");
		        $Tabla = $oConexion->DevolverQuery();
		        $oConexion->Cerrar_Trans();
		        
		        if(count($Tabla) == 0) {
					$oConexion = Conexion::nuevo();
			        $oConexion->setQuery("INSERT INTO proveedores (nombre, Habilitado) VALUES ('".mysql_real_escape_string($data['proveedor'])."', 1)");
			        $oConexion->EjecutarQuery();
			        $IdProveedor = mysql_insert_id();
			        $oConexion->Cerrar_Trans();
		        } else {
		        	$IdProveedor = $Tabla[0]['IdProveedor'];
		        }
		        
		        //Clase de producto
				$oConexion = Conexion::nuevo();
		        $oConexion->setQuery("SELECT IdClase FROM claseproductos WHERE Nombre = '".mysql_real_escape_string($data['clase'])."'");
		        $Tabla = $oConexion->DevolverQuery();
		        $oConexion->Cerrar_Trans();
		        
		        if(count($Tabla) == 0) {
					/*$oConexion = Conexion::nuevo();
			        $oConexion->setQuery("INSERT INTO claseproductos (Nombre, Habilitado) VALUES ('".mysql_real_escape_string($data['clase'])."', 1)");
			        $oConexion->EjecutarQuery();
			        $IdClase = mysql_insert_id();
			        $oConexion->Cerrar_Trans();*/
			        $_SESSION['resultadosErrorClase'][$data['nombre']] = $data['clase'];
			        continue;
		        } else {
		        	$IdClase = $Tabla[0]['IdClase'];
		        }
		        
		        //Marca
				$oConexion = Conexion::nuevo();
		        $oConexion->setQuery("SELECT IdMarca FROM marcasproductos WHERE Nombre = '".mysql_real_escape_string($data['marca'])."'");
		        $Tabla = $oConexion->DevolverQuery();
		        $oConexion->Cerrar_Trans();
		        
		        if(count($Tabla) == 0) {
					$oConexion = Conexion::nuevo();
			        $oConexion->setQuery("INSERT INTO marcasproductos (Nombre, Habilitado) VALUES ('".mysql_real_escape_string($data['marca'])."', 1)");
			        $oConexion->EjecutarQuery();
			        $IdMarca = mysql_insert_id();
			        $oConexion->Cerrar_Trans();
		        } else {
		        	$IdMarca = $Tabla[0]['IdMarca'];
		        }
		        
		        //Categor�a
				$oConexion = Conexion::nuevo();
		        $oConexion->setQuery("SELECT * FROM categorias WHERE IdCampania = ".$IdCampania." AND Nombre = '".mysql_real_escape_string($data['cat'])."' AND IdPadre = ".$categoriaPadreId);
		        $Tabla = $oConexion->DevolverQuery();
		        $oConexion->Cerrar_Trans();

		        if(count($Tabla) == 0) {
					/*$oConexion = Conexion::nuevo();
			        $oConexion->setQuery("INSERT INTO categorias (Nombre, IdCampania, Habilitado, IdPadre) VALUES ('".mysql_real_escape_string($data['cat'])."', ".$IdCampania.", 1, ".$categoriaPadreId.")");
			        $oConexion->EjecutarQuery();
			        $IdCategoria = mysql_insert_id();
			        $oConexion->Cerrar_Trans();*/
			        $_SESSION['resultadosErrorCategoria'][$data['nombre']] = $data['cat'];
			        continue;
		        } else {
		        	$IdCategoria = $Tabla[0]['IdCategoria'];
		        }
		        
		        //Producto
				if(!isset($_SESSION['resultados'][$data['nombre']])) {
			        $oConexion = Conexion::nuevo();
			        $oConexion->setQuery("INSERT INTO productos (IdClase, IdMarca, Referencia, Nombre, Descripcion, DescripcionAmpliada, PVenta, PCompra, PVP, Peso, Alto, Ancho, Profundidad, Habilitado, Ultimos, iva) VALUES (".$IdClase.", ".$IdMarca.", '".mysql_real_escape_string($data['ref'])."', '".mysql_real_escape_string($data['nombre'])."', '".mysql_real_escape_string($data['nombre'])."', '".mysql_real_escape_string($data['desc'])."', '".mysql_real_escape_string($data['venta'])."', '".mysql_real_escape_string($data['costo'])."', '".mysql_real_escape_string($data['mercado'])."', '".mysql_real_escape_string($data['peso'])."', '".mysql_real_escape_string($data['alto'])."', '".mysql_real_escape_string($data['ancho'])."', '".mysql_real_escape_string($data['profundidad'])."', 1, '".mysql_real_escape_string($data['ultimos'])."', '".mysql_real_escape_string($data['iva'])."')");
			        $oConexion->EjecutarQuery();
			        $IdProducto = mysql_insert_id();
					$_SESSION['resultados'][$data['nombre']] = $IdProducto;
			        $oConexion->Cerrar_Trans();

				    //Producto por categor�a
				    $oConexion = Conexion::nuevo();
			        $oConexion->setQuery("INSERT INTO productos_x_categorias (IdProducto, IdCategoria) VALUES (".$IdProducto.", ".$IdCategoria.")");
					$oConexion->EjecutarQuery();
					$oConexion->Cerrar_Trans();
	
				    $oConexion = Conexion::nuevo();
			        $oConexion->setQuery("INSERT INTO productos_x_categorias_globales (IdProducto, IdCategoriaGlobal) VALUES (".$IdProducto.", '".$data['catGlobal']."')");
					$oConexion->EjecutarQuery();
					$oConexion->Cerrar_Trans();
	
			    } else {
					$IdProducto = $_SESSION['resultados'][$data['nombre']];			    
			    }
			    				
				//Producto por proveedor
				$oConexion = Conexion::nuevo();
		        $oConexion->setQuery("INSERT INTO productos_x_proveedores (IdProducto, IdProveedor, Stock, Habilitado) VALUES (".$IdProducto.", ".$IdProveedor.", '".$data['stock']."', 1)");
		        $oConexion->EjecutarQuery();
		        $IdCodigoProdInterno = mysql_insert_id();
		        $oConexion->Cerrar_Trans();
			
				if($data['atributo'] != '') {
							    
				    //Atributo
			       	/*$oConexion = Conexion::nuevo();
			        $oConexion->setQuery("SELECT IdAtributo FROM atributos WHERE Nombre = '".mysql_real_escape_string($data['atributoNombre'])."'");
			        $Tabla = $oConexion->DevolverQuery();
			        $oConexion->Cerrar_Trans();
			        
			        if(count($Tabla) == 0) {
						$oConexion = Conexion::nuevo();
				        $oConexion->setQuery("INSERT INTO atributos (Nombre) VALUES ('".mysql_real_escape_string($data['atributoNombre'])."')");
				        $oConexion->EjecutarQuery();
				        $IdAtributo = mysql_insert_id();
				        $oConexion->Cerrar_Trans();
			        } else {
			        	$IdAtributo = $Tabla[0]['IdAtributo'];
			        }*/
			        
			        //Atributo por clase
			       	$oConexion = Conexion::nuevo();
			        //$oConexion->setQuery("SELECT idAtributoClase FROM atributos_x_clases WHERE idAtributo = ".$IdAtributo." AND idClase = ".$IdClase);
					$oConexion->setQuery("SELECT idAtributoClase FROM atributos_x_clases WHERE idClase = ".$IdClase);
			        $Tabla = $oConexion->DevolverQuery();
			        $oConexion->Cerrar_Trans();
			        
			        if(count($Tabla) == 0) {
						/*$oConexion = Conexion::nuevo();
				        $oConexion->setQuery("INSERT INTO atributos_x_clases (idAtributo, idClase) VALUES (".$IdAtributo.", ".$IdClase.")");
				        $oConexion->EjecutarQuery();
				        $IdAtributoClase = mysql_insert_id();
				        $oConexion->Cerrar_Trans();*/
				        $_SESSION['resultadosErrorClase'][$data['nombre']] = $data['clase'];
				        continue;
			        } else {
			        	$IdAtributoClase = $Tabla[0]['idAtributoClase'];
			        }

			        //Producto por atributo
					$oConexion = Conexion::nuevo();
			        $oConexion->setQuery("INSERT INTO productos_x_atributos (idAtributoClase, IdCodigoProdInterno, Valor) VALUES (".$IdAtributoClase.", ".$IdCodigoProdInterno.", '".mysql_real_escape_string($data['atributo'])."')");
			        $oConexion->EjecutarQuery();
			        $oConexion->Cerrar_Trans();
			        
			   }

			}
			
		}
				
		public static function ReimportarCSV($csv) {
			$csv = explode("\r\n", $csv);

			header('Content-Type: text/plain');
			header('Pragma: public');   // required
			header('Expires: 0');    // no cache
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Cache-Control: private',false);
			header('Content-Disposition: attachment; filename="importar.csv"');
			header('Content-Transfer-Encoding: binary');
			header('Connection: close');

			unset($csv[0]);

			$out = fopen('php://output', 'w');
			fputs($out, "Clase;Categoria; Referencia;Nombre del Producto;Descripcion Ampliada;Stock;Atributo;Precio Venta en GEELBE;Costo de Geelbe;Precio Venta en El Mercado;Peso;Alto;Ancho;Profundidad;Ultimos Productos;Proveedor;Marca;Codigo de linea\n");

			foreach($csv as $p) {
				
				$p = explode(';', $p);
				
				$ref = trim($p[0]);
				$att = trim($p[1]);
				$stock = trim($p[2]);

				$oConexion = Conexion::nuevo();
				$oConexion->setQuery("SELECT
					cl.Nombre 'Clase',
					cat.Nombre 'Categoria',
					p.referencia, 
					p.nombre, 
					p.descripcionampliada descripcion,
					'".$stock."' as stock,
					'".$att."' as atributo,
					p.pventa, 
					p.pcompra, 
					p.pvp, 
					p.peso,
					p.alto,
					p.ancho,
					p.profundidad,
					p.ultimos,
					pr.Nombre 'Proveedor',
					m.Nombre 'Marca',
					cg.Nombre 'Linea'
				FROM productos p
				LEFT JOIN claseproductos cl ON cl.IdClase = p.IdClase
				LEFT JOIN productos_x_categorias pxc ON pxc.IdProducto = p.IdProducto
				LEFT JOIN categorias cat ON cat.IdCategoria = pxc.IdCategoria
				LEFT JOIN productos_x_proveedores pxp ON pxp.IdProducto = p.IdProducto
				LEFT JOIN proveedores pr ON pr.IdProveedor = pxp.IdProveedor
				LEFT JOIN marcasproductos m ON m.IdMarca = p.IdMarca
				LEFT JOIN productos_x_categorias_globales pxcg ON pxcg.IdProducto = p.IdProducto
				LEFT JOIN categorias_globales cg ON cg.IdCategoriaGlobal = pxcg.IdCategoriaGlobal
				WHERE Referencia = '".$ref."'
				GROUP BY p.IdProducto");
		        $Tabla = $oConexion->DevolverQuery();
		        $oConexion->Cerrar_Trans();	

		        $Tabla[0]['descripcion'] = stripslashes($Tabla[0]['descripcion']);

		        if(count($Tabla) > 0)
		        	fputcsv($out, $Tabla[0], ';');

			}
			
			die;
			
		}
    }
    
?>
