<?php
    	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    	Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
        
        ob_start();
		set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        
        try {   
            $objPP = new MySQL_ProductosProveedores();
            $objPP->setIdProducto($_GET["IdProducto"]);
            $objPP->setIdCodigoProdInterno($_GET["IdCodigoProdInterno"]);
            
            dmProductos::deleteStockNew($objPP);
            
            if($IdProducto != 0) {
            	$IdCampania = dmProductos::getIdCampaniaByIdCodigoProdInterno(dmProductos::getIdCodigoProdInternoActual($_GET["IdProducto"]));
	            
            	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->clean(
				    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
				    array('group_cache_catalogo_campania_'.$IdCampania)
				);
            }
            header("Location: ".UrlResolver::getBaseURL("bo/productos/ABM_stock_new.php?IdProducto=".$_GET["IdProducto"]."&IdCampania=".$_GET["IdCampania"]));
			die;
        }
        catch(MySQLException $e) {
        	echo $e;
        }
        ob_flush();
?>