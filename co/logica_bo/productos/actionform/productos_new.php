<?php

    if($_POST) {
    	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    	Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
        ob_start();
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        extract($_POST);
	    
       if(!isset($_GET['page']) || $_GET['page'] < 1) $_GET['page'] = 1;

       try {   
            $IdProducto = ($txtIdProducto == "0")?0:$txtIdProducto;
            $objProducto = new MySQL_Productos();
            $objProducto->setIdProducto($IdProducto);
            $objProducto->setNombre($txtNombre);
            $objProducto->setDescripcion($txtDescripcion);
            $objProducto->setDescripcionAmpliada($txtDescripcionAmpliada);
            $objProducto->setGuiaDeTalles($txtTalles);
            $objProducto->setIdMarca(115);
            $objProducto->setIdClase(isset($cbxClase)?$cbxClase:0);
            $objProducto->setPVenta($txtPVenta);
            $objProducto->setPCompra($txtPCompra);
            $objProducto->setPVP($txtPVP);
            $objProducto->setPeso($txtPeso);
            $objProducto->setReferencia($txtReferencia);
            $objProducto->setAlto($txtAlto);
            $objProducto->setAncho($txtAncho);
            $objProducto->setProfundidad($txtProfundidad);
            $objProducto->setHabilitado(1);
            $objProducto->setOculto(isset($chkOculto)?1:0);
            $objProducto->setUltimos($txtUltimos);
            $objProducto->setIdLocal($cbxLocal);
           
            if(isset($_FILES["txtImagenPrincipal"])) {
                $ext = explode(".", $_FILES["txtImagenPrincipal"]["name"]);
                $objProducto->setImagen_Principal_Origen($_FILES["txtImagenPrincipal"]["tmp_name"]);
                $objProducto->setImagen_Principal_Destino("imagen.".$ext[1]);
            }
            
            
            foreach ($_FILES["txtImagenAdicional"]["name"] as $key => $value) {
                $ext = explode(".", $_FILES["txtImagenAdicional"]["name"][$key]);
                $objProducto->addImagen_Origen($_FILES["txtImagenAdicional"]["tmp_name"][$key]);
                $objProducto->addImagen_Destino("imagen.".$ext[1]);
            }
            
            
            $IdProductoNuevo = dmProductos::save($objProducto);
            dmProductos::saveCategoriasProducto($IdProductoNuevo, $cbxCategoria);
            
            if($IdProducto != 0) {
            	$IdCampania = dmProductos::getIdCampaniaByIdCodigoProdInterno(dmProductos::getIdCodigoProdInternoActual($IdProducto));
	            
            	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->clean(
				    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
				    array('group_cache_catalogo_campania_'.$IdCampania)
				);
            }
        
	    	if (isset($_POST['botonGuardarStock'])) { 
	    		header("Location: ".UrlResolver::getBaseURL("bo/productos/ABM_stock_new.php?IdProducto=$IdProductoNuevo&IdCampania=$txtIdCampania"));
				die;
	    	}
	    
	    	if (isset($_POST['botonGuardarSalir'])) { 
	    		header("Location: ".UrlResolver::getBaseURL("bo/campanias/productos.php?page=".$_GET['page']."&IdCampania=".$txtIdCampania));
				die;
	    	}
	    	
        }
        catch(MySQLException $e) {
        	echo $e;
        }
        ob_flush();
    }
?>