<?php

    	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    	Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
        ob_start();
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
	    
        try {   
        	$objProducto = dmProductos::getById($_GET["IdProducto"]);
        	
            if ($_GET["Habilitado"]==1) {
            	dmProductos::updateHabilitado($_GET["IdProducto"], 0);
            }
            else {
            	dmProductos::updateHabilitado($_GET["IdProducto"], 1);
            }
            
	    	header("Location: ".UrlResolver::getBaseURL("bo/campanias/productos.php?IdCampania=".$_GET["IdCampania"]));
			die;
	    	
        }
        catch(MySQLException $e) {
        	echo $e;
        }
        ob_flush();
?>