<?php
    if($_POST) {
    	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    	Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
        
        ob_start();
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        extract($_POST);
    	
	    if (isset($_POST['cancelar'])) { 
            header("Location: ".UrlResolver::getBaseURL("bo/productos/ABM_stock_new.php?IdProducto=$txtIdProducto&IdCampania=$txtIdCampania"));
			die;
	    }
        
        try {   
            $IdProducto = ($txtIdProducto == "0")?0:$txtIdProducto;
            
            $CodigoProdInterno = ($txtCodigoProdInterno == "0")?0:$txtCodigoProdInterno;
            
            $objPP = new MySQL_ProductosProveedores();
            
            $objPP->setIdProducto($IdProducto);
            $objPP->setIdCodigoProdInterno($CodigoProdInterno);
            $objPP->setIdProveedor(5);
            $objPP->setStock($txtStock);
            $objPP->setRegalo(0);
            $objPP->setIncremento("+0.00");
            $objPP->setComentarios("");
            $objPP->setHabilitado(1);
            
            
            foreach($txtAtributo as $key => $value) {
            	$objAtributo = new MySQL_ProductosAtributos();
                $objAtributo->setidAtributoClase($key);
                $objAtributo->setValor($value);
                $objPP->addAtributo($objAtributo);
            }
            dmProductos::agregarStockNew($objPP);
            
            if($IdProducto != 0) {
            	$IdCampania = dmProductos::getIdCampaniaByIdCodigoProdInterno(dmProductos::getIdCodigoProdInternoActual($IdProducto));
	            
            	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->clean(
				    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
				    array('group_cache_catalogo_campania_'.$IdCampania)
				);
            }
            header("Location: ".UrlResolver::getBaseURL("bo/productos/ABM_stock_new.php?IdProducto=$IdProducto&IdCampania=$txtIdCampania"));
			die;
        }
        catch(MySQLException $e) {
        	echo $e;
        }
        ob_flush();
    }
?>