<?php

    if($_POST)
    {
        define("MAX_IMAGENES",8);
        ob_start();
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php';
	    extract($_POST);

	    
        try
        {   
            $IdProducto = ($txtIdProducto == "0") ? 0 : Aplicacion::Decrypter($txtIdProducto);
            $objProducto = new MySQL_Productos();
            $objProducto->setIdProducto($IdProducto);
            $objProducto->setNombre($txtNombre);
            $objProducto->setDescripcion($txtDescripcion);
            $objProducto->setDescripcionAmpliada($txtDescripcionAmpliada);
            $objProducto->setGuiaDeTalles($txtTalles);
            $objProducto->setIdMarca($cbxMarca);
            $objProducto->setIdClase(isset($cbxClase)?$cbxClase:0);
            $objProducto->setPVenta($txtPVenta);
            $objProducto->setPCompra($txtPCompra);
            $objProducto->setPVP($txtPVP);
            $objProducto->setPeso($txtPeso);
            $objProducto->setReferencia($txtReferencia);
            $objProducto->setAlto($txtAlto);
            $objProducto->setAncho($txtAncho);
            $objProducto->setProfundidad($txtProfundidad);
            $objProducto->setHabilitado(isset($chkHabilitado)?1:0);
            $objProducto->setOculto(isset($chkOculto)?1:0);
            $objProducto->setUltimos($txtUltimos);
            if (isset($txt_tbl_locales))
            {
                foreach($txt_tbl_locales as $local)
                {
                    $objProducto->setIdLocal($local);
                }
            }
            if(isset($chkEliminar))
            {
                foreach($chkEliminar as $chk)
                {
                    @unlink(Aplicacion::getProductosImagenesRoot($IdProducto)."imagenes/".$chk);
                }
            }   
            if(isset($_FILES["txtImagenPrincipal"]))
            {
                $ext = explode(".", $_FILES["txtImagenPrincipal"]["name"]);
                $objProducto->setImagen_Principal_Origen($_FILES["txtImagenPrincipal"]["tmp_name"]);
                $objProducto->setImagen_Principal_Destino("imagen.".$ext[1]);
            }
            for($i=0; $i<MAX_IMAGENES;$i++)
            {
                if($_FILES["txtImagen"]["size"][$i]>0)
                {
                    $ext = explode(".", $_FILES["txtImagen"]["name"][$i]);
                    $objProducto->addImagen_Origen($_FILES["txtImagen"]["tmp_name"][$i]);
                    $objProducto->addImagen_Destino("imagen.".$ext[1]);
                }
            }
            dmProductos::save($objProducto);
            
            if($IdProducto != 0) {
            	$IdCampania = dmProductos::getIdCampaniaByIdCodigoProdInterno(dmProductos::getIdCodigoProdInternoActual($IdProducto));
	            
            	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->clean(
				    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
				    array('group_cache_catalogo_campania_'.$IdCampania)
				);
            }
            
            ?>
                <script type="text/javascript">
                    window.location.href = "../../bo/productos/index.php";
                </script>
            <?
        }
        catch(MySQLException $e)
        {
             ?>
                <script>
                    window.ErrorMSJ(<?=$e->getNumeroSQLERROR()?>);
                </script>
            <?
        }
        ob_flush();
    }
?>