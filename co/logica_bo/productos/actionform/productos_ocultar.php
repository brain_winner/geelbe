<?php

    	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    	Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
        ob_start();
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
	    
        if(!isset($_GET['page']) || $_GET['page'] < 1) $_GET['page'] = 1;

       try {   
        	$objProducto = dmProductos::getById($_GET["IdProducto"]);
        	
            if ($_GET["Ocultar"]==1) {
            	dmProductos::updateOculto($_GET["IdProducto"], 1);
            }
            else {
            	dmProductos::updateOculto($_GET["IdProducto"], 0);
            }
            
            //Traer IdCampania para limpiar cache
            if(isset($_GET['IdCampania']))
            	$idc = $_GET['IdCampania'];
            else {
	            $Cnn = Conexion::nuevo();
	            try
	            {
	                $Cnn->Abrir();
	                $Cnn->setQuery("SELECT c.IdCampania FROM categorias c JOIN productos_x_categorias pc ON pc.IdCategoria = c.IdCategoria WHERE pc.IdProducto =".$_GET['IdProducto']." LIMIT 1");
	                $Tabla = current($Cnn->DevolverQuery());
	                $idc = $Tabla['IdCampania'];
	            }        
	            catch(MySQLException $e)
	            {
	            }
            }

			if(is_numeric($idc)) {          
	            	
	            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->clean(
				    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
				    array('group_cache_catalogo_campania_'.$idc)
				);
				
			}
	            
            if(isset($_GET['IdCampania'])) {
            	header("Location: ".UrlResolver::getBaseURL("bo/campanias/productos.php?IdCampania=".$_GET["IdCampania"]."&page=".$_GET['page']));
		    } else
		    	header("Location: ".UrlResolver::getBaseURL("bo/productos/index.php?page=".$_GET['page']));
			die;
	    	
        }
        catch(MySQLException $e) {
        	echo $e;
        }
        ob_flush();
?>