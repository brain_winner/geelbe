<?php

    if($_POST) {
    	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    	Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
        ob_start();
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        extract($_POST);
	    
        try {   
            if(isset($_FILES["txtJson"])) {
                $json = file_get_contents($_FILES["txtJson"]["tmp_name"]);
                $producto_array = json_decode($json, true);
            //    echo $_REQUEST["idCategoria"]."<br><br><br>";
            //    echo var_dump($producto_array);
            //    echo "<br><br><br>";
            }
            
            $IdProducto = 0;
            $objProducto = new MySQL_Productos();
            $objProducto->setIdProducto($IdProducto);
            $objProducto->setNombre(utf8_decode($producto_array["Nombre"]));
            $objProducto->setDescripcion(utf8_decode($producto_array["DBreve"]));
            $objProducto->setDescripcionAmpliada(utf8_decode($producto_array["DAmpliada"]));
            $objProducto->setGuiaDeTalles($producto_array["Talles"]);
            $objProducto->setIdMarca(11);//333
            $objProducto->setIdClase(isset($producto_array["IdClase"])?$producto_array["IdClase"]:0);
            $objProducto->setPVenta($producto_array["PVenta"]);
            $objProducto->setPCompra($producto_array["PCompra"]);
            $objProducto->setPVP($producto_array["PVMercado"]);
            $objProducto->setPeso($producto_array["Peso"]);
            $objProducto->setReferencia(utf8_decode($producto_array["Referencia"]));
            $objProducto->setAlto($producto_array["Alto"]);
            $objProducto->setAncho($producto_array["Ancho"]);
            $objProducto->setProfundidad($producto_array["Profundidad"]);
            $objProducto->setHabilitado(1);
            $objProducto->setUltimos($producto_array["Ultimos"]);
            $a[0]=0;
            $objProducto->setIdLocal($a);
            
            
            $IdProductoNuevo = dmProductos::save($objProducto);
            
            $cbxCategoria = array();
            $cbxCategoria[0] = $_REQUEST["idCategoria"];
            dmProductos::saveCategoriasProducto($IdProductoNuevo, $cbxCategoria);
            
            echo "COMPLETO A <br><br><br>";
            
            if ($IdProducto != 0) {
            	$IdCampania = dmProductos::getIdCampaniaByIdCodigoProdInterno(dmProductos::getIdCodigoProdInternoActual($IdProducto));
	            
            	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->clean(
				    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
				    array('group_cache_catalogo_campania_'.$IdCampania)
				);
            }
        
        	if ($IdProductoNuevo != 0) {
        		rcopy(Aplicacion::getProductosImagenesRoot($producto_array["IdProducto"]), Aplicacion::getProductosImagenesRoot($IdProductoNuevo));
        	}
            
	    	header("Location: ".UrlResolver::getBaseURL("bo/campanias/productos.php?IdCampania=".$txtIdCampania));
			die;
	    	
        }
        catch(MySQLException $e) {
        	echo $e;
        }
        ob_flush();
    }

?>


<?php

// removes files and non-empty directories
function rrmdir($dir) {
  if (is_dir($dir)) {
    $files = scandir($dir);
    foreach ($files as $file)
    if ($file != "." && $file != "..") rrmdir("$dir/$file");
    rmdir($dir);
  }
  else if (file_exists($dir)) unlink($dir);
} 

// copies files and non-empty directories
function rcopy($src, $dst) {
  if (file_exists($dst)) rrmdir($dst);
  if (is_dir($src)) {
    mkdir($dst);
    $files = scandir($src);
    foreach ($files as $file)
    if ($file != "." && $file != "..") rcopy("$src/$file", "$dst/$file"); 
  }
  else if (file_exists($src)) copy($src, $dst);
}
?>
