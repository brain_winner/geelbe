<?php
    if($_POST)
    {
        ob_start();
        extract($_POST);
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php';
        
        
        try
        {   
            $IdProducto = ($txtIdProducto == "0") ? 0 : Aplicacion::Decrypter($txtIdProducto);
            $objProducto = new MySQL_Productos();
            $objProducto->setIdProducto($IdProducto);
            if(isset($txt_tbl))
            {
                foreach($txt_tbl as $i=>$fila)
                {
                    $objPP = new MySQL_ProductosProveedores();
                    $objPP->setIdCodigoProdInterno($fila[0]);
                    $objPP->setIdProveedor($fila[1]);
                    $objPP->setStock($fila[6]);
                    $objPP->setRegalo($fila[7]);
                    $objPP->setIncremento($fila[8]);
                    $objPP->setComentarios($fila[9]);
                    $objPP->setHabilitado(($fila[10] == "Si")?1:0);
                    
                    $IdAtributos = $fila[3];
                    $IdAtributos = explode("|", $IdAtributos);
                    $AtributosValores = $fila[5];
                    $AtributosValores = explode("|", $AtributosValores);
                    
                    for($i=0;$i<count($IdAtributos);$i++)
                    {
                        if ($IdAtributos[$i] != "")
                        {
                            $objAtributo = new MySQL_ProductosAtributos();
                            $objAtributo->setidAtributoClase($IdAtributos[$i]);
                            $objAtributo->setValor($AtributosValores[$i]);
                            $objPP->addAtributo($objAtributo);
                        }
                    }
                    $objProducto->addProductosProveedores($objPP);
                }
            }
            dmProductos::agregarStock($objProducto);
            
            if($IdProducto != 0) {
            	$IdCampania = dmProductos::getIdCampaniaByIdCodigoProdInterno(dmProductos::getIdCodigoProdInternoActual($IdProducto));
	            
            	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->clean(
				    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
				    array('group_cache_catalogo_campania_'.$IdCampania)
				);
            }
            
            ?>
                <script type="text/javascript">
                    window.location = DIRECTORIO_URL_BO+"productos/index.php"; 
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                    window.ErrorMSJ(<?=$e->getNumeroSQLERROR()?>);
                </script>
            <?
        }
        ob_flush();
    }
?>