<?php
    class MySQL_Productos extends productos
    {
        private $_imagen_principal_origen;
        private $_imagen_principal_destino;
        private $_imagen_origen=array();
        private $_imagen_destino=array();
        
        public function setImagen_Principal_Origen($imagen_principal_origen)
        {
            $this->_imagen_principal_origen = $imagen_principal_origen;
        }
        Public function setImagen_Principal_Destino($imagen_principal_destino)
        {
            $this->_imagen_principal_destino = $imagen_principal_destino;
        }
        public function getImagen_Principal_Origen()
        {
            return $this->_imagen_principal_origen;
        }
        Public function getImagen_Principal_Destino()
        {
            return $this->_imagen_principal_destino;
        }
        
        public function addImagen_Origen($imagen_origen)
        {
            $this->_imagen_origen[] = $imagen_origen;
        }
        public function addImagen_Destino($imagen_destino)
        {
            $this->_imagen_destino[] = $imagen_destino;
        }
        public function getImagen_Origen($index)
        {
            return $this->_imagen_origen[$index];
        }
        public function getImagenes_Origen()
        {
            return $this->_imagen_origen;
        }
        Public function getImagen_Destino($index)
        {
            return $this->_imagen_destino[$index];
        }
        Public function getImagenes_Destino()
        {
            return $this->_imagen_destino;
        }
        public function  CrearDirectorio()
        {
            try
            {
                
                @mkdir(Aplicacion::getProductosImagenesRoot($this->getIdProducto()),0777);
                @mkdir(Aplicacion::getProductosImagenesRoot($this->getIdProducto())."thumbs",0777);
            }
            catch(Exception $e)
            {
                throw $e;
            }        
        }
        /**
        * Metodo para borrar directorios de la campa�as
        */
        public function  EliminarDirectorio()
        {
            chdir("..");
            chdir("productos");
            $this->EliminarDir("productos_" . $this->getIdProducto());
        }
        public function  EliminarDir($carpeta )
        { 
            try
            {
                $directorio = @opendir($carpeta);
                while ($archivo = @readdir($directorio))
                { 
                    if($archivo !='.' && $archivo !='..')
                    {
                        //si es un directorio, volvemos a llamar a la funci�n para que elimine el contenido del mismo
                        if (is_dir($archivo )) 
                            $this->_EliminarDir($archivo ); 
                        //si no es un directorio, lo borramos 
                        //unlink($archivo); 
                    } 
                }
                closedir($directorio); 
                rmdir($carpeta);
            }
            catch(exception $e)
            {
                return false;
            }
            return true;        
        }
        public function Upload()
        {
            try
            {
                if(strlen(trim($this->getGuiaDeTalles()))>15)
                {
                    if (file_exists(Aplicacion::getProductosImagenesRoot($this->getIdProducto()). "talles.html"))
                        clsFile::Delete(Aplicacion::getProductosImagenesRoot($this->getIdProducto()). "talles.html");
                    $oArchivo = new clsFile(Aplicacion::getProductosImagenesRoot($this->getIdProducto()). "talles.html");
                    $oArchivo->Abrir(Aplicacion::getProductosImagenesRoot($this->getIdProducto()). "talles.html", "w");
                    $oArchivo->Escribir($this->_guiadetalles);
                    $oArchivo->Cerrar();                    
                }
                else  
                {
                    if (file_exists(Aplicacion::getProductosImagenesRoot($this->getIdProducto()). "talles.html"))
                    {
                        clsFile::Delete(Aplicacion::getProductosImagenesRoot($this->getIdProducto()). "talles.html");
                    }
                }
                
                if(strlen($this->getImagen_Principal_Origen())>0)
                {
                    $Archivo = new clsFile(Aplicacion::getProductosImagenesRoot($this->getIdProducto()).$this->getImagen_Principal_Destino());
                    clsFile::Delete(Aplicacion::getProductosImagenesRoot($this->getIdProducto()).$Archivo->getNombre(), ".*");
                    clsFile::Delete(Aplicacion::getProductosImagenesRoot($this->getIdProducto())."producto_1",".*");
                    clsFile::Delete(Aplicacion::getProductosImagenesRoot($this->getIdProducto())."thumbs/thumb_producto_1",".*");
                    
                     copy($this->getImagen_Principal_Origen(), Aplicacion::getProductosImagenesRoot($this->getIdProducto())."imagen_1.".get_extension($this->getImagen_Principal_Destino()));
                     Thumbnail(Aplicacion::getProductosImagenesRoot($this->getIdProducto())."imagen_1.".get_extension($this->getImagen_Principal_Destino()), Aplicacion::getProductosImagenesRoot($this->getIdProducto())."producto_1.".get_extension($this->getImagen_Principal_Destino()),240, 249);
                     Thumbnail(Aplicacion::getProductosImagenesRoot($this->getIdProducto())."imagen_1.".get_extension($this->getImagen_Principal_Destino()), Aplicacion::getProductosImagenesRoot($this->getIdProducto())."thumbs/thumb_producto_1.".get_extension($this->getImagen_Principal_Destino()),48,50);
                }
                for($i=0; $i<count($this->getImagenes_Origen());$i++)
                {
                    if(strlen($this->getImagen_Origen($i))>0)
                    {
                        $Archivo = new clsFile(strtolower(Aplicacion::getProductosImagenesRoot($this->getIdProducto()).$this->getImagen_Destino($i)));
                        clsFile::Delete(Aplicacion::getProductosImagenesRoot($this->getIdProducto())."producto_".($i+2), ".*");
                        clsFile::Delete(Aplicacion::getProductosImagenesRoot($this->getIdProducto())."thumbs/thumb_producto_".($i+2), ".*");
                        
                        //copy($this->getImagen_Origen($i), strtolower(Aplicacion::getProductosImagenesRoot($this->getIdProducto())."/".$this->getImagen_Destino($i)));
                        copy($this->getImagen_Origen($i), strtolower(Aplicacion::getProductosImagenesRoot($this->getIdProducto())."imagen_".($i+2) . "." . get_extension($this->getImagen_Destino($i))));
                        //Thumbnail(Aplicacion::getProductosImagenesRoot($this->getIdProducto()).$this->getImagen_Origen($i), Aplicacion::getProductosImagenesRoot($this->getIdProducto())."producto_".($i+2).".".get_extension($this->getImagen_Destino($i)),240, 249);
                        Thumbnail(strtolower(Aplicacion::getProductosImagenesRoot($this->getIdProducto())."imagen_".($i+2) . "." . get_extension($this->getImagen_Destino($i))), Aplicacion::getProductosImagenesRoot($this->getIdProducto())."producto_".($i+2).".".get_extension($this->getImagen_Destino($i)),240, 249);
                        //Thumbnail(Aplicacion::getProductosImagenesRoot($this->getIdProducto()).$this->getImagen_Origen($i), Aplicacion::getProductosImagenesRoot($this->getIdProducto())."thumbs/thumb_producto_".($i+2).".".get_extension($this->getImagen_Destino($i)),48,50);
                        Thumbnail(strtolower(Aplicacion::getProductosImagenesRoot($this->getIdProducto())."imagen_".($i+2) . "." . get_extension($this->getImagen_Destino($i))), Aplicacion::getProductosImagenesRoot($this->getIdProducto())."thumbs/thumb_producto_".($i+2).".".get_extension($this->getImagen_Destino($i)),48,50);
                    }
                }
                @unlink($this->getImagen_Principal_Destino());   
            }
            catch(Exception $e)
            {
                throw $e;
            }
        }
        public function getFiles()
        {
            try
            {
                $files=array();
                $i=0;
                $dir=@opendir(Aplicacion::getProductosImagenesRoot($this->getIdProducto()));
                while($archivo = @readdir($dir))
                {
                    if($archivo != "." && $archivo != ".." && $archivo != "thumbs")
                    {
                        $files[$i]["link"] = Aplicacion::getProductosImagenesRootUrl($this->getIdProducto());
                        $files[$i]["nombre"] = $archivo;
                        $i++;
                    }
                }
                @closedir($dir);
                $dir=@opendir(Aplicacion::getProductosImagenesRoot($this->getIdProducto())."thumbs/");
                while($archivo = @readdir($dir))
                {
                    if($archivo != "." && $archivo != "..")
                    {
                        $files[$i]["link"] = Aplicacion::getProductosImagenesRootUrl($this->getIdProducto());
                        $files[$i]["nombre"] = "thumbs/".$archivo;
                        $i++;
                    }
                }
                @closedir($dir);
                return $files;
            }
            catch(Exception $e)
            {
                throw $e;
            }
        }
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_idproducto")
                {   
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=($valor != 0) ? "'".$valor."', " : "NULL, ";
                }
                else if($propiedad != "_guiadetalles" && $propiedad != "_locales")
                {   
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = '" . mysql_real_escape_string($valor)."', ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .="'".mysql_real_escape_string($valor)."', ";
                }
            }
            return "INSERT INTO productos (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
    }
    class MySQL_ProductosProveedores extends productos_x_proveedores
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_idcodigoprodinterno")
                {   
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=($valor != 0) ? chr(34).mysql_real_escape_string($valor).chr(34).", " : "NULL, ";
                }
                else
                {   
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
            }
            return "INSERT INTO productos_x_proveedores (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
    }
    class MySQL_ProductosAtributos extends productos_x_atributos
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
            }
            return "INSERT INTO productos_x_atributos(".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
    }
    function CrearCombos_x_IdProducto($IdProducto)
    {
        try
        {
            $arrAtributos = dmProductos::AtributosByIdProducto($IdProducto);
            $primer_atributo = null;
            ?>
                <script>vAtributos = new Array();</script>
            <?
            foreach($arrAtributos as $atributo)
            {
                if (is_null($primer_atributo))
                    $primer_atributo = $atributo["IdAtributoClase"];
                ?>
                    <script>vAtributos.push(<?=$atributo["IdAtributoClase"]?>);</script>                    
                <?
            }
            $primer_atributo = null;
            ?><div style="width:100%; hieght:100%"><table style="width:100%; hieght:100%"><?
            foreach($arrAtributos as $i=>$atributo)
            {
                if (is_null($primer_atributo))
                    $primer_atributo = $atributo["IdAtributoClase"];
                ?>
                    <tr><td>
                    <span>Seleccione <?=$atributo["Nombre"]?></span></td>
                    <td><select id="cbx_<?=$atributo["IdAtributoClase"]?>" name="cbx_<?=$atributo["IdAtributoClase"]?>" onchange="CargarComboAtributos(<?=$i+1?>);" class="">
                        <option value="-1">***Seleccione***</option>
                    </select>
                    </td></tr>
                <?
            }
            ?></table></div><?
            if (!(is_null($primer_atributo)))
            {
                ?>
                    <script>CargarComboAtributos(0);</script>
                <?
            }
            else if((count($arrAtributos)) == 0)
            {
                ?>
                <script>NoAtributos();</script>
                <?
            }
        }
        catch(exception $e)
        {
            throw $e;
        }
    }
    function HTMLCrearCombos_x_IdProducto($IdProducto)
    {
        try
        {
            $arrAtributos = dmProductos::AtributosByIdProducto($IdProducto);
            $primer_atributo = null;
            $html = "";
            ?>
                <script>vAtributos = new Array();</script>
            <?
            foreach($arrAtributos as $atributo)
            {
                if (is_null($primer_atributo))
                    $primer_atributo = $atributo["IdAtributoClase"];
                ?>
                    <script>vAtributos.push(<?=$atributo["IdAtributoClase"]?>);</script>                    
                <?
            }
            $primer_atributo = null;
            $html = "<div style='width:100%; height:100%'><table style='width:100%; height:100%'>";
            foreach($arrAtributos as $i=>$atributo)
            {
                if (is_null($primer_atributo))
                    $primer_atributo = $atributo["IdAtributoClase"];
                $html .= "<tr><td>";
                $html .= "<span>Selecciona ".$atributo["Nombre"]. "</span></td>";
                $html .= "<td><select id='cbx_".$atributo["IdAtributoClase"]."' name='cbx_".$atributo["IdAtributoClase"]."' onchange='CargarComboAtributos(".($i+1).");' class=''>";
                $html .= "<option value='-1'>Seleccione</option>";
                $html .= "</select></td></tr>";
            }
            $html .="</table></div>";
            ?>
            
            <script>
                document.getElementById("divAtributos").innerHTML = "<?=$html?>";
            </script>
            <?
            if (!(is_null($primer_atributo)))
            {
                ?>
                    <script>CargarComboAtributos(0);</script>
                <?
            }
            else if((count($arrAtributos)) == 0)
            {
                ?>
                <script>NoAtributos();</script>
                <?
            }
        }
        catch(exception $e)
        {
            throw $e;
        }
    }
    function HTMLCrearCombos_x_IdProducto_New($IdProducto) {
            $arrAtributos = dmProductos::AtributosByIdProducto($IdProducto);
            $primer_atributo = null;
            $html = "";
            ?>
                <script>vAtributos = new Array();</script>
            <?
            foreach($arrAtributos as $atributo) {
                if (is_null($primer_atributo))
                    $primer_atributo = $atributo["IdAtributoClase"];
                ?>
                    <script>vAtributos.push(<?=$atributo["IdAtributoClase"]?>);</script>                    
                <?
            }
            $primer_atributo = null;
            $html = "";
            foreach($arrAtributos as $i=>$atributo) {
                if (is_null($primer_atributo))
                    $primer_atributo = $atributo["IdAtributoClase"];
                $html .= "<li>";
                $html .= "<span class='item'>Seleccione ".$atributo["Nombre"]. "</span>";
                $html .= "<select class='opciones-item' id='cbx_".$atributo["IdAtributoClase"]."' name='cbx_".$atributo["IdAtributoClase"]."' onchange='CargarComboAtributos(".($i+1).");'>";
                $html .= "<option value='-1'>Seleccione</option>";
                $html .= "</select>";
                $html .= "</li>";
            }
            ?>
            
            <script>
                document.getElementById("attributes").innerHTML = document.getElementById("attributes").innerHTML + "<?=$html?>";
            </script>
            <?
            if (!(is_null($primer_atributo))) {
                ?>
                    <script>CargarComboAtributos(0);</script>
                <?
            }
            else if((count($arrAtributos)) == 0) {
                ?>
                <script>NoAtributos();</script>
                <?
            }
    }
    function GenerarMatrizJS($IdProducto)
    {
        try
        {
            $arrDatos = dmProductos::getProductosInternos($IdProducto);
            $js = "<script>".chr(13);
            $js.= "var vValores = new Array();".chr(13);
            foreach($arrDatos as $i=>$pp)
            {
                $js.= "vValores[$i]= new Array();".chr(13);
                $js.= "vValores[$i]['IdCodigoProdInterno'] = ".$pp["IdCodigoProdInterno"].";".chr(13);
                $js.= "vValores[$i]['Stock'] = ".$pp["Stock"].";".chr(13);
                $js.= "vValores[$i]['Precio'] = ".ObtenerPrecioProductos($pp["PVenta"], $pp["Incremento"]).";".chr(13);
                $js.= "vValores[$i]['Regalo'] = ".$pp["Regalo"].";".chr(13);
                $js.= "vValores[$i]['Incluir'] = 1;".chr(13);
                $arrAtributos = dmProductos::getAtributosByIdCodigoProdInterno($pp["IdCodigoProdInterno"]);
                $js.= "vValores[$i]['Atributos'] = new Array();".chr(13);
                foreach($arrAtributos as $j=>$atr)
                {
                    $js.= "vValores[$i]['Atributos'][$j] = new Array();".chr(13);
                    $js.= "vValores[$i]['Atributos'][$j]['IdAtributoClase'] = ".$atr["idAtributoClase"].";".chr(13);
                    $js.= "vValores[$i]['Atributos'][$j]['Valor'] = \"".$atr["Valor"]."\";".chr(13);
                }
            }
            $js .= "</script>";
            echo $js;
        }
        catch(exception $e)
        {
            throw $e;
        }
    }
?>
