<?php

class ListadoColumnBean {

	const TEXTBOX_FILTER = 1;
	const COMBOBOX_FILTER = 2;
	
	private $columnName;
	private $sortName;
	private $filterName;
	private $filterCombo;
	private $filterType;

	private $showFilter;
	private $sortColumn;

	private $filterBoxWidth;

	private $columnDesc;
	private $columnLink;
	private $columnButtons;
	private $columnLinkTitle;
	
	public function ListadoColumnBean($columnName, $columnDesc, $showFilter, $sortColumn, $filterCombo) {
		$this->columnName = $columnName;
		$this->sortName = $columnName;
		$this->filterName = $columnName;
		$this->filterCombo = $filterCombo;
		
		if(isset($filterCombo)) {
			$this->filterType = self::COMBOBOX_FILTER;
		} else {
			$this->filterType = self::TEXTBOX_FILTER;
		}
		
		$this->columnDesc = $columnDesc;
		$this->showFilter = $showFilter;
		$this->sortColumn = $sortColumn;
		
		$this->columnButtons = array();
	}
	
	public function addColumnButton($buttonSource) {
		$this->columnButtons[] = $buttonSource;
	}
	
	public function setColumnName($columnName) {
		$this->columnName = $columnName;
	}
	
	public function setFilterName($filterName) {
		$this->filterName = $filterName;
	}
	
	public function setFilterBoxWidth($filterBoxWidth) {
		$this->filterBoxWidth = $filterBoxWidth;
	}
	
	public function setSortName($columnName) {
		$this->sortName = $columnName;
	}
	
	public function setColumnDesc($columnDesc) {
		$this->columnDesc = $columnDesc;
	}
	
	public function setShowFilter($showFilter) {
		$this->showFilter = $showFilter;
	}
	
	public function setSortColumn($sortColumn) {
		$this->sortColumn = $sortColumn;
	}

	public function setColumnLink($columnLink) {
		$this->columnLink = $columnLink;
	}

	public function setColumnLinkTitle($columnLinkTitle) {
		$this->columnLinkTitle = $columnLinkTitle;
	}
	
	public function setColumnButtons($buttonsSource) {
		$this->columnButtons = $buttonsSource;
	}
	
	public function setFilterCombo($filterCombo) {
		$this->filterCombo = $filterCombo;
		if(isset($filterCombo)) {
			$this->filterType = self::COMBOBOX_FILTER;
		} else {
			$this->filterType = self::TEXTBOX_FILTER;
		}
	}

	public function getColumnName() {
		return $this->columnName;
	}

	public function getFilterName() {
		return $this->filterName;
	}

	public function getSortName() {
		return $this->sortName;
	}
	
	public function getColumnDesc() {
		return $this->columnDesc;
	}
	
	public function getFilterBoxWidth() {
		return $this->filterBoxWidth;
	}
	
	public function getShowFilter() {
		return $this->showFilter;
	}
	
	public function getSortColumn() {
		return $this->sortColumn;
	}
	
	public function getColumnLink() {
		return $this->columnLink;
	}
	
	public function getColumnLinkTitle() {
		return $this->columnLinkTitle;
	}
	
	public function getColumnButtons() {
		return $this->columnButtons;
	}
	
	public function getFilterType() {
		return $this->filterType;
	}
	
	public function getFilterCombo() {
		return $this->filterCombo;
	}
}

?>