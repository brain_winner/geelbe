<?php

require_once("ListadoColumnBean.php");

class ListadoPrinter {
	
	private $data;
	private $columns = array();
	private $hiddenFilterValues = array();
	private $tableClass;
	private $pageUrl;
	private $listadoPaginator;
	private $disableFiltering = false;
	
	public function ListadoPrinter($data, $pageUrl, $listadoPaginator) {
		$this->data = $data;
		$this->listadoPaginator = $listadoPaginator;
		$this->pageUrl = $pageUrl;
		
		# Ugly Hack!! TODO: hacer algo mas lindo en los metodos que agregar parametros en la url.
		if(strpos($pageUrl, '?') <= 0) {
			$this->pageUrl = $pageUrl."?";
		}
	}
	
	public function showColumn($columnName, $columnDesc = null, $showFilter = true, $sortColumn = true, $filterCombo = null) {
		$description = $columnName;
		if(isset($columnDesc)) {
			$description = $columnDesc;
		}
		$this->columns[$columnName] = new ListadoColumnBean($columnName, $description, $showFilter, $sortColumn, $filterCombo);
	}
	
	public function linkColumn($columnName, $url, $linkTitle = "") {
		$this->columns[$columnName]->setColumnLink($url);
		$this->columns[$columnName]->setColumnLinkTitle($linkTitle);
	}
	
	public function addButtonToColumn($columnName, $buttonSource) {
		$this->columns[$columnName]->addColumnButton($buttonSource);
	}
	
	public function addHiddenValue($name, $value) {
		$this->hiddenFilterValues[$name] = $value;
	}
	
	public function setTableClass($tableClazz) {
		$this->tableClass = $tableClass;
	}
	
	public function setDisableFiltering($disableFilteringToSet) {
		$this->disableFiltering = $disableFilteringToSet;
	}
	
	public function setShowFilter($columnName, $value) {
		$this->columns[$columnName]->setShowFilter($value); 
	}
	
	public function setSortColumn($columnName, $value) {
		$this->columns[$columnName]->setSortColumn($value); 
	}
	
	public function setSortName($columnName, $sortName) {
		$this->columns[$columnName]->setSortName($sortName); 
	}
	
	public function setFilterName($columnName, $filterName) {
		$this->columns[$columnName]->setFilterName($filterName);
	}
	
	public function setFilterBoxWidth($columnName, $width) {
		$this->columns[$columnName]->setFilterBoxWidth($width); 
	}
	
	public function printListado() {
		echo '<form name="filterForm" id="filterForm" action='.$_SERVER['REQUEST_URI'].' method="get">';
		$this->printHiddenValues();
		
		if(sizeof($this->data) > 0) {
			echo '<p class="resultLegend">Resultados: '.$this->listadoPaginator->getTotalNumberOfItems().
				 ' (Visualizando: '.($this->listadoPaginator->getOffset() + 1). 
				 ' al '.$this->listadoPaginator->getEndOffset(sizeof($this->data)).')</p>';
		}
		
		if(!$this->disableFiltering) {
			echo '<input name="filterButton" type="submit" value="Filtrar" class="filtrar" />';
			echo '<input name="clearFilterButton" type="submit" value="Borrar Filtro" class="filtrar" />';
		}
		echo '<br style="clear:both;" />';
		echo '<table class="'.$this->tableClass.'">';
		
		$this->printHeader();
		$this->printFilters();

		if(sizeof($this->data) == 0) {
			echo '<tr class="alt">';
			echo '<td class="emptytable" colspan="'.sizeof($this->columns).'">Sin Resultados</td>';
			echo '</tr>';
		} else {
			$i = 0;
			foreach($this->data as $dataCol) {
				echo '<tr'.($i%2 == 0 ? ' class="alt"': '').'>';
				foreach($this->columns as $column) {
					$link = $column->getColumnLink();
					if(isset($link)) {
						echo '<td><a title="'.$column->getColumnLinkTitle().'" href="'.ListadoUtils::parseColumnLink($link, $dataCol).'">'.
							$dataCol[$column->getColumnName()].'</a>'.$this->getParsedColumnButtons(
									$column->getColumnButtons(), $dataCol).'</td>';
					} else {
						echo '<td>'.$dataCol[$column->getColumnName()].$this->getParsedColumnButtons(
									$column->getColumnButtons(), $dataCol).'</td>';
					}
				}
				echo '</tr>';
				$i++;
			}
		}
		
			
		echo '</table>';
		echo '</form>';
		
		$this->printPaginator();
	}
	
	private function getParsedColumnButtons($columnButtons, $dataCol) {
		$parsedButtons = "";
		foreach($columnButtons as $button) {
			$parsedButtons = $parsedButtons.ListadoUtils::parseColumnLink($button, $dataCol);
		}
		
		return $parsedButtons;
	}
		
	private function addPageParam($url, $page) {
		return isset($page)?$url."&page=".$page:$url;
	}
	
	private function addSortParams($url, $fieldName) {
		$url = $url.'&sortBy='.$fieldName;
		if($_REQUEST['sortBy'] == $fieldName) {
			if($_REQUEST['sortDirection']=="DESC") {
				return $url."&sortDirection=ASC";
			} else {
				return $url."&sortDirection=DESC";
			}
		} 
		return $url."&sortDirection=ASC";
	}
	
	private function addActualSortParam($url) {
		return $url = $url.'&sortBy='.$_REQUEST['sortBy']."&sortDirection=".$_REQUEST['sortDirection'];
	}
	
	private function addFilterParams($url) {
		if(!isset($_REQUEST['clearFilterButton'])) {
			foreach($this->columns as $column) {
				if($column->getShowFilter()) {
					$url = $url."&filter[".$column->getFilterName()."]=".ListadoUtils::getValueFromRequest(
							"filter", $column->getFilterName());
				}
			}
		}
		return $url;
	}
	
	private function printHeader() {
		echo '<tr>';
		foreach($this->columns as $column) {
			if($column->getSortColumn()) {
				echo '<th><a href="'.$this->addSortParams($this->addPageParam($this->addFilterParams($this->pageUrl), $_REQUEST['page']), 
					$column->getSortName()).'">'.$column->getColumnDesc().'</a></th>';
			} else {
				echo '<th>'.$column->getColumnDesc().'</th>';
			}
		}
		echo '</tr>';
	}
	
	private function printFilters() {
		echo '<tr>';
		foreach($this->columns as $column) {
			if($column->getShowFilter()) {
				if($column->getFilterType() == ListadoColumnBean::TEXTBOX_FILTER) {
					$boxStyle = ($column->getFilterBoxWidth()!=null)?'style="width:'.$column->getFilterBoxWidth().'px;"':'';
					echo '<td><input '.$boxStyle.' type="text" name="filter['.$column->getFilterName().']" value="'.
						(isset($_REQUEST['clearFilterButton'])?"":ListadoUtils::getValueFromRequest("filter", $column->getFilterName())).'" /></td>';
				} else {
					$value = (isset($_REQUEST['clearFilterButton'])?"":ListadoUtils::getValueFromRequest("filter", $column->getFilterName()));
					$boxStyle = ($column->getFilterBoxWidth()!=null)?'style="width:'.$column->getFilterBoxWidth().'px;"':'';
					echo '<td><select '.$boxStyle.' name="filter['.$column->getFilterName().']" value="'.$value.'" onchange="document.forms[\'filterForm\'].submit();">';

					echo '<option value="" />';
					foreach($column->getFilterCombo() as $option) {
						if($option["id"] == $value) {
							echo '<option selected="selected" value="'.$option["id"].'">'.$option["val"].'</option>';
						} else {
							echo '<option value="'.$option["id"].'">'.$option["val"].'</option>';
						}
					}
						
					echo '</select></td>';
				}
			} else {
				echo '<td></td>';
			}
		}
		echo '</tr>';
	}
	
	public function getFilteredUrl($url) {
		return $this->addFilterParams($url);
	}
	
	private function printPaginator() {
		echo '<div class="paginador">';
		if(!$this->listadoPaginator->isFirstPage()) {
			echo "<a href=\"".$this->addPageParam($this->addActualSortParam($this->addFilterParams($this->pageUrl)), 
						1)."\">Primera</a>";
		}
		if($this->listadoPaginator->hasPrevPage()) {
			echo "&nbsp;&nbsp;<a href=\"".$this->addPageParam($this->addActualSortParam($this->addFilterParams($this->pageUrl)), 
						$this->listadoPaginator->getActualPage() - 1)."\">&lt;&lt; Anterior</a>";
		}
		
		echo "&nbsp;P&aacute;gina&nbsp;".$this->listadoPaginator->getActualPage();
		
		if($this->listadoPaginator->hasNextPage()) {
			echo "&nbsp;&nbsp;<a href=\"".$this->addPageParam($this->addActualSortParam($this->addFilterParams($this->pageUrl)), 
						$this->listadoPaginator->getActualPage() + 1)."\">Siguiente &gt;&gt;</a>";
		}
		if(!$this->listadoPaginator->isLastPage()) {
			echo "&nbsp;&nbsp;<a href=\"".$this->addPageParam($this->addActualSortParam($this->addFilterParams($this->pageUrl)), 
						$this->listadoPaginator->getNumberOfPages())."\">&Uacute;ltima</a>";
		}
		echo '</div>';
	}
	
	private function printHiddenValues() {
		foreach($this->hiddenFilterValues as $name => $value) {
			echo '<input type="hidden" name="'.$name.'" value="'.$value.'" />';
		}
	}
	
}

