<?

class ListadoPaginator {
	
	private $itemsPerPage;
	private $totalNumberOfItems;
	private $offset;
	private $actualPage;
	private $numberOfPages;
	
	public function ListadoPaginator($totalNumberOfItems, $itemsPerPage = 10) {
		$this->itemsPerPage = $itemsPerPage;
		$this->totalNumberOfItems = $totalNumberOfItems;
		if(!isset($_REQUEST['page'])) {
			$this->actualPage = 1;
		} else {
			$this->actualPage = $_REQUEST['page'];
		}
		$this->offset = ($this->actualPage - 1) * $this->itemsPerPage;
		$this->numberOfPages = ceil($this->totalNumberOfItems / $this->itemsPerPage);		
	}
	
	public function getEndOffset($resultSetCount) {
		if($resultSetCount < $this->itemsPerPage) {
			return $this->offset + $resultSetCount;
		} else {
			return $this->offset + $this->itemsPerPage;
		}
	}
	
	public function isFirstPage() {
		return $this->actualPage == 1;
	}
	
	public function isLastPage() {
		if($this->numberOfPages == 0) {
			return true;
		}
		return $this->actualPage == $this->numberOfPages;
	}
	
	public function hasNextPage() {
		return $this->actualPage < $this->numberOfPages;
	}
	
	public function hasPrevPage() {
		return $this->actualPage > 1;
	}
	
	public function getItemsPerPage() {
		return $this->itemsPerPage;
	}
	
	public function getTotalNumberOfItems() {
		return $this->totalNumberOfItems;
	}
	
	public function getOffset() {
		return $this->offset;
	}
	
	public function getActualPage() {
		return $this->actualPage;
	}
	
	public function getNumberOfPages() {
		return $this->numberOfPages;
	}
}

?>