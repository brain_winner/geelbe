<?

class ListadoUtils {
	
	##
	# Frontend utils
	##
	
	public static function getValueFromRequest($key, $array = null) {
		if(is_null($array))
			return (isset($_REQUEST[$key]) ? $_REQUEST[$key] : null);
		else
			return (isset($_REQUEST[$key]) && isset($_REQUEST[$key][$array]) ? $_REQUEST[$key][$array] : null);
	}

	
	public static function generateFiltersParamArray($paramNames) {
		$filtersArray = array();
		foreach($paramNames as $param) {
			$filtersArray[$param] = ListadoUtils::getValueFromRequest('filter', $param);
		}
		
		return $filtersArray;
	}
	
	public static function generateSortParamArray($defaultSort, $defaultSortDirection = "ASC") {
		return array('sortBy' => $_REQUEST['sortBy'], 'sortDirection' => $_REQUEST['sortDirection'],
					'defaultSort' => $defaultSort, 'defaultSortDirection' => $defaultSortDirection);
	}
	
	public static function parseColumnLink($link, $data) {
		$token = strtok($link, "[[");
		$parameters = array();
		
		while ($token !== false) {
		    $parameters[] = substr($token, 0, stripos($token, "]]"));
		    $token = strtok("[[");
		}
		
		$newLink = $link;
		foreach($parameters as $param) {
			$newLink = str_replace("[[".$param."]]", $data[$param], $newLink);
		}
		
		return $newLink;
	}
	
	##
	# Backend utils
	##
	
	public static function generateFiltersSQL(array $columns) {
		if(isset($columns) && sizeof($columns) > 0) {
			$filters = array();
		  	foreach($columns as $key => $column) {
				$filters = ListadoUtils::addFilter($key, $column, $filters);
		  	}
		  	if(sizeof($filters) > 0) {
			  	return implode(' ', $filters);
		  	}
		} 
		return '1=1';
	}
	
	public static function generateSortSQL($sortArray) {
		return ((isset($sortArray['sortBy']) && $sortArray['sortBy']!='')?$sortArray['sortBy']:$sortArray['defaultSort'])." ".
			((isset($sortArray['sortDirection']) && $sortArray['sortDirection']!='')?$sortArray['sortDirection']:$sortArray['defaultSortDirection']);
	}
	
	public static function addFilter($key, $value, $filters) {
		
		// La conexion la tiene dado que el mysql_real_escape_string la necesita;
		// Sino la misma tira un WARNING
		$oConexion = Conexion::nuevo();
		if($value != '') {
			if(sizeof($filters)==0) {
				$filters[] = $key.' LIKE "%'.mysql_real_escape_string($value).'%"';
			} else {
				$filters[] = 'AND '.$key.' LIKE "%'.mysql_real_escape_string($value).'%"';
			}
		}
		$oConexion->Cerrar_Trans();
		return $filters;
	}
	
}

?>