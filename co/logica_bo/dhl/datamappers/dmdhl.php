<?
    class dmDHL
    {
        public static function getZonasByIdProvincia($IdProvincia)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT P.IdProvincia, P.IdPais, P.Descripcion FROM ciudades P INNER JOIN shipping_zonas_x_ciudades ZP ON ZP.IdCiudad = P.IdCiudad WHERE P.IdProvincia = ".mysql_real_escape_string($IdProvincia));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getTarifaByPesoZona($Peso, $IdZona = 0)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM (SELECT P.IdPeso, P.Tarifa, P.TarifaKG, P.IdZona, Pe.Desde, Pe.Hasta FROM shipping_precios P INNER JOIN shipping_pesos Pe ON Pe.IdPeso = P.IdPeso WHERE Pe.IdPeso = 0 UNION SELECT P.IdPeso, P.Tarifa, P.TarifaKG, P.IdZona FROM shipping_precios P INNER JOIN shipping_pesos Pe ON Pe.IdPeso = P.IdPeso WHERE ".$Peso." >= Pe.Desde AND ".$Peso." <= Pe.Hasta AND NOT Pe.IdPeso = 0 )A ".(($IdZona !=0)?"WHERE IdZona = ".mysql_real_escape_string($IdZona):"")." ORDER BY IdPeso");
                $Tabla = $oConexion->DevolverQuery();
                $objDHL = new DHL($Tabla, $Peso);
                $oConexion->Cerrar_Trans();
                return $objDHL;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
            
        public static function getTarifaByPesoPcia($Peso, $IdProvincia = 0, $campania = null) {
            $oConexion = Conexion::nuevo();
            $oConexion->setQuery("select P.Tarifa from shipping_precios P 
									inner join shipping_pesos Pe on Pe.IdPeso = P.IdPeso 
									inner join shipping_zonas_x_ciudades Z on P.IdZona = Z.IdZona 
									where Pe.Hasta > ".mysql_real_escape_string($Peso)." and Z.IdCiudad = ".mysql_real_escape_string($IdProvincia)." 
									order by Pe.Hasta 
									limit 1");
            $res = $oConexion->DevolverQuery();
            
            $tarifa = 0;
            if($res[0] != null && $res[0]["Tarifa"] != null) {
            	$tarifa = $res[0]["Tarifa"];
            } else {
                $oConexion->setQuery("select P.Tarifa, P.TarifaKG, Pe.Hasta from shipping_precios P 
										inner join shipping_pesos Pe on Pe.IdPeso = P.IdPeso 
										inner join shipping_zonas_x_ciudades Z on P.IdZona = Z.IdZona 
										where Z.IdCiudad = ".mysql_real_escape_string($IdProvincia)." and P.Tarifa = (
										select MAX(PP.Tarifa) from shipping_precios PP where PP.IdZona = P.IdZona)");
            	$res = $oConexion->DevolverQuery();
            	$tarifa = $res[0]["Tarifa"] + ($res[0]["TarifaKG"] * ($Peso - $res[0]["Hasta"]));
            }
            
            return new DHL($tarifa);
        }

        public static function getTarifaByPeso($Peso)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT P.IdPeso, P.Tarifa, P.TarifaKG, P.IdZona FROM shipping_precios P INNER JOIN shipping_pesos Pe ON Pe.IdPeso = P.IdPeso WHERE ".$Peso." >= Pe.Desde AND ".$Peso." <= Pe.Hasta AND NOT Pe.IdPeso = 0 ORDER BY Pe.IdPeso");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        

        public static function getAllDHL($columna, $sentido, $filtro="")  
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT shipping_precios.IdZona, shipping_precios.IdPeso, shipping_zonas_x_ciudades.IdCiudad, ciudades.Descripcion, shipping_pesos.Desde, shipping_pesos.Hasta, shipping_precios.Tarifa, shipping_precios.TarifaKG FROM shipping_pesos Inner Join shipping_precios ON shipping_precios.IdPeso = shipping_pesos.IdPeso Inner Join shipping_zonas ON shipping_precios.IdZona = shipping_zonas.IdZona AND '' = '' Inner Join shipping_zonas_x_ciudades ON shipping_zonas.IdZona = shipping_zonas_x_ciudades.IdZona Inner Join ciudades ON shipping_zonas_x_ciudades.IdCiudad = ciudades.IdCiudad WHERE shipping_zonas.Nombre LIKE \"%".$filtro."%\" OR ciudades.Descripcion LIKE \"%".$filtro."%\" ORDER BY " . $columna . " " . $sentido ." Limit 400");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(MySQLException $e)  
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function getAllDHLCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(*) as total
                                      FROM shipping_pesos dp
                                      INNER JOIN shipping_precios dpr ON dpr.IdPeso = dp.IdPeso 
                                      INNER JOIN shipping_zonas dz ON dpr.IdZona = dz.IdZona 
                                      INNER JOIN shipping_zonas_x_ciudades dzp ON dz.IdZona = dzp.IdZona 
                                      INNER JOIN ciudades p ON dzp.IdCiudad = p.IdCiudad 
                                      WHERE ($filtersSQL) ");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch(MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }  
        }
        
    
        public static function getAllDHLPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT dpr.Tarifa as `dpr.Tarifa`, 
                                             dpr.TarifaKG as `dpr.TarifaKG`, 
                                             dz.Nombre as `dz.Nombre`, 
                                             dp.Desde as `dp.Desde`, 
                                             dp.Hasta as `dp.Hasta`, 
                                             p.Descripcion as `p.Descripcion`, 
                                             dpr.IdPeso as `dpr.IdPeso`, 
                                             dpr.IdZona as `dpr.IdZona`,
                                             p.IdCiudad as `p.IdCiudad`
                                      FROM shipping_pesos dp
                                      INNER JOIN shipping_precios dpr ON dpr.IdPeso = dp.IdPeso 
                                      INNER JOIN shipping_zonas dz ON dpr.IdZona = dz.IdZona 
                                      INNER JOIN shipping_zonas_x_ciudades dzp ON dz.IdZona = dzp.IdZona 
                                      INNER JOIN ciudades p ON dzp.IdCiudad = p.IdCiudad
                                      WHERE ($filtersSQL)  
									  ORDER BY $sortSQL 
									  LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
									  
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();           
                return $Tabla;                
            }
            catch(MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }  
        }
        
        public static function getByPesoZona($peso, $zona, $provincia)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT shipping_precios.Tarifa, shipping_precios.TarifaKG, shipping_zonas.Nombre as zona, shipping_pesos.Desde, shipping_pesos.Hasta, ciudades.Descripcion as ciudad, shipping_precios.IdPeso as idpeso, shipping_precios.IdZona as idzona FROM shipping_pesos Inner Join shipping_precios ON shipping_precios.IdPeso = shipping_pesos.IdPeso Inner Join shipping_zonas ON shipping_precios.IdZona = shipping_zonas.IdZona AND '' = '' Inner Join shipping_zonas_x_ciudades ON shipping_zonas.IdZona = shipping_zonas_x_ciudades.IdZona Inner Join ciudades ON shipping_zonas_x_ciudades.IdCiudad = ciudades.IdCiudad WHERE shipping_precios.IdPeso = '".$peso . "' AND shipping_precios.IdZona = '".$zona."' AND shipping_zonas_x_ciudades.IdCiudad = '".$provincia."'");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(MySQLException $e)  
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function guardar($tarifa, $tarifaKG, $idpeso, $idzona)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE shipping_precios SET Tarifa = '".$tarifa."', TarifaKG = '". $tarifaKG."' WHERE IdPeso = " . $idpeso . " AND IdZona = " . $idzona);
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }            
        }

    }
?>