<?
	require_once dirname(__FILE__).'/dmcategoriasglobales.php';
    class dmCategoria
    {
        public static function save(MySQL_Categoria $objCategoria)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objCategoria->getInsert());
                $oConexion->EjecutarQuery();
                if($objCategoria->getIdCategoria() == 0)
                {
                    $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                    $ID = $oConexion->DevolverQuery();
                    $objCategoria->setIdCategoria($ID[0]["Id"]);
                }
                $oConexion->setQuery("DELETE FROM productos_x_categorias WHERE IdCategoria = ".$objCategoria->getIdCategoria());
                $oConexion->EjecutarQuery();
                foreach($objCategoria->getProductos() as $producto)
                {
                    $oConexion->setQuery("INSERT INTO productos_x_categorias (idproducto, idcategoria) VALUES (".$producto->getIdProducto().", ".$objCategoria->getIdCategoria().")");
                    $oConexion->EjecutarQuery();
                }
                $oConexion->Cerrar_Trans();
            }
            catch (Exception $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    
        public static function saveNew(MySQL_Categoria $objCategoria) {
            $oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objCategoria->getInsert());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (Exception $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
		public static function getLastInsertId() {
            $oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("select LAST_INSERT_ID() as total");
				$Tabla = $oConexion->DevolverQuery();
                $objProductos = new MySQL_Productos();
				
				$totalProductos = $Tabla[0]['total'];
				
				return $totalProductos;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function delete(MySQL_Categoria $objCategoria)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->setQuery("delete from productos_x_categorias where IdCategoria=".$objCategoria->getIdCategoria());
                $oConexion->EjecutarQuery();

                $oConexion->setQuery($objCategoria->getDelete());
                $oConexion->EjecutarQuery();
            }
            catch (Exception $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getByIdCategoria($Id)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $objCategoria = new MySQL_Categoria();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objCategoria->getById($Id));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                foreach($Tabla as $fila)
                {
                    foreach ($fila as $atributo => $valor)
                    {
                        $objCategoria->__set($valor, $atributo);
                    }                    
                    $objCategoria->setDeep(dmCategoria::getDeepByIdCategoria($objCategoria->getIdCategoria()));
                }
                return $objCategoria;
            }
            catch (Exception $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function getPadreByIdCampania($IdCampania) {
            $oConexion = Conexion::nuevo();
            try
            {  
                $objCategoria = new MySQL_Categoria();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM categorias WHERE IdPadre is null AND IdCampania = ".$IdCampania);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                foreach($Tabla as $fila)
                {
                    foreach ($fila as $atributo => $valor)
                    {
                        $objCategoria->__set($valor, $atributo);
                    }                    
                    $objCategoria->setDeep(dmCategoria::getDeepByIdCategoria($objCategoria->getIdCategoria()));
                }
                return $objCategoria;
            }
            catch (Exception $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        private static function getDeepByIdCategoria($IdCategoria)
        {
            try
            {
                $Prof=0;
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT IdPadre FROM categorias WHERE IdCategoria =".mysql_real_escape_string($IdCategoria));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                if(isset($Tabla[0]["IdPadre"]))
                {
                    $Prof++;
                    $Prof += dmCategoria::getDeepByIdCategoria($Tabla[0]["IdPadre"]);
                }
                return $Prof;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        
        public static function getCategorias($IdCampania, $IdPadre) {   
            $oConexion = Conexion::nuevo();
            $oConexion->Abrir_Trans();
            
            $oConexion->setQuery("SELECT IdCategoria, Nombre, Descripcion, IdCampania, IdPadre, Orden, Habilitado, IdOrdenamiento FROM categorias C WHERE C.IdCampania = ".mysql_real_escape_string($IdCampania)." AND C.IdPadre ".mysql_real_escape_string($IdPadre)." ORDER BY IdCategoria");
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            
            $i=0;
            $dtCategorias = array();
            foreach($Tabla as $fila) {
                $dtCategorias[$i] = $fila;
                $dtCategorias[$i]["Deep"] = dmCategoria::getDeepByIdCategoria($dtCategorias[$i]["IdCategoria"]);
                $dtCategorias[$i]["subCategoria"] = dmCategoria::getCategorias($fila["IdCampania"], " = ".$fila["IdCategoria"]);
                $i++;
            }                
            return $dtCategorias;
        } 
        
        public static function getCategoriasPadreQueryNew($IdCampania, $IdPadre) {   
            $oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                
                if ($IdPadre == null) {
	                $oConexion->setQuery("SELECT C.IdCategoria, C.Nombre, C.IdCampania, C.IdPadre  
	                                      FROM categorias C 
	                                      WHERE C.IdCampania = ".mysql_real_escape_string($IdCampania)."
	                                      AND C.IdPadre is null  
	                                      ORDER BY C.IdCategoria");
                }
                else {
	                $oConexion->setQuery("SELECT C.IdCategoria, C.Nombre, C.IdCampania, C.IdPadre  
	                                      FROM categorias C 
	                                      WHERE C.IdCampania = ".mysql_real_escape_string($IdCampania)."
	                                      AND C.IdPadre = ".mysql_real_escape_string($IdPadre)." 
	                                      ORDER BY C.IdCategoria");
                }
           
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();              
                return $Tabla;
            }
            catch (Exception $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
        }
        
    public static function getCategoriasPadreNew($IdCampania, $IdPadre) {   
            $ReturnValue = array();
        	$Tabla = dmCategoria::getCategoriasPadreQueryNew($IdCampania, $IdPadre);
    		if ($Tabla == null) {
            	return $ReturnValue;
            }
            foreach ($Tabla as $fila) {
            	$ReturnValue[] = $fila;
            	$SQ = dmCategoria::getCategoriasPadreNew($IdCampania, $fila["IdCategoria"]);
            	if ($SQ == null) {
            		continue;
            	}
            	foreach ($SQ as $sqFila) {
            		$ReturnValue[] = $sqFila;
            	}
			}
			return $ReturnValue;
        }
    
        public static function getCategoriasNew($IdCampania) {   
            $Tabla = dmCategoria::getCategoriasPadreNew($IdCampania, null);
            return $Tabla;
        }
        
        public static function getCategoriasProductos($IdProducto) { 
            $oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT C.IdCategoria, C.IdProducto  
	                                  FROM productos_x_categorias C 
	                                  WHERE C.IdProducto = ".mysql_real_escape_string($IdProducto));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();              
                return $Tabla;
            }
            catch (Exception $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function getCategoriasFullProductos($IdProducto) { 
            $oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT C.IdCategoria, CC.Nombre
	                                  FROM productos_x_categorias C 
	                                  JOIN categorias CC ON CC.IdCategoria = C.IdCategoria
	                                  WHERE C.IdProducto = ".mysql_real_escape_string($IdProducto));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();              
                return $Tabla;
            }
            catch (Exception $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function getEstasEn($IdCategoria=null, $IdCampania=null)
        {
            try
            {
                $EstasEn = "";
                if($IdCampania)
                {
                    $objCampania = dmCampania::getByIdCampania($IdCampania);
                    $HREF = Aplicacion::getRootUrl()."front/catalogo/index.php";
                    $EstasEn = "<a href='".$HREF."?IdCampania=".$objCampania->getIdCampania()."'>".$objCampania->getNombre()."</a>";
                }
                if($IdCategoria)
                {
                    $arrNombres = array();
                    $oCategoria = dmCategoria::getByIdCategoria($IdCategoria);
                    while($oCategoria->getIdPadre())
                    {
                        array_push($arrNombres, $oCategoria);
                        $oCategoria = dmCategoria::getByIdCategoria($oCategoria->getIdPadre());
                    }
                    $EstasEn = $EstasEn . dmCategoria::ArmarStringCategorias($arrNombres, $HREF);
                }
                return $EstasEn;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public static function getEstasEnNew($IdCategoria=null, $IdCampania=null)
        {
            try
            {
                $EstasEn = "";
                if($IdCampania)
                {
                    $objCampania = dmCampania::getByIdCampania($IdCampania);
                    $HREF = Aplicacion::getRootUrl()."front/catalogo/index.php";
                    $EstasEn = "<a href='".$HREF."?IdCampania=".$objCampania->getIdCampania()."'>".$objCampania->getNombre()."</a>";
                }
                if($IdCategoria)
                {
                    $arrNombres = array();
                    $oCategoria = dmCategoria::getByIdCategoria($IdCategoria);
                    while($oCategoria->getIdPadre())
                    {
                        array_push($arrNombres, $oCategoria);
                        $oCategoria = dmCategoria::getByIdCategoria($oCategoria->getIdPadre());
                    }
                    $EstasEn = $EstasEn . dmCategoria::ArmarStringCategoriasNew($arrNombres, $HREF);
                }
                return $EstasEn;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private static function ArmarStringCategorias(array $arrNombre, $HREF)
        {
                try
                {
                    $str = "";
                    $Array = array();
                     $Array = array_reverse($arrNombre);
                     foreach($Array as $element)
                     {
                         $str = $str . " | " . "<a href='".$HREF."?IdCampania=".$element->getIdCampania()."&IdCategoria=".$element->getIdCategoria()."'>".$element->getNombre()."</a>";
                     }
                     return $str;
                }
                catch(exception $e)
                {
                    throw $e;
                }
        }

        private static function ArmarStringCategoriasNew(array $arrNombre, $HREF) {
        	$flechaImg = UrlResolver::getImgBaseUrl("front/catalogo/images/flecha.jpg");
        	$str = "";
			$Array = array();
            $Array = array_reverse($arrNombre);
            foreach($Array as $element) {
				$str = $str . " <img src='".$flechaImg."' /> " . "<a href='".$HREF."?IdCampania=".$element->getIdCampania()."&IdCategoria=".$element->getIdCategoria()."'>".$element->getNombre()."</a>";
            }
            return $str;
        }
        public static function getProductosByIdCategoria($IdCategoria)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT P.IdProducto AS Codigo, CONCAT(P.Nombre, '(Stock:', IF(SUM(Stock) IS NULL,0,SUM(Stock)), ' Regalo:', IF(SUM(Regalo) IS NULL,0,SUM(Regalo)), ')') AS Producto FROM productos P INNER JOIN productos_x_proveedores PP ON PP.IdProducto = P.IdProducto INNER JOIN productos_x_categorias PC ON PC.IdProducto = P.IdProducto WHERE 1=1 AND PC.IdCategoria = ".mysql_real_escape_string($IdCategoria)." GROUP BY P.IdProducto , P.Nombre ORDER BY P.Nombre");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (Exception $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>