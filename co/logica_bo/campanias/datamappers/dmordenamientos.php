<?php
    class dmOrdenamientos
    {
        public static function getById($idOrdenamiento=1)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM ordenamientos WHERE IdOrdenamiento = " . $idOrdenamiento);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                $oOrdenamiento = new clsOrdenamientos();
                $oOrdenamiento->setIdOrdenamiento($Tabla[0]["idOrdenamiento"]);
                $oOrdenamiento->setCampo($Tabla[0]["Campo"]);
                $oOrdenamiento->setDescripcion($Tabla[0]["Descripcion"]);
                $oOrdenamiento->setOrdenamiento($Tabla[0]["Ordenamiento"]);
                return $oOrdenamiento;
            }
            catch (Exception $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getAll()
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM ordenamientos");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
               return $Tabla;
            }
            catch (Exception $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
            
        }
    }
?>
