<?
    class dmCategoriaGlobal
    {
        public static function getCategoriasProductos($IdProducto) {   
            $oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT C.IdCategoriaGlobal, C.IdProducto  
	                                  FROM productos_x_categorias_globales C 
	                                  WHERE C.IdProducto = ".mysql_real_escape_string($IdProducto));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();              
                return $Tabla;
            }
            catch (Exception $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function getCategoriasCampanias($IdProducto) {   
            $oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT C.IdCategoriaGlobal, C.IdCampania  
	                                  FROM campanias_x_categorias_globales C 
	                                  WHERE C.IdCampania = ".mysql_real_escape_string($IdProducto));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();              
                return $Tabla;
            }
            catch (Exception $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function getCategorias($padre = null, $nivel = 0, $recursive = true) { 
            $oConexion = Conexion::nuevo();
            try {  
            
            	$padre = ($padre == null ? 'IS NULL' : "= '".$padre."'");
            
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT *  
	                                  FROM categorias_globales
	                                  WHERE IdCategoriaPadre ".$padre);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();              
                
                $cats = array();
                foreach($Tabla as $i => $cat) {
	                
	                $cat['Nombre'] = str_repeat('&nbsp;', $nivel*3).$cat['Nombre'];
	                
	                $cats[] = $cat;
	                
	                if($recursive)
	                	$cats = array_merge($cats, dmCategoriaGlobal::getCategorias($cat['IdCategoriaGlobal'], $nivel+1));
	                
                }
                return $cats;
            }
            catch (Exception $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function menu($padre, $recursive = true) {
        
	        $oConexion = Conexion::nuevo();
            try {  
            
            	$padre = ($padre == null ? 'IS NULL' : "= '".$padre."'");
            
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT *  
	                                  FROM categorias_globales
	                                  WHERE IdCategoriaPadre ".$padre);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();              

				if(count($Tabla)) {
				                
	                $html = '<ul>';
	                foreach($Tabla as $i => $cat) {
		                
		                $html .= '<li><a href="index.php?IdCategoriaGlobal='.$cat['IdCategoriaGlobal'].'">'.$cat['Nombre'].'</a>';	                
		                if($recursive)
		                	$html .= dmCategoriaGlobal::menu($cat['IdCategoriaGlobal']);
		                $html .= '</li>';
		                
	                }
	                $html .= '</ul>';
	                
                }
                
                return $html;
            }
            catch (Exception $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
	        
        }
        
        public static function saveCampania($idc, $cats) {
	        $oConexion = Conexion::nuevo();
            try {  
                
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("DELETE FROM campanias_x_categorias_globales WHERE IdCampania = ".mysql_real_escape_string($idc));
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();              

				foreach($cats as $cat) {
				                
	                $oConexion->Abrir_Trans();
	                $oConexion->setQuery("INSERT INTO campanias_x_categorias_globales VALUES (".mysql_real_escape_string($idc).", '".mysql_real_escape_string($cat)."')");
	                $oConexion->EjecutarQuery();
	                $oConexion->Cerrar_Trans();              
                
                }
            }
            catch (Exception $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
            
        }
        
        public static function getChildrenIds($parent) {
	        
	        $ids = array();
	        $oConexion = Conexion::nuevo();
            try {  
                
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdCategoriaGlobal FROM categorias_globales WHERE IdCategoriaPadre = '".mysql_real_escape_string($parent)."'");
                $Table = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();              

				foreach($Table as $cat) {
				                
	                $ids[] = $cat['IdCategoriaGlobal'];
	                $ids = array_merge($ids, dmCategoriaGlobal::getChildrenIds($cat['IdCategoriaGlobal']));
                
                }
                
                return $ids;
                
            }
            catch (Exception $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
	        
        }
        
    }
?>