<?
    class dmCampania {
    	
	public static function getDatosFacturaXLS() {
    		$oConexion = Conexion::nuevo();  		
    		try { 
    			$oConexion->Abrir();

                $oConexion->setQuery("SELECT us.IdUsuario as idUsuario, 
                                             du.Nombre as nombreUsuario, 
                                             du.Apellido as apellidoUsuario, 
                                             pd.Domicilio as calle, 
                                             pd.Numero as numeroExterior, 
                                             pd.Piso as piso, 
                                             pd.Puerta as puerta, 
                                             pd.CP as codigoPostal, 
                                             pd.Provincia as provincia, 
                                             
                                             CONCAT(pd.codTelefono,' ',pd.Telefono) AS telefono, 
                                             pd.IdPedido as idPedido, 
                                             pro.Referencia as referencia, 
                                      FROM pedidos AS p
                                      LEFT JOIN usuarios AS us ON us.IdUsuario = p.IdUsuario
                                      LEFT JOIN campanias AS cam ON cam.IdCampania = p.IdCampania
                                      LEFT JOIN productos_x_pedidos AS pxp ON p.IdPedido = pxp.IdPedido
                                      LEFT JOIN productos_x_proveedores AS pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
                                      LEFT JOIN productos_x_categorias AS pxc ON pxpr.IdProducto = pxc.IdProducto
                                      LEFT JOIN categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
                                      LEFT JOIN productos AS pro ON pro.IdProducto = pxpr.IdProducto
                                      LEFT JOIN pedidos_direcciones AS pd ON pd.IdPedido = p.IdPedido
                                      LEFT JOIN paises AS pa ON pa.IdPais = pd.IdPais
                                      LEFT JOIN provincias AS pv ON pv.IdProvincia = pd.IdProvincia
                                      LEFT JOIN estadospedidos AS ep ON ep.IdEstadoPedido = p.IdEstadoPedidos
                                      LEFT JOIN datosusuariospersonales AS du ON du.IdUsuario = us.IdUsuario
                                      WHERE p.IdEstadoPedidos IN (2,7,9) AND cam.IdCampania IN (".mysql_real_escape_string($IdCampania).")
                                      GROUP BY p.IdPedido");
				
                $DT = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                
                return $DT;
            }
    		catch (MySQLException  $e) {
    			throw $e;
    		}
    	}
    	
    	public static function getCampaniasAx() {
    		$oConexion = Conexion::nuevo();  		
    		try
    		{ 
    			$oConexion->Abrir();
    			
    			$oConexion->setQuery("SET NAMES utf8");
				$oConexion->EjecutarQuery();
				
                $oConexion->setQuery("SELECT IdCampania, Nombre, Descripcion AS Slogan, FechaInicio, FechaFin, Descuento FROM campanias 
				WHERE FechaInicio <= NOW() AND FechaFin >= NOW()");
				
                $DT = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                
                return $DT;
            }
    		catch (MySQLException  $e)
    		{
    			throw $e;
    		}
    	}
		
		public static function isCampaignCategory($IdCampania, $IdCategoria)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("select count(*) as total from categorias c where c.IdCategoria = ".mysql_real_escape_string($IdCategoria)." and c.IdCampania = ".mysql_real_escape_string($IdCampania));
				$Tabla = $oConexion->DevolverQuery();
                $objProductos = new MySQL_Productos();
				
				$totalProductos = $Tabla[0]['total'];
				
				return ($totalProductos>0);
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
                	
    	public static function getIdLandingDeCampania($idCampania) {
    		
    		$oConexion = Conexion::nuevo();
			$oConexion->Abrir_Trans();
            $oConexion->setQuery("SELECT IdLanding FROM landings_x_campanias WHERE IdCampania=".mysql_real_escape_string($idCampania));
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            if ($Tabla) {
				return $Tabla[0]["IdLanding"];   		
            } else {
            	return "";
            }
    	}
    	
       	public static function getTextoLandingDeCampania($idLanding) {
    		
    		$oConexion = Conexion::nuevo();
			$oConexion->Abrir_Trans();
            $oConexion->setQuery("SELECT texto FROM landings WHERE id=".mysql_real_escape_string($idLanding));
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            if ($Tabla) {
				return $Tabla[0]["texto"];   		
            } else {
            	return "";
            }
    	}
    		   	
        public static function save(MySQL_Campania $objCampania) {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objCampania->getInsert());
                $oConexion->EjecutarQuery();
                if($objCampania->getIdCampania() == 0) {
                    $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                    $ID = $oConexion->DevolverQuery();
                    $objCampania->setIdCampania($ID[0]["Id"]);
                    $objCategorias = new MySQL_Categoria();
                    $objCategorias->setIdCampania($objCampania->getIdCampania());
                    $objCategorias->setIdPadre(0);
                    $objCategorias->setNombre($objCampania->getNombre());
                    $oConexion->setQuery($objCategorias->getInsert());
                    $oConexion->EjecutarQuery();
                }
                $oConexion->setQuery("DELETE FROM campanias_x_propiedades WHERE IdCampania = ".mysql_real_escape_string($objCampania->getIdCampania()));
                $oConexion->EjecutarQuery();
                foreach($objCampania->getPropiedades() as $propiedad) {
                	$oConexion->setQuery("INSERT INTO  campanias_x_propiedades(IdPropiedadCampamia, IdCampania) VALUES(".mysql_real_escape_string($propiedad).", ".mysql_real_escape_string($objCampania->getIdCampania()).")");
                    $oConexion->EjecutarQuery();
                }
                $objCampania->CrearDirectorio();
                $objCampania->Upload();
                $oConexion->Cerrar_Trans();
                return $objCampania->getIdCampania();
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function delete(MySQL_Campania $objCampania)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objCampania->getDelete());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getByIdCampania($Id)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $objCampania = new MySQL_Campania();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objCampania->getById($Id));
                $Tabla = $oConexion->DevolverQuery();
                foreach($Tabla as $fila) {
                    $objCampania->__setByArray($fila);
                }
                
                $oConexion->setQuery("SELECT IdPropiedadCampamia FROM campanias_x_propiedades WHERE IdCampania = ".mysql_real_escape_string($objCampania->getIdCampania()));
                $Tabla = $oConexion->DevolverQuery();
                foreach($Tabla as $fila) {
                	$objCampania->addPropiedad($fila);
                }
                
                
                $oConexion->Cerrar_Trans();
                return $objCampania;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }

		public static function saveVisit($Id)
        {
        	if(!is_numeric($Id))
        		return false;
        
            $oConexion = Conexion::nuevo();
            try
            {  
                
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE campanias SET Visitas = Visitas + 1 WHERE IdCampania = ".mysql_real_escape_string($Id));
                $Tabla = $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }

        public static function getCampanias($columnaNro, $columnaSentido, $filter = "")
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdCampania, Nombre, DATE_FORMAT( FechaInicio, '%d/%m/%Y %H:%i' ) AS 'Fecha de inicio', DATE_FORMAT( FechaFin, '%d/%m/%Y %H:%i' ) AS 'Fecha de fin', DATE_FORMAT( FechaFin, '%m/%d/%Y %H:%i' ) AS 'FechaMDY', DATE_FORMAT( VisibleDesde, '%d/%m/%Y %H:%i' ) AS 'Visible desde', IF( UNIX_TIMESTAMP( VisibleDesde ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Si', 'No' ) AS 'Visible', IF( UNIX_TIMESTAMP( FechaFin ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Cerrada', IF( UNIX_TIMESTAMP( FechaInicio ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Abierta', 'Proxima' )) AS Estado FROM campanias WHERE Nombre LIKE \"%".mysql_real_escape_string($filter)."%\" OR FechaInicio LIKE \"%".mysql_real_escape_string($filter)."%\" OR FechaFin LIKE \"%".mysql_real_escape_string($filter)."%\" OR VisibleDesde LIKE \"%".mysql_real_escape_string($filter)."%\" ORDER BY ".mysql_real_escape_string($columnaNro)." ".mysql_real_escape_string($columnaSentido));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
		public static function getCampaniasCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
				
		    $oConexion = Conexion::nuevo();
		    try
		    {
		        $oConexion->Abrir();
		        $oConexion->setQuery("SELECT count(1) as total  
		        					  FROM campanias C
									  WHERE ($filtersSQL)");
		        $Tabla = $oConexion->DevolverQuery();
		        //ColumnaMoneda(&$Tabla, "Monto total");
		        $oConexion->Cerrar_Trans();
		        return $Tabla[0]['total'];
		    }
		    catch(MySQLException $e)
		    {
		        $oConexion->RollBack();
		        throw $e;
		    }
		}
	    public static function getCampaniasPaginadas($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
				$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
				$sortSQL = ListadoUtils::generateSortSQL($sortArray);
					
				$oConexion = Conexion::nuevo();
				try
				{
					$oConexion->Abrir();
					$oConexion->setQuery("SELECT IdCampania as 'C.IdCampania', 
												 Nombre as 'C.Nombre', 
												 DATE_FORMAT( FechaInicio, '%d/%m/%Y %H:%i' ) AS 'DATE_FORMAT(FechaInicio,\'%d/%m/%Y %H:%i\')', 
												 DATE_FORMAT( FechaFin, '%d/%m/%Y %H:%i' ) AS 'DATE_FORMAT(FechaFin,\'%d/%m/%Y %H:%i\')',
												 DATE_FORMAT( FechaFin, '%m/%d/%Y %H:%i' ) AS 'DATE_FORMAT(FechaFin,\'%m/%d/%Y %H:%i\')', 
												 DATE_FORMAT( VisibleDesde, '%d/%m/%Y %H:%i' ) AS 'DATE_FORMAT(VisibleDesde,\'%d/%m/%Y %H:%i\')', 
												 DATE_FORMAT( FechaCorte, '%d/%m/%Y' ) AS 'DATE_FORMAT(FechaCorte,\'%d/%m/%Y\')', 
												 IF( UNIX_TIMESTAMP( C.VisibleDesde ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Si', 'No' ) AS 'IF(UNIX_TIMESTAMP(C.VisibleDesde)<=UNIX_TIMESTAMP(NOW()),\'Si\',\'No\')',
												 IF( UNIX_TIMESTAMP( C.FechaFin ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Cerrada', 
												 	IF( UNIX_TIMESTAMP( C.FechaInicio ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Abierta', 'Proxima' )) AS 'IF(UNIX_TIMESTAMP(C.FechaFin)<=UNIX_TIMESTAMP(NOW()),\'Cerrada\',IF(UNIX_TIMESTAMP(C.FechaInicio)<=UNIX_TIMESTAMP(NOW()),\'Abierta\',\'Proxima\'))' 
											FROM campanias C
											WHERE ($filtersSQL) 
											ORDER BY $sortSQL 
											LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
				                	
					$Tabla = $oConexion->DevolverQuery();
					ColumnaMoneda(&$Tabla, "Monto total");
					$oConexion->Cerrar_Trans();
					return $Tabla;
				}
				catch(MySQLException $e)
				{
					$oConexion->RollBack();
					throw $e;
				}
			}
        public static function getPropiedades($IdCampania)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT 	CP.IdPropiedadCampamia, 
                								CP.Nombre, 
                								IF((	SELECT IdPropiedadCampamia 
                										FROM  campanias_x_propiedades CxP 
                										WHERE CxP.IdPropiedadCampamia = CP.IdPropiedadCampamia 
                										AND IdCampania = ".mysql_real_escape_string($IdCampania).") IS NULL, 0, 1) as Esta 
                								FROM campanias_propiedades CP");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function getPropiedadesByIdCampania($IdCampania) {
            $oConexion = Conexion::nuevo();
            try
            {  
                $objCampania = new MySQL_Campania();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT CP.IdPropiedadCampamia, 
                                             P.Nombre 
                                      FROM campanias_x_propiedades CP 
                                      INNER JOIN campanias_propiedades P ON P.IdPropiedadCampamia = CP.IdPropiedadCampamia 
                                      WHERE CP.IdCampania = ".mysql_real_escape_string($IdCampania));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }

        public static function getMinMontoEnvioGratis($IdCampania) {
            $oConexion = Conexion::nuevo();
            try
            {  
                $objCampania = new MySQL_Campania();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT MinMontoEnvioGratis
                                      FROM campanias 
                                      WHERE IdCampania = ".mysql_real_escape_string($IdCampania));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]["MinMontoEnvioGratis"];
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
		public static function getPropiedadCampaniaByIdPropiedad($IdPropiedadCampania)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $objCampania = new MySQL_Campania();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM campanias_propiedades WHERE IdPropiedadCampamia = ".mysql_real_escape_string($IdPropiedadCampania));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0];
            }
            catch(exception $e)
            {
                throw $e;
            }
        }

        public static function getCampaniasWebservice($catsGlobales = null) {
            $oConexion = Conexion::nuevo();
            try { 
            
            	if($catsGlobales == null || count($catsGlobales) == 0)
            		$cats = '';
            	else {
            		//escape
            		foreach($catsGlobales as $i => $cat)
            			$catsGlobales[$i] = "'".mysql_real_escape_string($cat)."'";
            		
            		$cats = ' AND IdCampania IN (SELECT IdCampania FROM campanias_x_categorias_globales WHERE IdCategoriaGlobal IN ('.implode(',', $catsGlobales).'))';
            	}
            	
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * 
                                      FROM ( SELECT 
                                      		UNIX_TIMESTAMP(FechaInicio) as ff,
											IdCampania,
											Nombre,
											Descripcion,
											FechaInicio,
											FechaFin,
											VisibleDesde,
											TiempoEntregaIn,
											TiempoEntregaFn,
											TiempoEntregaIn2,
											TiempoEntregaFn2,
											FechaCorte,
											MinMontoEnvioGratis,
											Welcome,
											Coleccion
                                            FROM campanias 
                                            WHERE 
                                            	UNIX_TIMESTAMP( FechaFin ) > UNIX_TIMESTAMP( NOW( ) )
                                            	AND UNIX_TIMESTAMP( VisibleDesde ) <= UNIX_TIMESTAMP( NOW( ) )
                                            	".$cats."                                             	
                                          ) campanias 
                                      WHERE  1=1 ".$coleccion."
                                      ORDER BY ff DESC");
                $Tabla = $oConexion->DevolverQuery();
                
	            return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }

        
        public static function getCampaniasOrdenadas($present = true, $future = true, $coleccion = null, $catsGlobales = null) {
            $oConexion = Conexion::nuevo();
            try { 
            
            	if(is_numeric($coleccion))
            		$coleccion = ' AND Coleccion = '.$coleccion;
            	else
            		$coleccion = '';
            		
            	if($catsGlobales == null || count($catsGlobales) == 0)
            		$cats = '';
            	else {
            		//escape
            		foreach($catsGlobales as $i => $cat)
            			$catsGlobales[$i] = "'".mysql_real_escape_string($cat)."'";
            		
            		$cats = ' AND IdCampania IN (SELECT IdCampania FROM campanias_x_categorias_globales WHERE IdCategoriaGlobal IN ('.implode(',', $catsGlobales).'))';
            	}
            	
            	$Tabla1 = array();
            	$Tabla2 = array();
            	if($present) {
	                $oConexion->Abrir_Trans();
	                $oConexion->setQuery("SELECT * 
	                                      FROM ( SELECT UNIX_TIMESTAMP(FechaInicio) as ff,
	                                                    IdCampania, 
	                                                    Nombre, 
	                                                    Descripcion,
	                                                    Coleccion,
	                                                    DATE_FORMAT( FechaInicio, '%d/%m/%Y %H:%i' ) AS 'Fecha de inicio', 
	                                                    DATE_FORMAT( FechaFin, '%d/%m/%Y %H:%i' ) AS 'Fecha de fin', 
	                                                    DATE_FORMAT( FechaFin, '%m/%d/%Y %H:%i' ) AS 'FechaMDY', 
	                                                    DATE_FORMAT( VisibleDesde, '%d/%m/%Y %H:%i' ) AS 'Visible desde', 
	                                                    IF( UNIX_TIMESTAMP( VisibleDesde ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Si', 'No' ) AS 'Visible', 
	                                                    IF( UNIX_TIMESTAMP( FechaFin ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Cerrada', IF( UNIX_TIMESTAMP( FechaInicio ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Abierta', 'Proxima' ) ) AS Estado,  
	                                                    FechaInicio AS nuevo,
	                                                    welcome,
	                                                    idOrdenamiento,
	                                                    MinMontoEnvioGratis,
	                                                    DescuentoMax
	                                             FROM campanias 
	                                             WHERE UNIX_TIMESTAMP( FechaInicio ) <= UNIX_TIMESTAMP( NOW( ) ) ".$cats." ) A 
	                                      WHERE  Estado = 'Abierta' ".$coleccion."
	                                      ORDER BY ff DESC");
	                $Tabla1 = $oConexion->DevolverQuery();
	            }
	            
	            if($future) {
	                $oConexion->setQuery("SELECT * 
	                                      FROM ( SELECT UNIX_TIMESTAMP(FechaFin) as ff, 
	                                                    IdCampania, 
	                                                    Nombre, 
	                                                    Descripcion, 
	                                                    Coleccion,
	                                                    DATE_FORMAT( FechaInicio, '%d/%m/%Y %H:%i' ) AS 'Fecha de inicio', 
	                                                    DATE_FORMAT( FechaFin, '%d/%m/%Y %H:%i' ) AS 'Fecha de fin', 
	                                                    DATE_FORMAT( FechaFin, '%m/%d/%Y %H:%i' ) AS 'FechaDMY', 
	                                                    DATE_FORMAT( VisibleDesde, '%d/%m/%Y %H:%i' ) AS 'Visible desde', 
	                                                    IF( UNIX_TIMESTAMP( VisibleDesde ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Si', 'No' ) AS 'Visible', 
	                                                    IF( UNIX_TIMESTAMP( FechaFin ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Cerrada', IF( UNIX_TIMESTAMP( FechaInicio ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Abierta', 'Proxima' ) ) AS Estado,  
	                                                    FechaInicio AS nuevo, UNIX_TIMESTAMP( FechaInicio) AS FeIni  ,
	                                                    DescuentoMax
	                                             FROM campanias 
	                                             WHERE UNIX_TIMESTAMP( FechaInicio ) > UNIX_TIMESTAMP( NOW( ) ) ".$cats." ) A 
	                                      WHERE Estado = 'Proxima'  ".$coleccion."
	                                      ORDER BY FeIni ASC");
	                $Tabla2 = $oConexion->DevolverQuery();
	                $oConexion->Cerrar_Trans();
	            }
                
                $Tabla = array_merge($Tabla1, $Tabla2);
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
     	public static function getCampaniasContacto() {
            $oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdCampania, 
                                             Nombre,
                                             lower(Nombre) as Lnombre
                                      FROM campanias 
                                      WHERE UNIX_TIMESTAMP(FechaInicio) <= UNIX_TIMESTAMP(NOW()) 
                                      AND UNIX_TIMESTAMP(FechaFin) >= UNIX_TIMESTAMP(NOW() - INTERVAL 7 DAY)
                                      ORDER BY FechaInicio DESC");
                $Tabla = $oConexion->DevolverQuery();
                
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
    public static function getCampaniasUsuario($idUsuario) {
            $oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT c.IdCampania as IdCampania, 
                                             c.Nombre as Nombre,
                                             lower(c.Nombre) as Lnombre
                                      FROM campanias c, 
                                           pedidos p
                                      WHERE c.IdCampania = p.IdCampania
                                      AND p.IdUsuario = ".mysql_real_escape_string($idUsuario).
                                    " AND UNIX_TIMESTAMP(p.Fecha) >= UNIX_TIMESTAMP(NOW() - INTERVAL 4 MONTH) 
                                      GROUP BY c.Nombre ");
                $Tabla = $oConexion->DevolverQuery();
                
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
    
     	public static function getMarcaById($Id) {
            $oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdCampania, 
                                             Nombre,
                                             lower(Nombre) as Lnombre
                                      FROM campanias 
                                      WHERE IdCampania = ".mysql_real_escape_string($Id));
                $Tabla = $oConexion->DevolverQuery();
                
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
    
     	public static function getMarcas() {
            $oConexion = Conexion::nuevo();
            try {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdCampania, 
                                             Nombre,
                                             lower(Nombre) as Lnombre
                                      FROM campanias
                                      GROUP BY Nombre");
                $Tabla = $oConexion->DevolverQuery();
                
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
		public static function EsCampaniaAccesible($idCampania)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $objCampania = new MySQL_Campania();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM(
                SELECT UNIX_TIMESTAMP(FechaFin) as ff,IdCampania, Nombre, Descripcion, DATE_FORMAT( FechaInicio, '%d/%m/%Y %H:%i' ) AS 'Fecha de inicio', DATE_FORMAT( FechaFin, '%d/%m/%Y %H:%i' ) AS 'Fecha de fin', DATE_FORMAT( FechaFin, '%m/%d/%Y %H:%i' ) AS 'FechaMDY', DATE_FORMAT( VisibleDesde, '%d/%m/%Y %H:%i' ) AS 'Visible desde', IF( UNIX_TIMESTAMP( VisibleDesde ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Si', 'No' ) AS 'Visible', IF( UNIX_TIMESTAMP( FechaFin ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Cerrada', IF( UNIX_TIMESTAMP( FechaInicio ) <= UNIX_TIMESTAMP( NOW( ) ) , 'Abierta', 'Proxima' ) ) AS Estado,  DATEDIFF(FechaInicio,CURDATE()) AS nuevo FROM campanias WHERE UNIX_TIMESTAMP( FechaInicio ) <= UNIX_TIMESTAMP( NOW( ) ) )A WHERE  Estado = 'Abierta' AND IdCampania = ". mysql_real_escape_string($idCampania) ." ORDER BY ff ASC");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                if (count($Tabla[0]) > 0)
                    return true;
                else
                    return false;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function reporte($desde, $hasta) {
        	$desde = explode('/', $desde);
        	$hasta = explode('/', $hasta);
        	
        	$desde = $desde[2].'-'.$desde[1].'-'.$desde[0];
        	$hasta = $hasta[2].'-'.$hasta[1].'-'.$hasta[0];
        
	        $oConexion = Conexion::nuevo();
            try
            {  
                $objCampania = new MySQL_Campania();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("
                	SELECT
                		c.Nombre, c.FechaInicio, c.FechaFin,
                		SUM(pr.PVenta*pp.Cantidad) Venta, SUM(p.GastosEnvio) Envio, SUM(pr.PCompra*pp.Cantidad) Costo,
                		SUM(bxp.Importe) Creditos, SUM(p.Descuento) Descuento,
                		com.Nombre Comercial
                	FROM campanias c
                	LEFT JOIN pedidos p ON p.IdCampania = c.IdCampania
                	LEFT JOIN productos_x_pedidos pp ON pp.IdPedido = p.IdPedido
                	LEFT JOIN productos_x_proveedores pxp ON pxp.IdCodigoProdInterno = pp.IdCodigoProdInterno
                	LEFT JOIN productos pr ON pr.IdProducto = pxp.IdProducto
                	LEFT JOIN bonos_x_pedidos bxp ON bxp.IdPedido = p.IdPedido
                	LEFT JOIN comerciales com ON com.IdComercial = c.IdComercial
                	WHERE '".$desde."' < c.FechaFin AND c.FechaInicio < '".$hasta."' AND p.IdEstadoPedidos IN (2,3,6,9)
                	AND p.Fecha BETWEEN '".$desde."' AND '".$hasta."'
                	GROUP BY c.IdCampania
                ");
                
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();

                header("Content-Type: application/octet-stream");
				header("Content-Transfer-Encoding: Binary");
				header("Content-disposition: attachment; filename=\"campanias.csv\"");

                echo "Nombre;FechaInicio;FechaFin;Venta;Envio;Costo;Creditos Geelbe;Descuentos aplicados;Comercial\n";
                $out = fopen('php://output', 'w');
                foreach($Tabla as $row)
                	fputcsv($out , $row, ';');
                
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
    }
?>