<?
    class MySQL_Campania extends campanias
    {
        private $_trailer_origen;
        private $_trailer_destino;
        private $_logo_origen;
        private $_logo_destino;
        private $_logo2_origen;
        private $_logo2_destino;
        private $_imagen_origen;
        private $_imagen_destino;
        private $_greyimagen_destino;
        private $_greyimagen_origen;
        private $_welcome_origen;
        private $_welcome_destino;
        private $_VidrieraImagen_origen;
        private $_VidrieraImagen_destino;
        private $_VidrieraOffImagen_origen;
        private $_VidrieraOffImagen_destino;
        private $_news1_destino;
        private $_news2_destino;
        private $_news3_destino;
        private $_news1_origen;
        private $_news2_origen;
        private $_news3_origen;
        
        public function setTrailer_Origen($trailer_origen)
        {
            $this->_trailer_origen = $trailer_origen;
        }
        public function setTrailer_Destino($trailer_destino)
        {
            $this->_trailer_destino = $trailer_destino;
        }
        public function setLogo_Origen($logo_origen)
        {
            $this->_logo_origen = $logo_origen;
        }
        public function setLogo_Destino($logo_destino)
        {
            $this->_logo_destino = $logo_destino;
        }
        public function setLogo2_Origen($logo_origen)
        {
            $this->_logo2_origen = $logo_origen;
        }
        public function setLogo2_Destino($logo_destino)
        {
            $this->_logo2_destino = $logo_destino;
        }
        public function setImagen_Origen($imagen_origen)
        {
            $this->_imagen_origen = $imagen_origen;
        }
        public function setImagen_Destino($imagen_destino)
        {
            $this->_imagen_destino = $imagen_destino;
        }
    	public function setGreyImagen_Origen($greyimagen_origen)
        {
            $this->_greyimagen_origen = $greyimagen_origen;
        }
        public function setGreyImagen_Destino($greyimagen_destino)
        {
            $this->_greyimagen_destino = $greyimagen_destino;
        }
        public function setWelcome_Origen($imagen_origen)
        {
            $this->_welcome_origen = $imagen_origen;
        }
        public function setWelcome_Destino($imagen_destino)
        {
            $this->_welcome_destino = $imagen_destino;
        }
    	public function setVidrieraImagen_Origen($imagen_origen)
        {
            $this->_VidrieraImagen_origen = $imagen_origen;
        }
        public function setVidrieraImagen_Destino($imagen_destino)
        {
            $this->_VidrieraImagen_destino = $imagen_destino;
        }
   		public function setVidrieraOffImagen_Origen($imagen_origen)
        {
            $this->_VidrieraOffImagen_origen = $imagen_origen;
        }
        public function setVidrieraOffImagen_Destino($imagen_destino)
        {
            $this->_VidrieraOffImagen_destino = $imagen_destino;
        }
        public function setNews1_Origen($imagen_origen)
        {
            $this->_news1_origen = $imagen_origen;
        }
        public function setNews1_Destino($imagen_destino)
        {
            $this->_news1_destino = $imagen_destino;
        }
        public function setNews2_Origen($imagen_origen)
        {
            $this->_news2_origen = $imagen_origen;
        }
        public function setNews2_Destino($imagen_destino)
        {
            $this->_news2_destino = $imagen_destino;
        }
        public function setNews3_Origen($imagen_origen)
        {
            $this->_news3_origen = $imagen_origen;
        }
        public function setNews3_Destino($imagen_destino)
        {
            $this->_news3_destino = $imagen_destino;
        }
        public function getTrailer_Origen()
        {
            return $this->_trailer_origen;
        }
        public function getTrailer_Destino()
        {
            return $this->_trailer_destino;
        }
        public function getLogo_Origen()
        {
            return $this->_logo_origen;
        }
        public function getLogo_Destino()
        {
            return $this->_logo_destino;
        }
        public function getLogo2_Origen()
        {
            return $this->_logo2_origen;
        }
        public function getLogo2_Destino()
        {
            return $this->_logo2_destino;
        }
        public function getImagen_Origen()
        {
            return $this->_imagen_origen;
        }
        public function getImagen_Destino()
        {
            return $this->_imagen_destino;
        }        
        public function getGreyImagen_Origen()
        {
            return $this->_greyimagen_origen;
        }
        public function getGreyImagen_Destino()
        {
            return $this->_greyimagen_destino;
        }  
        public function getWelcome_Origen()
        {
            return $this->_welcome_origen;
        }
        public function getWelcome_Destino()
        {
            return $this->_welcome_destino;
        }       
        public function getVidrieraImagen_Origen()
        {
            return $this->_VidrieraImagen_origen;
        }
        public function getVidrieraImagen_Destino()
        {
            return $this->_VidrieraImagen_destino;
        }
        public function getVidrieraOffImagen_Origen()
        {
            return $this->_VidrieraOffImagen_origen;
        }
        public function getVidrieraOffImagen_Destino()
        {
            return $this->_VidrieraOffImagen_destino;
        }      
        public function getNews1_Destino()
        {
            return $this->_news1_destino;
        } 
        public function getNews2_Destino()
        {
            return $this->_news2_destino;
        } 
        public function getNews3_Destino()
        {
            return $this->_news3_destino;
        }     
        public function getNews1_Origen()
        {
            return $this->_news1_origen;
        } 
        public function getNews2_Origen()
        {
            return $this->_news2_origen;
        } 
        public function getNews3_Origen()
        {
            return $this->_news3_origen;
        }        
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if(!is_array($valor))
                {
                    if($propiedad == "_idcampania")
                    {   
                        $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                        $strValores .=($valor != 0) ? chr(34).mysql_real_escape_string($valor).chr(34).", " : "NULL, ";
                    }
                    else
                    {   
                        $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                        $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                        $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    }
                }   
            }
            return "INSERT INTO campanias (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getDelete()
        {
            return "DELETE FROM campanias WHERE IdCampania = ".mysql_real_escape_string($this->getIdCampania());
        }        
        public function getById($Id)
        {
            $strCampos="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if(!is_array($valor))
                {
                    if($propiedad == "_fechainicio")
                    {
                        $strCampos .="DATE_FORMAT(".substr($propiedad,1,strlen($propiedad)).", '%e/%c/%Y %H:%i') AS FechaInicio, ";
                    }
                    else if($propiedad == "_fechafin")
                    {
                        $strCampos .="DATE_FORMAT(".substr($propiedad,1,strlen($propiedad)).", '%e/%c/%Y %H:%i') AS FechaFin, ";
                    }
                    else if($propiedad == "_visibledesde")
                    {
                        $strCampos .="DATE_FORMAT(".substr($propiedad,1,strlen($propiedad)).", '%e/%c/%Y %H:%i') AS VisibleDesde, ";
                    }
                    else
                    {
                        $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    }
                }   
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM campanias WHERE IdCampania = ".mysql_real_escape_string($Id);
        }
        public function  CrearDirectorio()
        {
            try
            {
                $dirCamp = Aplicacion::getCampaniasImagenesRoot($this->getIdCampania());
                $dirTrai = Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."trailer/";
                $dirImg = Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/";
                
                if(!is_dir($dirCamp)){
                    @mkdir($dirCamp);
                    @chmod($dirCamp,0777);
				}
                if(!is_dir($dirTrai)){
                    @mkdir($dirTrai);
                    @chmod($dirTrai,0777);
                }
                if(!is_dir($dirImg)){
                    @mkdir($dirImg);
                    @chmod($dirImg,0777);
                }
            }
            catch(Exception $e)
            {
                throw $e;
            }        
        }
        /**
        * Metodo para borrar directorios de la campa�as
        */
        public function  EliminarDirectorio()
        {
            chdir("..");
            chdir("Campanias");
            $this->EliminarDir("campania_" . $this->getIdCampania());
        }
        public function  EliminarDir($carpeta )
        { 
            try
            {
                $directorio = @opendir($carpeta);
                while ($archivo = @readdir($directorio))
                { 
                    if($archivo !='.' && $archivo !='..')
                    {
                        //si es un directorio, volvemos a llamar a la funci�n para que elimine el contenido del mismo
                        if (is_dir($archivo )) 
                            $this->_EliminarDir($archivo ); 
                        //si no es un directorio, lo borramos 
                        //unlink($archivo); 
                    } 
                }
                closedir($directorio); 
                rmdir($carpeta);
            }
            catch(exception $e)
            {
                return false;
            }
            return true;        
        }
        public function Upload()
        {
            try
            {
            	require dirname(__FILE__).'/corners/corners.php';
            
                if(strlen($this->getTrailer_Origen())>0)
                {
                    if(is_file(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/trailer/".$this->getTrailer_Destino())) 
                        @unlink(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/trailer/".$this->getTrailer_Destino());
                      
                    @copy($this->getTrailer_Origen(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."trailer/".$this->getTrailer_Destino());             
                }
           		if(strlen($this->getVidrieraImagen_Origen())>0)
                {
                    if(is_file(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getVidrieraImagen_Destino())) 
                        @unlink(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getVidrieraImagen_Destino());
                      
                    @copy($this->getVidrieraImagen_Origen(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/".$this->getVidrieraImagen_Destino());             
                }
            	if(strlen($this->getVidrieraOffImagen_Origen())>0)
                {
                    if(is_file(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getVidrieraOffImagen_Destino())) 
                        @unlink(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getVidrieraOffImagen_Destino());
                      
                    @copy($this->getVidrieraOffImagen_Origen(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/".$this->getVidrieraOffImagen_Destino());             
                }
                if(strlen($this->getImagen_Origen())>0)
                {
                    $Archivo = new clsFile(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/".$this->getImagen_Destino()));
                    clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/".$Archivo->getNombre()), ".*");
                    clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/thumb_".$Archivo->getNombre()), ".*");
                    clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/no_".$Archivo->getNombre()), ".*");
                    clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/thumb_no_".$Archivo->getNombre()), ".*");
                    clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/thumb_catalogo_".$Archivo->getNombre()), ".*");
                    if(is_file(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getImagen_Destino())))
                    	unlink(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getImagen_Destino()));

                    
                  $destino = strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/".$this->getImagen_Destino());
                  move_uploaded_file($this->getImagen_Origen(), $destino);
                  copy($destino, str_replace('imagen.', 'imagen_original.', $destino));
                  IMGtoGrayScale(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getImagen_Destino(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/no_".$this->getImagen_Destino());                                 
                                
                    roundCorner(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getImagen_Destino()));
                    roundCorner(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/no_".$this->getImagen_Destino(), 'black.png');
                    
                    //Vidriera
                    Thumbnail(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/no_".$this->getImagen_Destino(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/thumb_no_".$this->getImagen_Destino(),270, 165);
                    Thumbnail(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getImagen_Destino(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/thumb_".$this->getImagen_Destino(),270, 165);
                     //Cat�logo
                    Thumbnail(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getImagen_Destino(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/thumb_catalogo_".$this->getImagen_Destino(),150, 92);
                    
                }                    
                
           		if(strlen($this->getGreyImagen_Origen())>0){
                $Archivo = new clsFile(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/".$this->getGreyImagen_Destino()));
                clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/no_".$Archivo->getNombre()), ".*");
                clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/thumb_no_".$Archivo->getNombre()), ".*");
                if(is_file(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getGreyImagen_Destino())))
                    unlink(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getGreyImagen_Destino()));

                  $destino = strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/no_".$this->getGreyImagen_Destino());
                  move_uploaded_file($this->getGreyImagen_Origen(), $destino);

                  IMGtoGrayScale(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/no_".$this->getGreyImagen_Destino(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/no_".$this->getGreyImagen_Destino());                                 
                                
                    roundCorner(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getGreyImagen_Destino()));
                    roundCorner(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/no_".$this->getGreyImagen_Destino(), 'black.png');
                    
                    //Vidriera
                    Thumbnail(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/no_".$this->getGreyImagen_Destino(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/thumb_no_".$this->getGreyImagen_Destino(),270, 165);      
                }
                
                if(strlen($this->getWelcome_Origen())>0)
                {
                    $Archivo = new clsFile(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/".$this->getWelcome_Destino()));
                    clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/".$Archivo->getNombre()), ".*");
                    if(is_file(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getWelcome_Destino())))
                    	unlink(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getWelcome_Destino()));
                    
                  $destino = strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/".$this->getWelcome_Destino());
                  move_uploaded_file($this->getWelcome_Origen(), $destino);                    
                }                    
                if(strlen($this->getLogo_Origen())>0)
                {
                    $Archivo = new clsFile(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/".$this->getLogo_Destino()));
                    clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/".$Archivo->getNombre()), ".*");
                    clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/thumb_".$Archivo->getNombre()), ".*");
                    clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/no_".$Archivo->getNombre()), ".*");
                    clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/thumb_no_".$Archivo->getNombre()), ".*");
                    clsFile::Delete(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/thumb_catalogo_".$Archivo->getNombre()), ".*");
                    
                    if(is_file(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getLogo_Destino())))
                        unlink(strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/".$this->getLogo_Destino()));
                        
                    if(is_file(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/logo2.png"))
                        unlink(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/logo2.png");  
                                           
                    if(is_file(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/no-logo2.png"))
                        unlink(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/no-logo2.png");                     

					$ext =	strtolower(substr(strrchr($this->getLogo_Destino(), "."), 1));
               		if($ext == 'png') {
	                   SaveTransparentLogo($this->getLogo_Origen(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/no-".$this->getLogo2_Destino(), 0);
	                   SaveTransparentLogo($this->getLogo_Origen(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getLogo2_Destino(), 1);
	                }

                	$destino = strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getLogo_Destino());
                    move_uploaded_file($this->getLogo_Origen(), $destino);
                    copy($destino, str_replace('logo.', 'logo_original.', $destino));
                    IMGtoGrayScale(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getLogo_Destino() , Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/no_".$this->getLogo_Destino());
                    //Vidriera
                    Thumbnail(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/no_".$this->getLogo_Destino(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/thumb_no_".$this->getLogo_Destino(), 158, 80);//imagen del titulo
                    Thumbnail(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getLogo_Destino(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/thumb_".$this->getLogo_Destino(), 158, 80);//imagen del titulo
                    //Cat�logo
                    Thumbnail(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getLogo_Destino(), Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/thumb_catalogo_".$this->getLogo_Destino(), 140, 71);
              
	                   
                }
                 if(strlen($this->getNews1_Origen())>0)
                {
                    $destino = strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getNews1_Destino());
                    move_uploaded_file($this->getNews1_Origen(), $destino);
                }
                
                if(strlen($this->getNews2_Origen())>0)
                {
                    $destino = strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getNews2_Destino());
                    move_uploaded_file($this->getNews2_Origen(), $destino);
                }
                
                if(strlen($this->getNews3_Origen())>0)
                {
                    $destino = strtolower(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."/imagenes/".$this->getNews3_Destino());
                    move_uploaded_file($this->getNews3_Origen(), $destino);
                }
                
            }
            catch(Exception $e)
            {
                throw $e;
            }
        }

        
        /*public function getFiles()
        {
            try
            {
                $files=array();
                $i=0;
                $dir=@opendir(Aplicacion::getCampaniasRoot($this->getIdCampania())."/");
                while($archivo = @readdir($dir))
                {
                    if($archivo != "." && $archivo != "..")
                    {
                        $files[$i]["link"] = Aplicacion::getCampaniasRoot($this->getIdCampania())."/";
                        $files[$i]["nombre"] = $archivo;
                        $i++;
                    }
                }
                @closedir($dir);
                $dir=@opendir(Aplicacion::getCampaniasRoot($this->getIdCampania())."/");
                while($archivo = @readdir($dir))
                {
                    if($archivo != "." && $archivo != "..")
                    {
                        $files[$i]["link"] = Aplicacion::getCampaniasRoot($this->getIdCampania())."/";
                        $files[$i]["nombre"] = $archivo;
                        $i++;
                    }
                }
                @closedir($dir);
                return $files;
            }
            catch(Exception $e)
            {
                throw $e;
            }
        }*/
    
	public function getFiles()
    {
        try
        {
            $files=array();
            $i=0;
            $dir=@opendir(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."trailer/");
            while($archivo = @readdir($dir))
            {
                if($archivo != "." && $archivo != "..")
                {
                    $files[$i]["link"] = Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."trailer/";
                    $files[$i]["nombre"] = $archivo;
                    $files[$i]["public_link"] = Aplicacion::getRootUrl()."front/campanias/archivos/campania_".$this->getIdCampania()."/trailer/".$archivo;
                    $i++;
                }
            }
            @closedir($dir);
            $dir=@opendir(Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/");
            while($archivo = @readdir($dir))
            {
                if($archivo != "." && $archivo != "..")
                {
                    $files[$i]["link"] = Aplicacion::getCampaniasImagenesRoot($this->getIdCampania())."imagenes/";
                    $files[$i]["nombre"] = $archivo;
                    $files[$i]["public_link"] = Aplicacion::getRootUrl()."front/campanias/archivos/campania_".$this->getIdCampania()."/imagenes/".$archivo;
                    $i++;
                }
            }
            @closedir($dir);
            return $files;
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
}
?>