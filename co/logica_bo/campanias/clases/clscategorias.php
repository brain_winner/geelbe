<?
    class MySQL_Categoria extends categorias
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if(!is_array($valor) && $propiedad != "_deep")
                {
                    if($propiedad == "_idcategoria" || $propiedad == "_idpadre")
                    {   
                        $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                        $strValores .=($valor != 0) ? chr(34).mysql_real_escape_string($valor).chr(34).", " : "NULL, ";
                    }
                    else
                    {   
                        $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                        $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                        $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    }
                }   
            }
            return "INSERT INTO categorias (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getDelete()
        {
            return "DELETE FROM categorias WHERE IdCategoria = ".$this->getIdCategoria();
        }        
        public function getById($Id)
        {
            $strCampos="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                 if(!is_array($valor) && $propiedad != "_deep")
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM categorias WHERE IdCategoria = ".mysql_real_escape_string($Id);
        }
    }
    class MySQL_ProductosCategoria extends productos_x_categorias
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if(!is_array($valor))
                {
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
            }
            return "INSERT INTO categorias (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
    }
?>