<?php
    class clsOrdenamientos
    {
        private $_idOrdenamiento;
        private $_descripcion;
        private $_campo;
        private $_ordenamiento;
        
        public function __construct()
        {
            $this->_idOrdenamiento=0;
            $this->_descripcion = "";
            $this->_campo = "";
            $this->_ordenamiento="";
        }
        public function setIdOrdenamiento($valor)
        {
            $this->_idOrdenamiento = $valor;
        }
        public function setDescripcion($valor)
        {
            $this->_descripcion = $valor;
        }
        public function setCampo($valor)
        {
            $this->_campo = $valor;
        }
        public function setOrdenamiento($valor)
        {
            $this->_ordenamiento = $valor;
        }
        public function getIdOrdenamiento()
        {
            return $this->_idOrdenamiento;
        }
        public function getDescripcion()
        {
            return $this->_descripcion;
        }
        public function getCampo()
        {
            return $this->_campo;
        }
        public function getOrdenamiento()
        {
            return $this->_ordenamiento;
        }
    }
?>
