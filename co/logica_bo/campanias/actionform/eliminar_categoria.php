<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
</head>
<body>
<?php
    if($_GET)
    {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        Includes::Scripts();
        extract($_GET);
        try
        {   
            $objCategoria = dmCategoria::getByIdCategoria($IdCategoria);
            dmCategoria::delete($objCategoria);
            
            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
			$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
			$cache->clean(
			    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
			    array('group_cache_catalogo_campania_'.mysql_real_escape_string($IdCampania))
			);
            
            ?>
                <script type="text/javascript">
                    window.location = DIRECTORIO_URL_BO + "campanias/index_categorias.php?IdCampania=<?=$objCategoria->getIdCampania()?>";
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                    window.open("../../../bo/campanias/index_categorias.php?error=<?=$e->getNumeroSQLERROR()?>", "_self");
                </script>
            <?
        }
        ob_flush();
    }
?>
</body>
</html>