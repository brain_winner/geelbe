<?
	header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=".date("Ymd")."_orden_siigo.csv");

 try
{
	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	ValidarUsuarioLogueadoBo(9);
 
}
catch(ACCESOException $e)
{
?>  
    <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
<?
    exit;
}    
try{
///NOTA DE PEDIDO!
	
	
	
	//Inicializo Variables
	
	//Numero de Campania
	$IdCampania = $_GET['IdCampania'];
	$DatosAtributos = array();
	$DatosProductos = array();
		
	$conexion = Conexion::nuevo();

	$conexion->setQuery('
	SELECT pd.*, SUM(pp.Cantidad) Cantidad, prv.Nombre as Proveedor, prv.CIF, cg.linea as Linea, cg.grupo as Grupo,
	(
		SELECT pa.Valor 
		FROM productos_x_atributos pa
		JOIN atributos_x_clases ac ON ac.idAtributoClase = pa.idAtributoClase
		JOIN atributos a ON a.IdAtributo = ac.IdAtributo
		WHERE pa.IdCodigoProdInterno = pp.IdCodigoProdInterno AND a.Nombre IN (\'Talla\', \'Talle\')
		LIMIT 1
	) as Talla,
	(
		SELECT pa.Valor 
		FROM productos_x_atributos pa
		JOIN atributos_x_clases ac ON ac.idAtributoClase = pa.idAtributoClase
		JOIN atributos a ON a.IdAtributo = ac.IdAtributo
		WHERE pa.IdCodigoProdInterno = pp.IdCodigoProdInterno AND a.Nombre = \'Color\'
		LIMIT 1
	) as Color
	FROM productos_x_pedidos pp
	LEFT JOIN productos_x_proveedores pr ON pr.IdCodigoProdInterno = pp.IdCodigoProdInterno
	LEFT JOIN productos pd ON pd.IdProducto = pr.IdProducto
	LEFT JOIN pedidos p ON p.IdPedido = pp.IdPedido
	LEFT JOIN proveedores prv ON prv.IdProveedor = pr.IdProveedor
	LEFT JOIN productos_x_categorias_globales pxcg ON pxcg.IdProducto = pd.IdProducto
	LEFT JOIN categorias_globales cg ON cg.IdCategoriaGlobal = pxcg.IdCategoriaGlobal
	WHERE p.IdCampania = '.mysql_real_escape_string($IdCampania).'
	GROUP BY pp.IdCodigoProdInterno
	');
	
	$DatosProductos = $conexion->DevolverQuery();	
	foreach($DatosProductos as $p) {
		
		echo $p['Proveedor'].';';
		echo $p['CIF'].';';
		echo $p['Referencia'].';';
		echo $p['Cantidad'].';';
		echo $p['Nombre'].';';
		echo $p['Talla'].';';
		echo $p['Color'].';';
		echo $p['Linea'].';';
		echo $p['Grupo'].';';
		echo ';';
		echo ';';
		echo ';';
		echo $p['iva'].';';
		echo $p['PVP'].';';
		echo ($p['PVP']*$p['Cantidad']).';';
		echo "\n";
		
	}
	
}
catch(Exception $e){
	echo Conexion::nuevo()->DevolverLog();
	throw $e;
}?>