<?
	header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=".date("Ymd")."_factura_siigo.csv");

 try
{
	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	ValidarUsuarioLogueadoBo(9);
 
}
catch(ACCESOException $e)
{
?>  
    <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
<?
    exit;
}    
try{
///NOTA DE PEDIDO!
	
	
	
	//Inicializo Variables
	
	//Numero de Campania
	$IdCampania = $_GET['IdCampania'];
	$DatosAtributos = array();
	$DatosProductos = array();
		
	$conexion = Conexion::nuevo();

	$conexion->setQuery('
	SELECT p.*, 
	u.NombreUsuario,
	da.NroDoc, da.Nombre, da.Apellido, da.Telefono, da.Celunumero,
	CONCAT(di.Domicilio, " ", di.Numero) as Domicilio, ciud.Descripcion Ciudad, prov.Nombre Provincia, di.CP, pa.Nombre Pais,
	c.Nombre Campania, e.Descripcion Estado,
	pd.IdProducto, pd.iva, pp.Cantidad, pd.Referencia, pd.Nombre Producto, pp.Precio, fp.Descripcion FormaPago, pp.IdCodigoProdInterno, cg.linea as Linea, cg.grupo as Grupo
	FROM pedidos p
	LEFT JOIN campanias c ON c.IdCampania = p.IdCampania
	LEFT JOIN estadospedidos e ON e.IdEstadoPedido = p.IdEstadoPedidos
	LEFT JOIN productos_x_pedidos pp ON pp.IdPedido = p.IdPedido
	LEFT JOIN productos_x_proveedores pr ON pr.IdCodigoProdInterno = pp.IdCodigoProdInterno
	LEFT JOIN productos pd ON pd.IdProducto = pr.IdProducto
	LEFT JOIN usuarios u ON u.IdUsuario = p.IdUsuario
	LEFT JOIN datosusuariospersonales da ON da.IdUsuario = u.IdUsuario
	LEFT JOIN datosusuariosdirecciones di ON di.IdUsuario = u.IdUsuario
	LEFT JOIN provincias prov ON prov.IdProvincia = di.IdProvincia
	LEFT JOIN ciudades ciud ON ciud.IdCiudad = di.IdCiudad
	LEFT JOIN paises pa ON pa.IdPais = di.IdPais
	LEFT JOIN formaspagos fp ON fp.IdFormaPago = p.IdFormaPago
	LEFT JOIN productos_x_categorias_globales pxcg ON pxcg.IdProducto = pd.IdProducto
	LEFT JOIN categorias_globales cg ON cg.IdCategoriaGlobal = pxcg.IdCategoriaGlobal
	WHERE p.IdCampania = '.mysql_real_escape_string($IdCampania).' AND p.IdEstadoPedidos IN (2,7,9) 
	');
	
	$DatosProductos = $conexion->DevolverQuery();	
	
	foreach($DatosProductos as $p) {

		$conexion = Conexion::nuevo();
		$conexion->setQuery('SELECT a.Nombre, pa.Valor 
		FROM productos_x_atributos pa
		JOIN atributos_x_clases ac ON ac.idAtributoClase = pa.idAtributoClase
		JOIN atributos a ON a.IdAtributo = ac.IdAtributo
		WHERE pa.IdCodigoProdInterno = '.$p['IdCodigoProdInterno']);
		$data = $conexion->DevolverQuery();
		$atts = array();
		foreach($data as $a)
			$atts[] = $a['Nombre'].': '.$a['Valor'];

		echo ";";
		echo ";";
		echo ";";
		echo $p['IdPedido'].';';
		echo $p['Apellido'].';';
		echo $p['Nombre'].';';
		echo $p['Fecha'].';';
		echo $p['NroDoc'].';';
		echo $p['Telefono'].';';
		echo $p['Celunumero'].';';
		echo $p['NombreUsuario'].';';
		echo $p['Domicilio'].';';
		echo $p['Ciudad'].';';
		echo $p['Provincia'].';';
		echo $p['CP'].';';
		echo $p['Pais'].';';
		echo $p['Campania'].';';
		echo $p['Estado'].';';
		echo $p['Referencia'].';';
		echo $p['Cantidad'].';';
		echo $p['Producto'].';';
		echo implode(', ', $atts).';';
		echo ';';
		echo $p['Linea'].';';
		echo $p['Grupo'].';';
		echo ';';
		echo ';';
		echo ';';
		echo ';';
		echo $p['iva'].';';
		echo $p['Precio'].';';
		echo $p['GastosEnvio'].';';
		echo ';';
		echo ';';
		echo ';';
		echo $p['Total'].';No;';
		echo $p['FormaPago'];

		echo "\n";
		
	}
	
}
catch(Exception $e){
	echo Conexion::nuevo()->DevolverLog();
	throw $e;
}?>