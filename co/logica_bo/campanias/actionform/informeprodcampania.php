<?
     header("Content-Description: File Transfer");
   	 header("Content-Type: application/force-download");
     header("Content-Disposition: attachment; filename=".date("Ymd")."_productos_campaign.csv");
   

try
{
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    ValidarUsuarioLogueadoBo(9);
}
catch(ACCESOException $e)
{
?>  
    <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
<?
    exit;
}    
try{
///NOTA DE PEDIDO!

	//Inicializo Variables
	
	//Numero de Campania
	$IdCampania = $_GET[IdCampania];
	$DatosAtributos = array();
	$DatosProductos = array();
		
	$conexion = Conexion::nuevo();
		
	$conexion->setQuery("SELECT camp.Nombre As Nombre, pro.Referencia AS Referencia, pro.Descripcion AS Descripcion, pro.PVenta as PCU, pro.PCompra AS Costo
	FROM productos AS pro
	JOIN productos_x_categorias AS pxc ON pxc.IdProducto = pro.IdProducto
	JOIN categorias AS cat ON cat.IdCategoria = pxc.IdCategoria
	JOIN campanias AS camp ON camp.IdCampania = cat.IdCampania
	WHERE camp.IdCampania IN (".mysql_real_escape_string($IdCampania).")
	GROUP BY pro.IdProducto");

	$DatosProductos = $conexion->DevolverQuery();
	
	//Extraigo Los IdCodigoProdInterno
	foreach($DatosProductos AS $i => $item){
		$item["IdCodigoProdInterno"] = intval($item["IdCodigoProdInterno"]);
		if($item["IdCodigoProdInterno"]>0) $Codigos[] = $item["IdCodigoProdInterno"];
		
		unset($DatosProductos[$i]['uid']);
	}
	
	$Codigos = array_unique($Codigos);
	
	//Busco los atributos para esos codigos
	if(strlen(implode(",",$Codigos))>0){	
	
		$conexion->setQuery("SELECT pxa.IdCodigoProdInterno, at.Nombre as Atributo, pxa.Valor
		FROM productos_x_atributos AS pxa
		JOIN atributos_x_clases AS axc ON axc.idAtributoClase = pxa.idAtributoClase
		JOIN atributos AS at ON at.IdAtributo = axc.IdAtributo
		WHERE  pxa.IdCodigoProdInterno IN (".implode(",",$Codigos).") ORDER BY at.Nombre");		
		$Atributos = $conexion->DevolverQuery();		
		$camposAtributos = array();
		
		//Armo un bonito array
		foreach($Atributos as $atrib){
			$DatosAtributos[$atrib["IdCodigoProdInterno"]][$atrib["Atributo"]] = $atrib["Valor"];
			$camposAtributos[] = $atrib["Atributo"];
		}
	}
	
	//ARMO EL CSV
	$datospdf = "";
		
	//Columnas
	$i=0;
	foreach($DatosProductos[0] AS $key=>$value){
		if($i!=0) $datospdf .=";";
		$datospdf .=$key;
		$i++;
	}
	$datospdf .="\n";
	
	//$datospdf .="Referenica;Cantidad;Producto;;;;Precio Unitario;Total\n";
	$datospdf = str_replace("IdCodigoProdInterno;",";;;",$datospdf);
	
	//Filas
	foreach($DatosProductos AS $item){
		//Primeros datos
		$j = 0;
		foreach($item as $key => $value){
			if($j!=0) $datospdf .= ";";
			if($key != "IdCodigoProdInterno"){
				//INSERT CUALQUIER OTRO CAMPO
				$datospdf .= $value;
			}else{				
				// INSERTO ATRIBUTOS
				$k=0;
				
				foreach($DatosAtributos[$item["IdCodigoProdInterno"]] as $aa){
					if($k!=0) $datospdf .= ";";
					$datospdf .= $aa;
					$k++;
				}
				
				while($k<3){
					if($k!=0) $datospdf .=";";
					$k++;
				}								
			}
			$j++;
		}
		
		$datospdf .="\n";
	}	
}
catch(Exception $e){
	echo Conexion::nuevo()->DevolverLog();
	throw $e;
}?>
<?=$datospdf?>