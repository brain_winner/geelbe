<?php
    if($_POST)
    {
        ob_start();
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        extract($_POST);
        
        
        try
        {   
            $IdCategoria = ($txtIdCategoria == "0") ? 0 : Aplicacion::Decrypter($txtIdCategoria);
            $IdCampania = ($txtIdCampania == "0") ? 0 : Aplicacion::Decrypter($txtIdCampania);
            $IdPadre = ($txtIdPadre == "0") ? 0 : Aplicacion::Decrypter($txtIdPadre);
            $objCategoria = new MySQL_Categoria();
            $objCategoria->setDescripcion($txtDescripcion);
            $objCategoria->setNombre($txtNombre);
            $objCategoria->setIdCampania($IdCampania);
            $objCategoria->setIdCategoria($IdCategoria);
            $objCategoria->setIdPadre($IdPadre);
            $objCategoria->setOrden($txtOrden);
            $objCategoria->setIdOrdenamiento($cbxOrdenamiento);
            $objCategoria->setHabilitado(isset($chkHabilitado)?1:0);
            if(isset($txt_tbl))
            {
                foreach($txt_tbl as $i=>$fila)
                {
                    $objPC = new MySQL_ProductosCategoria();
                    $objPC->setIdProducto($fila[0]);
                    $objCategoria->addProductos($objPC);
                }
            }   
            dmCategoria::save($objCategoria);
            
            if($IdCampania != 0) {
	            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->clean(
				    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
				    array('group_cache_catalogo_campania_'.$IdCampania)
				);
            }
            
            ?>
                <script type="text/javascript">
                   window.parent.location = DIRECTORIO_URL_BO + "campanias/index_categorias.php?IdCampania=<?=$IdCampania?>";
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                   window.ErrorMSJ(<?=$e->getNumeroSQLERROR()?>);
                </script>
            <?
        }
        ob_flush();
    }
?>