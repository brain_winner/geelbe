<?
	header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=".date("Ymd")."_pedidos_dhl.csv");

 try
{
	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	ValidarUsuarioLogueadoBo(9);
 
}
catch(ACCESOException $e)
{
?>  
    <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
<?
    exit;
}    
try{
///NOTA DE PEDIDO!
	
	
	
	//Inicializo Variables
	
	//Numero de Campania
	$IdCampania = $_GET[IdCampania];
	$DatosAtributos = array();
	$DatosProductos = array();
		
	$conexion = Conexion::nuevo();

	$conexion->setQuery('SELECT 
	
	us.IdUsuario as clave_d,
	" " as referencia,
	CONCAT(du.Nombre, " ", du.Apellido) as empresa,
	CONCAT(du.Nombre, " ", du.Apellido) as contacto,
	CONCAT(pd.Domicilio," ",pd.Numero," ",IF(pd.Piso NOT LIKE "",CONCAT("P",pd.Piso),"")," ",pd.Puerta) AS `direccion`,
	" " as ciudad_dhl,
	" " as iata,
	pd.CP as cp,
	CONCAT(pd.codTelefono," ",pd.Telefono) AS telefono,
	" " as centro_costos_d,
	"MX" as pais,
	us.NombreUsuario as email,
	" " as usuario,
	p.IdPedido as "Numero de Pedido"
FROM pedidos AS p
LEFT JOIN usuarios AS us ON us.IdUsuario = p.IdUsuario
LEFT JOIN campanias AS cam ON cam.IdCampania = p.IdCampania
LEFT JOIN productos_x_pedidos AS pxp ON p.IdPedido = pxp.IdPedido
LEFT JOIN productos_x_proveedores AS pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
LEFT JOIN productos_x_categorias AS pxc ON pxpr.IdProducto = pxc.IdProducto
LEFT JOIN categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
LEFT JOIN productos AS pro ON pro.IdProducto = pxpr.IdProducto
LEFT JOIN pedidos_direcciones AS pd ON pd.IdPedido = p.IdPedido
LEFT JOIN paises AS pa ON pa.IdPais = pd.IdPais
LEFT JOIN provincias AS pv ON pv.IdProvincia = pd.IdProvincia
LEFT JOIN estadospedidos AS ep ON ep.IdEstadoPedido = p.IdEstadoPedidos
LEFT JOIN datosusuariospersonales AS du ON du.IdUsuario = us.IdUsuario
WHERE p.IdEstadoPedidos IN (2,7,9) AND cam.IdCampania IN ('.mysql_real_escape_string($IdCampania).')
GROUP BY p.IdPedido');
	
	$DatosProductos = $conexion->DevolverQuery();	

	//ARMO EL CSV
	$datospdf = "";
		
	//$datospdf .="Referenica;Cantidad;Producto;".$camposAtributos.";Precio Unitario;Total\n";
	
	//Columnas
	$i=0;
	foreach($DatosProductos[0] AS $key=>$value){
		if($i!=0) $datospdf .=";";
		$datospdf .= trim($key);
		$i++;
	}
	$datospdf .="\n";
	
	//$datospdf .="Referenica;Cantidad;Producto;;;;Precio Unitario;Total\n";
	//$datospdf = str_replace("IdCodigoProdInterno;",";;;",$datospdf);
	
	//Filas
	foreach($DatosProductos AS $item){
		//Primeros datos
		$j = 0;
		foreach($item as $key => $value){
			if($j!=0) $datospdf .= ";";
			$datospdf .= strtoupper(utf8_encode($value));
			$j++;
		}
		
		$datospdf .="\n";
		
		//$datospdf .=$item["Referencia"].";".$item["Cantidad"].";".$item["Nombre"].";";		
						
		//$datospdf .=$item["PCU"].";".$item["CostoTotal"]."\n";	
	}	
}
catch(Exception $e){
	echo Conexion::nuevo()->DevolverLog();
	throw $e;
}?>
<?=$datospdf?>