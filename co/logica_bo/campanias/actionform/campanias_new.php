<?php
    if($_POST) {
    	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    	Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    	Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
        ob_start();
		set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        extract($_POST);
        
        if(!isset($_GET['page']) || $_GET['page'] < 1) $_GET['page'] = 1;
        
        list($fiDia, $fiMes, $fiAnio) = split('[/.-]', $FechaInicio["txtDia"]);
        list($ffDia, $ffMes, $ffAnio) = split('[/.-]', $FechaFin["txtDia"]);
        list($fvDia, $fvMes, $fvAnio) = split('[/.-]', $FechaVisible["txtDia"]);
        
        list($inDia, $inMes, $inAnio) = split('[/.-]', $TiempoEntregaIn);
        list($fnDia, $fnMes, $fnAnio) = split('[/.-]', $TiempoEntregaFn);
        list($inDia2, $inMes2, $inAnio2) = split('[/.-]', $TiempoEntregaIn2);
        list($fnDia2, $fnMes2, $fnAnio2) = split('[/.-]', $TiempoEntregaFn2);
        list($fnDia3, $fnMes3, $fnAnio3) = split('[/.-]', $FechaCorte);
        
        if ($txtMinMontoEnvioGratis == 0) {
        	$txtMinMontoEnvioGratisVal = 0;
        }
    	
        try {   
            $IdCampania = ($txtIdCampania == "0") ? 0 : $txtIdCampania;
            $objCampania = new MySQL_Campania();
            
            $objCampania->setIdCampania($IdCampania);
            
            $objCampania->setNombre($txtNombre);
            $objCampania->setDescuentoMax($txtDescuentoMax);
            $objCampania->setIdComercial($IdComercial);
            $objCampania->setDescripcion($txtDescripcion);
            
            $objCampania->setFechaInicio($fiAnio."-".$fiMes."-".$fiDia." ".$FechaInicio["txtHora"].":".$FechaInicio["txtMin"]);
            $objCampania->setFechaFin($ffAnio."-".$ffMes."-".$ffDia." ".$FechaFin["txtHora"].":".$FechaFin["txtMin"]);
            $objCampania->setVisibleDesde($fvAnio."-".$fvMes."-".$fvDia." ".$FechaVisible["txtHora"].":".$FechaVisible["txtMin"]);
            
            $objCampania->setMaxProd($txtMaxProd);
            $objCampania->setIdOrdenamiento($cbxOrdenamiento);
            
            $objCampania->setMinMontoEnvioGratis($txtMinMontoEnvioGratisVal);
            $objCampania->setTiempoEntregaIn($inAnio."-".$inMes."-".$inDia);
            $objCampania->setTiempoEntregaFn($fnAnio."-".$fnMes."-".$fnDia);
            $objCampania->setTiempoEntregaIn2($inAnio2."-".$inMes2."-".$inDia2);
            $objCampania->setTiempoEntregaFn2($fnAnio2."-".$fnMes2."-".$fnDia2);
            $objCampania->setFechaCorte($fnAnio3."-".$fnMes3."-".$fnDia3);
            $objCampania->setWelcome($welcome);
            
            if ($txtEnvioGratuito == 1) {
                $objCampania->addPropiedad(6);
            }
            
            if(isset($_FILES["txtTrailer"])) {
                $ext = explode(".", $_FILES["txtTrailer"]["name"]);
                $objCampania->setTrailer_Origen($_FILES["txtTrailer"]["tmp_name"]);
                $objCampania->setTrailer_Destino("trailer.".$ext[1]);
            }
            if(isset($_FILES["txtLogo"])) {
                $ext = substr(strrchr($_FILES["txtLogo"]["name"], "."), 1);
                $objCampania->setLogo_Origen($_FILES["txtLogo"]["tmp_name"]);
                $objCampania->setLogo_Destino("logo.".$ext);
                $objCampania->setLogo2_Destino("logo2.".$ext);
            }
            if(isset($_FILES["txtImagen"])) {
                $ext = explode(".", $_FILES["txtImagen"]["name"]);
                $objCampania->setImagen_Origen($_FILES["txtImagen"]["tmp_name"]);
                $objCampania->setImagen_Destino("imagen.".$ext[1]);
            }
        	if(isset($_FILES["txtGreyImagen"])) {
                $ext = explode(".", $_FILES["txtGreyImagen"]["name"]);
                $objCampania->setGreyImagen_Origen($_FILES["txtGreyImagen"]["tmp_name"]);
                $objCampania->setGreyImagen_Destino("imagen.".$ext[1]);
            }
            if(isset($_FILES["txtWelcome"])) {
                $ext = explode(".", $_FILES["txtWelcome"]["name"]);
                $objCampania->setWelcome_Origen($_FILES["txtWelcome"]["tmp_name"]);
                $objCampania->setWelcome_Destino("welcome.".$ext[1]);
            }
			if(isset($_FILES["txtVidrieraImagen"])) {
                $ext = explode(".", $_FILES["txtVidrieraImagen"]["name"]);
                $objCampania->setVidrieraImagen_Origen($_FILES["txtVidrieraImagen"]["tmp_name"]);
                $objCampania->setVidrieraImagen_Destino("banner_camp-on.".$ext[1]);
            }
        	if(isset($_FILES["txtVidrieraOffImagen"])) {
                $ext = explode(".", $_FILES["txtVidrieraOffImagen"]["name"]);
                $objCampania->setVidrieraOffImagen_Origen($_FILES["txtVidrieraOffImagen"]["tmp_name"]);
                $objCampania->setVidrieraOffImagen_Destino("banner_camp-off.".$ext[1]);
            }
            
            $txtIdCampania = dmCampania::save($objCampania);
            dmCategoriaGlobal::saveCampania($txtIdCampania, $cbxCategoriaGlobal);
        	
            // Carga de Categorķas.
	    	$listCategorias = dmCategoria::getCategoriasNew($txtIdCampania);
	    	
	    	foreach ($listCategorias as $filaCategoria) {
	        	if ($filaCategoria["IdPadre"] == null) { //categoria padre
	        		$padre = $filaCategoria["IdCategoria"];
	        		continue;
	    		}
	    		
	    		if ($filaCategoria["IdPadre"] == $padre) {
	    			$name = "c".$filaCategoria["IdCategoria"];
	    		}
	    		else {
	    			$name = "s".$filaCategoria["IdCategoria"]."c".$filaCategoria["IdPadre"];
	    		}
	    		$modified = false;
	    		
	    		$objCategoria = dmCategoria::getByIdCategoria($filaCategoria["IdCategoria"]);
	    		
	    		foreach ($txt as $key => $value) {
		    		if ($value == null || trim($value)=="") {
		    			continue;
		    		}
		        	if ($key == $name) {
		        		$modified = true;
		        		$objCategoria->setNombre($value);
		        		dmCategoria::saveNew($objCategoria);
		        		break;
		        	}
		        }
		        if (!$modified) {
		        	dmCategoria::delete($objCategoria);
		        	
		        }
	        }
	        if ($fatherId == "" || $fatherId == "null" || $fatherId == null) { 
	        	$fatherId = dmCategoria::getLastInsertId();
	        }
	        
	        $lastCatId = 0;
	    	foreach ($txt as $key => $value) {
	    		
	    		if ($value == null || trim($value)=="") {
	    			continue;
	    		}
	    		
	    		
		       	if (strncmp($key, "n", strlen("n")) == 0) {
		       		if (strncmp($key, "nc", strlen("nc")) == 0) {
		       			$IdPadre = $fatherId;
		       			$lastCatId = 0;
		       		}
		       		else {
		       			$IdPadre = $lastCatId;
		       		}
	            	
	            	$objCategoria = new MySQL_Categoria();
	            	$objCategoria->setDescripcion("");
	            	$objCategoria->setNombre($value);
	            	$objCategoria->setIdCampania($txtIdCampania);
	            	$objCategoria->setIdCategoria(0);
	            	$objCategoria->setIdPadre($IdPadre);
	            	$objCategoria->setOrden(0);
	            	$objCategoria->setIdOrdenamiento(1);
	            	$objCategoria->setHabilitado(1);
	            	dmCategoria::saveNew($objCategoria);
		       		
		       		if (strncmp($key, "nc", strlen("nc")) == 0) {
		       			$lastCatId = dmCategoria::getLastInsertId();
		       		}
		       	}
		       	
	    		if (strncmp($key, "c", strlen("c")) == 0) {
	    			$lastCatId = substr($key, 1);
		       	}
		    }
		    
            if($IdCampania != 0) {
	            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->clean(
				    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
				    array('group_cache_catalogo_campania_'.$IdCampania)
				);
            }
		    
	    	if (isset($_POST['botonGuardarYSalir'])) { 
	    		header("Location: ".UrlResolver::getBaseURL("bo/campanias/index_new.php?page=".$_GET['page']));
				die;
	    	}
	    
	    	if (isset($_POST['botonResponderYSiguiente'])) { 
	    		header("Location: ".UrlResolver::getBaseURL("bo/campanias/productos.php?IdCampania=".$txtIdCampania));
				die;
	    	}
        
        }
        catch(MySQLException $e) {
            echo "<br>Error: $e<br>";
        }
        ob_flush();
    }
?>