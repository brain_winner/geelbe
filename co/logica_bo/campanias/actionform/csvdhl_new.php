<?
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate");    
header ("Pragma: no-cache");    
header ('Content-type: application/x-msexcel');
header ("Content-Disposition: attachment; filename=FacturaCampania.xls" ); 
header ("Content-Description: PHP/INTERBASE Generated Data" );

try {
	ob_start();
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/exporters/xls-exporter.php");
    ob_clean();
    ValidarUsuarioLogueadoBo(9);
}
catch(ACCESOException $e) {
?><script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script><?
    exit;
}    
try {
    //Inicializo Variables
    
    //Numero de Campania
    $IdCampania = $_GET[IdCampania];

    $conexion = Conexion::nuevo();
    $conexion->setQuery("
    SELECT 
        p.IdUsuario AS idUsuario,
        pd.Nombre AS nombre,
        pd.Apellido AS apellido,
        pd.Domicilio AS domicilio,
        pd.Numero AS numero,
        pd.Piso AS piso,
        pd.CP AS cp,
        pd.codTelefono AS codtelefono,
        pd.Telefono AS telefono,
        ciudades.Nombre AS ciudad,
        estados.Nombre AS estado,
        p.Fecha AS fecha, 
        p.IdPedido AS idpedido,
        us.IdUsuario AS idusuario,
        pxp.IdCodigoProdInterno AS idcodigoprodinterno,
        pro.Nombre AS articulo,
        pxp.Cantidad AS cantidad,
        pxp.Precio AS precio,
        pro.Referencia AS referencia,
        p.GastosEnvio as gastos,
        bonos.Importe as bono
    FROM pedidos AS p 
    LEFT JOIN usuarios AS us ON us.IdUsuario = p.IdUsuario 
    LEFT JOIN datosusuariosdirecciones AS dir ON us.IdUsuario = dir.IdUsuario 
    LEFT JOIN datosusuariospersonales AS per ON us.IdUsuario = per.IdUsuario 
    LEFT JOIN dhl_zonas_x_estados AS dhlzp ON dhlzp.IdEstado = dir.IdProvincia 
    LEFT JOIN dhl_zonas AS dhlz ON dhlz.IdZona = dhlzp.IdZona 
    LEFT JOIN campanias AS cam ON cam.IdCampania = p.IdCampania 
    LEFT JOIN productos_x_pedidos AS pxp ON p.IdPedido = pxp.IdPedido 
    LEFT JOIN productos_x_proveedores AS pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno 
    LEFT JOIN productos_x_categorias AS pxc ON pxpr.IdProducto = pxc.IdProducto 
    LEFT JOIN categorias AS cat ON pxc.IdCategoria = cat.IdCategoria 
    LEFT JOIN productos AS pro ON pro.IdProducto = pxpr.IdProducto 
    LEFT JOIN pedidos_direcciones AS pd ON pd.IdPedido = p.IdPedido 
    LEFT JOIN dhl_ciudades AS ciudades ON ciudades.idCiudad = pd.Poblacion 
    LEFT JOIN dhl_estados AS estados ON estados.IdEstado = dir.IdProvincia 
    LEFT JOIN paises AS pa ON pa.IdPais = pd.IdPais         
    LEFT JOIN provincias AS pv ON pv.IdProvincia = pd.IdProvincia 
    LEFT JOIN estadospedidos AS ep ON ep.IdEstadoPedido = p.IdEstadoPedidos 
    LEFT JOIN (SELECT SUM(Importe) AS Importe, IdPedido FROM bonos_x_pedidos AS bxp GROUP BY bxp.IdPedido) bonos ON bonos.IdPedido = p.IdPedido 
    WHERE p.IdEstadoPedidos IN (2,7,9) 
    AND cam.IdCampania IN (".mysql_real_escape_string($IdCampania).") 
    AND dir.TipoDireccion = 0");
    
    $DatosProductos = $conexion->DevolverQuery();    

	//
	// the next lines demonstrate the generation of the Excel stream
	//
	xlsBOF();   // begin Excel stream

	xlsWriteLabel(0,0,"C�digo Cliente*");  // A1
	xlsWriteLabel(0,1,"Nombre Cliente*");  // B1
	xlsWriteLabel(0,2,"Calle*");  // C1
	xlsWriteLabel(0,3,"N�mero Exterior*");  // D1
	xlsWriteLabel(0,4,"N�mero Interior");  // E1
	xlsWriteLabel(0,6,"Codigo Postal");  // G1
	xlsWriteLabel(0,7,"Telefono 1");  // H1
	xlsWriteLabel(0,8,"Telefono 2");  // I1
	xlsWriteLabel(0,9,"Municipio");  // J1
	xlsWriteLabel(0,10,"Ciudad");  // K1
	xlsWriteLabel(0,11,"Estado");  // L1
	xlsWriteLabel(0,12,"Pais");  // M1
	xlsWriteLabel(0,13,"Fecha Pedido");  // N1
	xlsWriteLabel(0,14,"N�mero de Pedido");  // O1
	xlsWriteLabel(0,15,"C�digo de Cliente");  // P1
	xlsWriteLabel(0,16,"Cod. Articulo");  // Q1
	xlsWriteLabel(0,17,"Art�culo");  // R1
	xlsWriteLabel(0,18,"Cantidad");  // S1
	xlsWriteLabel(0,19,"Precio");  // T1
	xlsWriteLabel(0,20,"Bono");  // U1
	xlsWriteLabel(0,21,"Gasto Envio");  // V1

    $j = 1;
    //Filas
    foreach($DatosProductos AS $item){
    
    	if(isset($pid) && $pid != $item[idpedido]) {
			xlsWriteNumber(($j-1),20,$item[bono]);  // S1
			xlsWriteLabel(($j-1),21,$item[gastos]);  // T1
    	}
    
        //Datos
		xlsWriteNumber($j,0,$item[idUsuario]);  // A1
		xlsWriteLabel($j,1,$item[nombre]." ".$item[apellido]);  // B1
		xlsWriteLabel($j,2,$item[domicilio]);  // C1 
		xlsWriteLabel($j,3,$item[numero]);  // D1
		xlsWriteLabel($j,4,$item[piso]." ".$item[puerta]);  // E1
		xlsWriteNumber($j,6,$item[cp]);  // G1
		xlsWriteLabel($j,7,$item[codtelefono]." ".$item[telefono]);  // H1
		xlsWriteLabel($j,8,"");  // I1
		xlsWriteLabel($j,9,"");  // J1
		xlsWriteLabel($j,10,$item[ciudad]);  // K1
		xlsWriteLabel($j,11,$item[estado]);  // L1
		xlsWriteLabel($j,12,"M�xico");  // M1
		xlsWriteLabel($j,13,$item[fecha]);  // N1
		xlsWriteNumber($j,14,$item[idpedido]);  // O1
		xlsWriteNumber($j,15,$item[idusuario]);  // P1
		xlsWriteLabel($j,16,$item[referencia]);  // Q1
		xlsWriteLabel($j,17,$item[articulo]);  // R1
		xlsWriteNumber($j,18,$item[cantidad]);  // S1
		xlsWriteLabel($j,19,$item[precio]);  // T1
		
		$pid = $item[idpedido];
 
		$j++;
    }
    
    $item = $DatosProductos[(count($DatosProductos) -1)];
	xlsWriteNumber(($j-1),20,$item[bono]);  // S1
	xlsWriteLabel(($j-1),21,$item[gastos]);  // T1
	
	xlsEOF(); // close the stream
}
catch(Exception $e){
    echo $e;
}?>