<?
     header("Content-Description: File Transfer");
   	 header("Content-Type: application/force-download");
     header("Content-Disposition: attachment; filename=".date("Ymd")."_pedidos.csv");
   

try
{
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    ValidarUsuarioLogueadoBo(9);
}
catch(ACCESOException $e)
{
?>  
    <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
<?
    exit;
}    
try{
///NOTA DE PEDIDO!

	//Inicializo Variables
	
	//Numero de Campania
	$IdCampania = $_GET[IdCampania];
	$DatosAtributos = array();
	$DatosProductos = array();
		
	$conexion = Conexion::nuevo();
	//$conexion = new Conexion();
	/*
	$conexion->setQuery('SELECT p.IdPedido, pd.Apellido, pd.Nombre, DATE_FORMAT(p.Fecha, "%e-%c-%Y") AS Fecha, pd.NroDoc, CONCAT(pd.codTelefono,"-",pd.Telefono) AS Telefono,
CONCAT(pd.celucodigo,"-",pd.celunumero) AS Celuluar, us.NombreUsuario AS Email,
CONCAT(pd.Domicilio," ",pd.Numero," ",IF(pd.Piso NOT LIKE "",CONCAT("P",pd.Piso),"")," ",pd.Puerta) AS Direccion,
pd.Poblacion AS Localidad, pv.Nombre AS Provincia, pd.CP, pa.Nombre AS Pais, cam.Nombre AS Campania, ep.Descripcion AS EstadoDelPedido,
pro.Referencia, pxp.Cantidad, pro.Nombre as Producto, pxp.Precio AS PU, pxp.IdCodigoProdInterno, p.GastosEnvio, bonos.Importe AS Bono, p.Total, IF(us.IdPerfil <> 1,"Si","No") AS PedidoInterno
FROM pedidos AS p
JOIN usuarios AS us ON us.IdUsuario = p.IdUsuario
JOIN campanias AS cam ON cam.IdCampania = p.IdCampania
JOIN productos_x_pedidos AS pxp ON p.IdPedido = pxp.IdPedido
JOIN productos_x_proveedores AS pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
JOIN productos_x_categorias AS pxc ON pxpr.IdProducto = pxc.IdProducto
JOIN categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
JOIN productos AS pro ON pro.IdProducto = pxpr.IdProducto
JOIN pedidos_direcciones AS pd ON pd.IdPedido = p.IdPedido
LEFT JOIN paises AS pa ON pa.IdPais = pd.IdPais
LEFT JOIN provincias AS pv ON pv.IdProvincia = pd.IdProvincia
LEFT JOIN estadospedidos AS ep ON ep.IdEstadoPedido = p.IdEstadoPedidos
LEFT JOIN (SELECT SUM(Importe) AS Importe, IdPedido FROM bonos_x_pedidos AS bxp GROUP BY bxp.IdPedido)bonos ON bonos.IdPedido = p.IdPedido
WHERE p.IdEstadoPedidos IN (2,7,9) AND cam.IdCampania IN ('.$IdCampania.')');
	*/
	
	//Primer o segundo grupo de envios?
	if(isset($_GET['segundo']))
		$grupo = ' AND p.Fecha >= DATE_ADD(cam.FechaCorte, INTERVAL 1 DAY)';
	else if(isset($_GET['primero']))
		$grupo = ' AND (p.Fecha < DATE_ADD(cam.FechaCorte, INTERVAL 1 DAY) OR cam.FechaCorte IS NULL)';
	else
		$grupo = '';

	$conexion->setQuery("SELECT DISTINCT(CONCAT(pxp.IdPedido, pxp.IdCodigoProdInterno)) as uid, pro.Referencia, SUM(pxp.Cantidad) as Cantidad, pro.Nombre, 
	pxp.IdCodigoProdInterno, pro.PCompra as PCU, SUM(pro.PCompra*pxp.Cantidad) AS CostoTotal
	FROM pedidos AS p
	JOIN campanias AS cam ON cam.IdCampania = p.IdCampania
	JOIN productos_x_pedidos AS pxp ON p.IdPedido = pxp.IdPedido
	JOIN productos_x_proveedores AS pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
	JOIN productos AS pro ON pro.IdProducto = pxpr.IdProducto
	WHERE p.IdEstadoPedidos IN (2,7,9) AND cam.IdCampania IN (".mysql_real_escape_string($IdCampania).") ".$grupo."
	GROUP By pxp.IdCodigoProdInterno");
	
	
	$DatosProductos = $conexion->DevolverQuery();
	
	//Extraigo Los IdCodigoProdInterno
	foreach($DatosProductos AS $i => $item){
		$item["IdCodigoProdInterno"] = intval($item["IdCodigoProdInterno"]);
		if($item["IdCodigoProdInterno"]>0) $Codigos[] = $item["IdCodigoProdInterno"];
		
		unset($DatosProductos[$i]['uid']);
	}
	
	$Codigos = array_unique($Codigos);
	
	//Busco los atributos para esos codigos
	if(strlen(implode(",",$Codigos))>0){	
	
		$conexion->setQuery("SELECT pxa.IdCodigoProdInterno, at.Nombre as Atributo, pxa.Valor
		FROM productos_x_atributos AS pxa
		JOIN atributos_x_clases AS axc ON axc.idAtributoClase = pxa.idAtributoClase
		JOIN atributos AS at ON at.IdAtributo = axc.IdAtributo
		WHERE  pxa.IdCodigoProdInterno IN (".implode(",",$Codigos).") ORDER BY at.Nombre");		
		$Atributos = $conexion->DevolverQuery();		
		$camposAtributos = array();
		
		//Armo un bonito array
		foreach($Atributos as $atrib){
			$DatosAtributos[$atrib["IdCodigoProdInterno"]][$atrib["Atributo"]] = $atrib["Valor"];
			$camposAtributos[] = $atrib["Atributo"];
		}
				
		/*
		//Armo un string con los nombres de columna
		$camposAtributos = array_unique($camposAtributos);
		
		if(count($camposAtributos)>0){
			$camposAtributos = implode(";",$camposAtributos).";";
		}else{
			$camposAtributos = "";
		}*/
	}
	
	/*
	Cargo atributos para cada array.
	if(count($DatosAtributos)>0){
		foreach($DatosProductos as $key=>$value){
			array_push($DatosProductos[$key],$DatosAtributos[$value["IdCodigoProdInterno"]]);
		}
	}	
	*/
	
	
	//ARMO EL CSV
	$datospdf = "";
		
	//$datospdf .="Referenica;Cantidad;Producto;".$camposAtributos.";Precio Unitario;Total\n";
	
	//Columnas
	$i=0;
	foreach($DatosProductos[0] AS $key=>$value){
		if($i!=0) $datospdf .=";";
		$datospdf .=$key;
		$i++;
	}
	$datospdf .="\n";
	
	//$datospdf .="Referenica;Cantidad;Producto;;;;Precio Unitario;Total\n";
	$datospdf = str_replace("IdCodigoProdInterno;",";;;",$datospdf);
	
	//Filas
	foreach($DatosProductos AS $item){
		//Primeros datos
		$j = 0;
		foreach($item as $key => $value){
			if($j!=0) $datospdf .= ";";
			if($key != "IdCodigoProdInterno"){
				//INSERT CUALQUIER OTRO CAMPO
				$datospdf .= $value;
			}else{				
				// INSERTO ATRIBUTOS
				$k=0;
				
				foreach($DatosAtributos[$item["IdCodigoProdInterno"]] as $aa){
					if($k!=0) $datospdf .= ";";
					$datospdf .= $aa;
					$k++;
				}
				
				while($k<3){
					if($k!=0) $datospdf .=";";
					$k++;
				}								
			}
			$j++;
		}
		
		$datospdf .="\n";
		
		//$datospdf .=$item["Referencia"].";".$item["Cantidad"].";".$item["Nombre"].";";		
						
		//$datospdf .=$item["PCU"].";".$item["CostoTotal"]."\n";	
	}	
}
catch(Exception $e){
	echo Conexion::nuevo()->DevolverLog();
	throw $e;
}?>
<?=$datospdf?>