<?
	header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=".date("Ymd")."_inventario_siigo.csv");

 try
{
	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	ValidarUsuarioLogueadoBo(9);
 
}
catch(ACCESOException $e)
{
?>  
    <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
<?
    exit;
}    
try{
///NOTA DE PEDIDO!
	
	
	
	//Inicializo Variables
	
	//Numero de Campania
	$IdCampania = $_GET['IdCampania'];
	$DatosAtributos = array();
	$DatosProductos = array();
		
	$conexion = Conexion::nuevo();

	$conexion->setQuery('
	SELECT p.*, m.Nombre as Marca, c.FechaInicio
	FROM productos p
	LEFT JOIN marcasproductos m ON m.IdMarca = p.IdMarca
	JOIN productos_x_categorias pxc ON pxc.IdProducto = p.IdProducto
	JOIN categorias cat ON cat.IdCategoria = pxc.IdCategoria
	JOIN campanias c ON c.IdCampania = cat.IdCampania AND c.IdCampania = '.mysql_real_escape_string($IdCampania).'
	GROUP BY p.IdProducto
	');
	
	$DatosProductos = $conexion->DevolverQuery();	
	
	foreach($DatosProductos as $p) {

		echo "000,"; //línea
		echo "0000,"; //grupo
		printf("%06d,", $p['IdProducto']);
		echo '"'.substr($p['Descripcion'], 0, 50).'",';
		echo '"'.substr($p['Referencia'], 0, 60).'",';
		echo "P,"; //grupo
		
		$precio = explode('.', $p['PVenta']);
		printf("%09d,", $precio[0]);
		printf("%02d,", $precio[1]);
		
		for($i = 1; $i <= 11; $i++) {
			echo str_repeat("0", 9).',';
			echo str_repeat("0", 2).',';
		}
		
		echo str_repeat("0", 20).','; //codigo de barras
		echo "16,00,"; //iva
		echo str_repeat("0", 14).','; //stock max
		echo str_repeat("0", 14).','; //stock min
		echo str_repeat("0", 3).','; //tiempo de reposición
		echo "uni,"; //tipo de unidad
		echo str_repeat("0", 13).','; //código de equivalencia
		echo ","; //descripción de equivalencia
		echo '"'.$p['Marca'].'",';
		echo str_repeat("0", 6).','; //ubicación
		echo "N,"; //ajustable

		echo str_repeat("0", 13).','; //nit proveedor
		echo date("Ymd", strtotime($p['FechaInicio'])).','; //nit proveedor
		$precio = explode('.', $p['PCompra']);
		printf("%011d,", $precio[0]);
		printf("%02d,", $precio[1]);

		echo str_repeat("0", 13).','; //nit proveedor
		echo str_repeat("0", 8).','; //fecha
		echo str_repeat("0", 13).','; //precio

		echo str_repeat("0", 13).','; //nit proveedor
		echo str_repeat("0", 8).','; //fecha
		echo str_repeat("0", 13).','; //precio

		echo str_repeat("0", 13).','; //nit proveedor
		echo str_repeat("0", 8).','; //fecha
		echo str_repeat("0", 13).','; //precio
		
		//Factores
		echo ',';
		echo ',';		
		echo str_repeat("0", 10).',';
		echo str_repeat("0", 5).',';
		
		echo ',';
		echo ',';		
		echo str_repeat("0", 10).',';
		echo str_repeat("0", 5).',';
		
		echo ',';
		echo ',';		
		echo str_repeat("0", 10).',';
		echo str_repeat("0", 5).',';
		
		echo ',';
		echo ',';		
		echo str_repeat("0", 10).',';
		echo str_repeat("0", 5).',';
		
		echo ',';
		echo ',';		
		echo str_repeat("0", 10).',';
		echo str_repeat("0", 5).',';

		printf("%011d", $precio[0]);
		printf("%02d,", $precio[1]);
		echo str_repeat("0", 13).',';
		echo str_repeat("0", 13).',';
		echo str_repeat("0", 13).',';
		echo str_repeat("0", 13).',';
		echo str_repeat("0", 13).',';
		echo str_repeat("0", 13).',';
		echo str_repeat("0", 13).',';
		echo str_repeat("0", 13).',';
		echo str_repeat("0", 13).',';
		echo str_repeat("0", 13).',';
		echo str_repeat("0", 13).',';
		echo str_repeat("0", 13).',';

		echo 'S,'; //iva incluido
		echo '1,'; //tarifa
		echo str_repeat("0", 13).',';
		echo 'N,'; //manejo de clasificaciones
		echo 'N,'; //manejo de conversión
		echo str_repeat("0", 3).','; //impuesto consumo
		echo str_repeat("0", 2).',';
		echo str_repeat("0", 3).','; //impuesto deporte
		echo str_repeat("0", 2).',';
		echo str_repeat("0", 13).','; //peso
		echo 'S,'; //discrimina iva
		echo 'S,'; //activo
		echo 'N'; //seriales

		echo "\n";
		
	}
	
}
catch(Exception $e){
	echo Conexion::nuevo()->DevolverLog();
	throw $e;
}?>