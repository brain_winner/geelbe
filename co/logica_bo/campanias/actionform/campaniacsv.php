<?php
    header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=".date("Ymd")."_pedidos.csv");

    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("pedidos"));
   try
    {
        ValidarUsuarioLogueadoBo(9);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }    
    try
    {                                                            
        $Tabla = dmPedidos::getPedidosAceptadosByCamp($_GET[IdCampania]);

      echo("Numero de Pedido;Apellido y nombre;Fecha; Nombre campa�a;Estado del Pedido;Costo de Envio;Sub-Total de la Compra;Cr�ditos utilizados;Total de la Compra;DNI;Telefono;Celular;Email;Direccion;Localidad;Provincia;Codigo Postal;Pais;Codigo Producto; Producto; Cantidad; Precio;;Atributos\n");                                               
        foreach($Tabla as $Pedido)
        {   
            
            $objPedido = dmPedidos::getPedidoById($Pedido["Pedido"]);
            $Bonos = dmMisCreditos::VIEWgetByIdPedido($Pedido["Pedido"]);
            $objUsuario = dmMicuenta::GetUsuario($objPedido->getIdUsuario());
            $InfoPedido = dmPedidos::getInfoPedido($Pedido["Pedido"]);
            $BonoUsado = $Bonos[0]["Bono"]; 
            foreach($objPedido->getProductos() as $Articulo)
              {
                echo ($Pedido["Pedido"] . ";" . $Pedido["Apellido y nombre"] . ";" . $Pedido["Fecha"] . ";" . $Pedido["Nombre campa�a"]. ";" . $Pedido["Descripcion"] . ";");
                                
                echo(Moneda($objPedido->getGastosEnvio()) . ";". Moneda($objPedido->getTotal()+$BonoUsado) . ";" );  
                echo(Moneda($BonoUsado) . ";" . Moneda($objPedido->getTotal()) . ";");
                echo ($objUsuario->getDatos()->getNroDoc() . ";");
                echo ($objUsuario->getDatos()->getCodTelefono()." - ".$objUsuario->getDatos()->getTelefono() . ";");
                echo ($objUsuario->getDatos()->getcelucodigo()." - ".$objUsuario->getDatos()->getcelunumero() . ";");
                echo ($objUsuario->getNombreUsuario() .";");
                echo ($objPedido->getDireccion()->getDomicilio() ." ". $objPedido->getDireccion()->getNumero() ." ");
                echo ($objPedido->getDireccion()->getPiso() ." ");
                echo ($objPedido->getDireccion()->getPuerta() . ";" );
                echo($objPedido->getDireccion()->getPoblacion() . ";");
                 if($objPedido->getDireccion()->getIdProvincia() != "")
                {
                    $Prov = dmMicuenta::GetProvinciasById($objPedido->getDireccion()->getIdProvincia());
                    echo ($Prov[0]["Nombre"] . ";");
                }                            
                echo($objPedido->getDireccion()->getCP() . ";");
                if($objPedido->getDireccion()->getIdPais() != "")
                {
                    $Pais = dmMicuenta::GetPaisesById($objPedido->getDireccion()->getIdPais());
                    echo ($Pais[0]["Nombre"] . ";");
                } 
                
                  $objPP = dmProductos::getByIdCodigoProdInterno($Articulo->getIdCodigoProdInterno());
                  $objProducto = dmProductos::getById($objPP->getIdProducto());
                  $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($Articulo->getIdCodigoProdInterno());

                  echo ($objProducto->getReferencia() . ";");
                  echo($objProducto->getNombre() . ";");
                  echo($Articulo->getCantidad() . ";" . Moneda($Articulo->getPrecio() * $Articulo->getCantidad(),2) . ";");
                  foreach($Atributos as $atributo)
                  {
                      echo ($atributo["Nombre"].";".$atributo["Valor"] . ";");
                  }
                  echo("\n");                 
              }
        }
        
    }
    catch(exception $e)
    {
        print $e->getMessage();
    }
?>