<?php
    if($_POST)
    {
        ob_start();
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        extract($_POST);
        
        
        try
        {   
            $IdCampania = ($txtIdCampania == "0") ? 0 : Aplicacion::Decrypter($txtIdCampania);
            $objCampania = new MySQL_Campania();
            $objCampania->setDescripcion($txtDescripcion);
            $objCampania->setFechaFin($FechaFin["cbxAnio"]."-".$FechaFin["cbxMes"]."-".$FechaFin["cbxDia"]." ".$FechaFin["txtHora"].":".$FechaFin["txtMin"]);
            $objCampania->setFechaInicio($FechaInicio["cbxAnio"]."-".$FechaInicio["cbxMes"]."-".$FechaInicio["cbxDia"]." ".$FechaInicio["txtHora"].":".$FechaInicio["txtMin"]);
            $objCampania->setIdCampania($IdCampania);
            $objCampania->setMaxProd($txtMaxProd);
            $objCampania->setNombre($txtNombre);
            $objCampania->setDescuentoMax($txtDescuentoMax);
            $objCampania->setColeccion($coleccion);
            $objCampania->setTiempoEntregaIn($TiempoEntregaIn);
            $objCampania->setTiempoEntregaFn($TiempoEntregaFn);
            $objCampania->setTiempoEntregaIn2($TiempoEntregaIn2);
            $objCampania->setTiempoEntregaFn2($TiempoEntregaFn2);
            $objCampania->setFechaCorte($FechaCorte);
            $objCampania->setVisibleDesde($VisibleDesde["cbxAnio"]."-".$VisibleDesde["cbxMes"]."-".$VisibleDesde["cbxDia"]." ".$VisibleDesde["txtHora"].":".$VisibleDesde["txtMin"]);
            $objCampania->setMinMontoEnvioGratis($txtMinMontoEnvioGratis);
            $objCampania->setWelcome($welcome);
            $objCampania->setIdOrdenamiento($cbxOrdenamiento);

            foreach($chkPropiedad as $Id => $propiedad)
            {
                $objCampania->addPropiedad($Id);
            }
            
            if(isset($chkEliminar))
            {
                foreach($chkEliminar as $chk)
                {
                    @unlink(Aplicacion::getCampaniasImagenesRoot($IdCampania)."imagenes/".$chk);
                }
            }   
            if(isset($_FILES["txtTrailer"]))
            {
                $ext = substr(strrchr($_FILES["txtTrailer"]["name"], "."), 1);
                $objCampania->setTrailer_Origen($_FILES["txtTrailer"]["tmp_name"]);
                $objCampania->setTrailer_Destino("trailer.".$ext);
            }
            if(isset($_FILES["txtLogo"]))
            {
                $ext = substr(strrchr($_FILES["txtLogo"]["name"], "."), 1);
                $objCampania->setLogo_Origen($_FILES["txtLogo"]["tmp_name"]);
                $objCampania->setLogo_Destino("logo.".$ext);
                $objCampania->setLogo2_Destino("logo2.".$ext);
            }
           /* if(isset($_FILES["txtLogo2"]))
            {
                $ext =	strtolower(substr(strrchr($_FILES["txtLogo2"]["name"], "."), 1));
                if($ext == 'png') {
    	            $objCampania->setLogo2_Origen($_FILES["txtLogo2"]["tmp_name"]);
	                $objCampania->setLogo2_Destino("logo2.".$ext);
	            }
            }*/
            if(isset($_FILES["txtImagen"]))
            {
                $ext = substr(strrchr($_FILES["txtImagen"]["name"], "."), 1);
                $objCampania->setImagen_Origen($_FILES["txtImagen"]["tmp_name"]);
                $objCampania->setImagen_Destino("imagen.".$ext);
            }
            if(isset($_FILES["txtGreyImagen"]))
            {
                $ext = explode(".", $_FILES["txtGreyImagen"]["name"]);
                $objCampania->setGreyImagen_Origen($_FILES["txtGreyImagen"]["tmp_name"]);
                $objCampania->setGreyImagen_Destino("imagen.".$ext[1]);
            }
            if(isset($_FILES["txtWelcome"]))
            {
                $ext = explode(".", $_FILES["txtWelcome"]["name"]);
                $objCampania->setWelcome_Origen($_FILES["txtWelcome"]["tmp_name"]);
                $objCampania->setWelcome_Destino("welcome.".$ext[1]);
            }
        	if(isset($_FILES["txtVidrieraImagen"]))
            {
                $ext = explode(".", $_FILES["txtVidrieraImagen"]["name"]);
                $objCampania->setVidrieraImagen_Origen($_FILES["txtVidrieraImagen"]["tmp_name"]);
                $objCampania->setVidrieraImagen_Destino("banner_camp-on.".$ext[1]);
            }
        	if(isset($_FILES["txtVidrieraOffImagen"]))
            {
                $ext = explode(".", $_FILES["txtVidrieraOffImagen"]["name"]);
                $objCampania->setVidrieraOffImagen_Origen($_FILES["txtVidrieraOffImagen"]["tmp_name"]);
                $objCampania->setVidrieraOffImagen_Destino("banner_camp-off.".$ext[1]);
            }
            if(isset($_FILES["txtNewsBienvenida"]))
            {
                $ext = substr(strrchr($_FILES["txtNewsBienvenida"]["name"], "."), 1);
                $objCampania->setNews1_Origen($_FILES["txtNewsBienvenida"]["tmp_name"]);
                $objCampania->setNews1_Destino("news1.".$ext);
            }
            if(isset($_FILES["txtNewsDespedida"]))
            {
                $ext = substr(strrchr($_FILES["txtNewsDespedida"]["name"], "."), 1);
                $objCampania->setNews2_Origen($_FILES["txtNewsDespedida"]["tmp_name"]);
                $objCampania->setNews2_Destino("news2.".$ext);
            }
            if(isset($_FILES["txtNewsUltimos"]))
            {
                $ext = substr(strrchr($_FILES["txtNewsUltimos"]["name"], "."), 1);
                $objCampania->setNews3_Origen($_FILES["txtNewsUltimos"]["tmp_name"]);
                $objCampania->setNews3_Destino("news3.".$ext);
            }
            dmCampania::save($objCampania);
            
            if($IdCampania != 0) {
	            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->clean(
				    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
				    array('group_cache_catalogo_campania_'.$IdCampania)
				);
            }
            
            ?>
                <script type="text/javascript">
                    window.irVolver();
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                    window.ErrorMSJ(<?=$e->getNumeroSQLERROR()?>);
                </script>
            <?
        }
        ob_flush();
    }
?>