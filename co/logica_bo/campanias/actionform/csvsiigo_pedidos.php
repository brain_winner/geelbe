<?
	header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=".date("Ymd")."_pedidos_siigo.csv");

 try
{
	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	ValidarUsuarioLogueadoBo(9);
 
}
catch(ACCESOException $e)
{
?>  
    <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
<?
    exit;
}    
try{
///NOTA DE PEDIDO!
	
	
	
	//Inicializo Variables
	
	//Numero de Campania
	$IdCampania = $_GET['IdCampania'];
	$DatosAtributos = array();
	$DatosProductos = array();
		
	$conexion = Conexion::nuevo();

	$conexion->setQuery('
	SELECT p.*, da.NroDoc, pd.IdProducto, pp.Cantidad
	FROM pedidos p
	JOIN productos_x_pedidos pp ON pp.IdPedido = p.IdPedido
	JOIN productos_x_proveedores pr ON pr.IdCodigoProdInterno = pp.IdCodigoProdInterno
	JOIN productos pd ON pd.IdProducto = pr.IdProducto
	JOIN usuarios u ON u.IdUsuario = p.IdUsuario
	JOIN datosusuariospersonales da ON da.IdUsuario = u.IdUsuario
	JOIN datosusuariosdirecciones di ON di.IdUsuario = u.IdUsuario
	WHERE p.IdCampania = '.mysql_real_escape_string($IdCampania).'
	');
	
	$DatosProductos = $conexion->DevolverQuery();	
	
	foreach($DatosProductos as $p) {

		echo "L,";
		echo "001,";
		printf("%011d,", $p['NroDoc']);
		printf("%5d,", ''); //secuencia
		echo str_repeat('0', 13).','; //nit
		echo '000,';
		echo str_repeat('0', 10).','; //cuenta contable
		printf("%013d,", $p['IdProducto']);
		echo date("Ymd", strtotime($p['Fecha'])).',';						
		echo str_repeat('0', 4).','; //centro de costo
		echo str_repeat('0', 3).','; //subcentro de costo
		echo ','; //descripción
		echo 'C,'; //crédito
		echo str_repeat('0', 13).','; //valor del mov
		echo str_repeat('0', 2).',';
		echo str_repeat('0', 15).','; //base de retención
		echo str_repeat('0', 4).','; //código del vendedor
		echo str_repeat('0', 4).','; //código de la ciudad
		echo str_repeat('0', 3).','; //código de la zona
		echo str_repeat('0', 4).','; //código de la bodega
		echo str_repeat('0', 3).','; //código de la ubicación
		printf("%010d,", $p['Cantidad']); //cantidad
		echo str_repeat('0', 5).','; //cantidad (decimales)
		echo ','; //tipo de documento
		echo ','; //cod comprobante cruce
		echo str_repeat('0', 11).','; //num doc cruce
		echo str_repeat('0', 3).','; //secuencia doc cruce
		echo str_repeat('0', 8).','; //vencimiento doc cruce
		echo str_repeat('0', 4).','; //forma de pago
		echo str_repeat('0', 2).','; //banco
		echo ','; //tipo doc
		echo str_repeat('0', 3).','; //cod comprobante de pedido
		echo str_repeat('0', 11).','; //num comprobante de pedido
		echo str_repeat('0', 3).','; //secuencia de pedido
		echo str_repeat('0', 2).','; //cod de moneda
		echo str_repeat('0', 8).','; //tasa de cambio
		echo str_repeat('0', 7).','; 
		echo str_repeat('0', 13).','; //movimiento en extranjera
		echo str_repeat('0', 2).','; 
		echo str_repeat('0', 3).','; //nómina
		echo str_repeat('0', 9).','; //cantidad de pago
		echo str_repeat('0', 2).','; 
		echo str_repeat('0', 2).','; //porcentaje de descuento
		echo str_repeat('0', 2).','; 
		echo str_repeat('0', 11).','; //valor de descuento
		echo str_repeat('0', 2).','; 
		echo str_repeat('0', 2).','; //porcentaje de cargo
		echo str_repeat('0', 2).','; 
		echo str_repeat('0', 11).','; //valor de cargo
		echo str_repeat('0', 2).','; 
		echo str_repeat('0', 2).','; //porcentaje de iva
		echo str_repeat('0', 2).','; 
		echo str_repeat('0', 11).','; //valor de iva
		echo str_repeat('0', 2).','; 
		echo 'N,'; //indicador de nómina
		echo '1,'; //número de pago
		echo str_repeat('0', 11).','; //número de cheque
		echo 'N,'; //S
		echo ','; //computador
		echo ','; //anulado
		echo 'N,'; //devolución
		echo str_repeat('0', 2).','; //crédito tributario
		echo ','; //comprobante del proveedor
		echo ','; //doc del proveedor
		echo ','; //prefijo doc del proveedor
		echo str_repeat('0', 8).','; //fecha doc del proveedor
		echo str_repeat('0', 13).','; //precio unitario local
		echo str_repeat('0', 5).',';
		echo str_repeat('0', 13).','; //precio unitario extranjero
		echo str_repeat('0', 5).',';
		echo ','; //tipo de movimiento
		echo str_repeat('0', 3).','; //veces a depreciar el activo
		echo str_repeat('0', 2).','; //secuencia de transacción
		echo str_repeat('0', 10).','; //autorización imprenta
		echo ','; //secuencia marcada como iva para el coa
		echo str_repeat('0', 3).','; //número de caja
		echo str_repeat('0', 12).','; //puntos obtenidos
		echo str_repeat('0', 2).',';
		echo str_repeat('0', 10).','; //cantidad dos
		echo str_repeat('0', 5).',';
		echo str_repeat('0', 10).','; //cantidad alterna dos
		echo str_repeat('0', 5).',';
		echo 'L,'; //método de depreciación
		echo str_repeat('0', 13).','; //cantidad de factor de conversión
		echo str_repeat('0', 5).',';
		echo '0,'; //operador de factor de conversión
		echo str_repeat('0', 5).','; //factor de conversión
		echo str_repeat('0', 5).',';
		echo str_repeat('0', 8).','; //caducidad
		echo str_repeat('0', 2).','; //ice
		echo str_repeat('0', 5).','; //retención
		echo ','; //clase retención
		echo str_repeat('0', 4).','; //motivo devolución
		echo ','; //datos m/cia consignacion
		echo ','; //número comprobante fiscal propio
		echo ','; //número comprobante fiscal proveedor
		echo ','; //tipo de letra
		echo ','; //estado de la letra
		echo str_repeat('0', 13).','; //valor del movimiento local
		echo str_repeat('0', 5).',';
		echo str_repeat('0', 13).','; //valor del movimiento extranjero
		echo str_repeat('0', 5).',';
		echo str_repeat('0', 3).','; //código medio de pago
		echo str_repeat('0', 13).','; //base de transacción
		echo str_repeat('0', 2).',';
		echo str_repeat('0', 4); //actividades flujo

		echo "\n";
		
	}
	
}
catch(Exception $e){
	echo Conexion::nuevo()->DevolverLog();
	throw $e;
}?>