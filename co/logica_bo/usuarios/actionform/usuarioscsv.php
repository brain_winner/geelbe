<?php
    header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=".date("Ymd")."_usuarios_activos.csv");
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");   
    try
    {
        ValidarUsuarioLogueadoBo(21);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 

    Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
    try
    {
        $Tabla = dmUsuario::ObtenerRegistrados();
        echo("Nombre;Apellido; Nombre y Apellido; Email;\n");                                             
        foreach($Tabla as $UsuarioActivo)
        {
                echo $UsuarioActivo["Nombre"] . "; " . $UsuarioActivo["Apellido"] . "; " . $UsuarioActivo["Nombre"] . " " . $UsuarioActivo["Apellido"]. "; " .  $UsuarioActivo["NombreUsuario"] . "\n";
        }
    }
    catch(exception $e)
    {
        print $e->getMessage();
    }
?>