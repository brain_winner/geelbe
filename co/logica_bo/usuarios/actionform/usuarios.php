<?php
    if($_POST)
    {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("perfiles"));
        extract($_POST);
        try
        {   
            $objUsuario = new MySQL_Usuario();
            $objUsuario->setClave($txtClave);
                $objDatos = new DatosUsuariosPersonales();
                $objDatos->setApellido(ucwords(strtolower($txtApellido)));
                $objDatos->setFechaNacimiento(date("Y-m-d", time()));
                $objDatos->setIdTipoUsuario(1);
                $objDatos->setIdTratamiento("");
                $objDatos->setNombre(ucwords(strtolower($txtNombre)));
                $objDatos->esCompleto(0);
            $objUsuario->setDatos($objDatos);
            $objUsuario->setes_Activa(1);
            $objUsuario->setes_Provisoria(0);
            $objUsuario->setFechaIngreso(date("Y-m-d", time()));
            $objUsuario->setIdPerfil($cbxPerfil);
            $objUsuario->setIdUsuario($txtIdUsuario);
            $objUsuario->setNombreUsuario($txtEmail);
            $secciones=dmPerfiles::getById($objUsuario->getIdPerfil());
            $objUsuario->setSecciones($secciones);
            dmUsuario::save($objUsuario);
            ?>
                <script type="text/javascript">
                    window.parent.irVolver();
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                    window.parent.ErrorMSJ("<?=$e->getNumeroSQLERROR()?>", "");
                </script>
            <?
        }
        ob_flush();
    }
?>