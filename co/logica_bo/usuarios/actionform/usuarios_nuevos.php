<?php
	ini_set('memory_limit', '256M');

	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	
    header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=".date("Ymd")."_".$confRoot[1]."_lista_ems_".$_GET["offset"]."-".($_GET["offset"]+$_GET["max_results"]).".csv");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");   
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
    try
    {
        ValidarUsuarioLogueadoBo(21);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 

    Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
    try
    {	
		$oConexion = Conexion::nuevo();
        $oConexion->Abrir();
        $oConexion->setQuery("SELECT U.NombreUsuario, UDP.Nombre, UDP. Apellido, U.IdUsuario, YEAR(UDP.FechaNacimiento) as AnioNacimiento, MONTH(UDP.FechaNacimiento) as MesNacimiento, DAY(UDP.FechaNacimiento) as DiaNacimiento, IF(UDP.idTratamiento=1, 'Hombre', 'Mujer') as Genero, U.FechaIngreso, p.Nombre as Provincia FROM usuarios U LEFT JOIN datosusuariospersonales UDP ON U.IdUsuario = UDP.IdUsuario LEFT JOIN datosusuariosdirecciones DUD ON U.IdUsuario = DUD.IdUsuario LEFT JOIN provincias p ON DUD.IdProvincia = p.IdProvincia WHERE (DUD.TipoDireccion=0 OR DUD.TipoDireccion IS NULL) AND U.es_activa=0 AND U.IdPerfil = 1 AND U.NombreUsuario LIKE '%@%' AND U.NombreUsuario NOT IN (select email from acciones_listas_ems where tipo_accion='B') ORDER BY U.FechaIngreso DESC LIMIT ".$_GET["offset"].",".$_GET["max_results"]);
        $Tabla = $oConexion->DevolverQuery();
        $oConexion->Cerrar();
        
        
        echo("Nombre;Apellido;Email;ID;Hash;Fecha;Sexo;Provincia;DiaNacimiento;MesNacimiento;AnioNacimiento\n");                                             
        foreach($Tabla as $UsuarioActivo)
        {
        
		 echo str_replace(";", "", html_entity_decode($UsuarioActivo["Nombre"])).";";
		 echo str_replace(";", "", html_entity_decode($UsuarioActivo["Apellido"])).";";
		 echo $UsuarioActivo["NombreUsuario"] .";";
		 echo $UsuarioActivo["IdUsuario"] .";";
		 echo Hash::getHash($UsuarioActivo["IdUsuario"].$UsuarioActivo["NombreUsuario"]) .";";
		 echo $UsuarioActivo["FechaIngreso"] . ";"; 
		 echo $UsuarioActivo["Genero"].";";
		 echo $UsuarioActivo["Provincia"].";";
		 echo $UsuarioActivo["DiaNacimiento"].";";
		 echo $UsuarioActivo["MesNacimiento"].";";
		 echo $UsuarioActivo["AnioNacimiento"]."\n";
        }
    }
    catch(exception $e)
    {
        print $e->getMessage();
    }
?>