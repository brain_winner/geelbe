<?php
	header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=".date("Ymd")."_usuarios_activos.csv");
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");   
    try
    {
        ValidarUsuarioLogueadoBo(21);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
        

    Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
    try
    {	
		$oConexion = Conexion::nuevo();
        $oConexion->Abrir();
        $oConexion->setQuery("SELECT U.NombreUsuario, UDP.Nombre, UDP. Apellido, U.IdUsuario, U.FechaIngreso FROM usuarios U INNER JOIN datosusuariospersonales UDP ON U.IdUsuario = UDP.IdUsuario WHERE U.es_activa=0 AND U.NombreUsuario LIKE '%@%' ORDER BY U.FechaIngreso");
        $Tabla = $oConexion->DevolverQuery();
        $oConexion->Cerrar();
        
        echo("Nombre;Apellido;Nombre y Apellido;Email;ID;Hash;Fecha\n");                                             
        foreach($Tabla as $UsuarioActivo)
        {
			echo $UsuarioActivo["Nombre"] . "; " . $UsuarioActivo["Apellido"] . ";" . $UsuarioActivo["Nombre"] . " " . $UsuarioActivo["Apellido"]. ";" .  $UsuarioActivo["NombreUsuario"] . ";" . $UsuarioActivo["IdUsuario"]  . ";" . generar_hash_md5($UsuarioActivo["IdUsuario"]) . ";" . $UsuarioActivo["FechaIngreso"]. "\n";
        }
    }
    catch(exception $e)
    {
        print $e->getMessage();
    }
?>