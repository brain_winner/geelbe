<?
	class MySQL_Usuario extends usuarios {
		
        public function getInsert() {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor) {
                if($propiedad != "_secciones") {
                    if($propiedad == "_idusuario") {
                        $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                        $strValores .=($valor != 0) ? chr(34).mysql_real_escape_string($valor).chr(34).", " : "NULL, ";
                    }
                    else
                    {
                        $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                        $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                        $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    }
                }
            }
            return "INSERT INTO usuarios (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        
        public function getDelete() {
            return "DELETE FROM usuarios WHERE IdUsuario = ".mysql_real_escape_string($this->getIdUsuario());
        }
        
        public function getInsertDatosPersonales() {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->getDatos()->__toArray();
            foreach ($Me as $propiedad => $valor) {
                if($propiedad != "_idusuario") {
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }

            }
            return "INSERT INTO datosusuariospersonales (IdUsuario, ".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".mysql_real_escape_string($this->getIdUsuario()).", ".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        
        public function getById($Id) {
            return "SELECT idusuario, nombreusuario, clave, es_provisoria, padrino, fechaingreso, idperfil, es_activa FROM usuarios WHERE IdUsuario = ".mysql_real_escape_string($Id);
        }
        
        public function getDatosById($Id) {
            $strCampos="";
            $Me = $this->getDatos()->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM datosusuariospersonales WHERE IdUsuario = ".mysql_real_escape_string($Id);
        }
    }
    
    class MySQL_Perfil extends perfiles
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_idusuario")
                {
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=($valor != 0) ? chr(34).mysql_real_escape_string($valor).chr(34).", " : "NULL, ";
                }
                else
                {
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
            }
            return "INSERT INTO perfiles (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
    }
?>