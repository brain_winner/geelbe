<?
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
    class dmUsuario
    {
        public static function DesactivarCuenta($idUsuario)
        {
            $oConexion = Conexion::nuevo();
            try
            {
            	//Guardar en lista
            	$objUsuario = dmUsuario::getByIdUsuario($idUsuario);

                $sexoUsuario = "";
		        if($objUsuario->getDatos()->getIdTratamiento() == 1) {
		        	$sexoUsuario = "Hombre";
		        } else if ($objUsuario->getDatos()->getIdTratamiento() == 2) {
		        	$sexoUsuario = "Mujer";
		        }
		        
		        $provinciaUsuario = "";
		        if($objUsuario->getDirecciones(0)->getIdProvincia() > 0) {
	                $oConexion->setQuery("SELECT Nombre FROM dhl_estados WHERE IdEstado = ".mysql_real_escape_string($objUsuario->getDirecciones(0)->getIdProvincia()));                
	                $DT = $oConexion->DevolverQuery();
	                $oConexion->Cerrar();
	                
	                $provinciaUsuario = $DT[0]["Nombre"];
		        }
            	
	            $oConexion->Abrir();
                $oConexion->setQuery('INSERT INTO acciones_listas_ems (email, nombre, apellido, tipo_accion, fecha_accion, accion_encolada, accion_commiteada, id_usuario, hash_usuario, fechanacimiento_usuario, sexo_usuario, provincia_usuario) 
            		VALUES ("'.htmlspecialchars(mysql_real_escape_string($objUsuario->getNombreUsuario()), ENT_QUOTES).'", "'.htmlspecialchars(mysql_real_escape_string($objUsuario->getDatos()->getNombre()), ENT_QUOTES).'", "'.htmlspecialchars(mysql_real_escape_string($objUsuario->getDatos()->getApellido()), ENT_QUOTES).'", "B", NOW(), 0, 0, '.
            			mysql_real_escape_string($objUsuario->getIdUsuario()).', "'.Hash::getHash($objUsuario->getIdUsuario().$objUsuario->getNombreUsuario()).'", "'.mysql_real_escape_string($objUsuario->getDatos()->getFechaNacimiento()).'", "'.
		            	htmlspecialchars(mysql_real_escape_string($sexoUsuario), ENT_QUOTES).'", "'.htmlspecialchars(mysql_real_escape_string($provinciaUsuario), ENT_QUOTES).'");');

	            $oConexion->EjecutarQuery();

	            
				$oConexion = Conexion::nuevo();
				$oConexion->setQuery('UPDATE usuarios SET mail_suscription = false where IdUsuario = '.$objUsuario->getIdUsuario().';');
				$oConexion->EjecutarQuery();
	            
				//Desactivar
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE usuarios SET es_activa = 1, Clave='XXXXXX', es_Provisoria=0 WHERE IdUsuario = " . mysql_real_escape_string($idUsuario));
                $oConexion->EjecutarQuery();
                
                $oConexion->setQuery("UPDATE datosusuariosdirecciones SET Domicilio=Null, Numero=Null, Escalera=Null, 
                					  Piso=Null, Puerta=Null, CP=Null, IdProvincia=Null, IdPais=Null, TipoCalle=Null, 
                					  Poblacion=Null, Telefono=Null WHERE IdUsuario = " . mysql_real_escape_string($idUsuario));
                $oConexion->EjecutarQuery();
                
                $oConexion->setQuery("UPDATE datosusuariospersonales SET FechaNacimiento=Null, Apellido2=Null, 
                					  TipoDOc=Null, NroDoc=Null, codtelefono='XXXX', Telefono=Null, Email=Null, 
                					  CIF=Null, IdTipoUsuario=Null, Es_EnviarDatosCorreo=Null, idTratamiento=Null, 
                					  celucodigo='XXXX', celunumero='XXXX' WHERE IdUsuario = " . mysql_real_escape_string($idUsuario));
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function GuardarClave($clave, $idUsuario)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE usuarios SET Clave = '".mysql_real_escape_string(Hash::getHash($clave))."' WHERE IdUsuario = " . mysql_real_escape_string($idUsuario));
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
		 public static function changepassbo($clave, $idUsuario = "0", $idUsuarioAdmin)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE usuarios SET Clave = '".mysql_real_escape_string(Hash::getHash($clave))."' WHERE IdUsuario = " . mysql_real_escape_string($idUsuario));
                $oConexion->EjecutarQuery();
                $oConexion->setQuery("insert into change_pass (IdUserChange, IdUserAdmin, datetimeChange) values ('".mysql_real_escape_string($idUsuario)."', '".mysql_real_escape_string($idUsuarioAdmin)."', NOW())");
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function save(MySQL_Usuario $objUsuario)
        {
        	$objUsuario->setClave(Hash::getHash($objUsuario->getClave()));
        	
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objUsuario->getInsert());
                $oConexion->EjecutarQuery();
                if($objUsuario->getIdUsuario() == 0)
                {
                    $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                    $ID = $oConexion->DevolverQuery();
                    $objUsuario->setIdUsuario($ID[0]["Id"]);
                }
                $oConexion->setQuery($objUsuario->getInsertDatosPersonales());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function delete(MySQL_Usuario $objUsuario)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objUsuario->getDelete());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function Login($email, $clave)
        {
                $oConexion = Conexion::nuevo();
            try
            {
                $objUsuario = new MySQL_Usuario();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdUsuario, Clave FROM usuarios WHERE NombreUsuario = '".mysql_real_escape_string($email)."' AND IdPerfil NOT IN('1')");
                
                $Tabla = $oConexion->DevolverQuery();
                if (count($Tabla) > 0)
                {
                    if(Hash::compareHash(Hash::getHash($clave), $Tabla[0]["Clave"]) != 1) {
                		throw new Exception("Datos de usuario incorrectos");
                	}
                    return dmUsuario::getByIdUsuario($Tabla[0]["IdUsuario"]);
                }
                else
                {
                    throw new Exception("Datos de usuario incorrectos");
                }
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        
        public static function getByIdUsuario($Id) {
            $oConexion = Conexion::nuevo();
            try {
                $objUsuario = new MySQL_Usuario();
                $oConexion->Abrir();
                $oConexion->setQuery($objUsuario->getById($Id));
                $Tabla = $oConexion->DevolverQuery();
                foreach($Tabla as $fila) {
                    foreach ($fila as $atributo => $valor) {
                        $objUsuario->__set($valor, $atributo);
                    }
                    $oConexion->setQuery($objUsuario->getDatosById($Id));
                    $Datos = $oConexion->DevolverQuery();
                    foreach ($Datos as $fila2) {
                        foreach ($fila2 as $atributo2 => $valor2) {
                            $objUsuario->getDatos()->__set($valor2, $atributo2);
                        }
                    }
                }
                $oConexion->setQuery("SELECT idSeccion FROM secciones_x_perfiles WHERE idPerfil = ".mysql_real_escape_string($objUsuario->getIdPerfil())." ORDER BY idSeccion ASC");
                $secc = $oConexion->DevolverQuery();
                $objUsuario->setSecciones($secc);
                $oConexion->Cerrar();
                return $objUsuario;
            }
            catch (MySQLException $e)
            {
            //    $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getPerfiles()
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT * FROM perfiles WHERE NOT IdPerfil = 1 ORDER BY IdPerfil ");
                $Tabla = $oConexion->DevolverQuery();
                $collPerfiles = array();
                foreach ($Tabla as $fila)
                {
                    $objPerfil = new MySQL_Perfil();
                    foreach($fila as $atributo => $valor)
                    {
                        $objPerfil->__set($valor,$atributo);
                    }
                    $collPerfiles[]=$objPerfil;
                }
                $oConexion->Cerrar();
                return $collPerfiles;
            }
            catch (MySQLException $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
        }
        public static function getPerfilesById($Id)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT * FROM perfiles WHERE NOT IdPerfil = 1 AND IdPerfil = ".mysql_real_escape_string($Id)." ORDER BY IdPerfil ");
                $Tabla = $oConexion->DevolverQuery();
                $objPerfil = new MySQL_Perfil();
                foreach($Tabla[0] as $atributo => $valor)
                {
                    $objPerfil->__set($valor,$atributo);
                }
                $oConexion->Cerrar();
                return $objPerfil;
            }
            catch (MySQLException $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
        }
        public static function getUsuarios($columnaNro, $columnaSentido)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT IdUsuario AS Codigo, NombreUsuario AS Email, Descripcion AS Perfil, DATE_FORMAT(FechaIngreso, '%d/%m/%Y') AS 'Fecha de alta' FROM usuarios U INNER JOIN perfiles P ON U.IdPerfil = P.IdPerfil WHERE NOT U.IdPerfil = 1 ORDER BY ".$columnaNro." ".$columnaSentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }
        }
		
        public static function getUsuariosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);

            $oConexion = Conexion::nuevo();
            try {
				$oConexion->Abrir_Trans();
				$oConexion->setQuery("	SELECT 	count(*) as total 
										FROM usuarios U 
										WHERE NOT U.IdPerfil = 1 
										AND ($filtersSQL) ");
				$Tabla = $oConexion->DevolverQuery();
				$oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }

        public static function getUsuariosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		    $filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
            $oConexion = Conexion::nuevo();
            try {
				$oConexion->Abrir_Trans();
				$oConexion->setQuery("	SELECT 	U.IdUsuario as 'U.IdUsuario', 
												U.NombreUsuario as 'U.NombreUsuario', 
												D.Nombre as 'D.Nombre',
												D.Apellido as 'D.Apellido', 
												P.Descripcion as 'P.Descripcion', 
												U.Padrino as 'U.Padrino',
												DATE_FORMAT(U.FechaIngreso, '%d/%m/%Y') as 'FormatedFechaIngreso',
												IF(EXISTS(SELECT C.IdUsuario FROM usuarios_carrito_bo C WHERE C.IdUsuario = U.IdUsuario), 'S&iacute;', 'No') as CarritoBO
										FROM usuarios U 
										INNER JOIN perfiles P ON U.IdPerfil = P.IdPerfil 
										INNER JOIN datosusuariospersonales D ON U.IdUsuario = D.IdUsuario 
										WHERE NOT U.IdPerfil = 1 
										AND ($filtersSQL)  
										ORDER BY $sortSQL 
										LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
		
        public static function getIdByNombreUsuario($NombreUsuario)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT IdUsuario FROM usuarios WHERE NombreUsuario = '".mysql_real_escape_string($NombreUsuario)."'");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                return $Tabla[0]["IdUsuario"];
            }
            catch(exception $e)
            {
                return false;
            }
        }
        public static function EsUsuarioRegistrado($idUsuario)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT * FROM usuarios WHERE IdUsuario = '".mysql_real_escape_string($idUsuario)."' AND es_activa = 0");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                if (count($Tabla) > 0)
                    return true;
                else
                    return false;
                //return $Tabla[0];
            }
            catch(exception $e)
            {
                return false;
            }
        }
        public static function EsUsuarioComprador($idUsuario)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT * FROM pedidos WHERE IdUsuario = '".mysql_real_escape_string($idUsuario)."'");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                if (count($Tabla) > 0)
                    return true;
                else
                    return false;
                //return $Tabla[0];
            }
            catch(exception $e)
            {
                return false;
            }
        }
        public static function getUsuariosAll()
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT * FROM usuarios");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
             //   $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getUsuariosActivos()
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT * FROM usuarios WHERE es_activa = 1");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
               // $oConexion->RollBack();
                throw $e;
            }

        }
        public static function getUsuariosInactivos()
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT * FROM usuarios WHERE es_activa = 0");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                //$oConexion->RollBack();
                throw $e;
            }

        }
	public static function ObtenerRegistrados()
    {
        $oConexion = Conexion::nuevo();
        try
        {
            $oConexion->Abrir();
            $oConexion->setQuery("SELECT U.NombreUsuario, UDP.Nombre, UDP. Apellido, U.IdUsuario FROM usuarios U INNER JOIN datosusuariospersonales UDP ON U.IdUsuario = UDP.IdUsuario WHERE U.es_activa=0 AND U.NombreUsuario LIKE '%@%'");
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar();
            return $Tabla;
        }
        catch(exception $e)
        {
            throw $e;
        }

    }
    }
?>