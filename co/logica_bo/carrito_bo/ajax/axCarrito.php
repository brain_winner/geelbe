<?
class axCarrito{
	
		//-------------------------------------------------------------------
	
		protected function getFotosCampanias(array $campanias){
		foreach($campanias as $key => $campania){
			$campania['arrimagenes'] = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$campania["IdCampania"]."/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));
			$campanias[$key]["foto"] = !empty($campania['arrimagenes']["logo2"]) ? Aplicacion::getRootUrl().'front/campanias/archivos/campania_'.$campania["IdCampania"].'/imagenes/'.$campania['arrimagenes']["logo2"]:"";
		}
		return $campanias;
	}
	
	protected function _getCampanias(){
		
		$campanias  = dmCampania::getCampaniasAx();
		$campanias = $this->getFotosCampanias($campanias);
		return $campanias;
	}	
	
	public function getCampanias($campanias){
		
		$campanias  = dmCampania::getCampaniasAx();
		$campanias = $this->getFotosCampanias($campanias);
		
		
		$html .= '<div id="campanias">';
		$html .='<h3>Campa&ntilde;ias</h3>';
	
		
		foreach($campanias as $campania){
			$html .= '<ul class="post">';
			$html .= '<li>'.$campania["Nombre"].'</li>';
			$html .= '<li><img src="'.$campania["foto"].'" alt="Campania" /></li>';
			$html .='</ul>';
		}


		$html .='</div>';
		
		return $html;
		
	}
	
	//-------------------------------------------------------------------
	
	protected function getFotosProductos(array $productos){
		foreach($productos as $key => $producto){
			$producto['arrimagenes'] = DirectorytoArray(Aplicacion::getRoot()."front/productos/productos_".$producto["IdProducto"], 3, array("jpg", "jpeg", "gif", "png"));
			$productos[$key]["Foto"] = !empty($producto['arrimagenes']["producto_1"])? Aplicacion::getRootUrl().'front/productos/productos_'.$producto["IdProducto"].'/'.$producto['arrimagenes']["producto_1"]:"";
		}	
		return $productos;
	}
	
	protected function getFotosProductosThumb(array $productos){
		foreach($productos as $key => $producto){
			$producto['arrimagenes'] = DirectorytoArray(Aplicacion::getRoot()."front/productos/productos_".$producto["IdProducto"]."/thumbs", 3, array("jpg", "jpeg", "gif", "png"));
			$productos[$key]["Foto"] = !empty($producto['arrimagenes']["thumb_producto_1"])? Aplicacion::getRootUrl().'front/productos/productos_'.$producto["IdProducto"].'/thumbs/'.$producto['arrimagenes']["thumb_producto_1"]:"";
		}	
		return $productos;
	}
	
	protected function _getProductos($IdCampania){

		$productos  = dmProductos::getProductosAx($IdCampania);
		//$productos = $this->getFotosProductos($productos);
		$productos = $this->getFotosProductosThumb($productos);

		return $productos;
	}
		
	public function getProductos($IdCampania){
		
		$productos = $this->_getProductos($IdCampania);

		$html = '<div id="producto">';
		$html .= '<h3>Productos</h3>';
	
					
		if(is_array($productos))
		{
		$i = 1;
			foreach($productos as $producto){
				$html .= '<ul class="post'.(fmod($i, 7)==0?" last":"").'">';
				$html .= '<li>'.$producto["Nombre"].'</li>';
				//$html .='<li><img src="'.$producto["Foto"].'" alt="Producto" width=120px" heigth="115px" /></li>';
				$html .='<li><img src="'.$producto["Foto"].'" alt="Producto"/></li>';
				$html .='<li class="PInterno">'.$producto["PInterno"].'</li>';
				$html .='<li class="PVenta">'.$producto["PVenta"].'</li>';
				$html .= '</ul>';
				$i++;
			}
		}
		

		$html .= '</div>';
				
		return $html;
	}
	
	//----------------------------------------------------------------------------------------
	
	protected function _getAtributos($IdProducto){
	
		$atributos  = dmProductos::getAtributosAx($IdProducto);
		
		return $atributos;
	}
	
	public function getAtributos($IdCampania){
	
		$productos = array();
		$categorias = array();
		$valores = array();
		$idInternos = array();
		$arbolProductos = array();
	
		$atributos = $this->_getAtributos($IdCampania);	
		//var_dump($atributos);
		
		//Obtengo las nombres de categorias.
		foreach($atributos as $key => $atributo){
			if(!in_array($atributo["Nombre"], $categorias)){
				$categorias[] = $atributo["Nombre"];
				$valores[$atributo["Nombre"]] = array();
			}
		}
		array_push($categorias,"Stock");
		
		$categorias_flip = array_flip($categorias);
		
		foreach($atributos as $key => $atributo){
			if(!in_array($atributo["IdCodigoProdInterno"], $categorias)){
				$idInternos[] = $atributo["IdCodigoProdInterno"];
				//$arbolProductos[$idInterno] = array();

			}
		}
		
		
		//Obtengo los datos por prod
		foreach($idInternos as $idInterno){
			foreach($atributos as $atributo){		
				if($idInterno == $atributo["IdCodigoProdInterno"]){
					$arbolProductos[$idInterno][$atributo["Nombre"]] = $atributo["Valor"];
					$arbolProductos[$idInterno]["Stock"] = $atributo["Stock"];
				}
			}
		}
	
		
		
		//Obtengo los valores para esas categorias		
		foreach($atributos as $atributo){
			if(!in_array($atributo["Valor"],$valores[$atributo["Nombre"]]))
				array_push($valores[$atributo["Nombre"]], $atributo["Valor"]);
		}
		//var_dump($valores);
		
		//Totales de categorias
		$total_categorias = count($categorias);
		
		
		$html = '<div id="detalle">';
			$html .= '<h3>Detalle</h3>';
			$html .= '<ul>';
				$html .= '<li>'.$atributos[0]["NombreProducto"].'</li>';
				$html .= '<li>';
				
				foreach($categorias as $key => $categoria){
					if($categoria != "Stock"){
				
					$html .= '<label for="'.$categoria.'">'.$categoria.'</label>'."\r\n";
					
					//Genero Select.
					$html .= '<select id="'.$categoria.'" name="'.$key.'">'."\r\n";
					$html .='<option value="nada">Seleccione</option>'."\r\n";
					
					//Si es la primera categoria
					if($key == 0){
						foreach($valores[$categoria] as $valor){
							$html .='<option value="'.$valor.'">'.$valor.'</option>'."\r\n";
						}
					}
					$html .= '</select>'."\r\n"; 
					}
				}
				
				$html .= '</li>';
				$html .= '<li id="Stock"></li>';
			$html .= '</ul>';

			$html .= '<ul>';
				$html .= '<li class="botMas">Agregar nuevo Producto</li>';
			$html .= '</ul>';
		$html .= '</div>';
		
		
		//foreach($categorias as $key => $categoria){
		$json_datos = "<script> datos = [";
		$i = 0;
		foreach($arbolProductos as $IdProdInterno => $arbolProducto){
			if($i!=0) $json_datos .= ",";	
		
			$json_datos .= "{";	
			
			$json_datos .= "'Stock':'".$arbolProducto["Stock"]."','IdCodigoProdInterno':'".$IdProdInterno."'";	
			foreach($arbolProducto as $arbolCat => $arbolVal){
					$json_datos .= ",";
					$json_datos .=	"'".$categorias_flip[$arbolCat]."':'".$arbolVal."'";
			}
			$json_datos .= "}";
			$i++;
		}
		$json_datos .= "]; </script>";
			
	return $html.$json_datos;
		
	}

}

?>