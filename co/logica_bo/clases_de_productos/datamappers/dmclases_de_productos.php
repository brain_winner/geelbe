<?
    class dmClases
    {
        public static function save(MySQL_Clases $objClase)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objClase->getInsert());
                $oConexion->EjecutarQuery();
                if($objClase->getIdClase() == 0)
                {
                    $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                    $ID = $oConexion->DevolverQuery();
                    $objClase->setIdClase($ID[0]["Id"]);
                }
                $oConexion->setQuery("SELECT IdImpuesto FROM  impuestos_x_claseproductos WHERE IdClase =".$objClase->getIdClase());
                $TBImpuestos = $oConexion->DevolverQuery();
                
                $oConexion->setQuery("SELECT IdAtributo FROM  atributos_x_clases WHERE IdClase =".$objClase->getIdClase());
                $TBAtributos = $oConexion->DevolverQuery();
                
                foreach($TBImpuestos as $i=>$fila)
                {
                    foreach($objClase->getImpuestos() as $impuesto)
                    {
                        if($impuesto->getIdImpuesto() == $fila["IdImpuesto"])
                        {
                            unset($TBImpuestos[$i]);
                        }
                    }
                }
                foreach($TBImpuestos as $i=>$fila)
                {
                    $oConexion->setQuery("DELETE FROM impuestos_x_claseproductos WHERE IdImpuesto =".$fila["IdImpuesto"]);
                    $oConexion->EjecutarQuery();
                }
                foreach($TBAtributos as $i=>$fila)
                {
                    foreach($objClase->getParametros() as $atributo)
                    {
                        foreach($atributo->getAtributos() as $atr)
                        {
                            if($atr->getIdAtributo() == $fila["IdAtributo"])
                            {
                                unset($TBAtributos[$i]);
                            }
                        }   
                    }
                }
                foreach($TBAtributos as $i=>$fila)
                {
                    $oConexion->setQuery("DELETE FROM atributos WHERE IdAtributo =".$fila["IdAtributo"]);
                    $oConexion->EjecutarQuery();
                }
                foreach($objClase->getParametros() as $parametro)
                {
                    $parametro->setIdClase($objClase->getIdClase());
                    foreach($parametro->getAtributos() as $atributo)
                    {
                        $oConexion->setQuery($atributo->getInsert());
                        $oConexion->EjecutarQuery();
                        if($atributo->getIdAtributo() == 0)
                        {
                            $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                            $ID = $oConexion->DevolverQuery();
                            $atributo->setIdAtributo($ID[0]["Id"]);
                        }
                        $parametro->setIdAtributo($atributo->getIdAtributo());
                    }
                    $oConexion->setQuery($parametro->getInsert());
                    $oConexion->EjecutarQuery();
                }
                foreach($objClase->getImpuestos() as $impuesto)
                {
                    $impuesto->setIdClase($objClase->getIdClase());
                    $oConexion->setQuery($impuesto->getInsert());
                    $oConexion->EjecutarQuery();
                }
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function delete(MySQL_Clases $objClase)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objClase->getDelete());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getByIdClase($Id)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $objClase = new MySQL_Clases();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objClase->getById($Id));
                $Tabla = $oConexion->DevolverQuery();
                foreach($Tabla as $fila)
                {
                    $objClase->__setByArray($fila);
                }
                if($objClase->getIdClase()>0)
                {
                    $oConexion->setQuery("SELECT idAtributoClase, idClase, idAtributo FROM atributos_x_clases WHERE IdClase = ".$objClase->getIdClase());
                    $Tabla = $oConexion->DevolverQuery();
                    foreach($Tabla as $fila)
                    {
                        $objAC = new MySQL_AtributosClases();
                        $objAC->__setByArray($fila);
                        $oConexion->setQuery("SELECT IdAtributo, Detalle, Nombre, Pregunta FROM atributos WHERE IdAtributo = ".$objAC->getIdAtributo());
                        $TablaA = $oConexion->DevolverQuery();
                        foreach($TablaA as $filaA)
                        {
                            $objAtributo = new MySQL_Atributos();
                            $objAtributo->__setByArray($filaA);
                            $objAC->addAtributos($objAtributo);
                        }
                        $objClase->addParametro($objAC);
                    }
                    $oConexion->setQuery("SELECT IdClase, IdImpuesto FROM impuestos_x_claseproductos WHERE IdClase = ".$objClase->getIdClase());
                    $Tabla = $oConexion->DevolverQuery();
                    foreach($Tabla as $fila)
                    {
                        $objIC = new MySQL_ImpuestosClases();
                        $objIC->__setByArray($fila);
                        $objClase->addImpuesto($objIC);
                    }
                }
                $oConexion->Cerrar_Trans();
                return $objClase;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getClases($columnaNro, $columnaSentido, $filter = "")
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdClase AS Codigo, Nombre AS Clase, IF(Habilitado = 1, 'Si', 'No') AS Habilitado FROM claseproductos WHERE Nombre LIKE \"%".$filter."%\" ORDER BY ".$columnaNro." ".$columnaSentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getAtributosByIdClase($IdClase)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT idAtributoClase, idClase, idAtributo FROM atributos_x_clases WHERE IdClase = ".mysql_real_escape_string($IdClase));
                $Tabla = $oConexion->DevolverQuery();
                
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>