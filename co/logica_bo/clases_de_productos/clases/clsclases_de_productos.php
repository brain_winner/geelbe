<?
    class MySQL_Clases extends claseproductos
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_idclase")
                {   
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=($valor != 0) ? chr(34).mysql_real_escape_string($valor).chr(34).", " : "NULL, ";
                }
                else
                {   
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
            }
            return "INSERT INTO claseproductos (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getDelete()
        {
            return "DELETE FROM claseproductos WHERE IdClase = ".$this->getIdClase();
        }
        public function getById($Id)
        {
            $strCampos="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM claseproductos WHERE IdClase = ".mysql_real_escape_string($Id);
        }
    }
    class MySQL_Atributos extends atributos
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_idatributo")
                {   
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=($valor != 0) ? chr(34).mysql_real_escape_string($valor).chr(34).", " : "NULL, ";
                }
                else
                {   
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
            }
            return "INSERT INTO atributos (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getDelete()
        {
            return "DELETE FROM atributos WHERE IdAtributo = ".$this->getIdAtributo();
        }
        public function getById($Id)
        {
            $strCampos="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM atributos WHERE IdAtributo = ".mysql_real_escape_string($Id);
        }
    }
    class MySQL_AtributosClases extends atributosclase
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_idatributoclase")
                {   
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=($valor != 0) ? chr(34).mysql_real_escape_string($valor).chr(34).", " : "NULL, ";
                }
                else
                {   
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
            }
            return "INSERT INTO atributos_x_clases (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getDelete()
        {
            return "DELETE FROM atributos_x_clases WHERE idAtributoClase = ".$this->getIdAtributoClase();
        }
        public function getById($Id)
        {
            $strCampos="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampos, 0, strlen($strCampos)-2)." FROM atributos_x_clases WHERE idAtributoClase = ".mysql_real_escape_string($Id);
        }
    }
    class MySQL_ImpuestosClases extends impuestosclaseproductos
    {
        public function getInsert()
        {
            $strCampos="";
            $strCamposU="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
               $strCamposU .=substr($propiedad,1,strlen($propiedad))." = " . chr(34).mysql_real_escape_string($valor).chr(34).", ";
               $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
               $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
            }
            return "INSERT INTO impuestos_x_claseproductos (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getDelete()
        {
            return "DELETE FROM impuestos_x_claseproductos WHERE IdClase = ".$this->getIdClase()." AND IdImpuesto = ".$this->getIdImpuesto();
        }
    }
?>