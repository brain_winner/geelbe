<?php
    if($_POST)
    {
        ob_start();
        extract($_POST);
        try
        {   
            $objClase = new MySQL_Clases();
            $objClase->setIdClase($txtIdClase);
            $objClase->setNombre($txtNombre);
            $objClase->setHabilitado(isset($chkHabilitado)?1:0);
            if(isset($txt_tbl_atributos))
            {
                foreach($txt_tbl_atributos as $parametro)
                {
                    $objAtributos = new MySQL_Atributos;
                    $objAtributos->setIdAtributo($parametro[0]);
                    $objAtributos->setNombre($parametro[1]);
                    $objAC = new MySQL_AtributosClases;
                    $objAC->addAtributos($objAtributos);
                    $objClase->addParametro($objAC);
                }
            }
            if(isset($txt_tbl_impuestos))
            {
                foreach($txt_tbl_impuestos as $impuesto)
                {
                    $objImp = new MySQL_ImpuestosClases();
                    $objImp->setIdImpuesto($impuesto[0]);
                    $objClase->addImpuesto($objImp);
                }
            }
            dmClases::save($objClase);
            ?>
                <script type="text/javascript">
                    window.irVolver();
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            print_r($e);
            ?>
                <script>
                    window.ErrorMSJ("<?=$e->getNumeroSQLERROR()?>", "");
                </script>
            <?
        }
        ob_flush();
    }
?>