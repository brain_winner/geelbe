<?
    class dmProveedor
    {
        public static function save(MySQL_Proveedor $objProveedor)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objProveedor->getInsert());
                $oConexion->EjecutarQuery();
                if($objProveedor->getIdProveedor() == 0)
                {
                    $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                    $ID = $oConexion->DevolverQuery();
                    $objProveedor->setIdProveedor($ID[0]["Id"]);
                }
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function delete(MySQL_Proveedor $objProveedor)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objProveedor->getDelete());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getByIdProveedor($Id)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $objProveedor = new MySQL_Proveedor();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objProveedor->getById($Id));
                $Tabla = $oConexion->DevolverQuery();
                foreach($Tabla as $fila)
                {
                    foreach ($fila as $atributo => $valor)
                    {
                        $objProveedor->__set($valor, $atributo);
                    }                    
                }
                $oConexion->Cerrar_Trans();
                return $objProveedor;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getProveedors($columnaNro, $columnaSentido, $filtro = "")
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT P.IdProveedor, P.Nombre AS Proveedor, CP, Pro.Nombre AS Provincia, Telefono, Fax, WebSite, IF(P.Habilitado = 1, 'Si', 'No') AS Habilitado FROM proveedores P LEFT JOIN provincias Pro ON Pro.IdProvincia = P.IdProvincia WHERE P.Nombre LIKE \"%".$filtro."%\" OR Pro.Nombre LIKE \"%".$filtro."%\" OR Telefono LIKE \"%".$filtro."%\" OR Fax LIKE \"%".$filtro."%\" OR WebSite LIKE \"%".$filtro."%\" OR CP LIKE \"%".$filtro."%\" ORDER BY ".$columnaNro." ".$columnaSentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>