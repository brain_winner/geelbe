<?php
    if($_POST)
    {
        ob_start();
        extract($_POST);
        try
        {   
            //$IdProveedor = ($txtIdProveedor == "0") ? 0 : Aplicacion::Decrypter($txtIdProveedor);
            $IdProveedor = ($txtIdProveedor == "0") ? 0 : $txtIdProveedor;
            $objProveedor = new MySQL_Proveedor();
            $objProveedor->setCIF($txtCIF);
            $objProveedor->setCP($txtCP);
            $objProveedor->setComentarios($txtComentarios);
            $objProveedor->setDireccion($txtDireccion);
            $objProveedor->setFax($txtFax);
            $objProveedor->setIdProveedor($IdProveedor);
            $objProveedor->setIdProvincia($cbxIdProvincia);
            $objProveedor->setLocalidad($txtLocalidad);
            $objProveedor->setNombre($txtNombre);
            $objProveedor->setTelefono($txtTelefono);
            $objProveedor->setWebSite($txtWebSite);
            $objProveedor->setHabilitado(isset($chkHabilitado)?1:0);
            dmProveedor::save($objProveedor);
            ?>
                <script type="text/javascript">
                    window.irVolver();
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                   window.ErrorMSJ("<?=$e->getNumeroSQLERROR()?>", "");
                </script>
            <?
        }
        ob_flush();
    }
?>