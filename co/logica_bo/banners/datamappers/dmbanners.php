<?php
    class dmBanners
    {
        public static function getAll($columna = 'nombre', $sentido = 'asc', $filtro="")
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT b.id, b.Nombre FROM banners b ".($filtro != '' ? "WHERE b.nombre LIKE \"%".$filtro."%\"" : '')." ORDER BY " . $columna . " " . $sentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }

        public static function getVidriera()
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM banners WHERE tipo = 0 AND subtipo = 0 AND (limitado = 0 OR (NOW() BETWEEN desde AND hasta))");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                foreach($Tabla as $i => $t)
	                $Tabla[$i]['img'] = dmBanners::getImg($t['id']);
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }

        public static function getVidrieraSidebar()
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM banners WHERE tipo = 0 AND subtipo = 1 AND (limitado = 0 OR (NOW() BETWEEN desde AND hasta))");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                foreach($Tabla as $i => $t)
	                $Tabla[$i]['img'] = dmBanners::getImg($t['id']);
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }

        public static function getCampaniaCuerpo($idc = null)
        {
            $oConexion = Conexion::nuevo();
            try
            {
            
            	 if(is_numeric($idc))
            	 	$idc = '(IdCampania = '.$idc.' OR IdCampania is null) AND ';
            	 else
            	 	$idc = '';
            	
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM banners WHERE ".mysql_real_escape_string($idc)." tipo = 1 AND subtipo = 0 AND (limitado = 0 OR (NOW() BETWEEN desde AND hasta))");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                foreach($Tabla as $i => $t)
	                $Tabla[$i]['img'] = dmBanners::getImg($t['id']);
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }

        public static function getCampaniaSidebar($idc = null)
        {
            $oConexion = Conexion::nuevo();
            try
            {
            
            	 if(is_numeric($idc))
            	 	$idc = '(IdCampania = '.$idc.' OR IdCampania is null) AND ';
            	 else
            	 	$idc = '';
            	
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM banners WHERE ".mysql_real_escape_string($idc)." tipo = 1 AND subtipo = 1 AND (limitado = 0 OR (NOW() BETWEEN desde AND hasta))");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                foreach($Tabla as $i => $t)
	                $Tabla[$i]['img'] = dmBanners::getImg($t['id']);
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        
        public static function getProductoCuerpo($idc = null, $idp = null)
        {
            $oConexion = Conexion::nuevo();
            try
            {
            
            	 if(is_numeric($idc))
            	 	$idc = '(IdCampania = '.$idc.' OR IdCampania is null) AND ';
            	 else
            	 	$idc = '';
            	
            	 if(is_numeric($idp))
            	 	$idp = '(IdProducto = '.$idp.' OR IdProducto is null) AND ';
            	 else
            	 	$idp = '';
            	
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM banners WHERE ".mysql_real_escape_string($idc).mysql_real_escape_string($idp)." tipo = 2 AND subtipo = 0 AND (limitado = 0 OR (NOW() BETWEEN desde AND hasta))");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                foreach($Tabla as $i => $t)
	                $Tabla[$i]['img'] = dmBanners::getImg($t['id']);
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }

        public static function getProductoSidebar($idc = null, $idp = null)
        {
            $oConexion = Conexion::nuevo();
            try
            {
            
            	 if(is_numeric($idc))
            	 	$idc = '(IdCampania = '.$idc.' OR IdCampania is null) AND ';
            	 else
            	 	$idc = '';
            	
            	 if(is_numeric($idp))
            	 	$idp = '(IdProducto = '.$idp.' OR IdProducto is null) AND ';
            	 else
            	 	$idp = '';
            	
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM banners WHERE ".mysql_real_escape_string($idc).mysql_real_escape_string($idp)." tipo = 2 AND subtipo = 1 AND (limitado = 0 OR (NOW() BETWEEN desde AND hasta))");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                foreach($Tabla as $i => $t)
	                $Tabla[$i]['img'] = dmBanners::getImg($t['id']);
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }

        public static function save($oLocal)
        {
            $oConexion = Conexion::nuevo();
            try
            {                                    
                $oConexion->Abrir_Trans();
                if ($oLocal->getIdBanner() == "") {
                    $oConexion->setQuery("INSERT INTO banners (nombre, tipo, subtipo, idcampania, idproducto, limitado, desde, hasta, link) VALUES('".htmlspecialchars(mysql_real_escape_string($oLocal->getNombre()), ENT_QUOTES)."', '".$oLocal->getTipo()."', '".$oLocal->getSubtipo()."', ".$oLocal->getIdCampania().", ".$oLocal->getIdProducto().", '".$oLocal->getLimitado()."', '".$oLocal->getDesde()."', '".$oLocal->getHasta()."', '".$oLocal->getLink()."')");
                } else
                    $oConexion->setQuery("UPDATE banners SET nombre = '".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($oLocal->getNombre())), ENT_QUOTES)."', tipo = '".$oLocal->getTipo()."', subtipo = '".$oLocal->getSubtipo()."', idcampania = ".$oLocal->getIdCampania().", idproducto = ".$oLocal->getIdProducto().", limitado = '".$oLocal->getLimitado()."', desde = '".$oLocal->getDesde()."', hasta = '".$oLocal->getHasta()."', link = '".$oLocal->getLink()."' WHERE id = " . $oLocal->getIdBanner());
                $oConexion->EjecutarQuery();
                                
                if($oLocal->getIdBanner() == "")               	
                	$id = $oConexion->LastId();
                else
                	$id = $oLocal->getIdBanner();
                                
                if(isset($_FILES['img']) && is_uploaded_file($_FILES['img']['tmp_name'])) {
            			
           			$ext = pathinfo($_FILES['img']['name']);
           			$ext = strtolower($ext['extension']);

           			if($ext == 'jpeg' || $ext == 'jpg' || $ext == 'gif' || $ext == 'png')                			
              			dmBanners::upload($id, $_FILES['img']['tmp_name'], $ext);
                             
                }
                                
                $oConexion->Cerrar_Trans();
                return $id;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }            
        }
        
        public static function upload($id, $img, $ext) {
        
        	dmBanners::deleteImages($id);
        	$ruta = dirname(__FILE__).'/../../../front/archivos/banners/'.$id.'/';
			if(!is_dir($ruta))
				mkdir($ruta);
			
        	move_uploaded_file($img, $ruta.$id.'.'.$ext);
        	
        }
        
        public static function deleteImages($id) {
        	 $ruta = dirname(__FILE__).'/../../../front/archivos/banners/'.$id;
        	 if(!is_dir($ruta))
        	 	return false;
        	 	
          $dir = opendir($ruta);
          while($f = readdir($dir))
          	if(!is_dir($ruta.'/'.$f))
          		unlink($ruta.'/'.$f);
        }
        
        public static function getImg($id) {
        	 $ruta = dirname(__FILE__).'/../../../front/archivos/banners/'.$id.'/';
        	 if(!is_dir($ruta))
        	 	return false;
        	 	
          $dir = opendir($ruta);
          while($f = readdir($dir))
          	if(!is_dir($ruta.$f))
          		return $f;
        }
                
        public static function getById($idLocal)
        {
            $oConexion = Conexion::nuevo();
            $oLocal = new Banners();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM banners WHERE id = " . $idLocal);
                $Fila = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                
                    $oLocal->setIdBanner($Fila[0]["id"]);
                    $oLocal->setNombre($Fila[0]["nombre"]);
                    $oLocal->setTipo($Fila[0]["tipo"]);
                    $oLocal->setSubtipo($Fila[0]["subtipo"]);
                    $oLocal->setIdCampania($Fila[0]["IdCampania"]);
                    $oLocal->setIdProducto($Fila[0]["IdProducto"]);
                    $oLocal->setLimitado($Fila[0]["limitado"]);
                    $oLocal->setDesde($Fila[0]["desde"]);
                    $oLocal->setHasta($Fila[0]["hasta"]);
                    $oLocal->setLink($Fila[0]["link"]);

                }
                catch(MySQLException $e)
                {
                    throw $e;
                }
                return $oLocal;
            }
            
       	public static function delete($obj)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("DELETE FROM banners WHERE id = ".$obj->getIdBanner());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                
                dmBanners::deleteImages($obj->getIdBanner());

					dmBanners::deleteImages($obj->getIdBanner());
					$ruta = dirname(__FILE__).'/../../../front/archivos/banners/'.$obj->getIdBanner().'/';
					rmdir($ruta);
                
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>
