<?php
    class Banners
    {
        private $_idbanner;
        private $_nombre;
        private $_link;
        private $_tipo; //0: Vidriera, 1: Campa�a, 2: Producto
        private $_subtipo; //0: Cuerpo, 1: Sidebar
        private $_idcampania;
        private $_idproducto;
        private $_limitado;
        private $_desde;
        private $_hasta;
       
        public function __construct()
        {
            $this->_idbanner = "";
            $this->_nombre = "";
            $this->_link = "";
            $this->_tipo = "";
            $this->_subtipo = "";
            $this->_idcampania = "null";
            $this->_idproducto = "null";
            $this->_limitado = "";
            $this->_desde = "";
            $this->_hasta = "";
        }
        public function getIdBanner()
        {
            return $this->_idbanner;
        }
        public function setIdBanner($idlocal)
        {
            $this->_idbanner = $idlocal;
        }
        public function getNombre()
        {
            return $this->_nombre;
        }
        public function setNombre($idlocal)
        {
            $this->_nombre = $idlocal;
        }
        public function getLink()
        {
            return $this->_link;
        }
        public function setLink($idlocal)
        {
            $this->_link = $idlocal;
        }
        public function getTipo()
        {
            return $this->_tipo;
        }
        public function setTipo($descripcion)
        {
            $this->_tipo = $descripcion;
        }
        public function getSubtipo()
        {
            return $this->_subtipo;
        }
        public function setSubtipo($descripcion)
        {
            $this->_subtipo = $descripcion;
        }
        public function getIdCampania()
        {
            return $this->_idcampania;
        }
        public function setIdCampania($descripcion)
        {
            $this->_idcampania = $descripcion;
        }
        public function getIdProducto()
        {
            return $this->_idproducto;
        }
        public function setIdProducto($descripcion)
        {
            $this->_idproducto = $descripcion;
        }
        public function getLimitado()
        {
            return $this->_limitado;
        }
        public function setLimitado($descripcion)
        {
            $this->_limitado = $descripcion;
        }
        public function getDesde()
        {
            return $this->_desde;
        }
        public function setDesde($descripcion)
        {
            $this->_desde = $descripcion;
        }
        public function getHasta()
        {
            return $this->_hasta;
        }
        public function setHasta($descripcion)
        {
            $this->_hasta = $descripcion;
        }
    }
?>
