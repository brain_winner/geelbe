<html>
    <head>
    </head>
    <body>
<?php
    if($_POST)
    {	
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/banners/clases/clsbanners.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/banners/datamappers/dmbanners.php");
        extract($_POST);
        try
        {   
            $oLocal = new Banners();

            if(isset($idBanner) && is_numeric($idBanner))
            	$oLocal->setIdBanner($idBanner);
            	
            $oLocal->setNombre($txtNombre);
            $oLocal->setTipo($txtTipo);
            $oLocal->setSubtipo($txtSubtipo);
            $oLocal->setIdCampania($txtIdCampania);
            $oLocal->setIdProducto($txtIdProducto);
            $oLocal->setLimitado($txtLimitado);
            $oLocal->setDesde($txtDesde);
            $oLocal->setHasta($txtHasta);
            $oLocal->setLink($txtLink);
            $id = dmBanners::save($oLocal);
            ?>
                <script type="text/javascript">
                    window.location.href = '../../bo/banners/index.php';
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                    window.parent.ErrorMSJ("<?=$e->getNumeroSQLERROR()?>", "");
                </script>
            <?
        }
        ob_flush();
    }
?>
    </body>
</html>
