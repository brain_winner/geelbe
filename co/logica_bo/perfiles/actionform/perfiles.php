<html>
    <head>
    </head>
    <body>
<?php

    if($_POST)
    {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("perfiles"));
        extract($_POST);
        try
        {   
            $oPerfil = new Perfiles_bo();
            $oPerfil->setIdPerfil($idPerfil);
            $oPerfil->setDescripcion($txtDescripcion);
            $ultimoId=dmPerfiles::save($oPerfil);
            if($idPerfil!=0)
            {
                dmSecciones_x_perfiles::Delete($ultimoId);
            }
            foreach($chkSecciones as $secc)
            {
                $oSXP = new Secciones_x_perfiles();
                $oSXP->setidPerfil($ultimoId);
                $oSXP->setidSeccion($secc);
                
                dmSecciones_x_perfiles::Insert($oSXP);
            }
            ?>
                <script type="text/javascript">
                    window.irVolver();
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                    window.parent.ErrorMSJ("<?=$e->getNumeroSQLERROR()?>", "");
                </script>
            <?
        }
        ob_flush();
    }
?>
    </body>
</html>
