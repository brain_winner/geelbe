<?
    class dmPerfiles
    {
	    /**
	    *Realiza el INSERT en la base de datos
	    *@param class oPerfiles
	    *@return 1 si ejecuto con exito, 0 si ocurrio un error.
	    */
	    public static function Insert($operfiles)
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir_Trans();
			    $cn->setQuery("INSERT INTO perfiles (Descripcion) VALUES ('". $operfiles->getDescripcion() ."')");
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return 1;
		    }
		    catch(exception $e)
		    {
			    $cn->RollBack();
			   throw $e;
		    }		
	    }
	    /**
	    *Realiza el UPDATE en la base de datos
	    *@param class operfiles
	    *@return 1 si ejecuto con exito, 0 si ocurrio un error.
	    */
	    public static function Update($operfiles)
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir_Trans();
			    $cn->setQuery("UPDATE perfiles SET Descripcion = '". htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($operfiles->getDescripcion())), ENT_QUOTES) ."' WHERE IdPerfil = ". $operfiles->getIdPerfil() ."");
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return 1;
		    }
		    catch(exception $e)
		    {
			    $cn->RollBack();
			    throw $e;
		    }		
	    }
        public static function save($operfiles)
        {        
            $cn = Conexion::nuevo();
            try
            {   $cn->Abrir_Trans();
                if($operfiles->getIdPerfil() == "0")
                {
                    $cn->setQuery("INSERT INTO perfiles (Descripcion) VALUES ('". $operfiles->getDescripcion() ."')");
                    $cn->EjecutarQuery();
                    $cn->setQuery("SELECT LAST_INSERT_ID() AS ID");
                    $ID=$cn->DevolverQuery();
                    $cn->Cerrar_Trans();
                    $id=$ID[0]['ID'];
                    return $id;                                       
                }
                else
                {
                    $cn->setQuery("UPDATE perfiles SET Descripcion = '". htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($operfiles->getDescripcion())), ENT_QUOTES) ."' WHERE IdPerfil = ". $operfiles->getIdPerfil() ."");
                    $cn->EjecutarQuery();
                    $cn->Cerrar_Trans();
                    return $operfiles->getIdPerfil();
                }
            }
            catch(exception $e)
            {
                $cn->RollBack();
                throw $e;
            }        
        }        
	    /**
	    *Realiza el DELETE en la base de datos
	    *@param object operfiles
	    *@return 1 si ejecuto con exito, 0 si ocurrio un error.
	    */
	    public static function Delete($operfiles)
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir_Trans();
			    $cn->setQuery("DELETE FROM perfiles WHERE IdPerfil = ". $operfiles->getIdPerfil() ."");
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return 1;
		    }
		    catch(exception $e)
		    {
			    $cn->RollBack();
			    throw $e;
		    }		
	    }
	    /**
	    *Realiza un SELECT que devuelve todos los registros de perfiles
	    *@return array Assoc, si ocurrio un error devuelve array()
	    */
	    public static function getAll($columna, $sentido, $filtro="")
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir();
			    $cn->setQuery("SELECT * FROM perfiles P WHERE P.IdPerfil not in(1) and P.Descripcion LIKE \"%".$filtro."%\" ORDER BY " . $columna . " " . $sentido);
			    $Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();
			    return $Tabla;
		    }
		    catch(exception $e)
		    {			
			    throw $e;                   
		    }		
	    }
        /**
        *Realiza un SELECT que devuelve todos los registros de perfiles
        *@return array Assoc, si ocurrio un error devuelve array()
        */
        public static function getAllObj()
        {        
            $cn = Conexion::nuevo();
            try
            {
                $cn->Abrir();
                $cn->setQuery("SELECT * FROM perfiles");
                $Tabla = $cn->DevolverQuery();
                $operfiles[] = array();
                foreach($Tabla as $Fila)
                {
                    $operfiles[] = new Perfiles();
                    				$operfiles[count($oPerfiles)-1]->setIdPerfil($Fila['IdPerfil']);
				$operfiles[count($oPerfiles)-1]->setDescripcion($Fila['Descripcion']);

                }
                $cn->Cerrar();
                return $operfiles;
            }
            catch(exception $e)
            {            
                throw $e;                  
            }        
        }
	    /**
	    *Realiza un SELECT que devuelve UN registros de perfiles por $IdPerfil
	    *@param $IdPerfil condicion a buscar
	    *@return class clsperfiles, si ocurrio un error devuelve class clsperfiles vacio
	    */
	    public static function getById($IdPerfil)
	    {		
		    $cn = Conexion::nuevo();
		    $operfiles = new Perfiles();
		    try
		    {
			    $cn->Abrir();
			    $cn->setQuery("SELECT * FROM perfiles WHERE IdPerfil = ". $IdPerfil ." ");
			    $Tabla = $cn->DevolverQuery();
			    if ($Tabla)
			    {
    				$operfiles->setIdPerfil($Tabla[0]['IdPerfil']);
				$operfiles->setDescripcion($Tabla[0]['Descripcion']);

			    
			    }
			    $cn->Cerrar();			
		    }
		    catch(exception $e)
		    {			
			    throw $e;
		    }
		    return $operfiles;
	    }
    }
?>
