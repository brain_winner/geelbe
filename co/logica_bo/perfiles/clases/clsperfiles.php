<?
/**
*****************************************************************************************************
* CLASES																							*
* ATBK																								*
* www.atbk.com.ar																					*
*****************************************************************************************************
*/
/**
* Clase [%Nombre%]
* @author ATBK
* @copyright Copyright (c) 2007
* @license http://www.atbk.com.ar
*/
class Perfiles_bo
{
	/*Datos Privados*/
	private $_idperfil;
	private $_descripcion;


	/*Constructor*/
/**
*@var idperfil
*@var descripcion
*/
	function __construct($idperfil=0, $descripcion='')
	{
		$this->_idperfil=$idperfil;
		$this->_descripcion=$descripcion;


	}
	/*Metodos Publicos*/
	/**
	* Set IdPerfil
	* @var IdPerfil
	*/
	function setIdPerfil($idperfil)
	{
		$this->_idperfil =$idperfil;
	}
	/**
	* Get IdPerfil
	* @return IdPerfil
	*/
	function getIdPerfil()
	{
		return $this->_idperfil;
	}
	/**
	* Set Descripcion
	* @var Descripcion
	*/
	function setDescripcion($descripcion)
	{
		$this->_descripcion =$descripcion;
	}
	/**
	* Get Descripcion
	* @return Descripcion
	*/
	function getDescripcion()
	{
		return $this->_descripcion;
	}
	
	/*Destructor*/
	function __destruct()
	{
	}
}
?>
