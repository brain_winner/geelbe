<?php
    class dmLocales
    {
        public static function getAll($columna, $sentido, $filtro="")
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT L.*, P.Nombre FROM locales L INNER JOIN provincias P ON P.idProvincia = L.idProvincia WHERE L.Direccion LIKE \"%".$filtro."%\" OR L.Descripcion LIKE \"%".$filtro."%\" OR P.Nombre LIKE \"%".$filtro."%\" ORDER BY " . $columna . " " . $sentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                /*$arrLocales = array();
                foreach($Tabla as $Fila)
                {
                    $arrLocales[] = new Locales();
                    $arrLocales[count($arrLocales) - 1]->setIdLocal($Fila["idLocal"]);
                    $arrLocales[count($arrLocales) - 1]->setDescripcion($Fila["Descripcion"]);
                    $arrLocales[count($arrLocales) - 1]->setDireccion($Fila["Direccion"]);
                    $arrLocales[count($arrLocales) - 1]->setIdProvincia($Fila["idProvincia"]);
                }
                return $arrLocales;*/
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        public static function save($oLocal)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                if ($oLocal->getIdLocal() == "")
                    $oConexion->setQuery("INSERT INTO locales(Descripcion, Direccion, idProvincia) VALUES('".htmlspecialchars(mysql_real_escape_string($oLocal->getDescripcion()), ENT_QUOTES)."','".htmlspecialchars(mysql_real_escape_string($oLocal->getDireccion()), ENT_QUOTES)."', ".$oLocal->getIdProvincia().")");
                else
                    $oConexion->setQuery("UPDATE locales SET Descripcion = '".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($oLocal->getDescripcion())), ENT_QUOTES)."', Direccion = '".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($oLocal->getDireccion())), ENT_QUOTES)."', idProvincia=".$oLocal->getIdProvincia()." WHERE IdLocal = " . $oLocal->getIdLocal());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }            
        }
        public static function getById($idLocal)
        {
            $oConexion = Conexion::nuevo();
            $oLocal = new Locales();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM locales WHERE IdLocal = " . $idLocal);
                $Fila = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                
                    $oLocal->setIdLocal($Fila[0]["idLocal"]);
                    $oLocal->setDescripcion($Fila[0]["Descripcion"]);
                    $oLocal->setDireccion($Fila[0]["Direccion"]);
                    $oLocal->setIdProvincia($Fila[0]["idProvincia"]);
                }
                catch(MySQLException $e)
                {
                    throw $e;
                }
                return $oLocal;
            }
            public static function getLocalesbyCampania($IdCampania)
            {
                $oConexion = Conexion::nuevo();
            $oLocal = new Locales();
            try
            {
                $oConexion->Abrir_Trans();
                $sql= "SELECT DISTINCT L.IdLocal, L.Descripcion, L.Direccion, PROV.Nombre FROM locales L 
INNER JOIN provincias PROV ON PROV.IdProvincia = L.IdProvincia
INNER JOIN locales_x_producto LP ON LP.IdLocal = L.IdLocal
INNER JOIN productos P ON P.IdProducto = LP.IdProducto
INNER JOIN productos_x_categorias PC ON PC.IdProducto = P.IdProducto
INNER JOIN categorias CAT ON CAT.IdCategoria = PC.IdCategoria
INNER JOIN campanias C ON C.IdCampania = CAT.IdCampania
WHERE C.IdCampania = " . $IdCampania;
                $oConexion->setQuery($sql);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
                }
                catch(MySQLException $e)
                {
                    throw $e;
                }
                return $oLocal;
            }            
    }
?>
