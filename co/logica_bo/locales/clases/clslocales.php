<?php
    class Locales
    {
        private $_idlocal;
        private $_descripcion;
        private $_direccion;
        private $_idprovincia;
       
        public function __construct()
        {
            $this->_idlocal = "";
            $this->_descripcion = "";
            $this->_direccion = "";
            $this->_idprovincia = "";
        }
        public function getIdLocal()
        {
            return $this->_idlocal;
        }
        public function setIdLocal($idlocal)
        {
            $this->_idlocal = $idlocal;
        }
        public function getDescripcion()
        {
            return $this->_descripcion;
        }
        public function setDescripcion($descripcion)
        {
            $this->_descripcion = $descripcion;
        }
        public function getDireccion()
        {
            return $this->_direccion;
        }
        public function setDireccion($direccion)
        {
            $this->_direccion = $direccion;
        }
        public function getIdProvincia()
        {
            return $this->_idprovincia;
        }
        public function setIdProvincia($idprovincia)
        {
            $this->_idprovincia = $idprovincia;
        }
    }
?>
