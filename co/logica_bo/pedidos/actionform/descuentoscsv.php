<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");

   header("Content-Description: File Transfer");
   header("Content-Type: application/force-download");
   header("Content-Disposition: attachment; filename=".date("Ymd")."_pedidos.csv");

    try
    {
        ValidarUsuarioLogueadoBo(23);
    }
    catch(ACCESOException $e)
    {
    	 try
	    {
	        ValidarUsuarioLogueadoBo(4);
	    }
	    catch(ACCESOException $e)
	    {
	    ?>  
	        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
	    <?
	        exit;
	    } 
    } 

    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("pedidos-bo"));
    try
    {                                                            
        $Tabla = dmPedidos::getPedidosAceptadosConDescuento($_GET['IdDescuento']);
        
        echo("Numero de Pedido;E-Mail;\n");
        foreach($Tabla as $Pedido)
        {
            echo ($Pedido["Pedido"] . "; " . $Pedido["NombreUsuario"] . ";\n");
            
        }
        
    }
    catch(exception $e)
    {
        print $e->getMessage();
    }
?>