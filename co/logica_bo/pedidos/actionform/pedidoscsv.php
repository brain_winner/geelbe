<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");

   header("Content-Description: File Transfer");
   header("Content-Type: application/force-download");
   header("Content-Disposition: attachment; filename=".date("Ymd")."_pedidos.csv");

    try
    {
        ValidarUsuarioLogueadoBo(23);
    }
    catch(ACCESOException $e)
    {
    	 try
	    {
	        ValidarUsuarioLogueadoBo(4);
	    }
	    catch(ACCESOException $e)
	    {
	    ?>  
	        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
	    <?
	        exit;
	    } 
    } 

    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("pedidos-bo"));
    try
    {                                                            
        $Tabla = dmPedidos::getPedidosAceptados();

        
        foreach($Tabla as $Pedido)
        {
        	if(isset($_GET['IdDescuento']) && $Pedido['IdDescuento'] != $_GET['IdDescuento'])
        		continue;
        
            echo("Numero de Pedido;Apellido y nombre;Fecha; Nombre campa�a; Monto total;\n");                                             
            echo ($Pedido["Pedido"] . "; " . $Pedido["Apellido y nombre"] . "; " . $Pedido["Fecha"] . "; " . $Pedido["Nombre campa�a"]. "; " .  $Pedido["Monto total"] . ";\n");
            
            $objPedido = dmPedidos::getPedidoById($Pedido["Pedido"]);
            $Bonos = dmMisCreditos::VIEWgetByIdPedido($Pedido["Pedido"]);
            $objUsuario = dmMicuenta::GetUsuario($objPedido->getIdUsuario());
            $InfoPedido = dmPedidos::getInfoPedido($Pedido["Pedido"]);
            
            echo("Detalle de Pedido \n");
             foreach($objPedido->getProductos() as $Articulo)
              {
                  $objPP = dmProductos::getByIdCodigoProdInterno($Articulo->getIdCodigoProdInterno());
                  $objProducto = dmProductos::getById($objPP->getIdProducto());
                  $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($Articulo->getIdCodigoProdInterno());

                  echo($objProducto->getNombre() . "; \n ");

                  foreach($Atributos as $atributo)
                  {
                      echo ($atributo["Nombre"]."; ".$atributo["Valor"] . "; \n ");
                  }
                  echo($Articulo->getCantidad() . "; " . Moneda($Articulo->getPrecio() * $Articulo->getCantidad(),2) . "; \n");
				  
              }

	            $BonoUsado = $Bonos[0]["Bono"];
                echo("Costo de Envio; Sub-Total de la Compra ; Cr�ditos utilizados; Total de la Compra;\n");
                echo(Moneda($objPedido->getGastosEnvio()) . "; " . Moneda($objPedido->getTotal()+$BonoUsado) . "; " );
                echo(Moneda($BonoUsado) . "; " . Moneda($objPedido->getTotal()) . "; ");
                echo("\n");
                echo("Informacion de Pago\n");
                echo("DNI;Telefono;Celular;Direccion;Localidad;Provincia;Codigo Postal;Pais;\n");
                echo ($objUsuario->getDatos()->getNroDoc() . "; ");
                echo ($objUsuario->getDatos()->getCodTelefono()." - ".$objUsuario->getDatos()->getTelefono() . "; ");
                echo ($objUsuario->getDatos()->getcelucodigo()." - ".$objUsuario->getDatos()->getcelunumero() . "; ");
                echo ($objPedido->getDireccion()->getDomicilio() ." ". $objPedido->getDireccion()->getNumero() ." ");
                echo ($objPedido->getDireccion()->getPiso() ." ");
                echo ($objPedido->getDireccion()->getPuerta() . "; " );
                echo($objPedido->getDireccion()->getPoblacion() . "; ");
         
                if($objPedido->getDireccion()->getIdProvincia() != "")
                {
                    $Prov = dmMicuenta::GetProvinciasById($objPedido->getDireccion()->getIdProvincia());
                    echo ($Prov[0]["Nombre"] . "; ");
                }                            
                echo($objPedido->getDireccion()->getCP() . "; ");
                if($objPedido->getDireccion()->getIdPais() != "")
                {
                    $Pais = dmMicuenta::GetPaisesById($objPedido->getDireccion()->getIdPais());
                    echo ($Pais[0]["Nombre"] . "; ");
                }
                echo("\n\n\n");
        }
        
    }
    catch(exception $e)
    {
        print $e->getMessage();
    }
?>