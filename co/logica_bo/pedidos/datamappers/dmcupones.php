<?
class dmCupones{

	public static function obtenerCupon(){
		$oConexion = Conexion::nuevo();
	    $oConexion->Abrir();
        $oConexion->setQuery("SELECT codigodebarra, id FROM formaspagos_codigosdebarra
        WHERE Pedido_id = 0 ORDER BY id ASC LIMIT 1");
        $Tabla = $oConexion->DevolverQuery();
        return $Tabla[0];
	}
/*
	public static function fechaVencimientoCupon($idPedido){
		$oConexion = Conexion::nuevo();
	    $oConexion->Abrir();
        $oConexion->setQuery("SELECT DATE_SUB(c.FechaFin, INTERVAL 8 HOUR) AS fechaVencimiento FROM pedidos AS p
JOIN campanias AS c ON p.IdCampania = c.IdCampania WHERE p.IdPedido = ".$idPedido." LIMIT 1");
        $Tabla = $oConexion->DevolverQuery();
        return $Tabla[0];
	}
*/

	public static function asignarCupon($idCupon, $idPedido, $fechaAsignacion, $fechaVencimiento){
		$oConexion = Conexion::nuevo();
	    $oConexion->Abrir();
        $oConexion->setQuery("UPDATE formaspagos_codigosdebarra SET
        Pedido_Id = ".mysql_real_escape_string($idPedido).",
        fecha_asignacion = '".mysql_real_escape_string($fechaAsignacion)."',
        fecha_vencimiento = '".mysql_real_escape_string($fechaVencimiento)."'
        WHERE id = ".mysql_real_escape_string($idCupon)." LIMIT 1");
        $Tabla = $oConexion->EjecutarQuery();
       	return $Tabla[0];
	}


	public static function cuponPagado($idPedido){
		$oConexion = Conexion::nuevo();
	    $oConexion->Abrir();
        $oConexion->setQuery("UPDATE formaspagos_codigosdebarra SET codigobarra_pagado = 1 WHERE Pedido_Id = ".mysql_real_escape_string($idPedido)." LIMIT 1");
        $oConexion->EjecutarQuery();
	}

	public static function getIdPedidoByIdCupon($idCupon){	
		$oConexion = Conexion::nuevo();
	    $oConexion->Abrir();
        $oConexion->setQuery("SELECT Pedido_Id AS IdPedido FROM formaspagos_codigosdebarra WHERE id = ".mysql_real_escape_string($idCupon)." LIMIT 1");
        $Tabla = $oConexion->DevolverQuery();
        return $Tabla[0]["IdPedido"];
	}

	public static function getIdCuponByIdPedido($idPedido){
		$oConexion = Conexion::nuevo();
	    $oConexion->Abrir();
        $oConexion->setQuery("SELECT codigodebarra FROM formaspagos_codigosdebarra WHERE Pedido_Id = ".mysql_real_escape_string($idPedido)." LIMIT 1");
        $Tabla = $oConexion->DevolverQuery();
        return $Tabla[0]["codigodebarra"];
	}
	
	public static function getIdPedidosbyArray($arrayCupones){
		$oConexion = Conexion::nuevo();
	    $oConexion->Abrir();
	    
	    if(count($arrayCupones) == 0)
	    	return array();
	    
	    $where = "";
	    $i = 0;
	    foreach($arrayCupones as $cupon){
	    	if($i!=0) $where .= " OR";
	    	$where .= " codigodebarra LIKE \"____".$cupon."_\"";
	    	$i++;
	    	
	    }
        $oConexion->setQuery("SELECT Pedido_Id FROM formaspagos_codigosdebarra WHERE ".$where);
        $Tabla = $oConexion->DevolverQuery();
        return $Tabla;		
	}

}

//echo Conexion::nuevo()->DevolverLog();
?>