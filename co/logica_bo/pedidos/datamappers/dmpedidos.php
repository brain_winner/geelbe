<?
class dmPedidos {

	public static function getPedidosCount($paramArray = array()) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			
	    $oConexion = Conexion::nuevo();
	    try
	    {
	        $oConexion->Abrir();
	        $oConexion->setQuery("SELECT count(*) as total 
								  FROM usuarios U
								  INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario
								  INNER JOIN campanias C ON P.IdCampania = C.IdCampania
								  LEFT JOIN formaspagos FP ON P.IdFormaPago = FP.IdFormaPago
								  LEFT JOIN mensajerias M ON P.IdMensajeria = M.IdMensajeria
								  LEFT JOIN estadospedidos EP ON P.IdEstadoPedidos = EP.IdEstadoPedido
								  LEFT JOIN datosusuariospersonales DU ON DU.IdUsuario = P.IdUsuario
								  WHERE ($filtersSQL)");
	        $Tabla = $oConexion->DevolverQuery();
	        ColumnaMoneda(&$Tabla, "Monto total");
	        $oConexion->Cerrar_Trans();
	        return $Tabla[0]['total'];
	    }
	    catch(MySQLException $e)
	    {
	        $oConexion->RollBack();
	        throw $e;
	    }
	}

	public static function getPedidosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
		$sortSQL = ListadoUtils::generateSortSQL($sortArray);
		$oConexion = Conexion::nuevo();
	    try
	    {
	        $oConexion->Abrir();
	        $oConexion->setQuery("SELECT  P.IdPedido as 'P.IdPedido', 
			                              DU.Apellido as 'DU.Apellido', 
										  DU.Nombre as 'DU.Nombre', 
										  DATE_FORMAT( P.Fecha, '%d/%m/%Y' ) as 'Fecha', 
										  C.Nombre as 'C.Nombre', 
										  DATE_FORMAT( P.FechaEnvio, '%d/%m/%Y' ) as 'FechaEnvio', 
										  M.IdMensajeria as 'M.IdMensajeria', 
										  M.Nombre as 'M.Nombre', 
										  EP.Descripcion as 'EP.Descripcion', 
										  P.IdEstadoPedidos as 'P.IdEstadoPedidos', 
										  P.IdFormaEnvio as 'P.IdFormaEnvio', 
										  FP.Descripcion as 'FP.Descripcion', 
										  Total as 'MontoTotal', 
										  U.IdUsuario as 'U.IdUsuario', 
										  U.NombreUsuario as 'U.NombreUsuario',
										  P.IdDescuento as 'P.IdDescuento'
								  FROM usuarios U
								  INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario
								  INNER JOIN campanias C ON P.IdCampania = C.IdCampania
								  LEFT JOIN formaspagos FP ON P.IdFormaPago = FP.IdFormaPago
								  LEFT JOIN mensajerias M ON P.IdMensajeria = M.IdMensajeria
								  LEFT JOIN estadospedidos EP ON P.IdEstadoPedidos = EP.IdEstadoPedido
								  LEFT JOIN datosusuariospersonales DU ON DU.IdUsuario = P.IdUsuario
								  WHERE ($filtersSQL) 
								  ORDER BY $sortSQL 
								  LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
	
	        $Tabla = $oConexion->DevolverQuery();
	        ColumnaMoneda(&$Tabla, "Monto total");
	        $oConexion->Cerrar_Trans();
	        return $Tabla;
	    }
	    catch(MySQLException $e)
	    {
	        $oConexion->RollBack();
	        throw $e;
	    }
	}

	public static function getPedidosPagosCount($paramArray = array()) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			
	    $oConexion = Conexion::nuevo();
	    try
	    {
	        $oConexion->Abrir();
	        $oConexion->setQuery("SELECT count(*) as total 
								  FROM usuarios U
								  INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario
								  INNER JOIN campanias C ON P.IdCampania = C.IdCampania
								  LEFT JOIN formaspagos FP ON P.IdFormaPago = FP.IdFormaPago
								  LEFT JOIN mensajerias M ON P.IdMensajeria = M.IdMensajeria
								  LEFT JOIN estadospedidos EP ON P.IdEstadoPedidos = EP.IdEstadoPedido
								  LEFT JOIN datosusuariospersonales DU ON DU.IdUsuario = P.IdUsuario
								  WHERE ($filtersSQL) AND P.IdEstadoPedidos IN (2,3,6)");
	        $Tabla = $oConexion->DevolverQuery();
	        ColumnaMoneda(&$Tabla, "Monto total");
	        $oConexion->Cerrar_Trans();
	        return $Tabla[0]['total'];
	    }
	    catch(MySQLException $e)
	    {
	        $oConexion->RollBack();
	        throw $e;
	    }
	}

	public static function getPedidosPagosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
		$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
		$oConexion = Conexion::nuevo();
	    try
	    {
	        $oConexion->Abrir();
	        $oConexion->setQuery("SELECT  P.IdPedido as 'P.IdPedido', 
			                              DU.Apellido as 'DU.Apellido', 
										  DU.Nombre as 'DU.Nombre', 
										  DATE_FORMAT( P.Fecha, '%d/%m/%Y' ) as 'Fecha', 
										  C.Nombre as 'C.Nombre', 
										  DATE_FORMAT( P.FechaEnvio, '%d/%m/%Y' ) as 'FechaEnvio', 
										  M.IdMensajeria as 'M.IdMensajeria', 
										  M.Nombre as 'M.Nombre', 
										  EP.Descripcion as 'EP.Descripcion', 
										  P.IdEstadoPedidos as 'P.IdEstadoPedidos', 
										  P.IdFormaEnvio as 'P.IdFormaEnvio', 
										  FP.Descripcion as 'FP.Descripcion', 
										  Total as 'MontoTotal', 
										  U.IdUsuario as 'U.IdUsuario', 
										  U.NombreUsuario as 'U.NombreUsuario'
								  FROM usuarios U
								  INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario
								  INNER JOIN campanias C ON P.IdCampania = C.IdCampania
								  LEFT JOIN formaspagos FP ON P.IdFormaPago = FP.IdFormaPago
								  LEFT JOIN mensajerias M ON P.IdMensajeria = M.IdMensajeria
								  LEFT JOIN estadospedidos EP ON P.IdEstadoPedidos = EP.IdEstadoPedido
								  LEFT JOIN datosusuariospersonales DU ON DU.IdUsuario = P.IdUsuario
								  WHERE ($filtersSQL) AND P.IdEstadoPedidos IN (2,3,6)
								  ORDER BY $sortSQL 
								  ".($offset !== false ? "LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage) : ''));
	
	        $Tabla = $oConexion->DevolverQuery();
	        ColumnaMoneda(&$Tabla, "Monto total");
	        $oConexion->Cerrar_Trans();
	        return $Tabla;
	    }
	    catch(MySQLException $e)
	    {
	        $oConexion->RollBack();
	        throw $e;
	    }
	}
	

	public static function hayStockReal($IdPedido){

		$IdPedido = intval($IdPedido);
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir();

		$oConexion->setQuery("SELECT pxp.IdCodigoProdInterno, p.IdPedido, SUM(Cantidad) as Cantidad, p.IdEstadoPedidos, FechaSistemaPago
		FROM pedidos AS p JOIN productos_x_pedidos as pxp ON pxp.IdPedido = p.IdPedido
		WHERE DATE_ADD(p.FechaSistemaPago, INTERVAL 6 MINUTE) > NOW()
		AND pxp.IdCodigoProdInterno IN
		(SELECT pxp2.IdCodigoProdInterno FROM productos_x_pedidos AS pxp2 WHERE pxp2.IdPedido = ".mysql_real_escape_string($IdPedido).")
		AND p.IdPedido <> ".mysql_real_escape_string($IdPedido)."
		AND p.IdEstadoPedidos = 0 GROUP BY pxp.IdCodigoProdInterno");

		$tablaStockTemp = $oConexion->DevolverQuery();

//		$oConexion->setQuery("SELECT Stock, Cantidad
//		FROM productos_x_proveedores
//		INNER JOIN  productos_x_pedidos ON productos_x_pedidos.IdCodigoProdInterno = productos_x_proveedores.IdCodigoProdInterno
//		WHERE IdPedido = ".$IdPedido);

        $oConexion->setQuery("SELECT productos_x_pedidos.IdCodigoProdInterno, Cantidad, Stock FROM productos_x_proveedores
		INNER JOIN  productos_x_pedidos ON productos_x_pedidos.IdCodigoProdInterno = productos_x_proveedores.IdCodigoProdInterno
		WHERE IdPedido = ".mysql_real_escape_string($IdPedido));
        $TablaStock = $oConexion->DevolverQuery();


        $stockDb = true;
        foreach($TablaStock as $fila)
        {
            if($fila["Stock"] < $fila["Cantidad"])
            {
                $stockDb = false;
            }
        }


		$stockDinamico = true;
        foreach($TablaStock as $fila)
        {

        	if(is_array($stock))
        	{
        		foreach($tablaStockTemp as $st){
        			if($st["IdCodigoProdInterno"] = $fila["IdCodigoProdInterno"]){
        				$fila["Cantidad"] -= $st["Cantidad"];
        			}
        		}
        	}

            if($fila["Stock"] < $fila["Cantidad"])
            {
               $stockDinamico = false;
            }
        }
        return array("db"=>$stockDb, "temporal"=>$stockDinamico);
	}



	public static function setFechaEntradaSistemaPago($IdPedido){
		$IdPedido = intval($IdPedido);
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		$oConexion->setQuery("UPDATE pedidos SET FechaSistemaPago = NOW() WHERE IdPedido = ".mysql_real_escape_string($IdPedido));
		$oConexion->EjecutarQuery();
	}

	public static function getEstadoInicial($IdPedido){
		$IdPedido = intval($IdPedido);

		$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		$oConexion->setQuery("SELECT IdEstadoPedidos, StockDescontado, ReglaAplicada, Total, IdUsuario FROM pedidos WHERE IdPedido =".mysql_real_escape_string($IdPedido));
		$Tabla = $oConexion->DevolverQuery();
		if(empty($Tabla[0])){
			return -1;
		}
		return $Tabla[0];
	}

   	public static function getDatosMailByIdPedido($idPedido){

    	$oConexion = Conexion::nuevo();
    	$oConexion->Abrir();
   		$oConexion->setQuery("SELECT ep.Descripcion AS estadoPedido, u.NombreUsuario AS emailUsuario,dup.Nombre AS nombreUsuario, dup.Apellido AS apellidoUsuario, up.IdUsuario AS IdPadrino, up.NombreUsuario AS emailPadrino, dupp.Nombre AS nombrePadrino, dupp.Apellido AS apellidoPadrino, c.Nombre AS nombreVidriera, p.IdPedido AS numeroPedido, p.NroEnvio as numeroTrackingDHL, c.TiempoEntregaIn, c.TiempoEntregaFn, codbar.codigodebarra, p.Total AS total, codbar.fecha_vencimiento AS fechaVencimiento, u.IdUsuario, CONCAT(dir.Domicilio,' ',dir.Numero,' ',dir.Piso,' ',dir.Puerta) direccion, prov.Nombre departamento, ciu.Descripcion ciudad, fp.Descripcion formapago, p.GastosEnvio envio
FROM pedidos AS p
JOIN usuarios AS u ON p.IdUsuario = u.IdUsuario
JOIN datosusuariospersonales AS dup ON u.IdUsuario = dup.IdUsuario
JOIN datosusuariosdirecciones AS dir ON u.IdUsuario = dir.IdUsuario
LEFT JOIN provincias AS prov ON prov.IdProvincia = dir.IdProvincia
LEFT JOIN ciudades AS ciu ON ciu.IdCiudad = dir.IdCiudad
LEFT JOIN formaspagos fp ON fp.IdFormaPago = p.IdFormaPago
JOIN campanias AS c ON c.IdCampania = p.IdCampania
JOIN estadospedidos AS ep ON p.IdEstadoPedidos = ep.IdEstadoPedido
LEFT JOIN usuarios AS up ON u.Padrino = up.NombreUsuario
LEFT JOIN datosusuariospersonales AS dupp ON dupp.IdUsuario = up.IdUsuario
LEFT JOIN formaspagos_codigosdebarra AS codbar ON codbar.Pedido_Id = p.IdPedido
WHERE IdPedido = ".mysql_real_escape_string($idPedido)." LIMIT 1");

   		$tabla = $oConexion->DevolverQuery();
   		if(empty($tabla[0])){
   			throw new MySQLException("no existe este pedido($idPedido)");
   		}
   		return $tabla[0];
   	}

	public static function getEstadosPedidos(){
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		$oConexion->setQuery("SELECT IdEstadoPedido, Descripcion FROM estadospedidos LIMIT 1 ORDER BY IdEstadoPedido");
		$Tabla = $oConexion->DevolverQuery();
		return $Tabla[0];
	}

	public static function DescontarStock($idPedido){
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		$oConexion->setQuery("SELECT productos_x_proveedores.IdCodigoProdInterno, Stock, Cantidad FROM productos_x_proveedores INNER JOIN  productos_x_pedidos ON productos_x_pedidos.IdCodigoProdInterno = productos_x_proveedores.IdCodigoProdInterno WHERE IdPedido = ".mysql_real_escape_string($idPedido));
		$Tabla = $oConexion->DevolverQuery();
		foreach($Tabla as $fila)
		{
			if($fila["Stock"] < $fila["Cantidad"])
			{
				throw new FaltaSockException("El pedido ".str_pad($idPedido, 8, 0, 0)." no puede ser entregado por falta de stock.", 2);
			}
			else
			{
				 $oConexion->setQuery("UPDATE productos_x_proveedores SET Stock = Stock - ".$fila["Cantidad"]." WHERE IdCodigoProdInterno = ".$fila["IdCodigoProdInterno"]);
				 $oConexion->EjecutarQuery();
			}
			
			if(($fila['Stock'] - $fila['Cantidad']) <= dmPedidos::getMinStock($fila["IdCodigoProdInterno"]))
				self::notificarMinStock($fila["IdCodigoProdInterno"]);
		}
	}
	
	public static function getMinStock($idProducto) {
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		$oConexion->setQuery("SELECT * FROM productos_x_proveedores WHERE IdCodigoProdInterno = ".mysql_real_escape_string($idProducto));
		$Tabla = $oConexion->DevolverQuery();
		
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		$oConexion->setQuery("SELECT * FROM productos WHERE IdProducto = ".mysql_real_escape_string($Tabla[0]['IdProducto']));
		$Tabla = $oConexion->DevolverQuery();
		
		return $Tabla[0]['ultimos'];
	}
	
	public static function notificarMinStock($idProducto) {
		
		// Do nothing for now
		return false;
		
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		$oConexion->setQuery("SELECT * FROM productos_x_proveedores WHERE IdCodigoProdInterno = ".mysql_real_escape_string($idProducto));
		$Tabla = $oConexion->DevolverQuery();
		
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		$oConexion->setQuery("SELECT * FROM productos WHERE IdProducto = ".mysql_real_escape_string($Tabla[0]['IdProducto']));
		$Tabla = $oConexion->DevolverQuery();
		
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		$oConexion->setQuery("SELECT * FROM usuarios WHERE IdPerfil IN (18, 2)");
		$Tabla2 = $oConexion->DevolverQuery();

		foreach($Tabla2 as $i) {
					
			$emailBuilder = new EmailBuilder();
			$emailData = $emailBuilder->generateBajoStock($i['NombreUsuario'], $Tabla[0]['Nombre'], UrlResolver::getBaseUrl().'bo/productos/ABM.php?IdProducto='.$Tabla[0]['IdProducto']);
			
			$emailSender = new EmailSender();
			$emailSender->sendEmail($emailData);			
			
		}

	}

	/**
	* @desc Dice si hay stock para ese pedido
	* @return bool
	*/
	public static function HayStock($IdPedido)
	{
	    $oConexion = Conexion::nuevo();
	    $oConexion->Abrir();
        $oConexion->setQuery("SELECT Stock, Cantidad
		FROM productos_x_proveedores
		INNER JOIN  productos_x_pedidos ON productos_x_pedidos.IdCodigoProdInterno = productos_x_proveedores.IdCodigoProdInterno
		WHERE IdPedido = ".mysql_real_escape_string($IdPedido));
        $Tabla = $oConexion->DevolverQuery();
        foreach($Tabla as $fila)
        {
            if($fila["Stock"] < $fila["Cantidad"])
            {
                return false;
            }
        }
        return true;
	}

	/**
	* RestituirStock Actualiza la base de stock, devolviendo la mercaderia.
	*
	*	@param int $idPedido el ID del pedido a restituir.
	*	@param object $oConexion el objeto de la transaccion actual.
	**/
	public static function RestituirStock($idPedido){
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		$oConexion->setQuery("SELECT productos_x_proveedores.IdCodigoProdInterno, Stock, Cantidad FROM productos_x_proveedores INNER JOIN  productos_x_pedidos ON productos_x_pedidos.IdCodigoProdInterno = productos_x_proveedores.IdCodigoProdInterno WHERE IdPedido = ".mysql_real_escape_string($idPedido));
		$Tabla = $oConexion->DevolverQuery();
		foreach($Tabla as $fila){
			$oConexion->setQuery("UPDATE productos_x_proveedores SET Stock = Stock + ".$fila["Cantidad"]."
			WHERE IdCodigoProdInterno = ".$fila["IdCodigoProdInterno"]);
			$oConexion->EjecutarQuery();
		}
	}

	///////////////////////////////////////////////////


public static function getPedidosbyIdUsuarioCount($IdUsuario, $paramArray = array()) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			
	    $oConexion = Conexion::nuevo();
	    try
	    {
	        $oConexion->Abrir();
	        $oConexion->setQuery("SELECT count(*) as total 
								  FROM usuarios U
								  INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario
								  INNER JOIN campanias C ON P.IdCampania = C.IdCampania
								  LEFT JOIN formaspagos FP ON P.IdFormaPago = FP.IdFormaPago
								  LEFT JOIN mensajerias M ON P.IdMensajeria = M.IdMensajeria
								  LEFT JOIN estadospedidos EP ON P.IdEstadoPedidos = EP.IdEstadoPedido
								  LEFT JOIN datosusuariospersonales DU ON DU.IdUsuario = P.IdUsuario
								  WHERE U.IdUsuario = ".mysql_real_escape_string($IdUsuario)."
								  AND ($filtersSQL)");
	        $Tabla = $oConexion->DevolverQuery();
	        ColumnaMoneda(&$Tabla, "Monto total");
	        $oConexion->Cerrar_Trans();
	        return $Tabla[0]['total'];
	    }
	    catch(MySQLException $e)
	    {
	        $oConexion->RollBack();
	        throw $e;
	    }
	}

	public static function getPedidosbyIdUsuarioPaginados($IdUsuario, $paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
		$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
		$oConexion = Conexion::nuevo();
	    try
	    {
	        $oConexion->Abrir();
	        $oConexion->setQuery("SELECT  P.IdPedido as 'P.IdPedido', 
			                              DU.Apellido as 'DU.Apellido', 
										  DU.Nombre as 'DU.Nombre', 
										  DATE_FORMAT( P.Fecha, '%d/%m/%Y' ) as 'Fecha', 
										  C.Nombre as 'C.Nombre', 
										  DATE_FORMAT( P.FechaEnvio, '%d/%m/%Y' ) as 'FechaEnvio', 
										  M.IdMensajeria as 'M.IdMensajeria', 
										  M.Nombre as 'M.Nombre', 
										  EP.Descripcion as 'EP.Descripcion', 
										  P.IdEstadoPedidos as 'P.IdEstadoPedidos', 
										  P.IdFormaEnvio as 'P.IdFormaEnvio', 
										  FP.Descripcion as 'FP.Descripcion', 
										  Total as 'MontoTotal', 
										  U.IdUsuario as 'U.IdUsuario', 
										  U.NombreUsuario as 'U.NombreUsuario'
								  FROM usuarios U
								  INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario
								  INNER JOIN campanias C ON P.IdCampania = C.IdCampania
								  LEFT JOIN formaspagos FP ON P.IdFormaPago = FP.IdFormaPago
								  LEFT JOIN mensajerias M ON P.IdMensajeria = M.IdMensajeria
								  LEFT JOIN estadospedidos EP ON P.IdEstadoPedidos = EP.IdEstadoPedido
								  LEFT JOIN datosusuariospersonales DU ON DU.IdUsuario = P.IdUsuario
								  WHERE U.IdUsuario = ".mysql_real_escape_string($IdUsuario)."
								  AND ($filtersSQL) 
								  ORDER BY $sortSQL 
								  LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
	
	        $Tabla = $oConexion->DevolverQuery();
	        ColumnaMoneda(&$Tabla, "Monto total");
	        $oConexion->Cerrar_Trans();
	        return $Tabla;
	    }
	    catch(MySQLException $e)
	    {
	        $oConexion->RollBack();
	        throw $e;
	    }
	}

	public static function getPedidosAceptados()
    {
        $oConexion = Conexion::nuevo();
        $oConexion->Abrir();
        $oConexion->setQuery("SELECT P.IdPedido AS 'Pedido', P.IdDescuento, CONCAT( DU.Apellido, ' ', DU.Nombre ) AS 'Apellido y nombre', DATE_FORMAT( P.Fecha, '%d/%m/%Y' ) AS Fecha, C.Nombre AS 'Nombre campa�a', DATE_FORMAT( P.FechaEnvio, '%d/%m/%Y' ) AS 'Fecha de env�o', M.IdMensajeria, M.Nombre AS NombreMensajeria, EP.Descripcion, P.IdEstadoPedidos, P.IdFormaEnvio, Total AS 'Monto total'
            FROM usuarios U
            INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario
            INNER JOIN campanias C ON P.IdCampania = C.IdCampania
            LEFT JOIN mensajerias M ON P.IdMensajeria = M.IdMensajeria
            LEFT JOIN estadospedidos EP ON P.IdEstadoPedidos = EP.IdEstadoPedido
            INNER JOIN datosusuariospersonales DU ON DU.IdUsuario = P.IdUsuario
            WHERE IdEstadoPedidos = 2");
            $Tabla = $oConexion->DevolverQuery();
            return $Tabla;
    }

	public static function getPedidosAceptadosConDescuento($IdDescuento)
    {
        $oConexion = Conexion::nuevo();
        $oConexion->Abrir();
        $oConexion->setQuery("SELECT P.IdPedido AS 'Pedido', U.NombreUsuario
            FROM usuarios U
            INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario
            WHERE IdEstadoPedidos = 2 AND IdDescuento =".$IdDescuento);
            $Tabla = $oConexion->DevolverQuery();
            return $Tabla;
    }

    public static function getPedidosAceptadosByCamp($idCampania)
    {
        $oConexion = Conexion::nuevo();
        $oConexion->Abrir();
        $oConexion->setQuery("SELECT P.IdPedido AS 'Pedido', CONCAT( DU.Apellido, ' ', DU.Nombre ) AS 'Apellido y nombre', DATE_FORMAT( P.Fecha, '%d/%m/%Y' ) AS Fecha, C.Nombre AS 'Nombre campa�a', DATE_FORMAT( P.FechaEnvio, '%d/%m/%Y' ) AS 'Fecha de env�o', M.IdMensajeria, M.Nombre AS NombreMensajeria, EP.Descripcion, P.IdEstadoPedidos, P.IdFormaEnvio, Total AS 'Monto total'
            FROM usuarios U
            INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario
            INNER JOIN campanias C ON P.IdCampania = C.IdCampania
            LEFT JOIN mensajerias M ON P.IdMensajeria = M.IdMensajeria
            LEFT JOIN estadospedidos EP ON P.IdEstadoPedidos = EP.IdEstadoPedido
            INNER JOIN datosusuariospersonales DU ON DU.IdUsuario = P.IdUsuario
            WHERE C.IdCampania = ".mysql_real_escape_string($idCampania));
            $Tabla = $oConexion->DevolverQuery();
            return $Tabla;
    }

	public static function getPedidoById($IdPedido)
    {
    	// require_once dirname(__FILE__).'/../clases/clspedidos.php';
        $oConexion = Conexion::nuevo();
        $oConexion->Abrir();
        $objPedidos = new MySQL_Pedidos();
        $objPedidos->setIdPedido($IdPedido);
        $oConexion->setQuery($objPedidos->getById());
        $Tabla = $oConexion->DevolverQuery();
        if(isset($Tabla[0]))
        {
            $objPedidos->__setByArray($Tabla[0]);
            $objDireccion = new MySQL_PedidosDirecciones();
            $objDireccion->setIdPedido($objPedidos->getIdPedido());
            $objDireccion->setIdUsuario($objPedidos->getIdUsuario());
            $oConexion->setQuery($objDireccion->getById());
            $Tabla = $oConexion->DevolverQuery();
            if(isset($Tabla[0]))
                $objDireccion->__setByArray($Tabla[0]);
            $objPedidos->setDireccion($objDireccion);

            $oConexion->setQuery("SELECT IdPedido, IdCodigoProdInterno, Cantidad, Precio FROM productos_x_pedidos WHERE IdPedido = " .$objPedidos->getIdPedido());
            $Tabla = $oConexion->DevolverQuery();
            foreach($Tabla as $fila)
            {
                $objProducto = new productos_x_pedidos();
                $objProducto->__setByArray($fila);
                $objPedidos->setProducto($objProducto);
            }
        }
        return $objPedidos;
	}
	//
	public static function getInfoPedido($IdPedido)
    {
        $oConexion = Conexion::nuevo();
        $oConexion->Abrir();
        $T = array();
        $oConexion->setQuery("SELECT 'FormaPago' as Id, Descripcion FROM formaspagos WHERE IdFormaPago = (SELECT IdFormaPago FROM pedidos WHERE IdPedido =".mysql_real_escape_string($IdPedido).")");
        $Tabla = $oConexion->DevolverQuery();
        $T["FormaPago"]=$Tabla[0];
        $oConexion->setQuery("SELECT 'Estado' as Id, Descripcion FROM estadospedidos WHERE IdEstadoPedido = (SELECT IdEstadoPedidos FROM pedidos WHERE IdPedido =".mysql_real_escape_string($IdPedido).")");
        $Tabla = $oConexion->DevolverQuery();
        $T["Estado"]=$Tabla[0];
        return $T;
	}

	public static function cambiarEstado($idPedido,$estadoFinal){
    		$oConexion = Conexion::nuevo();
    		$oConexion->Abrir_Trans();

    		$set = "";

    		$i = 0;

    		foreach($estadoFinal as $columna => $valor){
    			if($i!=0){
    				$set .= ", ";
    			}
    			$set .= $columna." = '".$valor."'";
    			$i++;
    		}

    		$oConexion->setQuery("UPDATE pedidos SET ".$set."  WHERE IdPedido=" . $idPedido);
    		$oConexion->EjecutarQuery();
    		$oConexion->Cerrar_Trans();
    		
    }

    public static function save(MySQL_Pedidos $objPedido, $BonoImporte, MySQL_PedidosDirecciones $objDireccionPedidos)
    {

        $oConexion = Conexion::nuevo();
        try
        {	
        	$oConexion->Abrir();
            $oConexion->Abrir_Trans();
            $oConexion->setQuery($objPedido->getInsert());
            $oConexion->EjecutarQuery();
            if($objPedido->getIdPedido() == 0)
            {
                $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                $ID = $oConexion->DevolverQuery();
                $oConexion->setQuery("SELECT * FROM pedidos WHERE IdPedido = ".mysql_real_escape_string($ID[0]["Id"]));
                $objPedido->setIdPedido($ID[0]["Id"]);
            }
            if(!is_null($objDireccionPedidos)) {  
	            $objDireccionPedidos->setIdPedido($objPedido->getIdPedido());
    	        $oConexion->setQuery($objDireccionPedidos->getInsert());
        	    $oConexion->EjecutarQuery();
        	   }
            if ($BonoImporte>0)
            {
                $oConexion->setQuery("SELECT * FROM( SELECT A.*, IF(SUM(BP.Importe) IS NULL,0,SUM(BP.Importe)) AS Importe FROM ( SELECT BU.IdBono, CONCAT( 'Bono obtenido por ', ' ', TR.Descripcion ) AS Descripcion, UNIX_TIMESTAMP( BU.Fecha ) + RN.Caducidad * ( 24 *60 *60 ) AS FechaCaducidad, Aplica1 AS Valor, IdEstadoBono FROM bonos_x_usuarios BU INNER JOIN reglas_aplicadas RA ON BU.IdReglaAplicada = RA.IdReglaAplicada INNER JOIN reglasnegocio RN ON RA.IdRegla = RN.IdRegla INNER JOIN tiposbeneficios TB ON TB.IdBeneficio = RN.Beneficio1 INNER JOIN tiposreglas TR ON TR.IdTipoRegla = RN.IdTipoRegla INNER JOIN usuarios U ON BU.IdUsuario = U.IdUsuario WHERE BU.IdEstadoBono = 0 AND U.IdUsuario = ".$objPedido->getIdUsuario()." AND RN.Beneficio1 =1 AND BU.IdEstadoBono = 0 AND ( UNIX_TIMESTAMP( BU.Fecha ) + RN.Caducidad * ( 24 *60 *60 ) > UNIX_TIMESTAMP(now())  ))A  LEFT JOIN bonos_x_pedidos BP ON BP.IdBono = A.IdBono GROUP BY A.IdBono, A.Descripcion, A.FechaCaducidad, A.Valor, A.IdEstadoBono)B WHERE NOT Importe = Valor AND IdEstadoBono =0");
                $Tabla = $oConexion->DevolverQuery();
                $i=0;
                while($BonoImporte > 0 && $i<count($Tabla))
                {
                    $Importe = $Tabla[$i]["Valor"] - $Tabla[$i]["Importe"];
                    if($BonoImporte <= $Importe)
                    {
                        $Importe = $BonoImporte;
                    }
                    $BonoImporte = abs($Importe - $BonoImporte);
                    $oConexion->setQuery("INSERT INTO bonos_x_pedidos(IdPedido, IdBono, Importe) VALUES (".$objPedido->getIdPedido().", ".$Tabla[$i]["IdBono"].", ".$Importe.")");
                    $oConexion->EjecutarQuery();
                    $i++;
                }
            }


            foreach ($objPedido->getProductos() as $Producto)
            {
                $Producto->setIdPedido($objPedido->getIdPedido());
                $oConexion->setQuery($Producto->getInsert());
                $oConexion->EjecutarQuery();
            }
            $oConexion->Cerrar_Trans();
            return $objPedido->getIdPedido();
        }
        catch (MySQLException $e)
        {
            $oConexion->RollBack();
            throw $e;
        }
    }
    
    public static function restituirBonos($idPedido) {
    	$oConexion = Conexion::nuevo();
        $oConexion->setQuery("DELETE FROM bonos_x_pedidos where IdPedido = ".mysql_real_escape_string($idPedido));
        $oConexion->EjecutarQuery();
    }

    public static function getUsuariosPedidosPendientes()
        {
            $oConexion = Conexion::nuevo();
            $oConexion->Abrir();
            $oConexion->setQuery("SELECT P.IdUsuario,P.IdEstadoPedidos, P.IdPedido, P.Total, C.Nombre
            FROM pedidos AS P INNER JOIN campanias AS C ON P.IdCampania = C.IdCampania WHERE P.IdEstadoPedidos in (0,1,8) AND P.Fecha between (now() - INTERVAL 30 DAY) and now()");
            $Tabla = $oConexion->DevolverQuery();
    	    return $Tabla;
        }

    public static function getPedidoByIdDM($IdPedido)
        {
            $oConexion = Conexion::nuevo();
            $oConexion->Abrir();
            $oConexion->setQuery("SELECT IdEstadoPedidos, IdPedido FROM pedidos WHERE IdPedido = ".mysql_real_escape_string($IdPedido)." LIMIT 1");
            $Tabla = $oConexion->DevolverQuery();
            return $Tabla;
        }

    public static function getIdUsuarioByPedido($IdPedido)
        {
            $oConexion = Conexion::nuevo();
            $oConexion->Abrir();
            $oConexion->setQuery("select IdUsuario from pedidos where IdPedido=".mysql_real_escape_string($IdPedido));
            $Tabla = $oConexion->DevolverQuery();
            return $Tabla;
        }

	public static function getIdCategoriaByIdPedido($IdPedido)
        {
            $oConexion = new Conexion();
            try
            {  
                $objPedidos = new MySQL_Pedidos();
                $objPedidos->setIdPedido($IdPedido);
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objPedidos->getById());
                $Tabla = $oConexion->DevolverQuery();
                $IdCampania = 0;
                if(isset($Tabla[0]))
                {   
                 $IdCampania = $Tabla[0]['IdCampania'];
                }
                $oConexion->Cerrar_Trans();
                return $IdCampania;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
	}
        
    public static function getPedidosDirecciones($IdPedido){
    	$oConexion = Conexion::nuevo();
        $oConexion->Abrir();
        $oConexion->setQuery("SELECT * FROM pedidos_direcciones WHERE IdPedido = ". $IdPedido ." LIMIT 1");
        $Tabla = $oConexion->DevolverQuery();
        return $Tabla[0];

    }

    public static function getRecordarPago(){
    	$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		$oConexion->setQuery("SELECT p.IdPedido FROM pedidos AS p
		JOIN campanias AS c ON p.IdCampania = c.IdCampania
		WHERE p.Fecha > date_sub(NOW(),INTERVAL 2 DAY) AND
		p.Fecha < date_sub(NOW(),INTERVAL 1 DAY)
		AND p.IdEstadoPedidos IN (0,4)
		AND c.FechaInicio < NOW() AND c.FechaFin > date_sub(NOW(), INTERVAL 1 DAY)");
		$Tabla = $oConexion->DevolverQuery();
		return $Tabla;
    }
    
    public static function DHLCSV($csvData) {
    
    	$csvData = explode("\n", $csvData);
    	$updated = array();
    	$pedidosError = array();
    	foreach($csvData as $i => $pedido) {
    	
    		$pedido = explode(";", $pedido);
			if(is_numeric(trim($pedido[0])) && is_numeric(trim($pedido[1]))) {

				$estado = dmPedidos::getEstadoInicial($pedido[0]);
				$estadoNuevo = (isset($_GET['entregados']) ? 6 : 3);

				if($estado === -1) {
					$pedidosError[] = $pedido[0];
				} else /*if($estado['IdEstadoPedidos'] != $estadoNuevo)*/ {			
					$actualizador = actualizarPedidos::nuevo(trim($pedido[0]), $estadoNuevo);
					$actualizador->setDHL(trim($pedido[1]));
					$actualizador->Actualizar();
					
					//Guardar historial
					$oConexion = Conexion::nuevo();
		            $oConexion->Abrir();
		            $oConexion->setQuery("INSERT INTO pedidos_x_envios VALUES (".$pedido[0].", NOW(), '".trim($pedido[1])."', ".$estadoNuevo.")");
		            $oConexion->EjecutarQuery();
					
					$updated[] = $pedido[0];
				}
			}
			else{
			if($pedido[0] != null){
				$pedidosError[] = $pedido[0];
				}
			}
    	}

    	$resultados = array();
    	$resultados["updated"] = $updated; 
    	$resultados["pedidosError"] = $pedidosError; 
    	
    	return $resultados;
    
    }
    
    public static function addProduct($idPedido, $idProduct, $q) {
	    
	    $oConexion = new Conexion();
        try
        {  	
        	
        	$objPP = dmProductos::getByIdCodigoProdInterno($idProduct);
        	$objProducto = dmProductos::getById($objPP->getIdProducto());
        	$precio = ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento());
        	
        	//Guardar producto
            $oConexion->Abrir_Trans();
            $oConexion->setQuery("INSERT INTO productos_x_pedidos VALUES (".mysql_real_escape_string($idPedido).", ".mysql_real_escape_string($idProduct).", ".mysql_real_escape_string($q).", ".mysql_real_escape_string($precio).")");
            $oConexion->EjecutarQuery();
            $oConexion->Cerrar_Trans();
            
            //Actualizar total
            $oConexion->Abrir_Trans();
            $oConexion->setQuery("UPDATE pedidos SET Total = Total + ".mysql_real_escape_string(($precio*$q))." WHERE IdPedido = ".mysql_real_escape_string($idPedido));
            $oConexion->EjecutarQuery();
            $oConexion->Cerrar_Trans();
            
            //Actualizar stock
            $oConexion->Abrir_Trans();
            $oConexion->setQuery("UPDATE productos_x_proveedores SET Stock = Stock - ".$q." WHERE IdCodigoProdInterno = ".mysql_real_escape_string($idProduct));
			 $oConexion->EjecutarQuery();
            $oConexion->Cerrar_Trans();
            
            dmPedidos::recalcularTotal($idPedido);
        }
        catch (MySQLException $e)
        {
            $oConexion->RollBack();
            throw $e;
        }
	    
    }
    
    public static function updateQuantities($idPedido, $quants) {
        
 	    $oConexion = new Conexion();
 	    foreach($quants as $idCodProd => $q) {
           //Actualizar stock
            $oConexion->Abrir_Trans();
            $oConexion->setQuery("SELECT Cantidad FROM productos_x_pedidos WHERE IdPedido = ".mysql_real_escape_string($idPedido)." AND IdCodigoProdInterno = ".mysql_real_escape_string($idCodProd));
            $datos = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            $oldq = $datos[0]['Cantidad'];

            if($oldq != $q) {
            	
            	if($q > $oldq)
            		$stock = ' - '.($q-$oldq);
            	else
            		$stock = ' + '.($oldq-$q);
            
	            $oConexion->Abrir_Trans();
	            $oConexion->setQuery("UPDATE productos_x_proveedores SET Stock = Stock ".$stock." WHERE IdCodigoProdInterno = ".mysql_real_escape_string($idCodProd));
				 $oConexion->EjecutarQuery();
	            $oConexion->Cerrar_Trans();
	 	    
		    	//Actualizar total
	            $oConexion->Abrir_Trans();
	            $oConexion->setQuery("UPDATE productos_x_pedidos SET Cantidad = ".mysql_real_escape_string($q)." WHERE IdPedido = ".mysql_real_escape_string($idPedido)." AND IdCodigoProdInterno = ".mysql_real_escape_string($idCodProd));
	            $oConexion->EjecutarQuery();
	            $oConexion->Cerrar_Trans();
	            
	        }
    	}

        dmPedidos::recalcularTotal($idPedido);
    }
    
    public static function deleteProduct($idPedido, $idProducto) {
    
        //Actualizar stock
  	    $oConexion = new Conexion();
       $oConexion->Abrir_Trans();
        $oConexion->setQuery("SELECT Cantidad FROM productos_x_pedidos WHERE IdPedido = ".mysql_real_escape_string($idPedido)." AND IdCodigoProdInterno = ".mysql_real_escape_string($idProducto));
        $datos = $oConexion->DevolverQuery();
        $oConexion->Cerrar_Trans();
        $oldq = $datos[0]['Cantidad'];
        
        $oConexion->Abrir_Trans();
        $oConexion->setQuery("UPDATE productos_x_proveedores SET Stock = Stock + ".$oldq." WHERE IdCodigoProdInterno = ".mysql_real_escape_string($idProducto));
		 $oConexion->EjecutarQuery();
        $oConexion->Cerrar_Trans();
    
        $oConexion->Abrir_Trans();
        $oConexion->setQuery("DELETE FROM productos_x_pedidos WHERE IdPedido = ".mysql_real_escape_string($idPedido)." AND IdCodigoProdInterno = ".mysql_real_escape_string($idProducto));
        $oConexion->EjecutarQuery();
        $oConexion->Cerrar_Trans();
        
        dmPedidos::recalcularTotal($idPedido);
    } 

    public static function recalcularTotal($idPedido) {
	    //Actualizar total
  	    $oConexion = new Conexion();
       $oConexion->Abrir_Trans();
        $oConexion->setQuery("SELECT SUM(Precio*Cantidad) p FROM productos_x_pedidos WHERE IdPedido = ".mysql_real_escape_string($idPedido));
        $p = $oConexion->DevolverQuery();
        $oConexion->Cerrar_Trans();
        $total = floatval($p[0]['p']);
    	 
  	    $oConexion = new Conexion();
       $oConexion->Abrir_Trans();
        $oConexion->setQuery("SELECT Importe FROM bonos_x_pedidos WHERE IdPedido = ".mysql_real_escape_string($idPedido));
        $p = $oConexion->DevolverQuery();
        $oConexion->Cerrar_Trans();
        if(count($p) > 0)
	        $total -= floatval($p[0]['Importe']);

        $oConexion->Abrir_Trans();
        $oConexion->setQuery("UPDATE pedidos SET Total = ".mysql_real_escape_string($total)." + GastosEnvio WHERE IdPedido = ".mysql_real_escape_string($idPedido));
        $oConexion->EjecutarQuery();
        $oConexion->Cerrar_Trans();
    }


}
?>
