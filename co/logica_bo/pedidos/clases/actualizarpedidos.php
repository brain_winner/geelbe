<?
set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
require_once 'Zend/Cache.php'; 

abstract class actualizarPedidos{

    	public static function nuevo($idPedido,$idEstadoPedidos){
    		switch($idEstadoPedidos){
    			case 0:
    				$obj = new actualizarPedidos_EsperandoSistemaPago($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 1:
    				$obj = new actualizarPedidos_PagoProcesando($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 2:
    				$obj = new actualizarPedidos_Aceptado($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 3:
    				$obj = new actualizarPedidos_EnvioProgramado($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 4:
    				$obj = new actualizarPedidos_PagoRechazado($idPedido,$idEstadoPedidos);
    				return $obj;
    			case 5:
    				$obj = new actualizarPedidos_Anulado($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 6:
    				$obj = new actualizarPedidos_Entregado($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 7:
    				$obj = new actualizarPedidos_PagoOffline($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 8:
    				$obj = new actualizarPedidos_EsperandoCuponPago($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 9:
    				$obj = new actualizarPedidos_PedidoInterno($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 10:
    				$obj = new actualizarPedidos_StockInsuficiente($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 11:
    				$obj = new actualizarPedidos_Reversado($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 12:
    				$obj = new actualizarPedidos_Garantia($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 13:
    				$obj = new actualizarPedidos_Devolucion($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    			case 14:
    				$obj = new actualizarPedidos_GCash($idPedido,$idEstadoPedidos);
    				return $obj;
    			break;
    		}

    	}

    	public $oConexion;
    	protected $estadoInicial;
    	protected $idPedido;
    	protected $estadoFinal;
    	protected $padrino;


    	public function __construct($idPedido,$idEstadoPedidos){
    		$this->oConexion = Conexion::nuevo();
    		$this->oConexion->Abrir_Trans();
    		$this->estadoInicial = dmPedidos::getEstadoInicial($idPedido);

    		$this->idPedido = $idPedido;
    		$this->estadoFinal["IdEstadoPedidos"] = $idEstadoPedidos;

    		if(empty($this->estadoInicial)) throw new MySQLException("No Existe ese Id de Pedido");

    		foreach($this->estadoInicial as $indice => $valor){
    			if(empty($this->estadoFinal[$indice])){
    				$this->estadoFinal[$indice] = $valor;
    			}

    		}
    	}

    	protected function hayStock(){
		    if($this->estadoInicial["StockDescontado"]==0 && !dmPedidos::HayStock($this->idPedido)){
		    	throw new FaltaSockException("No hay stock para el pedido ".$this->idPedido);
		    }
		}

		/**
		 * Stock en base de datos + transacciones en tiempo real
		 *
		 */
		protected function hayStockReal(){
			$stock = dmPedidos::hayStockReal($this->idPedido);
		    if($stock["db"]&&$stock["temporal"]){
		    	return true;
		    }else{
		    	throw new FaltaSockException("No hay stock tempora para el pedido".$this->idPedido);
		    }
		}

    	protected function darCredito(){
    		if($this->estadoInicial["ReglaAplicada"]==0){
		     	try{
    			$GLOBALS["objReglaNegocio"]->Apadrinamiento($this->estadoInicial["IdUsuario"], $this->estadoInicial["Total"]);
		 		$this->estadoFinal["ReglaAplicada"] = 1;
		     	}
		     	catch(Exception $e){}
    		}
    	}
    	
    	protected function restituirCredito() {
    		dmPedidos::restituirBonos($this->idPedido);
    		
    		// Invalido la cache del credito de usuario
            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		    $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
		    $cache->remove('cache_credito_usuario_'.$this->estadoInicial["IdUsuario"]);
    	}

		protected function descontarStock(){
			if($this->estadoInicial["StockDescontado"] == 0){
				$this->estadoFinal["StockDescontado"] = 1;
				dmPedidos::DescontarStock($this->idPedido);
				
				$IdCampania = dmPedidos::getIdCategoriaByIdPedido($this->idPedido);
				$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->clean(
				    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
				    array('group_cache_catalogo_campania_'.$IdCampania)
				);
			}
		}

		protected function restituirStock(){
			if($this->estadoInicial["StockDescontado"] == 1){
			 	$this->estadoFinal["StockDescontado"] = 0;
			 	dmPedidos::RestituirStock($this->idPedido);
				
				$IdCampania = dmPedidos::getIdCategoriaByIdPedido($this->idPedido);
				$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->clean(
				    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
				    array('group_cache_catalogo_campania_'.$IdCampania)
				);
			}
		}

		protected function actualizarEstado(){
			dmPedidos::cambiarEstado($this->idPedido,$this->estadoFinal);
		}

		abstract protected function guardarTransaccionHistorico();

		abstract protected function ActualizarDb();

		protected function enviarEmail(){
			$Sender = new clsMailerPedidos($this->email,$this->idPedido);
			$Sender->enviar();
		}

		public function Actualizar(){
    			$this->ActualizarDb();
    			$this->guardarTransaccionHistorico();
    			$this->enviarEmail();
    	}

	}

    class actualizarPedidos_EsperandoSistemaPago extends actualizarPedidos{

    	protected function guardarTransaccionHistorico(){
    		//dmPedidos::guardarPedidosHistorico($idPedido);
    	}

		protected function ActualizarDb(){
			$this->hayStock();
			$this->actualizarEstado();
			$this->restituirStock();
		}

		protected function enviarEmail(){
			$mailer = new clsMailerPedidos("esperandosistemapago",$this->idPedido);
    		$mailer->enviar();
		}
    }

    class actualizarPedidos_PagoProcesando extends actualizarPedidos{
    	protected function guardarTransaccionHistorico(){
    		//dmPedidos::guardarPedidosHistorico($idPedido);
    	}

    	protected function ActualizarDb(){
    		$this->hayStock();
			$this->descontarStock();
    		$this->actualizarEstado();
    	}

    	protected function enviarEmail(){
    		$mailer = new clsMailerPedidos("procesandopago",$this->idPedido);
    		$mailer->enviar();
	    }

    }

    class actualizarPedidos_Aceptado extends actualizarPedidos{
    	protected function guardarTransaccionHistorico(){
    		//dmPedidos::guardarPedidosHistorico($idPedido);
    	}

		protected function ActualizarDb(){
			$this->hayStock();
			$this->marcarCupon();
			$this->darCredito();
			$this->descontarStock();
			$this->actualizarEstado();
		}

		 protected function marcarCupon(){
   			if($this->estadoInicial["IdEstadoPedidos"] == 8){
   				dmCupones::cuponPagado($this->idPedido);
   			}

  		}

		protected function enviarEmail(){
			$mailer = new clsMailerPedidos("pagoaceptado",$this->idPedido);
    		$mailer->enviar();

    		//Aviso al Padrino?
    		if($this->estadoInicial["ReglaAplicada"] == 0 && $this->estadoFinal["ReglaAplicada"] == 1){
    			$mailer->enviarPadrino("creditoareferido");
    		}
		}
    }

    class actualizarPedidos_EnvioProgramado extends actualizarPedidos{

    	protected $tracking;
		protected $fechaEnvio;

		protected function guardarTransaccionHistorico(){
			//dmPedidos::guardarPedidosHistorico($idPedido);
		}

		protected function ActualizarDb(){
			//$this->hayStock();
			//$this->descontarStock();
			$this->actualizarEstado();
		}

    	protected function enviarEmail(){
    		$mailer = new clsMailerPedidos("envioprogramado",$this->idPedido);
    		$mailer->enviar();
		}

		public function setDHL($numero){
    		$this->estadoFinal["NroEnvio"] = $numero;
    	}

    	public function setFechaEnvio($fecha){
    		$this->estadoFinal["FechaEnvio"] = $fecha;
    	}

    }

    class actualizarPedidos_PagoRechazado extends actualizarPedidos{
    	protected function guardarTransaccionHistorico(){
    		//dmPedidos::guardarPedidosHistorico($idPedido);
    	}

    	protected function ActualizarDb(){
    		$this->restituirStock();
    		$this->actualizarEstado();
    	}

    	protected function enviarEmail(){
    		$mailer = new clsMailerPedidos("pagorechazado",$this->idPedido);
    		$mailer->enviar();
        }
    }

    class actualizarPedidos_Anulado extends actualizarPedidos{
    	protected function guardarTransaccionHistorico(){
    		//dmPedidos::guardarPedidosHistorico($idPedido);
    	}

    	protected function ActualizarDb(){
    		$this->restituirCredito();
    		$this->restituirStock();
    		$this->actualizarEstado();
    	}

    	protected function enviarEmail(){
    		$mailer = new clsMailerPedidos("pagoanulado",$this->idPedido);
    		$mailer->enviar();
        }

        public function setMotivoAnulacion($anulacion){
    		$this->estadoFinal["MotivoAnulacion"] = $anulacion;
    	}
    }

    class actualizarPedidos_Entregado extends actualizarPedidos{
		protected function guardarTransaccionHistorico(){
    		//dmPedidos::guardarPedidosHistorico($idPedido);
    	}

		protected function ActualizarDb(){
			$this->darCredito();
			$this->actualizarEstado();
		}

		protected function enviarEmail(){
			$mailer = new clsMailerPedidos("pedidoentregado",$this->idPedido);
    		$mailer->enviar();
    		//Aviso al Padrino?
    		if($this->estadoInicial["ReglaAplicada"] == 0 && $this->estadoFinal["ReglaAplicada"] == 1){
    			$mailer->enviarPadrino("creditoareferido");
    		}
		}
		
		public function setDHL($numero){
    		$this->estadoFinal["NroEnvio"] = $numero;
    	}
    }

    class actualizarPedidos_PagoOffline extends actualizarPedidos{
    	protected function guardarTransaccionHistorico(){
    		//dmPedidos::guardarPedidosHistorico($idPedido);
    	}

    	protected function setIdFormaEnvio(){
    		$this->estadoFinal["IdFormaEnvio"] = 2;
    	}

		protected function ActualizarDb(){
			$this->hayStockReal();
			$this->descontarStock();
			$this->actualizarEstado();
		}

		protected function enviarEmail(){
			$mailer = new clsMailerPedidos("pagoaceptado",$this->idPedido);
    		$mailer->enviar();
		}
    }

	class actualizarPedidos_EsperandoCuponPago extends actualizarPedidos{
		protected $arrCupon;

    	protected function guardarTransaccionHistorico(){
    		//dmPedidos::guardarPedidosHistorico($idPedido);
    	}

		protected function ActualizarDb(){
			$this->hayStockReal();
			$this->descontarStock();
			$this->guardarCupon();
			$this->actualizarEstado();
		}

       /* public function setCupon($observacion, $mensaje, $mensajeSiNo){
    		$this->arrCupon = array("observacion"=>$observacion, "mensaje"=>$mensaje,"mensajeSiNo"=>$mensajeSiNo);
    	}
*/
    	protected function guardarCupon(){
			$Cupon = dmCupones::obtenerCupon();
			/*
			$fecha = dmCupones::fechaVencimientoCupon($this->idPedido);
			$fechaVencimiento = $fecha["fechaVencimiento"];
			*/
			$idCupon = $Cupon["id"];
			$fechaAsignacion = date('Y-m-d h:i:s');
			$fechaVencimiento = date('Y-m-d h:i:s', time()+24*60*60);

			$cuponAsignado = dmCupones::asignarCupon($idCupon, $this->idPedido, $fechaAsignacion, $fechaVencimiento);

    	}

    	protected function enviarEmail(){
			$mailer = new clsMailerPedidos("esperandocuponpago",$this->idPedido);
			$mailer->enviar();
		}


	}

	class actualizarPedidos_PedidoInterno extends actualizarPedidos_PagoOffline{}
	
	class actualizarPedidos_StockInsuficiente extends actualizarPedidos{

    	protected function guardarTransaccionHistorico(){
    	}

    	protected function ActualizarDb(){
    		$this->actualizarEstado();
    	}

    	protected function enviarEmail(){
    	
  			$oConexion = new Conexion();
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("SELECT * FROM usuarios WHERE IdPerfil IN (SELECT IdPerfil FROM perfiles WHERE notificacion_stock = 1)");
			$usuarios = $oConexion->DevolverQuery();
			$oConexion->Cerrar_Trans();
		    
  			$oConexion = new Conexion();
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("SELECT * FROM pedidos WHERE IdPedido = ".$this->idPedido);
			$pedido = $oConexion->DevolverQuery();
			$oConexion->Cerrar_Trans();
		   $pedido = $pedido[0];		   
		    
			foreach($usuarios as $i => $p) {
			
				$emailBuilder = new EmailBuilder();
				$emailData = $emailBuilder->generatePedidoInsuficiente($pedido['IdPedido'], $p['NombreUsuario']);
				
				$emailSender = new EmailSender();
				$emailSender->sendEmail($emailData);
				
			}
    	
	   }

    }

    class actualizarPedidos_Reversado extends actualizarPedidos{
    	protected function guardarTransaccionHistorico(){
    		//dmPedidos::guardarPedidosHistorico($idPedido);
    	}

    	protected function ActualizarDb(){
    		$this->restituirCredito();
    		$this->restituirStock();
    		$this->actualizarEstado();
    	}

    	protected function enviarEmail(){
    		$mailer = new clsMailerPedidos("pagoreversado",$this->idPedido);
    		$mailer->arrEmail = array('url' => 'front/emails/pagoreversado.html', 'asunto' => 'Tu pedido %NumeroPedido% est&aacute; Reversado');
    		$mailer->reemplazarString();
    		$mailer->enviar();
        }

        public function setMotivoReversado($anulacion){
    		$this->estadoFinal["MotivoAnulacion"] = $anulacion;
    	}
    }


    class actualizarPedidos_Garantia extends actualizarPedidos{
    	protected function guardarTransaccionHistorico(){
    		//dmPedidos::guardarPedidosHistorico($idPedido);
    	}

    	protected function ActualizarDb(){
    		$this->restituirCredito();
    		$this->restituirStock();
    		$this->actualizarEstado();
    	}

    	protected function enviarEmail(){
    		$mailer = new clsMailerPedidos("pagoanulado",$this->idPedido);
    		$mailer->arrEmail = array('url' => 'front/emails/pagogarantia.html', 'asunto' => 'Tu pedido %NumeroPedido% est&aacute; En Garant&iacute;a');
    		$mailer->reemplazarString();
    		$mailer->enviar();
        }

        public function setMotivoGarantia($anulacion){
    		$this->estadoFinal["MotivoAnulacion"] = $anulacion;
    	}
    }


    class actualizarPedidos_Devolucion extends actualizarPedidos{
    	protected function guardarTransaccionHistorico(){
    		//dmPedidos::guardarPedidosHistorico($idPedido);
    	}

    	protected function ActualizarDb(){
    		$this->restituirCredito();
    		$this->restituirStock();
    		$this->actualizarEstado();
    	}

    	protected function enviarEmail(){
    		$mailer = new clsMailerPedidos("pagoanulado",$this->idPedido);
    		$mailer->arrEmail = array('url' => 'front/emails/pagodevolucion.html', 'asunto' => 'Tu pedido %NumeroPedido% est&aacute; En Devoluci&oacute;n');
    		$mailer->reemplazarString();
    		$mailer->enviar();
        }

        public function setMotivoDevolucion($anulacion){
    		$this->estadoFinal["MotivoAnulacion"] = $anulacion;
    	}
    }


    class actualizarPedidos_GCash extends actualizarPedidos{
    	protected function guardarTransaccionHistorico(){
    		//dmPedidos::guardarPedidosHistorico($idPedido);
    	}

    	protected function ActualizarDb(){
    		$this->restituirCredito();
    		$this->restituirStock();
    		$this->actualizarEstado();
    	}

    	protected function enviarEmail(){
    		$mailer = new clsMailerPedidos("pagoanulado",$this->idPedido);
    		$mailer->arrEmail = array('url' => 'front/emails/pagodevoluciongcash.html', 'asunto' => 'Tu pedido %NumeroPedido% est&aacute; En Devoluci&oacute;n GCash');
    		$mailer->reemplazarString();
    		$mailer->enviar();
        }

        public function setMotivoGCash($anulacion){
    		$this->estadoFinal["MotivoAnulacion"] = $anulacion;
    	}
    }

?>