<?php
    if (!class_exists("MySQL_Pedidos"))
    {
        class MySQL_Pedidos extends pedidos
        {
            public function getInsert()
            {
                $strCampos="";
                $strValores="";
                $Me = $this->__toArray();
                foreach ($Me as $propiedad => $valor)
                {   
                    if(!is_array($valor))
                    {
                        if($propiedad == "_idpedido" || $propiedad == "_idmensajeria" || $propiedad == "_idformapago" || $propiedad == "_idformaenvio" || $propiedad == "_fechaenvio")
                        {   
                            $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                            $strValores .=(($valor == 0)?"NULL": chr(34).mysql_real_escape_string($valor).chr(34)). ", ";
                        }
                        else
                        {
                            $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                            $strValores .=chr(34).mysql_real_escape_string($valor).chr(34). ", ";
                        }
                    }   
                }
                return "INSERT INTO pedidos (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).")";
            }
            public function getById()        
            {
                return "SELECT IdPedido, DATE_FORMAT(Fecha, '%d/%m/%Y') AS Fecha, DATE_FORMAT(FechaEnvio, '%d/%m/%Y') AS FechaEnvio, NroEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdEstadoPedidos, IdCampania, IdDescuento, Descuento, GastosEnvio, Total, MotivoAnulacion FROM pedidos WHERE IdPedido = ".mysql_real_escape_string($this->getIdPedido());
            }
        }  
    }
    if (!class_exists("MySQL_PedidosProductos"))
    {
        class MySQL_PedidosProductos extends productos_x_pedidos
        {
            public function getInsert()
            {
                $strCampos="";
                $strValores="";
                $Me = $this->__toArray();
                foreach ($Me as $propiedad => $valor)
                {   
                   if(!is_array($valor))
                   {
                       $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                       $strValores .=chr(34).mysql_real_escape_string($valor).chr(34). ", ";
                   }
                }
                return "INSERT INTO productos_x_pedidos (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).")";
            }
            public function getById()
            {
                return "SELECT IdPedido, IdCodigoProdInterno, Cantidad, Precio FROM productos_x_pedidos WHERE IdPedido = " .$this->getIdPedido();
            }
        }
    }
     if (!class_exists("MySQL_PedidosDirecciones"))
     {
         class MySQL_PedidosDirecciones extends pedidos_direcciones
        {
            public function getInsert()
            {
                $strCampos="";
                $strCamposU="";
                $strValores="";
                $Me = $this->__toArray();
                foreach ($Me as $propiedad => $valor)
                {
                    if($propiedad != "_idusuario" && $propiedad != "_idpedido")
                    {
                        $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                        $strValores .=chr(34).mysql_real_escape_string($valor).chr(34). ", ";
                    }
                    else
                    {
                        $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                        $strValores .=chr(34).mysql_real_escape_string($valor).chr(34). ", ";
                        $strCamposU .=substr($propiedad,1,strlen($propiedad))." = ".chr(34).mysql_real_escape_string($valor).chr(34). ", ";
                    }
                }
                return "INSERT INTO pedidos_direcciones  (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
            }
            public function getUpdate()
            {
                $values = array();
                $Me = $this->__toArray();
                foreach ($Me as $propiedad => $valor)
                {
                    if($propiedad != "_idusuario" && $propiedad != "_idpedido")
                    {
                    	$values[] = substr($propiedad,1,strlen($propiedad)).' = '.chr(34).mysql_real_escape_string($valor).chr(34);
                    }
                }
                
                $values = implode(', ', $values);
                return "UPDATE pedidos_direcciones  SET ".$values." WHERE IdPedido = ".$this->getIdPedido()." AND IdUsuario = ".$this->getIdUsuario();
            }
            public function getById()
            {
                return "SELECT IdDireccionPedido, IdPedido, IdUsuario, Domicilio, Numero, Escalera, Piso, Puerta, CP, IdProvincia, IdPais, TipoCalle, Poblacion, Telefono, Completo, HorarioEntrega, IdCiudad FROM pedidos_direcciones WHERE IdPedido = ".mysql_real_escape_string($this->getIdPedido());
            }
        }   
     }
?>
