<?php

	require_once dirname(__FILE__).'/../clases/clsdescuentos.php';

    class dmDescuentos
    {
        public static function getAll($columna = 'nombre', $sentido = 'asc', $filtro="")
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT b.id as ID, b.descripcion as Descripcion, b.codigo as Codigo FROM descuentos b ".($filtro != '' ? "WHERE b.descripcion LIKE \"%".$filtro."%\" OR  b.codigo LIKE \"%".$filtro."%\"" : '')." ORDER BY " . $columna . " " . $sentido);
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }


        public static function save($oLocal, $camps)
        {
            $oConexion = Conexion::nuevo();
            try
            {   
            	
            	$hora = ($oLocal->getVencimientoHora() != '' ? "'".$oLocal->getVencimientoHora()."'" : 'NULL');
                                             
                $oConexion->Abrir_Trans();
                if ($oLocal->getIdDescuento() == "") {
                    $oConexion->setQuery("INSERT INTO descuentos (codigo, descripcion, descuento, porcentaje, vencimiento, vencimientohora, iniciofecha, iniciohora, enviogratis, unico) VALUES('".$oLocal->getCodigo()."', '".$oLocal->getDescripcion()."', '".$oLocal->getDescuento()."', ".$oLocal->getPorcentaje().", '".$oLocal->getVencimiento()."', ".$hora.", '".$oLocal->getInicioFecha()."', '".$oLocal->getInicioHora()."', '".$oLocal->getEnvioGratis()."', '".$oLocal->getUnico()."')");
                } else
                    $oConexion->setQuery("UPDATE descuentos SET codigo = '".$oLocal->getCodigo()."', descripcion = '".$oLocal->getDescripcion()."', descuento = '".$oLocal->getDescuento()."', porcentaje = ".$oLocal->getPorcentaje().", vencimiento = '".$oLocal->getVencimiento()."', vencimientohora = ".$hora.", iniciofecha = '".$oLocal->getInicioFecha()."', iniciohora = '".$oLocal->getInicioHora()."', envioGratis = '".$oLocal->getEnvioGratis()."', unico = '".$oLocal->getUnico()."' WHERE id = " . $oLocal->getIdDescuento());
                $oConexion->EjecutarQuery();
                if($oLocal->getIdDescuento() == "")               	
                	$id = $oConexion->LastId();
                else
                	$id = $oLocal->getIdDescuento();
                                
                $oConexion->Cerrar_Trans();
                
                //Camps
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("DELETE FROM descuentos_campanias WHERE IdDescuento = " . $id);
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();

                if(is_array($camps))
					foreach($camps as $cid) {
						$oConexion = Conexion::nuevo();
						$oConexion->Abrir_Trans();
		                $oConexion->setQuery("INSERT INTO descuentos_campanias VALUES (" . $id.','.$cid.')');
		                $oConexion->EjecutarQuery();
		                $oConexion->Cerrar_Trans();
					}
				                
                return $id;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }            
        }
        
        public static function getById($idLocal)
        {
            $oConexion = Conexion::nuevo();
            $oLocal = new Descuentos();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT * FROM descuentos WHERE id = " . $idLocal);
                $Fila = $oConexion->DevolverQuery();
                if(count($Fila) == 0) return false;
                $oConexion->Cerrar_Trans();
                
                    $oLocal->setIdDescuento($Fila[0]["id"]);
                    $oLocal->setCodigo($Fila[0]["codigo"]);
                    $oLocal->setDescripcion($Fila[0]["descripcion"]);
                    $oLocal->setDescuento($Fila[0]["descuento"]);
                    $oLocal->setPorcentaje($Fila[0]["porcentaje"]);
                    $oLocal->setVencimiento($Fila[0]["vencimiento"]);
                    $oLocal->setVencimientoHora($Fila[0]["vencimientohora"]);
                    $oLocal->setInicioFecha($Fila[0]["iniciofecha"]);
                    $oLocal->setInicioHora($Fila[0]["iniciohora"]);
                    $oLocal->setUsado($Fila[0]["usado"]);
                    $oLocal->setEnvioGratis($Fila[0]["enviogratis"]);
                    $oLocal->setUnico($Fila[0]["unico"]);

                }
                catch(MySQLException $e)
                {
                    throw $e;
                }
                return $oLocal;
            }
            
        public static function getByCodigo($idLocal, $campania)
        {
            $oConexion = Conexion::nuevo();
            $oLocal = new Descuentos();
            try
            {
                $oConexion->Abrir_Trans();
                //$oConexion->setQuery("SELECT d.* FROM descuentos d JOIN descuentos_campanias c ON c.IdDescuento = d.id AND c.IdCampania = ".$campania." WHERE d.vencimiento > NOW() AND d.iniciofecha <= NOW() AND d.iniciohora <= NOW() AND d.usado = 0 AND d.codigo = '" . mysql_real_escape_string($idLocal) . "'");
				$oConexion->setQuery("SELECT d.* FROM descuentos d 
				WHERE
					d.id NOT IN (
						SELECT IdDescuento FROM descuentos_campanias WHERE IdCampania = ".$campania."
					)
					AND
					(
						d.vencimiento > date_format(NOW(), \"%Y-%m-%d\") OR (
							d.vencimiento = date_format(NOW(), \"%Y-%m-%d\") AND
							(d.vencimientohora IS NULL OR d.vencimientohora > NOW())
						)
					)
					AND 
					(
						d.iniciofecha < date_format(NOW(), \"%Y-%m-%d\") OR 
						(
							d.iniciofecha = date_format(NOW(), \"%Y-%m-%d\")
							AND d.iniciohora <= NOW()
						)
					)
					AND
					(
						d.unico = 0
						OR
						d.id NOT IN (
							SELECT IdDescuento FROM pedidos WHERE IdUsuario = ".Aplicacion::Decrypter($_SESSION["User"]["id"])." AND IdDescuento IS NOT NULL
						)
					)
					AND d.codigo = '" . mysql_real_escape_string($idLocal) . "'");
                $Fila = $oConexion->DevolverQuery();
                if(count($Fila) == 0) return false;
                $oConexion->Cerrar_Trans();
                
                $oLocal->setIdDescuento($Fila[0]["id"]);
                $oLocal->setCodigo($Fila[0]["codigo"]);
                $oLocal->setDescripcion($Fila[0]["descripcion"]);
                $oLocal->setDescuento($Fila[0]["descuento"]);
                $oLocal->setPorcentaje($Fila[0]["porcentaje"]);
                $oLocal->setVencimiento($Fila[0]["vencimiento"]);
                $oLocal->setVencimientoHora($Fila[0]["vencimientohora"]);
                $oLocal->setInicioFecha($Fila[0]["iniciofecha"]);
                $oLocal->setInicioHora($Fila[0]["iniciohora"]);
                $oLocal->setUsado($Fila[0]["usado"]);
                $oLocal->setUnico($Fila[0]["unico"]);

            }
            catch(MySQLException $e)
            {
                throw $e;
            }
            return $oLocal;
        }
        
        public static function getCampanias($idLocal)
        {
            $oConexion = Conexion::nuevo();
            $oLocal = new Descuentos();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdCampania FROM descuentos_campanias WHERE IdDescuento = '" . mysql_real_escape_string($idLocal) . "'");
                $Fila = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                
                $camps = array();
                foreach($Fila as $d)
                	$camps[] = $d['IdCampania'];
                	
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
            return $camps;
        }
        
        public static function usarDescuento($id)
        {
        	$oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE descuentos SET usado = 1 WHERE id = ".$id);
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
            
       	public static function delete($obj)
        {
            $oConexion = Conexion::nuevo();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("DELETE FROM descuentos WHERE id = ".$obj->getIdDescuento());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
                
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    }
?>
