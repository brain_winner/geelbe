<?php
    class Descuentos
    {
        private $_iddescuento;
        private $_codigo;
        private $_descripcion;
        private $_descuento;
        private $_porcentaje;
        private $_vencimiento;
        private $_vencimientohora;
        private $_iniciofecha;
        private $_iniciohora;
        private $_usado;
        private $_enviogratis;
        private $_unico;
       
        public function __construct()
        {
            $this->_iddescuento = "";
            $this->_codigo = "";
            $this->_descripcion = "";
            $this->_descuento = "";
            $this->_porcentaje = "";
            $this->_vencimiento = "";
            $this->_vencimientohora = "";
            $this->_iniciofecha = "";
            $this->_iniciohora = "";
            $this->_usado = "";
            $this->_enviogratis = "";
            $this->_unico = "";
        }
        public function getIdDescuento()
        {
            return $this->_iddescuento;
        }
        public function setIdDescuento($idlocal)
        {
            $this->_iddescuento = $idlocal;
        }
        public function getCodigo()
        {
            return $this->_codigo;
        }
        public function setCodigo($idlocal)
        {
            $this->_codigo = $idlocal;
        }
        public function getUsado()
        {
            return $this->_usado;
        }
        public function setUsado($idlocal)
        {
            $this->_usado = $idlocal;
        }
        public function getDescripcion()
        {
            return $this->_descripcion;
        }
        public function setDescripcion($descripcion)
        {
            $this->_descripcion = $descripcion;
        }
        public function getDescuento()
        {
            return $this->_descuento;
        }
        public function setDescuento($descripcion)
        {
            $this->_descuento = $descripcion;
        }
        public function getPorcentaje()
        {
            return $this->_porcentaje;
        }
        public function setPorcentaje($descripcion)
        {
            $this->_porcentaje = $descripcion;
        }
        public function getVencimiento()
        {
            return $this->_vencimiento;
        }
        public function setVencimiento($descripcion)
        {
            $this->_vencimiento = $descripcion;
        }
        public function getVencimientoHora()
        {
            return $this->_vencimientohora;
        }
        public function setVencimientoHora($descripcion)
        {
            $this->_vencimientohora = $descripcion;
        }
        public function getInicioHora()
        {
            return $this->_iniciohora;
        }
        public function setInicioHora($descripcion)
        {
            $this->_iniciohora = $descripcion;
        }
        public function getInicioFecha()
        {
            return $this->_iniciofecha;
        }
        public function setInicioFecha($descripcion)
        {
            $this->_iniciofecha = $descripcion;
        }
        public function getEnvioGratis()
        {
            return $this->_enviogratis;
        }
        public function setEnvioGratis($descripcion)
        {
            $this->_enviogratis = $descripcion;
        }
        public function getUnico()
        {
            return $this->_unico;
        }
        public function setUnico($descripcion)
        {
            $this->_unico = $descripcion;
        }
    }
?>
