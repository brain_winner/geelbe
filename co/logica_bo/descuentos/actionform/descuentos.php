<html>
    <head>
    </head>
    <body>
<?php
    if($_POST)
    {	
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/clases/clsdescuentos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/datamappers/dmdescuentos.php");
        extract($_POST);
        try
        {   
            $oLocal = new Descuentos();

            if(isset($idDescuento) && is_numeric($idDescuento))
            	$oLocal->setIdDescuento($idDescuento);
            	
            $oLocal->setCodigo($txtCodigo);
            $oLocal->setDescripcion($txtDescripcion);
            $oLocal->setDescuento($txtDescuento);
            $oLocal->setPorcentaje($txtPorcentaje);
            $oLocal->setVencimiento($txtVencimiento);
            $oLocal->setVencimientoHora($txtVencimientoHora);
            $oLocal->setInicioHora($txtInicioHora);
            $oLocal->setInicioFecha($txtInicioFecha);
            $oLocal->setEnvioGratis($txtEnvioGratis);
            $oLocal->setUnico($txtUnico);
            $id = dmDescuentos::save($oLocal, $_POST['camp']);
            ?>
                <script type="text/javascript">
                    window.location.href = '../../bo/descuentos/index.php';
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            ?>
                <script>
                    window.parent.ErrorMSJ("<?=$e->getNumeroSQLERROR()?>", "");
                </script>
            <?
        }
        ob_flush();
    }
?>
    </body>
</html>
