<?php
    class dmReferenciados
    {
		public static function ObtenerReferenciados_NoRegistrados($idUsuario)
        {
            $oCN = Conexion::nuevo();
            try
            {
                $oCN->Abrir_Trans();
                $oCN->setQuery("SELECT A.* FROM ahijados A
WHERE A.email NOT IN (SELECT U.NombreUsuario FROM usuarios U) AND A.idUsuario = " . $idUsuario);
                $Tabla = $oCN->DevolverQuery();
                $oCN->Cerrar_Trans();
                return $Tabla;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }

        public static function ObtenerReferenciados()
        {
            $oConexion = Conexion::nuevo();
            try
            {
                 $oConexion->Abrir_Trans();
                 $oConexion->setQuery("SELECT A.idUsuario,
A.Email as 'Email', IF(ISNULL(CONCAT(CONCAT(DUP.Nombre, ' '),DUP.Apellido )), 1, 0) AS 'NoRegistrado' FROM ahijados A
LEFT JOIN usuarios U ON A.Email = U.NombreUsuario
LEFT JOIN datosusuariospersonales DUP ON DUP.idUsuario = U.idUsuario");
                 $Tabla = $oConexion->DevolverQuery();
                 $oConexion->setQuery("SELECT DISTINCT A.idUsuario,  CONCAT(CONCAT(DUP.Nombre, ' '), DUP.Apellido) AS 'Apenom' FROM ahijados A
INNER JOIN datosusuariospersonales DUP ON DUP.idUsuario = A.idUsuario");
                 $Tabla2 = $oConexion->DevolverQuery();
                 for($i=0; $i<count($Tabla); $i++)
                 {
                     $Tabla[$i]["Apenom"] = self::BuscarValor($Tabla[$i]["idUsuario"], $Tabla2);
                     //str_replace($Tabla[$i]["idUsuario"], self::BuscarValor($Tabla[$i]["idUsuario"], $Tabla2), $Tabla[$i]["idUsuario"]);
                 }
                 $oConexion->Cerrar_Trans();
                 return $Tabla;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }

        private function BuscarValor($valor, $matriz)
        {
            for($i=0; $i<count($matriz); $i++)
            {
                if ($matriz[$i]["idUsuario"] == $valor)
                    return $matriz[$i]["Apenom"];
            }
            return "Usuario inexistente";
        }

       public static function ObtenerInvitados()
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT ahijados.Email AS 'Ahijado', datosusuariospersonales.Nombre AS 'NombrePadrino', datosusuariospersonales.Apellido AS 'ApellidoPadrino', usuarios.NombreUsuario AS 'Padrino' FROM ahijados Inner Join usuarios ON ahijados.IdUsuario = usuarios.IdUsuario Inner Join datosusuariospersonales ON usuarios.IdUsuario = datosusuariospersonales.IdUsuario ");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(exception $e)
            {
                throw $e;
            }

        }

    }
?>
