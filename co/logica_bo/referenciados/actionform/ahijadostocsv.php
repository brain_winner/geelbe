<?php
    header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=".date("Ymd").".csv");
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
    try
    { 
        $Tabla = dmReferenciados::ObtenerReferenciados();   
        foreach($Tabla as $AhijadoNoRegistrado)
        {
            if ($AhijadoNoRegistrado["NoRegistrado"])
                echo $AhijadoNoRegistrado["Apenom"] . "; " . $AhijadoNoRegistrado["Email"] . "\n";
        }
    }
    catch(exception $e)
    {
        print $e->getMessage();
    }
?>
