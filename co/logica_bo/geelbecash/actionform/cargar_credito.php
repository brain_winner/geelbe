<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php";
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_service.php");

    error_reporting(E_ERROR | E_PARSE);
    
    try {
        ValidarUsuarioLogueadoBo(63);
	} catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
    
    if (!isset($_REQUEST["motivo"])) {
		header('Location: /'.$confRoot[1].'/bo/geelbecash/cargar_credito_form.php?error_type=empty_motivo&email='.$_REQUEST["email"].'&motivo='.$_REQUEST["motivo"].'&monto='.$_REQUEST["monto"]);
		exit;
    }
    
    if (!isset($_REQUEST["monto"])) {
		header('Location: /'.$confRoot[1].'/bo/geelbecash/cargar_credito_form.php?error_type=empty_monto&email='.$_REQUEST["email"].'&motivo='.$_REQUEST["motivo"]);
		exit;
    }
    
    try {
	    $idUsuarioBO = Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]);
	    $id = GeelbeCashService::cargarCredito($_REQUEST["email"], $_REQUEST["monto"], $_REQUEST["motivo"], $idUsuarioBO);
	    
	    $transaccion = GeelbeCashDAO::ObtenerPorId($id);
	    $objUsuario = dmUsuario::getByIdUsuario($transaccion->getIdUsuario());
	    
	    // Envio email de transaccion aceptada
        /*$emailBuilder = new EmailBuilder();
        $emailData = $emailBuilder->generateGeelbeCashTrxAcceptedEmail($objUsuario, $transaccion->getMonto());
           
	    $emailSender = new EmailSender();
	    $emailSender->sendEmail($emailData);*/
    } catch (InvalidUserEmailException $e) {
		header('Location: /'.$confRoot[1].'/bo/geelbecash/cargar_credito_form.php?error_type=invalid_user_email&email='.$_REQUEST["email"].'&motivo='.$_REQUEST["motivo"].'&monto='.$_REQUEST["monto"]);
		exit;
    }
    
    header('Location: /'.$confRoot[1].'/bo/geelbecash/index.php');
?>