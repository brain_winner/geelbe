<?php
	try {
        ValidarUsuarioLogueadoBo(110);
	} catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
    
        if(is_uploaded_file($_FILES['csv']['tmp_name'])) {
    
    	$file = fopen($_FILES['csv']['tmp_name'], "r");
		$error = array();
		while($data = fgetcsv($file, 0, ';')) {	    
			
			$email = $data[0];
			$monto = $data[1];
			$motivo = $data[2];
			
			if(strlen($email) > 0) {
						
				try {
				
				    $idUsuarioBO = Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]);
				    $id = GeelbeCashService::cargarCredito($email, $monto, $motivo, $idUsuarioBO);
				    
				    $error[] = 'Se cargaron $'.$monto.' al usuario '.$email.'. Transacci&oacute;n: '.$id.'.';
				    
			    } catch (InvalidUserEmailException $e) {
			    	$error[] = 'El email '.$email.' es inv&aacute;lido.';
			    }
				
			}
		    
		}
		
		if(count($error) > 0) {
			$error = '<p class="error">'.implode('<br>', $error).'<br /><br /></p>';
		}
	    
     }
    
?>