<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php";
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_service.php");

    error_reporting(E_ERROR | E_PARSE);
    
    try {
        ValidarUsuarioLogueadoBo(62);
	} catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
    
    if (!isset($_REQUEST["idTrx"])) {
		header('Location: /'.$confRoot[1].'/bo/geelbecash/anular_trx_form.php?error_type=invalid_trx_id&idTrx='.$_REQUEST["idTrx"].'&motivo='.$_REQUEST["motivo"]);
		exit;
    }
    
    if (!isset($_REQUEST["motivo"])) {
		header('Location: /'.$confRoot[1].'/bo/geelbecash/anular_trx_form.php?error_type=empty_motivo&idTrx='.$_REQUEST["idTrx"].'&motivo='.$_REQUEST["motivo"]);
		exit;
    }
    
    try {
    	$idUsuarioBO = Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]);
    	GeelbeCashService::anularTransaccion($_REQUEST["idTrx"], $_REQUEST["motivo"], $idUsuarioBO);
    } catch (InvalidTrxIdException $e) {
		header('Location: /'.$confRoot[1].'/bo/geelbecash/anular_trx_form.php?error_type=invalid_trx_id&idTrx='.$_REQUEST["idTrx"].'&motivo='.$_REQUEST["motivo"]);
		exit;
    } catch (AlreadyRevokeException $e) {
		header('Location: /'.$confRoot[1].'/bo/geelbecash/anular_trx_form.php?error_type=already_revoke_trx&idTrx='.$_REQUEST["idTrx"].'&motivo='.$_REQUEST["motivo"]);
		exit;
    }
    
    header('Location: /'.$confRoot[1].'/bo/geelbecash/trx_anuladas.php');
?>