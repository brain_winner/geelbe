<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");   
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");

    extract($_POST);
    if ($_POST)
    {
        try
        {
        
        	$fields = array(
				'txtEmail' => array(
					'exception' => 'Usuario vac&iacute;o',
					'check' => 'isset'
				),
				
				'txtClave' => array(
					'exception' => 'Clave vac&iacute;a',
					'check' => 'isset'
				)
									
			);
			
			ValidatorUtils::checkFields($fields, $_POST, false);
        
            $objUsuario = dmUsuario::Login($txtEmail, $txtClave);
            Aplicacion::loginUser($objUsuario);
            $_SESSION["BO"]["User"]["Password"] = $txtClave;
            ?>
                <script>
                    window.parent.location = "<?=Aplicacion::getRootUrl()?>bo/";
                </script>
            <?
        }
        catch(exception $e)
        {
            print $e->getMessage();
        }  
    }
?>
