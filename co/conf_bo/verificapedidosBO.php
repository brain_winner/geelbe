<?php
try
{
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("pedidos", "clases"));
    set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
	require_once 'Zend/Cache.php';
	
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
	
}
catch(exception $e)
{
    die(print_r($e));
}
$usuarios = dmPedidos::getUsuariosPedidosPendientes();
foreach($usuarios as $id)
{
    try
    {
        $dtUsuarios = dmPedidos::getPedidoByIdDM($id['IdPedido']);
        $LeyendaEstado="";
        $NombreCampania = $dtUsuarios[0]["Campania"];
        $TRX_ID = $dtUsuarios[0]["IdPedido"];
        if($dtUsuarios[0]["IdEstadoPedidos"] >=0 && $dtUsuarios[0]["IdEstadoPedidos"] <1)
        {
            try
            {
                dmPedidos::VerificarStock($TRX_ID);
                try
                {
                    $Cuenta = Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "e_comercio"))."2";
                    $Email = Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "email_cuenta"));
                    $Pin = Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "pin_consultas"));
                    $objDMReporte = new ReportePedidosDM($Email,$Cuenta, $Pin);
                    $objDMReporte->sendConsulta();
                    $arrPago = $objDMReporte->getPayByIdTRX($TRX_ID);
                    dmPedidos::PagoAceptado($TRX_ID, $id['IdUsuario']);
                    $dtUsuarios[0]["IdEstadoPedidos"] = 2;
                    $dtUsuarios[0]["Estado"] ="Pago aceptado";
                    
	                // Limpio la cache
	                $IdCampania = dmPedidos::getIdCategoriaByIdPedido($TRX_ID);
	                $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
					$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
					$cache->clean(
					    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
					    array('group_cache_catalogo_campania_'.$IdCampania)
					);
                    

                    try
                    {
                        global $objReglaNegocio;
                        $Padrino = $objReglaNegocio->Apadrinamiento($id['IdUsuario'], $dtUsuarios[0]["Importe"]);
                        
                        $emailBuilder = new EmailBuilder();
	                	$emailData = $emailBuilder->generateCreditoPotencialAcreditadoEmail($Padrino, str_pad($arrPedido["IdPedido"], 8, 0, 0), $arrPedido["Campania"]);
	                
				    	$emailSender = new EmailSender();
				    	$emailSender->sendEmail($emailData);
                    }
                    catch(exception $e)
                    {

                    }                
                }
                catch(exception $e)
                {
                //throw $e;
                }
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
    }
    
    catch(exception $e) 
    {
        die(print_r($e));
    }
}
?>