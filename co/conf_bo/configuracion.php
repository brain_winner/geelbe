<?
    @session_start();
    //error_reporting(E_ERROR);
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    define("ARCHIVO_CONF",  $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/config.xml");
    class xmlConf
    {
        private $_archivo;
        /**
        * @desc Constructor de la clase xmlConf
        * @var string. Nombre del archivo xml de configuracion.
        */
        public function xmlConf($archivo)
        {
            try
            {
                if (!@is_file($archivo))
                    throw new Exception("El archivo especificado no existe.");
                $this->_archivo = $archivo;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        /**
        * @desc Devuelve el valor del subtag.
        * @var string. Nombre del tag a buscar.
        * @var string. Nombre del subtag a buscar que contiene un valor.
        * @return string.
        */
        public function getValor($tag, $subtag)
        {
            try
            {
                return $this->_getValor($tag, $subtag);
            }
            catch(exception $e)
            {
                throw $e;
            }            
        }
        public function getIncludes($seccion, $seccion1, $parametro)
        {
            try
            {
                return $this->_getInclude($seccion, $seccion1, $parametro);
            }
            catch(exception $e)
            {
                throw $e;
            }            
        }
        public function getValor2($seccion, $seccion1, $parametro)
        {
            try
            {
                return $this->_getValor2($seccion, $seccion1,  $parametro);
            }
            catch(exception $e)
            {
                throw $e;
            }            
        }
        private static function _Buscador(XMLReader $node, $tag)
        {
            try
            {
                while($node->read())
                {
                    $node->next($tag);
                    $_tag = $node->name;
                    $_type = $node->nodeType;
                    if($_type == XMLReader::ELEMENT && $_tag == $tag)
                    {
                        return $node;
                    }                    
                }
                throw new Exception("No se encontro el tag $tag.");
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        
        /**
        * @desc Devuelve el valor del subtag.
        * @var string. Nombre del tag a buscar.
        * @var string. Nombre del subtag a buscar que contiene un valor.
        * @return string.
        * @access private.
        */
        private function _getValor($tag, $subtag)
        {
            try
            {
                $tag = strtolower($tag);
                $subtag = strtolower($subtag);
                $node = new XMLReader();
                $node->open($this->_archivo);
                if(!$node->read())
                    throw new Exception(("El archivo especificado no es del tipo xml."));                
                $node = $this->_Buscador($node, $tag);
                $node = $this->_Buscador($node, $subtag);
                $node->read();
                $valor = $node->value;
                $node->close();
                return $valor;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        /**
        * @desc Devuelve el valor del subtag.
        * @var string. Nombre del tag a buscar.
        * @var string. Nombre del subtag a buscar que contiene un valor.
        * @return string.
        * @access private.
        */
        private function _getInclude($nodo1, $nodo2, $nodo3)
        {
            try
            {
                $tag = strtolower($tag);
                $node = new XMLReader();
                $node->open($this->_archivo);
                if(!$node->read())
                    throw new Exception(("El archivo especificado no es del tipo xml."));
                $node = $this->_Buscador($node, $nodo1);
                $node = $this->_Buscador($node, $nodo2);
                $valor = array();
                if($parametro !="")
                {
                    $node = $this->_Buscador($node, $nodo3);
                    $nombre = $node->name;
                    $valor[$nombre]["carpeta"]=$node->getAttribute("carpeta");
                    $valor[$nombre]["inicio"]=$node->getAttribute("inicio");                    
                }
                else
                {
                    while ($node->read() && !($node->name == $nodo2 && $node->nodeType == XMLReader::END_ELEMENT))
                    {
                        $nombre = $node->name;
                        if ($node->nodeType == XMLReader::ELEMENT)
                        {
                            $valor[$nombre]["carpeta"]=$node->getAttribute("carpeta");
                            $valor[$nombre]["inicio"]=$node->getAttribute("inicio");
                        }
                    }
                }                    
                $node->close();
                return $valor;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function _getValor2 ($nodo1, $nodo2, $nodo3)
        {
            try
            {
                $tag = strtolower($tag);
                $node = new XMLReader();
                $node->open($this->_archivo);
                if(!$node->read())
                    throw new Exception(("El archivo especificado no es del tipo xml."));
                $node = $this->_Buscador($node, $nodo1);
                $node = $this->_Buscador($node, $nodo2);
                $valor = null;
                if($parametro !="")
                {
                    $node = $this->_Buscador($node, $nodo3);
                    $nombre = $node->name;
                    $node->read();
                    $valor = $node->value;
                }
                else
                {
                    while ($node->read() && !($node->name == $nodo2 && $node->nodeType == XMLReader::END_ELEMENT))
                    {
                        $nombre = $node->name;
                        if ($node->nodeType == XMLReader::ELEMENT)
                        {
                            $node->read();
                            $valor = $node->value;
                        }
                    }
                }                    
                $node->close();
                return $valor;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
    }
    /**
    * @desc Clase abstracta con todas las funciones y configuraciones de la aplicacion.
    */
    class Aplicacion
    {                 
        public static function getRootUrl()
        {
            return Aplicacion::getParametros("info","protocol").$_SERVER['SERVER_NAME']."/".Aplicacion::getParametros("info","dir");
        }
        public static function getRoot()
        {
            return $_SERVER['DOCUMENT_ROOT']."/".Aplicacion::getParametros("info","dir");
        }
        public static function getErrores($seccion, $id)
        {
            try
            {                                                                                                   
                
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        /**
        * @desc Devuelve el valor del parametro.
        * @var string. Nombre del tag.
        * @var string. Nombre del subtag.
        * @var string. Valor por default en caso de no existir el parametro.
        * @return string.
        */
        public static function getParametros($nombre, $propiedad, $default=null)
        {
            try
            {
                $objConfig = new xmlConf(ARCHIVO_CONF);
                return $objConfig->getValor($nombre, $propiedad);
            }
            catch(exception $e)
            {
                return $default;
            }
        }
        public static function getDirLocal()
        {
            /*$dir =Aplicacion::getParametros("info", "protocol");
            $dir .=Aplicacion::getParametros("info", "host");*/
            //$dir = $_SERVER['DOCUMENT_ROOT']."/";
            //$dir = "/";
            $dir =Aplicacion::getParametros("info", "dir");
            return $dir;
        }
        /**
        * @desc Devuelve el directorio del style.
        * @return string.
        */
        public static function getDirStyle()
        {   
            $dir =Aplicacion::getDirFrontEnd();
            $dir .=Aplicacion::getParametros("dir_styles", "dir");
            return $dir;
        }        
        /**
        * @desc Devuelve el directorio de la logica.
        * @var string.
        * @return string.
        */
        public static function getDirLogica($parametro="")
        {   
            $dir =Aplicacion::getDirLocal();
            $dir .=Aplicacion::getParametros("dir_logica", "dir");
            $dir .=Aplicacion::getParametros("dir_logica", $parametro);
            return $dir;
        }
        /**
        * @desc Devuelve el directorio del front end.
        * @var string.
        * @return string.
        */
        public static function getDirFrontEnd($parametro="")
        {   
            $dir =Aplicacion::getDirLocal();
            $dir .=Aplicacion::getParametros("dir_front_end", "dir");
            $dir .=Aplicacion::getParametros("dir_front_end", $parametro);
            return $dir;
        }
        /**
        * @desc Devuelve el directorio del back office.
        * @var string.
        * @return string.
        */
        public static function getDirBackOffice($parametro="")
        {   
            //$dir =Aplicacion::getDirLocal();
            $dir .=Aplicacion::getParametros("dir_back_office", "dir");
            $dir .=Aplicacion::getParametros("dir_back_office", $parametro);
            return $dir;
        }
        /**
        * @desc Devuelve el directorio de la clase conexoin.
        * @var string.
        * @return string.
        */
        public static function getConexion()
        {   
            $dir =Aplicacion::getDirLocal();
            $dir .=Aplicacion::getParametros("dir_logica", "dir");
            $dir .=Aplicacion::getParametros("conexion", "dir");
            return $dir;
        }
        /**
        * @desc Devuelve el directorio de la clase conexoin.
        * @var string.
        * @return string.
        */
        public static function getIncludes($seccion,  $parametro="")
        {   
            $objConfig = new xmlConf(ARCHIVO_CONF);
            $inclu = $objConfig->getIncludes("includes", $seccion, $parametro);
            foreach ($inclu as $key => $incluir)
            {                
                $Requeridos[$key]=$incluir["carpeta"].$incluir["inicio"];                
            }
            return $Requeridos;
        }
        
        /**
        * @desc Devuelve los datos del email
        * @var string.
        * @return array.
        */
        public static function getEmail($email)
        {           	
			// get contents of a file into a string			
			$handle = fopen(ARCHIVO_CONF, "r");
			$contents = fread($handle, filesize(ARCHIVO_CONF));
			fclose($handle);

            $xml = new SimpleXMLElement($contents);
           
            return array("asunto"=>$xml->emails->{$email}->asunto,"url"=>$xml->emails->{$email}->url);
            
        }
        public static function getValor2($seccion, $seccion1, $parametro="")
        {   
            $objConfig = new xmlConf(ARCHIVO_CONF);
            $Valor = $objConfig->getValor2($seccion, $seccion1, $parametro);
            return $Valor;
        }
        public static function CargarIncludes($includes)
        {   
            foreach ($includes as $key => $inc)
            {
                if ($key != "post")
                    require_once(Aplicacion::getRoot().$inc);
            }
        }
        /**
        * @desc Devuelve info.
        * @var string.
        * @return string.
        */
        public static function getInfo($parametro)
        {   
            $dir =Aplicacion::getParametros("info", $parametro);
            return $dir;
        }
        /**
        * @desc Encrypter.
        * @var string.
        * @return string.
        */
        public static function Encrypter($clave)
        {   
            try
            {
                //$Enc = new Encryption();
                //$NClave = $Enc->encrypt(Aplicacion::getParametros("info", "key"), $clave);
                $Enc = new textEncrypter();
                $NClave = $Enc->encode($clave);                
                return $NClave;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        /**
        * @desc Decypter.
        * @var string.
        * @return string.
        */
        public static function Decrypter($clave)
        {   
            try
            {
                //$Enc = new Encryption();
                //$NClave = $Enc->decrypt(Aplicacion::getParametros("info", "key"), $clave);
                $Enc = new textEncrypter();
                $NClave = $Enc->decode($clave);
                return $NClave;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public static function loginUser(usuarios $objUsuario)
        {
        	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
            try
            {
                //@session_register("User");
                unset($_SESSION["BO"]["User"]);
                $Tiempo = Aplicacion::getParametros("info", "usuarioexpiracion");
                session_cache_limiter($Tiempo);                
                $_SESSION["BO"]["User"]["nombre"] = Aplicacion::Encrypter($objUsuario->getDatos()->getApellido().", ".$objUsuario->getDatos()->getNombre());
                $_SESSION["BO"]["User"]["email"] = Aplicacion::Encrypter($objUsuario->getNombreUsuario());
                $_SESSION["BO"]["User"]["id"] = Aplicacion::Encrypter($objUsuario->getIdUsuario());
                $_SESSION["BO"]["User"]["perfil"] = Aplicacion::Encrypter($objUsuario->getIdPerfil());
                $_SESSION["BO"]["User"]["provisoria"] = Aplicacion::Encrypter($objUsuario->getes_Provisoria());
                $_SESSION["BO"]["User"]["activa"] = Aplicacion::Encrypter($objUsuario->getes_Activa());
                $_SESSION["BO"]["User"]["secciones"] = $objUsuario->getSecciones();  
                $_SESSION["BO"]["User"]["Dir"] = Aplicacion::Encrypter(Aplicacion::getDirLocal());
                $_SESSION["BO"]["User"]["country"] = $confRoot[1];  
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public static function logoutUser()
        {
            try
            {
                unset($_SESSION["BO"]["User"]);
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        /*public static function AplicarReglas($seccion)
        {
            try
            {
                $Tipo = Aplicacion::getValor2("reglasnegocio", "seccion", $seccion, $reglaext=null, $idcampania=null, $idproductos=null);
                if($Tipo != "")
                {
                    Aplicacion::CargarIncludes(Aplicacion::getIncludes("reglasnegocio"));
                    $Varias_veces = Aplicacion::getParametros("reglasnegocio","varias_veces");
                    $Unica_vez = Aplicacion::getParametros("reglasnegocio","unica_vez");
                    $Varias_veces = explode(",", $Varias_veces);
                    $Unica_vez = explode(",", $Unica_vez);
                    $Id = Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]);
                    if (in_array($Tipo, $Varias_veces))
                    {
                        $collReglas = dmReglasNegocio::getReglaByTipo($Tipo);
                    }
                    else if(in_array($Tipo, $Unica_vez))
                    {
                        $collReglas = dmReglasNegocio::getReglaByTipoUnicaVez($Tipo, $Id);
                    }
                    dmReglasNegocio::AplicarReglas($collReglas, $Id, $reglaext, $idcampania, $idproductos);
                }
            }
            catch(exception $e)
            {
                throw $e;
            }
        }*/
		public static function getCampaniasImagenesRoot($IdCampania)
        {
            return Aplicacion::getRoot() . "front/campanias/archivos/campania_".$IdCampania."/";
        }
        public static function getCampaniasRoot($IdCampania)
        {
            return Aplicacion::getRoot()."bo/campanias/archivos/campania_".$IdCampania."/";
        }
        public static function getCampaniasRootUrl($IdCampania)
        {
            return Aplicacion::getRootUrl()."bo/campanias/archivos/campania_".$IdCampania."/";
        }
		public static function getProductosImagenesRoot($IdProducto)
        {
            return Aplicacion::getRoot() . "front/productos/productos_".$IdProducto."/";
        }
		public static function getProductosImagenesRootUrl($IdProducto)
        {
            return Aplicacion::getRootUrl()."front/productos/productos_".$IdProducto."/";
        }
        public static function getProductosRoot($IdProducto)
        {
            return Aplicacion::getRoot()."bo/productos/productos_".$IdProducto."/";
        }
        public static function getProductosRootUrl($IdProducto)
        {
            return Aplicacion::getRootUrl()."bo/productos/productos_".$IdProducto."/";
        }
    }
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("framework"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("core"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("reglasnegocio"));
    $objReglaNegocio = new ReglasNegocioGestion();
	
	//Timezone configuration
    date_default_timezone_set(Aplicacion::getParametros('info', 'timezone'));
    
    $c= new Conexion();
    mysql_query("SET time_zone = '".Aplicacion::getParametros('info', 'mysql_timezone')."'");

?>