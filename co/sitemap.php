<?php
	ob_start();
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));     
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("catalogo"));

	$dtcampanias = dmCampania::getCampaniasOrdenadas(true, true, 0, $idCatGlobal);
	
	function encode($string) {
		return utf8_encode(html_entity_decode($string, ENT_COMPAT | ENT_HTML401, 'UTF-8'));
	}
	
	ob_end_clean();
	header("Content-type: text/xml");
	echo '<'.'?xml version="1.0" encoding="UTF-8"?>';
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
	<?php 
		
		foreach($dtcampanias as $i => $camp):
			
			$arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$camp["IdCampania"]."/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));
			$prods = dmProductos::getProductosAx($camp['IdCampania']);
	?>
		
	<url> 
		<loc>http://www.geelbe.com/co/front/catalogo/main.php?IdCampania=<?=$camp['IdCampania']?></loc> 
		<image:image>
			<image:loc><?=UrlResolver::getImgBaseUrl("front/campanias/archivos/campania_".$camp["IdCampania"]."/imagenes/".$arrArchivos["banner_camp-on"])?></image:loc> 
		</image:image>
		<campname><?=encode($camp['Nombre'])?></campname>
		<porc-desc><?=$camp['DescuentoMax']?></porc-desc>
	</url>
			
		<?php
			foreach($prods as $j => $p):
				 $arrImg = DirectorytoArray(Aplicacion::getRoot() . "front/productos/productos_".$p["IdProducto"]."/", 3, array("jpg", "jpeg", "gif", "png"));
		?>
		
		<url> 
			<loc>http://www.geelbe.com/co/front/catalogo/detalle_public.php?IdCampania=<?=$camp['IdCampania']?>&amp;IdCategoria=<?=dmProductos::getCategoria_byIdProducto($p['IdProducto'], $camp['IdCampania'])?>&amp;IdProducto=<?=$p['IdProducto']?></loc> 
			<image:image>
				<image:loc><?=UrlResolver::getImgBaseUrl("front/productos/productos_".$p["IdProducto"]."/producto_1.".get_extension($arrImg["producto_1"]))?></image:loc> 
			</image:image>
			<prodname><?=encode($p['Nombre'])?></prodname>
			<description-body><?=encode($p['Descripcion'])?></description-body>
		</url>
			
		<?php endforeach; ?>
	
	<?php endforeach; ?>
</urlset>