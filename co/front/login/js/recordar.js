function ValidarFormRec(formName, fieldName)
{
    try
    {
        var vError = new Array();
        if (!ValidarEmail(document.getElementById(fieldName).value))
        {
            vError.push(arrErrores["LOGIN"]["MAIL_INCORRECTO"]);
        }
        
        if (vError.length > 0)
        {
            oCortina.showError(vError, "Datos incompletos");
            //return false;
        }
        else
        {
            oCortina.showCargar("Procesando...");
            document.forms[formName].submit(); 
        }
    }
    catch(e)
    {
      throw e;
    }
}

function msjError(strError, title)
{
    vError = new Array();
    vError.push(strError);
    oCortina.showError(vError, title);    
}