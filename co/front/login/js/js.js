function refrescar_captcha()
{
    document.getElementById("frmCaptcha").src = "../../logica/framework/class/imgcaptcha.php";
}
function ErrorMSJ(titulo, code)
{
    oCortina.showError(Array(arrErrores["LOGIN"][code]), titulo);
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(!ValidarEmail(frm.txtUser.value))
        {
            arrError.push(arrErrores["LOGIN"]["MAIL_INCORRECTO"]);
        }
        if(frm.txtPass.value.length < 6 || frm.txtPass.value.length > 16)
        {
            arrError.push(arrErrores["LOGIN"]["CLAVE_INCORRECTO"]);
        }
        if (arrError.length>0)
        {
            oCortina.showError(arrError, arrErrores["LOGIN"]["TITLE_ERROR"]);
            return false;
        }
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}