function refrescar_captcha()
{
    document.getElementById("frmCaptcha").src = "../../logica/framework/class/imgcaptcha.php";
}
function ValidarFormRecuperar(frm)
{
    var vErrores = new Array();
    if (frm.txtPass.value == "" || frm.txtRepetirPass.value == "" )
    {
        vErrores.push(arrErrores["RECUPERAR"]["CLAVE"]);
    }
    else if (frm.txtPass.value != frm.txtRepetirPass.value)
    {
        vErrores.push(arrErrores["RECUPERAR"]["CLAVES DIFERENTES"]);
    }
    if (vErrores.length > 0)
    {
        msjError(vErrores, arrErrores["RECUPERAR"]["FRACASO"]);
    }
    else
    {
        frm.submit();
    }
}