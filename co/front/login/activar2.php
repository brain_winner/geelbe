<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::logoutUser();
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("recordar"), $_SERVER["DOCUMENT_ROOT"] . "/");
    $postURL = Aplicacion::getIncludes("post", "recordar");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/botones.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/login/js/js.js");?>" type="text/javascript"></script>
<script src="<?=UrlResolver::getJsBaseUrl("front/login/js/recordar.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">
	<h1 style="margin-top:20px;">Activaci&oacute;n de Cuenta</h1>
  </div>
  <div id="contenido">
<form target="iRecordar" name="frmRecordar" id="frmRecordar" method="post" action="../../../<?=Aplicacion::getDirLocal().$postURL["recordar"]?>" onSubmit="ValidarForm(this);">  
                <table border="0" align="left" cellpadding="0" cellspacing="0" class="txtblack">
                  <tr>
                    <td height="28" class="titulo" align="left">Volver a solicitar activaci&oacute;n de cuenta</td>
                  </tr>
                  <tr>
                    <td height="7" class="txtblack"><p>&nbsp;</p>
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <th width="180" scope="col"><div align="left">Dirección de correo electrónico <br />
                              <br />
                                    <input name='txtEmail' type='text' class="imputs" id='txtEmail' size="35" />
                            </div></th>
                            <th width="40" scope="col"><img src="<?=UrlResolver::getImgBaseUrl("front/login/images/x.gif");?>" width="1" height="1" /></th>
                            <th width="240" scope="col"><div align="left"></div></th>
                          </tr>
                          <tr>
                            <th height="10" colspan="3" scope="col"><img src="<?=UrlResolver::getImgBaseUrl("front/login/images/x.gif");?>" width="1" height="1" /></th>
                          </tr>
                          <tr>
                            <th height="10" colspan="3" scope="col"><img src="<?=UrlResolver::getImgBaseUrl("front/login/images/x.gif");?>" width="1" height="1" /></th>
                          </tr>
                          <tr>
                            <th colspan="3" scope="col"><div align="left">
                              <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td>								  
								   <table class="btn btnRed izquierda chico XXS izquierda " cellpadding="0" cellspacing="0" border="1px">
									<tr>
										<td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
											<a href="javascript:document.forms['frmRecordar'].onsubmit();"><strong>Enviar</strong>&nbsp;&nbsp;<img style="vertical-align:middle" src="<?=UrlResolver::getImgBaseUrl("front/images/bot_ico_flecha_vidrieraxsmall.jpg");?>" border="none" /></a>
										</td>
									</tr>
								</table>
                                 </td> 
                                </tr>
                              </table>
                            </div></th>
                          </tr>
                          <tr>
                            <th height="10" colspan="3" scope="col"><img src="<?=UrlResolver::getImgBaseUrl("front/login/images/x.gif");?>" width="1" height="1" /></th>
                          </tr>
                          <tr>
                            <th height="180" scope="col">&nbsp;</th>
                            <th width="40" scope="col">&nbsp;</th>
                            <th width="240" scope="col">&nbsp;</th>
                          </tr>
                      </table></td>
                  </tr>
                  <tr>
                    <td height="15"><img src="<?=UrlResolver::getImgBaseUrl("front/login/images/x.gif");?>" width="1" height="1" /></td>
                  </tr>
                </table>
            </form>
  </div>
	<div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
<iframe style="display:none" id="iRecordar" name="iRecordar"></iframe>
</body>
</html>
