<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::logoutUser();
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("recordar"), $_SERVER["DOCUMENT_ROOT"] . "/");
    $postURL = Aplicacion::getIncludes("post", "recordar");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/botones.css");?>" rel="stylesheet" type="text/css"  />
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe_new.css")?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/login/js/recordar.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">
    <h1 style="margin-top:20px;">Recordar Contrase�a y Activar Cuenta</h1>
  </div>
  <div id="contenido">
                <style>
                    th.recordarinfobox {
                        border: 3px solid rgb(201, 46, 46); 
                        padding: 5px 5px 30px;
                    }
                    
                    th.recordarinfobox .boxinput {
                        margin-bottom: 8px;
                    }
                    
                    th.recordarinfobox .boxdesc {
                        padding-bottom: 17px;
                        padding-top:10px;
                        
                    }
                </style>
                <table border="0" align="left" cellpadding="0" cellspacing="0" class="txtblack">
                  <tr>
                    <td height="7" class="txtblack"><p>&nbsp;</p>
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <th width="324" scope="col" class="recordarinfobox">
                              <form target="iRecordar" name="frmRecordar" id="frmRecordar" method="post" action="../../../<?=Aplicacion::getDirLocal().$postURL["recordar"]?>" onSubmit="ValidarFormRec('frmRecordar', 'txtEmailRecordar');">  
                                <div align="left"><span class="boxtitle">�Necesitas recordar tu contrase&ntilde;a?</span> <br />
                                  <p class="boxdesc">Si no recuerdas tu contrase&ntilde;a de acceso a Geelbe, debes ingresar tu cuenta de correo electr&oacute;nico y te enviaremos el acceso a la misma:</p>
                                  <input name='txtEmailRecordar' type='text' class="imputs boxinput" id='txtEmailRecordar' size="35" /><br />
                                 						  
								  <table class="btn btnRed izquierda chico XXS" cellpadding="0" cellspacing="0" border="1px">
									<tr>
										<td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
											<a href="javascript:document.forms['frmRecordar'].onsubmit();"><strong>Enviar</strong>&nbsp;&nbsp;<img style="vertical-align:middle" src="<?=UrlResolver::getImgBaseUrl("front/images/bot_ico_flecha_vidrieraxsmall.jpg");?>" border="none" /></a>
										</td>
									</tr>
								</table>
								  
                                </div>
                              </form>
                            </th>
                            <th width="20" scope="col"><div align="left"></div></th>
                            <th width="324" scope="col"  class="recordarinfobox">
                              <form target="iRecordar" name="frmActivar" id="frmActivar" method="post" action="../../../<?=Aplicacion::getDirLocal().$postURL["activarurl"]?>" onSubmit="ValidarFormRec('frmActivar', 'txtEmailActivar');">  
                                <div align="left"><span class="boxtitle">�Necesitas activar tu cuenta?</span> <br />
                                  <p class="boxdesc">Si nunca has recibido el correo electr&oacute;nico para que actives tu cuenta Geelbe, completa tu cuenta de correo electr&oacute;nico aqu&iacute;:</p>
                                  <input name='txtEmailActivar' type='text' class="imputs boxinput" id='txtEmailActivar' size="35" /><br />
                                 							 
								  <table class="btn btnRed izquierda chico XXS izquierda " cellpadding="0" cellspacing="0" border="1px">
									<tr>
										<td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
											<a href="javascript:document.forms['frmActivar'].onsubmit();"><strong>Enviar</strong>&nbsp;&nbsp;<img style="vertical-align:middle" src="<?=UrlResolver::getImgBaseUrl("front/images/bot_ico_flecha_vidrieraxsmall.jpg");?>" border="none" /></a>
										</td>
									</tr>
								</table>
								 
                                </div>
                              </form>
                            </th>
                          </tr>
                          <tr>
                            <th height="324" scope="col">&nbsp;</th>
                            <th width="20" scope="col">&nbsp;</th>
                            <th width="324" scope="col">&nbsp;</th>
                          </tr>
                      </table></td>
                  </tr>
                  <tr>
                    <td height="15"><img src="<?=UrlResolver::getImgBaseUrl("front/login/images/x.gif");?>" width="1" height="1" /></td>
                  </tr>
                </table>    
  </div>
  <div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
<iframe style="display:none" id="iRecordar" name="iRecordar"></iframe>
</body>
</html>
