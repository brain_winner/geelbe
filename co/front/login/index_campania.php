<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::logoutUser();
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("catalogo"));
    $postURL = Aplicacion::getIncludes("post", "login");
    
    $objCampania = dmCampania::getByIdCampania($_GET["IdCampania"]);
    $arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$_GET["IdCampania"]."/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));
    
    $Dias = Aplicacion::getParametros("campania_new", "dias") * 24 * 60 * 60;
    $Hoy = mktime();
    $FechaCampania = strtotime($objCampania->getFechaInicioEn());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <? Includes::Scripts() ?>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title><?=Aplicacion::getParametros("info", "nombre");?> </title> 

    <link href="<?=UrlResolver::getCssBaseUrl("front/geelbe_new.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?=UrlResolver::getCssBaseUrl("front/botones.css");?>" rel="stylesheet" type="text/css"  />
    <link href="<?=UrlResolver::getCssBaseUrl("front/highslide.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?=UrlResolver::getCssBaseUrl("front/vidriera/css/vidriera.css");?>" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.4.min.js")?>"></script>
    <script src="<?=UrlResolver::getBaseUrl("front/login/js/js.js", true);?>" type="text/javascript"></script>
  </head>
  <body class="campania-login-body"> 
    <? require("../menuess/cabecera.php") ?>
    <script type="text/javascript">
      fechaHoy = "<?=date("m/d/Y H:i:s");?>";
      $('#primary, #loginPopup').remove();
    </script>
    
	<div id="container-vidriera" style="padding-bottom:0">

		<div style="padding-top:15px;">
			<?php require_once dirname(__FILE__).'/../categorias/menu.php'; ?>
		</div>

	</div>

	<div class="campania-login">
			
		<p class="off">Hasta 70% OFF en tus marcas preferidas</p>                
				
		<form  method="post" name="frmLogin" id="frmLogin" onkeypress="if (ValidarEnter(event)) this.onsubmit();" target="iLogin" action="/<?=Aplicacion::getDirLocal()?>logica/login/actionform/login.php">
  	
			<div class="logo">
				<img src="<?=UrlResolver::getImgBaseUrl("front/campanias/archivos/campania_".$_GET["IdCampania"]."/imagenes/".$arrArchivos['logo2']);?>" />
                               
                        </div>                    
                       

			<fieldset>
				<h2>Acceso para socios</h2>
					
				<input name="txtUser" maxlength="60" placeholder="Email" type="text" />
				<input name="txtPass" type="password" placeholder="Contrase&ntilde;a" maxlength="60" />
				<input name="redirectCamp" type="hidden" value="<?=$objCampania->getIdCampania()?>" />
				
				<p class="olvidaste"><a href="<?=UrlResolver::getBaseUrl("front/login/recordar2.php");?>">&iquest;Olvidaste tu contrase&ntilde;a?</a></p>
				
				<div class="buttons">
					<a href="http://<?=$_SERVER["SERVER_NAME"]?>/appfacebook/register.php?country=co&codact=facebooklogin" target="_blank">
						<img src="/<?=Aplicacion::getDirLocal()?>front/images/new_facebook2.png" alt="Facebook" />
					</a>
					<input type="image" src="/<?=Aplicacion::getDirLocal()?>front/images/new_entrar2.png" name="login" value="Entrar" id="loginButton" />
				</div>
			</fieldset>
			
			<fieldset class="ultimo">
				<h2>&iquest;A&uacute;n no eres socio?</h2>
				
				<div class="buttons">
					<a href="http://<?=$_SERVER["SERVER_NAME"]?>/appfacebook/register.php?country=co&codact=facebooklogin" target="_blank">
						<img src="/<?=Aplicacion::getDirLocal()?>front/images/new_facebook3.png" alt="Facebook" />
					</a>
					<a href="/<?=Aplicacion::getDirLocal()?>registro">
						<img src="/<?=Aplicacion::getDirLocal()?>front/images/new_register.png" alt="Facebook" />
					</a>
				</div>
			</fieldset>
			
		</form>
		
		<div class="main">
			<p class="termina">
				<?php if($Hoy >= $FechaCampania && $Hoy <= ($FechaCampania + $Dias)): ?>
				
				&iexcl;RECI&Eacute;N COMIENZA!
				
				<?php else: ?>
				
				<?=$objCampania->getNombre()?>
				finaliza en
				<span id="reloj<?=$_GET["IdCampania"]?>"><script>IniciarReloj("reloj<?=$_GET["IdCampania"]?>", "<?=$objCampania->getFechaFinEn()?>", fechaHoy, false);</script></span>
				
				<?php endif; ?>
			</p>
			<img src="<?=UrlResolver::getImgBaseUrl("front/campanias/archivos/campania_".$_GET["IdCampania"]."/imagenes/".$arrArchivos['welcome']);?>" alt="" />
                         <div style="position: absolute; left:570px; bottom: 50px;">
                        <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.geelbe.com%2Fco%2Ffront%2Flogin%2Findex_campania.php%3FIdCampania%3D<?php echo $_GET["IdCampania"]; ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
                    </div>
		</div>
		
		<div style="clear:both"></div>
		
	</div>
	
	<? require("../menuess/footerNew.php");?>
<div id="fb-root"></div>
<script>
	
	window.fbAsyncInit = function() {
		FB.init({appId: '130962696927517', status: true, cookie: false,
						 xfbml: true});
	};
	(function() {
		var e = document.createElement('script'); e.async = true;
		e.src = document.location.protocol +
			'//connect.facebook.net/es_LA/all.js';
		document.getElementById('fb-root').appendChild(e);
	}());
</script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() {
	$("#loginButton").click(function() {
		if(ValidarForm($("#frmLogin")[0])) {
			$("#frmLogin").submit();
		}
	});
	
});
//]]>
</script>
</body>
</html>
