<?
	header("Location: ../vidriera/index.php");
	die;
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::logoutUser();
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login"));
    $postURL = Aplicacion::getIncludes("post", "login");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Geelbe - Club Privado de Compras</title>
	<link href="<?=UrlResolver::getBaseUrl("css/login.css", true);?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getBaseUrl("css/button.css", true);?>" rel="stylesheet" type="text/css" />
	<?
	    Includes::Scripts(true);
	?>
	<script src="<?=UrlResolver::getBaseUrl("js/jquery.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("front/login/js/js.js", true);?>" type="text/javascript"></script>
</head>

<body>
	<div id="container">
		<!--HEADER-->
		<div id="cont_1">
			<div id="header_1">
				<div id="logo"><img src="<?=UrlResolver::getImgBaseUrl("img/logo.jpg");?>" width="226" height="83" border="0" /></div>
				<div id="registra"><a id="registerButton" class="button large red" href="<?=UrlResolver::getBaseURL("registro/index.html");?>"><span>Registrate</span></a></div>
				<div id="cont_login">
					<!--INICIA FORM-->
					<div id="form_box">
						<form  method="post" name="frmLogin" id="frmLogin" onkeypress="if (ValidarEnter(event)) this.onsubmit();" target="iLogin" action="/<?=Aplicacion::getDirLocal().$postURL["login"]?>">
							<input type="hidden" name="auto" id="auto" value="<?=$campaign?>" />
	
							<div id="form_tit" class="form_tit">Acceso para Socios</div>
							<div id="form_email1" class="form_txt">Email</div>
							<div id="form_email2"><input name="txtUser" maxlength="60" type="text" src="<?=UrlResolver::getImgBaseUrl("img/bt_inn.jpg");?>" style="width:140px; height:17px;" class="form_txt2" /></div>
							<div id="form_pass1" class="form_txt">Contrase&ntilde;a</div>
							<div id="form_pass2"><input name="txtPass" type="password" maxlength="60" style="width:140px; height:17px;" class="form_txt2" /></div>
							<div id="form_pass3" class="form_txt"><a href="<?=UrlResolver::getBaseUrl("front/login/recordar2.php");?>">¿Olvidaste tu contrase&ntilde;a?</a></div>
							
							<div id="form_rem" class="form_txt">
								<a href="http://<?=$_SERVER["SERVER_NAME"]?>/appfacebook/register.php?country=co&codact=facebooklogin" target="_blank"><img src="images/facebook.jpg" alt="Facebook" /></a>
							</div>
							
							<div id="form_send"><a id="loginButton" class="button small red" href="javascript:;"><span>Entrar</span></a></div>
							<input type="submit" name="submitButton" value="" style="visibility:hidden;" />
						</form>
					</div>
					<!--FIN FORM-->
				</div>
			</div>
		</div>
		<!--FIN HEADER-->
		
		<!-- PICTURE-->
		<div id="picture"></div>
		<!--FIN PICTURE-->
		
		<!-- BOTTOM-->
		<div id="cont_3">
			<div id="bottom_3">
				<div id="cont_menu">
					<div id="menu" class="menu"><a href="/<?=$confRoot[1]?>/front/faq" target="_blank">¿C&oacute;mo funciona Geelbe?</a> | <a href="<?=UrlResolver::getBaseURL("registro/index.html");?>">Hacerme Socio</a> | <a href="http://blog.geelbe.com/co" target="_blank">Blog</a> | <a href="/<?=$confRoot[1]?>/front/faq" target="_blank">Ayuda</a></div>
				</div>
				<div id="sponsors">
					<div id="sponsor_img"><img src="<?=UrlResolver::getImgBaseUrl("img/logos_cert.jpg");?>" border="0" /></div>
					<div id="copyright" class="copyright">Geelbe © 2012 | <a href="/<?=$confRoot[1]?>/front/terminos" target="_blank">T&eacute;rminos y Condiciones</a> | <a href="/<?=$confRoot[1]?>/front/faq" target="_blank">Ayuda</a></div>
				</div>
				<div id="socials">
					<div id="twitter"><a href="https://twitter.com/geelbeco" class="twitter-follow-button" data-button="grey" data-text-color="#FFFFFF" data-link-color="#00AEFF" data-lang="es">Segui @geelbeco</a></div>
					<div id="facebook"><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2FGeelbeColombia&amp;send=false&amp;layout=standard&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=dark&amp;font&amp;height=35" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:35px;" allowTransparency="true"></iframe></div>
				</div>
			</div>
		</div>
		<!--FIN BOTTOM-->
		
		<iframe style="display:none" id="iLogin" name="iLogin"></iframe>
	</div>
</body>
<script type="text/javascript" src="http://www.google-analytics.com/urchin.js"></script>
<script type="text/javascript">
	_uacct = "UA-6895743-8";urchinTracker();
  document.getElementById("txtUser").focus();
</script>
<script src="//platform.twitter.com/widgets.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() {
	var backgroundClasses = ['nike','tramontina','ermenegildo'];
	var rndNum = Math.floor(Math.random() * backgroundClasses.length);
	$("#picture").removeClass();
	$("#picture").addClass(backgroundClasses[rndNum]);

	$("#loginButton").click(function() {
		if(ValidarForm($("#frmLogin")[0])) {
			$("#frmLogin").submit();
		}
	});
});
//]]>
</script>
</html>
