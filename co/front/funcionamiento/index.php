<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/funcionamiento/js/js.js");?>" type="text/javascript"></script>

<style type="text/css">
<!--
.style1 {
	color: #666666;
	font-weight: bold;
}
-->
</style>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">

  </div>
  <div id="contenido">
	<div id="top">	
	  <h1>Acerca de Geelbe</h1>
	</div>
	<div id="central">
	  <h2 class="reloj"><strong>Sobre el Funcionamiento de Geelbe</strong></h2>
	  <p>&nbsp;</p>
	  <p><strong>Geelbe</strong> te da la bienvenida como Miembro de su Club Privado  de Compras por Internet y te presenta r&aacute;pidamente su funcionamiento.<br />
&nbsp;&nbsp;<br />
Inicio: Deber&aacute;s  identificarte, ingresando tu eMail y contrase&ntilde;a, para poder entrar a la secci&oacute;n  exclusiva para Miembros del Club de Compras.<br />
&nbsp;&nbsp;<br />
Escaparate: En la parte superior de la p&aacute;gina principal  encontrar&aacute;s las Campa&ntilde;as de Ventas en curso y m&aacute;s abajo podr&aacute;s ver las  propuestas que se presentar&aacute;n en <strong>Geelbe</strong>.  Desde el Escaparate puedes acceder a cada una de las Campa&ntilde;as de Ventas, con  los productos ofrecidos en cada una, a tu cuenta de usuario o a la secci&oacute;n de  novedades e informaci&oacute;n corporativa.<br />
&nbsp;&nbsp;<br />
Campa&ntilde;a de Venta: Cada marca ofrece sus productos a trav&eacute;s de su  Campa&ntilde;a de Venta en <strong>Geelbe</strong>,  con un cat&aacute;logo que te permite acceder a la descripci&oacute;n detallada de cada  art&iacute;culo y seleccionar las caracter&iacute;sticas del que deseas agregar a tu carrito  de compras.<br />
&nbsp;&nbsp;<br />
Mi Cuenta: Aqu&iacute; encontrar&aacute;s toda tu informaci&oacute;n personal, los  datos que has ingresado en el registro, tus compras realizadas, con los pedidos  en curso y los entregados y la lista de tus referidos, as&iacute; como el detalle del  dinero acreditado en tu cuenta por las compras que ellos realizan.<br />
&nbsp;&nbsp;<br />
Blog Geelbe: Adem&aacute;s de la secci&oacute;n de ayuda, los documentos legales  y el formulario de contacto, <strong>Geelbe</strong> mantiene un blog con informaci&oacute;n corporativa y novedades sobre las marcas que  se actualiza permanentemente para comunicar noticias a sus miembros.<br />
&nbsp;&nbsp;<br />
Invita Amigos: A trav&eacute;s del formulario de recomendaci&oacute;n puedes  referir a tus amigos, envi&aacute;ndoles un eMail de invitaci&oacute;n con tu c&oacute;digo de  registro a <strong>Geelbe</strong>,  y obsequiarles $10.000 para que utilicen en su primera compra (superior a $50.000).</p>
	  <p>&nbsp;</p>
	  <p>Cuando tus invitados realicen su primera  compra se acreditar&aacute;n $10.000 en tu cuenta, que podr&aacute;s utilizar en tus compras  futuras. Por cada amigo referido que realice su primera compra recibir&aacute;s $10.000,  por lo tanto mientras m&aacute;s recomiendes <strong>Geelbe</strong> m&aacute;s dinero podr&aacute;s obtener para tus compras.</p>
	  <p><br />
      </p>
	  <p></p>
	</div>
  <div id="menuleft">
		<? 
		$act = 4;
		require("../menuess/acercade.php")
		?>
	</div>
</div>
  <div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
</body>
</html>
