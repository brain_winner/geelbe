<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	require '../lib/connect.php';
	
	/*function buildList($cats, $n = 0) {
      
		//Preguntas
		foreach($cats['faqs'] as $i => $d)			
			echo '<div class="pregunta" id="f'.$d['id'].'">'.$d['nombre'].'</div>
      				<div id="rf'.$d['id'].'">
      					<div class="respuesta" style="padding-left:10px;">'.$d['respuesta'].'</div>
	      				<div class="dot"></div>
	      			</div>'."\n";
		
		//Categor�as
		if(isset($cats['sub']))
			foreach($cats['sub'] as $i => $d) {
				echo '<div class="'.($n == 0 ? 'cat' : 'sub').'">'.$d['nombre']."</div>\n";				
				echo buildList($d, $n+1);
				//echo '<div class="dot"></div>'."\n";
			}		
	}*/
	
	function buildList($cats, $n = 0) {
		
		echo '<ul'.($n == 0 ? ' id="faq"' : '').'>';

		//Preguntas
		foreach($cats['faqs'] as $i => $d)			
			echo '<li><h2 class="pregunta" id="f'.$d['id'].'">'.$d['nombre'].'</h2>
			<div class="respuesta" id="rf'.$d['id'].'">'.htmlspecialchars_decode($d['respuesta']).'</div></li>'."\n";
		
		//Categor�as
		if(isset($cats['sub']))
			foreach($cats['sub'] as $i => $d) {
				echo '<li><div class="'.($n == 0 ? 'cat' : 'sub').'">'.$d['nombre']."</div>";
				echo buildList($d, $n+1);
				echo '</li>'."\n";
			}
		
		
		echo '</ul>';
		
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Preguntas</title>
		<link href="<?=UrlResolver::getCssBaseUrl("front/faq/faq.css");?>" type="text/css" rel="stylesheet" />
		<link href="<?=UrlResolver::getCssBaseUrl("front/faq/js/jquery.treeview.css");?>" type="text/css" rel="stylesheet" />
		<script src="<?=UrlResolver::getJsBaseUrl("front/faq/js/jquery.js");?>" type="text/javascript"></script>
		<script src="<?=UrlResolver::getJsBaseUrl("front/faq/js/jquery.treeview.pack.js");?>" type="text/javascript"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
					
				<?php
					if(isset($_SESSION["User"]["id"]))
						echo 'login = "&login=true"';
					else
						echo 'login = ""';
				?>	
					
				var h = $('.all .pregunta');
				for(i = 0; i < h.length; i++) {
					
					var resp = document.getElementById('r'+h[i].id);
					resp.style.display = 'none';
					
					h[i].style.cursor = 'pointer';
					h[i].onclick = function() {
						
						var resp = document.getElementById('r'+this.id);
						if(resp.style.display == 'none') {
							resp.style.display = 'block';
							$.get('visit.php?id='+this.id.substr(1)+login);
						} else {
							resp.style.display = 'none'
						}
					
					}

				
				}
				
				$("#faq").treeview({
					collapsed: true,
					unique: false
				});
				
				
			})			
		</script>
	</head>
	<body>
	<div class="all">
<?php		
			
	$cats = buildArray();
			
	if(count($cats) == 0) {
	
		echo '<p>No hay preguntas registradas.</p>';
	
	} else {

		buildList($cats);
	
	}

?>
	</div>
	</body>
</html>
