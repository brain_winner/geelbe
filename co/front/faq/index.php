<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	require '../lib/connect.php';
	require_once 'Zend/Cache.php'; 
	
	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
	$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 3600), array('cache_dir' => $cacheDir));
	
	function buildList($cats, $n = 0) {
		
		echo '<ul'.($n == 0 ? ' id="faq"' : '').'>';

		//Preguntas
		foreach($cats['faqs'] as $i => $d)			
			echo '<li><h2 class="pregunta" id="f'.$d['id'].'">'.$d['nombre'].'</h2>
			<div class="respuesta" id="rf'.$d['id'].'">'.stripslashes(htmlspecialchars_decode($d['respuesta'])).'</div></li>'."\n";
			
		//Categorias
		if(isset($cats['sub']))
			foreach($cats['sub'] as $i => $d) {
				echo '<li><div class="'.($n == 0 ? 'cat' : 'sub').'" id="'.str_replace(' ', '', $d['nombre']).'">'.$d['nombre']."</div>";
				echo buildList($d, $n+1);
				echo '</li>'."\n";
			}
		
		
		echo '</ul>';
		
	}
	
?>


<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Preguntas</title>
		<link href="<?=UrlResolver::getCssBaseUrl("front/faq/faq.css")?>" type="text/css" rel="stylesheet" />
		<link href="<?=UrlResolver::getBaseUrl("front/geelbe_new.css", true);?>" rel="stylesheet" type="text/css" />
		<script src="<?=UrlResolver::getJsBaseUrl("front/faq/js/jquery.js");?>" type="text/javascript"></script>
		<link href="<?=UrlResolver::getCssBaseUrl("front/faq/js/jquery.treeview.css")?>" type="text/css" rel="stylesheet" />
		<script src="<?=UrlResolver::getJsBaseUrl("front/faq/js/jquery.treeview.pack.js");?>" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() {
					
				<?php
					if(isset($_SESSION["User"]["id"]))
						echo 'login = "&login=true"';
					else
						echo 'login = ""';
				?>	
					
				var h = $('.all .pregunta');
				for(i = 0; i < h.length; i++) {
					
					var resp = document.getElementById('r'+h[i].id);
					resp.style.display = 'none';
					
					h[i].style.cursor = 'pointer';
					h[i].onclick = function() {
						
						var resp = document.getElementById('r'+this.id);
						if(resp.style.display == 'none') {
							resp.style.display = 'block';
							$.get('visit.php?id='+this.id.substr(1)+login);
						} else {
							resp.style.display = 'none'
						}
					
					}

				
				}
				
				$("#faq").treeview({
					collapsed: true,
					unique: false
				});
				
				$('#faq ul').get(0).style.display = 'block'				
				//Expandir acci�n de los botones del treeview
				//a todo el div contenedor
				$(".hitarea + div").css('cursor', 'pointer');
				$(".hitarea + div").click(function() {
					$(this.previousSibling).trigger('click');
				})

			})			
		</script>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css")?>" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {font-size: 18px}
-->
</style>
<link href="<?=UrlResolver::getCssBaseUrl("front/faq/faq.css")?>" rel="stylesheet" type="text/css" />

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
  <?   require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">

  </div>
  <div id="contenido">
	<div id="top">	
	  <h1>Preguntas Frecuentes</h1>
	</div>
	
	  
      
      <div class="all">
<?php		
	if(!$cacheFAQ = $cache->load('geelbe_cache_faq')) {
		ob_start();
		$cats = buildArray();
		if(count($cats) == 0) {
			echo '<p>No hay preguntas registradas.</p>';
		} else {
			buildList($cats);
		}
		$cacheFAQ = ob_get_clean();
		$cache->save($cacheFAQ, 'geelbe_cache_faq');
	}
	echo $cacheFAQ;
?>
	</div>
	 

   
	 
	  
</div>
  <div id="contenido-bot"></div>
</div>

<? require("../menuess/footer.php")?>

</body>
</html>
