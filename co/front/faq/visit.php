<?php 

	//Ajax (contador de visitas)
	if(!isset($_GET['id']) || !is_numeric($_GET['id']))
		die;
		
	session_start();
	if(!isset($_SESSION['faq'])) {
		$_SESSION['faq'] = array();
	}

	if(isset($_GET['login']))
		$type = 'login';
	else
		$type = 'no_login';

	header("Cache-Control: no-cache");
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php"); 
	include '../lib/connect.php';
	
	if(!isset($_SESSION['faq'][$_GET['id']])) {
		mysql_query("UPDATE faq SET visits_".$type." = visits_".$type."+1 WHERE id =".$_GET['id']);
		$_SESSION['faq'][$_GET['id']] = true;
	}
	
?>