function ErrorMSJ(titulo, code) {
    oCortina.showError(Array(arrErrores["CONTACTO"][code]), titulo);
}
function InfoMSJ(titulo, code, pagina, showbutton) {
        oCortina.showInfo(Array(arrErrores["CONTACTO"][code]), titulo, pagina, showbutton);
}
function ValidarForm(frm) {
    try {
        var arrError = new Array();
        if(!ValidarEmail(frm.txtEmail.value)) {
            arrError.push(arrErrores["CONTACTO"]["MAIL_INCORRECTO"]);
        }
        if(frm.txtComentario.value.length == 0) {
            arrError.push(arrErrores["CONTACTO"]["COMENTARIO"]);
        }
        if(frm.isCampaniaSel.value == "true") {
	        if(frm.txtMotivo.value != "31") {
	        	if(frm.txtCampania.value < 0) {
	        		arrError.push(arrErrores["CONTACTO"]["CAMPANIA"]);
	        	}
	        }
        }
        if (arrError.length >0) {
            oCortina.showError(arrError, arrErrores["CONTACTO"]["TITLE_ERROR"]);
            return false;
        }   
        frm.submit();
    }
    catch(e) {
        throw e;
    }
}