<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases")); 
        $postURL = Aplicacion::getIncludes("post","contacto");
        $datosUser = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css")?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/contacto-socio/js/js.js");?>" type="text/javascript"></script>
<script type="text/javascript">

function ismaxlength(obj){
var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : ""
if (obj.getAttribute && obj.value.length>mlength)
obj.value=obj.value.substring(0,mlength)
}

</script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">
	<h1 style="margin-top:20px;">Contacto</h1>
  </div>
  <div id="contenido">
	<h3><span style="margin-top:20px;"><img src="<?=UrlResolver::getImgBaseUrl("front/images/mail_edit.png");?>" alt="Contacto" width="48" height="48" align="middle" /></span>Para cualquier consulta completa el siguiente formulario. A la brevedad nos pondremos en contacto con vos. </h3>
   <form method="post" id="frmContacto" name="frmContacto" action="../../../<?=Aplicacion::getDirLocal().$postURL["contacto"]?>" target="iContacto" onSubmit="ValidarForm(this);">
    <input name="txtEmail" readonly="true" value="<? echo $datosUser["Email"];?>" type="hidden" size="30" />
    <input name="txtNombre" readonly="true" value="<? echo $datosUser["Nombre"]; ?>" type="hidden" size="30" />           
    <input name="txtApellido" readonly="true" value="<? echo $datosUser["Apellido"]; ?>" type="hidden" size="30" />   
	<table cellspacing="0" cellpadding="2" border="0">
      <tbody>
        <tr>
          <td height="10" background="<?=UrlResolver::getImgBaseUrl("front/images/gris-top.gif");?>" scope="col"></td>
          </tr>
          <tr>
          <td width="100%" colspan="2" bgcolor="#F0F0F0" scope="col">
          Motivo: <select name="motivo" id="motivo">
             <option>Contacto</option>
            <option>Pagos</option>
            <option>Pedidos</option>
            <option>Sugerencias</option>
            <option>Otros</option>
          </select>
          </td>
          </tr>
        <tr>
          <td bgcolor="#F0F0F0" scope="col"><strong>Usuario: <? echo $datosUser["Nombre"] . " " . $datosUser["Apellido"]; ?></strong></td>
          </tr>
        <tr>
          <td valign="top" bgcolor="#F0F0F0" scope="col">Asunto<br />
            <input name="txtAsunto" id="txtAsunto" size="65" /></td>
          </tr>
        
        <tr>
          <td 
                                height="10" align="left" bgcolor="#F0F0F0" scope="col">Comentario<br />
          <textarea name="txtComentario" cols="70" rows="4" maxlength="400" onKeyUp="return ismaxlength(this)"></textarea></td>
          </tr>
        <tr>
          <td height="10" background="<?=UrlResolver::getImgBaseUrl("front/images/gris-bottom.gif");?>" scope="col"></td>
        </tr>
        <tr align="left">
          <td scope="col">
	  	  	<div style="text-align:right"><a href="javascript:document.forms['frmContacto'].onsubmit();"><img src="<?=UrlResolver::getImgBaseUrl("front/images/boton-enviar.gif");?>" alt="Enviar Consulta" width="81" height="20" /></a></div></td>
          </tr>
      </tbody>
    </table>
	</form>
   <p>&nbsp;</p>
   <p>&nbsp;</p>
   <p>&nbsp;</p>
   <p>&nbsp;</p>
  </div>
  <div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
<iframe style="display:none" name="iContacto" id="iContacto"></iframe> 
</body>
</html>
