<?
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));  
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases")); 
        $postURL = Aplicacion::getIncludes("post","contacto");
        if(isset($_SESSION['User'])) {
        	$datosUser = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        }
    }
    catch(exception $e) {
		header('Location: /'.$confRoot[1]);
		exit;
    }
    try {
        ValidarUsuarioLogueado();
        $dtcampanias = dmCampania::getCampaniasUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        $hasCampains = count($dtcampanias)>0;
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <? Includes::Scripts() ?>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title><?=Aplicacion::getParametros("info", "nombre");?> </title>
    <link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?=UrlResolver::getCssBaseUrl("front/botones.css");?>" rel="stylesheet" type="text/css" />
    <script src="<?=UrlResolver::getJsBaseUrl("front/contacto-socio/js/js.js");?>" type="text/javascript"></script>
    <script src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.2.min.js");?>" type="text/javascript"></script>
	
    <script type="text/javascript">
		function ismaxlength(obj){
			var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : "";
			if (obj.getAttribute && obj.value.length>mlength) {
				obj.value=obj.value.substring(0,mlength);
			}
		}
    </script>
    
  <?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
  </head>
  <body>
    <? require("../menuess/cabecera.php") ?>
    <div id="container">
  
      <div id="contenido-top">
	    <h1 style="margin-top:20px;">Contacto</h1>
      </div>
  
      <div id="contenido">
	<h3><span style="margin-top:20px;"><img src="<?=UrlResolver::getImgBaseUrl("front/images/mail_edit.png");?>" alt="Contacto" width="48" height="48" align="middle" /></span>Para cualquier consulta completa el siguiente formulario. A la brevedad nos pondremos en contacto contigo.<br/> 
           <span style="color:#F00; font-size:11px;">* Recuerda que nuestro Departamento de Atenci&oacute;n al Socio se encuentra disponible de lunes a viernes <br/>
de 8:00 a 17:30 hs.</span>
        </h3>
        
        <form method="post" id="frmContacto" name="frmContacto" action="../../../<?=Aplicacion::getDirLocal().$postURL["contacto"]?>" target="iContacto" onSubmit="ValidarForm(this);">
          <input name="txtEmail" readonly="true" value="<? echo $datosUser["Email"];?>" type="hidden" />
          <input name="txtNombre" readonly="true" value="<? echo $datosUser["Nombre"]; ?>" type="hidden" />           
          <input name="txtApellido" readonly="true" value="<? echo $datosUser["Apellido"]; ?>" type="hidden" />   
	      
	      <table border="0" align="center" cellpadding="2" cellspacing="0">
            <tbody>
              <tr>
                <td height="10" background="<?=UrlResolver::getImgBaseUrl("front/images/gris-top.gif");?>" scope="col"></td>
              </tr>
              <tr>
                <td width="100%" colspan="2" bgcolor="#F0F0F0" scope="col">
                  Motivo: <br />
		          <select name="txtMotivo" id="txtMotivo">
		            <option value="100">Como hacer una compra
					<option value="101">Como pago en efectivo</option>
					<option value="102">Como usar Gcash</option>
					<option value="103">Como uso mis cr&eacute;ditos Geelbe</option>
		            <?php if($hasCampains) {?>
					<option value="104">Cuando llega mi pedido</option>
					<option value="105">Garant&iacute;as</option>
					<option value="41">Cambios y Devoluciones</option>
					<option value="31">Pagos</option>
		            <?php }?>
					<option value="99">Otros Motivos</option>
		          </select>
                </td>
              </tr>
              
              <tr id="campcombo" style="display:none;">
                <td width="100%" colspan="2" bgcolor="#F0F0F0" scope="col">
                  Marca: <br />
		          <select name="txtCampania" id="txtCampania">
					<option value="-1" id="sumC">Seleccione una Marca</option>
					<?php 
						$sdtcampanias = array_sort($dtcampanias, 'Lnombre', SORT_ASC);
						foreach ($sdtcampanias as $idx => $campania) {
							if ($campania["IdCampania"] == 270) {
								continue;
							}
					?>
							<option value="<?=$campania["IdCampania"];?>"><?=$campania["Nombre"];?></option>
					<?php
						}
					?>
		          </select>
		          <input type="hidden" name="isCampaniaSel" id="isCampaniaSel" value="false" />
                </td>
              </tr>

		      <tr>
         	    <td height="10" align="left" bgcolor="#F0F0F0" scope="col">
         	      Comentario:<br />
                  <textarea name="txtComentario" id="txtComentario" cols="70" rows="4" maxlength="400" onKeyUp="return ismaxlength(this)"></textarea>
                </td>
              </tr>
		      <tr>
		        <td height="10" background="<?=UrlResolver::getImgBaseUrl("front/images/gris-bottom.gif");?>" scope="col"></td>
		      </tr>
		      <tr align="left">
				<td scope="col">
			  	  <table class="btn btnRed derecha normal XS" cellpadding="0" cellspacing="0" border="1px">
				    <tr>
					  <td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
					    <a href="javascript:document.forms['frmContacto'].onsubmit();"><strong>Enviar</strong>&nbsp;&nbsp;<img style="vertical-align:bottom" src="<?=UrlResolver::getImgBaseUrl("front/images/bot_ico_flecha_vidrierasmall.jpg");?>" border="none"/></a>
					  </td>
					</tr>
				  </table>
				</td>
		      </tr>
            </tbody>
          </table>
	    </form>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
      </div>
      <div id="contenido-bot"></div>
    </div>
    <? require("../menuess/footer.php")?>
    <iframe style="display:none" name="iContacto" id="iContacto"></iframe> 
    
    <script type="text/javascript">
    	$("#txtMotivo").change(function(){
        	if ($("#txtMotivo").val()==104 || $("#txtMotivo").val()==105 || $("#txtMotivo").val()==31 || $("#txtMotivo").val()==41) {
        		$("#campcombo").show();
        		$("#isCampaniaSel").val("true");
        		if ($("#txtMotivo").val()==31) {
        			$("#sumC").html("Otra Marca");
        		}
        		else {
        			$("#sumC").html("Seleccione una Marca");
        		}
        	}
        	else {
        		$("#campcombo").hide();
        		$("#isCampaniaSel").val("false");
        	}
		});
    </script>
  </body>
</html>
<?php 
    
function array_sort($array, $on, $order=SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}
?>
