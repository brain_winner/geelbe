<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    $arrayHTML = Aplicacion::getIncludes("onlyget", "htmlTerminos");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="tipo_contenido"  content="text/html;" http-equiv="content-type" charset="utf-8">
<title>Términos y Condiciones y Política de Privacidad<!--?=Aplicacion::getParametros("info", "nombre");?!--> </title>
<link href="<?=UrlResolver::getBaseUrl("front/geelbe_new.css", true);?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
  <? require("../menuess/cabecera.php") ?>
  <div id="container">
  <div id="contenido-top">
			<br /><h1>Términos y Condiciones y Política de Privacidad</h1>
  </div>
  <div id="contenido">

<strong>ALCANCE DE LA POLITICA</strong><br/>

La presente política se refiere exclusivamente a las bases de datos y a los datos regulados por la Ley 1581 de 2012.

<br/><br/><strong>DATOS DE CONTACTO DEL RESPONSABLE O ENCARGADO DEL DATO - ÁREA RESPONSABLE DE LA ATENCIÓN DE CONSULTAS, PETICIONES Y RECLAMOS.</strong><br/>

Geelbe Colombia S.A.S. (“Geelbe”), identificada con el NIT 900 343 822 4, ubicada en la Av. Kr. 9 # 103-13 de la ciudad de Bogotá. Podrán contactarnos al siguiente correo electrónico: info@corp.geelbe.com o al número telefónico (571) 742 5913 en Bogotá D.C. En adelante el Responsable del Tratamiento.

<br/><br/><strong>TRATAMIENTO DE LOS DATOS Y  FINALIDAD </strong><br/>

El Responsable del Tratamiento, previa autorización de los titulares de la información (en caso que esta sea requerida), pueden obtener información directamente de personas naturales, clientes o usuarios, de diferentes fuentes (comercios vinculados, entidades financieras, entidades participantes del sistema de pago, proveedores de servicios de telecomunicaciones, y fuentes propias entre otros). También es posible que tengamos una relación directa con personas naturales (empleados, proveedores, etc.), que podrán optar por brindarnos información personal en conexión con esa relación. Obtenemos información de empresas que utilizan productos o servicios del Responsable del Tratamiento, y todas sus sociedades filiales, sistemas de información disponibles públicamente, operadores de bases de datos y servicios de información comercial. Además, podemos recolectar información directamente dada por personas naturales cuando existe una relación contractual, comercial, laboral o de prestación de servicios con nosotros. Los tipos de datos personales que recolectamos son datos públicos, semi privados, y no serán recolectados datos sensibles o de menores de edad, a menos que se tenga autorización expresa y una finalidad válida en este sentido.<br/>

Los usuarios, clientes, trabajadores y proveedores autorizan expresamente al Responsable del Tratamiento a confirmar la información personal suministrada acudiendo a entidades públicas, compañías especializadas o centrales de riesgo, a sus contactos, o al empleador, así como a sus referencias personales, bancarias o laborales, entre otros. Esta información será tratada por el Responsable del Tratamiento en forma confidencial.<br/>

Los datos personales proporcionados al Responsable del Tratamiento serán recolectados, almacenados, depurados, usados, analizados, circulados, actualizados y cruzados con información propia o de terceros, datos de contacto, información sobre preferencias de consumo, comportamiento en los canales de contacto y demás información recolectada, con la finalidad de realizar actividades de promoción, publicidad y/o mercadeo de nuestros productos y servicios, productos y servicios de nuestras matrices, subordinadas y vinculadas y/o aliados comerciales, así como para la facturación, programación, soporte técnico, inteligencia de mercados, mejoramiento del servicio, verificaciones y consultas, control, comportamiento, hábito y habilitación de medios de pago, prevención de fraude, y cualquier otra relacionada con nuestros productos y servicios, actuales y futuros, para el cumplimiento de las obligaciones contractuales y de nuestro objeto social y demás finalidades estrechamente asociadas y necesarias para cumplir los fines aquí descritos.

<br/><br/><strong>DERECHOS DE LOS TITULARES</strong><br/>

El titular de los datos personales tiene los siguientes derechos frente al Responsable del Tratamiento:

1. Conocer, actualizar y rectificar sus datos personales.<br/>
2. Solicitar prueba de la autorización otorgada.<br/>
3. Ser informado previa solicitud, respecto del uso que le ha dado a sus datos personales.<br/>
4. Presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a lo dispuesto en la Ley 1581 de 2012, una vez haya agotado el trámite de consulta o reclamo ante el Responsable del Tratamiento.<br/>
5. Revocar la autorización y/o solicitar la supresión del dato cuando en el tratamiento no se respeten los principios, derechos y garantías constitucionales y legales.<br/>
6. Acceder en forma gratuita a sus datos personales que hayan sido objeto de tratamiento.

<br/><br/><strong>PROCEDIMIENTO PARA EJERCER LOS DERECHOS AL HABEAS DATA.</strong><br/>

Los titulares de la información podrán ejercer los derechos a conocer, actualizar, rectificar y revocar la autorización mediante el envío de una comunicación a la dirección y al área indicada en al presente política. Esta comunicación deberá contener como mínimo lo siguiente:
<br/>
1. El nombre, domicilio del titular y medio de contacto para recibir la respuesta como teléfono, correo electrónico, dirección de residencia.<br/>
2. Los documentos que acrediten la identidad o la representación de su representado.<br/>
3. La descripción clara y precisa de los datos personales respecto de los cuales el titular busca ejercer alguno de los derechos.<br/>
4. En caso dado otros elementos o documentos que faciliten la localización de los datos personales.<br/>

Una vez recibida la comunicación por parte de los Responsables del Tratamiento, estos procederán a dar respuesta dentro de los términos establecidos en la regulación aplicable.

<br/><br/><strong>MODIFICACIÓN Y/O ACTUALIZACIÓN DE LA POLÍTICA DE PROTECCIÓN DE DATOS Y MANEJO DE INFORMACIÓN</strong><br/>

Cualquier cambio sustancial en las políticas de tratamiento, se comunicará de forma oportuna a los titulares de los datos a través de los medios habituales de contacto y/o a través de la página de internet www.geelbe.com.

<br/><br/><strong>VIGENCIA DE LAS POLÍTICAS DE TRATAMIENTO DE INFORMACIÓN PERSONAL</strong><br/>

Las presentes políticas rigen a partir del 25 de julio de 2013.

Por regla general, el término de las autorizaciones sobre el uso de los datos personales por los clientes y/o usuarios se entiende por el término de la vinculación de las partes y durante el ejercicio del objeto social de la compañía.


</div>
<br/>

<div id="contenido-bot" style="padding-bottom:45px; margin-top:-16px!important;"></div>
</div>
<? require("../menuess/footer.php")?>
</body>
</html>