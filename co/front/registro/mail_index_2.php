<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::logoutUser();
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("registro"));
    $arrayHTML = Aplicacion::getIncludes("onlyget", "htmlTerminos"); 
    $postURL = Aplicacion::getIncludes("post", "registro");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<? Includes::Scripts() ?>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
	<link href="<?=UrlResolver::getCssBaseUrl("front/landing/css/glamout.css");?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getCssBaseUrl("front/registro/newsignup.css");?>" rel="stylesheet" type="text/css" />
	<script src="<?=UrlResolver::getJsBaseUrl("front/registro/js/js.js");?>" type="text/javascript"></script>

</head>
<body> 

	<? // pego directamente el codigo de la cabecera ya que esto es un testing. require("../menuess/cabecera_2.php") ?>
	
	<!-- empieza cabecera -->
	<div class="header">
		<div class="menu">
			<ul>
				<li class="last"><a class="fiveth" href="/co/front/contacto"></a></li>
				<li><a class="fourth" href="/co/front/acerca/?act=1"></a></li>
				<li><a class="third" href="/blog"></a></li>
				<li><a class="second" href="/co/front/registro"></a></li>
				<li><a class="first" href="/co/front/login"></a></li>
			</ul>
		</div>
		<a href="/" class="logo"></a>
	</div>
	<!-- termina cabecera -->

	<div class="contenedor">
		<h3>Registro de nuevos miembros</h3>
		<div class="column_420">
			<h1>&Uacute;nete a Geelbe</h1>
			<h2>Completa el formulario para aceptar la invitaci&oacute;n de <em><strong>%nombre padrino% %apellido padrino%</strong></em> y obtener tu membres&iacute;a. Una vez que seas miembro podr&aacute;s acceder a ventas de prestigiosas marcas con descuentos que llegan al 70%.</h2>
	
			<form  method="POST" name="frmRegistro" id="frmRegistro" target="iRegistro" action="../../../<?=Aplicacion::getDirLocal().$postURL["registro"]?>" onSubmit="ValidarForm(this);">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
			<tbody>
			<tr>
			<td width="699" bgcolor="#FFFFFF" scope="col"><h6>Nombre</h6></td>
			
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" scope="col"><input class="simpletb" size="30" name="txtNombre" /></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" scope="col"><h6>Apellido</h6></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" scope="col"><input class="simpletb" size="30" name="txtApellido" /></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" scope="col"><h6>Sexo</h6></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" valign="top" scope="col"><label><input class="radio" id="cbxIdTratamiento3" type="radio" value="1" name="cbxIdTratamiento" checked="checked" />
			Hombre</label>
			<label><input class="radio" id="cbxIdTratamiento" type="radio" value="2" name="cbxIdTratamiento" />
			Mujer</label></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" valign="top" scope="col"><h6>Fecha de Nacimiento</h6></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" valign="top" scope="col"><table style="margin:0;" width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td width="14%"><select class="select_1_s_2" name="cmb_nacimientoDia">
			<option value="0" >D�a</option>
			<? for ($i=1; $i<=31; $i++)
											echo ("<option value=".$i." >".$i."</option>")
									?>
			</select></td>
			<td width="24%"><select class="select_2_s_2" name="cmb_nacimientoMes">
			<option value="0" >Mes</option>
			<option value="1" >Enero</option>
			<option value="2" >Febrero</option>
			<option value="3" >Marzo</option>
			<option value="4" >Abril</option>
			<option value="5" >Mayo</option>
			<option value="6" >Junio</option>
			<option value="7" >Julio</option>
			<option value="8" >Agosto</option>
			<option value="9" >Septiembre</option>
			<option value="10" >Octubre</option>
			<option value="11" >Noviembre</option>
			<option value="12" >Diciembre</option>
			</select></td>
			<td width="62%"><select class="select_1_s_2" name="cmb_nacimientoAnio">
			<option value="0" >A�o</option>
			<? for ($i=(date("Y")-99); $i<=date("Y"); $i++)
										 echo ("<option value=".$i." >".$i."</option>")
									 ?>
			</select></td>
			</tr>
			</table></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" 
											height="5" align="left" scope="col"><h6>Email</h6></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td 
											height="5" align="left" scope="col"><?
						 if ($_POST["emailRef"])
						 {
							$emailRef = $_POST["emailRef"];
						 }
						 else
						 {
							$emailRef = "";
						 }
						 ?> <input size="30" class="simpletb" name="txtEmail" value="<?=$emailRef?> %CORREO USER%" /></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" 
											height="5" align="left" scope="col"><span class="ht">Geelbe utiliza el e-mail como medio de comunicaci&oacute;n para presentar propuestas de venta a los socios del club privado de compras. Para informaci&oacute;n sobre nuestras estrictas reglas de protecci&oacute;n de informaci&oacute;n accede a las pol&iacute;ticas de privacidad.</span></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" 
											height="10" align="left" scope="col"><h6>Contrase&ntilde;a</h6></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" 
											height="5" align="left" scope="col"><input class="simpletb" type="password" size="15" name="txtClave" /></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td 
											height="5" align="left" scope="col"><span class="ht">M&iacute;nimo 6 car&aacute;cteres</span></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" 
											height="10" align="left" scope="col"><h6>Confirmar Contrase&ntilde;a</h6></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" height="5" scope="col"><input class="simpletb" type="password" size="15" name="txtConfClave" /></td>
			</tr>
			<!-- Borrar if necessary
			<tr bgcolor="#FFFFFF">
			<td height="5" scope="col"><h6>C&oacute;digo de Invitaci&oacute;n</h6></td>
			</tr>
			
			<tr bgcolor="#FFFFFF">
			<td height="5" scope="col"><? if ($_POST["codact"])
						 {                      
					  ?>
					   <input name="txtPadrino" class="simpletb" type="text" size="30" readonly="readonly" value="<?=$_POST["codact"]?>" />
					  <? 
						 }
						 else
						 {
					  ?>
					  <input name="txtPadrino" class="simpletb" type="text" size="30" />
					  <? } ?></td>
			</tr>
			
			<tr bgcolor="#FFFFFF">
			<td height="5" scope="col"><span class="ht"><strong>Para registrarse en Geelbe es necesario tener un C�digo de Invitaci�n, solic�talo a tus amigos!</strong><br />
			Ingresa el c�digo de invitaci�n que recibiste en el email de invitaci�n o que puedes haber le�do en los medios relacionados a Geelbe.</span></td>
			</tr>-->
			<tr bgcolor="#FFFFFF">
			<td width="699" height="5" scope="col"><label class="terms">
			<input name="acepto2" type="checkbox" class="radio" id="acepto2" value="acepto" />
			<span>Acepto los <a href="<?=UrlResolver::getBaseUrl("front/terminos/");?>" target="_blank">T&eacute;rminos y Condiciones</a></span></label></td>
			</tr>
			<tr bgcolor="#FFFFFF">
			<td width="699" height="10" scope="col">&nbsp;</td>
			</tr>
			<!-- Captcha -->
			<?/* Deshabilito el catcha.
							<tr>
								<td bgcolor="#F0F0F0" colspan="3">Ingresa los caracteres en rojo respetando may&uacute;sculas y min&uacute;sculas:</td>
							</tr>
							<tr>
								<td bgcolor="#F0F0F0" colspan="3">
									<iframe src="<?=Aplicacion::getRootUrl()?>logica/framework/class/imgcaptcha.php" id="frmCaptcha" name="frmCaptcha" scrolling="no" frameborder="0" height="100">
									</iframe>
									
									<a href="javascript:refrescar_captcha();" alt="Recargar imagen" style="position:relative;left:-115px;top:-15px;"><img alt="Recargar imagen" src="/co/front/images/refrescar-imagen.gif" border="0" /></a>
									
									<br />&nbsp;&nbsp;&nbsp;<input type="text" id="txtCaptcha" name="txtCaptcha" size="23" maxlength="6" />
								</td>
							 </tr>
							  <tr>
								<td height="23" colspan="3" background="../images/gris-both.gif" scope="col"></td>
							</tr>
							*/?>
			<!-- Fin captcha -->
			<tr align="left">
			<td colspan="3" scope="col"><a href="javascript:document.forms['frmRegistro'].onsubmit();"><img src="<?=UrlResolver::getImgBaseUrl("images/signup_continuar.jpg");?>" alt="Registrase como socio de Geelbe" width="178" height="52" border="0" /></a></td>
			</tr>
			</tbody>
			</table>
		</form>
			
		</div>
		<div class="column_330">
			<div class="compromisos">
				<div class="title">Compromisos Geelbe</div>
				<div class="item1">
					<span>Art&iacute;culos 100% originales</span>
					Todos los productos ofrecidos a trav&eacute;s de Geelbe son originales y cuentan con el respaldo de su fabricante. <a href="<?=UrlResolver::getBaseUrl("front/compromisos/");?>">M&aacute;s informaci�n</a>.
				</div>
				<div class="item2">
					<span>Mayores descuentos</span>
					Comprando con Geelbe, obten&eacute; descuentos que alcanzan hasta el 70% con respecto al mercado tradicional. <a href="<?=UrlResolver::getBaseUrl("front/compromisos/");?>">M&aacute;s informaci�n</a>.
				</div>
			<div class="item3">
					<span>Primeras Marcas</span>
					Geelbe trabaja directamente con marcas de primera l&iacute;nea para ofrecer a sus miembros oportunidades exclusivas. <a href="<?=UrlResolver::getBaseUrl("front/compromisos/");?>">M&aacute;s informaci�n</a>.
				</div>
<a href="<?=UrlResolver::getBaseUrl("front/compromisos/");?>"><img src="<?=UrlResolver::getImgBaseUrl("images/signup_knowmore.jpg");?>" alt="Ver m&aacute;s compromisos" width="270" height="71" border="0" class="knowmore" /></a>
			</div>
		</div>
		<div class="clearboth"></div>
	</div>
	<? // pego el nuevo footer, no se incluye      require("../menuess/footer_2.php")?>
	
	<div id="nfooter">
		<div class="links">
			<span>Copyright &copy; <?=date("Y");?> Geelbe, todos los derechos reservados</span>
			<a href="/co/front/terminos"> T�rminos y Condiciones</a> | 
			<a href="/co/front/contacto"> Contacto</a>	
		</div>
	</div>

	<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	
	</script>
	<script type="text/javascript">
	var pageTracker = _gat._getTracker("UA-6895743-8");
	pageTracker._initData();
	pageTracker._trackPageview();
	</script>

	<iframe id="iRegistro" name="iRegistro" style="display:none"></iframe>

</body>
</html>
