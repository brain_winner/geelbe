var lastContactId = 0;
var ajaxLoader;
var emailAddresses = new Array();
var paymentPerContact = 20;
var invitesSent = false;
var godfatherChoosen = false;

function validateFields() {
	validated = true;
	$('#formValidators1').hide();
	$('#formValidators2').hide();
	$('#formValidators1field1').hide();
	$('#formValidators1field2').hide();
	if (!$('#nameFrom').val()) { $('#formValidators1').show(); $('#formValidators1field1').show(); validated = false; }
	if (!$('#emailFrom').val()) { $('#formValidators1').show(); $('#formValidators1field2').show(); validated = false; }
	var paramsString = "";
	var i = 0;
	$('input:checkbox:checked').each(function() {
		if (this.name.substr(0,21) == "sendEmailContactCheck") {
			fieldNumber = this.name.substr(21);
			paramsString += "&sendEmailContactName"+i+"="+$('#sendEmailContactName'+fieldNumber).val();
			paramsString += "&sendEmailContactEmail"+i+"="+$('#sendEmailContactEmail'+fieldNumber).val();
			i++;
		};
	});
	if (i == 0) { $('#formValidators2').show(); validated = false; }
	return validated;
}
function appendOptionLast(text,value,checked,holder,radio) {
	for (var all in emailAddresses) {
		if (emailAddresses[all] == value) var exists = true;
	}
	if (!exists) emailAddresses.push(value);
	//else return false;
	
	if (lastContactId == 0) {
		//$('#contactsNo').remove();
		$('#grabContactsFindActions').slideDown();
	}
	var tr = $('<tr></tr>').appendTo(holder).addClass(lastContactId%2 ? 'even' : 'odd');
	var td = $('<td></td>').appendTo(tr);
	if (radio) {
		$('<input type="radio"/>').appendTo(td).attr({
			name:'sendEmailContactRadio',
			value:value,
			id:'sendEmailContactCheck'+lastContactId,
			rel:'grabContactsContact',
			checked:(lastContactId > 0 ? false : true)
		}).change(selectGodfather).css({backgroundColor:'transparent',borderWidth:'0'}).parent().parent().addClass(lastContactId == 0 ? 'selectedRow' : '');
	} else {
		$('<input type="checkbox"/>').appendTo(td).attr({
			name:'sendEmailContactCheck'+lastContactId,
			id:'sendEmailContactCheck'+lastContactId,
			rel:'grabContactsContact',
			checked:checked
		}).click(countChecked).css({backgroundColor:'transparent',borderWidth:'0'}).parent().parent().addClass(checked ? 'selectedRow' : '');
	}
	$('<input type="hidden"/>').appendTo(td).attr({
		value:value,
		name:'sendEmailContactName'+lastContactId,
		id:'sendEmailContactName'+lastContactId
	});
	var td = $('<td></td>').appendTo(tr);
	$('<span>'+text+'</span>').appendTo(td);
	var td = $('<td></td>').appendTo(tr);
	$('<span>&lt;'+value+'&gt;</span>').appendTo(td);
	lastContactId++;
	$('#grabContactsTotalCount').text(emailAddresses.length);
	$('#grabContactsPotentialEarn').text('$'+(emailAddresses.length*paymentPerContact));
}
function selectGodfather() {
	$('#txtPadrino').val($(this).val());
	$('.selectedRow').removeClass('selectedRow');
	if ($(this).attr('checked')) $(this).parent().parent().addClass('selectedRow');
}
function countChecked() {
	var count = 0;
	
	if ($(this).attr('checked')) $(this).parent().parent().addClass('selectedRow');
	else $(this).parent().parent().removeClass('selectedRow');
	
	$('#grabContactsList').find('input:checked').each(function() {
		if ($(this).attr('rel') == 'grabContactsContact') count++;
	});
	$('#grabContactsSelectedCount').text(count);
	$('#grabContactsSelectedEarn').text('$'+(count*paymentPerContact));
}
$(document).ready(function(){
	$('#sendEmail').submit(function() {
		paramsString += "&count="+i;
		paramsString += "&fromName="+$('#nameFrom').val();
		paramsString += "&fromEmail="+$('#emailFrom').val();
		paramsString += "&messageId="+$('#messageId').val();
		$('#sendMailWorkingBg').show();
		$('#sendMailWorking').show();
		$('#provider_box').hide();
		$.ajax({
			url: 'sendEmail.php',
			data: paramsString,
			type: 'post',
			success: function(j) {
				$('#sendMailWorking').hide();
				$('#sentMail').show();
				j = eval("["+j+"]")[0];
				if (j.status == "true") {
					response = "<h2>Tu mensaje fue enviado</h2>";
				} else {
					response = "<h3>Error. Intentalo de nuevo</h3>";
				}
				$('#sentMail').html("<div style=\"margin-top:210px; margin-left:220px; width:315px;\"><div class=\"emotisTop\"></div><div class=\"emotisBg\" style=\"text-align:center;\">"+response+"</div><div class=\"emotisBottom\"></div></div>");
			}
		});
		return false;
	});

	function importContacts() {
		$('#contactsNo').text('');
		var emailBox = $('#grabContactsTxtUser').val();
		var passwordBox = $('#grabContactsTxtPass').val();
		var providerBox = $('#grabContactsSelectProvider1:checked,#grabContactsSelectProvider2:checked,#grabContactsSelectProvider3:checked,').val();
		var paramsString = "";
		paramsString += "&email_box="+emailBox;
		paramsString += "&password_box="+passwordBox;
		paramsString += "&provider_box="+providerBox;
		enableGrabContacts(false);
		ajaxLoader = $.ajax({
			url: fullPath+'lib/invite/importContacts.php',
			data: paramsString,
			type: 'post',
			success: function (j) {
				//alert("imp "+j);
				enableGrabContacts(true);
				j = eval("["+j+"]");
				if (j.length) j = j[0];
				if (j.status == "true") {
					j.contacts.sort(function(a, b) {
						var x = a.name.toLowerCase();
						var y = b.name.toLowerCase();
						return ((x < y) ? -1 : ((x > y) ? 1 : 0));
					});
					var table = $('<table></table>').prependTo('#grabContactsList').attr('cellspacing','0');
					var tr = $('<tr></tr>').appendTo(table).addClass('grabContactsTableHeader');
					$('<td colspan="2"></td>').appendTo(tr).text('contactos importados de '+emailBox+' ('+providerBox+')');
					if (!j.contacts.length) $('#contactsNo').text('No se pudo importar ning�n contacto. Por favor, verific� los datos e intentalo nuevamente o prob� con otra cuenta.');
					else $('#contactsNo').text('');
					if (!lastContactId) doSlide = true;
					else doSlide = false;
					for (all in j.contacts) {
						appendOptionLast(j.contacts[all].name,j.contacts[all].email,true,table);
					}
					countChecked();
					if (doSlide) {
						$('#grabContactsList').slideDown();
						$('#grabContactsCountBottom').slideDown();
					}
				} else {
					if (j.error) {
						switch (j.error) {
							case 'login': $('#contactsNo').text('Error al loguear a tu cuenta. Cheque� los datos ingresados e intentalo nuevamente'); break;
							case 'contacts': $('#contactsNo').text('Error al obtener contactos. Cheque� los datos ingresados e intentalo nuevamente'); break;
							case 'provider': $('#contactsNo').text('Error. Verific� el proveedor seleccionado.'); break;
						}
					} else {
						$('#contactsNo').text('Error. Cheque� los datos ingresados e intentalo nuevamente');
					}
				};
			}
		});
		return false;
	}
	
	function findContacts() {
		$('#contactsNo').text('');
		var emailBox = $('#grabContactsTxtUser').val();
		var passwordBox = $('#grabContactsTxtPass').val();
		var providerBox = $('#grabContactsSelectProvider1:checked,#grabContactsSelectProvider2:checked,#grabContactsSelectProvider3:checked,').val();
		var paramsString = "";
		paramsString += "&email_box="+emailBox;
		paramsString += "&password_box="+passwordBox;
		paramsString += "&provider_box="+providerBox;
		enableGrabContacts(false);
		ajaxLoader = $.ajax({
			url: fullPath+'lib/invite/importContacts.php',
			data: paramsString+'&reg=true',
			type: 'post',
			success: function (j) {
				//alert("find "+j);
				enableGrabContacts(true);
				j = eval("["+j+"]");
				if (j.length) j = j[0];
				if (j.status == "true") {
					j.contacts.sort(function(a, b) {
						var x = a.name.toLowerCase();
						var y = b.name.toLowerCase();
						return ((x < y) ? -1 : ((x > y) ? 1 : 0));
					});
					if (!$('#findTableHeader').length) {
						var table = $('<table></table>').prependTo('#grabContactsList').attr({cellspacing:'0',id:'findTableHeader'});
						var tr = $('<tr></tr>').appendTo(table).addClass('grabContactsTableHeader');
						$('<td colspan="2"></td>').appendTo(tr).text('Estos amigos ya son parte de Geelbe. Elige a uno como tu "padrino" y continua');
					} else {
						var table = $('#findTableHeader');
					}
					if (!j.contacts.length) $('#contactsNo').html('Ninguno de tus contactos se encuentra registrado en Geelbe.<br/>Te invitamos a que visites nuestros gurpos en las m�s populares redes sociales. All� podr�s encontrar varios codigos de invitaci�n:<br/><a href="http://www.facebook.com/home.php?#/pages/Geelbe/33206272604" style="color:#0066cc;" target="_blank"><img src="img/facebook.jpg" alt=""/> Facebook</a> <a href="http://www.sonico.com/geelbe" style="color:#0066cc;" target="_blank"><img src="img/sonico.jpg" alt=""/> Sonico</a>');
					else $('#contactsNo').text('Seleccion� un contacto de la lista para que sea tu padrino.');
					if (!lastContactId && j.contacts.length) doSlide = true;
					else doSlide = false;
					for (all in j.contacts) {
						appendOptionLast(j.contacts[all].name,j.contacts[all].email,true,table,true);
					}
					countChecked();
					if (doSlide) {
						$('#grabContactsList').slideDown();
						$('#grabContactsCountBottom').slideDown();
						$('#simplemodal-container').animate({marginTop:'-160px'});
					}
				} else {
					if (j.error) {
						switch (j.error) {
							case 'login': $('#contactsNo').text('Error al loguear a tu cuenta. Cheque� los datos ingresados e intentalo nuevamente'); break;
							case 'contacts': $('#contactsNo').text('Error al obtener contactos. Cheque� los datos ingresados e intentalo nuevamente'); break;
							case 'provider': $('#contactsNo').text('Error. Verific� el proveedor seleccionado.'); break;
						}
					} else {
						$('#contactsNo').text('Error. Cheque� los datos ingresados e intentalo nuevamente');
					}
				};
			}
		});
		return false;
	}
	
	function enableGrabContacts(val) {
		if (val) {
			$('#grabContactsLoading').hide();
			$('#contactsNo').show();
			$('#grabContactsSubmit').removeAttr('disabled');
			$('#grabContactsSubmit').attr({src:'img/grabContactsSubmit.jpg'});
		} else {
			$('#grabContactsLoading').show();
			$('#contactsNo').hide();
			$('#grabContactsSubmit').attr({disabled:true,src:'img/grabContactsSearching.jpg'});
		}
	}
	$('#grabContactsFindForm').submit(findContacts);
	$('#grabContactsFindSubmit').click(findContacts);
	$('#grabContactsForm').submit(importContacts);
	$('#grabContactsSubmit').click(importContacts);
	$('#grabContactsAbort').click(function() {
		ajaxLoader.abort();
		enableGrabContacts(true);
	});
	$('#grabContactsSubmit').removeAttr('disabled');
	
	//enviar invitaciones
	$('#grabContactsSend').click(function() {
		var contacts = new Array();
		$('#grabContactsList').find('input:checked').each(function() {
			if ($(this).attr('rel') == 'grabContactsContact') {
				var checkName = $(this).attr('name');
				var valueName = checkName.replace('sendEmailContactCheck','sendEmailContactName');
				var value = $('#'+valueName).val();
				contacts.push('"'+value+'"');
			}
		});
		$.ajax({
			url:'invites_queue.php',
			type:'post',
			data:'data={"contacts":['+contacts.join(',')+']}&userEmail='+$('#sendInviteFromEmail').val(),
			success:function(data) {
				data = eval('['+data+']')[0];
				if (data.status == 'success') {
					$('#grabContactsHolder').slideUp();
					$('#grabContactsSent').slideDown();
					$('#smallPrint').fadeOut("normal");
				}
			}
		});
		invitesSent = true;
	});
	
	$('#grabContactsManualForm').submit(function(){
		if ($('#grabContactsTxtManual').val()) {
			var val = $('#grabContactsTxtManual').val();
			if (!lastContactId) doSlide = true;
			if ($('#grabContactsManualList').length < 1) {
				var table = $('<table></table>').prependTo('#grabContactsList').attr({cellspacing:'0',id:'grabContactsManualList'});
				var tr = $('<tr></tr>').appendTo(table).addClass('grabContactsTableHeader');
				$('<td colspan="2"></td>').appendTo(tr).text('contactos ingresados manualmente');
			} else {
				var table = $('#grabContactsManualList');
			}
			appendOptionLast(val,val,true,table);
			$('#grabContactsTxtManual').val("")
			if (doSlide) {
				$('#grabContactsList').slideDown();
				$('#grabContactsCountBottom').slideDown();
			}
			countChecked();
		}
		return false;
	});
	
	$('#grabContactsTxtUser').change(function() {
		$('#grabContactsTxtPass').val("");
		var email = $(this).val();
		var emailParts = email.split("@");
		var emailDomain = emailParts[emailParts.length-1];
		$("#grabContactsSelectProvider1,#grabContactsSelectProvider2,#grabContactsSelectProvider3").removeAttr("checked");
		switch(emailDomain.toLowerCase()) {
			case 'gmail.com': $("#grabContactsSelectProvider2").attr("checked","checked"); break;
			case 'hotmail.com': case 'live.com': case 'msn.com': $("#grabContactsSelectProvider1").attr("checked","checked"); break;
			case 'yahoo.com': case 'yahoo.com.ar': $("#grabContactsSelectProvider3").attr("checked","checked"); break;
		}
	});
	
	$('#grabContactsFindStart').click(function() {
		$('#grabContactsHolder').modal({
			onShow:function() {
				$('#simplemodal-container').css({opacity:0}).animate({opacity:'1'});
				$('#simplemodal-overlay').css({opacity:0}).animate({opacity:'.7'});
			},
			onClose:function() {
				$('#simplemodal-container').animate({opacity:'0'});
				$('#simplemodal-overlay').animate({opacity:'0'},function(){
					$.modal.close();
				});
			}
		});
		return false;
	});
	$('#grabContactsFindCancel').click(function() {
		$('#txtPadrino').val('');
		$('#simplemodal-container').animate({opacity:'0'});
		$('#simplemodal-overlay').animate({opacity:'0'},function(){
			$.modal.close();
		});
	});
	$('#grabContactsFindSend').click(function() {
		var selected = 0;
		var value;
		$('#grabContactsList').find('input:checked').each(function() {
			if ($(this).attr('rel') == 'grabContactsContact') {
				selected++;
				value = $(this).val();
			}
		});
		if (selected == 1 && value) {
			$('#txtPadrino').val(value);
			$('#grabContactsValidateGodfather').show();
			$('#contactsNo').text('').hide();
			$.ajax({
				url:'add_godfather.php',
				type:'post',
				data:'userId='+$('#IdUsuario').val()+'&godfather='+value,
				success:function(data){
					//alert(data);
					$('#grabContactsValidateGodfather').hide();
					$('#contactsNo').show();
					data = eval('['+data+']')[0];
					if (data.status == 'success') {
						godfatherChoosen = true;
						$('#simplemodal-container').animate({opacity:'0'});
						$('#simplemodal-overlay').animate({opacity:'0'},function(){
							$.modal.close();
							$('#frmRegistro').submit();
						});
					} else {
						$('#contactsNo').text('Error. '+data.error).css({color:'#d60105'});
						alert(data.sql);
					}
				}
			});
		} else if (selected == 0) {
			if (lastContactId == 0) $('#contactsNo').text('Primero ten�s que buscar un contacto registrado en Geelbe para que se tu padrino.');
			else $('#contactsNo').text('Primero ten�s que seleccionar un contacto de la lista para que se tu padrino.');
			$('#contactsNo').css({color:'#d60105'});
		} else {
			$('#contactsNo').text('Error. Cheque� que hayas seleccionado un solo contacto e intentalo de nuevo.');
			$('#contactsNo').css({color:'#d60105'});
		}
	});
});
function checkInvites(frm) {
	if (!invitesSent) { 
		invitesSent = true;
		return confirm("A�n no invitaste a ning�n de tus amigos. Est�s seguro que dese�s continuar?");
	}
	ValidarPadrino(frm);
}