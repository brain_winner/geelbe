<?php
  ob_start();
  $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php";
  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/registro/formvalidator.php";
  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/registro.php";
  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/datamappers/dmregistro.php";
  Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
  
  
  if(isset($_POST['submitForm']) || isset($_POST['submitForm_x'])) {
    $validator = new FormValidator();
    $validator->addValidation("email","req","Por favor, ingrese su email.");
    $validator->addValidation("email","maxlen=255", "Su email supera el maximo permitido de 255 caracteres.");
    $validator->addValidation("email","email", "Su email no posee una direccion valida.");

    if(isset($_POST['autopassword'])) {
    	$_POST['password'] = RegistroController::randomPassword(10);
    	$_POST['confirmpass'] = $_POST['password'];
    }
    
	$validator->addValidation("password","req","Por favor, ingrese su contraseņa.");
	$validator->addValidation("password","maxlen=30","Su contraseņa supera el maximo permitido de 30 caracteres.");
	$validator->addValidation("password","minlen=6","Su contraseņa debera ser de al menos 6 caracteres.");

    $validator->addValidation("tos","shouldselchk=on","Debe aceptar los terminos y condiciones de Geelbe.");

    if($validator->ValidateForm()) {
	
	  if(isset($_SESSION['codact']) && empty($_POST['activationcode'])) {
	  	$_POST['activationcode'] = $_SESSION['codact'];
		$_GET['activationcode'] = $_SESSION['codact'];
		$_REQUEST['activationcode'] = $_SESSION['codact'];
	  }

      $registro = new RegistroController();
      $registro->registrarV2();
    }
    else {
      // Validation Errors
      $error_hash = $validator->GetErrors();
	  $errors = "<ul>";
      foreach($error_hash as $inpname => $inp_err) {
        $errors = $errors."<li>$inp_err</li>";
      }        
	  $errors = $errors."</ul>";
	  $data = "errors=true&errorValue=$errors";
	  $uri = $_SERVER["HTTP_REFERER"];
      header("Location: $uri?$data");
    }
	ob_flush();
  }
?>