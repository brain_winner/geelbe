<?
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");    
	Aplicacion::logoutUser();
	
	if(empty($_SESSION['IdRegistro'])){
		redirect(Aplicacion::getRootUrl()."/front/registro");
		exit();
	}
	
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("registro"));
    $arrayHTML = Aplicacion::getIncludes("onlyget", "htmlTerminos");
    $postURL = Aplicacion::getIncludes("post", "registro_paso2");
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("css/forms_g.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/registro/js/js.js");?>" type="text/javascript"></script>
<script type="text/javascript">var fullPath = '/<?=Aplicacion::getDirLocal();?>';</script>

<script src="<?=UrlResolver::getJsBaseUrl("front/registro/js/jquery.js");?>" type="text/javascript"></script>
<script src="<?=UrlResolver::getJsBaseUrl("front/registro/jquery.preloadImages.js");?>" type="text/javascript"></script>
<script src="<?=UrlResolver::getJsBaseUrl("front/registrojs/jquery.simplemodal.js");?>" type="text/javascript"></script>
<script src="<?=UrlResolver::getJsBaseUrl("front/registro/js/invite.js");?>" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$.preloadImages(['img/ajaxLoadingMedium.gif'],
									['img/grabContactsSearching.jpg'],
									['img/grabContactsSend.jpg']);
});
</script>
<link href="/<?=Aplicacion::getDirLocal(); ?>css/grabContacts.css" rel="stylesheet" type="text/css" />
<link href="/<?=Aplicacion::getDirLocal(); ?>css/modal.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 7]><link href="/<?=Aplicacion::getDirLocal(); ?>css/modal_ie.css" rel="stylesheet" type="text/css" /><![endif]-->

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
<?
require("../menuess/cabecera1.php");
$splitEmail = split("@",$_SESSION['inviteEmail']);
$emailDomain = $splitEmail[1];
switch ($emailDomain) {
	case 'gmail.com': $emailProvider = 'gmail'; break;
	case 'hotmail.com': case 'msn.com': case 'live.com': $emailProvider = 'hotmail'; break;
	case 'yahoo.com': case 'yahoo.com.ar': $emailProvider = 'yahoo'; break;
}
?>
<div id="container">
	<div id="contenido">
		<h1>Ingres&aacute; tu c&oacute;digo de invitaci&oacute;n para finalizar.</h1>
		<form  method="post" name="frmPadrino" id="frmRegistro" target="iRegistro" action="/<?=Aplicacion::getDirLocal().$postURL["registro_paso2"]?>" onSubmit="ValidarPadrino(this);">
		<label style="display:block;padding:10px 0 20px 0;">
		<input type="text" class="bigone" name="txtPadrino" id="txtPadrino" />
		</label>
		<br />
		<input type="hidden" name="urlVariables" value="" />
		<input type="hidden" name="IdUsuario" id="IdUsuario" value="<?=$_SESSION['IdRegistro'][0]['Id']?>" />
		<input type="hidden" name="hash" value="<?=generar_hash_md5($_SESSION['IdRegistro'][0]['Id'])?>" />
		
		<h2>
 Ahora, hac&eacute; clic en el siguiente bot&oacute;n para finalizar.</h2>
		<a onclick="javascript:godfatherChoosen=true;" href="javascript:document.forms['frmPadrino'].onsubmit();"><img src="<?=UrlResolver::getImgBaseUrl("front/landing/img/button_finish.jpg");?>" alt="Registrase como socio de Geelbe" width="188" height="55" border="0" /></a>
	</form>


<h4><br />
&iquest;No ten&eacute;s c&oacute;digo de invitaci&oacute;n?</h4>
<ul class="step2">
	<li>
		<h5>
			<strong>Busc� a tus amigos:</strong>
			Si alg�n contacto de tus libreta de direcciones o alg�n amigo de tus redes sociales ya es socio de Geelbe.com, puede ser tu padrino.<br/>
			<div id="grabContactsForActivation">
				<? include('find_friends.php'); ?>
			</div>
			<a href="#" id="grabContactsFindStart" class="blue">Hac� click ac� para buscarlos</a>
		</h5>
	</li>
	<li>
		<h5>
			<strong>Busc� en Redes Sociales:</strong> Te invitamos a que visites nuestros grupos en las m�s populares redes sociales:<br/>
			All� podr�s encontrar varios c�digos de invitaci�n para ingresar en el club privado de compras por internet m�s grande de Latinoam�rica!<br/>
			<img src="<?=UrlResolver::getImgBaseUrl("front/registro/img/facebook.jpg");?>" alt=""/> <a href="http://www.facebook.com/home.php?#/pages/Geelbe/33206272604" class="blue" target="_blank"> Geelbe en Facebook</a><br/>
			<img src="<?=UrlResolver::getImgBaseUrl("front/registro/img/sonico.jpg");?>" alt=""/> <a href="http://www.sonico.com/geelbe" class="blue" target="_blank">Geelbe en Sonico</a><br/>
			<img src="<?=UrlResolver::getImgBaseUrl("front/registro/img/google-favicon.ico");?>" alt="Buscar en Google"/> <a href="http://www.google.com.ar/search?hl=es&q=invitacion+geelbe&btnG=Buscar&meta=" class="blue" target="_blank"> Buscar invitaciones en Google</a>
		</h5>
	</li>
</ul>

	<!--h3>&nbsp;</h3!-->
	<!--h3>Por favor segu&iacute; las instrucciones en el email que te acabamos de enviar!</h3!-->
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br />
</div>
<div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
<iframe id="iRegistro" name="iRegistro" style="display:none">
</iframe>
</body>
</html>
