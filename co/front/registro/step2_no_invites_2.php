<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::logoutUser();
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("registro"));
    $arrayHTML = Aplicacion::getIncludes("onlyget", "htmlTerminos"); 
    $postURL = Aplicacion::getIncludes("post", "registro");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<? Includes::Scripts() ?>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
	<link href="<?=UrlResolver::getCssBaseUrl("css/forms_g.css");?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getCssBaseUrl("front/newsignup.css");?>" rel="stylesheet" type="text/css" />
	<script src="<?=UrlResolver::getJsBaseUrl("front/registro/js/js.js");?>" type="text/javascript"></script>

</head>
<body> 

	<? // pego directamente el codigo de la cabecera ya que esto es un testing. require("../menuess/cabecera_2.php") ?>
	
	<!-- empieza cabecera -->
	<div class="header">
		<div class="menu">
			<ul>
				<li class="last"><a class="fiveth" href="/co/front/contacto"></a></li>
				<li><a class="fourth" href="/co/front/acerca/?act=1"></a></li>
				<li><a class="third" href="/blog"></a></li>
				<li><a class="second" href="/co/front/registro"></a></li>
				<li><a class="first" href="/co/front/login"></a></li>
			</ul>
		</div>
		<a href="/" class="logo"></a>
	</div>
	<!-- termina cabecera -->

	<div class="contenedor">
		<h3>Registro de nuevos miembros</h3>
		<div class="column_full">
			<h1>Agust&iacute;n, ahora ingres&aacute; tu c&oacute;digo de invitaci&oacute;n</h1>
			<br />
			<input class="simpletb" size="30" name="txtNombre" />
			<br />
			<br />
		<h2>Ahora, hac&eacute; clic en el siguiente bot&oacute;n para finalizar</h2><br />
			
		<img src="<?=UrlResolver::getImgBaseUrl("front/images/signup_finalizar.jpg");?>" alt="Registrase como socio de Geelbe" width="178" height="52" border="0" />
					
			<br /><br /><br /><br />
			
			<h2><strong>&iquest;No ten&eacute;s c&oacute;digo de invitaci&oacute;n?</strong></h2>
			
			<ul class="donothave">
				<li>
					<span class="item"><u>Preguntale a tus amigos</u>: </strong> si alguno de tus amigos ya es socio de Geelbe.com, pod&eacute;s utilizar su e-mail como c&oacute;digo de invitaci&oacute;n.</span></li>
				<li>
					<span class="item"><u>Busca en blogs</u>: muchos de los blogs m&aacute;s interesantes ofrecen invitaciones para Geelbe. &iexcl;Busca en tus favoritos!</span>
				</li>
				<li>
					<span class="item"><u>Google&aacute;</u>: si buscas en Google encontrar&aacute;s diversos sitios haciendo referencia a Geelbe. &iexcl;Date ma&ntilde;a y encontrar&aacute;s invitaciones gratuitas!</span>
				</li>
			</ul>
			
		</div>
<div class="clearboth"></div>
	</div>
	<? // pego el nuevo footer, no se incluye      require("../menuess/footer_2.php")?>
	
	<div id="nfooter">
		<div class="links">
			<span>Copyright &copy; <?=date("Y");?> Geelbe, todos los derechos reservados</span>
			<a href="/co/front/terminos"> T�rminos y Condiciones</a> | 
			<a href="/co/front/contacto"> Contacto</a>	
		</div>
	</div>

	<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	
	</script>
	<script type="text/javascript">
	var pageTracker = _gat._getTracker("UA-6895743-8");
	pageTracker._initData();
	pageTracker._trackPageview();
	</script>

	<iframe id="iRegistro" name="iRegistro" style="display:none"></iframe>

</body>
</html>
