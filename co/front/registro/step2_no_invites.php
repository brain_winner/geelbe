<?
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");    
	Aplicacion::logoutUser();
	
	if(empty($_SESSION['IdRegistro'])){
		redirect(Aplicacion::getRootUrl()."/front/registro");
		exit();
	}
		
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("registro"));
    $arrayHTML = Aplicacion::getIncludes("onlyget", "htmlTerminos");
    $postURL = Aplicacion::getIncludes("post", "registro_paso2");
    $usuario = dmUsuario::getByIdUsuario($_SESSION['IdRegistro'][0]['Id']);
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("css/forms_g.css");?>"  rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/botones.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/registro/css/send_invites.css");?>" rel="stylesheet" type="text/css" />

<script src="<?=UrlResolver::getJsBaseUrl("front/registro/js/js.js");?>" type="text/javascript"></script>
<script type="text/javascript">var fullPath = '/<?=Aplicacion::getDirLocal();?>';</script>
<script src="<?=UrlResolver::getJsBaseUrl("front/registro/js/jquery.js");?>" type="text/javascript"></script>
<script src="<?=UrlResolver::getJsBaseUrl("front/registro/js/jquery.preloadImages.js");?>" type="text/javascript"></script>
<script src="<?=UrlResolver::getJsBaseUrl("front/registro/js/jquery.simplemodal.js");?>" type="text/javascript"></script>
<script src="<?=UrlResolver::getJsBaseUrl("front/registro/js/invite.js");?>" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$.preloadImages(['img/ajaxLoadingMedium.gif'],
									['img/grabContactsSearching.jpg'],
									['img/grabContactsSend.jpg']);
});
</script>
<link href="<?=UrlResolver::getCssBaseUrl("css/grabContacts.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("css/modal.css");?>" rel="stylesheet" type="text/css" />
<!--[if lt IE 7]><link href="/<?=Aplicacion::getDirLocal(); ?>css/modal_ie.css" rel="stylesheet" type="text/css" /><![endif]-->

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>

<body class="interior">
<?
require("../menuess/cabecera1.php");
$splitEmail = split("@",$_SESSION['inviteEmail']);
$emailDomain = $splitEmail[1];
switch ($emailDomain) {
	case 'gmail.com': $emailProvider = 'gmail'; break;
	case 'hotmail.com': case 'msn.com': case 'live.com': $emailProvider = 'hotmail'; break;
	case 'yahoo.com': case 'yahoo.com.ar': $emailProvider = 'yahoo'; break;
}
?>
	<div id="container">
		<div id="contenido">
			<div style=" overflow:auto;">
                <div id="proc-reg">Proceso De Registro</div>
                <div class="timeline-on"><p>Ingresa<br />tus datos</p></div>
                <div class="timeline-off" style="background-position:-85px 0;">
              <p>Confirma<br />tu email</p></div>
                <div class="timeline-off" style="background-position:-170px 0;"><p>Registro<br />exitoso</p></div>
            </div>
			<div id="Padrino">
				<div>
					<img src="img/ingresa-codact.jpg" />
					<form  method="post" name="frmPadrino" id="frmRegistro" target="iRegistro" action="/<?=Aplicacion::getDirLocal().$postURL["registro_paso2"]?>" onSubmit="ValidarPadrino(this);">
                        <div style="float:left; padding:0 10px 0 0;">
                            <label style="display:block;padding:8px 0 0;">
                                <!-- OJO cambi� la clase del input -->
                                <input type="text" class="inviteInput manualInvitation" name="txtPadrino" id="txtPadrino" />
                            </label>
                        </div>
                        <div style="float:left;">
                            <input type="hidden" name="urlVariables" value="" />
                            <input type="hidden" name="IdUsuario" id="IdUsuario" value="<?=$_SESSION['IdRegistro'][0]['Id']?>" />
                            <input type="hidden" name="hash" value="<?=generar_hash_md5($_SESSION['IdRegistro'][0]['Id'])?>" />
                            <table cellspacing="0" cellpadding="0" border="1px" class="btn btnRed izquierda normal M"> 
                                <tbody>
                                    <tr>
                                        <td>
                                            <a onclick="javascript:godfatherChoosen=true;" href="javascript:document.forms['frmPadrino'].onsubmit();"><strong>Continuar</strong></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
				</div>
			</div>

			<img href="javascript:;" src="http://img.static.geelbe.com/ar/front/referenciados/img/divisor.jpg" style="padding: 2px;">

			<div id="Huerfano">
				<span style="text-align: left; font-size: 16px; font-family:Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; color:#323296; font-weight:bold;">&iquest;No tienes c&oacute;digo de invitaci&oacute;n?</span>
		  <ul class="step2">
					<li>
						<p><strong>Busca un padrino entre tus contactos<br />de Hotmail, GMail o Yahoo.</strong> �Si alguno de ellos<br />ya es socio de Geelbe, podr�s elegirlo como padrino<br />y acceder a descuentos de hasta 70%!</p>
						<div id="grabContactsForActivation">
							  <? include('find_friends.php'); ?>
						</div>
                            <img style="padding:0 10px 0 0; cursor:pointer" src="img/bot-providerLive.jpg" alt="Live/Hotmail" onclick="$('#grabContactsFindStart').click()"/>
                            <img style="padding:0 10px 0 0; cursor:pointer;" src="img/bot-providerGmail.jpg" alt="Gmail" onclick="$('#grabContactsFindStart').click()"/>
                            <img style="padding:0 10px 0 0; cursor:pointer;" src="img/bot-providerYahoo.jpg" alt="Yahoo" onclick="$('#grabContactsFindStart').click()"/>
						<table cellspacing="0" cellpadding="0" border="1px" class="btn btnViolet izquierda chico M" style="margin:0.5em 0 0 0;;"> 
							<tbody>
								<tr>
									<td>
										<a href="#" id="grabContactsFindStart"><strong>Buscar contactos</strong></a>
									</td>
								</tr>
							</tbody>
						</table>
					</li>
					<li>
						<p><strong>Encuentra un padrino en internet</strong><br />
					  Tambi&eacute;n puedes buscar un padrino en nuestra p&aacute;gina de Facebook, en Google, o en Twitter donde muchos socios comparten sus invitaciones para que otras personas puedan aprovecharlas.<br/>
					  </p>
              <div class="redes">
                	<a href="http://www.facebook.com/Geelbe?v=wall" target="_blank"><img src="img/redes_fb.jpg" alt="Geelbe en Facebook"/></a>
                </div>
                <div class="redes">
                	<a href="http://www.google.com.ar/search?hl=es&q=invitacion+geelbe&btnG=Buscar&meta=" target="_blank"><img src="img/redes_goo.jpg" alt="Buscar invitaciones en Google"/></a></div>
                <div class="redes">
                	<a href="http://search.twitter.com/search?q=geelbe%2Bregistrate+near%3Acolombia+within%3A1000km" alt="Geelbe en Twitter" target="_blank"><img src="img/redes_tw.jpg"/></a>
                </div>
        </li>
    </ul>
    </div>
        <!--h3>&nbsp;</h3!-->
        <!--h3>Por favor segu&iacute; las instrucciones en el email que te acabamos de enviar!</h3!-->
	
    </div>
    <div id="contenido-bot"></div>
    </div>
<? require("../menuess/footer.php")?>
<iframe id="iRegistro" name="iRegistro" style="display:none">
</iframe>
</body>
</html>
