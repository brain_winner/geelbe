<?
$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");    
Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados"));
$output['sql'] = array();

if (!empty($_POST['userId']) && !empty($_POST['godfather'])) {

	$godfather = $_POST['godfather'];
	$godfatherExists = false;
	$userId = $_POST['userId'];
	$userExists = false;
	
	$invitesSql = new Conexion();
	$invitesSql->Abrir_Trans();
	$referenciadoSql = new dmReferenciadoH();
	$referenciadoObj = new clsReferenciadoH();
	
	//chequea que el padrino realmente exista y sea un usuario activo
	$invitesSql->setQuery("select IdUsuario from usuarios where NombreUsuario = '$godfather' and es_activa = '0'");
	$invitesQuery = $invitesSql->EjecutarQuery();
	if (mysql_num_rows($invitesQuery)) {
		$row = mysql_fetch_array($invitesQuery);
		$godfatherId = $row['IdUsuario'];
		$godfatherExists = true;
	}
	
	//chequea que el usuario realmente exista en la tabla 'usuarios'
	$invitesSql->setQuery("select NombreUsuario from usuarios where IdUsuario = '$userId'");
	$invitesQuery = $invitesSql->EjecutarQuery();
	if (mysql_num_rows($invitesQuery)) {
		$row = mysql_fetch_array($invitesQuery);
		$userEmail = htmlspecialchars(mysql_real_escape_string($row['NombreUsuario']), ENT_QUOTES);
		$userExists = true;
	}
	$invitesSql->setQuery("select Nombre,Apellido from datosusuariospersonales where IdUsuario = '$userId'");
	$invitesQuery = $invitesSql->EjecutarQuery();
	if (mysql_num_rows($invitesQuery)) {
		$row = mysql_fetch_array($invitesQuery);
		$userFirstName = $row['Nombre'];
		$userLastName = $row['Apellido'];
	}
	
	//si ambos existen almacena el padrino/ahijado en la db
	if ($godfatherExists && $userExists) {
        $oConexion = Conexion::nuevo();
        $oConexion->setQuery("SELECT SUM(Aplica1) AS Importe FROM reglasnegocio WHERE IdTipoRegla = 6;");
        $DataBono = $oConexion->DevolverQuery();
        $ValorBono = $DataBono[0]["Importe"];
		$godfather = htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($godfather)), ENT_QUOTES);
        
		$invitesSql->setQuery("update usuarios set Padrino = '$godfather' where IdUsuario = '$userId'");
		$invitesQuery = $invitesSql->EjecutarQuery();
		$invitesSql->setQuery("insert into ahijados (IdUsuario,Email,IdUsuarioReg, Valor) values ('$godfatherId','$userEmail','$userId', $ValorBono)");
		$invitesQuery = $invitesSql->EjecutarQuery();
		$invitesSql->Cerrar_Trans();
		
		$referenciadoObj->setNombreUsuario($godfather);
		$referenciadoObj->setNombre($userFirstName);
		$referenciadoObj->setApellido($userLastName);
		$referenciadoObj->setFecha(date('Y-m-d'));
		$referenciadoObj->setMailAhijado($userEmail);
		$referenciadoSql->Guardar($referenciadoObj);
		
		$output['status'] = 'success';
	} else {
		//estos casos no deber�an suceder
		if (!$godfatherExists) { $output['error'] .= 'No existe el padrino'; }
		if (!$userExists) { $output['error'] .= 'No existe el usuario'; }
		$output['status'] = 'error';
	}
} else {
	$output['status'] = 'error';
}
echo json_encode($output);
?>