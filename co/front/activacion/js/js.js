function ErrorMSJ(code)
{                                                                                                 
    oCortina.showError(Array(arrErrores["ACTIVACION"][code]), arrErrores["ACTIVACION"]["TITLE_ERROR"]);
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(document.getElementById("frmDaU[txtNombre]").value.length == 0)
        {
            arrError.push(arrErrores["MICUENTA"]["NOMBRE"]);
        }
        if(document.getElementById("frmDaU[txtApellido]").value.length == 0)
        {
            arrError.push(arrErrores["MICUENTA"]["APELLIDO"]);
        }
        if(document.getElementById("frmU[txtEmailNuevo1]").value.length != 0 )
        {
            if(document.getElementById("frmU[txtEmailNuevo1]").value != document.getElementById("frmU[txtEmailNuevo2]").value)
            {
                arrError.push(arrErrores["MICUENTA"]["MAIL_DIFERENTES"]);
            }
            else if(!ValidarEmail(document.getElementById("frmU[txtEmailNuevo1]").value))
            {
                arrError.push(arrErrores["MICUENTA"]["MAIL_INCORRECTO"]);
            }
        }
        if(document.getElementById("frmU[txtClaveNuevo1]").value.length != 0 )
        {
            if(document.getElementById("frmU[txtClaveNuevo1]").value != document.getElementById("frmU[txtClaveNuevo2]").value)
            {
                arrError.push(arrErrores["MICUENTA"]["CLAVE_DIFERENTES"]);
            }
            else if(document.getElementById("frmU[txtClaveNuevo1]").value.length < 6 && document.getElementById("frmU[txtClaveNuevo1]").value.length < 16)
            {
                arrError.push(arrErrores["MICUENTA"]["CLAVE_INCORRECTO"]);
            }
        }
        if(arrError.length >0)
        {
            oCortina.showError(arrError, arrErrores["MICUENTA"]["TITLE_ERROR"]);
            return false;
        }
        document.frmMiCuenta.submit();
        //frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}
function iniciar(pais,prov,celue)
{
    setProvinciaDir(prov);
    setPaisDir(pais);
    setCeluEmpresa(celue); 
}

function setProvinciaDir(prov)
{
    document.getElementById('frmDi1[cbxProvincia]').value = prov;    
}
function setPaisDir(pais)
{
    document.getElementById('frmDi1[cbxPais]').value = pais;
}
function setCeluEmpresa(celue){
    document.getElementById('frmDaU[cmb_CelularComp]').value = celue;
}