<?
    require_once("../../conf/configuracion.php");
    Aplicacion::logoutUser();
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts("../../") ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("css/forms_g.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/newsignup.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/confirmacion/js/js.js");?>" type="text/javascript"></script>

</head>
<body>
  <!-- empieza cabecera -->
	<div class="header">		
		<div class="menu">		
			<ul>
				<li class="last"><a class="fiveth" href="/<?=Aplicacion::getDirLocal()?>front/contacto/"></a></li>
				<li><a class="third" href="/blog"></a></li>
				<li><a class="fourth" href="/<?=Aplicacion::getDirLocal()?>front/acerca/"></a></li>
				<li><a class="second" href="/<?=Aplicacion::getDirLocal()?>front/registro/"></a></li>
				<li><a class="nineth" href="/<?=Aplicacion::getDirLocal()?>front/login/"></a></li>
			</ul>
		</div>
		<a href="/" class="logo"></a>
	</div>
	<!-- termina cabecera -->
<div class="contenedor">
  	<h1>Ahora, debes confirmar tu registraci&oacute;n</h1>
	<h2>Hemos enviado un email a tu casilla, debes chequearlo para confirmar la registración.</h2>
	<br />
	<h4>&iquest;No recibiste el email?</h4>
	<ul class="step2">
		<li><h5>&iquest;Pasaron menos de 10 minutos? A veces tarda unos minutos en llegar.</h5></li>
		<li><h5>Revisa la carpeta de correo no deseado (spam) de tu casilla. Si esto sucede, agrega en tu libreta de direcciones a geelbe@geelbemail.com como direcci&oacute;n segura.</h5></li>
	</ul>

	<!--h3>&nbsp;</h3!-->
	<!--h3>Por favor segu&iacute; las instrucciones en el email que te acabamos de enviar!</h3!-->
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

</div>       
<? require("../menuess/footer.php")?>

<script type="text/javascript"> if(typeof(_gat)!='object')document.write('<sc'+'ript src="http'+ (document.location.protocol=='https:'?'s://ssl':'://www')+ '.google-analytics.com/ga.js"></sc'+'ript>')</script> <script type="text/javascript"> try { var pageTracker=_gat._getTracker("UA-6895743-8"); pageTracker._trackPageview("/2941166409/goal"); }catch(err){}</script>


</body>
</html>
