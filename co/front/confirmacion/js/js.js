function refrescar_captcha()
{
    document.getElementById("frmCaptcha").src = "../../logica/framework/class/imgcaptcha.php";
}
function ErrorMSJ(titulo, code)
{
    oCortina.showError(Array(arrErrores["LOGIN"][code]), titulo);
}
function InfoMSJ(titulo, code, pagina, showbutton)
{
        oCortina.showInfo(Array(arrErrores["REGISTRO"][code]), titulo, pagina, showbutton);
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtPass.value.length < 6 || frm.txtPass.value.length > 16)
        {
            arrError.push(arrErrores["LOGIN"]["CLAVE_INCORRECTO"]);
        }
        if (arrError.length>0)
        {
            oCortina.showError(arrError, arrErrores["LOGIN"]["TITLE_ERROR"]);
            return false;
        }
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}

function calcular_edad(ano, mes, dia){ 

    //calculo la fecha de hoy 
    hoy=new Date() 
    
    if (isNaN(ano)) 
       return false 
    if (isNaN(mes)) 
       return false 
    if (isNaN(dia)) 
       return false 
    //resto los a�os de las dos fechas 
    edad=hoy.getFullYear()- ano - 1; //-1 porque no se si ha cumplido a�os ya este a�o 
   
    //si resto los meses y me da menor que 0 entonces no ha cumplido a�os. Si da mayor si ha cumplido 
    if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0 
       return edad 
    if (hoy.getMonth() + 1 - mes > 0) 
       return edad+1 
    //entonces es que eran iguales. miro los dias 
    //si resto los dias y me da menor que 0 entonces no ha cumplido a�os. Si da mayor o igual si ha cumplido 
    if (hoy.getUTCDate() - dia >= 0) 
      return edad + 1 

    return edad 
} 