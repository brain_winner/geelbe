<?
    require_once("../../conf/configuracion.php");
    Aplicacion::logoutUser();
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts("../../") ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/botones.css");?>" rel="stylesheet" type="text/css"  />
<script src="<?=UrlResolver::getJsBaseUrl("front/confirmacion/js/js.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">
	<h1 style="margin-top:5px;">Paso 2 de 3 - Registro de Socios <br /> 
	Mir&aacute; el email que te acabamos de enviar y segu&iacute; las instrucciones!</h1>
  </div>
  <div id="contenido">
	<!--h3>&nbsp;</h3!-->
	<!--h3>Por favor segu&iacute; las instrucciones en el email que te acabamos de enviar!</h3!-->
	<p>&nbsp;</p>
	<h2><strong style="color:#FF0000">Importante:</strong> <span lang="es" xml:lang="es">Si no te lleg&aacute; en tu bandeja de entrada,  verific&aacute; </span>tu <span lang="es" xml:lang="es">correo no deseado (Spam).<br />
    Evit&aacute; esto agregando el email geelbe@geelbemail.com como direcci&oacute;n segura en tu libreta de   direcciones.</span></h2>
    <!--h2><strong style="color:#FF0000">Importante:</strong> <span lang="es" xml:lang="es">Es probable </span><span lang="es" xml:lang="es">que este primer email que te enviamos llegue como correo no deseado o   Spam. Para evitarlo agreg&aacute; el email geelbe@geelbemail.com como direcci&oacute;n segura en tu libreta de   direcciones.</span></h2!-->
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>
		<table class="btn btnRed izquierda normal XS" cellpadding="0" cellspacing="0" border="1px">
			<tr>
				<td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
					<a href="/testing/front/login"><strong>Aceptar</strong></a>
				</td>
			</tr>
		</table>
	</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
    	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>

	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p><br />
    </p>
	<p>&nbsp;</p>  
  </div>
  <div id="contenido-bot"></div>
</div>       
<? require("../menuess/footer.php")?>
</body>
</html>
