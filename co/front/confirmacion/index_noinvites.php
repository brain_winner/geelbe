<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/partnerscod/clases/clspartnerscod.php");

	if(empty($_SESSION['IdRegistro'])){
		redirect(Aplicacion::getRootUrl()."/front/registro");
		exit();
	}

    Aplicacion::logoutUser();
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
    
    
    
    $codInvitacion = "";
    if(isset($_SESSION['IdRegistro'][0]["Id"])) {
	    try {
	       $objUsuario = new MySQL_micuenta();
	       $objUsuario = dmMicuenta::GetUsuario($_SESSION['IdRegistro'][0]["Id"]);
	       
	    } catch(exception $e) {
	        echo $e;
	        exit;
	    } 
        if(isset($objUsuario)) {
		    $codInvitacion = $objUsuario->getPadrino();

	       $oConexion = Conexion::nuevo();
           $oConexion->setQuery("SELECT b.pixel_registro FROM partners_codigos_base b JOIN partners_codigos p ON p.IdCodigoBase = b.IdCodigoBase WHERE p.Codigo = '".$objUsuario->getPadrino()."'");
           $pixel = current($oConexion->DevolverQuery());
           $pixel = PartnersCod::pixel($pixel['pixel_registro'], $objUsuario->getIdUsuario(), $objUsuario->getNombreUsuario());
	    }
    }
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?></title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css")?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/confirmacion/confirmacion.css")?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("css/forms_g.css")?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/botones.css")?>" rel="stylesheet" type="text/css" />

<script src="<?=UrlResolver::getJsBaseUrl("front/confirmacion/js/js.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
<style type="text/css">
#footer{
	background:none!important;
	background-color:#FFF!important;
}
.oscurito{
	#color:#333;
	font-weight:400;
	
}

h1.preg{
	font-size:20px;
	font-weight:bold;
	color:#333;
}

</style>

</head>
<body>
<?php require_once '../menuess/google.php'; ?>
  <?
    
$splitEmail = split("@",$_SESSION['inviteEmail']);
$emailDomain = $splitEmail[1];
switch ($emailDomain) {
	case 'gmail.com': $emailProvider = 'gmail'; break;
	case 'hotmail.com': case 'msn.com': case 'live.com': $emailProvider = 'hotmail'; break;
	case 'yahoo.com': case 'yahoo.com.ar': $emailProvider = 'yahoo'; break;
}
?>
<div id="container" style="margin-top:20px; background-color:#fff!important">
  <div id="contenido" style="background:none!important; background-color:#FFF">
        <div>
            <div id="proc-reg">Proceso de Registro</div>
        </div>
        <div class="unfloat">
            <div class="col_izquierda">
                <div><img align="left" src="<?=UrlResolver::getImgBaseUrl("front/confirmacion/img/titulo.jpg")?>" /></div>
                <div>
                  <p style="clear:both">Ingresa a tu cuenta de correo (<?=$_SESSION['inviteEmail']?>) y activa tu cuenta para poder empezar a aprovechar los beneficios de ser socio de Geelbe.</p>
                </div>

				<? if ($emailProvider == 'hotmail') { ?>
                <p>
                <table class="btn btnRed normal M" cellpadding="0" cellspacing="0" border="1px">
                        <tr>
                            <td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
                                <a href="http://www.hotmail.com"><strong>Ir a Hotmail</strong></a>
                            </td>
                        </tr>
                    </table>
                </p>
                <? } ?>	
                <? if ($emailProvider == 'yahoo') { ?>
                <p>
                <table class="btn btnRed normal M" cellpadding="0" cellspacing="0" border="1px">
                        <tr>
                            <td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
                                <a href="http://mail.yahoo.com"><strong>Ir a Yahoo</strong></a>
                            </td>
                        </tr>
                    </table>
                </p>
                <? } ?>
                <? if ($emailProvider == 'gmail') { ?>
                <p>
                    <table class="btn btnRed normal centro XS" cellpadding="0" cellspacing="0" border="1px">
                        <tr>
                            <td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
                                <a href="http://www.gmail.com"><strong>Ir a Gmail</strong></a>
                            </td>
                        </tr>
                    </table>
                </p>
                <? } ?>
				<h1 class="preg">&iquest;A�n no recibiste el correo electr&oacute;nico de activaci&oacute;n?</h1>
            <p>Revisa la carpeta de <i>Correo no deseado</i> de tus emails <i>(spam)</i>,<br />para verificar si el mail se encuentra all�.<br />
              Si esto sucede, agrega en tu libreta de direcciones<br />
              a geelbe@geelbemail.com como direcci&oacute;n segura.</p>
            <h2><br /><strong>&iquest;Pasaron menos de 10 minutos?</strong></h2>
            <p>A veces los mails tardan algunos minutos en llegar.<br />
            Por favor, vuelve a intentarlo m&aacute;s tarde.</p>

	    </div>
        <div class="col_derecha">
        	<img src="<?=UrlResolver::getImgBaseUrl("front/confirmacion/img/montaje-mail.jpg")?>" />
        </div>
	</div>

	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
    <p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>  
  </div>
</div>       
<?  

if($codInvitacion == 'visacolombiaon1' || $codInvitacion == 'visacolombiaoff1'){
	      	echo '<script src="'.UrlResolver::getJsBaseUrl("logica/scripts/cookie_padrino.js").'" type="text/javascript"></script>';
			$_SESSION['padrino'] = true;
  }

require("../menuess/footer.php");	
?>

<!-- Google Analytics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script src="http://js.static.geelbe.com/co/registro/js/ga.js" type="text/javascript"></script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-6895743-8");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<!-- Google Analytics -->

<!-- Pixeles de Conversion -->
<?php include_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/confirmacion/pixels.php"; ?>
<?php include_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/confirmacion/gwo.php"; ?>
<!-- Pixeles de Conversion -->

<!-- Google Code for REGISTROS GEELBE Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1020556748;
var google_conversion_language = "en";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "AQf0CLSNv1IQzOvR5gM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript"  
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""  
src="//www.googleadservices.com/pagead/conversion/1020556748/?value=0&amp;label=AQf0CLSNv1IQzOvR5gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript">
var fb_param = {};
fb_param.pixel_id = '6008284374590';
fb_param.value = '0.00';
(function(){
  var fpw = document.createElement('script');
  fpw.async = true;
  fpw.src = '//connect.facebook.net/en_US/fp.js';
  var ref = document.getElementsByTagName('script')[0];
  ref.parentNode.insertBefore(fpw, ref);
})();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6008284374590&amp;value=0" /></noscript>

	<?php if($objUsuario->getPadrino() == 'campananate-nov-131'): ?>
	<img src="http://tbl.tradedoubler.com/report?organization=1879910&event=303419&leadNumber=<?=($_SESSION['server'])?>" border=0>
	<img src="http://tbl.tradedoubler.com/report?organization=1879910&event=303419&leadNumber=<?=$objUsuario->getNombreUsuario()?>" border=0>
	<?php endif; ?>

	<?php if($objUsuario->getPadrino() == 'ims-dic-131'): ?>
	<img src="http://tbl.tradedoubler.com/report?organization=1879910&event=303419&leadNumber=<?=($_SESSION['server'])?>" border=0>
	<img src="http://tbl.tradedoubler.com/report?organization=1879910&event=303419&leadNumber=<?=$objUsuario->getNombreUsuario()?>" border=0>
	<?php endif; ?>
	
	<?php if(isset($pixel)) echo $pixel; ?>

</body>
</html>
