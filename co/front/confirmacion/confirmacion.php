<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::logoutUser();
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases")); 
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/partnerscod/clases/clspartnerscod.php");
    $postURL = Aplicacion::getIncludes("post", "login");
    
    $codInvitacion = "";
    if(isset($_SESSION['IdRegistro'][0]["Id"])) {
	    try {
	       $objUsuario = new MySQL_micuenta();
	       $objUsuario = dmMicuenta::GetUsuario($_SESSION['IdRegistro'][0]["Id"]);
	       
	    } catch(exception $e) {
	        echo $e;
	        exit;
	    } 
        if(isset($objUsuario)) {
		    $codInvitacion = $objUsuario->getPadrino();

	       $oConexion = Conexion::nuevo();
           $oConexion->setQuery("SELECT b.pixel_activacion FROM partners_codigos_base b JOIN partners_codigos p ON p.IdCodigoBase = b.IdCodigoBase WHERE p.Codigo = '".$objUsuario->getPadrino()."'");
           $pixel = current($oConexion->DevolverQuery());
           $pixel = PartnersCod::pixel($pixel['pixel_activacion'], $objUsuario->getIdUsuario(), $objUsuario->getNombreUsuario());
           
	    }
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>

<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css")?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("css/forms_g.css")?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/botones.css")?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/confirmacion/js/js.js");?>" type="text/javascript"></script>
<link href="<?=UrlResolver::getCssBaseUrl("front/confirmacion/confirmacion.css");?>" rel="stylesheet" type="text/css" />

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
  <? require("../menuess/cabecera.php") ?>
<div id="container">
    <div id="contenido">

    	<div>
			<div id="proc-reg">Proceso de Registro</div>
            <div class="timeline-off"><p>Ingresa<br />tus datos</p></div>
            <div class="timeline-off" style="background-position:-85px 0;"><p>Invita<br />amigos</p></div>
            <div class="timeline-off" style="background-position:-170px 0;"><p>Confirma<br />tu email</p></div>
            <div class="timeline-on" style="background-position:-255px 0;"><p>Registro exitoso</p></div>
		</div>
		
        <div>
  	<h1>&iexcl;Bienvenido a Geelbe!</h1>
	<h2>Ya eres parte de Geelbe.com, ingresa a la vitrina y disfruta de nuestras ofertas.</h2>
	
    <form  method="POST" name="frmLogin" id="frmLogin" onkeypress="if (ValidarEnter(event)) this.onsubmit();" target="iLogin" action="../../../<?=Aplicacion::getDirLocal().$postURL["login"]?>" onSubmit="ValidarForm(this);">
    <table border="0" cellpadding="0" cellspacing="0">
<tr>
            <td colspan="3"><h3>E-mail</h3></td>
      </tr>
<tr>
  <td colspan="3"><input type="text" maxlength="60" name="txtUser" id="txtUser" size=25 value="<?=$_POST["ee"]?>" /></td>
</tr>
        <tr>
        <td colspan="3"><h3>Contrase&ntilde;a</h3></td>
        </tr>
        <tr>
          <td colspan="3"><input type="password" name="txtPass" id="txtPass" maxlength="16" /></td>
        </tr>
	</table>

	<table class="btn btnRed izquierda grande XS" cellpadding="0" cellspacing="0" border="1px">
				<tr>
					<td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
						<a href="javascript:document.forms['frmLogin'].onsubmit();"><strong>Ingresar</strong></a>
					</td>
				</tr>
	</table>
    <br />
  </form>

  </div>
  </div>
  <div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
<iframe style="display:none" name="iLogin" id="iLogin"></iframe>
	<?php if(isset($pixel)) echo $pixel; ?>

</body>
</html>
