<?
    require_once("../../conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts("../../") ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/invitar/js/js.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">

  </div>
  <div id="contenido">
	<div id="top">	
	  <h1>Acerca de Geelbe</h1>
	</div>
	<div id="central">
	  <h2><img src="<?=UrlResolver::getImgBaseUrl("front/images/mail_compose_48.png");?>" alt="Invitar Amigos" width="48" height="48" align="middle" /> <strong>Invitar Amigos a Geelbe</strong></h2>
	  <h3>R&aacute;pidamente descubrir&aacute;s las ventajas que Geelbe ofrece  a los miembros del club privado de compras y desear&aacute;s compartirlo con tus  familiares y amigos!</h3>
	  <p>&nbsp;</p>
	  <p>S&oacute;lo se puede formar parte del club  privado de compras por Internet obteniendo una recomendaci&oacute;n de un miembro  registrado de <strong>Geelbe</strong>,  que refiere la suscripci&oacute;n de un nuevo integrante. </p>
	  <p>&nbsp;</p>
	  <p align="left">Para  esto creamos un programa de Referidos que te permite invitar amigos, regalarles  dinero para que realicen su primera compra y adem&aacute;s acumular cr&eacute;dito en tu  cuenta de <strong>Geelbe</strong>.<br />
          <br />
	    Cuando un miembro del club refiere a un nuevo integrante le obsequia $10.000 para  que los utilice en su primera compra (superior a $50.000). En el momento que el  referido realiza su primera compra (superior a $50.000) al miembro que lo  recomienda se le acreditan $10.000 en su cuenta, para que pueda utilizarlos en sus  adquisiciones dentro de <strong>Geelbe</strong>.</p>
	  <p>&nbsp;</p>
	  <p><strong style="color:#FF0000">Mientras m�s personas invites a Geelbe m�s dinero tendr�s acreditado en tu cuenta!</strong></p>
	  <p>&nbsp;</p>
	  <h3>C&oacute;mo invitar amigos? </h3>
	  <p>Para referir la suscripci&oacute;n de un nuevo  miembro tienes 2 herramientas disponibles:</p>
	  <p>&nbsp;</p>
	  <p><strong>Formulario de Recomendaci&oacute;n:</strong> A trav&eacute;s del sistema de recomendaci&oacute;n puedes ingresar  la direcci&oacute;n de correo electr&oacute;nico de tus amigos para enviarles un eMail de  invitaci&oacute;n con el c&oacute;digo de recomendaci&oacute;n que les permite registrarse en <strong>Geelbe</strong>. </p>
	  <p>&nbsp;</p>
	  <p><strong>Formulario de Registro:</strong> Tambi&eacute;n puedes referir nuevos integrantes si ingresan  tu direcci&oacute;n de eMail -con la que est&aacute;s registrado como miembro- cuando acceden  al formulario de registro de <strong>Geelbe</strong>.</p>
	  <p>&nbsp;</p>
	  <h2><a href="/co/front/terminos">Condiciones de uso y otorgamiento de cr�ditos</a></h2>
	  <p>&nbsp;</p>
	  <div class="grisado">
        <h2>
		<?
	    	if ($_SESSION["User"]["id"])
			{
		?>
				<a href="/co/front/referenciados/?act=4"><strong>Gana dinero invitando a tus amigos a Geelbe!</strong></a>
		<?
			}
			else
			{
		?>
				<a href="/co/registro/"><strong>Gana dinero invitando a tus amigos a Geelbe!</strong></a>
         <?
		 	}
		 ?>
        
        </h2>
	    <p>Con  nuestro sistema de recomendaci&oacute;n puedes invitar a tus amigos para que se  registren en <strong>Geelbe</strong> y acreditar dinero en tu cuenta para utilizarlo en las futuras compras.  Mientras m&aacute;s amigos recomiendes en <strong>Geelbe</strong> m&aacute;s beneficios obtendr&aacute;s como miembro.</p>
      </div>
	  <h2>&nbsp;</h2>
    </div>
  <div id="menuleft">
		<? 
		$act=8;
		require("../menuess/acercade.php")
		?>
	</div>
</div>
  <div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?></body>
</html>
