function ErrorMSJ(titulo, code)
{
    oCortina.showError(Array(arrErrores["INVITACIONMANUAL"][code]), titulo);
}
function ValidarForm(frm)
{
    try
    {      
        var arrError = new Array();
        var cantMails = 8;
        var cantVacios = 0;
        for (i=0; i<frm.length; i++)
        {
            if (frm[i].name == "frmEmail[]")
            {
                if (frm[i].value != "")
                {
                    if (!ValidarEmail(frm[i].value))
                    {
                        arrError.push("Usuario " + (i + 1) + ": " + arrErrores["INVITACIONMANUAL"]["MAIL_INCORRECTO"]);
                    }
                }
                else
                {
                    cantVacios++;                    
                }
            }
        }
        if (cantVacios == cantMails)
        {
            arrError.push("No ingresaste ning�n mail.");
        }
        if (arrError.length > 0)
        {
             window.parent.msjError(arrError, arrErrores["INVITACIONMANUAL"]["TITLE_ERROR"]);
             return false;
        }
        //window.parent.msjCargar("enviando invitaciones");
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}