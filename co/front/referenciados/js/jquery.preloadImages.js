/*
 * jQuery preloadImages plugin
 * Version 0.1.1  (20/12/2007)
 * @requires jQuery v1.2.1+
 *
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * @name preloadImages
 * @type jQuery
 * @cat Plugins/Browser Tweaks
 * @author Blair McBride <blair@theunfocused.net>
*/

(function($) {
$.preloadImages = function(arr) {
	$.preloadImages.add(arr);

	queuedStop = false;
	startPreloading();
};
$.preloadImages.add = function(arr) {
	if(typeof(arr) == 'string') {
		$.preloadImages.imageQueue.push(arr);
		return;
	}

	if(arr.length < 1) return;

	for(var i = 0, numimgs = arr.length; i < numimgs; i++) {
		if(typeof(arr[i]) == 'string')
			$.preloadImages.imageQueue.push(arr[i]);
		else if(typeof(arr[i]) == 'object' && arr[i].length > 0)
			$.preloadImages.add(arr[i]);
	}
}
$.preloadImages.imageQueue = [];
var isPreloading = false;
var queuedStop = false;
function startPreloading() {
	if (isPreloading) return;
	for (var i = 0 ; i < $.preloadImages.imageQueue.length ; i++) {
		var img = document.createElement('img');
		$(img).load(function() { $(this).remove(); });
		img.src = $.preloadImages.imageQueue[i];
	}
}


})(jQuery);
