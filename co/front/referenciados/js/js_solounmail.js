function refrescar_captcha()
{
    document.getElementById("frmCaptcha").src = "../../logica/framework/class/imgcaptcha.php";
}
function ErrorMSJ(titulo, code)
{
    oCortina.showError(Array(arrErrores["REGISTRO"][code]), titulo);
}
function InfoMSJ(titulo, code, pagina, showbutton)
{
        oCortina.showInfo(Array(arrErrores["REGISTRO"][code]), titulo, pagina, showbutton);
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtNombre.value.length == 0)
        {
            arrError.push(arrErrores["REGISTRO"]["NOMBRE"]);
        }
        if(frm.txtApellido.value.length == 0)
        {
            arrError.push(arrErrores["REGISTRO"]["APELLIDO"]);
        }
        if(!ValidarEmail(frm.txtEmail.value))
        {
            arrError.push(arrErrores["REGISTRO"]["MAIL_INCORRECTO"]);
        }
        
        if(frm.txtPadrino.value.indexOf("?")!=-1){
        	//Parche para el caso del mailing erroneo ej: mmmm@ppp.com?cod=1234 (queremos recortar a partir del ?).
        	
        	pos_questionmark = frm.txtPadrino.value.indexOf("?");
        	frm.txtPadrino.value = (pos_questionmark == false) ? $padrino : frm.txtPadrino.value.substr(0,pos_questionmark);
        }

		if(frm.txtPadrino.value.length == 0)
        {
            arrError.push(arrErrores["REGISTRO"]["PADRINO"]);
        }
             
        
        
        if(frm.txtPadrino.value.length >0 && frm.txtEmail.value == frm.txtPadrino.value)
        {
            arrError.push(arrErrores["REGISTRO"]["PADRINO_MISMO"]);
        }
        if(frm.txtClave.value.length < 6 || frm.txtClave.value.length > 16)
        {
            arrError.push(arrErrores["REGISTRO"]["CLAVE_INCORRECTO"]);
        }
        if(frm.txtClave.value != frm.txtConfClave.value)
        {
            arrError.push(arrErrores["REGISTRO"]["CLAVE_DIFERENTES"]);
        }
		// Lo hizo Gabriel Miller para que en la invitaci�n de S�nico no valide el mail.
		// http://www.geelbe.com/co/front/registro/sonico.php
        //
		//if(frm.txtEmail.value != frm.txtEmail2.value)
        //{
        //    arrError.push(arrErrores["REGISTRO"]["EMAILS_DIFERENTES"]);
        //}         
        if (frm.cmb_nacimientoAnio.value == 0 || frm.cmb_nacimientoMes.value == 0 ||  frm.cmb_nacimientoDia.value == 0)
        {
            arrError.push(arrErrores["REGISTRO"]["FECHA DE NACIMIENTO"]);
        }
        else
        {
             if (!(EsFechaValida(frm.cmb_nacimientoDia.value, frm.cmb_nacimientoMes.value, frm.cmb_nacimientoAnio.value)))
            {
                arrError.push(arrErrores["REGISTRO"]["FECHA INVALIDA"]);
            }                                                                                                                    
            else if(calcular_edad(frm.cmb_nacimientoAnio.value,frm.cmb_nacimientoMes.value,frm.cmb_nacimientoDia.value) < 18)
            {
                arrError.push(arrErrores["REGISTRO"]["MENOR DE EDAD"]);
            }
        }
        if(frm.acepto2.checked == false)
        {
            arrError.push(arrErrores["REGISTRO"]["TERMINOS"]);
        }         

        if (arrError.length >0)
        {
            oCortina.showError(arrError, arrErrores["REGISTRO"]["TITLE_ERROR"]);
            return false
        }   
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}

function calcular_edad(ano, mes, dia){ 

    //calculo la fecha de hoy 
    hoy=new Date() 
    
    if (isNaN(ano)) 
       return false 
    if (isNaN(mes)) 
       return false 
    if (isNaN(dia)) 
       return false 
    //resto los a�os de las dos fechas 
    edad=hoy.getFullYear()- ano - 1; //-1 porque no se si ha cumplido a�os ya este a�o 
   
    //si resto los meses y me da menor que 0 entonces no ha cumplido a�os. Si da mayor si ha cumplido 
    if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0 
       return edad 
    if (hoy.getMonth() + 1 - mes > 0) 
       return edad+1 
    //entonces es que eran iguales. miro los dias 
    //si resto los dias y me da menor que 0 entonces no ha cumplido a�os. Si da mayor o igual si ha cumplido 
    if (hoy.getUTCDate() - dia >= 0) 
      return edad + 1 

    return edad 
} 