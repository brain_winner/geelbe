<?
    class Includes
    {
        public static function Scripts($ssl=false)
        {	
        	$css = (!$ssl ? 'getCssBaseUrl' : 'getBaseUrl');
        	$js = (!$ssl ? 'getJsBaseUrl' : 'getBaseUrl');
            ?>
            <link rel="icon" type="image/png" href="<?=UrlResolver::getImgBaseUrl('favicon.png');?>" />
            
            <script type="text/javascript">
                var DIRECTORIO_URL_JS = "<?=UrlResolver::$js();?>";
                var DIRECTORIO_JS = "<?=UrlResolver::$js();?>";
                var MONEDA_FORMATO = "<?=Aplicacion::getParametros("moneda", "formato")?>";
                var MONEDA_DECIMALES = "<?=Aplicacion::getParametros("moneda", "decimales")?>";
            </script>
            <script src="<?=UrlResolver::$js("logica/scripts/AC_RunActiveContent.js",$ssl);?>" type="text/javascript"></script>
            <script src="<?=UrlResolver::$js("logica/scripts/cortina.js",$ssl);?>" type="text/javascript"></script>
            <script src="<?=UrlResolver::$js("logica/scripts/global_msj.js",$ssl);?>" type="text/javascript"></script>
            <script src="<?=UrlResolver::$js("logica/scripts/funcionesgenericas.js",$ssl);?>" type="text/javascript"></script>
            <script src="<?=UrlResolver::$js("logica/scripts/reloj.js",$ssl);?>" type="text/javascript"></script>

            <script src="<?=UrlResolver::$js("logica/scripts/highslide-with-html.js",$ssl);?>" type="text/javascript"></script>
            <script src="<?=UrlResolver::$js("logica/scripts/swfobject.js",$ssl);?>" type="text/javascript"></script>
            <link href="<?=UrlResolver::$css("logica/scripts/stylecortina.css",$ssl);?>" rel="stylesheet" type="text/css" />

             <script type="text/javascript">
                hs.graphicsDir = DIRECTORIO_URL_JS + "front/images/highslide/";
                hs.outlineType = 'rounded-white';
                hs.outlineWhileAnimating = true;

                hs.allowSizeReduction = false;
                // always use this with flash, else the movie will be stopped on close:
                hs.preserveContent = false;
            </script>
            
            <?
        }
    }
?>