<?
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("catalogo"));
        
		
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php';
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/catalogo/categoria_head.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/framework/paginador_new.php");
		
    }
    catch(Exception $e) {
        die(print_r($e));
    }
    
    
    $validbo = false;
    try {
		ValidarUsuarioLogueadoBo(9);
		$validbo = true;
	}
	catch(Exception $e) {
		$validbo = false;
	}
    
    if ($validbo) { // Modo Test
    	try {
			$isPageOk = true;
			$showAll = false;
			$pageException = "Exception";
			
			// Validaciones de parametros basicas
	        if (!isset($_GET["IdCampania"])) { // IdCampania Seteado
	            $isPageOk = false;
				$pageException = "IdCampania not setted";
			}
			else if (!ValidatorUtils::validateNumber($_GET["IdCampania"])) { // IdCampania Numerico
				$isPageOk = false;
				$pageException = "IdCampania is not a number";
			}
			else if (isset($_GET["IdCategoria"])) {
				if (!ValidatorUtils::validateNumber($_GET["IdCategoria"])) { // IdCategoria Numerico
					$isPageOk = false;
					$pageException = "IdCategoria is not a number";
				}
				else if (!dmCampania::isCampaignCategory($_GET["IdCampania"], $_GET["IdCategoria"])) { // IdCategoria Correspondiente con IdCampania
					
					$isPageOk = false;
					$pageException = "IdCampania no corresponde con IdCategoria";
					
					if  ($_GET["IdCategoria"] == -1) {
						$isPageOk = true;
						$showAll = true;
					}
				}
				
				
				
			}
			
			if (!$isPageOk) {
				throw new Exception($pageException);
			}
	    }
	    catch(Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
			header('HTTP/1.1 404 Not Found');
			include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
			exit;
	    }
    }
    else {
	    try {
	        ValidarUsuarioLogueado();
		}
		catch(Exception $e) {
			echo $e;
			header('Location: /'.$confRoot[1].'/front/login');
			exit;
	    }
		try {
			$isPageOk = true;
			$showAll = false;
			$pageException = "Exception";
			
			// Validaciones de parametros basicas
	        if (!isset($_GET["IdCampania"])) { // IdCampania Seteado
	            $isPageOk = false;
				$pageException = "IdCampania not setted";
			}
			else if (!ValidatorUtils::validateNumber($_GET["IdCampania"])) { // IdCampania Numerico
				$isPageOk = false;
				$pageException = "IdCampania is not a number";
			}
			else if (!dmCampania::EsCampaniaAccesible($_GET["IdCampania"])) { // IdCampania Accesible
				$isPageOk = false;
				$pageException = "Campa�a not accesible";
			}
			else if (isset($_GET["IdCategoria"])) {
				if (!ValidatorUtils::validateNumber($_GET["IdCategoria"])) { // IdCategoria Numerico
					$isPageOk = false;
					$pageException = "IdCategoria is not a number";
				}
				else if (!dmCampania::isCampaignCategory($_GET["IdCampania"], $_GET["IdCategoria"])) { // IdCategoria Correspondiente con IdCampania
					
					$isPageOk = false;
					$pageException = "IdCampania no corresponde con IdCategoria";
					
					if  ($_GET["IdCategoria"] == -1) {
						$isPageOk = true;
						$showAll = true;
					}
				}
				
				
				
			}
			
			if (!$isPageOk) {
				throw new Exception($pageException);
			}
	    }
	    catch(Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
			header('HTTP/1.1 404 Not Found');
			include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
			exit;
	    }
    }
    
    
    
    if (!isset($showAll) || $showAll!=true) {
		$IdCategoria = $_GET["IdCategoria"];
		$cacheId = $IdCategoria;
	}
	else {
		$cacheId = "all";
	}
	
    
    //Obtener los datos
    $_REQUEST['PAG'] = isset($_REQUEST['PAG'])?$_REQUEST['PAG']:1;
	$_REQUEST['MAX_X_PAG'] = isset($_REQUEST['MAX_X_PAG'])?$_REQUEST['MAX_X_PAG']:15;

   	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
	$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 300), array('cache_dir' => $cacheDir));
	$nombreCache = "";
	if (isset($_GET["IdCategoria"])) {
		$nombreCache = 'cache_new_catalogo_campania_'.$_GET["IdCampania"].'_cat_'.$cacheId.'_maxxpag_'.(isset($_REQUEST['MAX_X_PAG'])?$_REQUEST['MAX_X_PAG']:Aplicacion::getParametros("paginador", "iniciar")).'_pag_'.(isset($_REQUEST['PAG'])?$_REQUEST['PAG']:1);
	} else {
		$nombreCache = 'cache_new_catalogo_campania_'.$_GET["IdCampania"].'_maxxpag_'.(isset($_REQUEST['MAX_X_PAG'])?$_REQUEST['MAX_X_PAG']:Aplicacion::getParametros("paginador", "iniciar")).'_pag_'.(isset($_REQUEST['PAG'])?$_REQUEST['PAG']:1);
	}

	try {
		if(!$contenidoCache = $cache->load($nombreCache)) {
			
			
	        if (isset($IdCategoria)) {
	            $oCategoria = dmCategoria::getByIdCategoria($IdCategoria);
	        }
	        $objCampania = dmCampania::getByIdCampania($_GET["IdCampania"]);
		    $dtCategorias = dmCategoria::getCategorias($_GET["IdCampania"], (!isset($IdCategoria) ? "IS NULL" : " = ".$IdCategoria));
	        $dtAllCategorias = dmCategoria::getCategorias($_GET["IdCampania"], "IS NULL");
	        $arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$_GET["IdCampania"]."/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));
	        
	        //Redireccionar si hay un solo producto
	        extract($_POST);
	        if($IdCategoria) {
	            array_push($dtCategorias, array("IdCategoria" => $IdCategoria, "Nombre" => "", "Descripcion" => "", "IdCampania" => $_GET["IdCampania"], "IdPadre" => 0,  "Orden" => 0, "subCategoria" => array ()));
	            if(isset($oCategoria)) {
	                for($j=0; $j<count($dtCategorias); $j++) {
	                    $dtCategorias[$j]["IdOrdenamiento"] = $oCategoria->getIdOrdenamiento();
	                }
	            }
	                //$dtCategorias[0]["IdOrdenamiento"] = $oCategoria->getIdOrdenamiento();
	                //array_push($dtCategorias, array("IdOrdenamiento" => $oCategoria->getIdOrdenamiento()));
	        }
	        
	        //$ProductosSinPaginar = dmProductos::getProductosOrdenados($dtCategorias);

	        if (isset($oCategoria)) {
	          $oOrdenamiento = dmOrdenamientos::getById($oCategoria->getIdOrdenamiento()); 
	          //$ProductosSinPaginar = OrdenarMatriz($ProductosSinPaginar, $oOrdenamiento->getCampo(), $oOrdenamiento->getOrdenamiento());  
	        } 
	        else {
	 	      $oOrdenamiento = dmOrdenamientos::getById($objCampania->getIdOrdenamiento()); 
	          //$ProductosSinPaginar = OrdenarMatriz($ProductosSinPaginar, $oOrdenamiento->getCampo(), $oOrdenamiento->getOrdenamiento());
	        }
	        /*
	        else {
				$ProductosSinPaginar = OrdenarMatriz($ProductosSinPaginar, 'PVenta', 'ASC');    	    
	    	}*/
	        
	        $ProductosTotal = dmProductos::getProductosTotal($dtCategorias);

	        $_Productos = dmProductos::getProductosOrdenadosPaginados($dtCategorias, $_REQUEST['PAG'], $_REQUEST['MAX_X_PAG'], $oOrdenamiento->getCampo(), $oOrdenamiento->getOrdenamiento());
	        
	        if(count($_Productos) == 1) {
	        	header("Location: detalle.php?IdCampania=".$objCampania->getIdCampania()."&IdCategoria=".dmProductos::getCategoria_byIdProducto($_Productos[0]["IdProducto"], $objCampania->getIdCampania())."&IdProducto=".$_Productos[0]["IdProducto"]);
	        	die;
	        }
	        
	    } // Fin de cache
    }
    catch(exception $e) {
    	$cache->remove($nombreCache);
        ?>
            <script>window.open('../accesodenegado/accesodenegado.php?errNo=<?=Aplicacion::Encrypter($e->getCodigo())?>&r=<?=Aplicacion::Encrypter($e->getTitulo())?>','_self');</script>
        <?
    }
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <? Includes::Scripts(); ?>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title><?=Aplicacion::getParametros("info", "nombre");?> </title>
	<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe_new.css");?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getCssBaseUrl("front/highslide.css")?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getCssBaseUrl("front/botones.css")?>" rel="stylesheet" type="text/css" />

    <link href="<?=UrlResolver::getCssBaseUrl("front/catalogo/css/producto_2011.css")?>" rel="stylesheet" type="text/css" />

    <script src="<?=UrlResolver::getJsBaseUrl("front/catalogo/js/js.js");?>" type="text/javascript"></script>
    <script src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.2.min.js");?>" type="text/javascript"></script>
    
    
    <?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>

<body>
    <? require("../menuess/menu_log.php") ?>
    <div id="container" style="margin:0 auto">
      <script type="text/javascript">
         fechaHoy = "<?=date("m/d/Y H:i:s");?>";
      </script>
    <? 
      if(!$contenidoCache = $cache->load($nombreCache)) {
      	 ob_start();
    ?>

<!-- head de campa�a -->
		<div id="camp-head" >
			<div id="camp-logo"><a href="main.php?IdCampania=<?=$objCampania->getIdCampania()?>">
				<?php $imgCamp = (isset($arrArchivos["logo2"]) ? $arrArchivos["logo2"] : $arrArchivos["thumb_logo"]); ?>
				<img src="<?=UrlResolver::getImgBaseUrl("front/campanias/archivos/campania_".$_GET["IdCampania"]."/imagenes/".$imgCamp);?>" width="140" alt="" /></a></div>

<!-- men� de categor�as -->
			<div id="categorias">
            	<h2><?=$objCampania->getNombre()?> finaliza en <div id="reloj"><script>IniciarReloj("reloj", "<?=ParserSTR::Timestamp_to_date(ParserSTR::Date_to_timestamp($objCampania->getFechaFin()), 'm/d/Y H:i');?>", fechaHoy);</script></div></h2>
                <hr />
                <?
                    $objCategoriaHead = new Categoria_Head("divCategorias", $dtAllCategorias );
                    $objCategoriaHead->setIdCategoria_Seleccionada((isset($IdCategoria))?$IdCategoria:-1);
                    $objCategoriaHead->setPagina("index.php");
                    $objCategoriaHead->setAdicionalGet("&IdCampania=".$_GET["IdCampania"]);
                    $objCategoriaHead->setNombreCampania($objCampania->getNombre(9));
                    $objCategoriaHead->show();
                ?>
            </div>
<!-- fin men� de categor�as -->

            	<hr class="separator" />
		</div>
<!-- fin head de campa�a -->


                <?

                    $action = "index.php?IdCampania=".$_GET["IdCampania"];
                    (isset($IdCategoria))?$action .="&IdCategoria=".$IdCategoria:"";
                    $objPaginador = new PaginadorNew(isset($POSICION)?$POSICION:0, array(), $action);
                    $objPaginador->setMaxXPag(isset($_POST['MAX_X_PAG'])?$_POST['MAX_X_PAG']:15);
                    $objPaginador->setPagina(isset($_POST['PAG'])?$_POST['PAG']:1);
                    $objPaginador->setCantPaginas(ceil($ProductosTotal/$_REQUEST['MAX_X_PAG']));
                    /*$objPaginador->setSeleccion(isset($cbxPaginasPaginador)?$cbxPaginasPaginador:count($ProductosSinPaginar));
                    $Productos = $objPaginador->getArrayPaginado();*/

                    $i=0;
                    for($x=0; $x<count($_Productos); $x++)
                    {
                         $arrImg = DirectorytoArray(Aplicacion::getRoot() . "front/productos/productos_".$_Productos[$x]["IdProducto"]."/", 3, array("jpg", "jpeg", "gif", "png"));
                      ?>

		<!-- producto -->
			<div class="prod-box">
			<!--foto producto -->
            	<div id="lupa"><img src="<?=UrlResolver::getImgBaseUrl("front/catalogo/images/over.jpg");?>" /></div>
            	<div id="foto-prod">
                
                <?php
	                $linkDetalle = "javascript:;"; 
	                if ($_Productos[$x]["StockMax"] > 0)	{
	                	$linkDetalle = "detalle.php?IdCampania=".$objCampania->getIdCampania()."&IdCategoria=".dmProductos::getCategoria_byIdProducto($_Productos[$x]['IdProducto'], $objCampania->getIdCampania())."&IdProducto=".$_Productos[$x]['IdProducto'];
	                }
                ?>
                
                <a href="<?=$linkDetalle?>">

   <?
                                    if ($arrImg["producto_1"]) {
                                        if ($_Productos[$x]["StockMax"] > 0) {
                                          ?>
                                                <div><img src="<?=UrlResolver::getImgBaseUrl("front/productos/productos_".$_Productos[$x]["IdProducto"]."/producto_1.".get_extension($arrImg["producto_1"]));?>" width="210" height="210" alt="Ver detalle" title="Ver detalle" border="1" /></div>   
                                          <?  
                                        } else {
                                            ?>
                                                <div><img src="<?=UrlResolver::getImgBaseUrl("front/productos/productos_".$_Productos[$x]["IdProducto"]."/producto_1.".get_extension($arrImg["producto_1"]));?>" alt="Ver detalle" title="Ver detalle"  width="210" height="210" border="1" /></div>   
                                            <?                                            
                                        }
                                    } else {
                                        if($_Productos[$x]["StockMax"] > 0)
                                        {
                                            ?>
                                                <p><img src="<?=UrlResolver::getImgBaseUrl("front/productos/noimagen.jpg");?>" alt="Ver detalle" title="Ver detalle"  border="1" /></p>
                                            <?
                                        } else {
                                            ?>
                                                <p ><img src="<?=UrlResolver::getImgBaseUrl("front/productos/noimagen.jpg");?>" alt="Ver detalle" title="Ver detalle" border="1" /></p>   
                                            <?                                            
                                        }
                                    }
                                ?>
				</a></div>
				<?
				/* Not for the moment..

                <div id="color-miniatura">
                	<ul id="color">
                		<li><img src="images/color-sample.jpg" /></li>
                       	<li><img src="images/color-sample.jpg" /></li>
                		<li><img src="images/color-sample.jpg" /></li>
                		<li><img src="images/color-sample.jpg" /></li>
                    </ul>
                </div>
                
                */
                ?>
			<!-- fin foto producto -->
			    <div>
                <p class="nombre-prod"><a href="<?=$linkDetalle?>"><?=$_Productos[$x]["Nombre"];?></a></p>
			    <p class="precio-geelbe">$<?=number_format($_Productos[$x]["PVenta"], 0, ',', '')?>
			      <?php
			        if($objCampania->getColeccion() == 0 && $_Productos[$x]["PVenta"] != $_Productos[$x]["PVP"]) {
			      ?>
			          <span style="font-weight:normal;letter-spacing:-0.5px" id="precio-lista">$<?=number_format($_Productos[$x]["PVP"], 0, ',', '')?></span>
			      <?php
			        }
			      ?>
			    </p>
			    </div>
                <?
                	if ($_Productos[$x]["StockMax"] > 0)	{
						$stock = '<p class="ultprod" >'.($_Productos[$x]['StockMax'] == 1 ? '&Uacute;ltima unidad' : '&Uacute;ltimas '.$_Productos[$x]['StockMax'].' unidades').'</p>';
						if (!($_Productos[$x]['ultimos'] == 0 || $_Productos[$x]['ultimos'] < $_Productos[$x]['StockMax'])) {
	                		echo $stock;
	                	}
                	} else {
				?>
                	<div class="agotado"></div>
                <?
                    }
                ?>
			</div>
		<!-- fin producto -->


                                <?
                        $i++;
                        if($i==2) {
                            $i=0;
                        }
                    }
                    $objPaginador->show();
                ?>    
        <script type="text/javascript">
			var paginas = <?=ceil($ProductosTotal/$_REQUEST['MAX_X_PAG'])?>;
		</script>              	

        <?
            $contenidoCache = ob_get_clean();
            $cache->save($contenidoCache, $nombreCache, array('group_cache_catalogo_campania_'.$_GET["IdCampania"]));
          }
          echo $contenidoCache;
        ?>
    </div>
    <? require("../menuess/footerNew.php");?>
</body>

<script type="text/javascript">

var pagina = 1;
var cargando = true;

$(window).bind('scroll', function() {

	if(cargando)
		return false;

	$('table.paginador').hide();
	var height = $(document).height() - $(window).height();
	var scroll = $(window).scrollTop();
	
	if(height - scroll <= 400) {
		
		cargando = true;
		pagina++;
		
		$.ajax({
			url: location.href+'&PAG='+pagina,
			dataType: 'html',
			success: function(data) {
				$('#container').append($(data).find('.prod-box'))
				if(pagina < paginas)
					cargando = false;
			}
		})
		
	}
	
	
})


function showSubCategory(idCategoria) {
	var display = $("#subcategorias"+idCategoria).css("display");

	if(display == "none") {
		$(".subcategorias").each(function(i, e){ $("#"+e.id).css("display", "none"); });
		$("#subcategorias"+idCategoria).css("display", "block");
		var position = $("#subcategorias"+idCategoria+"Trigger").offset();
		$("#subcategorias"+idCategoria).css({ top: position.top + 17, left: position.left - 2 });
	} else {
		$("#subcategorias"+idCategoria).css("display", "none");
	}
}
</script>
</html>