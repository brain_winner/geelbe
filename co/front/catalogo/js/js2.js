var currentPosition = 0;   
function imagenBox(num,prod,ext)
{
	document.getElementById('link_pp').href = "http://img.static.geelbe.com/co/front/productos/productos_" + prod +"/imagen_"+ num +"." + ext;
}   
function CambiarImagen(ruta)
 {
    document.getElementById("imgBamburgh").src = ruta; 
    
 }
function ValidarFormulario()
{
    var OK = true;
    OK = TodosLosCombosSeleccionados();
    if (OK)
    {
        if (parseInt(StockBase) < parseInt(document.getElementById("cbxCantidad").value))
        {
            msjError("No puedes comprar esa cantidad","Error al comprar");
        }
        else
        {
            var hdnCantidad = document.getElementById("Cantidad");
            hdnCantidad.value = document.getElementById("cbxCantidad").value;           
            document.forms[0].submit();
        }        
    }    
    else
    {
      msjError("Debes seleccionar todos los atributos","Error al comprar");       
    }    
}
function TodosLosCombosSeleccionados()
{
    for(i=0;i<vAtributos.length; i++)
    {
        var cbx = document.getElementById("cbx_"+vAtributos[i]);
        if (!(cbx.selectedIndex > 0))
            return false;
    }
    return true;
}
function NoAtributos()
{
        StockBase = vValores[0]["Stock"];
        var StockDisp = document.getElementById("StockDisp");
        StockDisp.value = StockBase;
        CargarComboboxNumerico(StockBase);
        document.getElementById("IdCodigoProdInterno").value = vValores[0]["IdCodigoProdInterno"];
        document.getElementById("divPrecio").innerHTML=Moneda(vValores[0]["Precio"], MONEDA_DECIMALES, MONEDA_FORMATO);
}
function CargarComboAtributos(index)
{
    if(vAtributos[index])
    {
        StockBase=0;
        if(document.getElementById("divPrecio"))
            document.getElementById("divPrecio").innerHTML = Moneda(PrecioBase, MONEDA_DECIMALES, MONEDA_FORMATO);
        var NombreCB = "cbx_" + vAtributos[index];
        var vOptions = new Array();
        vOptions = CargarVectorAtributos(vAtributos[index], index);
        VaciarCombosAtributos(index);
        var cbx = document.getElementById(NombreCB);
        for(i=0; i<vOptions.length; i++)
        {
           var option = document.createElement("option");
           option.setAttribute("value", vOptions[i][0]);
           option.appendChild(document.createTextNode(vOptions[i][0]));
           cbx.appendChild(option);
        }
    }
    if(TodosLosCombosSeleccionados())
    {
        vOptions = CargarVectorAtributos(vAtributos[index-1], index);
        var pos=0, i=0, Max=0;
        for(i=0; i< vOptions.length; i++)
        {
            if(vOptions[i][2] >= Max)
            {
                pos = i;
                Max = vOptions[i][2];
            }   
        }
        StockBase = Max;
        var StockDisp = document.getElementById("StockDisp");
        StockDisp.value = StockBase;
        CargarComboboxNumerico(Max);
        document.getElementById("IdCodigoProdInterno").value = vOptions[pos][3];
        document.getElementById("divPrecio").innerHTML=Moneda(vOptions[pos][1], MONEDA_DECIMALES, MONEDA_FORMATO);
    }
}
function CargarComboboxNumerico(max)
{
    var cbx = document.getElementById("cbxCantidad");
    cbx.length = 0;
    var n = 0;
    for(n=1; n<=max; n++)
    {
        var Option = document.createElement("option");
        Option.value = n;
        Option.innerHTML = n;
        cbx.appendChild(Option);
    }
}
function VaciarCombosAtributos(index)
{
    var Inicio = index;
    for(i=Inicio; i<vAtributos.length; i++)
    {
        IdAtributoClase = vAtributos[i];
        KillCombo(document.getElementById('cbx_' + IdAtributoClase));
    }
}
function KillCombo(cbx)
{
    cbx.options.length = 0;
    var option = document.createElement("option");
    option.setAttribute("value", "-1");
    option.appendChild(document.createTextNode("Seleccione"));
    cbx.appendChild(option);
}
function copy(o)  
{     
    if (typeof o != "object" || o === null) 
        return o;     
        var r = o.constructor == Array ? [] : {};     
        for (var i in o) 
        {
            r[i] = copy(o[i]);
        }     return r; }
function CargarVectorAtributos(IdAtributoClase, index)
{
    var AuxValores = new Array();
    var x=0,h=0,j=0,y=0;
    AuxValores = copy(vValores);
    for(h=0; h<index; h++) //Filtro
    {
        for(y=0; y<AuxValores.length; y++)
        {
            if(AuxValores[y]["Incluir"] != 0)
            {
                var ValorNuevo = document.getElementById("cbx_"+vAtributos[h]).value;
                if(AuxValores[y]["Atributos"][h]["Valor"] != ValorNuevo)
                {
                    AuxValores[y]["Incluir"]= 0;
                }
            }
        }
    }
    var vOp = new Array();
    for(x=0; x<AuxValores.length; x++)
    {
        for (j=0; j<AuxValores[x]["Atributos"].length; j++)
        {
            if(AuxValores[x]["Incluir"] != 0)
            {
                if(AuxValores[x]["Atributos"][j]["IdAtributoClase"] == IdAtributoClase)
                {
                    if(!ExisteEnVector(vOp, AuxValores[x]["Atributos"][j]["Valor"]))
                    {
                        var N = new Array();
                        N[0] = AuxValores[x]["Atributos"][j]["Valor"];
                        N[1] = AuxValores[x]["Precio"];
                        N[2] = AuxValores[x]["Stock"];
                        N[3] = AuxValores[x]["IdCodigoProdInterno"];
                        vOp.push(N);
                    }
                }
            }
        }
    }
    return vOp;
}
function ExisteEnVector(vPajar, Aguja)
{
    var i;
    for(i=0; i<vPajar.length; i++)
    {
        if (vPajar[i][0] == Aguja)
        {
            return 1;
        }
    }
    return 0;
}
function RecomendarAmigo()
{
    var email = document.getElementById("txtEmailAmigo").value;
    if (!ValidarEmail(email))
    {
        msjError("El mail ingresado no tiene un formato correcto", "Recomendar");
    }
    else
	{
		document.forms[1].submit();	
	}
}