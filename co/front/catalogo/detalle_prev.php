<?
	try
	{
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("catalogo"));
		$postURL = Aplicacion::getIncludes("post", "productos");

	}
	catch(exception $e)
	{
		die(print_r($e));
	}
	try
	{
		ValidarUsuarioLogueadoBO();
		if(!isset($_GET["IdCampania"]) && !isset($_GET["IdProducto"]))
			throw new ACCESOException("Entrada indirecta", "504", "vidriera");
	}
	catch(ACCESOException $e)
	{
	?>  
		<script>window.open('../accesodenegado/accesodenegado.php?errNo=<?=Aplicacion::Encrypter($e->getCodigo())?>&r=<?=Aplicacion::Encrypter($e->getTitulo())?>','_self');</script>
	<?
        exit;
     }
	
	try
	{
		$objCampania = dmCampania::getByIdCampania($_GET["IdCampania"]);
		$dtCategorias = dmCategoria::getCategorias($_GET["IdCampania"], (!isset($_GET["IdCategoria"]))?"IS NULL":" = ".$_GET["IdCategoria"]);
		$dtAllCategorias = dmCategoria::getCategorias($_GET["IdCampania"],"IS NULL");
		if (isset($_GET["IdCategoria"]))
			$oCategoria = dmCategoria::getByIdCategoria($_GET["IdCategoria"]);
		$oProducto = dmProductos::getById($_GET["IdProducto"]);
		$arrArchivos = DirectorytoArray(Aplicacion::getRoot()."/front/campanias/archivos/campania_".$_GET["IdCampania"]."/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));
	}
	catch(exception $e)
	{
		?>
		<script>window.open('../accesodenegado/accesodenegado.php?errNo=<?=Aplicacion::Encrypter($e->getCodigo())?>&r=<?=Aplicacion::Encrypter($e->getTitulo())?>','_self');</script>
		<?
	}        
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? 
    Includes::Scripts();
?>
<script src="<?=UrlResolver::getJsBaseUrl("front/catalogo/js/js.js");?>" type="text/javascript"></script>

	<script>                                                                                                
    var PrecioBase = "<?=$oProducto->getPVenta()?>";
    var StockBase = 0;
    </script> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/highslide.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/catalogo/css/slide.css");?>" rel="stylesheet" type="text/css" />

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>             
<div id="container">
  <div id="contenido-top">

  </div>
  <div id="contenido">
	<div id="top">	
	  <div id="topright" >
	    <h3><?=$objCampania->getNombre()?>.</h3>
        <h5><?=$objCampania->getDescripcion()?></h5>
	    <div class="right" style="margin:0;">Finaliza en <strong style="color:#666"><div id="reloj"> </div></strong></div>

        <?
            $dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($_GET["IdCampania"]);
            foreach($dtCampaniasPropiedades as $propiedad)
            {
                ?>
                <a href="<?=Aplicacion::getRootUrl() ?>front/compromisos/index.php?idCampaniaPropiedad=<?=$propiedad["IdPropiedadCampamia"]?>" onClick="return hs.htmlExpand(this, { objectType: 'iframe' } )" title="<?=$propiedad["Nombre"]?>" width="30" height="30" /><img src="<?=UrlResolver::getImgBaseUrl("front/campanias/propImg/".$propiedad["IdPropiedadCampamia"].".gif");?>"/></a>
                <?
            }
        ?>
	  </div> 
	  <div id="topleft"> 
                        <img src="../../front/campanias/archivos/campania_<?=$_GET["IdCampania"]?>/imagenes/<?=$arrArchivos["thumb_catalogo_logo"]?>" alt="" />
      </div>
	</div>
    
<div id="central">
	  <p><strong>Est�s en:</strong> <?=dmCategoria::getEstasEn($oCategoria?$oCategoria->getIdCategoria():null, $objCampania->getIdCampania()) ?></p>
	  <table width="516" border="0">
      <tr>
      <td width="255" valign="top">
	 <table width="99%" border="0" cellpadding="0" cellspacing="0">
    	  <tr>
		        <td width="248" height="279" style="background-repeat:no-repeat;padding:5px;" valign="top">
				<h3><?=$oProducto->getNombre()?></h3> 
                
                
				<div id="gallery">
				  <div id="imagearea">
				    <div id="image">
						<?
						$dir = "../../front/productos/productos_".$oProducto->getIdProducto()."/thumbs/";
						$arrImg = DirectorytoArray($dir, 1, array("jpg", "jpeg", "gif", "png"));
						if (!count($arrImg) > 0)
						{
							?>
								<a href="#"><img id="imgPrincipal" name="imgPrincipal" src="<?=UrlResolver::getImgBaseUrl("front/productos/noimagen.jpg");?>" alt="" border="1" /></a>
							<?
						}
						?>
				    </div>
				  </div>
				  <div id="thumbwrapper">
				  <span id="left_"></span>
					  <span id="right_"></span>
				    <div id="thumbarea">
					  <ul id="thumbs">
						<?                        
                            for($i=0; $i<count($arrImg); $i++)
                              {
                                 if ($arrImg[$i])
									{
                                       ?>
                                          <li value="<? $e = $i; $e++; echo $e;?>"><img id="<?=$dir.$arrImg[$i]?>" src="<?=$dir.$arrImg[$i]?>" alt="" /></li><?
                                    }
                                }
                            ?>
							
				      </ul>
				    </div>
				  </div>
				</div>
				
		        </td>
       	  </tr>
     </table>    

    </td>
    <td width="251"  background="<?=UrlResolver::getImgBaseUrl("front/images/ale3.gif");?>" style="background-repeat:repeat-y;" valign="top">
   	    <h5><?=$oProducto->getDescripcion()?></h5><br />
	    <form id="frmProducto" method="post" action="../../../<?=Aplicacion::getDirLocal().$postURL["productos"]?>">
        <input type="hidden" id="IdProducto" name="IdProducto" value="<?=$_GET["IdProducto"]?>" />
        <input type="hidden" id="IdCampania" name="IdCampania" value="<?=$_GET["IdCampania"]?>" />
        <input type="hidden" id="IdCodigoProdInterno" name="IdCodigoProdInterno" value="" />
        <input type="hidden" id="Cantidad" name="Cantidad" value="" />
        <input type="hidden" id="StockDisp" name="StockDisp" value="" />
        <?
            //CrearCombos_x_IdProducto($_GET["IdProducto"]);
        ?>
        <div name="divAtributos" id="divAtributos"></div>
        <?
			if (file_exists(Aplicacion::getRoot() . "front/productos/productos_" . $oProducto->getIdProducto() . "/talles.html"))
			{
				?><div align="right" style="margin-right:15px">
				 <a href="<?=Aplicacion::getRootUrl() . "front/productos/productos_" . $oProducto->getIdProducto() . "/talles.html"?>" onClick="return hs.htmlExpand(this, { objectType: 'iframe' } )">Ver gu&iacute;a de talles</a></div>
				 <?
			}
		?>
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td colspan="2"><p>Precio de Lista: <?=Moneda($oProducto->getPVP())?></p></td>
            </tr>
            <tr>
              <td width="59%" colspan="2" align="right" background="<?=UrlResolver::getImgBaseUrl("front/images/bg-precio-det.gif");?>" style="background-repeat:no-repeat;background-position:left">
			  <h1 style="color:#FFFFFF; font-size: 30px; margin-bottom:12px;margin-right:35px;margin-top:45px;"><div id="divPrecio" name="divPrecio" align="right"><?=Moneda($oProducto->getPVenta())?></div></h1>
			  <p>			    Producto Disponible</p>			  </td>
            </tr>
            <tr>
            <?
                if(!Aplicacion::VerificarCampaniaCarrito($objCampania->getIdCampania()))
                {
                    ?>
                    <td colspan="2"><b>IMPORTANTE: No pueden agregarse al carrito de compras productos de diferentes campa�as, por favor finalice la compra actual y vuelva a seleccionar este producto. Muchas Gracias.
.</b></td>
                    <?
                }
                else
                {
                    ?>
                      <td>
                            <select id="cbxCantidad" name="cbxCantidad">
                              <!--<?
                                for($i=1; $i<=$objCampania->getMaxProd(); $i++)
                                {
                                    ?>
                                        <option value="<?=$i?>"><?=$i?></option>
                                    <?                            
                                }
                              ?>-->
                            </select>
</td>
              <td><img src="<?=UrlResolver::getImgBaseUrl("front/images/boton-agregar.gif");?>" alt="Agregar al Carrito" width="131" height="20" /></td>
                    <?
                }
            ?>
            </tr>
          </table>
		  
			<?
            if(Aplicacion::VerificarCampaniaCarrito($objCampania->getIdCampania()))
            	{
            ?>
	      			<h4>INDICA LA CANTIDAD QUE DESEAS ADQUIRIR, LUEGO PRESIONA EL BOTON PARA AGREGAR EL PRODUCTO SELECCIONADO A TU CARRITO DE COMPRAS.</h4>
			<?                            
 				}
            ?>
            <br>
            </form>
            

            
		    </td>
  </tr>
  <tr>
    <td colspan="2">
    <div class="catalogodescr">
	    <h3>Descripci�n:</h3>
	    <p><?=$oProducto->getDescripcionAmpliada()?></p>
	  </div>
    </td>
  </tr>
</table>    
<?
    GenerarMatrizJS($_GET["IdProducto"]);
    HTMLCrearCombos_x_IdProducto($_GET["IdProducto"]);
?>
<tr>
  	<td colspan="2">
Recomendar producto a:
                <form target="iRecomendar" id="frmRecomendar" name="frmRecomendar" method="post" action="<?=Aplicacion::getRootUrl() . $postURL["recomendaramigo"]?>">
                <input type="text" id="txtEmailAmigo" name="txtEmailAmigo" />
                <img src="<?=UrlResolver::getImgBaseUrl("front/images/boton-enviar.gif");?>" />
                <input type="hidden" id="IdProducto" name="IdProducto" value="<?=$oProducto->getIdProducto()?>" />
                <input type="hidden" id="IdCampania" name="IdCampania" value="<?=$objCampania->getIdCampania()?>" />
                <input type="hidden" id="IdCategoria" name="IdCategoria" value="<?=$_GET["IdCategoria"];?>" />
                <br/>
            </form>
            <iframe id="iRecomendar" name="iRecomendar" style="display:none;"></iframe>
                </td>
  </tr>
	</div>  <div id="menuleft">
    <div class="item-list">
	Menu
    </div>
    <img src="../../front/campanias/archivos/campania_<?=$_GET["IdCampania"]?>/imagenes/<?=$arrArchivos["thumb_catalogo_imagen"]?>" class="logo-prod" />
	</div>
</div>
  <div id="contenido-bot"></div>
</div>

				<script type="text/javascript">
					var imgid = 'image';
					var imgdir = "<?=UrlResolver::getImgBaseUrl("front/productos/productos_".$oProducto->getIdProducto());?>";
					var prodid = '<?=$oProducto->getIdProducto()?>';
					var howmany = '<? echo $e ?>';
					var imgext = '.jpg';
					var thumbid = 'thumbs';
					var auto = false;
					var autodelay = 5;
				</script>
				<script src="<?=UrlResolver::getJsBaseUrl("front/catalogo/js/slide.js");?>" type="text/javascript"></script>
					
</body>
 </html>
