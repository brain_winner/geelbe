<?
	try
	{
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("catalogo"));
		$postURL = Aplicacion::getIncludes("post", "productos");
		
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php';
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/urlshorter/UrlShorterApiCall.php";	
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
    	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/banners/clases/clsbanners.php");
    	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/banners/datamappers/dmbanners.php");

		if(isset($_GET['codact']))
			$_SESSION['codact'] = $_GET['codact'];

	}
	catch(exception $e)
	{
		die(print_r($e));
	}
	
	    
    $validbo = false;
    
    if ($validbo) { // Modo Test
    	try {
			$isPageOk = true;
			$pageException = "Exception";
			
			// Validaciones de parametros basicas
			if(!isset($_GET["IdCampania"]) && !isset($_GET["IdProducto"])) { // IdCampania y IdProducto Seteado
	            $isPageOk = false;
				$pageException = "IdCampania or IdProducto not setted";
				echo "IdCampania or IdProducto not setted";
			}
			else if (!ValidatorUtils::validateNumber($_GET["IdCampania"])) { // IdCampania Numerico
				$isPageOk = false;
				$pageException = "IdCampania is not a number";
			}
			else if (!ValidatorUtils::validateNumber($_GET["IdProducto"])) { // IdProducto Numerico
				$isPageOk = false;
				$pageException = "IdProducto is not a number";
			}
			else if (!dmProductos::isCampaignProduct($_GET["IdProducto"], $_GET["IdCampania"])) { // IdProducto Correspondiente con IdCampania
				$isPageOk = false;
				$pageException = "IdProducto no corresponde con IdCampania";
			}
			else if (isset($_GET["IdCategoria"])) { 
				if (!ValidatorUtils::validateNumber($_GET["IdCategoria"])) { // IdCategoria Numerico
					$isPageOk = false;
					$pageException = "IdCategoria is not a number";
				}
				else if (!dmCampania::isCampaignCategory($_GET["IdCampania"], $_GET["IdCategoria"])) { // IdCategoria Correspondiente con IdCampania
					$isPageOk = false;
					$pageException = "IdCampania no corresponde con IdCategoria";
				}
			}
			
			if (!$isPageOk) {
				header("Location: /".$confRoot[1].'/front/login/');
				exit;
			}
	
	
		}
	    catch(Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
			header('HTTP/1.1 404 Not Found');
			include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
			exit;
	    }
    }
    else {
		try {
			$isPageOk = true;
			$pageException = "Exception";
			
			// Validaciones de parametros basicas
			if(!isset($_GET["IdCampania"]) && !isset($_GET["IdProducto"])) { // IdCampania y IdProducto Seteado
	            $isPageOk = false;
				$pageException = "IdCampania or IdProducto not setted";
				echo "IdCampania or IdProducto not setted";
			}
			else if (!ValidatorUtils::validateNumber($_GET["IdCampania"])) { // IdCampania Numerico
				$isPageOk = false;
				$pageException = "IdCampania is not a number";
			}
			else if (!ValidatorUtils::validateNumber($_GET["IdProducto"])) { // IdProducto Numerico
				$isPageOk = false;
				$pageException = "IdProducto is not a number";
			}
			else if (!dmProductos::isCampaignProduct($_GET["IdProducto"], $_GET["IdCampania"])) { // IdProducto Correspondiente con IdCampania
				$isPageOk = false;
				$pageException = "IdProducto no corresponde con IdCampania";
			}
			else if (!dmCampania::EsCampaniaAccesible($_GET["IdCampania"])) { // IdCampania Accesible
				$isPageOk = false;
				$pageException = "Campa�a not accesible";
			}
			else if (isset($_GET["IdCategoria"])) { 
				if (!ValidatorUtils::validateNumber($_GET["IdCategoria"])) { // IdCategoria Numerico
					$isPageOk = false;
					$pageException = "IdCategoria is not a number";
				}
				else if (!dmCampania::isCampaignCategory($_GET["IdCampania"], $_GET["IdCategoria"])) { // IdCategoria Correspondiente con IdCampania
					$isPageOk = false;
					$pageException = "IdCampania no corresponde con IdCategoria";
				}
			}
			
			if (!$isPageOk) {
				throw new Exception($pageException);
			}
	
	
		}
	    catch(Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
			header('HTTP/1.1 404 Not Found');
			include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
			exit;
	    }
    }
    
    require_once($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/logica/micarrito/actionform/productos.php');
  
	$dir = "../../front/productos/productos_".$_GET['IdProducto']."/thumbs/";
	$imgPath = UrlResolver::getImgBaseUrl("front/productos/productos_".$_GET['IdProducto']."/thumbs/");
	$arrImg = DirectorytoArray($dir, 1, array("jpg", "jpeg", "gif", "png"));

	$largeDir = "../../front/productos/productos_".$_GET['IdProducto']."/";
	$largeImgPath = UrlResolver::getImgBaseUrl("front/productos/productos_".$_GET['IdProducto']."/");
	$arrImg = DirectorytoArray($largeDir, 1, array("jpg", "jpeg", "gif", "png"));
	$firstImg = current($arrImg);
		
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts(); ?>
	<script src="<?=UrlResolver::getJsBaseUrl("front/catalogo/js/js.js");?>" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
	<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe_new.css");?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getCssBaseUrl("front/highslide.css")?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getCssBaseUrl("front/botones.css")?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getCssBaseUrl("front/catalogo/css/slide_new.css")?>" rel="stylesheet" type="text/css" />
	
	<link href="<?=UrlResolver::getCssBaseUrl("front/catalogo/css/producto_2011.css")?>" rel="stylesheet" type="text/css" />
	
	<script src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.2.min.js");?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getJsBaseUrl("front/catalogo/js/jquery.zoom-min.js");?>" type="text/javascript"></script>
	<meta property="og:image" content="<?=$largeImgPath.$firstImg?>"/>
	<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>             
	<? require("../menuess/menu_sin.php") ?>
 	<script type="text/javascript">
		fechaHoy = "<?=date("m/d/Y H:i:s");?>";
	</script>
<? 
   
    $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
	$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 300), array('cache_dir' => $cacheDir));
	$nombreCache = "cache_new_producto_public_".$_GET["IdProducto"]."_campania_".$_GET["IdCampania"]."_categoria_".$_GET["IdCategoria"];
$reloadCache = false;

		try
		{
			$objCampania = dmCampania::getByIdCampania($_GET["IdCampania"]);
			$dtCategorias = dmCategoria::getCategorias($_GET["IdCampania"], (!isset($_GET["IdCategoria"]))?"IS NULL":" = ".$_GET["IdCategoria"]);
			$dtAllCategorias = dmCategoria::getCategorias($_GET["IdCampania"],"IS NULL");
			if (isset($_GET["IdCategoria"]))
				$oCategoria = dmCategoria::getByIdCategoria($_GET["IdCategoria"]);
			
			$oProducto = dmProductos::getById($_GET["IdProducto"]);
			$arrArchivos = DirectorytoArray(Aplicacion::getRoot()."/front/campanias/archivos/campania_".$_GET["IdCampania"]."/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));
			$reloadCache = true;
			
			//Banners
	       $cuerpo = dmBanners::getProductoCuerpo($_GET['IdCampania'], $_GET['IdProducto']);
	       $sidebar = dmBanners::getProductoSidebar($_GET['IdCampania'], $_GET['IdProducto']);
				
		}
		catch(exception $e)
		{
			$cache->remove($nombreCache);
			?>
			<script>window.open('../accesodenegado/accesodenegado.php?errNo=<?=Aplicacion::Encrypter($e->getCodigo())?>&r=<?=Aplicacion::Encrypter($e->getTitulo())?>','_self');</script>
			<?
		}  
?>

<script>                                                                                                
    var PrecioBase = "<?=$oProducto->getPVenta()?>";
    var StockBase = 0;
</script> 

<div id="container" style="margin:0 auto">

        <?php $bannerDisplay = ($cuerpo != null && count($cuerpo) > 0)?"":";display:none"; ?>
        <div id="bannerTop" style="margin-bottom: 10px<?=$bannerDisplay?>">
        <?php
            foreach($cuerpo as $i => $c)
                if(isset($c['img']))
                    echo '<div style="text-align:center;"><img src="'.UrlResolver::getImgBaseUrl('front/archivos/banners/'.$c['id'].'/'.$c['img']).'" /></div>';
        ?>
       </div>

<!-- head de campa�a -->
		<div id="camp-head" class="pag-producto">
			<?php $logoCamp = (isset($arrArchivos["logo2"]) ? $arrArchivos["logo2"] : $arrArchivos["thumb_logo"]); ?>
			<div id="camp-logo"><a href="index_public.php?IdCampania=<?=$objCampania->getIdCampania()?>"><img src="<?=UrlResolver::getImgBaseUrl('front/campanias/archivos/campania_'.$_GET["IdCampania"].'/imagenes/'.$logoCamp)?>" width="140" alt="" /></a></div>

<!-- men� de categor�as -->
			<div id="path">                
            </div>
<!-- fin men� de categor�as -->

			<hr class="separator-producto" />
		</div>
<!-- fin head de campa�a -->

		<div id="detalle-prod">
        	<div id="prod-gallery">

				  <div id="imagearea">
				    <div id="image">
						<?
						if (!count($arrImg) > 0)
						{
							?>
								<a href="#"><img id="imgPrincipal" name="imgPrincipal" src="<?=UrlResolver::getImgBaseUrl("front/productos/noimagen.jpg");?>" alt="" border="1" /></a>
							<?
						}
						?>
				    </div>
				  </div>
				  <div id="thumbwrapper">
				  <span id="left_"></span>
					  <span id="right_"></span>
				    <div id="thumbarea">
					  <ul id="thumbs">
						<?                        
                            for($i=0; $i<count($arrImg); $i++)
                              {
                                 if ($arrImg[$i])
									{
                                       ?>
                                          <li value="<? $imgNumbers = $i; $imgNumbers++; echo $imgNumbers;?>"><img id="<?=$imgPath.$arrImg[$i]?>" src="<?=$imgPath.$arrImg[$i]?>" alt="" /></li><?
                                    }
                                }
                            ?>			
				      </ul>
				    </div>
				  </div>
				</div>

          <div id="prod-attributes">
            	<span class="prodname"><?=$oProducto->getNombre()?></span>
                <?php
                	$porcDescuento = number_format((1 - ($oProducto->getPVenta() / $oProducto->getPVP()))*100, 0, ',', '.');
	        		if($porcDescuento >= 20) {
    			?>
            	<span class="porc-desc">(<?=$porcDescuento?>% OFF)</span>
                <?php
	        		}
    			?>
    			
    			
    			<div id="register-prodnl">
				  <div id="reg-tit" class="registro-txt">
				    REGISTRATE AHORA Y ACCED� A ESTE DESCUENTO POR
				    <br>
				    <strong>�TIEMPO LIMITADO!</strong>
				  </div>
				  <div id="reg-face">
				    <a href="http://<?=$_SERVER["SERVER_NAME"]?>/appfacebook/register_no_popup.php?country=co&codact=facebooklogin&IdProducto=<?=$oProducto->getIdProducto()?>&IdCampania=<?=$_GET['IdCampania']?>&IdCategoria=<?=$_GET['IdCategoria']?>"><img src="images/face-bt.jpg" width="186" height="45" border="0"></a>
				  </div>
				  <div id="reg-bt" class="reg-member">
				    <a href="http://<?=$_SERVER["SERVER_NAME"]?>/co/registro/index.html"><img src="images/registro-bt.jpg" width="130" height="45" border="0"></a>
				    <br>
				    Ya soy miembro. 
				    <a href="http://<?=$_SERVER["SERVER_NAME"]?>/co/front/login/">Ingresar</a>
				  </div>
				</div>
				
				<div id="facebookfaces-prodnl">
					<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fgeelbecolombia&send=false&layout=standard&width=402&show_faces=true&action=like&colorscheme=light&font=arial&height=80&appId=241492119282516" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:402px; height:80px;" allowtransparency="true"></iframe>
				</div>
    			
   </div>

			<!-- inicio bloque descripcion -->
            <div id="description">

            	<div id="social"> <!-- redes sociales -->
                	<img class="descripcion" src="<?=UrlResolver::getImgBaseUrl("front/catalogo/images/compartir.jpg")?>" />
            		<a target="javascript:;" class="share mail" id="shareWithMailTrigger"><img onMouseOver="this.src='<?=UrlResolver::getImgBaseUrl("front/catalogo/images/share_mail_on.jpg")?>'" onMouseOut="this.src = '<?=UrlResolver::getImgBaseUrl("front/catalogo/images/share_mail_off.jpg")?>'" src="<?=UrlResolver::getImgBaseUrl("front/catalogo/images/share_mail_off.jpg")?>"></a>
            		<a target="_blank" class="share twitter twitter-share-button" href="http://twitter.com/share" style="cursor:pointer;" data-url="<?=UrlShorterApiCall::getShortUrl(UrlResolver::getBaseURL("front/catalogo/detalle_public.php?IdCampania=".$objCampania->getIdCampania()."&IdCategoria=".$_GET['IdCategoria']."&IdProducto=".$oProducto->getIdProducto()."&codact=".$userEmail));?>" data-text="Me gust&oacute; este producto de <?=$objCampania->getNombre()?> en @geelbeco: " data-count="none" data-lang="es"><img src="<?=UrlResolver::getImgBaseUrl("front/catalogo/images/social_tw.jpg")?>"></a>
            		<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
            		<iframe frameborder="0" scrolling="no" allowtransparency="true" style="border: medium none; overflow: hidden; width: 190px; height: 20px; top: 4px; position: relative;" src="http://www.facebook.com/plugins/like.php?href=<?=urlencode(UrlResolver::getBaseURL("front/catalogo/detalle_public.php?IdCampania=".$objCampania->getIdCampania()."&IdCategoria=".$_GET['IdCategoria']."&IdProducto=".$oProducto->getIdProducto()));?>&amp;layout=button_count&amp;show_faces=false&amp;width=100&amp;action=like&amp;font&amp;colorscheme=light&amp;height=21&amp;share=true"></iframe>

	                <div id="recommendByMail" style="margin-top:10px;display:none;">
					  <table cellspacing="0" cellpadding="0">
					    <tr>
						  <td colspan="2">
						    <p>Recomendar producto a:</p>
					      </td>
						</tr>
						<tr>
						  <td>
						    <form style="margin-bottom:0px;" target="iRecomendar" id="frmRecomendar" name="frmRecomendar" method="post" action="<?=Aplicacion::getRootUrl() . $postURL["recomendaramigo"]?>">
							  <input type="text" id="txtEmailAmigo" name="txtEmailAmigo" />
							  <input type="hidden" id="IdProducto" name="IdProducto" value="<?=$oProducto->getIdProducto()?>" />
							  <input type="hidden" id="IdCampania" name="IdCampania" value="<?=$objCampania->getIdCampania()?>" />
							  <input type="hidden" id="IdCategoria" name="IdCategoria" value="<?=$_GET["IdCategoria"];?>" />
							</form>
						  </td>
						  <td style="padding:0px;">
						    <button type="button" onclick="$('#frmRecomendar').submit()">
							  <span>Enviar</span>
				            </button>
				            <script type="text/javascript">
				            	$('#frmRecomendar').bind('submit', function() {
				            		var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
				            		if(!pattern.test($('#txtEmailAmigo').val())) {
				            			alert('Ingresa una direcci�n de e-mail v�lida.');
				            			return false;
				            		}
				            	})
				            </script>
						  </td>
  						</tr>
				      </table>
					  <iframe id="iRecomendar" name="iRecomendar" style="display:none;"></iframe>
                    </div>
                </div> <!-- fin redes sociales -->
            </div>
			<!-- fin bloque descripcion -->
            
            <div id="description-tabs">
            	<ul>
                	<li>DESCRIPCI&Oacute;N</li>
                </ul>
            </div>
  
            <div id="description-body">
				<p><?=htmlspecialchars_decode($oProducto->getDescripcionAmpliada())?></p>
            </div>

        </div>


</div>

				<script type="text/javascript">
					var imgid = 'image';
					var imgdir = '<?=UrlResolver::getImgBaseUrl("front/productos/productos_".$oProducto->getIdProducto());?>';
					var prodid = '<?=$oProducto->getIdProducto()?>';
					var howmany = "<?=$imgNumbers?>";
					var imgext = '.jpg';
					var thumbid = 'thumbs';
					var auto = false;
					var autodelay = 5;
				</script>

				<script src="<?=UrlResolver::getJsBaseUrl("front/catalogo/js/slide.js");?>" type="text/javascript"></script>
<? require("../menuess/footerNew.php");?>
				
</body>

<script>
$("#botonshare").click(function(event) {
	if($("#share").hasClass("mostrar")) {
		$("#share").removeClass("mostrar");
	} else {
		$("#share").addClass("mostrar");
	}
});
$("#shareWithFacebookTrigger").click(function(event) {
	$("#recommendByMail").hide();
	newwindow = window.open("http://www.facebook.com/sharer.php?u=<?=urlencode($publicPageUrl)?>", "Facebook", "height=440, width=620, scrollbars=true");
	if (window.focus) {
		newwindow.focus();
	}
	return false;
});
$("#shareWithTwitterTrigger").click(function(event) {
	$("#recommendByMail").hide();
	newwindow = window.open("http://twitter.com/home?status=<?=urlencode($publicPageUrl)?>", "Twitter", "height=650, width=1024, scrollbars=true");
	if (window.focus) {
		newwindow.focus();
	}
	return false;
});
$("#shareWithMailTrigger").click(function(event) {
	$("#recommendByMail").slideToggle('slow', function() {
});
});

</script>


 </html>
