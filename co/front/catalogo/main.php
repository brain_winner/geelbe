<?php
try {
    $confRoot = explode("/", dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("catalogo"));


    set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
    require_once 'Zend/Cache.php';
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica_bo/validators/ValidatorUtils.php");
} catch (Exception $e) {
    die(print_r($e));
}

$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 300), array('cache_dir' => $cacheDir));

$validbo = false;
try {
    ValidarUsuarioLogueadoBo(59);
    $validbo = true;
} catch (Exception $e) {
    $validbo = false;
}
$validbo = false; //*This was working well, don´t know if the change that vurbia has been doing something affect / Jose*/
if ($validbo) { // Modo Test
    try {
        $isPageOk = true;
        $showAll = false;
        $pageException = "Exception";

        // Validaciones de parametros basicas
        if (!isset($_GET["IdCampania"])) { // IdCampania Seteado
            $isPageOk = false;
            $pageException = "IdCampania not setted";
        } else if (!ValidatorUtils::validateNumber($_GET["IdCampania"])) { // IdCampania Numerico
            $isPageOk = false;
            $pageException = "IdCampania is not a number";
        } else if (isset($_GET["IdCategoria"])) {
            if (!ValidatorUtils::validateNumber($_GET["IdCategoria"])) { // IdCategoria Numerico
                $isPageOk = false;
                $pageException = "IdCategoria is not a number";
            } else if (!dmCampania::isCampaignCategory($_GET["IdCampania"], $_GET["IdCategoria"])) { // IdCategoria Correspondiente con IdCampania
                $isPageOk = false;
                $pageException = "IdCampania no corresponde con IdCategoria";

                if ($_GET["IdCategoria"] == -1) {
                    $isPageOk = true;
                    $showAll = true;
                }
            }
        }

        $objCampania = dmCampania::getByIdCampania($_GET["IdCampania"]);
        if ($objCampania->getWelcome() == 0) {
            header("Location: index.php?IdCampania=" . $_GET["IdCampania"]);
            die;
        }

        if (!$isPageOk) {
            throw new Exception($pageException);
        }
    } catch (Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
        header('HTTP/1.1 404 Not Found');
        include($_SERVER['DOCUMENT_ROOT'] . '/' . $confRoot[1] . '/front/error/404.php');
        exit;
    }
} else {
   
    try {
        
        ValidarUsuarioLogueado();
    } catch (Exception $e) {
        echo $e;
        header('Location: /' . $confRoot[1] . '/front/login/index_campania.php?IdCampania=' . $_GET['IdCampania']);
        exit;
    }
    try {
        $isPageOk = true;
        $showAll = false;
        $pageException = "Exception";

        if (!$esAccesible = $cache->load("campania_accesible_" . $_GET["IdCampania"])) {
            $esAccesible = (string) dmCampania::EsCampaniaAccesible($_GET["IdCampania"]);
            $cache->save($esAccesible, "campania_accesible_" . $_GET["IdCampania"], array('group_cache_catalogo_campania_' . $_GET["IdCampania"]));
        }

        // Validaciones de parametros basicas
        if (!isset($_GET["IdCampania"])) { // IdCampania Seteado
            $isPageOk = false;
            $pageException = "IdCampania not setted";
        } else if (!ValidatorUtils::validateNumber($_GET["IdCampania"])) { // IdCampania Numerico
            $isPageOk = false;
            $pageException = "IdCampania is not a number";
        } else if (!$esAccesible) { // IdCampania Accesible
            $isPageOk = false;
            $pageException = "Campa�a not accesible";
        } else if (isset($_GET["IdCategoria"])) {
            if (!$esCategoriaDeCampania = $cache->load("es_categoria_de_campania_" . $_GET["IdCampania"] . "_categoria_" . $_GET["IdCategoria"])) {
                $esCategoriaDeCampania = (string) dmCampania::isCampaignCategory($_GET["IdCampania"], $_GET["IdCategoria"]);
                $cache->save($esCategoriaDeCampania, "es_categoria_de_campania_" . $_GET["IdCampania"] . "_categoria_" . $_GET["IdCategoria"], array('group_cache_catalogo_campania_' . $_GET["IdCampania"]));
            }

            if (!ValidatorUtils::validateNumber($_GET["IdCategoria"])) { // IdCategoria Numerico
                $isPageOk = false;
                $pageException = "IdCategoria is not a number";
            } else if (!$esCategoriaDeCampania) { // IdCategoria Correspondiente con IdCampania
                $isPageOk = false;
                $pageException = "IdCampania no corresponde con IdCategoria";

                if ($_GET["IdCategoria"] == -1) {
                    $isPageOk = true;
                    $showAll = true;
                }
            }
        }

        if (!$sinWelcomePage = $cache->load("sin_welcome_page" . $_GET["IdCampania"])) {
            $objCampania = dmCampania::getByIdCampania($_GET["IdCampania"]);
            $sinWelcomePage = (string) ($objCampania->getWelcome() == 0);
            $cache->save($sinWelcomePage . "", "sin_welcome_page" . $_GET["IdCampania"], array('group_cache_catalogo_campania_' . $_GET["IdCampania"]));
        }

        if ($sinWelcomePage) {
            header("Location: index.php?IdCampania=" . $_GET["IdCampania"]);
            die;
        }

        if (!$isPageOk) {
            throw new Exception($pageException);
        }
    } catch (Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
        header('HTTP/1.1 404 Not Found');
        include($_SERVER['DOCUMENT_ROOT'] . '/' . $confRoot[1] . '/error/error404.html');
        exit;
    }
}

//Obtener los datos

$nombreCache = "";
if (isset($_GET["IdCategoria"])) {
    $nombreCache = 'cache_catalogo_campania_' . $_GET["IdCampania"] . '_cat_' . $_GET["IdCategoria"] . '_maxxpag_' . (isset($_POST['MAX_X_PAG']) ? $_POST['MAX_X_PAG'] : Aplicacion::getParametros("paginador", "iniciar")) . '_pag_' . (isset($_POST['PAG']) ? $_POST['PAG'] : 1);
} else {
    $nombreCache = 'cache_catalogo_campania_' . $_GET["IdCampania"] . '_maxxpag_' . (isset($_POST['MAX_X_PAG']) ? $_POST['MAX_X_PAG'] : Aplicacion::getParametros("paginador", "iniciar")) . '_pag_' . (isset($_POST['PAG']) ? $_POST['PAG'] : 1);
}

try {
    if (!$contenidoCache = $cache->load($nombreCache)) {

        if (isset($_GET["IdCategoria"]))
            $oCategoria = dmCategoria::getByIdCategoria($_GET["IdCategoria"]);
        $objCampania = dmCampania::getByIdCampania($_GET["IdCampania"]);
        $dtCategorias = dmCategoria::getCategorias($_GET["IdCampania"], (!isset($_GET["IdCategoria"]) ? "IS NULL" : " = " . $_GET["IdCategoria"]));
        $dtAllCategorias = dmCategoria::getCategorias($_GET["IdCampania"], "IS NULL");
        $arrArchivos = DirectorytoArray(Aplicacion::getRoot() . "front/campanias/archivos/campania_" . $_GET["IdCampania"] . "/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));

        //Redireccionar si hay un solo producto
        extract($_POST);
        if ($_GET["IdCategoria"]) {
            array_push($dtCategorias, array("IdCategoria" => $_GET["IdCategoria"], "Nombre" => "", "Descripcion" => "", "IdCampania" => $_GET["IdCampania"], "IdPadre" => 0, "Orden" => 0, "subCategoria" => array()));
            if (isset($oCategoria)) {
                for ($j = 0; $j < count($dtCategorias); $j++) {
                    $dtCategorias[$j]["IdOrdenamiento"] = $oCategoria->getIdOrdenamiento();
                }
            }
            //$dtCategorias[0]["IdOrdenamiento"] = $oCategoria->getIdOrdenamiento();
            //array_push($dtCategorias, array("IdOrdenamiento" => $oCategoria->getIdOrdenamiento()));
        }

        $ProductosSinPaginar = dmProductos::getProductosOrdenados($dtCategorias);

        /* if (isset($oCategoria)) {
          $oOrdenamiento = dmOrdenamientos::getById($oCategoria->getIdOrdenamiento());
          $ProductosSinPaginar = OrdenarMatriz($ProductosSinPaginar, $oOrdenamiento->getCampo(), $oOrdenamiento->getOrdenamiento());
          }
          else {
          $ProductosSinPaginar = OrdenarMatriz($ProductosSinPaginar, 'PVenta', 'ASC');
          } */

        if (count($ProductosSinPaginar) == 1) {
            header("Location: detalle.php?IdCampania=" . $objCampania->getIdCampania() . "&IdCategoria=" . dmProductos::getCategoria_byIdProducto($ProductosSinPaginar[0]["IdProducto"], $objCampania->getIdCampania()) . "&IdProducto=" . $ProductosSinPaginar[0]["IdProducto"]);
            die;
        }
    } // Fin de cache
} catch (exception $e) {
    $cache->remove($nombreCache);
    ?>
    <script>window.open('../accesodenegado/accesodenegado.php?errNo=<?= Aplicacion::Encrypter($e->getCodigo()) ?>&r=<?= Aplicacion::Encrypter($e->getTitulo()) ?>','_self');</script>
    <?
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <? Includes::Scripts(); ?>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?= Aplicacion::getParametros("info", "nombre"); ?> </title>
        <link href="<?= UrlResolver::getCssBaseUrl("front/geelbe_new.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= UrlResolver::getCssBaseUrl("front/highslide.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= UrlResolver::getCssBaseUrl("front/botones.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= UrlResolver::getCssBaseUrl("front/catalogo/css/slide.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= UrlResolver::getCssBaseUrl("front/catalogo/css/producto_new.css") ?>" rel="stylesheet" type="text/css" />

        <script src="<?= UrlResolver::getJsBaseUrl("front/catalogo/js/js.js"); ?>" type="text/javascript"></script>

        <? include $_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica_bo/background/actionform/background.php"; ?>
    </head>
    <body>
        <? require("../menuess/menu_log.php") ?>
        <div id="container" style="position:relative;">
            <script type="text/javascript">
                fechaHoy = "<?= date("m/d/Y H:i:s"); ?>";
            </script>
            <?
            if (!$contenidoCache = $cache->load($nombreCache)) {
                ob_start();
                ?>
                <?
                $IdLanCampaign = dmCampania::getIdLandingDeCampania($_GET["IdCampania"]);

                if ($validbo) { // Modo Test
                    $userEmail = Aplicacion::Decrypter($_SESSION["BO"]["User"]["email"]);
                } else {
                    $userEmail = Aplicacion::Decrypter($_SESSION["User"]["email"]);
                }

                if (isset($IdLanCampaign)) {
                    $UrlLanding = UrlResolver::getBaseUrl("registro/marca/marca_index.php?id=" . $IdLanCampaign . "&codact=appfacebook-likecampaign1");
                } else {
                    $UrlLanding = UrlResolver::getBaseUrl("registro/index.html?codact=appfacebook-likecampaign1");
                }
                ?>

                <div id="contenido-top"></div>
                <div id="contenido">
                    <div>	
                        <div id="topright_new" >
                            <!-- est�s en<div class="col_izquierda"><strong>Est�s en:</strong> <?= dmCategoria::getEstasEn($oCategoria ? $oCategoria->getIdCategoria() : null, $objCampania->getIdCampania()) ?></div> -->
                            <div class="contador"><?= $objCampania->getNombre() ?> Finaliza <strong><div id="reloj"><script>IniciarReloj("reloj", "<?= ParserSTR::Timestamp_to_date(ParserSTR::Date_to_timestamp($objCampania->getFechaFin()), "m/d/Y H:i"); ?>", fechaHoy);</script></div></strong></div>

                        </div> 
                        <div class="unfloat"></div>
                    </div>
                    <div style="background-image: url('<?= UrlResolver::getImgBaseUrl("front/campanias/archivos/campania_" . $_GET['IdCampania'] . "/imagenes/" . $arrArchivos["welcome"]) ?>'); min-height: 380px; overflow:auto;height:380px;">
                       
                        
                        <div id="central">
                          <!-- est�s en <p><strong>Est�s en: </strong><a href=""><?= dmCategoria::getEstasEn($oCategoria ? $oCategoria->getIdCategoria() : null, $objCampania->getIdCampania()) ?></a></p> -->
                        </div>
                        <div id="menuleft" style="position:relative;">
                            <div style="width:100%; background-color:#FFFFFF;">
                                <?php $imgLogo = (isset($arrArchivos["logo2"]) ? $arrArchivos["logo2"] : $arrArchivos["thumb_logo"]); ?>
                                <img src="<?= UrlResolver::getImgBaseUrl("front/campanias/archivos/campania_" . $_GET["IdCampania"] . "/imagenes/" . $imgLogo) ?>" width="140" alt="" />
                            </div>

                            <div class="item-list" id="categories">
                                <?
                                $objCategoriaLateral = new Categoria_Lateral("divCategorias", $dtAllCategorias);
                                $objCategoriaLateral->setIdCategoria_Seleccionada(-2);
                                $objCategoriaLateral->setPagina("index.php");
                                $objCategoriaLateral->setAdicionalGet("&IdCampania=" . $_GET["IdCampania"]);
                                $objCategoriaLateral->show();
                                ?>
                            </div>
                            <div class="campanafoto">

                            </div>


                            <div style="margin-top:15px;">
                                &nbsp;
                            </div>
                            <?
                            $contenidoCache = ob_get_clean();
                            $cache->save($contenidoCache, $nombreCache, array('group_cache_catalogo_campania_' . $_GET["IdCampania"]));
                        }
                        echo $contenidoCache;
                        ?>
                        <?
                        $contenidoCache = "";
                        if (!$contenidoCache = $cache->load($nombreCache . "_2")) {
                            ob_start();
                            ?>					
                        </div>
                        <!-- div style="clear:both;">&nbsp;</div -->
                    </div>
                     <div>
                            &nbsp;
                        </div>
                    <div style="text-align: right">
                        <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.geelbe.com%2Fco%2Ffront%2Flogin%2Findex_campania.php%3FIdCampania%3D<?php echo $_GET["IdCampania"]; ?>&amp;width=100&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;width: 120px" allowTransparency="true"></iframe>
                       </div>
                   
                    <div style=" clear:both;">&nbsp;</div>
                </div>
                <div style=" clear:both;"></div>
                <div id="contenido-bot"></div>
                <?
                $contenidoCache = ob_get_clean();
                $cache->save($contenidoCache, $nombreCache . "_2", array('group_cache_catalogo_campania_' . $_GET["IdCampania"]));
            }
            echo $contenidoCache;
            
            ?>
        </div>
        <? require("../menuess/footerNew.php") ?>
    </body>
</html>