<?
try {
    $confRoot = explode("/", dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("catalogo"));
    $postURL = Aplicacion::getIncludes("post", "productos");

    set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
    require_once 'Zend/Cache.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica/urlshorter/UrlShorterApiCall.php";
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica_bo/validators/ValidatorUtils.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica_bo/banners/clases/clsbanners.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica_bo/banners/datamappers/dmbanners.php");
} catch (exception $e) {
    die(print_r($e));
}


$validbo = false;
try {
    ValidarUsuarioLogueadoBo(9);
    $validbo = true;
} catch (Exception $e) {
    $validbo = false;
}

try {
    ValidarUsuarioLogueadoBo(59); //nuevas campañas
    $validbo = true;
} catch (Exception $e) {
    $validbo = false;
}

if ($validbo) { // Modo Test
    try {
        $isPageOk = true;
        $pageException = "Exception";

        // Validaciones de parametros basicas
        if (!isset($_GET["IdCampania"]) && !isset($_GET["IdProducto"])) { // IdCampania y IdProducto Seteado
            $isPageOk = false;
            $pageException = "IdCampania or IdProducto not setted";
            echo "IdCampania or IdProducto not setted";
        } else if (!ValidatorUtils::validateNumber($_GET["IdCampania"])) { // IdCampania Numerico
            $isPageOk = false;
            $pageException = "IdCampania is not a number";
        } else if (!ValidatorUtils::validateNumber($_GET["IdProducto"])) { // IdProducto Numerico
            $isPageOk = false;
            $pageException = "IdProducto is not a number";
        } else if (!dmProductos::isCampaignProduct($_GET["IdProducto"], $_GET["IdCampania"])) { // IdProducto Correspondiente con IdCampania
            $isPageOk = false;
            $pageException = "IdProducto no corresponde con IdCampania";
        } else if (isset($_GET["IdCategoria"])) {
            if (!ValidatorUtils::validateNumber($_GET["IdCategoria"])) { // IdCategoria Numerico
                $isPageOk = false;
                $pageException = "IdCategoria is not a number";
            } else if (!dmCampania::isCampaignCategory($_GET["IdCampania"], $_GET["IdCategoria"])) { // IdCategoria Correspondiente con IdCampania
                $isPageOk = false;
                $pageException = "IdCampania no corresponde con IdCategoria";
            }
        }

        if (!$isPageOk) {
            throw new Exception($pageException);
        }
    } catch (Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
        header('HTTP/1.1 404 Not Found');
        include($_SERVER['DOCUMENT_ROOT'] . '/' . $confRoot[1] . '/front/error/404.php');
        exit;
    }
} else {
    try {
        ValidarUsuarioLogueado();
    } catch (Exception $e) {
        echo $e;
        header('Location: /' . $confRoot[1] . '/front/catalogo/detalle_public.php?' . $_SERVER['QUERY_STRING']);
        exit;
    }
    try {
        $isPageOk = true;
        $pageException = "Exception";

        // Validaciones de parametros basicas
        if (!isset($_GET["IdCampania"]) && !isset($_GET["IdProducto"])) { // IdCampania y IdProducto Seteado
            $isPageOk = false;
            $pageException = "IdCampania or IdProducto not setted";
            echo "IdCampania or IdProducto not setted";
        } else if (!ValidatorUtils::validateNumber($_GET["IdCampania"])) { // IdCampania Numerico
            $isPageOk = false;
            $pageException = "IdCampania is not a number";
        } else if (!ValidatorUtils::validateNumber($_GET["IdProducto"])) { // IdProducto Numerico
            $isPageOk = false;
            $pageException = "IdProducto is not a number";
        } else if (!dmProductos::isCampaignProduct($_GET["IdProducto"], $_GET["IdCampania"])) { // IdProducto Correspondiente con IdCampania
            $isPageOk = false;
            $pageException = "IdProducto no corresponde con IdCampania";
        } else if (!dmCampania::EsCampaniaAccesible($_GET["IdCampania"])) { // IdCampania Accesible
            $isPageOk = false;
            $pageException = "Campaña not accesible";
        } else if (isset($_GET["IdCategoria"])) {
            if (!ValidatorUtils::validateNumber($_GET["IdCategoria"])) { // IdCategoria Numerico
                $isPageOk = false;
                $pageException = "IdCategoria is not a number";
            } else if (!dmCampania::isCampaignCategory($_GET["IdCampania"], $_GET["IdCategoria"])) { // IdCategoria Correspondiente con IdCampania
                $isPageOk = false;
                $pageException = "IdCampania no corresponde con IdCategoria";
            }
        }

        if (!$isPageOk) {
            throw new Exception($pageException);
        }
    } catch (Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
        header('HTTP/1.1 404 Not Found');
        include($_SERVER['DOCUMENT_ROOT'] . '/' . $confRoot[1] . '/front/error/404.php');
        exit;
    }
}
echo $contenidoCache;
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . $confRoot[1] . '/logica/micarrito/actionform/productos.php');
?> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <? Includes::Scripts(); ?>
        <script src="<?= UrlResolver::getJsBaseUrl("js/jquery-1.4.2.min.js"); ?>" type="text/javascript"></script>
        <script src="<?= UrlResolver::getJsBaseUrl("front/catalogo/js/js.js"); ?>" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?= Aplicacion::getParametros("info", "nombre"); ?> </title>
        <link href="<?= UrlResolver::getCssBaseUrl("front/geelbe_new.css"); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= UrlResolver::getCssBaseUrl("front/highslide.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= UrlResolver::getCssBaseUrl("front/botones.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= UrlResolver::getCssBaseUrl("front/catalogo/css/slide_new.css") ?>" rel="stylesheet" type="text/css" />

        <link href="<?= UrlResolver::getCssBaseUrl("front/catalogo/css/producto_2011.css") ?>" rel="stylesheet" type="text/css" />

        <script src="<?= UrlResolver::getJsBaseUrl("front/catalogo/js/jquery.zoom-min.js"); ?>" type="text/javascript"></script>

        <? include $_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica_bo/background/actionform/background.php"; ?>
    </head>
    <body>             
        <? require("../menuess/menu_log.php") ?>
        <script type="text/javascript">
            fechaHoy = "<?= date("m/d/Y H:i:s"); ?>";
        </script>
        <?
        $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
        $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 300), array('cache_dir' => $cacheDir));
        $nombreCache = "cache_new_producto_" . $_GET["IdProducto"] . "_campania_" . $_GET["IdCampania"] . "_categoria_" . $_GET["IdCategoria"];
        $reloadCache = false;

        try {
            if (!$contenidoCache = $cache->load($nombreCache)) {
                $objCampania = dmCampania::getByIdCampania($_GET["IdCampania"]);
                $dtCategorias = dmCategoria::getCategorias($_GET["IdCampania"], (!isset($_GET["IdCategoria"])) ? "IS NULL" : " = " . $_GET["IdCategoria"]);
                $dtAllCategorias = dmCategoria::getCategorias($_GET["IdCampania"], "IS NULL");
                if (isset($_GET["IdCategoria"]))
                    $oCategoria = dmCategoria::getByIdCategoria($_GET["IdCategoria"]);

                $oProducto = dmProductos::getById($_GET["IdProducto"]);
                $arrArchivos = DirectorytoArray(Aplicacion::getRoot() . "/front/campanias/archivos/campania_" . $_GET["IdCampania"] . "/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));
                $reloadCache = true;

                //Banners
                $cuerpo = dmBanners::getProductoCuerpo($_GET['IdCampania'], $_GET['IdProducto']);
                $sidebar = dmBanners::getProductoSidebar($_GET['IdCampania'], $_GET['IdProducto']);
            }
        } catch (exception $e) {
            $cache->remove($nombreCache);
            ?>
            <script>window.open('../accesodenegado/accesodenegado.php?errNo=<?= Aplicacion::Encrypter($e->getCodigo()) ?>&r=<?= Aplicacion::Encrypter($e->getTitulo()) ?>','_self');</script>
            <?
        }

        if ($reloadCache) {
            ob_start();
            $IdCodigoProdInterno = dmProductos::getIdCodigoProdInternoActual($oProducto->getIdProducto());
            $productInfo = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
            //echo 'Stock=' . $productInfo->Stock . ' - getMaxProd:' . $objCampania->getMaxProd();
            $actualStock=$productInfo->Stock;
            ?>

            <script>                                                                                                
                var PrecioBase = "<?= $oProducto->getPVenta() ?>";
                var StockBase = 0;
            </script> 

            <div id="container" style="margin:0 auto">

                <?php $bannerDisplay = ($cuerpo != null && count($cuerpo) > 0) ? "" : ";display:none"; ?>
                <div id="bannerTop" style="margin-bottom: 10px<?= $bannerDisplay ?>">
                    <?php
                    foreach ($cuerpo as $i => $c)
                        if (isset($c['img'])) {
                            echo '<div style="text-align:center;">';
                            if ($c['link'] != null)
                                echo '<a href="' . $c['link'] . '">';
                            echo '<img src="' . UrlResolver::getImgBaseUrl('front/archivos/banners/' . $c['id'] . '/' . $c['img']) . '" />';
                            if ($c['link'] != null)
                                echo '</a>';
                            echo '</div>';
                        }
                    ?>
                </div>

                <!-- head de campaña -->
                <div id="camp-head" class="pag-producto">
                    <?php $logoCamp = (isset($arrArchivos["logo2"]) ? $arrArchivos["logo2"] : $arrArchivos["thumb_logo"]); ?>
                    <div id="camp-logo"><a href="main.php?IdCampania=<?= $objCampania->getIdCampania() ?>"><img src="<?= UrlResolver::getImgBaseUrl('front/campanias/archivos/campania_' . $_GET["IdCampania"] . '/imagenes/' . $logoCamp) ?>" width="140" alt="" /></a></div>

                    <!-- menú de categorías -->
                    <div id="path">
                        <h2><?= $objCampania->getNombre() ?> finaliza en <strong><div id="reloj"><script>IniciarReloj("reloj", '<?= ParserSTR::Timestamp_to_date(ParserSTR::Date_to_timestamp($objCampania->getFechaFin()), "m/d/Y H:i"); ?>', fechaHoy);</script></div></strong></h2>
                        <hr/>
                        <div> <?= dmCategoria::getEstasEnNew($oCategoria ? $oCategoria->getIdCategoria() : null, $objCampania->getIdCampania()) ?></div>
                        <br/> 
                    </div>
                    <!-- fin menú de categorías -->

                    <hr class="separator-producto" />
                </div>
                <!-- fin head de campaña -->

                <div id="detalle-prod">
                    <div id="prod-gallery">

                        <div id="imagearea">
                            <div id="image">
                                <?
                                $dir = "../../front/productos/productos_" . $oProducto->getIdProducto() . "/thumbs/";
                                $imgPath = UrlResolver::getImgBaseUrl("front/productos/productos_" . $oProducto->getIdProducto() . "/thumbs/");
                                $arrImg = DirectorytoArray($dir, 1, array("jpg", "jpeg", "gif", "png"));
                                if (!count($arrImg) > 0) {
                                    ?>
                                    <a href="#"><img id="imgPrincipal" name="imgPrincipal" src="<?= UrlResolver::getImgBaseUrl("front/productos/noimagen.jpg"); ?>" alt="" border="1" /></a>
                                    <?
                                }
                                ?>
                            </div>
                        </div>
                        <div id="thumbwrapper">
                            <span id="left_"></span>
                            <span id="right_"></span>
                            <div id="thumbarea">
                                <ul id="thumbs">
                                    <?
                                    for ($i = 0; $i < count($arrImg); $i++) {
                                        if ($arrImg[$i]) {
                                            ?>
                                            <li value="<?
                                $imgNumbers = $i;
                                $imgNumbers++;
                                echo $imgNumbers;
                                            ?>"><img id="<?= $imgPath . $arrImg[$i] ?>" src="<?= $imgPath . $arrImg[$i] ?>" alt="" /></li><?
                                }
                            }
                                    ?>			
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?
                    $validbo = false;
                    try {
                        ValidarUsuarioLogueadoBo(9);
                        $validbo = true;
                    } catch (Exception $e) {
                        $validbo = false;
                    }
                    try {
                        ValidarUsuarioLogueadoBo(59);
                        $validbo = true;
                    } catch (Exception $e) {
                        $validbo = false;
                    }

                    if ($validbo) { // Modo Test
                        $userEmail = Aplicacion::Decrypter($_SESSION["BO"]["User"]["email"]);
                    } else {
                        $userEmail = Aplicacion::Decrypter($_SESSION["User"]["email"]);
                    }
                    ?>
                    <div id="prod-attributes">
                        <span class="prodname"><?= $oProducto->getNombre() ?></span>
                        <?php
                        $porcDescuento = number_format((1 - ($oProducto->getPVenta() / $oProducto->getPVP())) * 100, 0, ',', '.');
                        if ($porcDescuento >= 20) {
                            ?>
                            <span class="porc-desc">(<?= $porcDescuento ?>% OFF)</span>
                            <?php
                        }
                        ?>
                        <form id="frmProducto" method="post" action="<?= $_SERVER['REQUEST_URI'] ?>">
                            <input type="hidden" id="IdProducto" name="IdProducto" value="<?= $_GET["IdProducto"] ?>" />
                            <input type="hidden" id="IdCampania" name="IdCampania" value="<?= $_GET["IdCampania"] ?>" />
                            <input type="hidden" id="IdCodigoProdInterno" name="IdCodigoProdInterno" value="" />
                            <input type="hidden" id="Cantidad" name="Cantidad" value="" />
                            <input type="hidden" id="StockDisp" name="StockDisp" value="" />
                            <input type="hidden" id="MaxProd" name="MaxProd" value="<?= $objCampania->getMaxProd() ?>" />

                            <ul id="pre-attributes">

                                <li> <!-- precio -->
                                    <p id="cuotas-container"> 
                                        <span id="divPrecio"><strong>$<?= number_format($oProducto->getPVenta(), 0, ',', '.') ?></strong></span>
                                        <?php
                                        if ($oProducto->getPVenta() != $oProducto->getPVP()) {
                                            ?>
                                            <span id="precio-lista">$<?= number_format($oProducto->getPVP(), 0, ',', '.') ?></span>
                                            <?php
                                        }
                                        ?>
                                    </p>
                                </li>
                            </ul>

                            <ul id="attributes">
                                <?
                                /* Not for now
                                  <li><!-- talle -->
                                  <span class="item">Seleccionar talle</span>
                                  <ul id="talle">
                                  <li class="no-disponible"><a href="#">XS</a></li>
                                  <li class="deselected"><a href="#">S</a></li>
                                  <li class="deselected"><a href="#">M</a></li>
                                  <li class="selected"><a href="#">L</a></li>
                                  <li class="deselected"><a href="#">XL</a></li>
                                  </ul>
                                  <div class="unfloat"></div>
                                  </li>

                                  <li><!-- color -->
                                  <span class="item">Seleccionar color</span>
                                  <ul id="color">
                                  <li class="no-disponible"><a href="#"><img src="images/color-sample.jpg" /></a></li>
                                  <li class="deselected"><a href="#"><img src="images/color-sample.jpg" /></a></li>
                                  <li class="selected"><a href="#"><img src="images/color-sample.jpg" /></a></li>
                                  <li class="deselected"><a href="#"><img src="images/color-sample.jpg" /></a></li>
                                  </ul>
                                  <div class="unfloat"></div>
                                  </li>
                                 */
                                ?>
                            </ul>

                            <ul id="divStockExist" style="display:block">
                                <?
                                if (file_exists(Aplicacion::getRoot() . "front/productos/productos_" . $oProducto->getIdProducto() . "/talles.html")) {
                                    ?><li>
                                        <a href="<?= Aplicacion::getRootUrl() . "front/productos/productos_" . $oProducto->getIdProducto() . "/talles.html" ?>" onClick="return hs.htmlExpand(this, { objectType: 'iframe' } )">Ver gu&iacute;a de talles</a>
                                    </li>
                                    <?
                                }

                                $contenidoCache = ob_get_clean();
                                $cache->save($contenidoCache, $nombreCache, array('group_cache_catalogo_campania_' . $_GET["IdCampania"]));
                            } // fin primer cache
                            echo $contenidoCache;

                            if ($reloadCache) {
                                ob_start();
                               // $actualStock = 0;
                                if ($actualStock > 0) {
                                    ?>

                                    <li id="aceptar"><!-- call to action -->                                    
                                        <a href="javascript:ValidarFormulario()"><img class="btn-aceptar" src="<?= UrlResolver::getImgBaseUrl("front/catalogo/images/cta.jpg") ?>" /></a>
                                    </li>

                                    <li id="cantitems"><!-- cantidad -->
                                        Cantidad<br />
                                        <select id="cbxCantidad" name="cbxCantidad">
                                            <?
                                            for ($i = 1; $i <= $objCampania->getMaxProd(); $i++) {
                                                ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>
                                                <?
                                            }
                                            ?>
                                        </select>

                                        <?
                                        /* Not for now
                                          <div id="ultimos">
                                          <span style="width:auto;margin-left:5px" class="ultprod">&iexcl;&Uacute;ltimas X unidades!</span>
                                          </div>
                                         */
                                        ?>
                                    </li>
                                    <?
                                } else {
                                    ?>
                                    <div id="divAgotado" ><img src="images/agotado.gif" alt="Agotado" /></div>
                                    <?php
                                }
                                $contenidoCache = ob_get_clean();
                                $cache->save($contenidoCache, $nombreCache . "_2", array('group_cache_catalogo_campania_' . $_GET["IdCampania"]));
                            } else {
                                $contenidoCache = $cache->load($nombreCache . "_2");
                            }
                            echo $contenidoCache;
                            ?>
                        </ul>
                        <?
                        if (!Aplicacion::VerificarCampaniaCarrito($_GET["IdCampania"])) {
                            ?>
                            <script>document.getElementById("divStockExist").innerHTML = "<li><b>IMPORTANTE: No pueden agregarse al carrito de compras productos de diferentes campa&ntilde;as, por favor finalice la compra actual y vuelva a seleccionar este producto. Muchas Gracias.</b></li>"</script>
                            <?
                        }
                        ?>
                        <div id="divAgotado" style="display:none"><img src="images/agotado.gif" alt="Agotado" /></div>
                    </form>
                </div>
                <?
                if ($reloadCache) {
                    ob_start();
                    ?>
                    <!-- inicio bloque descripcion -->
                    <div id="description">
                        <!-- redes sociales | 
                        Genera un error en ambiente BW se quita y se coloca antes de commit-->

                        <!-- fin redes sociales -->
                    </div>
                    <!-- fin bloque descripcion -->

                    <div id="description-tabs">
                        <ul>
                            <li class="current">DESCRIPCI&Oacute;N</li>
                            <li>TIEMPOS DE ENTREGA</li>
                            <li>GARANTIAS Y DEVOLUCIONES</li>
                        </ul>
                    </div>

                    <div id="description-body" class="tab">
                        <p><?= htmlspecialchars_decode($oProducto->getDescripcionAmpliada()) ?></p>
                    </div>

                    <div id="description-body" class="tab" style="display:none">
                        <p>Al finalizar cada campaña de ventas, empezamos el proceso de envío. Te queremos compartir este proceso para que tengas en cuenta los tiempo de entrega.</p>
                        <p><strong>Paso 1</strong> - Pendiente entrega marca: Esperamos a que la marca nos envíe los productos. Esto puede tardar entre 3 y 5 días.</p>
                        <p><strong>Paso 2</strong> - En Bodega: Empezamos a preparar los pedidos, esto lo hacemos con mucha dedicación, por lo que dura un par de días mas.</p>
                        <p><strong>Paso 3</strong> - Transportadora: Tu pedido es recogido por nuestra transportadora y en camino a su destino final. En este paso te enviaremos el número de guía para que puedas hacer seguimiento.</p>
                        <p><strong>Paso 4</strong> - Entregado: ¡Disfruta tu pedido!</p>

                        <p>*Algunas campañas pueden tener condiciones de envío especiales. Estas serán anunciadas en el momento del lanzamiento de la campaña de ventas.</p>

                        <p>Geelbe es Tu Club Privado de Compras por Internet y por eso trabajamos día a día por tu satisfacción.  Tenemos un Equipo de Servicio al Cliente que estará dispuesto a ayudarte en lo que necesites. Encuentra la opción en: “Mi Cuenta”/”Mis Consultas”.</p>
                    </div>

                    <div id="description-body" class="tab" style="display:none">
                        <p><strong>Devoluciones</strong></p>
                        <p>Si no te quedó la talla, recibiste un producto diferente al que pediste, si tiene algún defecto o simplemente cambiaste de opinión sobre tu compra, lo puedes devolver. Para esto cuentas con 7 días hábiles Desde el momento en que tu pedido es entregado en el domicilio.</p>
                        <p>Recuerda que no aceptamos devoluciones de prendas íntimas por razones de higiene.</p>
                        <p>Geelbe es Tu Club Privado de Compras por Internet y por eso trabajamos día a día por tu satisfacción.  Tenemos un Equipo de Servicio al Cliente que estará dispuesto a ayudarte en lo que necesites. Encuentra la opción en: “Mi Cuenta”/”Mis Consultas”.</p>
                    </div>

                </div>

                <?
                GenerarMatrizJS($_GET["IdProducto"]);
                HTMLCrearCombos_x_IdProducto_New($_GET["IdProducto"]);
                ?>

            </div>

            <script type="text/javascript">
                var imgid = 'image';
                var imgdir = '<?= UrlResolver::getImgBaseUrl("front/productos/productos_" . $oProducto->getIdProducto()); ?>';
                var prodid = '<?= $oProducto->getIdProducto() ?>';
                var howmany = "<?= $imgNumbers ?>";
                var imgext = '.jpg';
                var thumbid = 'thumbs';
                var auto = false;
                var autodelay = 5;
            </script>
            <script type="text/javascript">
                var precioProducto = <?= number_format($oProducto->getPVenta(), 0, ',', '') ?>;
                				
                var intereses = {};
                intereses[3] = 5;
                intereses[6] = 11;
                intereses[9] = 16;
                intereses[12] = 22;
                				
                $(document).ready(function() {
                				   
                    $("#cuotas").change(function () {
                        $("#cuotas option:selected").each(function () {
                            var cuota = $(this).val();
                            var interes = intereses[cuota];
                            $("#divPrecioCuota").html("$" + (Math.round((precioProducto + (precioProducto*interes/100))/cuota))); 
                        });
                    });
                });
            </script>

            <script src="<?= UrlResolver::getJsBaseUrl("front/catalogo/js/slide.js"); ?>" type="text/javascript"></script>
            <?
            $contenidoCache = ob_get_clean();
            $cache->save($contenidoCache, $nombreCache . "_3", array('group_cache_catalogo_campania_' . $_GET["IdCampania"]));
        } else {
            $contenidoCache = $cache->load($nombreCache . "_3");
        }
        echo $contenidoCache;
        ?>				
        <? require("../menuess/footerNew.php"); ?>

    </body>

    <script>
        $("#botonshare").click(function(event) {
            if($("#share").hasClass("mostrar")) {
                $("#share").removeClass("mostrar");
            } else {
                $("#share").addClass("mostrar");
            }
        });
        $("#shareWithFacebookTrigger").click(function(event) {
            $("#recommendByMail").hide();
            newwindow = window.open("http://www.facebook.com/sharer.php?u=<?= urlencode($publicPageUrl) ?>", "Facebook", "height=440, width=620, scrollbars=true");
            if (window.focus) {
                newwindow.focus();
            }
            return false;
        });
        $("#shareWithTwitterTrigger").click(function(event) {
            $("#recommendByMail").hide();
            newwindow = window.open("http://twitter.com/home?status=<?= urlencode($publicPageUrl) ?>", "Twitter", "height=650, width=1024, scrollbars=true");
            if (window.focus) {
                newwindow.focus();
            }
            return false;
        });
        $("#shareWithMailTrigger").click(function(event) {
            $("#recommendByMail").slideToggle('slow', function() {
            });
        });
    
    </script>


</html>
