<?php
  class Categoria_Head {
  	
    private $_dt = array();
    private $_nombre = "";
    private $_pagina = "";
    private $_idcategoria_seleccionada=-1;
    private $_adicionalGet="";
    private $_subcategoriesString="";
    private $_categoria_seleccionada;
    private $_nombreCampania;
    
    public function Categoria_Head($nombre, array $dt) {
		$this->_nombre=$nombre;
        $this->_dt=$dt;
    }

    public function setPagina($pagina) {
        $this->_pagina = $pagina;
    }
    
    public function getPagina($Id) {
        $Pagina  = $this->_pagina."?IdCategoria=".$Id.$this->getAdicionalGet();
        return $Pagina;
    }
    
    public function setAdicionalGet($adicionalGet) {
        $this->_adicionalGet = $adicionalGet;
    }
    
    public function setNombreCampania($nombreCampania) {
        $this->_nombreCampania = $nombreCampania;
    }
    
    public function getAdicionalGet() {
        return $this->_adicionalGet;
    }
    
    public function setIdCategoria_Seleccionada($idcategoria_seleccionada) {
        $this->_idcategoria_seleccionada = $idcategoria_seleccionada;
    }
    
    public function getIdCategoria_Seleccionada() {
        return $this->_idcategoria_seleccionada;
    }
    
    public function show() {
    	if ($this->_idcategoria_seleccionada != -1) {
    		echo $this->_estasEn($this->_encontrarCategoriaSeleccionada($this->_dt[0]["subCategoria"]));
    	} else {
	        $this->_printCategorias($this->_dt[0]["subCategoria"]);
	        echo $this->_subcategoriesString;
    	}
    }
    
    private function _encontrarCategoriaSeleccionada($categorias = array()) {
    	 foreach($categorias as $categoria) {
    	 	if($categoria["IdCategoria"] == $this->_idcategoria_seleccionada) {
    	 		return $categoria;
    	 	}
    	 }
    	 
    	 return null;
    }
    
    private function _inicioSubCat($subCatId = null) {
    	return '<div class="subcategorias" id="subcategorias'.$subCatId.'"><ul>';   
    }
    
    private function _finSubCat() {
    	return "</ul></div>";
    }
    
    private function _inicioCat($listadoNro = "") {
	    return '<ul class="listado-categorias" id="listado-categorias-'.$listadoNro.'">';   
    }

    private function _finCat() {
    	return "</ul>";
    }
    
    private function _createTodos() {
        $class_seleccionado = "";
        if ($this->_idcategoria_seleccionada == -1) {
            $class_seleccionado = 'class="seleccionada" ';
        }
        return '<li><a '.$class_seleccionado.'href="'.$this->getPagina(-1).'" title="Todos">Todos</a></li>';
    }
    
    private function _printCategorias(array $nodo) {
    	$counter = 0;
        foreach($nodo as $categoria) {
        	if ($counter == 0) {
        		echo $this->_inicioCat(1);
        	} else if($counter == 4) {
        		echo $this->_finCat();
        		echo $this->_inicioCat(2);
        	} else if ($counter == 8) {
        		echo $this->_finCat();
        		echo $this->_inicioCat(3);
        	}
        	
		    echo $this->_printCategoria($categoria);
		    $counter++;
        }

        echo $this->_createTodos();
        echo $this->_finCat();
    }
    
    private function _printSubCategorias(array $nodo) {
    	$content = "";
    	
    	foreach($nodo as $subcategoria) {
    		$content .= $this->_printCategoria($subcategoria);
    		if ($this->_idcategoria_seleccionada == $subcategoria["IdCategoria"]) {
            	$this->_categoria_seleccionada = $subcategoria;
            }
    	}
    	
    	return $content;
    }
    
    private function _printCategoria($categoria = null, $tieneSubCategorias = false) {
	    return '<li><a '.$class_seleccionado.' href="'.$this->getPagina($categoria["IdCategoria"]).'" title="'.$categoria["Descripcion"].'">'.$categoria["Nombre"].'</a></li>';
    }
    
    private function _estasEn($dataCategoriaSeleccionada = null) {
    	$EstasEn = '<div id="path"><div>';
        $EstasEn .= "<a href='".$this->getPagina(-1)."'>".$this->_nombreCampania."</a>";

        $arrNombres = array();
        $oCategoria = dmCategoria::getByIdCategoria($this->_idcategoria_seleccionada);
        while($oCategoria->getIdPadre()) {
			array_push($arrNombres, $oCategoria);
            $oCategoria = dmCategoria::getByIdCategoria($oCategoria->getIdPadre());
        }
        $tieneSubcategorias = (is_array($dataCategoriaSeleccionada["subCategoria"]) && count($dataCategoriaSeleccionada["subCategoria"]) > 0);
        $EstasEn .= $this->_armarStringCategorias($arrNombres, $tieneSubcategorias, $dataCategoriaSeleccionada["IdCategoria"]);
        $EstasEn .= '</div></div>';

        if ($tieneSubcategorias) {
        	$subCatStart = $this->_inicioSubCat($dataCategoriaSeleccionada["IdCategoria"]);
		    $subCatContent = $this->_printSubCategorias($dataCategoriaSeleccionada["subCategoria"]);
		    $subCatEnd = $this->_finSubCat();
        	
		    $EstasEn .= $subCatStart.$subCatContent.$subCatEnd;
        }
        
		return $EstasEn;
	}
        
	private function _armarStringCategorias(array $arrNombre, $tieneSubcategorias = false, $IdCategoria = "") {
        $flechaImg = UrlResolver::getImgBaseUrl("front/catalogo/images/flecha.jpg");
        $flechaDownImg = UrlResolver::getImgBaseUrl("front/catalogo/images/flecha_down.jpg");
        $str = "";
        $Array = array();
        $Array = array_reverse($arrNombre);
        foreach($Array as $element) {
        	if($tieneSubcategorias) {
				$str = $str . " <img src='".$flechaImg."' /> " . "<a id='subcategorias".$IdCategoria."Trigger' href='javascript:showSubCategory(".$IdCategoria.");'>".$element->getNombre()." <img class='flecha-down' src='".$flechaDownImg."' /></a>";
        	} else {
				$str = $str . " <img src='".$flechaImg."' /> " . "<a href='".$this->getPagina($element->getIdCategoria())."'>".$element->getNombre()."</a>";
        	}
        }
    	return $str;
	}
    
  }
?>
