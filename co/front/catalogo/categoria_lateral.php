<?php
  class Categoria_Lateral
  {
    private $_dt = array();
    private $_nombre = "";
    private $_pagina = "";
    private $_idcategoria_seleccionada=-1;
    private $_adicionalGet="";
    
    public function Categoria_Lateral($nombre,array $dt)
    {
        try
        {
            $this->_nombre=$nombre;
            $this->_dt=$dt;
        }
        catch(exception $e)
        {
            throw $e;
        }
    }
    public function setPagina($pagina)
    {
        $this->_pagina = $pagina;
    }
    public function getPagina($Id)
    {
        $Pagina  = $this->_pagina."?IdCategoria=".$Id.$this->getAdicionalGet();
        return $Pagina;
    }
    public function setAdicionalGet($adicionalGet)
    {
        $this->_adicionalGet = $adicionalGet;
    }
    public function getAdicionalGet()
    {
        return $this->_adicionalGet;
    }
    public function setIdCategoria_Seleccionada($idcategoria_seleccionada)
    {
        $this->_idcategoria_seleccionada = $idcategoria_seleccionada;
    }
    public function getIdCategoria_Seleccionada()
    {
        return $this->_idcategoria_seleccionada;
    }
    public function show()
    {
        try
        {
            $this->_inicioCat();
            $this->_createNivel($this->_dt[0]["subCategoria"],"");
            $this->_createTodos();
            $this->_finCat();
        }
        catch(exception $e)
        {
            throw $e;
        }
    }
    private function _inicioCat()
    {
        try
        {
            ?>
             <ul>   
            <?
        }
        catch(exception $e)
        {
            throw $e;
        }
    }
    private function _finCat()
    {
        try
        {
            ?>
                </ul>
            <?
        }
        catch(exception $e)
        {
            throw $e;
        }
    }
    
    private function _createTodos() {
        ?>
        <li><a id="<?=($this->getIdCategoria_Seleccionada() == -1)?"active":"sub"?>"
		href="<?=$this->getPagina(-1)?>"
		title="Todos">Todos</a></li>
        <?
    }
    
    private function _createNivel(array $nodo, $sep="")
    {
        try
        {
            foreach($nodo as $categoria) {
                if ($categoria["Deep"] == 2)
                    $symbol = ">  ";
                else if($categoria["Deep"] > 2)
                    $symbol = ">>*  ";
                $sep = $symbol;
                
            ?>
                <li><a id="<?=($this->getIdCategoria_Seleccionada() == $categoria["IdCategoria"])?"active":"sub"?>"
                href="<?=$this->getPagina($categoria["IdCategoria"])?>"
                title="<?=$categoria["Descripcion"]?>"><?=$sep.$categoria["Nombre"]?></a></li>
            <?
                if(is_array($categoria["subCategoria"]))
                { 
				        $this->_createNivel($categoria["subCategoria"]);//$sep."&nbsp;&nbsp;&nbsp;"
                }
            }
        }
        catch(exception $e)
        {
            throw $e;
        } 
    }
  }
?>
