<?
$cartUrlProtocol = "https://";
if($_SERVER['HTTP_HOST'] == "localhost") {
	$cartUrlProtocol = "http://";
}

$CarritoCargado = true;
try {
    $MiCarrito = Aplicacion::getCarrito();
} catch(ACCESOException $e) {
   $CarritoCargado = false;
} 
if($CarritoCargado && $MiCarrito != null && count($MiCarrito->getArticulos() > 0)) {
	$MostarMiCarrito = true;
} else {
	$MostarMiCarrito = false;
}

?>

<div class="item-list">
	<ul>
	<li><a <?=$act==1 ? "id=active" : ""?> href="/<?=Aplicacion::getDirLocal()?>front/micuenta/mi-credito.php" title="Mi cr&eacute;dito" >Mi cr&eacute;dito <img src="<?=UrlResolver::getImgBaseUrl("front/login/images/x.gif");?>" alt="" width="87" height="10" /></a></li>
	<li><a <?=$act==2 ? "id=active" : ""?> href="/<?=Aplicacion::getDirLocal()?>front/micuenta/mi-perfil.php" title="Mi perfil">Mi perfil <img src="<?=UrlResolver::getImgBaseUrl("front/login/images/x.gif");?>" alt="" width="100" height="10" /></a></li>
    <li><a <?=$act==3 ? "id=active" : ""?> href="/<?=Aplicacion::getDirLocal()?>front/micuenta/mispedidos.php" title="Mis pedidos">Mis pedidos <img src="<?=UrlResolver::getImgBaseUrl("front/login/images/x.gif");?>" alt="" width="70" height="10" /></a></li>       
    <li><a <?=$act==4 ? "id=active" : ""?> href="/<?=Aplicacion::getDirLocal()?>front/referenciados/" title="Invita a un amigo">Invita a un amigo <img src="<?=UrlResolver::getImgBaseUrl("front/login/images/x.gif");?>" alt="" width="50" height="10" /></a></li>
    <? 
    if ($MostarMiCarrito){
    ?>
    	<li><a <?=$act==5 ? "id=active" : ""?> href="<?=$cartUrlProtocol.$_SERVER['HTTP_HOST']."/".$confRoot[1]."/ssl/carrito/step_1.php"?>" title="Mi carrito">Mi carrito <img src="<?=UrlResolver::getImgBaseUrl("front/login/images/x.gif");?>" alt="" width="100" height="10" /></a></li>
	<?
	}
    ?>
    <li><a <?=$act==6 ? "id=active" : ""?> href="/<?=Aplicacion::getDirLocal()?>front/micuenta/misconsultas.php?act=6" title="Mis consultas">Mis consultas <img src="<?=UrlResolver::getImgBaseUrl("front/login/images/x.gif");?>" alt="" width="100" height="10" /></a></li>
	</ul>
  </div>