<?
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("miscreditos"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
		
		$validbo = false;
	    try {
			ValidarUsuarioLogueadoBo(9);
			$validbo = true;
		}
		catch(Exception $e) {
			$validbo = false;
		}
	    try {
			ValidarUsuarioLogueadoBo(59);
			$validbo = true;
		}
		catch(Exception $e) {
			$validbo = false;
		}
		
    	if ($validbo) { // Modo Test
    		$datosUser = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]));
    	}
    	else {
    		$datosUser = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));
    	}
		
        try {
        	$MiCarrito = Aplicacion::getCarrito();
			$Productos = count($MiCarrito->getArticulos());
		} catch(Exception $e) {
			$Productos = 0;
		}
		
		require_once dirname(__FILE__).'/google.php';
?>
<?php 

	Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));
	$objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
	if($objUsuario->getPadrino() == 'daviviendademo1'):

?>
<style type="text/css">
	#header {
    	background-color: #EEEEEE;
	}
	
	#headerInterior {
    	background-color: #EEEEEE;
    	background-image: url(/<?=Aplicacion::getDirLocal()?>/front/vidriera/images/header_davivienda.png);
	}
	
	#header span {
    	color: #333 !important
	}
</style>
<?php endif; ?>
<script src="<?=UrlResolver::getJsBaseUrl("logica/scripts/AC_RunActiveContent.js");?>" type="text/javascript"></script>

<div id="header" style="height:98px;">
<div id="headerInterior" style="height:98px;">
  <span style="color:#cccccc;padding:5px;padding-right:20px;top:0%;position:relative;vertical-align:middle;float:right;">Has ingresado como <? echo stripslashes($datosUser["Nombre"])." ".stripslashes($datosUser["Apellido"]); ?>
  <?php if($Productos > 0): ?>
  - <a href="/<?=Aplicacion::getDirLocal()?>ssl/carrito/step_1.php">Mi Carrito</a> (<?=$Productos?>) -
  <?php endif; ?>
  (<a style="color:#999999;" href="/<?=Aplicacion::getDirLocal()?>front/login/logout.php" title="Salir de Geelbe.">salir</a>).</span>

  <a href="/<?=Aplicacion::getDirLocal()?>front/vidriera/" id="logoppal">Geelbe</a>
  	<div id="primary" style="clear:both;float:right;">
	    <ul>
	      <li>
	        <a href="https://<?=$_SERVER['HTTP_HOST']."/".$confRoot[1]?>/ssl/geelbecash/">
			  <img src="<?=UrlResolver::getImgBaseUrl('front/images/gcash-nav.png');?>" style="margin-top:-3px" />
	        </a>
	      </li>	
 			<li><a href="/<?=Aplicacion::getDirLocal()?>front/faq/">Ayuda</a></li>
	      <li><a href="/<?=Aplicacion::getDirLocal()?>front/referenciados/">Invitar</a></li>
	      <li><a href="/<?=Aplicacion::getDirLocal()?>front/micuenta">Mi Cuenta</a></li>
	      <li style="margin-left:124px;"><a href="/<?=Aplicacion::getDirLocal()?>front/vidriera/">Vitrina</a></li>
	    </ul>
	  </div>
	</div>
</div>
