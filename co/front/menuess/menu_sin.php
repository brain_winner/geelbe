<?php
   require_once dirname(__FILE__).'/google.php';
?>
<div id="header" style="height:98px;">

	<div id="headerInterior" style="height:98px;">
	  
		<a href="/co/front/vidriera/" id="logoppal">Geelbe</a>
		
		<div id="primary" class="login" style="clear:both;float:right;margin-right:25px">
		<ul>
		  <li><a href="/<?=Aplicacion::getDirLocal()?>front/login" class="login">Entrar</a> /</li>
		  <li><a href="/<?=Aplicacion::getDirLocal()?>registro" class="registro">Reg&iacute;strate</a></li>
		</ul>
		</div>
		
		<!--<div id="loginPopup">
			<form  method="post" name="frmLogin" id="frmLogin" onkeypress="if (ValidarEnter(event)) this.onsubmit();" target="iLogin" action="/<?=Aplicacion::getDirLocal()?>logica/login/actionform/login.php">
			
			<input name="txtUser" maxlength="60" type="text"  placeholder="Email"/>
			<input name="txtPass" type="password" maxlength="60" placeholder="Contrase&ntilde;a" />
			<a href="<?=UrlResolver::getBaseUrl("front/login/recordar2.php");?>">&iquest;Olvidaste tu contrase&ntilde;a?</a>
			
			<div class="buttons">
				<a href="http://<?=$_SERVER["SERVER_NAME"]?>/appfacebook/register.php?country=co&codact=facebooklogin" target="_blank">
					<img src="/<?=Aplicacion::getDirLocal()?>front/images/new_facebook.png" alt="Facebook" />
				</a>
				<input type="image" src="/<?=Aplicacion::getDirLocal()?>front/images/new_entrar.png" name="login" value="Entrar" id="loginButton" />
			</div>
		</form>
		</div> -->
		
		  <div id="loginPopup">
		  	<div>
		  		<form  method="post" name="frmLogin" id="frmLogin" onkeypress="if (ValidarEnter(event)) this.onsubmit();" target="iLogin" action="/<?=Aplicacion::getDirLocal()?>logica/login/actionform/login.php">
			  		<fieldset>
						<h2>Acceso para socios</h2>
							
						<input name="txtUser" maxlength="60" placeholder="Email" type="text" />
						<input name="txtPass" type="password" placeholder="Contrase&ntilde;a" maxlength="60" />
						
						<p class="olvidaste"><a href="<?=UrlResolver::getBaseUrl("front/login/recordar2.php");?>">&iquest;Olvidaste tu contrase&ntilde;a?</a></p>
						
						<div class="buttons">
							<a href="http://<?=$_SERVER["SERVER_NAME"]?>/appfacebook/register.php?country=co&codact=facebooklogin" target="_blank">
								<img src="/<?=Aplicacion::getDirLocal()?>front/images/new_facebook2.png" alt="Facebook" />
							</a>
							<input type="image" src="/<?=Aplicacion::getDirLocal()?>front/images/new_entrar2.png" name="login" value="Entrar" id="loginButton" />
						</div>
					</fieldset>
				</form>
		  	</div>
		  </div>
		  
	
	</div>
</div>

 <div style="clear:both"></div>

<div id="bg"></div>
<iframe style="display:none" id="iLogin" name="iLogin"></iframe>
<script src="<?=UrlResolver::getBaseUrl("js/jquery.js", true);?>" type="text/javascript"></script>
<script src="<?=UrlResolver::getBaseUrl("front/login/js/js.js", true);?>" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() {
	$("#loginButton").click(function() {
		if(ValidarForm($("#frmLogin")[0])) {
			$("#frmLogin").submit();
		}
	});
	
	$('a.login').click(function() {
		$('#bg, #loginPopup').show();
		return false;
	})
	
	$('a.registro, #logoppal').click(function(e) {
		e.stopPropagation();
		return true;
	})
	
	$('#loginPopup').click(function(e) {
		e.stopPropagation();
	})
	
	$('#header, #bg').click(function(e) {
		e.stopPropagation();
		$('#bg, #loginPopup').hide();
		return false;
	})
});
//]]>
</script>
