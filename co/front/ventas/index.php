<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/ventas/js/js.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
  <? 
  $act=5;
  require("../menuess/cabecera.php") 
  ?>
<div id="container">
  <div id="contenido-top">

  </div>
  <div id="contenido">
	<div id="top">	
	  <h1>Acerca de Geelbe</h1>
	</div>
	<div id="central">
	  <h2><img src="<?=UrlResolver::getImgBaseUrl("front/images/shopping_cart.png");?>" alt="Las Ventas" width="48" height="48" align="middle" /><strong> Las ventas de Geelbe</strong></h2>
	  <p>&nbsp;</p>
	  <p>El modelo de ventas utilizado por <strong>Geelbe</strong> pone los productos a disposici&oacute;n de los miembros del  club de compras por un tiempo limitado. Durante este tiempo los miembros del  club pueden recorrer el cat&aacute;logo, conocer a detalle los productos ofrecidos y  seleccionar los que desean agregar a su carrito de compras. El d&iacute;a que se cierra la venta se re&uacute;nen todos los  pedidos para luego armar los paquetes con el producto y enviarlos a los miembros que  realizaron compras.</p>
	  <p>&nbsp;</p>
	  <p><strong>Antes de una venta</strong>: Unos d&iacute;as antes de  cada venta <strong>Geelbe</strong> env&iacute;a un eMail a sus  miembros para promocionar la Campa&ntilde;a de la marca que se inicia y la duraci&oacute;n de  la misma. Tambi&eacute;n se promocionan los productos y se comunican las fechas en el  website.</p>
	  <p>&nbsp;</p>
	  <p><strong>Durante la Venta: </strong>Desde el eMail de  promoci&oacute;n (o ingresando tus datos de usuario en el website) podr&aacute;s acceder al  cat&aacute;logo de productos ofrecidos, conocerlos en detalle y seleccionar los que  deseas agregar a tu carrito de compras.</p>
	  <p>Cuando termines de seleccionar productos -y agregarlos  en tu carrito de compras- indica el m&eacute;todo de pago que deseas utilizar y  confirma la transacci&oacute;n. Recuerda que el contenido de tu carrito de compras no  se reservar&aacute; hasta que se confirme que el pago ha sido realizado con &eacute;xito.</p>
	  <p>&nbsp;</p>
	  <p>Al confirmar el contenido de tu carrito de compras  recibir&aacute;s un eMail con el detalle de tu pedido: Precio de cada producto, forma  de pago, fechas de env&iacute;o programadas, direcci&oacute;n de entrega y el c&oacute;digo para  hacer el seguimiento -tracking- de tu compra. </p>
	  <p>Debes tener en cuenta que si no realizas el pago  dentro de las 24hs de cerrada la venta no podremos procesarlo, se cancelar&aacute; tu  pedido y el monto quedar&aacute; acreditado en tu cuenta para futuras compras.</p>
	  <p>&nbsp;</p>
	  <div class="grisado">
	    <p><strong style="color:#FF0000">Consejo:</strong><strong>Geelbe</strong> reserva una cantidad  limitada de cada producto -para ofrecer a sus miembros- por lo tanto mientras  avanzan los d&iacute;as de la venta es probable que comiencen a agotarse las opciones  de compra m&aacute;s atractivas (modelos, tallas, colores, etc). Te recomendamos  ingresar a la venta durante sus primeros d&iacute;as y si te interes&oacute; un producto no  dudes en adquirirlo porque puede agotarse.</p>
	  </div>
	  <p>&nbsp;</p>
	  <p><strong>Despu�s de una venta: </strong>Pasadas las 24 hs del cierre de una venta se env&iacute;a el pedido de  productos a la marca que particip&oacute; de la propuesta. La marca puede demorar  hasta 48hs en entregar los productos, luego de ese tiempo se procede a armar y  enviar los pedidos a sus compradores. Dependiendo de las caracter&iacute;sticas -y la  distancia del env&iacute;o- un pedido puede tardar entre 2 y 4 d&iacute;as en llegar a su  destino, considerados a partir del fin de la Campa&ntilde;a. Igualmente <strong>Geelbe</strong> te env&iacute;a el c&oacute;digo de seguimiento -tracking- para que puedas saber en  todo momento d&oacute;nde se encuentra tu pedido.<br />
	    <br />
      </p>
	  <p></p>
	</div>
  <div id="menuleft">
		<? require("../menuess/acercade.php")?>
	</div>
</div>
  <div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
</body>
</html>
