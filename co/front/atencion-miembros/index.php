<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css")?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/atencion-miembros/js/js.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
  <? 
  $act=7;
  require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">

  </div>
  <div id="contenido">
	<div id="top">	
	  <h1>Acerca de Geelbe</h1>
	</div>
	<div id="central">
	  <h2><img src="<?=UrlResolver::getImgBaseUrl("front/images/help.png");?>" alt="Ayuda" width="48" height="48" align="middle" /> <strong>Servicio de atenci�n a miembros</strong></h2>
	  <p>&nbsp;</p>
	  <p><strong>Geelbe</strong> cumple con los m&aacute;s  altos requisitos legales y est&aacute;ndares de calidad, garantizando a los miembros  del club privado que pueden devolver los productos de su compra si no cumplen  con las caracter&iacute;sticas descritas en la venta publicada en la Tienda Online.</p>
	  <p>&nbsp;</p>
	  <p><strong>Condiciones de  Devoluci&oacute;n y Cambio:</strong> El consumidor tiene derecho a revocar  la presente operaci&oacute;n comercial (por adquisici&oacute;n de cosas y/o prestaci&oacute;n de  servicios) durante un plazo de DIEZ (10) d&iacute;as corridos, contados a partir de la  fecha que se entregue el producto o se celebre el contrato, lo &uacute;ltimo que  ocurra, sin responsabilidad alguna. Esta facultad no puede ser dispensada ni  renunciada. El consumidor comunicar&aacute; fehacientemente dicha revocaci&oacute;n al  proveedor y pondr&aacute; el producto a su disposici&oacute;n. Para ejercer el derecho de  revocaci&oacute;n el consumidor deber&aacute; poner el productto a disposici&oacute;n del vendedor  sin haberla usado y manteni&eacute;ndola en el mismo estado en que la recibi&oacute;,  debiendo restituir el proveedor los importes recibidos (Res. 906/98). Pasado  ese plazo, la empresa proveedora podr&aacute; buscar la mejor soluci&oacute;n posible en pos  de la satisfacci&oacute;n del consumidor, sin que esto sea obligatorio para ella.</p>
	  <p>&nbsp;</p>
	  <p><strong>Condiciones de la  Garant&iacute;a:</strong> Todo producto vendido a trav&eacute;s de <strong>Geelbe</strong> cuenta con una  garant&iacute;a del fabricante, por defectos de origen o fabricaci&oacute;n, que nunca ser&aacute;  menor a 90 d&iacute;as (salvo productos consumibles). </p>
	  <p>&nbsp;</p>
	  <p>En aquellos casos en donde un producto posea una  garant&iacute;a superior, <strong>Geelbe</strong> lo indicar&aacute; a sus  miembros en las condiciones de venta particulares de la promoci&oacute;n.</p>
	  <p><br />
      </p>
	</div>
  <div id="menuleft">
		<? require("../menuess/acercade.php")?>
	</div>
</div>
  <div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
</body>
</html>
