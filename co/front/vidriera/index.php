<?
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
		
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));     
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));

     	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcategoriasglobales.php");
     	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/banners/clases/clsbanners.php");
    	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/banners/datamappers/dmbanners.php");
       
		$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 300), array('cache_dir' => $cacheDir));
		$cache->remove('cache_campanias_new');
		$cuerpo = dmBanners::getVidriera($_GET['IdCampania']);
      	$objUsuario = new MySQL_micuenta();
      	
      	try {
	      	ValidarUsuarioLogueado();
	      	$objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
      	} catch(Exception $e) {

      	}
        
        if(isset($_GET['IdCategoriaGlobal'])) {
        	$idCatGlobal = dmCategoriaGlobal::getChildrenIds($_GET['IdCategoriaGlobal']);
        	$idCatGlobal[] = $_GET['IdCategoriaGlobal'];
        } else
        	$idCatGlobal = null;
    }
    catch(exception $e) {
		header('HTTP/1.1 404 Not Found');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }

    /*try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		$cache->remove('cache_campanias_new');
		header('Location: /'.$confRoot[1]);
		exit;
    }*/

	function parseTimeFromString($string) {
        $year = intval(substr($string, 6, 4));
        $month = intval(substr($string, 3, 2));
        $day = intval(substr($string, 0, 2));
        $hour = intval(substr($string, 11, 2));
        $minute = intval(substr($string, 14, 2));
        return(mktime($hour, $minute, 0, $month, $day, $year));
    }
    
    
    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <? Includes::Scripts() ?>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title><?=Aplicacion::getParametros("info", "nombre");?> </title> 

    <link href="<?=UrlResolver::getCssBaseUrl("front/geelbe_new.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?=UrlResolver::getCssBaseUrl("front/botones.css");?>" rel="stylesheet" type="text/css"  />
    <link href="<?=UrlResolver::getCssBaseUrl("front/highslide.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?=UrlResolver::getCssBaseUrl("front/vidriera/css/vidriera.css");?>" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.4.min.js")?>"></script>
    <script src="<?=UrlResolver::getJsBaseUrl("front/vidriera/js/js.js");?>" type="text/javascript"></script>

    <script type="text/javascript">
    //Tabs
	$(document).ready(function() {
		var tabs = $('#principal .tab');
		
		if(tabs.size() > 1) {
			
			var menu = $('#tabs').show().find('li');
			menu.each(function(k, el) {
				
				$(el).click(function() {
					
					tabs.hide();
					tabs.eq(k).show();
					
					$(menu).removeClass('active').eq(k).addClass('active')
					
				})
				
			})
			
		}
	})
    </script>

    <?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
  </head>
  <body class="vitrina"> 
    <? require("../menuess/cabecera.php") ?>
    <script type="text/javascript">
      fechaHoy = "<?=date("m/d/Y H:i:s");?>";
    </script>
    
	<div id="container-vidriera">

	<div style="padding-top:15px;">
		<?php require_once dirname(__FILE__).'/../categorias/menu.php'; ?>
	</div>
	
      <!-- bannertop -->
      <div id="bannerTop" style="margin-bottom:10px; margin-top:-10px; text-align:center;">
      <?php
      	$id = 28;
		if($objUsuario->getPadrino() == 'alianzadavg-12-02-20141' && $objUsuario->getes_Provisoria() == 0) {
			echo '<a href="/co/front/micuenta/mi-contrasena.php">
				<img src="'.UrlResolver::getImgBaseUrl('front/archivos/banners/'.$id.'/'.dmBanners::getImg($id)).'" />
			</a>';			
		} else {
		   
			foreach($cuerpo as $i => $c) {
				if($c['id'] != $id) {
		      		if(isset($c['img'])) {
		      			if($c['link'] != null) echo '<a href="'.$c['link'].'">';
		      			echo '<img src="'.UrlResolver::getImgBaseUrl('front/archivos/banners/'.$c['id'].'/'.$c['img']).'" />';
		      			if($c['link'] != null) echo '</a>';
		      		}
		      	}
			}
			if(count($cuerpo) == 0) {
				echo '';
			}
			
		}
      ?>
      </div>
      <!-- fin bannertop -->
      
      
    
      <ul id="tabs">
      	<li class="active">Descuentos</li>
      	<li>Edici&oacute;n Limitada</li>
      </ul>
      <div style="clear:both"></div>
      <div id="principal">
	     <h1>Actualmente</h1>
        <?
			try {
				if(!$cache->start('cache_campanias_new') || is_array($idCatGlobal)) {
					$dtcampanias = dmCampania::getCampaniasOrdenadas(true, true, 0, $idCatGlobal);
					$dtcolecciones = dmCampania::getCampaniasOrdenadas(true, true, 1, $idCatGlobal);					
					$cerro_campania = true;
					$titular = 0;
					
					//Reordenar
					$abiertas = $proximas = array();
					foreach($dtcampanias as $camp)
						if($camp["Estado"] == "Abierta")
							$abiertas[] = $camp;
						else
							$proximas[] = $camp;
					
					$dtcampanias = array_merge($abiertas, $proximas);
					
					$cs = array($dtcampanias, $dtcolecciones);
					
				$index = 0;
				foreach($cs as $campanias) {
				
					if(count($campanias) == 0)
						continue;
				
					echo '<div class="tab" '.($index == 1 ? 'style="display:none"' : '').'>';
					$index++;
					
					foreach ($campanias as $campania) {
						if($campania['IdCampania']==270) {
							continue;
						}
						$arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$campania["IdCampania"]."/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));
						$Trailer = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$campania["IdCampania"]."/trailer/", 3, array("swf", "jpg", "png", "gif", "html", "php"));
						if($campania["Estado"] == "Abierta") {
        ?>

          <!-- each campaign -->
          <div id="producto" onClick="window.location.href='../catalogo/main.php?IdCampania=<?=$campania["IdCampania"]?>'" style="cursor:pointer;background-image:url(<?=UrlResolver::getImgBaseUrl("front/campanias/archivos/campania_".$campania["IdCampania"]."/imagenes/".$arrArchivos["banner_camp-on"]);?>);">
            <?
				$Dias = Aplicacion::getParametros("campania_new", "dias") * 24 * 60 * 60;
                $Hoy = strtotime(date("Y-m-d H:i:s"));
                $FechaCampania = strtotime($campania["nuevo"]);
            ?>

            <?php
            	$esEnvioGratis=false;
				$dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($campania["IdCampania"]);
                foreach($dtCampaniasPropiedades as $propiedad) {
                    if ($propiedad["IdPropiedadCampamia"] == 6) {
                    	$esEnvioGratis=true;
					}
                }
            ?>
            
            <?php 
            	if($esEnvioGratis==true || $campania['MinMontoEnvioGratis'] > 0) {
            	
             		if($campania['MinMontoEnvioGratis'] > 0)
            			$desde = ' por compras mayores a $'.number_format($campania['MinMontoEnvioGratis'], 0, ',', '.');
            		else
            			$desde = '';
           ?> 
             <div class="timing-enviogratis">
                <div style="float:left;margin:4px;margin-left:6px;font-size:14px;">
	            	<span><?php echo $campania["Nombre"];?> - <span style="font-weight:bold;">CON ENV&Iacute;O GRATIS<?=$desde?></span> -</span>
	            </div>
            <?php
				} else if($Hoy >= $FechaCampania && $Hoy <= ($FechaCampania + $Dias)) {
            ?>
             <div class="timing-new">
                <div style="float:left;margin:4px;margin-left:6px;font-size:14px;">
	            	<span><?php echo $campania["Nombre"];?></span>
	            </div>
 			<?php
				} else {
            ?>
             <div class="timing">
                <div style="float:left;margin:4px;margin-left:6px;font-size:14px;">
	            	<span><?php echo $campania["Nombre"];?></span>
	            </div>
            <?php
				}
            ?>
            
            <?php 
                if($Hoy >= $FechaCampania && $Hoy <= ($FechaCampania + $Dias)) {
            ?>
	            <div style="float:right;font-size:14px;margin:4px;font-weight:bold;">
		            <span>&iexcl;RECI&Eacute;N  COMIENZA!</span>
                </div>
 			<?php
				} else {
            ?>
	            <div style="float:right;font-size:14px;margin:4px;">
	                <span>
	                  	<div style="display:inline;" name="reloj<?=$campania["IdCampania"]?>" id="reloj<?=$campania["IdCampania"]?>"><script>IniciarReloj("reloj<?=$campania["IdCampania"]?>", "<?=$campania["FechaMDY"]?>", fechaHoy, true);</script></div>
	                </span>
                </div>
            <?php
				}
            ?>
			</div>
            <?php
                $DTLocales = dmLocales::getLocalesbyCampania($campania["IdCampania"]);
                if (count($DTLocales)) {
			?>
			<a href="<?=Aplicacion::getRootUrl() ?>front/locales/locales.php?IdCampania=<?=Aplicacion::Encrypter($campania["IdCampania"])?>" onClick="return hs.htmlExpand(this, { objectType: 'iframe' } )" title="Locales" /><img src="<?=UrlResolver::getImgBaseUrl("front/campanias/propImg/7.gif");?>" /></a>
            <?
				}
            ?>
	        </div>
            <!-- fin each campaign -->
            <?
			}
        	else if($campania["Estado"] == "Proxima" && $campania["Visible"] == "Si") {
				$cerro_campania = false;
				if($titular == 0) {
					echo "";
					echo "<h2>PR&Oacute;XIMAMENTE DISPONIBLE</h2>";
					$titular++;
				}
            ?>

            <!-- upcoming campaign -->
            <div id="productoprox" style="background-image:url(<?=UrlResolver::getImgBaseUrl("front/campanias/archivos/campania_".$campania["IdCampania"]."/imagenes/".$arrArchivos["banner_camp-off"]);?>);">
	            	<div class="timing">
		            <div style="float:left;margin:4px;margin-left:6px;font-size:14px;">
		            	<span><?php echo $campania["Nombre"];?></span>
		            </div>
		            <div style="float:right;font-size:14px;margin:4px;">
			            <span>Comienza el </span>
		                <span>
		                  	<div style="display:inline;" name="reloj<?=$campania["IdCampania"]?>" id="reloj<?=$campania["IdCampania"]?>"><?=str_replace(" ", " - ", $campania["Fecha de inicio"])?></div>
	                	</span>
	                </div>           
	            </div>
            </div>
            <!-- end upcoming campaign -->
            <?
            }
        }
        	//end foreach
	    	echo '</div>';
	    }
	    $cache->end();
    }
} catch(Exception $e) {
	$cache->remove('cache_campanias_new');
	header('Location: /'.$confRoot[1]);
	exit;
}
      ?>
  </div>
  </div>
  </div>
  </div>
<? require("../menuess/footerNew.php");?>
<div id="fb-root"></div>
<script>
	window.fbAsyncInit = function() {
		FB.init({appId: '130962696927517', status: true, cookie: false,
						 xfbml: true});
	};
	(function() {
		var e = document.createElement('script'); e.async = true;
		e.src = document.location.protocol +
			'//connect.facebook.net/es_LA/all.js';
		document.getElementById('fb-root').appendChild(e);
	}());
</script>
</body>
</html>
