<?  
	header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=".$_GET['IdCampania']."_campa�a.ics");
	
	$beginCalendar = "BEGIN:VCALENDAR
VERSION:2.0
METHOD:PUBLISH
CALSCALE:GREGORIAN
PRODID:iCalendar-Ruby
BEGIN:VEVENT
LOCATION:http://www.geelbe.com/co/
SEQUENCE:0
URL:http://www.geelbe.com/co/";
	echo $beginCalendar;
	
	try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));     
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
    }
    catch(exception $e) {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}

	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }
	try {
		$isPageOk = true;
		$pageException = "Exception";
		
		// Validaciones de parametros basicas
        if (!isset($_GET["IdCampania"])) { // IdCampania Seteado
            $isPageOk = false;
			$pageException = "IdCampania not setted";
		}
		else if (!ValidatorUtils::validateNumber($_GET["IdCampania"])) { // IdCampania Numerico
			$isPageOk = false;
			$pageException = "IdCampania is not a number";
		}
		if (!$isPageOk) {
			throw new Exception($pageException);
		}
    }
    catch(Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
		header('HTTP/1.1 404 Not Found');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
    
    
    
	$campania = $_GET['IdCampania'];
	$objCampania = dmCampania::getByIdCampania($campania);
	$vCalStart = $objCampania->getFechaInicio();
	
    function formatIsc ($fecha){
	$fecha = preg_split("/[\s\/:]+/", $fecha);
	$d = $fecha[0];
	$m = $fecha[1];
	$y = $fecha[2];
	$h = $fecha[3];
	$mn = $fecha[4];
	if (strlen($d) == 1){
	$d = "0". $d;
	}
	if (strlen($m) == 1){
	$m = "0". $m;
	}
	if (strlen($h) == 1){
	$h = "0". $h;
	}
	if (strlen($mn) == 1){
	$mn  = "0". $mn;
	}
	return ($y.$m.$d."T".$h.$mn."00"); 
	}
	
	$calendar = "
DTEND:".formatIsc($vCalStart)."
DTSTART:".formatIsc($vCalStart)."
UID:geelbeco_".$campania."
DTSTAMP:20101013T143233
DESCRIPTION:Geelbe presenta ".$objCampania->getNombre()."\\n".$objCampania->getFechaInicio()."\\n
SUMMARY:".$objCampania->getNombre()." en Geelbe
BEGIN:VALARM
TRIGGER:-PT5M
ACTION:DISPLAY
END:VALARM
END:VEVENT
END:VCALENDAR
";

echo $calendar;
	
?>