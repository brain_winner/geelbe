<?php

	function buildArray($pid = null) {
		
		//Construye array asociativo con categor�as y preguntas
		$cats = array('sub' => array(), 'faqs' => array());	
		
		if(isset($_SESSION["User"]["id"]))
			$field = 'login';
		else
			$field = 'no_login';
		
		if($pid == null || !is_numeric($pid)) {
			$parent = 'is null';
			
			//Preguntas del nivel padre		
			$data = mysql_query("SELECT id, nombre, visits_login, visits_no_login, ".$field." as respuesta FROM faq WHERE catID is null ORDER BY `order` ASC");
			while($d2 = mysql_fetch_assoc($data)) {
				$cats['faqs'][] = $d2;
			}
		} else
			$parent = '= '.mysql_real_escape_string($pid);
		
		$dbcats = mysql_query("SELECT id, nombre FROM faq_cat WHERE parent ".$parent." ORDER BY `order` ASC");
	
		$cat = 0;
		while($d = mysql_fetch_assoc($dbcats)) {
					
			$cats['sub'][$cat] = $d;
			$cats['sub'][$cat] = array_merge($cats['sub'][$cat], buildArray($d['id']));
			$cats['sub'][$cat]['visits_login'] = 0;
			$cats['sub'][$cat]['visits_no_login'] = 0;
			
			//Contabilizar visitas del subnivel
			foreach($cats['sub'][$cat]['sub'] as $i => $c) {
				$cats['sub'][$cat]['visits_login'] += $c['visits_login'];	
				$cats['sub'][$cat]['visits_no_login'] += $c['visits_no_login'];	
			}
			
			$data = mysql_query("SELECT id, nombre, visits_login, visits_no_login, ".$field." as respuesta FROM faq WHERE catID = ".mysql_real_escape_string($d['id'])." ORDER BY `order` ASC");
			while($d2 = mysql_fetch_assoc($data)) {
				$cats['sub'][$cat]['faqs'][] = $d2;
				$cats['sub'][$cat]['visits_login'] += $d2['visits_login'];
				$cats['sub'][$cat]['visits_no_login'] += $d2['visits_no_login'];
			}
			
			$cat++;
			
								
		}
		
		return $cats;
	}

	function post($v) {
		return (isset($_POST[$v]) ? $_POST[$v] : '');
	}
	
	//Selector de categor�as
	function categoriesSelect($selected = null, $parent = null, $n = 0) {
		
		if($parent == null || !is_numeric($parent))
			$parent = 'is null';
		else
			$parent = '= '.mysql_real_escape_string($parent);
			
		$opt = '';

		$data = mysql_query("SELECT id, nombre FROM faq_cat WHERE parent ".$parent." ORDER BY `order` ASC");
		while($d = mysql_fetch_assoc($data)) {

			$opt .= '<option value="'.$d['id'].'"'.($d['id'] == $selected ? ' selected="selected"' : '').'>'.str_repeat('&nbsp;', $n*3).$d['nombre'].'</option>';
			$opt .= categoriesSelect($selected, $d['id'], $n+1);
		
		}
		
		return $opt;
		
	}
?>