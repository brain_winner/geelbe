<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("miscreditos"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("misinvitados"));
        $postURL = Aplicacion::getIncludes("post", "invitacionmanual");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    } 	
    try
    {	
      $disponibles = dmMisCreditos::getCreditoDisponible(Aplicacion::Decrypter($_SESSION["User"]["id"]));
      $potenciales = dmMisCreditos::getCreditoPotencial(Aplicacion::Decrypter($_SESSION["User"]["id"]), Aplicacion::Decrypter($_SESSION["User"]["email"]));		
    }
    catch(exception $e)
    {
        die(print_r($e));
    }

	try {
		$isPageOk = true;
		$pageException = "Exception";
		
		// Validaciones de parametros basicas
        if (isset($_GET["c"]) && $_GET["c"]!="") { // c Seteado
        	if (!ValidatorUtils::validateNumber($_GET["c"])) { // c Numerico
				$isPageOk = false;
				$pageException = "c is not a number";
			}
        }
        if (isset($_GET["s"]) && $_GET["s"]!="") { // s Seteado
			if ($_GET["s"]!="ASC" && $_GET["s"]!="DESC") { // s Valido
				$isPageOk = false;
				$pageException = "s is not valid";
			}
        }
		
		if (!$isPageOk) {
			throw new Exception($pageException);
		}
    }
    catch(Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
		header('HTTP/1.1 404 Not Found');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
    
    extract($_POST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/botones.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/micuenta/js/js.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
<? require("../menuess/menu_log.php") ?>
<div id="container">
  <div id="contenido-top">

  </div>
  <div id="contenido">
	<div id="central">
	  <h2><img src="<?=UrlResolver::getImgBaseUrl("front/images/calculator_accept_48.png");?>" alt="Mi cr�dito" width="48" height="48" align="middle" /> <strong>Mi cr�dito</strong></h2>
	  <p>&nbsp;</p>
	  <h2>El cr�dito acumulado en tu cuenta es de: <strong><?=Moneda($disponibles)?>
	    </strong></h2>
	  <h4>POR CADA UNO DE TUS INVITADOS QUE SE REGISTRE Y REALICE SU PRIMERA COMPRA MAYOR A ($50.000) EN  GEELBE, RECIBES UN BONO DE&nbsp; $10.000 &nbsp;EN TU CUENTA</h4>

	  <h4>&nbsp;</h4>
	  <h2>Dinero potencial para acreditarse en tu cuenta: <strong><?=Moneda($potenciales)?></strong></h2>
	  <h4>DINERO QUE  SE ACREDITARA EN TU CUENTA EN EL MOMENTO QUE LOS AMIGOS QUE INVITASTE SE  REGISTREN Y REALICEN SU PRIMERA COMPRA MAYOR A $50.000.</h4>
	  <h4>&nbsp;</h4>
	  <h2>Estado de tus cr�ditos</h2>
	  <p>Revisa  el estado de las invitaciones que enviaste a tus amigos.</p>
	  <p>&nbsp;</p>
      <?
        $count = dmMisCreditos::CountPotencialesgetByIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
	    $listPager = new ListadoPaginator($count);
        $viewUsuario_PotencialesCreditos = dmMisCreditos::VIEWPotencialesgetByIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]), isset($PaginadortxtFiltro)?$PaginadortxtFiltro:$_SESSION["PALABRA_CLAVE"], (isset($_GET["c"])?$_GET["c"]:2), (isset($_GET["s"])?$_GET["s"]:"ASC"), $listPager->getOffset(), $listPager->getItemsPerPage());
       ?>
       <form onSubmit="Recordar(this)" id="frmInvitacionManual" name="frmInvitacionManual" target="iInvitacionManual" action="../../../<?=Aplicacion::getDirLocal().$postURL["recordarinvitacion"]?>" method="post">
	  <table border="0" cellpadding="0" cellspacing="0" bgcolor="F0F0F0">
        <tr>
          <td width="1%"><h3><input type="checkbox" id="chkAll" name="chkAll" title="Chequear todos" onClick="AllChecks(this.checked);"/></h3></td>
          <td width="30%"><h3><a href="mi-credito.php?act=1&c=2&s=<?=$Orden?>">Tus Invitados</a></h3></td>
          <td width="15%"><h3><a href="mi-credito.php?act=1&c=4&s=<?=$Orden?>">Registro</a></h3></td>
          <td width="30%"><h3><a href="mi-credito.php?act=1&c=5&s=<?=$Orden?>">Compra mayor a $50.000</a></h3></td>
          <td width="25%"><h3>Tu cr�dito </h3></td>
        </tr>
       <?
       foreach($viewUsuario_PotencialesCreditos as $usuarios_potenciales)
       {
           ?>
           <tr>
            <td><input type="checkbox" id="frmEmail[]" name="frmEmail[]" value="<?=$usuarios_potenciales["Email"]?>" /> </td>
            <td><?=$usuarios_potenciales["Email"]?></td>
            <?
                if($usuarios_potenciales["Registrado"] != "Pendiente" && $usuarios_potenciales["SoyPadrino"] == 0)
                {
                    ?><td colspan="3"><p align='center'><b>Realizado - El usuario eligi� a otro referenciador.</b></p></td><?
                }
                else
                {
                 ?>
                    <td><p align='center'><?=$usuarios_potenciales["Registrado"]?></p></td>
                    <td><p align='center'><?=$usuarios_potenciales["Compro"]?></p></td>
                    <?
                        if($usuarios_potenciales["Compro"] == "Concretada")
                        {
                            if($usuarios_potenciales["Regla"] == "Aplicada")
                            {
                                ?><td><p align='center'><b><?=Moneda($usuarios_potenciales["BonoA"])?>&nbsp;acreditado</b></p></td><?
                            }
                            else
                            {
                                ?><td><p align='center'><?=Moneda($usuarios_potenciales["Bono"])?>&nbsp;potencial**</p></td><?
                            }
                        }
                        else
                        {
                            ?><td><p align='center'><?=Moneda($usuarios_potenciales["Bono"])?>&nbsp;potencial</p></td><?
                        }
                    ?>
                <?
                }
            ?></tr><?
       }
       ?>
       <tr>
       <td colspan="100%"><!--<input type="button" id="btnRecordar" name="btnRecordar" value="Recordar a los referenciados seleccionados" onclick="document.forms['frmInvitacionManual'].onsubmit()"/>-->
	   
	   <table style="width: 100%;">
	     <tbody>
           <tr>                 
             <td width="100%" colspan="2">
               <a href="/<?=$confRoot[1]?>/front/micuenta/mi-credito.php">Primera</a>
               <? for ($i = 1; $i <= $listPager->getNumberOfPages(); $i++) {
               	    if($i == $listPager->getActualPage()) {
               ?>
		               	&nbsp;<a><?=$i?></a>
		       <?
               	    } else {
               ?>
		               	&nbsp;<a href="/<?=$confRoot[1]?>/front/micuenta/mi-credito.php?page=<?=$i?>"><?=$i?></a>
		       <?
               	    }
               ?>
                
               <? } ?>
               <a href="/<?=$confRoot[1]?>/front/micuenta/mi-credito.php?page=<?=$listPager->getNumberOfPages()?>">&Uacute;ltima</a>
             </td>
           </tr>
         </tbody>
       </table>
	   
	   <table class="btn btnRed normal XXXL" cellpadding="0" cellspacing="0" border="1px"> 
				<tr>
					<td>
						<a href="javascript:document.forms['frmInvitacionManual'].onsubmit()" ><strong>Recu&eacute;rdales tu invitaci&oacute;n</strong></a>
					</td>
				</tr>
			</table>	
	   
	   </td> 
       </tr>
       <tr>
       <td colspan="100%"><strong>** cr&eacute;dito pendiente de bonificaci&oacute;n</strong></td>
       </tr>	   
     </table>
     </form>
     <iframe id="iInvitacionManual" name="iInvitacionManual" style="display:none"></iframe>
	  <div class="grisado">
      <h2><a href="/<?=Aplicacion::getDirLocal()?>front/referenciados/?act=4"><strong>Gana dinero invitando a tus amigos a Geelbe!</strong></a></h2>
	    <p>Con nuestro sistema de recomendaci&oacute;n puedes invitar a tus amigos  para que se registren en Geelbe y acreditar dinero en tu cuenta para utilizarlo  en tus futuras compras. Mientras m&aacute;s amigos recomiendes en Geelbe m&aacute;s  beneficios obtendr&aacute;s como miembro.</p>
      </div>
	  <p><br />
      </p>
	</div>
  <div id="menuleft">
	    <? 
		$act=1;
		require("../menuess/micuenta.php")
		?>
  </div>
</div>
  <div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
</body>
</html>
