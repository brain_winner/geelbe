<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados"));
        $postURL = Aplicacion::getIncludes("post", "invitacionmanual");
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }   
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?
    Includes::Scripts();
?>
<title><?=Aplicacion::getParametros("info", "nombre");?> Invitacion manual</title>
<link href="<?=UrlResolver::getCssBaseUrl("front/referenciados/invitacss.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/micuenta/js/js.js");?>" type="text/javascript"></script>

</head>
<body>
<form id="frmInvitacionManual" name="frmInvitacionManual" target="iInvitacionManual" action="../../../<?=Aplicacion::getDirLocal().$postURL["invitacionmanual"]?>" method="post">
    <input type="hidden" id="frmEmail[]" name="frmEmail[]" value="<?=$_GET["email"]?>"/>
</form>
<script>document.forms["frmInvitacionManual"].submit();</script>
<iframe id="iInvitacionManual" name="iInvitacionManual" style="display:none"></iframe>
</body>
</html>