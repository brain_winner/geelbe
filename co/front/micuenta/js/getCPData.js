function showAndHide() {
	if(document.getElementById('buscarManual').checked) {
		document.getElementById('manual').style.display = 'block';
		document.getElementById('porCP').style.display = 'none';
	} else {
		document.getElementById('manual').style.display = 'none';
		document.getElementById('porCP').style.display = 'block';
	}
	
}

function setCP(cod, cbxciudad, cbxprov, hide, setCiudad) {
	setCPReal(cod);
	
	ci = document.getElementById(cbxciudad)
	pr = document.getElementById(cbxprov)
	
	$("."+hide).css("display", "none")
	
	$.get("/co/front/micuenta/getCPData.php", { cp: cod },
		function(data){
	
			if(data != 0) {
				data = data.split("\n");
				
				for(i = 0; i < pr.options.length; i++)
					if(pr.options[i].value == data[0]) {
						pr.options[i].selected = true
						break;
					}
					
				getCiudades(data[0], cbxciudad, data[1], setCiudad)
									
			} else {
				alert('No se encontr� el CP');
			}
			
			$("."+hide).css("display", "block")
		}
	);	
}

function getCiudades(prov, cbxciudad, select, getCPS) {
	ci = document.getElementById(cbxciudad)
	ci.onchange = function() {
		getCP(prov, this.options[this.selectedIndex].value)
	}
	$(".city").css("display", "block")
	
	while(ci.firstChild) {
		ci.removeChild(ci.firstChild)
	}
			
	var opt = document.createElement('option');
	opt.value = 0;
	opt.appendChild(document.createTextNode('Ciudades'))		
	ci.appendChild(opt)
	
	$.get("/co/front/micuenta/getCiudades.php", { id: prov },
		function(data){
	
			data = data.split("\n");
			
			for(i=0; i < data.length; i++) {
			
				d = data[i].split(':');
				if(d.length == 2) {
					var opt = document.createElement('option');
					opt.value = d[0];
					opt.appendChild(document.createTextNode(d[1]))
					if(select == d[0])
						opt.selected = true;
						
					ci.appendChild(opt)
				}
			
			}
		
		}
	);	
	

	if(getCPS) {	
		$.get("/co/front/micuenta/getCPs.php", { ide: prov },
			function(data){
		
				data = data.split("\n");
				
				var ci = document.getElementById('cbxCP');
				ci.onchange = function() {
					setCP(this.options[this.selectedIndex].value, 'txtpbl', 'cbxprov', 'hide', false);
				}
				
				while(ci.firstChild) {
					ci.removeChild(ci.firstChild)
				}
				
				for(i=0; i < data.length; i++) {
				
					d = data[i];
					var opt = document.createElement('option');
					opt.value = d;
					opt.appendChild(document.createTextNode(d))					
					ci.appendChild(opt)
				
				}
			
			}
		);	
	}
}

function getCP(prov, city) {
	
	$.get("/co/front/micuenta/getCP.php", { ide: prov, idc: city },
		function(data){
			cp = document.getElementById('txtcp');
			cp.value = data;		
			
			cp = document.getElementById('cbxCP');
			for(i = 0; i < cp.options.length; i++) {
				if(cp.options[i].value == data) {
					cp.options[i].selected = true;
					continue;
				}
			}
			
			setCPReal(data)
		}
	);	
}

function setCPReal(cod) {
	if(cod.substr(0, 1) == '0')
		cod = cod.substr(1, cod.length);

	document.getElementById('cpReal').value = cod
}