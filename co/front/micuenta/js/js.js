function ErrorMSJ(titulo, code)
{
    oCortina.showError(Array(arrErrores["MICUENTA"][code]), titulo);
}
function AllChecks(estado)
{
            var checks_elegidos = new Array();
            var inputs = document.getElementsByTagName("input");
            var i;
            for(i=0; i<inputs.length; i++)
            {
                if (inputs[i].getAttribute("type") == "checkbox")
                {
                    inputs[i].checked = estado;
                }
            }
}
function ValidarForm(frm)
{
    try
    {
		var Nombre = document.getElementById("frmDaU[txtNombre]").value;
		var Apellido = document.getElementById("frmDaU[txtApellido]").value;
        var NroDoc = document.getElementById("frmDaU[txtNroDoc]").value;
		var Domicilio = document.getElementById("frmDi1[txtDomicilio]").value;
		var Ciudad = document.getElementById("idCiudad").value;
		var Provincia = document.getElementById("idProvincia").value;
        var CodTelefono = document.getElementById("frmDaU[txtCodTelefono]").value;
        var Telefono = document.getElementById("frmDaU[txtTelefono]").value;
        //var CodCelular = document.getElementById("frmDaU[txt_CelularCodigo]").value;
        var Celular = document.getElementById("frmDaU[txt_CelularNumero]").value;

        var vErrores = new Array();
		if (Nombre.length == 0)
            vErrores.push("Por favor, ingrese su Nombre");
		if (Apellido.length == 0)
            vErrores.push("Por favor, ingrese su Apellido");
		if (Domicilio.length == 0)
            vErrores.push("Por favor, ingrese su Domicilio");		
		if (Ciudad == "" || Ciudad == "0")
            vErrores.push("Por favor, ingrese su Ciudad");	
		if (Provincia == "" || Provincia == "0")
            vErrores.push("Por favor, ingrese su Departamento");			
        if (!esInteger(NroDoc) || NroDoc.length == 0)
            vErrores.push("N�mero de documento de identidad s�lo puede ser un n�mero o no puede quedar vac�o");
        if (!esInteger(CodTelefono) || CodTelefono.length == 0 || Telefono.length==0 ||!esInteger(Telefono))
            vErrores.push("N�mero de tel�fono s�lo puede ser un n�mero o no puede quedar vac�o");
        if (Celular.length > 0 && !esInteger(Celular))
            vErrores.push("N�mero de celular s�lo puede ser un n�mero");
                        
	    if (frm.cmb_nacimientoAnio.value == 0 || frm.cmb_nacimientoMes.value == 0 ||  frm.cmb_nacimientoDia.value == 0)
        {
            vErrores.push(arrErrores["REGISTRO"]["FECHA DE NACIMIENTO"]);
        }
        else
        {
             if (!(EsFechaValida(frm.cmb_nacimientoDia.value, frm.cmb_nacimientoMes.value, frm.cmb_nacimientoAnio.value)))
            {
                vErrores.push(arrErrores["REGISTRO"]["FECHA INVALIDA"]);
            }
            else if(calcular_edad(frm.cmb_nacimientoAnio.value,frm.cmb_nacimientoMes.value,frm.cmb_nacimientoDia.value) < 18)
            {
                vErrores.push(arrErrores["REGISTRO"]["MENOR DE EDAD"]);
            }
        }                                  
                
       if (vErrores.length > 0) {
            var lista = '<ul>';
            for(i = 0; i < vErrores.length; i++)
            	lista += '<li>'+vErrores[i]+'</li>';
            lista += '</ul>';            
            list = document.getElementById("error-msj");
            list.style.display = "block";
            list.innerHTML = lista
        } else
            frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}

function ValidarFormContrasena(frm) {
	try
    {
		var clave1 = document.getElementById("clave1").value;
		var clave2 = document.getElementById("clave2").value;

        var vErrores = new Array();
		if (clave1.length < 6)
            vErrores.push("La clave debe tener al menos seis caracteres.");
		else if (clave1 != clave2)
            vErrores.push("Las claves ingresadas no coinciden.");
                                     
       if (vErrores.length > 0) {
            var lista = '<ul>';
            for(i = 0; i < vErrores.length; i++)
            	lista += '<li>'+vErrores[i]+'</li>';
            lista += '</ul>';            
            list = document.getElementById("error-msj");
            list.style.display = "block";
            list.innerHTML = lista
        } else
            frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}

function Recordar(frm)
{
    try
    {
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}
function iniciar(pais,prov,celue)
{
    setProvinciaDir(prov);
    setPaisDir(pais);
    setCeluEmpresa(celue); 
}

function setProvinciaDir(prov)
{
	if(document.getElementById('idProvincia')) {
		document.getElementById('idProvincia').value = prov;    
	}
}
function setPaisDir(pais)
{
	if(document.getElementById('frmDi1[cbxPais]')) {
		document.getElementById('frmDi1[cbxPais]').value = pais;
	}
}
function setCeluEmpresa(celue){
	if(document.getElementById('frmDaU[cmb_CelularComp]')) {
		document.getElementById('frmDaU[cmb_CelularComp]').value = celue;
	}
}                                  


function calcular_edad(ano, mes, dia){

    //calculo la fecha de hoy
    hoy=new Date()

    if (isNaN(ano))
       return false
    if (isNaN(mes))
       return false
    if (isNaN(dia))
       return false
    //resto los a�os de las dos fechas
    edad=hoy.getFullYear()- ano - 1; //-1 porque no se si ha cumplido a�os ya este a�o

    //si resto los meses y me da menor que 0 entonces no ha cumplido a�os. Si da mayor si ha cumplido
    if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0
       return edad
    if (hoy.getMonth() + 1 - mes > 0)
       return edad+1
    //entonces es que eran iguales. miro los dias
    //si resto los dias y me da menor que 0 entonces no ha cumplido a�os. Si da mayor o igual si ha cumplido
    if (hoy.getUTCDate() - dia >= 0)
      return edad + 1

    return edad
}