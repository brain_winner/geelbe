<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));
        $postURL = Aplicacion::getIncludes("post", "micuenta");
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    } 
    try
    {
       $objUsuario = new MySQL_micuenta();
       $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
       
       $fbkId = dmMicuenta::GetFacebookId(Aplicacion::Decrypter($_SESSION["User"]["id"]));
    }
    catch(exception $e)
    {
        die(print_r($e));
    }   
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="../geelbe_new.css" rel="stylesheet" type="text/css" />
<!--link href="<?=UrlResolver::getCssBaseUrl("front/referenciados/css/invites_loged.css");?>" rel="stylesheet" type="text/css" /-->
<!--link href="<?=UrlResolver::getCssBaseUrl("front/micuenta/css/mi-cuenta.css");?>" rel="stylesheet" type="text/css" /-->
<link href="css/mi-cuenta.css" rel="stylesheet" type="text/css" />
<link href="http://css.static.geelbe.com/co/front/botones.css" rel="stylesheet" type="text/css">

<script src="<?=UrlResolver::getJsBaseUrl("front/micuenta/js/js.js");?>" type="text/javascript"></script>
<script src="<?=UrlResolver::getJsBaseUrl("js/jquery.js");?>" type="text/javascript"></script>
<script src="<?=UrlResolver::getJsBaseUrl("front/micuenta/js/getCPData.js");?>" type="text/javascript"></script>

<script type="text/javascript">

	<?php 
		$user_id = Aplicacion::Decrypter($_SESSION['User']['id']);
		$hash = md5($user_id . "pajarito");
	?>

	function updateUserInfoWithFbk() {
		FB.login(function(response) {
			if (response.session) {
				if (response.perms) {
					var session = FB.getSession();
					window.location.href="/appfacebook/tab/update_user_info.php?" + "fb_sig_session_key= " +
						session.session_key + "&fb_sig_user=" + session.uid + "&fb_sig_app_id=130962696927517" +
						"&access_token=" + session.access_token + "&redirection=micuenta&user_id=<?=$user_id?>&hash=<?=$hash?>";
				} else {
					alert("Debes aceptar los permisos para poder actualizar tus datos en Geelbe.")
				}
			} else {
				alert("Debes aceptar los permisos para poder actualizar tus datos en Geelbe.")
			}
		}, {perms:'user_birthday,email,publish_stream,offline_access,rsvp_event'});
	}
</script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
 <? require("../menuess/menu_log.php") ?>
<div >
  <div id="container">
  	<img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/p-titulo.jpg");?>" />
    <div class="bloques perfil">
    	<h2 style="margin:0 0 5px 0;">Mant�n actualizados tus datos personales</h2>
    	<? $nombreYApellido = stripslashes($datosUser["Nombre"])." ".stripslashes($datosUser["Apellido"]);?>
        <div id="datospersonales" class="col_izquierda">
            <? if(isset($fbkId) && $fbkId != null) {?>
              <img id="personalimg" src="http://graph.facebook.com/<?=$fbkId?>/picture" title="<?=$nombreYApellido?>" alt="<?=$nombreYApellido?>" />
            <? } ?>
        	<p id="datoscontainer"><?=$nombreYApellido?><br />email: <?=$objUsuario->getNombreUsuario();?><br />Tel.: <?=$objUsuario->getDatos()->getCodTelefono()?> <?=$objUsuario->getDatos()->getTelefono()?></p>
        </div>
        <div class="col_derecha">
        	<a href="mi-perfil.php"><img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/cta.jpg");?>" style="margin:10px 0 0 70px; cursor:pointer;" /></a>
        	<a href="javascript:;" onclick="updateUserInfoWithFbk()"><img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/Fb-cnnct-bot.jpg");?>" style="margin:10px 0 0 70px; cursor:pointer;" /></a>
        </div>
        <div class="unfloat"></div>
    </div>
	<div id="edicion">
	</div>
    <div>
    	<div class="col_izquierda">
        	<div class="bloques sections">
            	<div class="col_izquierda">
                	<h2>MI CR�DITO</h2>
                    <p>Por cada invitado que<br>
                    se registre y realice<br>
                    su primera compra<br>
                    en Geelbe, recibir�s<br>
                    $10.000 de cr�dito en tu cuenta.<br>
                    Consulta el estado<br> de tu cr�dito en Geelbe.</p>
                    <a href="/co/front/micuenta/mi-credito.php">Ingresar <img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/seguir.jpg");?>" /></a>                </div>
                <div class="col_derecha">
                	<img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/p-cred.jpg");?>" />                </div>
            </div>
   	  <div class="bloques sections">
            	<div class="col_izquierda">
                	<h2>MIS PEDIDOS</h2>
                    <p>Ingresando en esta secci�n, puedes hacer el seguimiento del env�o de cada una de tus compras en Geelbe.</p>
                    <a href="/co/front/micuenta/mispedidos.php">Ingresar <img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/seguir.jpg");?>" /></a>                </div>
                <div class="col_derecha">
                	<img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/p-pedidos.jpg");?>" />                </div>
            </div>
	  </div>
        <div class="col_derecha">
        	<div class="bloques sections">
            	<div class="col_izquierda">
                	<h2>INVITA A UN AMIGO</h2>
                    <p>Invita a tus amigos a registrarse. Geelbe les obsequiar� dinero a ti y a ellos para que lo utilicen en sus compras.</p>
                    <a href="/co/front/referenciados/">Ingresar <img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/seguir.jpg");?>" /></a>                </div>
                <div class="col_derecha">
                	<img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/p-invite.jpg");?>" />                </div>
            </div>
   	  <div class="bloques sections">
            	<div class="col_izquierda">
                	<h2>MIS CONSULTAS</h2>
                    <p>Si quieres realizar alguna consulta, puedes enviarnos un mensaje. Nuestro equipo de atenci�n al cliente te responder� a la brevedad.</p>
                    <a href="/co/front/micuenta/misconsultas.php?act=6">Ingresar <img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/seguir.jpg");?>" /></a>                </div>
                <div class="col_derecha">
                	<img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/p-consultas.jpg");?>" />                </div>
            </div>
      </div>
    </div>
	</div>
</div>

</div>

</div>
<? require("../menuess/footerNew.php")?>
<div id="fb-root"></div>
<script>
	window.fbAsyncInit = function() {
		FB.init({appId: '130962696927517', status: true, cookie: false,
						 xfbml: true});
	};
	(function() {
		var e = document.createElement('script'); e.async = true;
		e.src = document.location.protocol +
			'//connect.facebook.net/es_LA/all.js';
		document.getElementById('fb-root').appendChild(e);
	}());
</script>
<script>
    <?
      if ($objUsuario->getDirecciones(0)->getIdProvincia()>0)
      {
        $Provincia = $objUsuario->getDirecciones(0)->getIdProvincia();
      }
      else
      {
        $Provincia = 0;
      }
       if ($objUsuario->getDirecciones(0)->getIdPais()>0)
      {
        $Pais = $objUsuario->getDirecciones(0)->getIdPais();
      }
      else
      {
        $Pais = 0;
      }
      if($objUsuario->getDatos()->getceluempresa()>0)
      {
        $celuempresa = $objUsuario->getDatos()->getceluempresa();
      }
      else
      {
        $celuempresa = 0;
      }
    ?>  
   iniciar(<?=$Pais?>,<?=$Provincia?>,<?=$celuempresa?>);
   
  function actualizarCiudades() {
	   $.ajax({ url: "<?=Aplicacion::getRootURl()?>logica/micuenta/actionform/obtener_ciudades.php?IdProvincia="+$("#idProvincia").val(), context: document.body, success: function(data){
 			$("#idCiudad").html(data);
	   }});
   }

   
</script>    
</body>
</html>
