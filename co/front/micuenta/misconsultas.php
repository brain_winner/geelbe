<?
	if(!isset($_GET['act']) || !is_numeric($_GET['act']))
		die;

    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
        $postURL = Aplicacion::getIncludes("post", "micuenta");
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }   
    try
    {
       $objUsuario = new MySQL_micuenta();
       $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
    }
    catch(exception $e)
    {
        die(print_r($e));
    }   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/botones.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/highslide.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/micuenta/js/js.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
 <? require("../menuess/menu_log.php") ?>
<div id="container">
  <div id="contenido-top">

  </div>
  <div id="contenido">
    <div id="central">
      <h2><img src="<?=UrlResolver::getImgBaseUrl("front/images/user_edit_48.png");?>" alt="Mi Perfil" width="48" height="48" align="middle" /> <strong>Mis Consultas</strong></h2>
      <h3>Información sobre tus consultas en Geelbe </h3>
        <table cellspacing="0" cellpadding="2" border="0">
          <tbody>                     
            <!--<tr>
              <td height="10"colspan="3" background="../images/gris-top1.gif" scope="col"></td>
            </tr>-->
            <tr>
                <td>
            <?php
                extract($_POST);
                $dtMensajes = dmMensajes::getMensajes_ByIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]), isset($PaginadortxtFiltro)?$PaginadortxtFiltro:"");
                $action = "misconsultas.php?act=".$_GET["act"];
                $PalabraClave = isset($PaginadortxtFiltro)?$PaginadortxtFiltro:$_SESSION["PALABRA_CLAVE"];
                $_SESSION["PALABRA_CLAVE"] = $PalabraClave;
                if ($MAX_X_PAG)
                    $_SESSION["MAX_X_PAG"] = $MAX_X_PAG;
                $objPaginador = new Paginador(isset($POSICION)?$POSICION:0, $dtMensajes,$action,true);
                $objPaginador->setMaxXPag(isset($_SESSION["MAX_X_PAG"])?$_SESSION["MAX_X_PAG"]:Aplicacion::getParametros("paginador", "iniciar"));
                $objPaginador->setPagina((isset($PAG)?$PAG:1));
                $objPaginador->setSeleccion(isset($_SESSION["MAX_X_PAG"])?$_SESSION["MAX_X_PAG"]:Aplicacion::getParametros("paginador", "iniciar"));
                $objPaginador->setPalabraClave($PalabraClave);
                $objPaginador->show();
                $dtMensajes = $objPaginador->getArrayPaginado();
                ?>
                        </td>
                    </tr>
                <?
                $i = 0;
                
                foreach($dtMensajes as $Mensaje)
                {
                    $i++;
                    ?>
                        <tr>
                                <td background="<?=UrlResolver::getImgBaseUrl("front/images/gris-top1.gif");?>">
                                    <span style="color:#FF0000"><a href="../mensajes/mensajes.php?IdMensaje=<?=$Mensaje["idMensaje"]?>" onClick="return hs.htmlExpand(this, { objectType: 'iframe' } )" title="Leer respuesta"><?=$i + (($objPaginador->getPagina() - 1)* $objPaginador->getMaxXPag()) . ". " . stripslashes($Mensaje["Asunto"])?></a></span><span> (<?=count(dmMensajes::getMensajesRespondidos_ByIdMensaje($Mensaje["idMensaje"]))?> respuestas)</span>
                                </td>
                        </tr>
                        <tr>
                            <td><span style="color:#FF0000; font-size:9px;"><i>&nbsp;&nbsp;Preguntado el <?=$Mensaje["Fecha"]?></i></span></td>
                        </tr>
                    <?
                }
            ?>
            <tr>
                <table class="btn btnRed normal XXXL" cellpadding="0" cellspacing="0" border="1px"> 
					<tr>
						<td>
							<a href="../contacto-socio/"><strong>Realizar nueva consulta</strong></a>
						</td>
					</tr>
				</table>	
            </tr>
        </table>
      </form>
      <p>&nbsp;</p>
    </div>
  <div id="menuleft">
        <? 
		$act=6;
		require("../menuess/micuenta.php")
		?>
    </div>
</div>
  <div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
<iframe id="iMiCuenta" name="iMiCuenta" style="display:none">
</iframe>   
</body>
</html>