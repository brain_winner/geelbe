<?
    try
    {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("mispedidos", "clases"));
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }   
    try
    {
        extract($_POST);
    	//Aplicacion::CargarIncludes(Aplicacion::getIncludes("verificaDineroMail", "clases"));
        $dtUsuarios = dmMisPedidos::getPedidoByIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]), isset($PaginadortxtFiltro)?$PaginadortxtFiltro:"");
    }
    catch(exception $e)
    {
        die(print_r($e));
    }

    function mes($date) {
    	$en = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    	$es = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	    return str_replace($en, $es, $date);
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/micuenta/js/js.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
 <? require("../menuess/menu_log.php") ?>
<div id="container">
  <div id="contenido-top">

  </div>
  <div id="contenido">
	<div id="central">
	  <h2><img src="<?=UrlResolver::getImgBaseUrl("front/images/calculator_accept_48.png");?>" alt="Mi cr�dito" width="48" height="48" align="middle" /> <strong>Mis pedidos</strong></h2>
	  <p>&nbsp;</p>
	  <h2>&nbsp;</h2>
      <?
        $action = "mispedidos.php?act=".$_GET["act"];
        $objPaginador = new Paginador(isset($POSICION)?$POSICION:0, $dtUsuarios,$action,true);
        $objPaginador->setMaxXPag(isset($MAX_X_PAG)?$MAX_X_PAG:Aplicacion::getParametros("paginador", "iniciar"));
        $objPaginador->setPagina(isset($PAG)?$PAG:1);
        $objPaginador->setSeleccion(isset($cbxPaginasPaginador)?$cbxPaginasPaginador:Aplicacion::getParametros("paginador", "iniciar"));
        $objPaginador->setPalabraClave(isset($PaginadortxtFiltro)?$PaginadortxtFiltro:"");
        $objPaginador->show();
        $dtUsuarios = $objPaginador->getArrayPaginado();
      ?>
	  <table border="0" cellpadding="0" cellspacing="0" bgcolor="FFFFFF">
        <tr>
            <td width="40" valign="top" bgcolor="#F5F5F5"><h2>No</h2></td>
            <td width="62" valign="top" bgcolor="#F5F5F5"><h2>Fecha</h2></td>
            <td width="107" valign="top" bgcolor="#F5F5F5"><h2>Campa�a</h2></td>
            <td width="77" valign="top" bgcolor="#F5F5F5"><h2>Importe</h2></td>
            <td width="125" valign="top" bgcolor="#F5F5F5"><h2> Entrega</h2></td>
            <td width="141" valign="top" bgcolor="#F5F5F5"><h2>Estado</h2>
        </tr>
<?
 
            foreach($dtUsuarios as $fila)
            {
                ?>
                    <tr> <?
                    		$camp = dmMisPedidos::getPedidoCampania($fila["IdPedido"]);
                            if (dmCampania::EsCampaniaAccesible($camp) || ExisteEnArray($fila["IdEstadoPedidos"], array("2","3","6")))
                            { 
	                            
	                            $camp = dmCampania::getByIdCampania($camp);
	                            
	                            $fecha = explode('/', $fila["Fecha"]);
	                            $fecha = mktime(0, 0, 0, $fecha[1], $fecha[0], $fecha[2]);

	                            $in = $camp->getTiempoEntregaInCorte($fecha);
	                            $fn = $camp->getTiempoEntregaFnCorte($fecha);
                            ?>
                        <td valign="top"><a href="detallepedido.php?Id=<?=$fila["IdPedido"]?>"><?=str_pad($fila["IdPedido"],Aplicacion::getParametros("pedidos", "cantidad"),0,0)?></a></td>
                        <td valign="top"><?=$fila["Fecha"]?></td>
                        <td valign="top"><h3><?=$fila["Campania"]?></h3>
                        <h4>&nbsp;</h4></td>
						<td valign="top"><p><?=Moneda($fila["Importe"])?></p></td>
                        <td valign="top"><?=mes(strftime("%d de %B", strtotime($in)))?> al <?=mes(strftime("%d de %B", strtotime($fn)))?></td>
                        <td valign="top">
                                <?
                            	if($fila['IdEstadoPedidos']==10){
						        ?>
				                <p>Pendiente de aprobaci&oacute;n</p>
						        <?
						        }
                                else if ($fila['IdEstadoPedidos']==0)
                                {
                                    $cartUrlProtocol = "https://";
							        if($_SERVER['HTTP_HOST'] == "localhost") {
										$cartUrlProtocol = "http://";
									}
                                    ?>
                                    <p align="center">
                                    	<a href="<?=$cartUrlProtocol.$_SERVER['HTTP_HOST']."/".$confRoot[1]."/ssl/carrito/step_1.php"?>?IdPedido=<?=$fila["IdPedido"];?>">Reanudar pago</a>
                                    	<?php if(dmMisPedidos::puedeAnularPedido($fila['IdPedido'])): ?>
                                    	<a href="mispedidos.php?anular=<?=$fila["IdPedido"];?>">Anular pedido</a>
                                    	<?php endif; ?> 
                                    </p>
                                    <?
                                }
                                else
                                {   
                                	?>
                                	<p align="center"><?=$fila["Estado"]?></p>
                                	<?
                                }
                            }
                            else
                            {
                                ?>
                                    <td valign="top"><a href="detallepedido.php?Id=<?=$fila["IdPedido"]?>"><?=str_pad($fila["IdPedido"],Aplicacion::getParametros("pedidos", "cantidad"),0,0)?></a></td>
                                    <td valign="top"><?=$fila["Fecha"]?></td>
                                    <td valign="top"><h3><?=$fila["Campania"]?></h3>
                                    <h4>&nbsp;</h4></td>
                                    <td valign="top"><p><?=Moneda($fila["Importe"])?></p></td>
                                    <td valign="top">Cancelado</td>
                                    <td valign="top">
                                    <p>Campa&ntilde;a finalizada</p>
                                <?
                            }
                         ?>
                        </td>
                    </tr>
                <?
            }
        ?>
     </table>
      <br/>
     <p>Haciendo clic en el correo podr� ver el estado del env�o provisto por el proveedor.</p>
      <br/>
      <br/>

	  <p><br />
      </p>
	</div>
  <div id="menuleft">
	    <?
		$act = 3;
		require("../menuess/micuenta.php")
		?>

	</div>
</div>
  <div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
</body>
</html>
