<?
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("mispedidos"));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/clases/clsdescuentos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/datamappers/dmdescuentos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/dhl/datamappers/dmdhl.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
    }
    catch(exception $e) {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }	
    try {
		$isPageOk = true;
		$pageException = "Exception";
		
		// Validaciones de parametros basicas
        if (!isset($_GET["Id"])) { // Id Seteado
            $isPageOk = false;
			$pageException = "Id not setted";
		}
		else if (!ValidatorUtils::validateNumber($_GET["Id"])) { // Id Numerico
			$isPageOk = false;
			$pageException = "Id is not a number";
		}
		if (!$isPageOk) {
			throw new Exception($pageException);
		}
    }
    catch(Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
		header('HTTP/1.1 404 Not Found');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
    try
    {
        $Pedido = dmPedidos::getPedidoById($_GET["Id"]);
        if (Aplicacion::Decrypter($_SESSION["User"]["id"])!=$Pedido->getIdUsuario())
        {
            throw new ACCESOException("",505,505);
        }
    }
    catch(exception $e)
    {
        ?>
        <script>window.open('../accesodenegado/accesodenegado.php?errNo=<?=Aplicacion::Encrypter($e->getCodigo())?>&r=<?=Aplicacion::Encrypter($e->getTitulo())?>','_self');</script>
        <?
        exit;
    }
    try
    {
        $objPedido = dmPedidos::getPedidoById($_GET["Id"]);
        $Bonos = dmMisCreditos::VIEWgetByIdPedido($_GET["Id"]);        
        $objUsuario = dmMicuenta::GetUsuario($objPedido->getIdUsuario());
        $InfoPedido = dmPedidos::getInfoPedido($_GET["Id"]);
        $arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$objPedido->getIdCampania()."/imagenes/",3,array("jpg", "jpeg", "png", "gif"));
        
        $camp = dmMisPedidos::getPedidoCampania($_GET["Id"]);
        $camp = dmCampania::getByIdCampania($camp);
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    
    function mes($date) {
    	$en = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    	$es = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	    return str_replace($en, $es, $date);
    }
    
	$in = $camp->getTiempoEntregaInCorte(strtotime($objPedido->getFecha()));
	$fn = $camp->getTiempoEntregaFnCorte(strtotime($objPedido->getFecha()));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/micuenta/js/js.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
 <? require("../menuess/menu_log.php") ?>
<div id="container">
  <div id="contenido-top">

  </div>
  <div id="contenido">
    <div id="top">    
      <h1>Detalle del Pedido
      </h1>
      <div id="topright" >
        <div class="right" style="margin: -10px 5px 0 0;">

          <h1><img src="<?=UrlResolver::getJsBaseUrl("front/campanias/archivos/campania_".$objPedido->getIdCampania()."/imagenes/logo2.png");?>" alt="" />
          </h1>
        </div>
      </div> 
    </div>
    <div class="right" style="margin:20px 5px 0 0;">
          <h3>C�digo de Compra: <?=str_pad($objPedido->getIdPedido(),Aplicacion::getParametros("pedidos", "cantidad"),0,0)?> </h3>
    </div>
    <h3>Contenido de tu compra</h3>
    <img src="<?=UrlResolver::getImgBaseUrl("front/images/carrito-top.gif");?>" alt="" width="687" height="10" class="left" />
    <div class="carro">
      <table border="0" cellpadding="3" cellspacing="0" bgcolor="#FFFFFF" style="width:670px;">
          <tr>
            <td width="89" valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td width="399" valign="top" bgcolor="#F5F5F5"><h2>Producto</h2></td>
            <td width="64" valign="top" bgcolor="#F5F5F5"><h2>Cantidad</h2></td>
            <td width="84" valign="top" bgcolor="#F5F5F5"><h2>Importe</h2></td>
          </tr>
          <?            
            foreach($objPedido->getProductos() as $Articulo)
            {
                $objPP = dmProductos::getByIdCodigoProdInterno($Articulo->getIdCodigoProdInterno());
                $objProducto = dmProductos::getById($objPP->getIdProducto());
                $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($Articulo->getIdCodigoProdInterno());
                $arrThumbs = DirectorytoArray(Aplicacion::getRoot() . "front/productos/productos_".$objProducto->getIdProducto()."/thumbs", 3,array("jpg", "gif", "png"));
                ?>
                    <tr>
                      <td valign="top">
                     
                      </td>
                        <td valign="top"><h3><?=$objProducto->getNombre()?></h3>
                        <h4>
                            <?
                                foreach($Atributos as $atributo)
                                {
                                    echo $atributo["Nombre"]." = ".$atributo["Valor"] . " | ";
                                }
                            ?>
                        </h4></td>
                        <td valign="top"><p><?=$Articulo->getCantidad()?></p></td>
                        <td valign="top"><p>
                        <?=Moneda($Articulo->getPrecio() * $Articulo->getCantidad(),2);?></p></td>
                      </tr>
                <?
            }
            
            /* DESCUENTOS */
			$Descuento = $objPedido->getDescuento();
			if($Descuento > 0):			
				$objDescuento = dmDescuentos::getById($objPedido->getIdDescuento());

				if(is_object($objDescuento)) {
								
					if($objDescuento->getPorcentaje())
						$detalle = $objDescuento->getDescuento().'%';
					else
						$detalle = Moneda($objDescuento->getDescuento());
					
					$descripcion = $objDescuento->getDescripcion();	
				} else {
					$detalle = 'N/A';
					$descripcion = 'Descuento';
				}
					
				$SubTotal -= $Descuento;
				
		?>
		<tr>
          <td valign="top">
          
          </td>
            <td valign="top"><h3><?=$descripcion?></h3>
            <h4><?=$detalle?> de descuento</h4></td>
            <td valign="top"><p>1</p></td>
            <td valign="top"><p>
            - <?=Moneda($Descuento,2);?></p></td>
          </tr>
		<?php
            endif; 
			/* FIN DESCUENTOS */
            
            $BonoUsado = $Bonos[0]["Bono"];
          ?>
          <tr>
            <td valign="middle"><img src="<?=UrlResolver::getImgBaseUrl("front/images/home_next.png");?>" alt="" width="24" height="24" /></td>
            <td valign="top"><h3>Costo de Env�o</h3>
            <p>Todos los env&iacute;os de Geelbe son realizados por un servicio de mensajer&iacute;a independiente</p></td>
            <td valign="top">&nbsp;</td>
            <td valign="top"><?=Moneda($objPedido->getGastosEnvio())?></td>
          </tr>
          <tr>
            <td valign="top">&nbsp;</td>
            <td valign="top"><h3>Sub-Total de la Compra </h3></td>
            <td valign="top">&nbsp;</td>
            <td valign="top"><?=Moneda($objPedido->getTotal()+$BonoUsado)?></td>
          </tr>
          <tr>
            <td valign="top">&nbsp;</td>
            <td valign="top"><h3>Cr�ditos utilizados de tu cuenta</h3></td>
            <td valign="top">&nbsp;</td>
            <td valign="top"><h2><?=Moneda($BonoUsado)?></h2></td>
          </tr>
          <tr>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5"><h3 style="font-size:14px">TOTAL DE LA COMPRA </h3></td>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5"><h3 style="font-size:14px"><?=Moneda($objPedido->getTotal())?></h3></td>
          </tr>
          <tr>
            <td valign="top">&nbsp;</td>
            <td valign="top" colspan="3">
            	<img src="images/truck.png" alt="env�o" style="margin: 20px 10px 20px 0;float:left" />
            	<p style="margin-top:25px">Fecha estimada de entrega: del <?=mes(strftime("%d de %B", strtotime($in)))?> al <?=mes(strftime("%d de %B", strtotime($fn)))?></p>
            </td>
          </tr>
          <?php  if($objPedido->getNroEnvio() != '' && $objPedido->getNroEnvio() > 0): ?>
          <tr>
            <td></td>
            <td colspan="3">
              <span style="width:158px;display:block;float:left">Gu�a seguimiento:</span>
              <?=$objPedido->getNroEnvio()?>    Sigue tu pedido en <a target="_blank" href="http://www.servientrega.com/wps/portal/inicio">Servientrega.com</a>

            </td>
          </tr>
          <?php endif; ?>
           <tr>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5" colspan="3" style="padding:5px 0">
            	<h3 style="font-size:12px;color:black;float:left;width:160px">Informaci�n de pago</h3>
            	Forma de pago: <?=$InfoPedido["FormaPago"]["Descripcion"]?>
            </td>
          </tr>
          <tr>
          	<td colspan="4"></td>
          </tr>
          <tr>
          	<td></td>
          	<td colspan="3">
          		<span style="width:158px;display:block;float:left">Estado del pedido:</span>
          		<?=$InfoPedido["Estado"]["Descripcion"]?>
          	</td>
          </tr>
          <tr>
          	<td colspan="4"></td>
          </tr>
           <tr>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5" colspan="3" style="padding:5px 0">
            	<h3 style="font-size:12px;color:black;float:left;width:160px">Informaci�n de env�o</h3>
            </td>
          </tr>
          <tr>
          	<td></td>
          	<td colspan="3">
          		<span style="width:158px;display:block;float:left">Nombre:</span>
          		<?=$objUsuario->getDatos()->getNombre()?>
          	</td>
          </tr>
          <tr>
          	<td></td>
          	<td colspan="3">
          		<span style="width:158px;display:block;float:left">Apellido:</span>
          		<?=$objUsuario->getDatos()->getApellido()?>
          	</td>
          </tr>
          <tr>
          	<td></td>
          	<td colspan="3">
          		<span style="width:158px;display:block;float:left">Tel�fono:</span>
          		<?=$objUsuario->getDatos()->getCodTelefono()." - ".$objUsuario->getDatos()->getTelefono()?>
          	</td>
          </tr>
          <tr>
          	<td></td>
          	<td colspan="3">
          		<span style="width:158px;display:block;float:left">Tel�fono m�vil:</span>
          		<?=$objUsuario->getDatos()->getcelucodigo()." - ".$objUsuario->getDatos()->getcelunumero()?>
          	</td>
          </tr>
          <tr>
          	<td></td>
          	<td colspan="3">
          		<span style="width:158px;display:block;float:left">Direcci�n de env�o:</span>
          		<?=$objPedido->getDireccion()->getDomicilio()?> <?=$objPedido->getDireccion()->getNumero()?> <?=$objPedido->getDireccion()->getPiso()?>
          		CP <?=$objPedido->getDireccion()->getCP()?>
          	</td>
          </tr>
          <tr>
          	<td></td>
          	<td colspan="3">
          		<span style="width:158px;display:block;float:left">Ciudad:</span>
          		<?  
                    if($objPedido->getDireccion()->getIdCiudad() != "")
                    {
                        $Prov = dmMicuenta::GetCiudadById($objPedido->getDireccion()->getIdCiudad());
                        echo $Prov[0]["Descripcion"];
                    }
                ?>
          	</td>
          </tr>
          <tr>
          	<td></td>
          	<td colspan="3">
          		<span style="width:158px;display:block;float:left">Departamento:</span>
          		<?  
                    if($objPedido->getDireccion()->getIdProvincia() != "")
                    {
                        $Prov = dmMicuenta::GetProvinciasById($objPedido->getDireccion()->getIdProvincia());
                        echo $Prov[0]["Nombre"];
                    }
                ?>
          	</td>
          </tr>
          <tr>
          	<td colspan="4"></td>
          </tr>
     </table>
    </div>
</div>
  <div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
</body>
</html>