<?
	if(!isset($_GET['id']) || !is_numeric($_GET['id']))
		die;

    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));

		$oConexion = Conexion::nuevo();
		$oConexion->Abrir_Trans();
        $oConexion->setQuery('SELECT idCiudad, Nombre FROM dhl_ciudades WHERE idCiudad IN (SELECT idCiudad FROM dhl_cp WHERE IdEstado = '.mysql_real_escape_string($_GET['id']).')');
        $data = $oConexion->DevolverQuery();
        
        foreach($data as $i => $j)
        	echo $j['idCiudad'].':'.$j['Nombre']."\n";
        die;

    }
    catch(exception $e)
    {
        die();
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }   
?>
