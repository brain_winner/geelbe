<?
	if(!isset($_GET['cp']) || !is_numeric($_GET['cp']))
		die;

    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));

		$oConexion = Conexion::nuevo();
		$oConexion->Abrir_Trans();
        $oConexion->setQuery('SELECT idEstado, idCiudad FROM dhl_cp WHERE cp = '.mysql_real_escape_string($_GET['cp']));
        $data = $oConexion->DevolverQuery();
        
        if(count($data) == 0)
        	echo 0;
        else {
        	echo $data[0]['idEstado']."\n";
        	echo $data[0]['idCiudad']; //Ciudad
        }        	
        
        die;

    }
    catch(exception $e)
    {
        die();
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }   
?>
