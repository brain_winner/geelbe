<?php
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("mispedidos", "clases"));
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
    }
    catch(exception $e)
    {
		header('HTTP/1.1 404 Not Found');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
    try {
        ValidarUsuarioLogueado();
		$id = Aplicacion::Decrypter($_GET["Id"]);
		$isPageOk = true;
		$pageException = "Exception";
		
		// Validaciones de parametros basicas
        if (!isset($id)) { // $id Seteado
            $isPageOk = false;
			$pageException = "IdCampania not setted";
		}
		else if (!ValidatorUtils::validateNumber($id)) { // $id Numerico
			$isPageOk = false;
			$pageException = "IdCampania is not a number";
		}
		
		if (!$isPageOk) {
			throw new Exception($pageException);
		}
		
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }
    try
    {
        $dtUsuarios = dmMisPedidos::getPedidoById(Aplicacion::Decrypter($_GET["Id"]));
        $LeyendaEstado="";
        $NombreCampania = $dtUsuarios[0]["Campania"];
        $TRX_ID = $dtUsuarios[0]["IdPedido"];

        if($dtUsuarios[0]["IdEstadoPedidos"] === 0 || $dtUsuarios[0]["IdEstadoPedidos"] ==1)
        {
        	if(dmPedidos::HayStock($TRX_ID)){

	        	$Cuenta = Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "e_comercio"))."2";
	            $Email = Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "email_cuenta"));
	            $Pin = Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "pin_consultas"));

	            $objDMReporte = new ReportePedidosDM($Email,$Cuenta, $Pin);
	            $objDMReporte->sendConsulta();

	            //Si no tira exception cambio el estado.
	            $arrPago = $objDMReporte->getPayByIdTRX($TRX_ID);

	            try
	            {
	            	actualizarPedidos::nuevo($TRX_ID,2)->Actualizar();
	         		Conexion::nuevo()->Cerrar_Trans();
	         		
	         		$idUsuarioDePedido = dmPedidos::getIdUsuarioByPedido($TRX_ID);
	                
	         		// Invalido la cache del credito de usuario
		            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				    $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				    $cache->remove('cache_credito_usuario_'.$idUsuarioDePedido[0]["IdUsuario"]);
		            
				    // Invalido la cache del padrino
				    $objUsuario = dmUsuario::getByIdUsuario($idUsuarioDePedido[0]["IdUsuario"]);
		    	    $nombrePadrino = $objUsuario->getPadrino();
		    	    if($nombrePadrino && $nombrePadrino != "Huerfano") {
			    	  $idPadrino = dmUsuario::getIdByNombreUsuario($nombrePadrino);
				      $cache->remove('cache_credito_usuario_'.$idPadrino);
		    	    }
	            }
	            catch (FaltaSockException $e){
	   				Conexion::nuevo()->RollBack();

	   				//Anulo.
   					try
		            {
		            	$actualizador = actualizarPedidos::nuevo($TRX_ID,10);
		            	$actualizador->Actualizar();
		         		Conexion::nuevo()->Cerrar_Trans();
		            }
		            catch(exception $e)
		            {
		             	Conexion::nuevo()->RollBack();
		            }

	            }
	            catch(exception $e)
	            {
	             	Conexion::nuevo()->RollBack();
	            }
        	}
        }
    }
    catch(exception $e)
    {
		header('HTTP/1.1 404 Not Found');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />

<script src="<?=UrlResolver::getJsBaseUrl("front/micuenta/js/js.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body style="width:300px; heigth:200px">
    <div style="width:100%; heigth:100%">
        <p>El estado actual del pedido es:<br/>
        <strong><?=$dtUsuarios[0]["Estado"]?></strong></p>
        <?
            unset($_SESSION["User"]["DineroMail"]);
            switch($dtUsuarios[0]["IdEstadoPedidos"])
            {
                case 0:



	            $hayStock = dmPedidos::hayStockReal($TRX_ID);

	            if(!$hayStock["db"]){
	            	//ACA LE CIERRO EL PEDIDO
					$actualizador = actualizarPedidos::nuevo($TRX_ID, 10);
					$actualizador->Actualizar();
		            Conexion::nuevo()->Cerrar_Trans();
		            exit();
	            }elseif(!$hayStock["temporal"]){
	            	//ACA LE TIRO UN CARTEL DE ERROR Y QUE SI QUIERE QUE VUELVA A INTENTAR

	            	exit("El ultimo articulo en stock esta siendo abonado por otro usuario. Intente nuevamente mas tarde.");
	            }else{
	            	//ACA SIGO ADELANTE CON TODO

		           	$_SESSION["User"]["DineroMail"]=array();
	                $_SESSION["User"]["DineroMail"]["NroItem"] =Aplicacion::Encrypter($dtUsuarios[0]["IdPedido"]);
	                $_SESSION["User"]["DineroMail"]["Vidriera"] =Aplicacion::Encrypter($NombreCampania);



	                dmPedidos::setFechaEntradaSistemaPago($dtUsuarios[0]["IdPedido"]);
	                    ?>
	                        <form id="frmDineroMail" method="post" target="_parent" name="frmDineroMail" action="<?=Aplicacion::getParametros("dineromail", "post")?>">
	                            <input type="hidden" id="E_Comercio" name="E_Comercio" value="<?=Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "e_comercio"))?>" />
	                            <input type="hidden" id="PrecioItem" name="PrecioItem" value="<?=$dtUsuarios[0]["Importe"]?>" />
	                            <input type="hidden" id="NroItem" name="NroItem" value="" />
	                            <input type="hidden" id="DireccionExito" name="DireccionExito" value="<?=Aplicacion::getRootUrl().Aplicacion::getParametros("dineromail", "exito")?>" />
	                            <input type="hidden" id="DireccionFracaso" name="DireccionFracaso" value="<?=Aplicacion::getRootUrl().Aplicacion::getParametros("dineromail", "fallo")?>" />
	                            <input id="btnEnviar" name="btnEnviar" type="submit" value="Comenzar a pagar con dineromail" onclick="noCortina()"/>
	                            <input type="hidden" name="TipoMoneda" id="TipoMoneda" value="1">
	                            <input type="hidden" name="DireccionEnvio" id="DireccionEnvio" value="0">
	                            <input type="hidden" name="Mensaje" id="Mensaje" value="0">
	                            <input type="hidden" name="image_url" id="image_url" value="1">
	                            <input type="hidden" name="NombreItem" id="NombreItem" value="Geelbe - <?=$NombreCampania;?>">
	                            <input type="hidden" name="trx_id" id="trx_id" value="<?=$TRX_ID?>">
	                            <input type="hidden" name="MediosPago" id="MediosPago" value="4,5,6,14,15,16,17,18"><!--'4,5,6,14,15,16,17,18'--><!--4,5,6,14,15,16,17,2,7,13-->

	                            <input type="hidden" name="usr_nombre" id="usr_nombre" value="<?=Aplicacion::Decrypter($_SESSION["User"]["nombre"])?>" />
	                            <input type="hidden" name="usr_apellido" id="usr_apellido" value="<?=Aplicacion::Decrypter($_SESSION["User"]["apellido"])?>" />
	                            <input type="hidden" name="usr_tel_numero" id="usr_tel_numero" value="<?=Aplicacion::Decrypter($_SESSION["User"]["telefono"])?>" />
	                            <input type="hidden" name="usr_email" id="usr_email" value="<?=Aplicacion::Decrypter($_SESSION["User"]["email"])?>" />
	                        </form>
                    	<?
	            }
                break;
                case 1:
                    $LeyendaEstado ="El sistema de pago esta procesando la informaci&oacute;n. Si en las proximas 48 horas no tiene noticias, por favor comuniquese con la mesa de ayuda de Geelbe.";
                    echo $LeyendaEstado;
                break;
                case 2:
                    $LeyendaEstado ="El pago fue aceptado. En breve nos estaremos comunicando para enviarle el pedido.";
                    echo $LeyendaEstado;
                break;
                case 3:
                    $LeyendaEstado ="Se programo la fecha de envio para el d&iacute;a {$dtUsuarios[0]["FechaEnvio"]}.";
                    echo $LeyendaEstado;
                break;
                case 4:
                    $LeyendaEstado ="El pago fue rechazado.";
                    echo $LeyendaEstado;
                break;
                case 5:
                    $LeyendaEstado ="El pedido ha sido anulado.<br />El motivo de la anulaci&oacute;n es: ".$dtUsuarios[0]["MotivoAnulacion"];
                    echo $LeyendaEstado;
                break;

                case 6:
                    $LeyendaEstado ="Este pedido ha sido entregado el dia ".$dtUsuarios[0]["FechaEnvio"];
                    echo $LeyendaEstado;
                break;
				case 8:
                    $LeyendaEstado ="N&uacute;mero: ".$dtUsuarios[0]["CuponPago"];
                    echo $LeyendaEstado;
                break;
				case 10:
                    $LeyendaEstado ="Pendiente de aprobaci&oacute;n";
                    echo $LeyendaEstado;
                break;
            }
        ?>
    </div>
</body>
</html>