<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

try {
    $confRoot = explode("/", dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/conf/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica/url_resolver/UrlResolver.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/ssl/carrito/logic/CartUtils.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica_bo/validators/ValidatorUtils.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica_bo/clientes/datamappers/dmclientes.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica/micuenta/clases/clsmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica/micuenta/datamappers/dmmicuenta.php");
} catch (Exception $e) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}
try {
    ValidarUsuarioLogueado();
} catch (Exception $e) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

$view = $_GET['view'];
//$dest = $_GET["dest"];
$action = $_GET["action"];

if ($view == "front") {   
        switch ($action) {
            case "validaDoc":
                ob_start();
                $numDoc=$_POST['NroDoc'];
                $userId = Aplicacion::Decrypter($_SESSION["User"]["id"]);
                //$customerController=new CustomerController();
                 $custPersonalInfo = dmMicuenta::validateDoc($numDoc);
                
                if (!$custPersonalInfo['IdUsuario'] || $custPersonalInfo['IdUsuario'] == $userId){
                    echo 0;
                }else{
                    echo $userId;
                }
                break;
        }   
} elseif ($view == "back_office") {
    
}
?>
