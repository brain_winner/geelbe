<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));
        $postURL = Aplicacion::getIncludes("post", "micuenta");
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    } 
    try
    {
       $objUsuario = new MySQL_micuenta();
       $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
       
       $fbkId = dmMicuenta::GetFacebookId(Aplicacion::Decrypter($_SESSION["User"]["id"]));
    }
    catch(exception $e)
    {
        die(print_r($e));
    }   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="../geelbe_new.css" rel="stylesheet" type="text/css" />
<!--link href="<?=UrlResolver::getCssBaseUrl("front/referenciados/css/invites_loged.css");?>" rel="stylesheet" type="text/css" /-->
<!--link href="<?=UrlResolver::getCssBaseUrl("front/micuenta/css/mi-cuenta.css");?>" rel="stylesheet" type="text/css" /-->
<link href="css/mi-cuenta.css" rel="stylesheet" type="text/css" />
<link href="http://css.static.geelbe.com/co/front/botones.css" rel="stylesheet" type="text/css">

<script src="<?=UrlResolver::getJsBaseUrl("front/micuenta/js/js.js");?>" type="text/javascript"></script>
<script src="<?=UrlResolver::getJsBaseUrl("js/jquery.js");?>" type="text/javascript"></script>
<script src="<?=UrlResolver::getJsBaseUrl("front/micuenta/js/getCPData.js");?>" type="text/javascript"></script>

<script type="text/javascript">

	<?php 
		$user_id = Aplicacion::Decrypter($_SESSION['User']['id']);
		$hash = md5($user_id . "pajarito");
	?>

	function updateUserInfoWithFbk() {
		FB.login(function(response) {
			if (response.session) {
				if (response.perms) {
					var session = FB.getSession();
					window.location.href="/appfacebook/tab/update_user_info.php?" + "fb_sig_session_key= " +
						session.session_key + "&fb_sig_user=" + session.uid + "&fb_sig_app_id=130962696927517" +
						"&access_token=" + session.access_token + "&redirection=micuenta&user_id=<?=$user_id?>&hash=<?=$hash?>";
				} else {
					alert("Debes aceptar los permisos para poder actualizar tus datos en Geelbe.")
				}
			} else {
				alert("Debes aceptar los permisos para poder actualizar tus datos en Geelbe.")
			}
		}, {perms:'user_birthday,email,publish_stream,offline_access,rsvp_event'});
	}
</script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body onload="document.getElementById('clave1').focus()">
 <? require("../menuess/menu_log.php") ?>
<div >
  <div id="container">
  	<img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/p-titulo.jpg");?>" />
	<div id="edicion">
    	<form method="POST" name="frmMiCuenta" id="frmMiCuenta" onKeyPress="if(ValidarEnter(event))this.submit();" target="iMiCuenta" action="../../../co/logica/micuenta/actionform/micontrasena.php" onSubmit="ValidarFormContrasena(this);">
        <div class="izquierda">
            <input name='frmU[txtEmail]' id='frmU[txtEmail]' type='hidden' value="<?=$objUsuario->getNombreUsuario();?>"  readonly="readonly"/>               
            <div class="edit-perfil bloques" >
            	<h2 style="margin:0 0 5px 0;">Actualiza tu contrase�a</h2>
            	<p style="margin:0;">Para modificar la contrase�a de acceso a Geelbe,<br />ingresa una nueva clave personal.<h4>Para mayor seguridad, te recomendamos<br />utilizar una combinaci�n de letras y n�meros.</h4></p>
                <p>Nueva Contrase�a</p>
                <input name='frmU[txtClaveNuevo1]' id='clave1' type='password' maxlength="12" size="20" /><h4> <strong>MINIMO 6 DIGITOS </strong></h4>
                <p>Confirmar nueva Contrase�a</p>
                <input name='frmU[txtClaveNuevo1]' id='clave2' type='password' maxlength="12" size="20" />
                <input name='frmU[txtClave]' id='frmU[txtClave]' type='hidden' value="<?=$objUsuario->getClave();?>"/>
            </div>
        </div>
<div id="error-msj" class="edit-perfil" style="display:none"></div>
      <div class="unfloat" style="padding:10px 0 5px;">
        	<table class="btn btnRed centro normal S" cellpadding="0" cellspacing="0" border="1px">
				<tr>
					<td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000">
						<a href="javascript:document.forms[0].onsubmit();" ><strong>Actualizar</strong></a>
					</td>
				</tr>
			</table>
            <a style="color:#8483b8; float: right; margin-top:-15px;" onclick="" href="<?="/".$confRoot[1]."/"?>logica/micuenta/actionform/enviareliminacion.php" linkindex="153">Desactivar mi cuenta</a>
	    </div>
		</form>
	</div>
    <div>
    	<div class="col_izquierda">
        	<div class="bloques sections">
            	<div class="col_izquierda">
                	<h2>MI CR�DITO</h2>
                    <p>Por cada invitado que<br>
                    se registre y realice<br>
                    su primera compra<br>
                    en Geelbe, recibir�s<br>
                    $10.000 de cr�dito en tu cuenta.<br>
                    Consulta el estado<br> de tu cr�dito en Geelbe.</p>
                    <a href="/co/front/micuenta/mi-credito.php">Ingresar <img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/seguir.jpg");?>" /></a>                </div>
                <div class="col_derecha">
                	<img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/p-cred.jpg");?>" />                </div>
            </div>
   	  <div class="bloques sections">
            	<div class="col_izquierda">
                	<h2>MIS PEDIDOS</h2>
                    <p>Ingresando en esta secci�n, puedes hacer el seguimiento del env�o de cada una de tus compras en Geelbe.</p>
                    <a href="/co/front/micuenta/mispedidos.php">Ingresar <img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/seguir.jpg");?>" /></a>                </div>
                <div class="col_derecha">
                	<img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/p-pedidos.jpg");?>" />                </div>
            </div>
	  </div>
        <div class="col_derecha">
        	<div class="bloques sections" style="margin-left:11px!important;">
            	<div class="col_izquierda">
                	<h2>INVITA A UN AMIGO</h2>
                    <p>Invita a tus amigos a registrarse. Geelbe les obsequiar� dinero a ti y a ellos para que lo utilicen en sus compras.</p>
                    <a href="/co/front/referenciados/">Ingresar <img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/seguir.jpg");?>" /></a>                </div>
                <div class="col_derecha">
                	<img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/p-invite.jpg");?>" />                </div>
            </div>
   	  <div class="bloques sections" style="margin-left:11px!important;">
            	<div class="col_izquierda">
                	<h2>MIS CONSULTAS</h2>
                    <p>Si quieres realizar alguna consulta, puedes enviarnos un mensaje. Nuestro equipo de atenci�n al cliente te responder� a la brevedad.</p>
                    <a href="/co/front/micuenta/misconsultas.php?act=6">Ingresar <img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/cta.jpg");?>images/seguir.jpg" /></a>                </div>
                <div class="col_derecha">
                	<img src="<?=UrlResolver::getImgBaseUrl("front/micuenta/images/p-consultas.jpg");?>" />                </div>
            </div>
      </div>
    </div>


	<!--div id="central">
	  <h3>Geelbe te da la bienvenida a la Interface de Administraci�n Personal.      </h3>
	  <p>Desde &quot;MiCuenta&quot; podr�s mantener actualizada la informaci�n de tu membres�a, referir amigos para que se registren en Geelbe como tus invitados y chequear el dinero que obtienes para realizar tus compras.</p>
	  <div style="margin: 10px 0; height: 180px;">
	  <div class="columna">
	  <div class="columna">

	    <h2><a href="/<?=Aplicacion::getDirLocal()?>front/referenciados/?act=4"><img src="<?=UrlResolver::getImgBaseUrl("front/images/mail_compose_24.png");?>" alt="Invita amigos" width="24" height="24" longdesc="Invita a tus amigos, reg�lales cr�dito y acumula dinero en tu cuenta" /> Invita a un amigo</a></h2>
	    <h5>Invita a tus amigos a registrarse en el club privado de compras en Internet. Geelbe les obsequiar� dinero -en tu nombre- para que lo utilicen en su primera compra y acreditar� dinero en tu cuenta por cada invitado que se registre. </h5>
	    <p>&nbsp;</p>
	    </div>
	  <h2>&nbsp;</h2>
	  </div>
	  <div class="columna">
	  <div class="columna">
	    <h2><a href="mi-perfil.php?act=2"><img src="<?=UrlResolver::getImgBaseUrl("front/images/user_edit_24.png");?>" alt="Mi Perfil" width="24" height="24" longdesc="Edita tus datos" /> Mi perfil</a></h2>
	    <h5>Verifica la informaci�n ingresada en el formulario de registro. Modifica la contrase�a de acceso al club privado de compras en Internet. Actualiza los datos de tu membres�a en Geelbe para poder contactarnos contigo.</h5>
	    </div>
	  <h2>&nbsp;</h2>
	  </div>
	  <div class="columna">
  	  <h2><a href="mi-credito.php?act=1"><img src="<?=UrlResolver::getImgBaseUrl("front/images/calculator_accept_24.png");?>" alt="Mi cr�dito" width="24" height="24" longdesc="Revisa el estado de tu cr�dito" /> Mi cr�dito</a></h2>
  	  <h5> Por cada invitado que se registre y realice su primera compra en Geelbe recibir�s $10 de cr�dito en tu cuenta, que podr�s utilizar en las compras futuras. Chequea el estado de tu cr�dito en Geelbe.</h5>
  	  </div>
	  </div>
	  <div class="grisado">
	    <h2><a href="/<?=Aplicacion::getDirLocal()?>front/referenciados/?act=4"><strong>Gana dinero invitando a tus amigos a Geelbe!</strong></a></h2>
	    <p>Con nuestro sistema de recomendaci�n puedes invitar a tus amigos para que se registren en Geelbe y acreditar dinero en tu cuenta para utilizarlo en las futuras compras. Mientras m�s amigos recomiendes en Geelbe m�s beneficios obtendr�s como miembro.</p>
	  </div>
	</div-->
	<!--div id="menuleft">
    ><?// require("../menuess/micuenta.php")?>
	</div-->
	</div>
</div>

</div>

</div>
<? require("../menuess/footerNew.php")?>
<div id="fb-root"></div>
<script>
	window.fbAsyncInit = function() {
		FB.init({appId: '130962696927517', status: true, cookie: false,
						 xfbml: true});
	};
	(function() {
		var e = document.createElement('script'); e.async = true;
		e.src = document.location.protocol +
			'//connect.facebook.net/es_LA/all.js';
		document.getElementById('fb-root').appendChild(e);
	}());
</script>
<iframe id="iMiCuenta" name="iMiCuenta" style="display:none"></iframe>
<script>
    <?
      if ($objUsuario->getDirecciones(0)->getIdProvincia()>0)
      {
        $Provincia = $objUsuario->getDirecciones(0)->getIdProvincia();
      }
      else
      {
        $Provincia = 0;
      }
       if ($objUsuario->getDirecciones(0)->getIdPais()>0)
      {
        $Pais = $objUsuario->getDirecciones(0)->getIdPais();
      }
      else
      {
        $Pais = 0;
      }
      if($objUsuario->getDatos()->getceluempresa()>0)
      {
        $celuempresa = $objUsuario->getDatos()->getceluempresa();
      }
      else
      {
        $celuempresa = 0;
      }
    ?>  
   iniciar(<?=$Pais?>,<?=$Provincia?>,<?=$celuempresa?>);
   
  function actualizarCiudades() {
	   $.ajax({ url: "<?=Aplicacion::getRootURl()?>logica/micuenta/actionform/obtener_ciudades.php?IdProvincia="+$("#idProvincia").val(), context: document.body, success: function(data){
 			$("#idCiudad").html(data);
	   }});
   }

   
</script>    
</body>
</html>
