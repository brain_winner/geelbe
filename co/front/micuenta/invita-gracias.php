<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css");?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getCssBaseUrl("front/botones.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/micuenta/js/js.js");?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">

  </div>
  <div id="contenido">
	<div id="central">
	  <h2><strong>Gracias por recomendar Geelbe a tus amigos!</strong></h2>
	  <p>&nbsp;</p>
	  <p>El sistema est� enviando las invitaciones a las direcciones de email que ingresaste. En breves instantes tus amigos podr�n registrarse en Geelbe y tendr�s $20 acreditados en tu cuenta cuando cada uno de ellos realice su primera compra.</p>
	  <p>&nbsp;</p>
	  <h2><a href="/<?=Aplicacion::getDirLocal()?>front/micuenta"><img src="<?=UrlResolver::getImgBaseUrl("front/images/boton-cuenta.gif");?>" alt="Volver a mi Cuenta" width="142" height="20" /></a> <a href="/<?=Aplicacion::getDirLocal()?>front/referenciados"><img src="<?=UrlResolver::getImgBaseUrl("front/images/boton-continuar.gif");?>" alt="Continuar Invitando Amigos" width="250" height="20" /></a></h2>
	
	<table>
		<tr>
		<td width="20%">
		<table class="btn btnViolet izquierda chico XL" cellpadding="0" cellspacing="0" border="1px"> 
			<tr>
				<td>
					<a href="/<?=Aplicacion::getDirLocal()?>front/micuenta"><strong>Volver a mi cuenta</strong></a>
				</td>
			</tr>
		</table>
		</td>
		<td width="80%">
		<table class="btn btnRed izquierda chico XXXL" cellpadding="0" cellspacing="0" border="1px"> 
			<tr>
				<td>
					<a href="/<?=Aplicacion::getDirLocal()?>front/referenciados"><strong>Continuar invitando amigos</strong></a>
				</td>
			</tr>
		</table>
		</td>
		</tr>
		</table>
	</div>
	<div id="menuleft">
	    <? require("../menuess/micuenta.php")?>
	</div>
</div>
  <div id="contenido-bot"></div>
</div>
<div id="bottom" >
</div>
<? require("../menuess/footer.php")?>
</body>
</html>
