<?
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	
    try {
        ValidarUsuarioLogueado();
	} catch (Exception $e) {
		header("Location: http://".$_SERVER['HTTP_HOST']."/".$confRoot[1].'/front/login');
		exit;
    }
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

		<link href="<?='/'.$confRoot[1].'/front/geelbe_new.css'?>" rel="stylesheet" type="text/css" />
		<link href="<?='/'.$confRoot[1].'/ssl/carrito/css/cart.css'?>" rel="stylesheet" type="text/css" />
		<link href="<?='/'.$confRoot[1].'/front/micarrito/error/style.css'?>" rel="stylesheet" type="text/css" />
   		<link href="<?='/'.$confRoot[1].'/ssl/geelbecash/css/dinero-n-cuenta.css'?>" rel="stylesheet" type="text/css" />
		<link href="<?='/'.$confRoot[1].'/front/botones.css'?>" rel="stylesheet" type="text/css" />
		<title>Gracias por comprar en Geelbe</title>
		<? include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background_ssl.php"; ?>
	</head>
	<body> 
	
		<? require("../../front/menuess/menu_ssl.php") ?>
		<div id="container">

		<div id="errorcompra_box">
		<div id="errorcompra_title"><img src="error/img/titulo.gif"  /></div>
		<div id="errorcompra_topbar"></div>
		<div id="errorcompra_cont_box">
		<div id="errorcompra_cont_txt" class="txt_errorcompra"><span class="txt_errorcompra_especial">
		
		<strong>N�mero de Transacci�n:</strong> <?=$_REQUEST['ref_venta'];?><br />
		<strong>Moneda:</strong> <?=$_REQUEST['moneda'];?><br />
        <strong>Monto total:</strong> <?=Moneda($_REQUEST['valorPesos']);?><br /><br />
		
		Oops... Tu pago no fue finalizado</span>. Por alguna raz�n no terminaste de completar el pago de tu Pedido. Si tenes alguna duda o podemos ayudarte, Si necesitas comunicarte con nuestro Centro de Atenci�n al Cliente hacelo <a href="/<?=$confRoot[1]?>/front/micuenta/misconsultas.php?act=6">desde ac� </a>.<br /><br />
		Te saluda atentamente,<br /><br />
		<span class="txt_errorcompra_especial">Geelbe</span><br />
		<span class="txt_errorcompra_mini">tu club privado de compras por Internet</span>
		</div>
		<div id="errorcompra_img"><img src="error/img/error.gif" /></div>
		 </div><!--FIN COMPRA BOX-->
		</div><!--FIN COMPRA CONT_BOX-->


		</div>
		<? require("../../front/menuess/footer_ssl.php");?>
	</body>
</html>
