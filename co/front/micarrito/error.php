<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="/<?=$confRoot[1]?>/front/geelbe.css" rel="stylesheet" type="text/css" />
<link href="/<?=$confRoot[1]?>/front/botones.css" rel="stylesheet" type="text/css" />
<script src="/<?=$confRoot[1]?>/front/micarrito/js/js.js" type="text/javascript"></script>
<script src="/<?=$confRoot[1]?>/js/jquery.js" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">
  </div>
    <div id="contenido">

	<div id="top">	
	  <h1>Error al completar su Compra
      </h1>
    </div>
	
	<p style="padding-bottom:226px; padding-top:15px; font-size:13px; line-height:23px;">
    	Hubo un error al completar su compra. El error reportado es: <?=$txnResponseCode.' - '.getResponseDescription($txnResponseCode)?>.<br />
        Si desea recibir m�s informaci�n al respecto dir�gase al sector de <a href="../contacto-socio/">Contacto</a>.
    
    </p>
	
</div>
  <div id="contenido-bot"></div>
</div>	
<? require("../menuess/footer.php")?>
<iframe style="display:none" name="iContacto" id="iContacto"></iframe> 
</body>
</html>
