<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito", "clases")); 
        $postURL = Aplicacion::getIncludes("post","micarrito_fincompra");
        $datosUser = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }   
    try
    {
        $objDireccion = new MySQL_PedidosDirecciones();
        $objDireccion->setCP($_SESSION["User"]["DireccionEnvio"]["txtCP"]);
        $objDireccion->setDomicilio($_SESSION["User"]["DireccionEnvio"]["txtDomicilio"]);
        $objDireccion->setEscalera($_SESSION["User"]["DireccionEnvio"]["txtEscalera"]);
        $objDireccion->setIdPais($_SESSION["User"]["DireccionEnvio"]["cbxPais"]);
        $objDireccion->setIdProvincia($_SESSION["User"]["DireccionEnvio"]["cbxProvincia"]);
        $objDireccion->setNumero($_SESSION["User"]["DireccionEnvio"]["txtNumero"]);
        $objDireccion->setPoblacion($_SESSION["User"]["DireccionEnvio"]["txtCiudad"]);
        $objDireccion->setPiso($_SESSION["User"]["DireccionEnvio"]["txtPiso"]);
        $objDireccion->setPuerta($_SESSION["User"]["DireccionEnvio"]["txtPuerta"]);
        $objDireccion->setTipoCalle($_SESSION["User"]["DireccionEnvio"]["cbxTipoCalle"]);
        $objDireccion->setHorarioEntrega($_SESSION["User"]["DireccionEnvio"]["cbxHorarioEntrega"]);
        $objDireccion->setIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        $objDireccion->setNombre($_SESSION["User"]["DireccionEnvio"]["txtNombre"]);
        $objDireccion->setApellido($_SESSION["User"]["DireccionEnvio"]["txtApellido"]);
        $objDireccion->setNroDoc($_SESSION["User"]["DireccionEnvio"]["txtDNI"]);
        $objDireccion->setTelefono($_SESSION["User"]["DireccionEnvio"]["txtTelefono"]);
        $objDireccion->setCodTelefono($_SESSION["User"]["DireccionEnvio"]["txtCodTelefono"]);
        $objDireccion->setceluempresa($_SESSION["User"]["DireccionEnvio"]["cbxCeluEmpresa"]);
        $objDireccion->setcelucodigo($_SESSION["User"]["DireccionEnvio"]["txtCelularCodigo"]);
        $objDireccion->setcelunumero($_SESSION["User"]["DireccionEnvio"]["txtCelularNumero"]);
        $objUsuario = new MySQL_micuenta();
        $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        $arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$MiCarrito->getIdCampania()."/imagenes/", 1, array("jpg", "jpeg", "gif", "png"));
        $arrArchivos = QuitarExtensiones_arrImagenes(ArrayValue_to_Index($arrArchivos));
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="/<?=$confRoot[1]?>/front/geelbe.css" rel="stylesheet" type="text/css" />
<link href="/<?=$confRoot[1]?>/front/micarrito/micarrito.css" rel="stylesheet" type="text/css" />
<script src="/<?=$confRoot[1]?>/front/micarrito/js/js.js" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">
  </div>
    <div id="contenido">
<form id="frmFinalizar" name="frmFinalizar" action="../../../<?=Aplicacion::getDirLocal().$postURL["micarrito_fincompra"]?>" method="post">

	<div id="top">	
	  <h1>Mi Carrito
      </h1>
	  	  <div id="topright" >
		<div class="right" style="margin: 0 5px 0 0;">
		  <h1>
		  <a href="../catalogo/index.php?IdCampania=<?=$MiCarrito->getIdCampania()?>"><img src="../../front/campanias/archivos/campania_<?=$MiCarrito->getIdCampania()?>/imagenes/<?=$arrArchivos["thumb_catalogo_logo"]?>" alt="Nombre de la Marca" class="logo-prod" style="margin-top:-5px;" /></a> 
          </h1>
		</div>
	  </div> 
    </div>
	<table width="100%" height="50" border="0" cellpadding="8" cellspacing="0" background="/<?=$confRoot[1]?>/front/images/pasos.gif">
      <tr>
        <td><div align="center">1</div></td>
        <td><div align="center">2</div></td>
        <td><div align="center"><strong>3</strong></div></td>
      </tr>
      <tr>
        <td><div align="center">Informaci�n de Env�o </div></td>
        <td><div align="center">Confirmaci�n de Compra </div></td>
        <td><div align="center"><strong>Sistema de Pago </strong></div></td>
      </tr>
    </table>
	<!--<div class="right" style="margin: 0 5px 0 0;">
		  <h3>C�digo de Compra: 00000000 </h3>
	</div>-->
	<h3>Contenido de tu carrito de compras</h3>
	<img src="/<?=$confRoot[1]?>/front/images/carrito-top.gif" alt="" width="687" height="10" class="left" />
	<div class="carro">
	  <table border="0" cellpadding="3" cellspacing="0" bgcolor="#FFFFFF" style="width:100%;">
          <tr>
            <td width="89" valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td width="399" valign="top" bgcolor="#F5F5F5"><h2>Producto</h2></td>
            <td width="64" valign="top" bgcolor="#F5F5F5"><h2>Cantidad</h2></td>
            <td width="84" valign="top" bgcolor="#F5F5F5"><h2>Importe</h2></td>
          </tr>
          
          <?            
            $Total = 0;
            $Peso = 0;
            $Ganancia = 0;
            foreach($MiCarrito->getArticulos() as $IdCodigoProdInterno => $Articulo)
            {
                $objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
                $objProducto = dmProductos::getById($objPP->getIdProducto());
                $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($IdCodigoProdInterno);
                $arrImg = DirectorytoArray(Aplicacion::getRoot() . "front/productos/productos_".$objProducto->getIdProducto()."/", 1, array("jpg", "jpeg", "gif", "png"));
                ?>
                    <tr>
                      <td valign="top">
                      <?
                        if($arrImg[0])
                        {
                            ?>
                                <a href="<?=Aplicacion::getRootUrl()."front/catalogo/index.php?IdCampania=".$MiCarrito->getIdCampania()?>"><img src="<?=Aplicacion::getRootUrl()."front/productos/productos_".$objProducto->getIdProducto()."/".$arrImg[0]?>" alt="" width="48" /></a>
                            <?
                        }
                        else
                        {
                            ?>
                                <a href="<?=Aplicacion::getRootUrl()."front/catalogo/index.php?IdCampania=".$MiCarrito->getIdCampania()?>"><img src="/<?=$confRoot[1]?>/front/productos/noimagen.jpg" alt="" width="48"  /></a><!--height="50"-->
                            <?                            
                        }
                      ?>
                      </td>
                        <td valign="top"><h3><a href="<?=Aplicacion::getRootUrl()."front/catalogo/index.php?IdCampania=".$MiCarrito->getIdCampania()?>"><?=$objProducto->getNombre()?></a></h3>
                        <h4>
                            <?
                                foreach($Atributos as $atributo)
                                {
                                    echo $atributo["Nombre"]." = ".$atributo["Valor"] . " | ";
                                }
                            ?>
                        </h4></td>
                        <td valign="top"><p><?=$Articulo["Cantidad"]?></p></td>
                        <td valign="top"><p><?=Moneda(ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo["Cantidad"]);?></p></td>
                      </tr>
                <?
                        $dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($MiCarrito->getIdCampania());
                        
                        $free = 1;
                        foreach($dtCampaniasPropiedades as $propiedad)
                        {
                            if($propiedad["IdPropiedadCampamia"]=='6')
                            {
                               $free = 0;
                            }
                        }                
                $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
                $Peso += $P * $Articulo["Cantidad"];
                $Ganancia +=abs(($objProducto->getPVenta() - $objProducto->getPCompra())*$Articulo["Cantidad"]);
                $Total += ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo["Cantidad"];
            }
            if($free!=0)
            {
                $objDHL = dmDHL::getTarifaByPesoPcia($Peso, $objDireccion->getIdProvincia());
                $Total += $objDHL->getImporte();
                $Envio = Moneda($objDHL->getImporte());
                $ValorEnvio = $objDHL->getImporte();
            }
            else
            {
                $Envio = "Sin Cargo";
                $ValorEnvio = 0;
            }
          ?>
          <tr>
            <td valign="middle"><img src="/<?=$confRoot[1]?>/front/images/home_next.png" alt="" width="24" height="24" /></td>
            <td valign="top"><h3>Costo de Env�o</h3>
              <p>Todos los env&iacute;os de Geelbe son realizados por un servicio de mensajer&iacute;a independiente</p></td>
            <td valign="top">&nbsp;</td>
            <td valign="top"><?=$Envio;?></td>
            <input id="txtGastosEnvio" name="txtGastosEnvio" type="hidden" value="<?=$ValorEnvio;?>"/>
          </tr>
          <tr>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5"><h3>Cr�ditos utilizados de tu cuenta</h3></td>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5"><h2><span id="CreditoUtilizado"><?=Moneda(0)?></span></h2></td>
          </tr>
          <tr>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5"><h3>Total de la Compra </h3></td>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5"><h3> 
            <span id="verTotalCompra"><?=Moneda($Total)?></span>
            </h3></td>
          </tr>
      </table>
    </div>
      <img src="/<?=$confRoot[1]?>/front/images/carrito-bottom.gif" alt="" width="687" height="11" style="margin-top:-5px;" /><input type="hidden" id="txtTotal" name="txtTotal" value="<?=$Total?>"/>
<input type="hidden" id="txtBonoUtilizado" name="txtBonoUtilizado" value="0"/>
</form>
</div>
  <div id="contenido-bot"><div class="line"></div><div class="tit1">Seleccione su Forma de Pago:</div> 
<div class="bloque1"><div class="fleft"><div class="bot">
      <input type="radio" name="radio" id="radio" value="radio" />
    <img src="/<?=$confRoot[1]?>/front/micarrito/img/visa.jpg" width="78" height="44" align="absmiddle" /></div>
  <div class="bot2">
     <input type="radio" name="radio" id="radio" value="radio" />
  <img src="/<?=$confRoot[1]?>/front/micarrito/img/mastercard.jpg" width="73" height="43" align="absmiddle" /></div>
  <div class="bot">
     <input type="radio" name="radio" id="radio" value="radio" />
   <img src="/<?=$confRoot[1]?>/front/micarrito/img/american.jpg" width="78" height="44" align="absmiddle" /></div>
  <div class="cuotas"> Seleccione las cuotas que desea abonar:<br />
   <div class="fleft"> 
      <select name="select" id="select">
        <option>Standard Bank</option>
      </select>
      <select name="select2" id="select2">
        <option>3 cuotas</option>
      </select>
    </div>
   <div class="igual"><strong>=</strong></div> <div class="text1"><strong>Costo por cuota: $13.33 </strong><br />
  Total a pagar financiado: $80.00</div></div></div><div><img src="/<?=$confRoot[1]?>/front/micarrito/img/standard-bank.jpg" width="195" height="168" /></div>
</div>
<div class="bloque3"> <div class="tit1">Otras Opciones de Pago:</div><div class="bot4"><div><div style="float:left"><input type="radio" name="radio" id="radio" value="radio" />
      <img src="/<?=$confRoot[1]?>/front/micarrito/img/dineromail.jpg" width="156" height="40" align="absmiddle" /><br />
      <img src="/<?=$confRoot[1]?>/front/micarrito/img/saldo_03.jpg" width="157" height="44" /></div><div class="text3">Puede abonar los productos con cr&eacute;dito DineroMail Tarjeta de Cr&eacute;dito, la cual es procesada online a trav&eacute;s de un servidor certificado seguro, por v&iacute;as de encriptaci&oacute;n de 128 bits. Esta seguridad permite garantizar a los usuarios la confidencialidad y seguridad de los datos de sus tarjetas de cr&eacute;dito.</div> </div></div></div></div>
</div>	
<? require("../menuess/footer.php")?>
<iframe style="display:none" name="iContacto" id="iContacto"></iframe> 
<script>
    var MaxCreditos = <?=(isset($disponibles)) ? $disponibles:0?>;
    var Ganancia = <?=(isset($Ganancia)) ? $Ganancia:0?>;
    var Total = <?=(isset($Total)) ? $Total:0?>;
</script>
</body>
</html>
