<?php
	
	ob_start();
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("paypal"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("emails"));
    
	set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
	require_once 'Zend/Cache.php'; 

	// *********************
	// START OF MAIN PROGRAM
	// *********************
	
	// Define Constants
	// ----------------
	// This is secret for encoding the MD5 hash
	// This secret will vary from merchant to merchant
	// To not create a secure hash, let SECURE_SECRET be an empty string - ""
	// $SECURE_SECRET = "secure-hash-secret";
	$SECURE_SECRET = Aplicacion::getParametros("banamex", "hash");
	
	// If there has been a merchant secret set then sort and loop through all the
	// data in the Virtual Payment Client response. While we have the data, we can
	// append all the fields that contain values (except the secure hash) so that
	// we can create a hash and validate it against the secure hash in the Virtual
	// Payment Client response.
	
	// NOTE: If the vpc_TxnResponseCode in not a single character then
	// there was a Virtual Payment Client error and we cannot accurately validate
	// the incoming data from the secure hash. */
	
	// get and remove the vpc_TxnResponseCode code from the response fields as we
	// do not want to include this field in the hash calculation
	$vpc_Txn_Secure_Hash = $_GET["vpc_SecureHash"];
	unset($_GET["vpc_SecureHash"]); 
	
	// set a flag to indicate if hash has been validated
	$errorExists = false;
	
	if (strlen($SECURE_SECRET) > 0 && $_GET["vpc_TxnResponseCode"] != "7" && $_GET["vpc_TxnResponseCode"] != "No Value Returned") {
	
	    $md5HashData = $SECURE_SECRET;
	
	    // sort all the incoming vpc response fields and leave out any with no value
	    foreach($_GET as $key => $value) {
	        if ($key != "vpc_Secure_Hash" or strlen($value) > 0) {
	            $md5HashData .= $value;
	        }
	    }
	    
	    // Validate the Secure Hash (remember MD5 hashes are not case sensitive)
		// This is just one way of displaying the result of checking the hash.
		// In production, you would work out your own way of presenting the result.
		// The hash check is all about detecting if the data has changed in transit.
	    if (strtoupper($vpc_Txn_Secure_Hash) == strtoupper(md5($md5HashData))) {
	        // Secure Hash validation succeeded, add a data field to be displayed
	        // later.
	        $hashValidated = true;
	    } else {
	        // Secure Hash validation failed, add a data field to be displayed
	        // later.
	        $hashValidated = false;
	        $errorExists = true;
	    }
	} else {
	    // Secure Hash was not validated, add a data field to be displayed later.
	    $hashValidated = false;
	}
	
	// Define Variables
	// ----------------
	// Extract the available receipt fields from the VPC Response
	// If not present then let the value be equal to 'No Value Returned'
	
	// Standard Receipt Data
	$amount          = null2unknown($_GET["vpc_Amount"]);
	$locale          = null2unknown($_GET["vpc_Locale"]);
	$command         = null2unknown($_GET["vpc_Command"]);
	$message         = null2unknown($_GET["vpc_Message"]);
	$version         = null2unknown($_GET["vpc_Version"]);
	$orderInfo       = null2unknown($_GET["vpc_OrderInfo"]);
	$merchantID      = null2unknown($_GET["vpc_Merchant"]);
	$txnResponseCode = null2unknown($_GET["vpc_TxnResponseCode"]);
	$merchTxnRef     = array_key_exists("vpc_MerchTxnRef", $_GET) ? $_GET["vpc_MerchTxnRef"] : "No Value Returned";
	$cardType        = array_key_exists("vpc_Card", $_GET)             ? $_GET["vpc_Card"]             : "No Value Returned";
	
	/*
	$batchNo         = array_key_exists("vpc_BatchNo", $_GET)          ? $_GET["vpc_BatchNo"]          : "No Value Returned";
	$receiptNo       = array_key_exists("vpc_ReceiptNo", $_GET)        ? $_GET["vpc_ReceiptNo"]        : "No Value Returned";
	$authorizeID     = array_key_exists("vpc_AuthorizeId", $_GET)      ? $_GET["vpc_AuthorizeId"]      : "No Value Returned";
	
	$transactionNo   = array_key_exists("vpc_TransactionNo", $_GET)    ? $_GET["vpc_TransactionNo"]    : "No Value Returned";
	$acqResponseCode = array_key_exists("vpc_AcqResponseCode", $_GET)  ? $_GET["vpc_AcqResponseCode"]  : "No Value Returned";
	
	// CSC Receipt Data
	$cscResultCode   = array_key_exists("vpc_CSCResultCode", $_GET)    ? $_GET["vpc_CSCResultCode"]    : "No Value Returned";
	$cscACQRespCode  = array_key_exists("vpc_AcqCSCRespCode", $_GET)   ? $_GET["vpc_AcqCSCRespCode"]   : "No Value Returned";
	
	// AVS Receipt Data
	$avs_City        = array_key_exists("vpc_AVS_City", $_GET)         ? $_GET["vpc_AVS_City"]         : "No Value Returned";
	$avs_Country     = array_key_exists("vpc_AVS_Country", $_GET)      ? $_GET["vpc_AVS_Country"]      : "No Value Returned";
	$avs_Street01    = array_key_exists("vpc_AVS_Street01", $_GET)     ? $_GET["vpc_AVS_Street01"]     : "No Value Returned";
	$avs_PostCode    = array_key_exists("vpc_AVS_PostCode", $_GET)     ? $_GET["vpc_AVS_PostCode"]     : "No Value Returned";
	$avs_StateProv   = array_key_exists("vpc_AVS_StateProv", $_GET)    ? $_GET["vpc_AVS_StateProv"]    : "No Value Returned";
	$avsRequestCode  = array_key_exists("vpc_AVSRequestCode", $_GET)   ? $_GET["vpc_AVSRequestCode"]   : "No Value Returned";
	$avsResultCode   = array_key_exists("vpc_AVSResultCode", $_GET)    ? $_GET["vpc_AVSResultCode"]    : "No Value Returned";
	$vACQAVSRespCode = array_key_exists("vpc_AcqAVSRespCode", $_GET)   ? $_GET["vpc_AcqAVSRespCode"]   : "No Value Returned";
	
	// 3-D Secure Data
	$verType         = array_key_exists("vpc_VerType", $_GET)          ? $_GET["vpc_VerType"]          : "No Value Returned";
	$verStatus       = array_key_exists("vpc_VerStatus", $_GET)        ? $_GET["vpc_VerStatus"]        : "No Value Returned";
	$token           = array_key_exists("vpc_VerToken", $_GET)         ? $_GET["vpc_VerToken"]         : "No Value Returned";
	$verSecurLevel   = array_key_exists("vpc_VerSecurityLevel", $_GET) ? $_GET["vpc_VerSecurityLevel"] : "No Value Returned";
	$enrolled        = array_key_exists("vpc_3DSenrolled", $_GET)      ? $_GET["vpc_3DSenrolled"]      : "No Value Returned";
	$xid             = array_key_exists("vpc_3DSXID", $_GET)           ? $_GET["vpc_3DSXID"]           : "No Value Returned";
	$acqECI          = array_key_exists("vpc_3DSECI", $_GET)           ? $_GET["vpc_3DSECI"]           : "No Value Returned";
	$authStatus      = array_key_exists("vpc_3DSstatus", $_GET)        ? $_GET["vpc_3DSstatus"]        : "No Value Returned";
	*/
	
	if($hashValidated && $txnResponseCode == '0') {
	
		$pedido = dmPedidos::getPedidoById($merchTxnRef);
		$pagado = dmPedidos::PagoAceptado($merchTxnRef);
		
			    		
        // Limpio la cache de producto
        $IdCampania = dmPedidos::getIdCategoriaByIdPedido($merchTxnRef);
        $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
		$cache->clean(
		    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
		    array('group_cache_catalogo_campania_'.$IdCampania)
		);
    	
		
		if($pagado) {
			
			dmPedidos::ActualizarTarjeta($merchTxnRef, $cardType);
    		$camp = dmCampania::getByIdCampania($pedido->getIdCampania());
			$u = dmUsuario::getByIdUsuario($pedido->getIdUsuario());
			$email = dmPedidos::getDatosMailByIdPedido($pedido->getIdPedido());
			
	$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	$mesFn = $meses[date("n", strtotime($email["TiempoEntregaFn"])) - 1];
	$mesIn = $meses[date("n", strtotime($email["TiempoEntregaIn"])) - 1];
			
			$datos = array(
				'NumeroPedido' => $pedido->getIdPedido(),
				'SistemaPago' => 'Banamex',
				'NombreVidriera' => $camp->getNombre(),
				'IdUsuario' => $pedido->getIdUsuario(),
				'HashIdUsuario' => Aplicacion::Encrypter($pedido->getIdUsuario()),
				'NombreUsuario' => $u->getDatos()->getNombre(),
				'ApellidoUsuario' => $u->getDatos()->getApellido(),
				"TiempoEntregaFn" => date("d", strtotime($email["TiempoEntregaFn"]))." de ".$mesFn,
				"TiempoEntregaIn" => date("d", strtotime($email["TiempoEntregaIn"]))." de ".$mesIn
			);
			
			emailPagoAceptado($u->getDatos()->getEmail(), $datos);
			
		}
	
		require_once dirname(__FILE__).'/fin.php';
	} else {
		require_once dirname(__FILE__).'/error.php';
	}
	
	die;
	
	// *******************
	// END OF MAIN PROGRAM
	// *******************
		
	// This method uses the QSI Response code retrieved from the Digital
	// Receipt and returns an appropriate description for the QSI Response Code
	//
	// @param $responseCode String containing the QSI Response Code
	//
	// @return String containing the appropriate description
	//
	function getResponseDescription($responseCode) {
	
	    switch ($responseCode) {
	        case "0" : $result = "Transaction Successful"; break;
	        case "?" : $result = "Transaction status is unknown"; break;
	        case "1" : $result = "Unknown Error"; break;
	        case "2" : $result = "Bank Declined Transaction"; break;
	        case "3" : $result = "No Reply from Bank"; break;
	        case "4" : $result = "Expired Card"; break;
	        case "5" : $result = "Insufficient funds"; break;
	        case "6" : $result = "Error Communicating with Bank"; break;
	        case "7" : $result = "Payment Server System Error"; break;
	        case "8" : $result = "Transaction Type Not Supported"; break;
	        case "9" : $result = "Bank declined transaction (Do not contact Bank)"; break;
	        case "A" : $result = "Transaction Aborted"; break;
	        case "C" : $result = "Transaction Cancelled"; break;
	        case "D" : $result = "Deferred transaction has been received and is awaiting processing"; break;
	        case "F" : $result = "3D Secure Authentication failed"; break;
	        case "I" : $result = "Card Security Code verification failed"; break;
	        case "L" : $result = "Shopping Transaction Locked (Please try the transaction again later)"; break;
	        case "N" : $result = "Cardholder is not enrolled in Authentication scheme"; break;
	        case "P" : $result = "Transaction has been received by the Payment Adaptor and is being processed"; break;
	        case "R" : $result = "Transaction was not processed - Reached limit of retry attempts allowed"; break;
	        case "S" : $result = "Duplicate SessionID (OrderInfo)"; break;
	        case "T" : $result = "Address Verification Failed"; break;
	        case "U" : $result = "Card Security Code Failed"; break;
	        case "V" : $result = "Address Verification and Card Security Code Failed"; break;
	        default  : $result = "Unable to be determined"; 
	    }
	    return $result;
	}
	
	//  ----------------------------------------------------------------------------
	
	// This function uses the QSI AVS Result Code retrieved from the Digital
	// Receipt and returns an appropriate description for this code.
	
	// @param vAVSResultCode String containing the QSI AVS Result Code
	// @return description String containing the appropriate description
	
	function displayAVSResponse($avsResultCode) {
	    
	    if ($avsResultCode != "") { 
	        switch ($avsResultCode) {
	            Case "Unsupported" : $result = "AVS not supported or there was no AVS data provided"; break;
	            Case "X"  : $result = "Exact match - address and 9 digit ZIP/postal code"; break;
	            Case "Y"  : $result = "Exact match - address and 5 digit ZIP/postal code"; break;
	            Case "S"  : $result = "Service not supported or address not verified (international transaction)"; break;
	            Case "G"  : $result = "Issuer does not participate in AVS (international transaction)"; break;
	            Case "A"  : $result = "Address match only"; break;
	            Case "W"  : $result = "9 digit ZIP/postal code matched, Address not Matched"; break;
	            Case "Z"  : $result = "5 digit ZIP/postal code matched, Address not Matched"; break;
	            Case "R"  : $result = "Issuer system is unavailable"; break;
	            Case "U"  : $result = "Address unavailable or not verified"; break;
	            Case "E"  : $result = "Address and ZIP/postal code not provided"; break;
	            Case "N"  : $result = "Address and ZIP/postal code not matched"; break;
	            Case "0"  : $result = "AVS not requested"; break;
	            default   : $result = "Unable to be determined"; 
	        }
	    } else {
	        $result = "null response";
	    }
	    return $result;
	}
	
	//  ----------------------------------------------------------------------------
	
	// This function uses the QSI CSC Result Code retrieved from the Digital
	// Receipt and returns an appropriate description for this code.
	
	// @param vCSCResultCode String containing the QSI CSC Result Code
	// @return description String containing the appropriate description
	
	function displayCSCResponse($cscResultCode) {
	    
	    if ($cscResultCode != "") {
	        switch ($cscResultCode) {
	            Case "Unsupported" : $result = "CSC not supported or there was no CSC data provided"; break;
	            Case "M"  : $result = "Exact code match"; break;
	            Case "S"  : $result = "Merchant has indicated that CSC is not present on the card (MOTO situation)"; break;
	            Case "P"  : $result = "Code not processed"; break;
	            Case "U"  : $result = "Card issuer is not registered and/or certified"; break;
	            Case "N"  : $result = "Code invalid or not matched"; break;
	            default   : $result = "Unable to be determined"; break;
	        }
	    } else {
	        $result = "null response";
	    }
	    return $result;
	}
	
	//  -----------------------------------------------------------------------------
	
	// This method uses the verRes status code retrieved from the Digital
	// Receipt and returns an appropriate description for the QSI Response Code
	
	// @param statusResponse String containing the 3DS Authentication Status Code
	// @return String containing the appropriate description
	
	function getStatusDescription($statusResponse) {
	    if ($statusResponse == "" || $statusResponse == "No Value Returned") {
	        $result = "3DS not supported or there was no 3DS data provided";
	    } else {
	        switch ($statusResponse) {
	            Case "Y"  : $result = "The cardholder was successfully authenticated."; break;
	            Case "E"  : $result = "The cardholder is not enrolled."; break;
	            Case "N"  : $result = "The cardholder was not verified."; break;
	            Case "U"  : $result = "The cardholder's Issuer was unable to authenticate due to some system error at the Issuer."; break;
	            Case "F"  : $result = "There was an error in the format of the request from the merchant."; break;
	            Case "A"  : $result = "Authentication of your Merchant ID and Password to the ACS Directory Failed."; break;
	            Case "D"  : $result = "Error communicating with the Directory Server."; break;
	            Case "C"  : $result = "The card type is not supported for authentication."; break;
	            Case "S"  : $result = "The signature on the response received from the Issuer could not be validated."; break;
	            Case "P"  : $result = "Error parsing input from Issuer."; break;
	            Case "I"  : $result = "Internal Payment Server system error."; break;
	            default   : $result = "Unable to be determined"; break;
	        }
	    }
	    return $result;
	}
	
	//  -----------------------------------------------------------------------------
	   
	// If input is null, returns string "No Value Returned", else returns input
	function null2unknown($data) {
	    if ($data == "") {
	        return "No Value Returned";
	    } else {
	        return $data;
	    }
	} 
	    
	//  ----------------------------------------------------------------------------
	die;

?>