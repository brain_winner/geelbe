<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito", "clases")); 
        $postURL = Aplicacion::getIncludes("post","micarrito_fincompra");
        $datosUser = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try
    {
        ValidarUsuarioLogueado();
        
         if(isset($_GET['IdPedido']) && is_numeric($_GET['IdPedido'])) {
	        $MiCarrito = dmPedidos::getPedidoById($_GET['IdPedido']);    	
			if($MiCarrito->getIDEstadoPedidos() == 0)
				$IdPedido = $_GET['IdPedido'];			
		}	
    	
    	if(isset($IdPedido)) {
    	    $Articulos = $MiCarrito->getProductos();
    	    $IdProvincia = $MiCarrito->getDireccion()->getIdProvincia();
    	} else {
    		$MiCarrito = Aplicacion::getCarrito();
	   	    $Articulos = $MiCarrito->getArticulos();
	   	    $IdProvincia = $_SESSION["User"]["DireccionEnvio"]["cbxProvincia"];
    	}
        
        //$MiCarrito = Aplicacion::getCarrito();
        $objCampania = dmCampania::getByIdCampania($MiCarrito->getIdCampania());
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('../accesodenegado/accesodenegado.php?errNo=<?=Aplicacion::Encrypter($e->getCodigo())?>&r=<?=Aplicacion::Encrypter($e->getTitulo())?>','_self');</script>
    <?
        exit;
    }
    
    $bono = (isset($_POST['txtBonoUtilizado']) ? $_POST['txtBonoUtilizado'] : 0);
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="/<?=$confRoot[1]?>/front/geelbe.css" rel="stylesheet" type="text/css" />
<link href="/<?=$confRoot[1]?>/front/micarrito.css" rel="stylesheet" type="text/css" />
<script src="/<?=$confRoot[1]?>/front/micarrito/js/js.js" type="text/javascript"></script>
<script src="/<?=$confRoot[1]?>/js/jquery.js" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">
  </div>
    <div id="contenido">
<form id="frmFinalizar" name="frmFinalizar" action="../../../<?=Aplicacion::getDirLocal().$postURL["micarrito_fincompra"]?>" method="post">

	<?php
		if(isset($IdPedido))
			echo '<input type="hidden" name="IdPedido" value="'.$IdPedido.'" />';
	?>

	<div id="top">	
	  <h1>Mi Carrito
      </h1>
	  	  <div id="topright" >
		<div class="right" style="margin: 0 5px 0 0;">
		  
		</div>
	  </div> 
    </div>
	<div class="pasos">
		<div class="paso">Informaci�n del Env�o</div>
		<div class="paso">Confirmaci�n de  Compra</div>
		<div class="pasoact">Formas de Pago</div>
	</div>
	
	<h3>Contenido de tu carrito de compras</h3>
	<img src="/<?=$confRoot[1]?>/front/images/carrito-top.gif" alt="" width="687" height="10" class="left" />
	<div class="carro">
	  <table border="0" cellpadding="3" cellspacing="0" bgcolor="#FFFFFF" style="width:100%;">
          <tr>
            <td width="89" valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td width="399" valign="top" bgcolor="#F5F5F5"><h2>Producto</h2></td>
            <td width="64" valign="top" bgcolor="#F5F5F5"><h2>Cantidad</h2></td>
            <td width="84" valign="top" bgcolor="#F5F5F5"><h2>Importe</h2></td>
          </tr>
          
          <?            
            $Total = 0;
            $Peso = 0;
            $Ganancia = 0;
            foreach($Articulos as $IdCodigoProdInterno => $Articulo)
            {
            
            	//Para reanudar un pedido debo transformar
            	//objeto en array
            	if(!is_array($Articulo)) {
            		$IdCodigoProdInterno = $Articulo->getIdCodigoProdInterno();
            		$Articulo = array('Cantidad' => $Articulo->getCantidad());
            	}
            
                $objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
                $objProducto = dmProductos::getById($objPP->getIdProducto());
                $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($IdCodigoProdInterno);
                $arrImg = DirectorytoArray(Aplicacion::getRoot() . "front/productos/productos_".$objProducto->getIdProducto()."/", 1, array("jpg", "jpeg", "gif", "png"));
                ?>
                    <tr>
                      <td valign="top">
                      <?
                        if($arrImg[0])
                        {
                            ?>
                                <img src="<?=Aplicacion::getRootUrl()."front/productos/productos_".$objProducto->getIdProducto()."/".$arrImg[0]?>" alt="" width="48" />
                            <?
                        }
                        else
                        {
                            ?>
                               <img src="/<?=$confRoot[1]?>/front/productos/noimagen.jpg" alt="" width="48"  /><!--height="50"-->
                            <?                            
                        }
                      ?>
                      </td>
                        <td valign="top"><h3><?=$objProducto->getNombre()?></h3>
                        <h4>
                            <?
                                foreach($Atributos as $atributo)
                                {
                                    echo $atributo["Nombre"]." = ".$atributo["Valor"] . " | ";
                                }
                            ?>
                        </h4></td>
                        <td valign="top"><p><?=$Articulo["Cantidad"]?></p></td>
                        <td valign="top"><p><?=Moneda(ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo["Cantidad"]);?></p></td>
                      </tr>
                <?
                        $dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($MiCarrito->getIdCampania());
                        
                        $free = 1;
                        foreach($dtCampaniasPropiedades as $propiedad)
                        {
                            if($propiedad["IdPropiedadCampamia"]=='6')
                            {
                               $free = 0;
                            }
                        }                
                $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
                $Peso += $P * $Articulo["Cantidad"];
                $Ganancia +=abs(($objProducto->getPVenta() - $objProducto->getPCompra())*$Articulo["Cantidad"]);
                $Total += ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo["Cantidad"];
            }
            if($free!=0)
            {
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("
                	(
					SELECT p.Tarifa, p.TarifaKG, ps.Hasta
					FROM `dhl_precios` p
					LEFT JOIN `dhl_zonas_x_estados` zp ON p.IdZona = zp.IdZona
					LEFT JOIN `dhl_pesos` ps ON ps.IdPeso = p.IdPeso
					WHERE zp.IdEstado = ".mysql_real_escape_string($IdProvincia)." AND ps.Desde <= ".$Peso." AND ps.Hasta >= ".$Peso."
					)
					
					UNION
					
					(
					SELECT p.Tarifa, p.TarifaKG, ps.Hasta
					FROM `dhl_precios` p
					LEFT JOIN `dhl_zonas_x_estados` zp ON p.IdZona = zp.IdZona
					LEFT JOIN `dhl_pesos` ps ON ps.IdPeso = p.IdPeso
					WHERE zp.IdEstado = ".mysql_real_escape_string($IdProvincia)."
					ORDER BY p.IdPeso DESC LIMIT 1
					)
                ");

                $Tabla = $oConexion->DevolverQuery();

            	if(isset($Tabla[0])) {    
                	$datos = $Tabla[0];
                	$importe = $Tabla[0]['Tarifa'];
                	
                	//Si supera el m�ximo, contar kilos
                	$Extra = $Peso - $datos['Hasta'];
                	if($Extra > 0) {
                		$importe += $Tabla[0]['TarifaKG'] * $Extra;
                	}
                	
	                $Total += $importe;
    	            $Envio = Moneda($importe);
        	        $ValorEnvio = $importe;
        	        
        	    } else {
        	    
        	    	$ValorEnvio = 0;
        	    	$Envio = "Sin Cargo";
        	    	
        	    }

            }
            else
            {
                $Envio = "Sin Cargo";
                $ValorEnvio = 0;
            }
            
            $Total -= $bono;
          ?>
          <tr>
            <td valign="middle"><img src="/<?=$confRoot[1]?>/front/images/home_next.png" alt="" width="24" height="24" /></td>
            <td valign="top"><h3>Costo de Env�o</h3>
              <p>Todos los env&iacute;os de Geelbe son realizados por un servicio de mensajer&iacute;a independiente</p></td>
            <td valign="top">&nbsp;</td>
            <td valign="top"><?=$Envio;?></td>
            <input id="txtGastosEnvio" name="txtGastosEnvio" type="hidden" value="<?=$ValorEnvio;?>"/>
          </tr>
          <tr>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5"><h3>Cr�ditos utilizados de tu cuenta</h3></td>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5"><h2><span id="CreditoUtilizado"><?=Moneda($bono)?></span></h2></td>
          </tr>
          <tr>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5"><h3>Total de la Compra </h3></td>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5"><h3> 
            <span id="verTotalCompra"><?=Moneda($Total)?></span>
            </h3></td>
          </tr>
      </table>
    </div>
	<img src="/<?=$confRoot[1]?>/front/images/carrito-bottom.gif" alt="" width="687" height="11" style="margin-top:-5px;" />
	
	<!--<div class="right" style="margin: 0 5px 0 0;">
		  <h3>C�digo de Compra: 00000000 </h3>
	</div>-->
	
	<div class="line" style="margin-top: 20px"></div>
	<div class="tit1">Seleccione su Forma de Pago:</div> 
	
	<div class="bloque1">
		<div class="fleft">
			<label class="bot" for="banamex" id="banamexLabel">
			    <input type="radio" name="paymentMethod" id="banamex" value="8" />
			    <img src="/<?=$confRoot[1]?>/front/images/carrito/banamex.jpg" align="absmiddle" />
			</label>
		</div>
	</div>

	<div class="bloque2" style="margin-top: 20px">
		<div class="tit1">Otras Opciones de Pago:</div>
		
		<div class="bot3">
			<div>
				<input type="radio" name="paymentMethod" id="paypal" value="2" checked="checked" onChange="showConf(this.value, 1)" />
			    <img src="/<?=$confRoot[1]?>/front/images/carrito/paypal.jpg" width="156" height="40" align="absmiddle" /><br />
			    <img src="/<?=$confRoot[1]?>/front/images/carrito/tarjetas-paypal.jpg" width="174" height="61" /> 
			</div>
			<div class="text2">Puedes realizar el pago de tus  productos con cr&eacute;dito PayPal o Tarjeta de Cr&eacute;dito, la cual es procesada online  a trav&eacute;s de un servidor certificado seguro, por v&iacute;as de encriptaci&oacute;n de 128  bits. Esta seguridad permite garantizar a los usuarios la confidencialidad y  seguridad de los datos de sus tarjetas de cr&eacute;dito.</div>
		</div>
		
		<div class="bot3">
			<div>
				<input type="radio" name="paymentMethod" id="dineromail" value="1" onChange="showConf(this.value, 1)" />
			    <img src="/<?=$confRoot[1]?>/front/images/carrito/dineromail.jpg);?>" width="156" height="40" align="absmiddle" /><br />
		       <img src="/<?=$confRoot[1]?>/front/images/carrito/tarjetas-dineromail.jpg" width="179" height="58" /> <br />
			</div>
			<div class="text2">Puedes realizar el pago de tus  productos con cr&eacute;dito DineroMail o Tarjeta de Cr&eacute;dito, la cual es procesada  online a trav&eacute;s de un servidor certificado seguro, por v&iacute;as de encriptaci&oacute;n de  128 bits. Esta seguridad permite garantizar a los usuarios la confidencialidad  y seguridad de los datos de sus tarjetas de cr&eacute;dito.</div>
		</div>
				
  		<?php
			    // Safety pay data
			$proxySTP = new SafetyPayProxy();
			$proxySTP->LetKeys(Aplicacion::getParametros("saftpay", "apikey"), Aplicacion::getParametros("saftpay", "signaturekey"));
			$txtCommunicationTest = $proxySTP->CommunicationTest();
			if($txtCommunicationTest == 'Communication Successful') {
			
				$toCurrency = null;
	
				if($ValorEnvio <= 150000)
					$curr = stp_GetCurrencies($proxySTP, 'ES', 'MXN', ($Total - $ValorEnvio), $toCurrency, $refNo, $conversion);
				else
					$curr = stp_GetCurrencies($proxySTP, 'ES', 'MXN', $Total, $toCurrency, $refNo, $conversion);
		?>
		
		<div class="bot3">
			<div>
				<input type="radio" name="paymentMethod" id="saftpay" value="3" onChange="showConf(this.value, 0)" />
		    	<img src="/<?=$confRoot[1]?>/front/images/carrito/safetypay.jpg" width="156" height="40" align="absmiddle" /><br />
		    	<img src="/<?=$confRoot[1]?>/front/images/carrito/tarjetas-safetpay.jpg" width="176" height="63" /> </div>
				<div class="text2">Puedes realizar el pago de  tus&nbsp;productos con saldo de tu cuenta bancaria. SafetyPay es el sistema  seguro de pagos electr&oacute;nicos que te permite pagar online directamente a trav&eacute;s  de tu banca electr&oacute;nica.</div>
			</div>
		</div>
		
		<div id="saftpayData">
			<?php
				if($ValorEnvio <= 150000)
					echo '<p style="margin-bottom: 10px"><strong>Has seleccionado pagar tu pedido con SafetyPay, el costo de env&iacute;o ser&aacute; bonificado. El total de tu pedido es: '.Moneda($Total - $ValorEnvio).'.</strong></p>'
			?>
		
  			<label for="curr">
  				Selecciona tu moneda
  				<select id="curr" name="saftpay[curr]">
  					<option>Selecciona una moneda</option>
  					<?=$curr;?>
  				</select>
  			</label>
  			
  			<div id="cargando" style="display:none">
  				Cargando...
  			</div>
  			
  			<div id="cargado" style="display: none">
      			<label for="banco">
      				Selecciona tu banco
	  				<select id="banco" name="saftpay[bank]">
  					</select>
  				</label>
  			
  				<label for="safttotal">
  					Monto a pagar
      				<input type="text" readonly="readonly" id="safttotal" name="saftpay[total]">
      			</label>         			
  			
	  			<input type="hidden" name="saftpay[refno]" id="saftref" />
	  		</div>
  		</div>
  		
  		<script type="text/javascript">
  			document.getElementById('saftpayData').style.display = 'none';
  			function showConf(el, b) {
  				if(el && !b)
  					document.getElementById('saftpayData').style.display = 'block';
  				else
  					document.getElementById('saftpayData').style.display = 'none';
  			}
  			setCurrency(document.getElementById('curr'))
  		</script>
		
		<?php	
			} else {
		?>
		<div class="bot3">
			<div>
				<p>El pago con SafetyPay no est� disponible temporalmente.</p>
			</div>
		</div>
		</div>
		<?php	
			}
  		?>

       <table cellspacing="0" cellpadding="2" border="0">   		
         <tbody>
          <tr align="left">
            <td scope="col">&nbsp;</td>
            <td scope="col">
            	<div style="text-align:right">
            		<a href="javascript:FinalizarCompra();"><img  src="/<?=$confRoot[1]?>/front/images/boton-compra.gif" alt="Finalizar Compra" width="131" height="20" /></a>
					<!--a href="javascript:frmInvitacionManual.FinalizarCompra();"><img  src="../images/boton-compra.gif" alt="Finalizar Compra" width="131" height="20" /></a!-->
                </div>
                </td>
             </tr>
        </tbody>
      </table> 
	  <input type="hidden" id="txtTotal" name="txtTotal" value="<?=$Total?>"/>
	  <input type="hidden" id="txtValorEnvio" name="txtValorEnvio" value="<?=$ValorEnvio?>"/>
      <input type="hidden" id="txtBonoUtilizado" name="txtBonoUtilizado" value="<?=$bono?>"/>
</form>
</div>
  <div id="contenido-bot"></div>
</div>	
<? require("../menuess/footer.php")?>
<iframe style="display:none" name="iContacto" id="iContacto"></iframe> 
<script>
    var MaxCreditos = <?=(isset($disponibles)) ? $disponibles:0?>;
    var Ganancia = <?=(isset($Ganancia)) ? $Ganancia:0?>;
    var Total = <?=(isset($Total)) ? $Total:0?>;
</script>
</body>
</html>
