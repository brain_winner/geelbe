<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito", "clases")); 
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("mispedidos")); 
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito")); 
        $postURL = Aplicacion::getIncludes("post","micarrito_fincompra");
        $datosUser = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));

   }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }   
    try
    {
        if(isset($_GET['IdPedido']) && is_numeric($_GET['IdPedido'])) {
	        $MiCarrito = dmPedidos::getPedidoById($_GET['IdPedido']);    	
			if($MiCarrito->getIDEstadoPedidos() == 0)
				$IdPedido = $_GET['IdPedido'];			
		}	
    	
    	if(isset($IdPedido)) {
    	    $Articulos = $MiCarrito->getProductos();
    	    $IdProvincia = $MiCarrito->getDireccion()->getIdProvincia();
    	} else {
    		$MiCarrito = Aplicacion::getCarrito();
	   	    $Articulos = $MiCarrito->getArticulos();
	   	    $IdProvincia = $_SESSION["User"]["DireccionEnvio"]["cbxProvincia"];
    	}
    	
   	    $objCampania = dmCampania::getByIdCampania($MiCarrito->getIdCampania());
        
        $arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$MiCarrito->getIdCampania()."/imagenes/", 1, array("jpg", "jpeg", "gif", "png"));
        $arrArchivos = QuitarExtensiones_arrImagenes(ArrayValue_to_Index($arrArchivos));

       $objDireccion = new MySQL_PedidosDirecciones();
        $objDireccion->setCP($_SESSION["User"]["DireccionEnvio"]["txtCP"]);
        $objDireccion->setDomicilio($_SESSION["User"]["DireccionEnvio"]["txtDomicilio"]);
        $objDireccion->setEscalera($_SESSION["User"]["DireccionEnvio"]["txtEscalera"]);
        $objDireccion->setIdPais($_SESSION["User"]["DireccionEnvio"]["cbxPais"]);
        $objDireccion->setIdProvincia($_SESSION["User"]["DireccionEnvio"]["cbxProvincia"]);
        $objDireccion->setNumero($_SESSION["User"]["DireccionEnvio"]["txtNumero"]);
        $objDireccion->setPoblacion($_SESSION["User"]["DireccionEnvio"]["txtPoblacion"]);
        $objDireccion->setPiso($_SESSION["User"]["DireccionEnvio"]["txtPiso"]);
        $objDireccion->setPuerta($_SESSION["User"]["DireccionEnvio"]["txtPuerta"]);
        $objDireccion->setTipoCalle($_SESSION["User"]["DireccionEnvio"]["cbxTipoCalle"]);
        $objDireccion->setHorarioEntrega($_SESSION["User"]["DireccionEnvio"]["cbxHorarioEntrega"]);
        $objDireccion->setIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        $objDireccion->setNombre($_SESSION["User"]["DireccionEnvio"]["txtNombre"]);
        $objDireccion->setApellido($_SESSION["User"]["DireccionEnvio"]["txtApellido"]);
        $objDireccion->setNroDoc($_SESSION["User"]["DireccionEnvio"]["txtDNI"]);
        $objDireccion->setTelefono($_SESSION["User"]["DireccionEnvio"]["txtTelefono"]);
        $objDireccion->setCodTelefono($_SESSION["User"]["DireccionEnvio"]["txtCodTelefono"]);
        $objDireccion->setceluempresa($_SESSION["User"]["DireccionEnvio"]["cbxCeluEmpresa"]);
        $objDireccion->setcelucodigo($_SESSION["User"]["DireccionEnvio"]["txtCelularCodigo"]);
        $objDireccion->setcelunumero($_SESSION["User"]["DireccionEnvio"]["txtCelularNumero"]);
        $objUsuario = new MySQL_micuenta();
        $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));

    }
    catch(ACCESOException $e)
    { 
    ?>  
        <script>window.open('../accesodenegado/accesodenegado.php?errNo=<?=Aplicacion::Encrypter($e->getCodigo())?>&r=<?=Aplicacion::Encrypter($e->getTitulo())?>','_self');</script>
    <?
        exit;
    }
    
    if(isset($_SESSION['Carrito']['Bono'])) $_POST['txtBonoUtilizado'] = $_SESSION['Carrito']['Bono'];
    
    $bono = (isset($_POST['txtBonoUtilizado']) ? $_POST['txtBonoUtilizado'] : 0);
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="/<?=$confRoot[1]?>/front/micarrito/styleNew.css" rel="stylesheet" type="text/css" />
<link href="/<?=$confRoot[1]?>/front/geelbe.css" rel="stylesheet" type="text/css" />
<link href="/<?=$confRoot[1]?>/front/micarrito.css" rel="stylesheet" type="text/css" />
<script src="/<?=$confRoot[1]?>/front/micarrito/js/js.js" type="text/javascript"></script>
<script src="/<?=$confRoot[1]?>/js/jquery.js" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">
  </div>
    <div id="contenido">
<form id="frmFinalizar" name="frmFinalizar" action="../../../<?=Aplicacion::getDirLocal().$postURL["micarrito_fincompra"]?>"  method="post">

<?php
	if(isset($IdPedido))
		echo '<input type="hidden" name="IdPedido" value="'.$IdPedido.'" />';
?>

<div class="todo">
<div><br /></div>
<div class="micarrito">
  <div class="tit1">Formas de Pago</div>
<br />
Compre los productos de esta marca en 3 sencillos pasos</div>
<div><a href="../catalogo/index.php?IdCampania=<?=$MiCarrito->getIdCampania()?>"><img src="../../front/campanias/archivos/campania_<?=$MiCarrito->getIdCampania()?>/imagenes/<?=$arrArchivos["logo2"]?>" alt="Nombre Marca" class="logo-prod" style="margin-top:-70px;" /></a>
</div>
<div><img src="/<?=$confRoot[1]?>/front/micarrito/imgNew/formas-de-pago.jpg" width="675" height="76" /></div>
<br />
<div class="productos-pago"><img src="/<?=$confRoot[1]?>/front/micarrito/imgNew/top-productos.jpg" width="675" height="12" />
 
 <?            
            $Total = 0;
            $Peso = 0;
            $Ganancia = 0;
            foreach($Articulos as $IdCodigoProdInterno => $Articulo)
            {
            	//Para reanudar un pedido debo transformar
            	//objeto en array
            	if(!is_array($Articulo)) {
            		$IdCodigoProdInterno = $Articulo->getIdCodigoProdInterno();
            		$Articulo = array('Cantidad' => $Articulo->getCantidad());
            	}
            
                $objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
                $objProducto = dmProductos::getById($objPP->getIdProducto());
                $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($IdCodigoProdInterno);
                $arrImg = DirectorytoArray(Aplicacion::getRoot() . "front/productos/productos_".$objProducto->getIdProducto()."/", 1, array("jpg", "jpeg", "gif", "png"));
                ?>
                      <div class="item-producto"><div class="img-prod">
                      <?
                        if($arrImg[0])
                        {
                            ?>
                                <img src="<?=Aplicacion::getRootUrl()."front/productos/productos_".$objProducto->getIdProducto()."/".$arrImg[0]?>" alt="" width="48" />
                            <?
                        }
                        else
                        {
                            ?>
                               <img src="/<?=$confRoot[1]?>/front/productos/noimagen.jpg" alt="48" width="48"  /><!--height="50"-->
                            <?                            
                        }
                      ?>
                      </div>
					  <div class="tit6"><?=$Articulo['Cantidad'];?> x <?=$objProducto->getNombre()?>
					  <div class="text7" style="float:none"><?
                                foreach($Atributos as $atributo)
                                {
                                    echo $atributo["Nombre"]." = ".$atributo["Valor"] . " | ";
                                }
                            ?>
                     </div></div>
 					<div class="total-producto"><strong><?=Moneda(ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo["Cantidad"]);?></strong></div></div>
                <?
                        $dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($MiCarrito->getIdCampania());
                        
                        $free = 1;
                        foreach($dtCampaniasPropiedades as $propiedad)
                        {
                            if($propiedad["IdPropiedadCampamia"]=='6')
                            {
                               $free = 0;
                            }
                        }                
                $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
                $Peso += $P * $Articulo["Cantidad"];
                $Ganancia +=abs(($objProducto->getPVenta() - $objProducto->getPCompra())*$Articulo["Cantidad"]);
                $Total += ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo["Cantidad"];
            }
            if($free!=0)
            {
                //$objDHL = dmDHL::getTarifaByPesoPcia($Peso, $objDireccion->getIdProvincia());
                //$Total += $objDHL->getImporte();
                //$Envio = Moneda($objDHL->getImporte());
                //$ValorEnvio = $objDHL->getImporte();
                
                //Devuelve el precio para el peso del pedido
                //Si no lo encuentra, devuelve el mayor peso
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("
                	(
					SELECT p.Tarifa, p.TarifaKG, ps.Hasta
					FROM `dhl_precios` p
					LEFT JOIN `dhl_zonas_x_estados` zp ON p.IdZona = zp.IdZona
					LEFT JOIN `dhl_pesos` ps ON ps.IdPeso = p.IdPeso
					WHERE zp.IdEstado = ".$objDireccion->getIdProvincia()." AND ps.Desde <= ".$Peso." AND ps.Hasta >= ".$Peso."
					)
					
					UNION
					
					(
					SELECT p.Tarifa, p.TarifaKG, ps.Hasta
					FROM `dhl_precios` p
					LEFT JOIN `dhl_zonas_x_estados` zp ON p.IdZona = zp.IdZona
					LEFT JOIN `dhl_pesos` ps ON ps.IdPeso = p.IdPeso
					WHERE zp.IdEstado = ".$objDireccion->getIdProvincia()."
					ORDER BY p.IdPeso DESC LIMIT 1
					)
                ");

                $Tabla = $oConexion->DevolverQuery();

            	if(isset($Tabla[0])) {    
                	$datos = $Tabla[0];
                	$importe = $Tabla[0]['Tarifa'];
                	
                	//Si supera el m�ximo, contar kilos
                	$Extra = $Peso - $datos['Hasta'];
                	if($Extra > 0) {
                		$importe += $Tabla[0]['TarifaKG'] * $Extra;
                	}
                	
	                $Total += $importe;
    	            $Envio = Moneda($importe);
        	        $ValorEnvio = $importe;
        	        
        	    } else {
        	    
        	    	$ValorEnvio = 0;
        	    	$Envio = "Sin Cargo";
        	    	
        	    }
            }
            else
            {
                $Envio = "Sin Cargo";
                $ValorEnvio = 0;
            }            
            $Total -= $bono;
          ?>
 <input id="txtGastosEnvio" name="txtGastosEnvio" type="hidden" value="<?=$ValorEnvio;?>"/>
</div>

<div class="total-totalcompra">
<div class="izq-formasdepago">
  <div class="text5"><strong>Costo de Env�o</strong></div> <div class="total-creditos"><strong><?=$Envio;?></strong></div>
  <div class="text5"><strong>Descuento por Cr�ditos</strong></div> <div class="total-creditos"><strong><?=Moneda($bono);?></strong></div>
  </div>
<div>
  <div class="text8"><strong>Total de la Compra</strong></div> <div class="total-final"><strong><?=Moneda($Total)?></strong></div></div>
</div>
<br />
<div><img src="/<?=$confRoot[1]?>/front/micarrito/imgNew/banner-banco.jpg" width="675" height="95" /></div>
<div class="tit-seleccione"><div class="tit7">Seleccione la forma m�s adecuada para su compra</div></div>
	
	<div class="bloque1">
		<div class="fleft">
			<label class="bot" for="banamex" id="banamexLabel">
			    <input type="radio" name="paymentMethod" id="banamex" value="8" />
			    <img src="/<?=$confRoot[1]?>/front/images/carrito/banamex.jpg" align="absmiddle" />
			</label>
		</div>
	</div>

	<div class="bloque2" style="margin-top: 20px">
		<div class="tit1">Otras Opciones de Pago:</div>
		
		<div class="bot3">
			<div>
				<input type="radio" name="paymentMethod" id="paypal" value="2" checked="checked" onChange="showConf(this.value, 1)" />
			    <img src="/<?=$confRoot[1]?>/front/images/carrito/paypal.jpg" width="156" height="40" align="absmiddle" /><br />
			    <img src="/<?=$confRoot[1]?>/front/images/carrito/tarjetas-paypal.jpg" width="174" height="61" /> 
			</div>
			<div class="text2">Puedes realizar el pago de tus  productos con cr&eacute;dito PayPal o Tarjeta de Cr&eacute;dito, la cual es procesada online  a trav&eacute;s de un servidor certificado seguro, por v&iacute;as de encriptaci&oacute;n de 128  bits. Esta seguridad permite garantizar a los usuarios la confidencialidad y  seguridad de los datos de sus tarjetas de cr&eacute;dito.</div>
		</div>
		
		<div class="bot3">
			<div>
				<input type="radio" name="paymentMethod" id="dineromail" value="1" onChange="showConf(this.value, 1)" />
			    <img src="/<?=$confRoot[1]?>/front/images/carrito/dineromail.jpg" width="156" height="40" align="absmiddle" /><br />
		       <img src="/<?=$confRoot[1]?>/front/images/carrito/tarjetas-dineromail.jpg" width="179" height="58" /> <br />
			</div>
			<div class="text2">Puedes realizar el pago de tus  productos con cr&eacute;dito DineroMail o Tarjeta de Cr&eacute;dito, la cual es procesada  online a trav&eacute;s de un servidor certificado seguro, por v&iacute;as de encriptaci&oacute;n de  128 bits. Esta seguridad permite garantizar a los usuarios la confidencialidad  y seguridad de los datos de sus tarjetas de cr&eacute;dito.</div>
		</div>
				
  		<?php
			    // Safety pay data
			$proxySTP = new SafetyPayProxy();
			$proxySTP->LetKeys(Aplicacion::getParametros("saftpay", "apikey"), Aplicacion::getParametros("saftpay", "signaturekey"));
			$txtCommunicationTest = $proxySTP->CommunicationTest();
			if($txtCommunicationTest == 'Communication Successful') {
			
				$toCurrency = null;
	
				if($ValorEnvio <= 150000)
					$curr = stp_GetCurrencies($proxySTP, 'ES', 'MXN', ($Total - $ValorEnvio), $toCurrency, $refNo, $conversion);
				else
					$curr = stp_GetCurrencies($proxySTP, 'ES', 'MXN', $Total, $toCurrency, $refNo, $conversion);
		?>
		
		<div class="bot3">
			<div>
				<input type="radio" name="paymentMethod" id="saftpay" value="3" onChange="showConf(this.value, 0)" />
		    	<img src="/<?=$confRoot[1]?>/front/images/carrito/safetypay.jpg" width="156" height="40" align="absmiddle" /><br />
		    	<img src="/<?=$confRoot[1]?>/front/images/carrito/tarjetas-safetpay.jpg" width="176" height="63" /> </div>
				<div class="text2">Puedes realizar el pago de  tus&nbsp;productos con saldo de tu cuenta bancaria. SafetyPay es el sistema  seguro de pagos electr&oacute;nicos que te permite pagar online directamente a trav&eacute;s  de tu banca electr&oacute;nica.</div>
			</div>
		</div>
		
		<div id="saftpayData">
			<?php
				if($ValorEnvio <= 150000)
					echo '<p style="margin-bottom: 10px"><strong>Has seleccionado pagar tu pedido con SafetyPay, el costo de env&iacute;o ser&aacute; bonificado. El total de tu pedido es: '.Moneda($Total - $ValorEnvio).'.</strong></p>'
			?>
		
  			<label for="curr">
  				Selecciona tu moneda
  				<select id="curr" name="saftpay[curr]">
  					<option>Selecciona una moneda</option>
  					<?=$curr;?>
  				</select>
  			</label>
  			
  			<div id="cargando" style="display:none">
  				Cargando...
  			</div>
  			
  			<div id="cargado" style="display: none">
      			<label for="banco">
      				Selecciona tu banco
	  				<select id="banco" name="saftpay[bank]">
  					</select>
  				</label>
  			
  				<label for="safttotal">
  					Monto a pagar
      				<input type="text" readonly="readonly" id="safttotal" name="saftpay[total]">
      			</label>         			
  			
	  			<input type="hidden" name="saftpay[refno]" id="saftref" />
	  		</div>
  		</div>
  		
  		<script type="text/javascript">
  			document.getElementById('saftpayData').style.display = 'none';
  			function showConf(el, b) {
  				if(el && !b)
  					document.getElementById('saftpayData').style.display = 'block';
  				else
  					document.getElementById('saftpayData').style.display = 'none';
  			}
  			setCurrency(document.getElementById('curr'))
  		</script>
		
		<?php	
			} else {
		?>
		<div class="bot3">
			<div>
				<p>El pago con SafetyPay no est� disponible temporalmente.</p>
			</div>
		</div>
		</div>
		<?php	
			}
  		?>

       <table cellspacing="0" cellpadding="2" border="0">   		
         <tbody>
          <tr align="left">
            <td scope="col">&nbsp;</td>
            <td scope="col">
            	<div style="text-align:right">
            		<a href="javascript:FinalizarCompra();"><img  src="/<?=$confRoot[1]?>/front/images/boton-compra.gif" alt="Finalizar Compra" width="131" height="20" /></a>
					<!--a href="javascript:frmInvitacionManual.FinalizarCompra();"><img  src="../images/boton-compra.gif" alt="Finalizar Compra" width="131" height="20" /></a!-->
                </div>
                </td>
             </tr>
        </tbody>
      </table> 
	  <input type="hidden" id="txtTotal" name="txtTotal" value="<?=$Total?>"/>
	  <input type="hidden" id="txtValorEnvio" name="txtValorEnvio" value="<?=$ValorEnvio?>"/>
      <input type="hidden" id="txtBonoUtilizado" name="txtBonoUtilizado" value="<?=$bono?>"/>
</form>
</div>
</div>
  <div id="contenido-bot"></div>
</div>	
<? require("../menuess/footer.php")?>
<iframe style="display:none" name="iContacto" id="iContacto"></iframe> 
<script>
    var MaxCreditos = <?=(isset($disponibles)) ? $disponibles:0?>;
    var Ganancia = <?=(isset($Ganancia)) ? $Ganancia:0?>;
    var Total = <?=(isset($Total)) ? $Total:0?>;
</script>
</body>
</html>
