<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito", "clases")); 
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("accesodenegado"));
        $postURL = Aplicacion::getIncludes("post","contacto");		
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }   
    try
    {
        $MiCarrito = Aplicacion::getCarrito();  
        $objCampania = dmCampania::getByIdCampania($MiCarrito->getIdCampania());
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('../accesodenegado/accesodenegado.php?errNo=<?=Aplicacion::Encrypter($e->getCodigo())?>&r=<?=Aplicacion::Encrypter($e->getTitulo())?>','_self');</script>
    <?
        exit;
    }
    try
    {
        $datosUser = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        $objUsuario = new MySQL_micuenta();
        $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        $arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$MiCarrito->getIdCampania()."/imagenes/", 1, array("jpg", "jpeg", "gif", "png"));
        $arrArchivos = QuitarExtensiones_arrImagenes(ArrayValue_to_Index($arrArchivos));
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title> 
<link href="/<?=$confRoot[1]?>/front/geelbe.css" rel="stylesheet" type="text/css" />
<link href="/<?=$confRoot[1]?>/front/botones.css" rel="stylesheet" type="text/css" />
<script src="/<?=$confRoot[1]?>/front/micarrito/js/js.js" type="text/javascript"></script>

<script src="/<?=$confRoot[1]?>/front/micuenta/js/getCPData.js" type="text/javascript"></script>
<script src="/<?=$confRoot[1]?>/front/micarrito/js/js.js" type="text/javascript"></script>
<script src="/<?=$confRoot[1]?>/js/jquery.js" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
    <body> 
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">
  
  </div>
  <div id="contenido">
	<div id="top">	
	  <h1>Mi Carrito
      </h1>
	  <div id="topright" >
		<div class="right" style="margin: 0 5px 0 0;">
            
		</div>
	  </div> 
    </div>
<div class="pasos">
	<div class="pasoact">Informaci�n del Env�o</div>
	<div class="paso">Confirmaci�n de  Compra</div>
	<div class="paso">Formas de Pago</div>
</div>
	<!--<div class="right" style="margin: 0 5px 0 0;">
		  <h3>C�digo de Compra: 00000000 </h3>
	</div>-->
    <form  method="POST" name="frmlMiCarrito" id="frmlMiCarrito" target="iMiCarrito" onSubmit="ValidarForm(this);" action="../../../<?=Aplicacion::getDirLocal().$postURL["micarrito"]?>">
	<h3>Contenido de tu carrito de compras</h3>
	<img src="/<?=$confRoot[1]?>/front/images/carrito-top.gif" alt="" width="687" height="10" class="left"/>
	<div class="carro">
	  <table border="0" cellpadding="3" cellspacing="0" bgcolor="#FFFFFF" style="width:100%;">
          <tr>
            <td valign="top" bgcolor="#F5F5F5">&nbsp;</td>
            <td valign="top" bgcolor="#F5F5F5"><h2>Producto</h2></td>
            <td valign="top" bgcolor="#F5F5F5"><h2>Cantidad</h2></td>
            <td valign="top" bgcolor="#F5F5F5"><h2>Importe</h2></td>
            <td valign="top" bgcolor="#F5F5F5"><h2>Eliminar</h2></td>
          </tr>
          <?            
            foreach($MiCarrito->getArticulos() as $IdCodigoProdInterno => $Articulo)
            {
                $objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
                $objProducto = dmProductos::getById($Articulo["IdProducto"]);
                $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($IdCodigoProdInterno);
                $arrImg = DirectorytoArray(Aplicacion::getRoot() . "front/productos/productos_".$objProducto->getIdProducto()."/", 1, array("jpg", "jpeg", "gif", "png"));
                ?>
                    <tr>
                    <td valign="top">
                    <?
                        if ($arrImg[0])
                        {
                        ?>
                            <a href="<?=Aplicacion::getRootUrl()."front/catalogo/index.php?IdCampania=".$MiCarrito->getIdCampania()?>"><img src="<?=Aplicacion::getRootUrl()."front/productos/productos_".$objProducto->getIdProducto()."/".$arrImg[0]?>" alt="" width="48" height="50" /></a>
                        <?
                        }
                        else
                        {
                         ?>
                            <a href="<?=Aplicacion::getRootUrl()."front/catalogo/index.php?IdCampania=".$MiCarrito->getIdCampania()?>"><img src="/<?=$confRoot[1]?>/front/productos/noimagen.jpg" alt="" width="48" height="50" /></a>
                         <?   
                        }
                    ?>
                      </td>
                        <td valign="top"><h3><a href="<?=Aplicacion::getRootUrl()."front/catalogo/index.php?IdCampania=".$MiCarrito->getIdCampania()?>"><?=$objProducto->getNombre()?></a></h3>
                        <h4>
                            <?         
                                foreach($Atributos as $atributo)
                                {
                                    echo $atributo["Nombre"]." = ".$atributo["Valor"] . " | ";
                                }
                            ?>
                        </h4></td>
                        <td valign="top"><p>
                          <select name="frmA[cbxCantidad][<?=$IdCodigoProdInterno?>]" id="frmA[cbxCantidad][<?=$IdCodigoProdInterno?>]" onChange="document.getElementById('verImporte<?=$Articulo["IdCodigoProdInterno"]?>').innerHTML= Moneda(this.value * document.getElementById('txtImporte<?=$IdCodigoProdInterno?>').value, MONEDA_DECIMALES, MONEDA_FORMATO);">
                            <?
							$disponible = min($Articulo["Stock"],$objCampania->getMaxProd());
                                /*for($i=1; $i<=$objCampania->getMaxProd(); $i++)*/
                                for($i=1; $i<=$disponible; $i++)
                                {
                                    ?><option value="<?=$i?>"><?=$i?></option><?
                                }
                            ?>
                          </select>
                          <span id="sinStock<?=$IdCodigoProdInterno?>" style="display:none;color:red">stock insuficiente</span>
                          <script>
                            document.getElementById("frmA[cbxCantidad][<?=$IdCodigoProdInterno?>]").value ="<?=$Articulo["Cantidad"]?>";
                          </script>
                        </p>            </td>
                        <td valign="top">
                            <p>
                                <span id="verImporte<?=$Articulo["IdCodigoProdInterno"]?>"><?=Moneda(ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo["Cantidad"])?></span> 
                                <input type="hidden"  id="txtImporte<?=$IdCodigoProdInterno?>" name="txtImporte<?=$IdCodigoProdInterno?>" value="<?=ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento())?>"/>
                            </p>
                        </td>
                        <td valign="top">
                          <p>
							   <!-- <input name="frmBorrar[]" id="frmBorrar[]" type="checkbox" class="radio" value="<?=$Id?>"/> -->
                              <a href="javascript:Redirect('<?=Aplicacion::getRootUrl() . $postURL["micarrito_eliminarproducto"]?>?IdCodigoProdInterno=<?=$IdCodigoProdInterno?>', '_self')"><img src="/<?=$confRoot[1]?>/front/images/delete_btn.gif" border="0" alt="Eliminar"/></a>
                          </p>
                        </td>
                      </tr>
                <?
            }
          ?>
      </table>
    </div>
      <img src="/<?=$confRoot[1]?>/front/images/carrito-bottom.gif" alt="" width="687" height="11" style="margin-top:-5px;" />    
      <table cellspacing="0" cellpadding="2" border="0">
        <tbody>
          <tr>
            <td colspan="3"  background="/<?=$confRoot[1]?>/front/images/gris-top.gif" scope="col"></td>
          </tr>
          <tr>
            <td colspan="3" bgcolor="#F0F0F0" scope="col"><strong>Informaci�n de Env�o </strong></td>
          </tr>
          <tr>
            <td bgcolor="#F0F0F0" scope="col"> Nombre<br />
                <input size="30" name="frmDU[txtNombre]" id="frmDU[txtNombre]" value="<?=$objUsuario->getDatos()->getNombre()?>"/></td>
            <td bgcolor="#F0F0F0" scope="col">Apellido<br />
                <input size="30" name="frmDU[txtApellido]" id="frmDU[txtApellido]" value="<?=$objUsuario->getDatos()->getApellido()?>" /></td>
          </tr>
          <tr>
            <td valign="top" bgcolor="#F0F0F0" scope='col' >Tel&eacute;fono<br />
                <input name="frmDU[txtCodTelefono]" id="frmDU[txtCodTelefono]" value="<?=$objUsuario->getDatos()->getCodTelefono()?>" type="text" class="imputbportada" onBlur="(esInteger(this.value)?this.value = this.value:this.value=0)" onKeyPress="return solosIntegersKEYCODE(event);" maxlength="3" size="3"/>
                <input name="frmDU[txtTelefono]" id="frmDU[txtTelefono]" value="<?=$objUsuario->getDatos()->getTelefono()?>" type='text' class='imputbportada' onBlur="(esInteger(this.value)?this.value = this.value:this.value=0)" onKeyPress="return solosIntegersKEYCODE(event);" size="15" maxlength="15" />
                <h4>Ingresa el C�digo de &Aacute;rea y luego el N�mero de Tel�fono</h4></td>
            <td colspan="2" bgcolor="#F0F0F0" scope="col"><p>Tel&eacute;fono M�vil<br />
                <select name="frmDU[cbxCeluEmpresa]" id="frmDU[cbxCeluEmpresa]">
                      <option value="0">Compa&ntilde;ia</option>
                      <option value="1">Telcel</option>
                      <option value="2">MoviStar</option>
                      <option value="3">Iusacell</option>
                      <option value="4">Nextel </option>
                        <option value="5">OTRO</option>
                </select>
                <script>
                <?
                    if($objUsuario->getDatos()->getceluempresa() != "")
                    {
                        ?>
                            document.getElementById("frmDU[cbxCeluEmpresa]").value = "<?=$objUsuario->getDatos()->getceluempresa()?>";
                        <?
                    }
                ?>
                </script>
                <input name="frmDU[txtCelularCodigo]" id="frmDU[txtCelularCodigo]" value="<?=$objUsuario->getDatos()->getcelucodigo()?>" type='text' class='imputbportada' onBlur="(esInteger(this.value)?this.value = this.value:this.value=0)" onKeyPress="return solosIntegersKEYCODE(event);" size="3" maxlength="3" />
                <input name="frmDU[txtCelularNumero]" id="frmDU[txtCelularNumero]" value="<?=$objUsuario->getDatos()->getcelunumero()?>" type='text' class='imputbportada' onBlur="(esInteger(this.value)?this.value = this.value:this.value=0)" onKeyPress="return solosIntegersKEYCODE(event);" size="15" maxlength="15" />
</p>
            <h4>Ingresa el C�digo de &Aacute;rea y luego el N�mero de Tel�fono sin 044 ni guiones</h4></td>
          </tr>

          <tr>
            <td colspan="3"  background="/<?=$confRoot[1]?>/front/images/gris-both.gif" scope="col"></td>
          </tr>
          <tr>
            <td colspan="3" align="left" bgcolor="#F0F0F0" scope="col"><strong>Direcci�n de Env�o</strong></td>
          </tr>
          <tr>
          	<td colspan="3">
		            Pa�s: <strong style="color:#666;">M�xico</strong>
            </td>
          </tr>
          <tr>
            <td colspan="3" align="left" bgcolor="#F0F0F0" scope="col"><table border="0" cellspacing="0" cellpadding="0">
              
              <tr>
                <td>Calle <br />
                    <input name="frmDir[txtDomicilio]" id="frmDir[txtDomicilio]" value="<?=$objUsuario->getDirecciones(0)->getDomicilio()?>" type='text' class='imputbportada' size='30' />
                    </span>
                </td>
                <td>N�mero Exterior<br />
                    <input name="frmDir[txtNumero]" id="frmDir[txtNumero]" value="<?=$objUsuario->getDirecciones(0)->getNumero()?>" type='text' class='imputbportada' size='8' /></td>
                <td>N�mero Interior<br />
                    <input name="frmDir[txtPiso]" id="frmDir[txtPiso]" value="<?=$objUsuario->getDirecciones(0)->getPiso()?>" type='text' class='imputbportada' size='5' /></td>
                 
              </tr>
            </table></td>
          </tr>
          <tr>
            <td colspan="3" align="left" bgcolor="#F0F0F0" scope="col"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
                	<td style="padding-top: 10px">
                		Buscar por:
                		
                		<input type="radio" id="buscarPorCP" name="buscarPor" value="1" onChange="showAndHide()" checked="checked" />
                		C�digo Postal
                		
                		<input type="radio" id="buscarManual" name="buscarPor" value="2" onChange="showAndHide()" />
                		Manualmente
                	
                	</td>
                </tr>
             <tr>
              	<td><div id="porCP">
              		C�digo Postal<br />
                  
				  <table>
				  <tr>
				  <td width="5%" style="padding:0px" >
				  <input id='txtcp' type='text' size='8' value="<?=$objUsuario->getDirecciones(0)->getCP();?>" onChange="setCPReal(this.value)" onKeyUp="setCPReal(this.value)" />
                  
                  <input type="hidden" id="cpReal" name="frmDir[txtCP]" value="<?=$objUsuario->getDirecciones(0)->getCP();?>" />
				  <td width="95%" style="padding:0px" >
				  <table class="btn btnViolet izquierda chico XXS" cellpadding="0" cellspacing="0" border="1px"> 
					<tr>
						<td>
							<a href="javascript:;"  onClick="setCP(document.getElementById('txtcp').value, 'txtpbl', 'cbxprov', 'hide', true)"><strong>Buscar</strong></a>
						</td>
					</tr>
				</table>
				  </td>
				  </tr>
				  </table>
				  </div>
                  </td>
              </tr>
              <tr>
                <td width="24%">
                <div class="hide">
                	Estado<br />
                  <select name='frmDir[cbxProvincia]' id='cbxprov' class='imputbportada' onChange="getCiudades(this.options[this.selectedIndex].value, 'txtpbl', 0, true)"> 
                  <option value="0" >Estados</option>
                                                        <? 
                         $dt = dmMicuenta::GetProvincias();
                         foreach($dt as $value)
                         {
                            ?>
                               <option value='<?=$value['IdProvincia']?>' <?=($objUsuario->getDirecciones(0)->getIdProvincia() == $value['IdProvincia'] ? 'selected="selected"': '')?>><?=$value['Nombre']?></option>
                            <?
                         }                                                 
                  ?>                                            
                  </select>
                  </div>
                  </td>
              </tr>
              <tr>
                  <td >
	                <div class="hide city" style="display:none">
	                	Ciudad<br />
	                  <select name='frmDir[txtPoblacion]' id='txtpbl' style="width:180px">
	                  </select>
	                  <script type="text/javascript">
	                    var poblacion = '<?=$objUsuario->getDirecciones(0)->getPoblacion();?>';
	                    if(poblacion != '') {
	                  		getCiudades(document.getElementById('cbxprov').options[document.getElementById('cbxprov').selectedIndex].value, 'txtpbl', poblacion, true);
	                    }
	                  </script>
	                </div>  
                 </td>
              </tr>
              <tr>
              	<td><div id="manual" style="display: none">
              		C�digo Postal<br />
              		<select id="cbxCP">
	                  	<option></option>
	                 </select>
	             </div></td>
              </tr>
             
                <!--<td>Horario de Entrega<br />
                  <select name='frmDir[cbxHorarioEntrega]' id='frmDir[cbxHorarioEntrega]'>
                    <option value="de 9:00 a 13:00 hs" selected="selected">de 9:00 a 12:00 hs</option>
                    <option value="de 13:00 a 20:00 hs" selected="selected">de 13:00 a 16:00 hs</option>
                  </select></td>-->
              </tr>
            </table>
                  <label>
                    <input name="chkDirPredeterminada" id="chkDirPredeterminada" checked="checked" type="checkbox"  class="radio" value="OK" />
                  Selecciona la casilla si deseas mantener la informaci�n de env�o como predeterminada.</label>
            </td>
          </tr>

          <tr>
            <td colspan="3"  background="/<?=$confRoot[1]?>/front/images/gris-bottom.gif" scope="col"></td>
          </tr>

			  <table>
					<tr>
					<td>
					<table class="btn btnViolet izquierda normal XXL" cellpadding="0" cellspacing="0" border="1px">
						<tr>
							<td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
								<a href="<?=Aplicacion::getRootUrl()."front/catalogo/index.php?IdCampania=".$MiCarrito->getIdCampania()?>"><strong>Continuar Comprando</strong></a>
							</td>
						</tr>
					</table>
					</td>
					<td>
					<table class="btn btnRed derecha normal XL" cellpadding="0" cellspacing="0" border="1px">
						<tr>
							<td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
								<a href="javascript:document.forms[0].onsubmit();"><strong>Ir a paso 2 de 3</strong>&nbsp;&nbsp;&nbsp;<img style="vertical-align:bottom" src="/<?=$confRoot[1]?>/front/images/bot_ico_flecha_vidrierasmall.jpg" border="none"/></a>
							</td>
						</tr>
					</table>
					</td>
					</tr>
				</table>
			 </tbody>
		  </table>
	  		 	  	
      </form>
  </div>
  <div id="contenido-bot"></div>
</div>

<? require("../menuess/footer.php")?>
<iframe style="display:none" name="iMiCarrito" id="iMiCarrito"></iframe> 
</body>
</html>
