<?
	if(!isset($_GET['id']) || !is_numeric($_GET['id']))
		die;

    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito")); 
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }   
    
	$n = new Conexion();
	$base = Aplicacion::getParametros("conexion","base");
	mysql_select_db($base);
	$data = mysql_query("SELECT s.*, p.IdUsuario FROM `pedidos_saftpay` s LEFT JOIN `pedidos` p ON p.IdPedido = s.IdPedido WHERE p.IdPedido = ".$_GET['id']);

	if(mysql_num_rows($data) == 0) die;
	
	$data = mysql_fetch_assoc($data);
	
	if($data['IdUsuario'] != Aplicacion::Decrypter($_SESSION['User']['id'])) die;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>

<link href="/<?=$confRoot[1]?>/front/geelbe.css" rel="stylesheet" type="text/css" />
<link href="/<?=$confRoot[1]?>/front/micarrito.css" rel="stylesheet" type="text/css" />
<script src="/<?=$confRoot[1]?>/front/micarrito/js/js.js");?>" type="text/javascript"></script>
<script src="/<?=$confRoot[1]?>/js/jquery.js");?>" type="text/javascript"></script>


<style type="text/css">
	#contenido p, #contenido li { margin: 20px 0; }
	
	#contenido ul { margin: 0 20px}
	
	a.pay { text-align: center; display: block; }
</style>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">
  </div>
    <div id="contenido">
<form id="frmFinalizar" name="frmFinalizar" action="../../../<?=Aplicacion::getDirLocal().$postURL["micarrito_fincompra"]?>" method="post">

	<div id="top">	
	  <h1>Mi Carrito
      </h1>
    </div>
	<div class="pasos">
		<div class="paso">Informaci�n del Env�o</div>
		<div class="paso">Confirmaci�n de  Compra</div>
		<div class="pasoact">Formas de Pago</div>
	</div>
		<!--<div class="right" style="margin: 0 5px 0 0;">
		  <h3>C�digo de Compra: 00000000 </h3>
	</div>-->
	<h2 style="color:#333!important;">�Gracias por comprar en <strong>Geelbe</strong>. Su pedido N&uacute;mero <?=$data['IdPedido'];?> est&aacute; en proceso de pago</h2>
	
	<img src="/<?=$confRoot[1]?>/front/images/carrito/safetylogo.jpg/images/carrito/safetylogo.jpg" alt="SAFETYPAY" style="float: right; margin: 10px;" />
	<p>Elegiste pagar con <strong>SAFETYPAY</strong>, por favor toma nota de los siguientes datos para completar el pago directamente en tu Banca Electr�nica:</p>
    <div style="padding:15px; font-size:14px; border:1px solid #930;">
    <div style="font-weight:bold; color:#900;">Esta informaci&oacute;n deber&aacute; ser ingresada en la pr&oacute;xima pantalla, por favor tome nota:</div>
	
	<p><strong>N�mero de Transacci�n:</strong> <?=$data['transactionId'];?><br />
	<strong>Moneda:</strong> <?=$data['currency'];?><br />
	<strong>Monto total:</strong> <?=$data['amount'];?><br /></p>
	
    </div>
	<p>Para completar tu pago ser�s redireccionado a la Banca Electr�nica de tu Banco. <br />
	Ingresa con tu usuario y claves habituales. Si tu banco es:</p>
	
	<ul>
		<li><strong>Banamex y Scotiabank</strong>: Selecciona la cuenta de pago, verifica el monto a pagar (no necesitas capturarlo) y confirma tu pago.</li>
		<li><strong>Santander:</strong> Selecciona la pesta�a Pagos y dentro de ella selecciona Servicios. Elige la Cuenta de pago y da click en el Bot�n de SAFETYPAY, captura el n�mero de transacci�n y el monto, y confirma tu pago.</li>
	</ul>
	
	<a href="<?=$data['url'];?>" class="pay"><img src="/<?=$confRoot[1]?>/front/images/boton-compra.gif" alt="Completar el pago" /></a>
	
	<p><strong>Complete su pago en este momento, en caso contrario deber� realizar nuevamente el proceso de compra.</strong></p>
	
</form>
</div>
  <div id="contenido-bot"></div>
</div>	
<? require("../menuess/footer.php")?>
<iframe style="display:none" name="iContacto" id="iContacto"></iframe> 
<script>
    var MaxCreditos = <?=(isset($disponibles)) ? $disponibles:0?>;
    var Ganancia = <?=(isset($Ganancia)) ? $Ganancia:0?>;
    var Total = <?=(isset($Total)) ? $Total:0?>;
</script>
</body>
</html>
