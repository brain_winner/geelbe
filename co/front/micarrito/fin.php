<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito", "clases")); 
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }
    
    //Validar estado pedido
    $_GET['id'] = $_SESSION['lastPedidoId'];
	
	//Buscar la fecha de entrega

	$arrDatos = dmPedidos::getDatosMailByIdPedido($_SESSION['lastPedidoId']);
	$diasEntregaIn = $arrDatos["tiempoEntregaIn"];
	$diasEntregaFn = $arrDatos["tiempoEntregaFn"];
	
	$usuario = dmUsuario::getByIdUsuario($arrDatos['IdUsuario']);
	$pedido = dmPedidos::getPedidoById($_SESSION['lastPedidoId']);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="/<?=$confRoot[1]?>/front/geelbe.css" rel="stylesheet" type="text/css" />
<link href="/<?=$confRoot[1]?>/front/botones.css" rel="stylesheet" type="text/css" />

<script src="/<?=$confRoot[1]?>/front/micarrito/js/js.js" type="text/javascript"></script>
<script src="/<?=$confRoot[1]?>/js/jquery.js" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">
  </div>
    <div id="contenido">

	<div id="top">	
	  <h1>Fin de Compra
      </h1>
    </div>
    
    <div style="background-color:#d1d1e6; color:#000000; text-align:center; height:160px; padding-top:20px;">
    
  	<div style="float:left; width:160px; padding:10px; padding-left:25px;" >
    	
    	<img style="margin-left:-25px;" src="/<?=$confRoot[1]?>/front/images/postpago_ilust_1.jpg" />
        <div style="width:125px; text-align:center; margin-top:8px;">
        <p>Aprobaci�n de pago</p>
        </div>
    </div>
    
    <div style="float:left; width:220px; padding:10px; padding-left:25px;">
    	<img src="/<?=$confRoot[1]?>/front/micarrito/images/postpago_ilust_2.jpg" />
        <div style="width:190px; padding-top:8px; text-align:center;">
	        <p style="font-weight:bold;">El pedido se despachar� de nuestros dep�sitos en <? echo $diasEntregaIn/24; ?>-<? echo $diasEntregaFn/24; ?> d�as h�biles</p>
        </div>
   </div>
   
   
   <div style="float:left; width:160px; padding:10px; padding-left:25px;" ><img style="margin-left:-30px;" src="/<?=$confRoot[1]?>/front/micarrito/images/postpago_ilust_3.jpg" />
        <div style="width:125px; text-align:center; margin-top:8px;">
        <p>Se env�a el pedido<br />a trav�s de Envio</p>
        </div>
  </div>
  
  </div>
    
  <table class="btn btnRed derecha normal L" cellpadding="0" cellspacing="0" border="1px"> 
						<tr>
							<td>
								<a href="../vidriera/index.php"><strong>Ir a la vitrina</strong>&nbsp;&nbsp;&nbsp;<img style="vertical-align:bottom" src="/<?=$confRoot[1]?>/front/images/bot_ico_flecha_vidrierasmall.jpg" border="none"/></a>
							</td>
						</tr>
					</table>


<br />
<br />
<br />

	
	
</div>
  <div id="contenido-bot"></div>
</div>	
<? require("../menuess/footer.php")?>
<iframe style="display:none" name="iContacto" id="iContacto"></iframe> 
<script>
    var MaxCreditos = <?=(isset($disponibles)) ? $disponibles:0?>;
    var Ganancia = <?=(isset($Ganancia)) ? $Ganancia:0?>;
    var Total = <?=(isset($Total)) ? $Total:0?>;
</script>

<!-- MentAd Code for Purchase Notification -->
<script type="text/javascript">

    var mentad_website_id = "A0D740hqzM_s"; // Unique ID supplied by MentAd
    var mentad_purchase_total = "<?=$arrDatos['total']?>"; // The total purchase price as a number. For example: 123.45
    var mentad_purchase_currency = "COP"; // The 3 letter currency code. For example: USD, EUR, ARS, NIS
    var mentad_purchase_order_id = "<?=$_SESSION['lastPedidoId']?>"; // The unique order ID. For example: "AB-123456"
    
    var mentad_purchase_items = [];
    // Iterate over the items in the cart and add them to the "mentad_purchase_items" array
    // i.e. for (var item in cart) {
    
    <?php foreach($pedido->getProductos() as $prod):
    		$p = dmProductos::getByIdCodigoProdInterno($prod->getIdCodigoProdInterno());
    		$p = dmProductos::getById($p->getIdProducto());
    		
    		$dir = "../productos/productos_".$p->getIdProducto()."/";
			$arrImg = current(DirectorytoArray($dir, 1, array("jpg", "jpeg", "gif", "png")));

    ?>
      mentad_purchase_items.push({ 
          sku: "<?=$p->getIdProducto()?>", // The unique product ID. For example: "AB-00001234"
          price: "<?=$prod->getPrecio()?>", // The price of one item as a number. For example: 12.34
          quantity: "<?=$prod->getCantidad()?>", // The amount of items of this type. For example: 2
          name: "<?=$p->getNombre()?>", // The name of this item (used for display). For example: "Kindle 3G"
          image_url: "http://geelbe.com/<?=$confRoot[1]?>/front/productos/productos_<?=$p->getIdProducto()?>/<?=$arrImg?>" // A URL of an image of this item (used for display). For example: "http://a.com/img/1234.jpg"
      });
    <?php endforeach; ?>
    
    //}
    
    // Add the details of the shopper for data mining purposes
    var mentad_shopper_info = {
        title: "###", // The title of the purchaser. For example: "Mr"
        first_name: "<?=$arrDatos['nombreUsuario']?>", // The first name of the purchaser. For example: "John"
        last_name: "<?=$arrDatos['apellidoUsuario']?>", // The last name of the purchaser. For example: "Doe"
        birthdate: "<?=$usuario->getDatos()->getFechaNacimiento()?>", // The birthdate of the purchaser in DD-MM-YYYY format. For example: "31-01-1970"
        email: "<?=$arrDatos['emailUsuario']?>", // The email of the purchaser. For example: "a@b.com"
        phone: "<?=$usuario->getDatos()->getTelefono()?>", // The phone number of the purchaser. For example: +123-4-5678901
        mobile: "<?=$usuario->getDatos()->getCeluCodigo()?> <?=$usuario->getDatos()->getCeluNumero()?>" // The mobile phone number of the purchaser. For example: +123-4-5678901
    };
    
    // Trigger the purchase notification asynchronously
    (function(){
		var js, id = 'mentad-notification'; if (document.getElementById(id)) {return;}
		js = document.createElement('script'); js.id = id; js.async = true;
		js.src = "//mentad.com/static/js/mentad-purchase-notification.js";
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(js, s);
	})();
	
</script>

</body>
</html>
