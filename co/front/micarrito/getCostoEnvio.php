<?
	ob_start();
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito", "clases")); 
        $postURL = Aplicacion::getIncludes("post","contacto");        
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/login');
		exit;
    }   	
    try
    {
        $MiCarrito = Aplicacion::getCarrito();  
        $objCampania = dmCampania::getByIdCampania($MiCarrito->getIdCampania());
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('../accesodenegado/accesodenegado.php?errNo=<?=Aplicacion::Encrypter($e->getCodigo())?>&r=<?=Aplicacion::Encrypter($e->getTitulo())?>','_self');</script>
    <?
        exit;
    }
    try
    {
    	if(isset($_GET['c']))
    		$Cantidades = unserialize($_GET['c']);
    	else
    		$Cantidades = array();
    
        $datosUser = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        $objUsuario = new MySQL_micuenta();
        $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        $arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$MiCarrito->getIdCampania()."/imagenes/", 1, array("jpg", "jpeg", "gif", "png"));
        $arrArchivos = QuitarExtensiones_arrImagenes(ArrayValue_to_Index($arrArchivos));
        $objDireccion = $objUsuario->getDirecciones(0);
        
        foreach($MiCarrito->getArticulos() as $IdCodigoProdInterno => $Articulo)
        {
            $objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
            $objProducto = dmProductos::getById($Articulo["IdProducto"]);
            $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($IdCodigoProdInterno);
            $arrImg = DirectorytoArray(Aplicacion::getRoot() . "front/productos/productos_".$objProducto->getIdProducto()."/", 1, array("jpg", "jpeg", "gif", "png"));
                        
            $dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($MiCarrito->getIdCampania());
                        
            $free = 1;
            foreach($dtCampaniasPropiedades as $propiedad)
            {
                if($propiedad["IdPropiedadCampamia"]=='6')
                {
                   $free = 0;
                }
            }
            
             $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
             
             if(isset($Cantidades[$IdCodigoProdInterno]))
             	$Peso += $P * $Cantidades[$IdCodigoProdInterno];
             else
             	$Peso += $P * $Articulo["Cantidad"];
        }
        
         if($free!=0)
            {
            
            	if(isset($_GET['p']) && is_numeric($_GET['p']))
		    		$provincia = $_GET['p'];
		    	else
		    		$provincia = $objDireccion->getIdProvincia();
            
                
                //$objDHL = dmDHL::getTarifaByPesoPcia($Peso, $objDireccion->getIdProvincia());
                //$Total += $objDHL->getImporte();
                //$Envio = Moneda($objDHL->getImporte());
                //$ValorEnvio = $objDHL->getImporte();
                
                //Devuelve el precio para el peso del pedido
                //Si no lo encuentra, devuelve el mayor peso
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("
                	(
					SELECT p.Tarifa, p.TarifaKG, ps.Hasta
					FROM `dhl_precios` p
					LEFT JOIN `dhl_zonas_x_estados` zp ON p.IdZona = zp.IdZona
					LEFT JOIN `dhl_pesos` ps ON ps.IdPeso = p.IdPeso
					WHERE zp.IdEstado = ".$provincia." AND ps.Desde <= ".$Peso." AND ps.Hasta >= ".$Peso."
					)
					
					UNION
					
					(
					SELECT p.Tarifa, p.TarifaKG, ps.Hasta
					FROM `dhl_precios` p
					LEFT JOIN `dhl_zonas_x_estados` zp ON p.IdZona = zp.IdZona
					LEFT JOIN `dhl_pesos` ps ON ps.IdPeso = p.IdPeso
					WHERE zp.IdEstado = ".$provincia."
					ORDER BY p.IdPeso DESC LIMIT 1
					)
                ");

                $Tabla = $oConexion->DevolverQuery();

            	if(isset($Tabla[0])) {    
                	$datos = $Tabla[0];
                	$importe = $Tabla[0]['Tarifa'];
                	
                	//Si supera el m�ximo, contar kilos
                	$Extra = $Peso - $datos['Hasta'];
                	if($Extra > 0) {
                		$importe += $Tabla[0]['TarifaKG'] * $Extra;
                	}
                	
	                $Total += $importe;
    	            $Envio = Moneda($importe);
        	        $ValorEnvio = $importe;
        	        
        	    } else {
        	    
        	    	$ValorEnvio = 0;
        	    	$Envio = "Sin Cargo";
        	    	
        	    }
            }
            else
            {
                $Envio = "Sin Cargo";
                $ValorEnvio = 0;
            }                   
        ob_end_clean();    
        echo $ValorEnvio;

    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    
?>
