<?
try {
    $confRoot = explode("/", dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    $postURL = 'logica/ems/login.php';
} catch (exception $e) {
    die(print_r($e));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<? Includes::Scripts() ?>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?= Aplicacion::getParametros("info", "nombre"); ?> </title>
        <link href="<?= UrlResolver::getImgBaseUrl("front/geelbe.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= UrlResolver::getImgBaseUrl("front/geelbe_new.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= UrlResolver::getImgBaseUrl("front/highslide.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?= UrlResolver::getImgBaseUrl("front/botones.css") ?>" rel="stylesheet" type="text/css" />

        <script src="<?= UrlResolver::getJsBaseUrl("front/ems/js/js_login.js") ?>" type="text/javascript"></script>

<? include $_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica_bo/background/actionform/background.php"; ?>
    </head>
    <body> 
        <? require("../menuess/menu_sin.php") ?>
        <div id="container">
            <div id="contenido-top">

            </div>
            <div id="contenido">
                <div id="top">    
                    <h1>Mis Emails</h1>
                </div>
                <div id="central">
                    <h2><strong>Por favor, ingresa tus datos para continuar</strong></h2>

                    <form action="/<?= Aplicacion::getDirLocal() . $postURL ?>" method="post" name="frmLogin2" id="frmLogin2" onkeypress="if (ValidarEnter(event)) this.submit();" target="iLogin">

                        E-mail<br />

                        <input type="text" maxlength="60" name="txtUser" id="txtUser" value="" class="form-text required" /><br />
                        Clave<br />
                        <input type="password" name="txtPass" id="txtPass" maxlength="60" class="form-text required" /><br />

                        <table class="btn btnRed izquierda normal M" cellpadding="0" cellspacing="0" border="1px">
                            <tr>
                                <td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
                                    <a href="javascript:;" onclick="ValidarFormEMS(document.frmLogin2);"><strong>Continuar</strong>&nbsp;&nbsp;</a>
                                </td>
                            </tr>
                        </table>

                        <br />
                    </form>


                </div>
                <div id="menuleft">

                </div>
            </div>
            <div id="contenido-bot"></div>
        </div>
        <? require("../menuess/footer_desuscriber.php") ?>
        <iframe id="iLogin" name="iLogin" style="display:none">
        </iframe>   
    </body>
</html>