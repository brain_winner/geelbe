function refrescar_captcha()
{
    document.getElementById("frmCaptcha").src = "../../logica/framework/class/imgcaptcha.php";
}
function ErrorMSJ(titulo, code)
{
    oCortina.showError(Array(arrErrores["LOGIN"][code]), titulo);
}
function ValidarFormEMS(frm)
{
    try
    {
        var arrError = new Array();
        if(!ValidarEmail($('#frmLogin2 #txtUser').get(0).value))
        {
            arrError.push(arrErrores["LOGIN"]["MAIL_INCORRECTO"]);
        }
        if($('#frmLogin2 #txtPass').get(0).value.length < 6 || $('#txtPass').get(0).value.length > 16)
        {
            arrError.push(arrErrores["LOGIN"]["CLAVE_INCORRECTO"]);
        }
        if (arrError.length>0)
        {
            oCortina.showError(arrError, arrErrores["LOGIN"]["TITLE_ERROR"]);
            return false;
        }
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}