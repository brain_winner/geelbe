<?

    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
        $postURL = Aplicacion::getIncludes("post", "micuenta");
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/front/ems/pre-desuscribir.php');
		exit;
    } 
    try
    {
       $objUsuario = new MySQL_micuenta();
       $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
    }
    catch(exception $e)
    {
        die(print_r($e));
    }   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getImgBaseUrl("front/geelbe.css")?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getImgBaseUrl("front/highslide.css")?>" rel="stylesheet" type="text/css" />
<link href="<?=UrlResolver::getImgBaseUrl("front/botones.css")?>" rel="stylesheet" type="text/css" />

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
 <? require("../menuess/menu_log.php") ?>
<div id="container">
  <div id="contenido-top">

  </div>
  <div id="contenido">
    <div id="top">    
      <h1>Mis Emails</h1>
    </div>
    <div id="central">
      <h2>A partir de ahora, ya no recibir&aacute;s nuestros mails de campa&ntilde;a y novedades.</h2>

      <br />
      
      <table class="btn btnRed izquierda normal M" cellpadding="0" cellspacing="0" border="1px">
									<tr>
										<td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
											<a href="/<?=$confRoot[1]?>/front/vidriera/"><strong>Ir a la vitrina</strong>&nbsp;&nbsp;</a>
										</td>
									</tr>
								</table>
    </div>
  <div id="menuleft">
        <? 
		require("../menuess/micuenta.php")
		?>
    </div>
</div>
  <div id="contenido-bot"></div>
</div>
<? require("../menuess/footer.php")?>
</iframe>   
</body>
</html>