<ul class="categoriasGlobales">
	<li <?=(!isset($_GET['IdCategoriaGlobal']) ? 'class="selected"' : '')?>><a href="/co/front/vidriera/index.php">TODAS</a></li>
	<?php
		$cats = dmCategoriaGlobal::getCategorias(null, 0, false);
		foreach($cats as $cat):
	?>
	<li <?=(isset($_GET['IdCategoriaGlobal']) && $_GET['IdCategoriaGlobal'] == $cat['IdCategoriaGlobal'] ? 'class="selected"' : '')?>>
		<a href="/co/front/vidriera/index.php?IdCategoriaGlobal=<?=$cat['IdCategoriaGlobal']?>"><?=$cat['Nombre']?></a>
		<!--<div class="sub">
			<?php //echo dmCategoriaGlobal::menu($cat['IdCategoriaGlobal']); ?>
			<div class="clear"></div>
		</div>-->
	</li>
	<?php endforeach; ?>
</ul>