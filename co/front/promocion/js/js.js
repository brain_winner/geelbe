function ErrorMSJ(titulo, code)
{
    oCortina.showError(Array(arrErrores["CONTACTO"][code]), titulo);
}
function InfoMSJ(titulo, code, pagina, showbutton)
{
        oCortina.showInfo(Array(arrErrores["CONTACTO"][code]), titulo, pagina, showbutton);
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtNombre.value.length == 0)
        {
            arrError.push(arrErrores["CONTACTO"]["APENOM"]);
        }
        if(frm.txtAsunto.value.length == 0)
        {
            arrError.push(arrErrores["CONTACTO"]["ASUNTO"]);
        }
       if(!ValidarEmail(frm.txtEmail.value))   
        {
            arrError.push(arrErrores["CONTACTO"]["MAIL_INCORRECTO"]);
        }
        if(frm.txtComentario.value.length == 0)
        {
            arrError.push(arrErrores["CONTACTO"]["COMENTARIO"]);
        }

      
        if (arrError.length >0)
        {
            oCortina.showError(arrError, arrErrores["CONTACTO"]["TITLE_ERROR"]);
            return false
        }   
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}