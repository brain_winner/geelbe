<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases")); 
    $postURL = Aplicacion::getIncludes("post","contacto");
    session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="<?=UrlResolver::getCssBaseUrl("front/geelbe.css")?>" rel="stylesheet" type="text/css" />

    <link href="<?=UrlResolver::getCssBaseUrl("front/botones.css");?>" rel="stylesheet" type="text/css" />
<script src="<?=UrlResolver::getJsBaseUrl("front/contacto/js/js.js");?>" type="text/javascript"></script>

<script type="text/javascript">

function ismaxlength(obj){
var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : ""
if (obj.getAttribute && obj.value.length>mlength)
obj.value=obj.value.substring(0,mlength)
}

</script>
</head>
<body> 
  <? require("../menuess/cabecera.php") ?>
<div id="container">
  <div id="contenido-top">
	<h1 style="margin-top:20px;">Contacto</h1>
  </div>
  <div id="contenido">
	<h3>Para cualquier consulta completa el siguiente formulario. A la brevedad nos pondremos en contacto contigo. </h3>

	 <span style="color:#F00; font-size:11px;">* Recuerda que nuestro Departamento de Atencion al Socio se encuentra disponible 
    de lunes a viernes <br />
    de 9:30 a 18:30 hs. </span></h3>
   <form method="post" id="frmContacto" name="frmContacto" action="../../../<?=Aplicacion::getDirLocal().$postURL["contacto"]?>" target="iContacto" onSubmit="ValidarForm(this);">
	<input name="txtMotivo" readonly="true" value="71" type="hidden" />
    <input name="isCampaniaSel" readonly="true" value="false" type="hidden" />
    
	<table cellspacing="0" cellpadding="2" border="0">
      <tbody>
        <tr>
          <td width="40%" bgcolor="#F0F0F0" scope="col">Nombre<br />
            <input name="txtNombre" type="text" size="30" />
          </td>
          <td bgcolor="#F0F0F0" scope="col">Apellido<br />
            <input name="txtApellido" type="text" size="30" />
          </td>
        </tr>
        <tr>
          <td valign="top" bgcolor="#F0F0F0" scope="col">Asunto<br />
            <input name="txtAsunto" type="text" size="30" />
          </td>
          <td bgcolor="#F0F0F0" scope="col">Email<br />
            <input name="txtEmail" type="text" class="imputbportada" size="30" />                 
           </td>
        </tr>
        
        <tr>
          <td height="10" colspan="2" align="left" bgcolor="#F0F0F0" scope="col">Comentario<br />
            <textarea name="txtComentario" cols="70" rows="4" maxlength="400" onkeyup="return ismaxlength(this)"></textarea>
           </td>
        </tr>  
          <!-- Captcha -->
          <tr>
            <td bgcolor="#F0F0F0" colspan="2"><strong>Ingresa el c�digo que observas en la imagen :</strong></td>
          </tr>
          <tr>
            <td bgcolor="#F0F0F0" colspan="2">
                <iframe src="<?=Aplicacion::getRootUrl()?>logica/framework/class/imgcaptcha.php" id="frmCaptcha" name="frmCaptcha" scrolling="no" frameborder="0" height="100">
                </iframe><br/>
                 &nbsp;&nbsp;&nbsp;<a href="javascript:refrescar_captcha();">Refrescar la imagen</a>
            </td>
          </tr>
          <tr>
            <td bgcolor="#F0F0F0" colspan="2">&nbsp;&nbsp;&nbsp;<input type="text" id="txtCaptcha" name="txtCaptcha" size="6" maxlength="6" />
            <br />
            <span class="links">&nbsp;&nbsp;&nbsp;Ingresa los caracteres en rojo respetando may&uacute;sculas y min&uacute;sculas.</span></td>
          </tr>
          <tr>
            <td bgcolor="#F0F0F0" colspan="3">
                
            </td>
          </tr>
          <!-- Fin captcha -->
        <tr>
          <td height="10" colspan="2" background="<?=UrlResolver::getImgBaseUrl("front/images/gris-bottom.gif");?>" scope="col"></td>
        </tr>
		      <tr align="left">
				<td scope="col" colspan="3">
			  	  <table class="btn btnRed derecha normal XS" cellpadding="0" cellspacing="0" border="1px">
				    <tr>
					  <td bordercolor="#9595aa" bordercolordark="#a50000" bordercolorlight="#a50000" >
					    <a href="javascript:document.forms['frmContacto'].onsubmit();"><strong>Enviar</strong>&nbsp;&nbsp;<img style="vertical-align:bottom" src="<?=UrlResolver::getImgBaseUrl("front/images/bot_ico_flecha_vidrierasmall.jpg");?>" border="none"/></a>
					  </td>
					</tr>
				  </table>
				</td>
		      </tr>
      </tbody>
    </table>
    </form>
  </div>
  <div id="contenido-bot"></div>
  <iframe style="display:none" name="iContacto" id="iContacto"></iframe> 
</div>
<? require("../menuess/footer.php")?>

</body>
</html>