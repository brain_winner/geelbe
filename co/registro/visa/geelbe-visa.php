<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
       
    }
    catch(exception $e)
    {
        die(print_r($e));
    }

    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Geelbe - Visa</title>

	<link href="<?=UrlResolver::getBaseUrl("registro/visa/css/landing-visa.css", true);?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getBaseUrl("logica/scripts/stylecortina.css", true);?>" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">
	    var DIRECTORIO_URL_JS = "http://js.static.geelbe.com/co/";
	</script>
	
	<script src="<?=UrlResolver::getBaseUrl("logica/scripts/cortina.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("logica/scripts/AC_RunActiveContent.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("logica/scripts/global_msj.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("logica/scripts/funcionesgenericas.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("js/jquery-1.4.2.min.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("js/jquery.simplemodal.js", true);?>" type="text/javascript"></script>
    <script src="<?=UrlResolver::getBaseUrl("js/jquery.geturlvars.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("registro/js/js.js", true);?>" type="text/javascript"></script>

</head>

<body>
	<div id="contenedor">
		<div id="header"><img src="http://js.static.geelbe.com/co/front/archivos/header_visa.jpg" alt="Geelbe" /></div>
	  <div id="contenido">
        		<div>
                    <h1>Visa te acerca a Geelbe!</h1>
                    <p>Completa el  formulario para convertirte en socio de <strong>Geelbe</strong>, el Club Privado de Compras por  Internet que te permite acceder a prestigiosas marcas con descuentos de <strong>hasta el 70%.</strong>        </p>
   		  </div>
        	<div><img src="<?=UrlResolver::getImgBaseUrl('registro/visa/images/foto_mujerbolsasciudad_op.jpg');?>" /></div>
        	
        <!-- inicio formulario -->
            <div id="formulario">

                    <form method="post" name="registerForm" id="registerForm" action="/co/front/registro/register.php">
            	<div class="col_izquierda">
                      <input type="hidden" class="smaller" name="landingtype" id="landingtype" value="" />
                      <div id="registerForm_errorloc" class="error_strings"></div>
                      <div class="form-campo"><label for="firstname">Nombre</label></div>
                      <input style="margin-bottom:5px;" size="30" name="firstname" id="firstname" type="text" value="" /><br />
                      <div class="form-campo"><label for="lastname">Apellido</label></div>
                      <input style="margin-bottom:5px;" size="30" name="lastname" id="lastname" type="text" value="" /><br />
                      
<div class="form-campo"><label for="birthdayday">Fecha de Nacimiento</label></div>
<table>
    <td width="14%">
        <select class="select_1_s" name="birthdayday">
            <option selected="selected" value="0">D&iacute;a</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <option value="13">13</option>
            <option value="14">14</option>
            <option value="15">15</option>
            <option value="16">16</option>
            <option value="17">17</option>
            <option value="18">18</option>
            <option value="19">19</option>
            <option value="20">20</option>
            <option value="21">21</option>
            <option value="22">22</option>
            <option value="23">23</option>
            <option value="24">24</option>
            <option value="25">25</option>
            <option value="26">26</option>
            <option value="27">27</option>
            <option value="28">28</option>
            <option value="29">29</option>
            <option value="30">30</option>
            <option value="31">31</option>
        </select>                                                      
    </td>
    <td width="24%">
        <select class="select_2_s" name="birthdaymonth">
            <option selected="selected" value="0">Mes</option>
            <option value="1">Enero</option>
            <option value="2">Febrero</option>
            <option value="3">Marzo</option>
            <option value="4">Abril</option>
            <option value="5">Mayo</option>
            <option value="6">Junio</option>
            <option value="7">Julio</option>
            <option value="8">Agosto</option>
            <option value="9">Septiembre</option>
            <option value="10">Octubre</option>
            <option value="11">Noviembre</option>
            <option value="12">Diciembre</option>
        </select>
    </td>
    <td width="62%">
        <select class="select_1_s" name="birthdayyear">
            <option selected="selected" value="0">Año</option>
            <option value="1935">1935</option>
            <option value="1936">1936</option>
            <option value="1937">1937</option>
            <option value="1938">1938</option>
            <option value="1939">1939</option>
            <option value="1940">1940</option>
            <option value="1941">1941</option>
            <option value="1942">1942</option>
            <option value="1943">1943</option>
            <option value="1944">1944</option>
            <option value="1945">1945</option>
            <option value="1946">1946</option>
            <option value="1947">1947</option>
            <option value="1948">1948</option>
            <option value="1949">1949</option>
            <option value="1950">1950</option>
            <option value="1951">1951</option>
            <option value="1952">1952</option>
            <option value="1953">1953</option>
            <option value="1954">1954</option>
            <option value="1955">1955</option>
            <option value="1956">1956</option>
            <option value="1957">1957</option>
            <option value="1958">1958</option>
            <option value="1959">1959</option>
            <option value="1960">1960</option>
            <option value="1961">1961</option>
            <option value="1962">1962</option>
            <option value="1963">1963</option>
            <option value="1964">1964</option>
            <option value="1965">1965</option>
            <option value="1966">1966</option>

            <option value="1967">1967</option>
            <option value="1968">1968</option>
            <option value="1969">1969</option>
            <option value="1970">1970</option>
            <option value="1971">1971</option>
            <option value="1972">1972</option>
            <option value="1973">1973</option>
            <option value="1974">1974</option>
            <option value="1975">1975</option>
            <option value="1976">1976</option>
            <option value="1977">1977</option>
            <option value="1978">1978</option>
            <option value="1979">1979</option>
            <option value="1980">1980</option>
            <option value="1981">1981</option>
            <option value="1982">1982</option>
            <option value="1983">1983</option>
            <option value="1984">1984</option>
            <option value="1985">1985</option>
            <option value="1986">1986</option>
            <option value="1987">1987</option>
            <option value="1988">1988</option>
            <option value="1989">1989</option>
            <option value="1990">1990</option>
            <option value="1991">1991</option>
            <option value="1992">1992</option>
            <option value="1993">1993</option>
            <option value="1994">1994</option>
            <option value="1995">1995</option>
            <option value="1996">1996</option>
            <option value="1997">1997</option>
            <option value="1998">1998</option>
            <option value="1999">1999</option>
            <option value="2000">2000</option>
        </select>
    </td>
</table>
                      <div class="form-campo"><label for="gender">Sexo</label></div>
                      <input class="radio" id="gender" value="1" name="gender" type="radio">
                      <label class="moving">Hombre</label>
                      <input class="radio" id="gender" value="2" name="gender" type="radio">
                      <label class="moving">Mujer</label><br />
                      <input name="tos" id="tos" type="checkbox" checked="checked" />
                      <label for="tos">Acepto los <a href="/co/front/terminos/" target="_blank">T&eacute;rminos y Condiciones</a></label><br />

					</div>

					<div class="col_derecha">
                      <div class="form-campo"><label for="email">Email</label></div>
                      <input style="margin-bottom:5px;" size="30" name="email" id="email" type="text" value="" /><br />
                      <div class="form-campo"><label for="password">Contrase&ntilde;a</label></div>
                      <input style="margin-bottom:5px;" size="30" name="password" id="password" type="password" value="" /><br />
                      <div class="form-campo"><label for="confirmpass">Confirmar Contrase&ntilde;a</label></div>
                      <input style="margin-bottom:5px;" size="30" name="confirmpass" id="confirmpass" type="password" value="" /><br />
                      <div class="unfloat"></div>
                      <input type="hidden" size="30" class="smaller" name="activationcode" id="activationcode" value="" />
                      <div id="cta"><input type="image" name="submitForm" src="<?=UrlResolver::getImgBaseUrl('registro/visa/images/boton.jpg');?>" value="Aceptar" /></div>
                      <div id="member"><span>Ya soy miembro.<br /><a href="http://www.geelbe.com/co">Ingresar</a></span></div>
	                  </div>
                  </form>
                  <div class="unfloat"></div>
                </div>
        <div class="beneficios"><img src="<?=UrlResolver::getImgBaseUrl('registro/visa/images/1_marcas_v_op2.jpg');?>" /></div>
		<div class="beneficios"><img src="<?=UrlResolver::getImgBaseUrl('registro/visa/images/benefits2.jpg');?>" /></div>
		<div class="unfloat"/></div>
</div>
	</div>
	
	<iframe id="iRegistro" name="iRegistro" style="display:none"></iframe>

<!-- Geelbe Form -->
<script type="text/javascript">
$(document).ready(function() {
    var codAct = unescape($.getUrlVar('codact'))
    var email = unescape($.getUrlVar('email'))
    if (email == 'undefined') email = '';
    if (codAct == 'undefined') codAct = '';
    $("#activationcode").val(codAct);
    $("#email").val(email);
});
</script>
<script language="javascript">
var Url = location.href;
Url = Url.replace(/\?.*(.*?)/, "");
Url = Url.replace(/.*\/co\/(.*?)/, "");
$("#landingtype").val(Url);
</script>
<!-- Geelbe Form -->
<!-- Geelbe Server Validation Form -->
<script type="text/javascript">
$(document).ready(function() {
    var errors = unescape($.getUrlVar('errors'))
    var errorValue = unescape($.getUrlVar('errorValue'))
    if (errors == 'undefined') errors = '';
    if (errorValue == 'undefined') errorValue = '';
    $("#registerForm_errorloc").html(errorValue);
});
</script>
<!-- Geelbe Server Validation Form -->
<!-- Geelbe Validation Form -->
<script type="text/javascript">
function ConfirmPasswordValidation() {
  var frm = document.forms["registerForm"];
  if(frm.password.value != frm.confirmpass.value) {
    sfm_show_error_msg('Sus contrase&ntilde;as son diferentes.',frm.password);
    return false;
  }
  else {
    return true;
  }
}

function BirthDayValidation() {
  var frm = document.forms["registerForm"];
  var currentDate = new Date() ;
  
  var birthDatePlus = new Date(parseInt(frm.birthdayyear.value)+18, parseInt(frm.birthdaymonth.value)-1, frm.birthdayday.value);

  var currentString = ''+currentDate.getFullYear()+currentDate.getMonth()+currentDate.getDate();
  var birthString = ''+birthDatePlus.getFullYear()+birthDatePlus.getMonth()+birthDatePlus.getDate();
  
  if (currentDate<birthDatePlus) {
    sfm_show_error_msg('Debe ser mayor de 18 a&ntilde;os.',frm.birthdayyear);
    return false;
  }
  else {
    return true;
  }
}

var formValidator  = new Validator("registerForm");
formValidator.EnableOnPageErrorDisplaySingleBox();
formValidator.EnableMsgsTogether();
formValidator.EnableFocusOnError(false);

formValidator.setAddnlValidationFunction("ConfirmPasswordValidation"); 
formValidator.setAddnlValidationFunction("BirthDayValidation"); 

formValidator.addValidation("firstname","req","Por favor, ingrese su nombre.");

formValidator.addValidation("lastname","req","Por favor, ingrese su apellido.");

formValidator.addValidation("gender","selone_radio","Por favor, indique su sexo.");

formValidator.addValidation("birthdayday","gt=0","Por favor, ingrese su d&iacute;a de nacimiento.");

formValidator.addValidation("birthdaymonth","gt=0","Por favor, ingrese su mes de nacimiento.");

formValidator.addValidation("birthdayyear","gt=0","Por favor, ingrese su a&ntilde;o de nacimiento.");

formValidator.addValidation("email","req","Por favor, ingrese su email.");
formValidator.addValidation("email","maxlen=255", "Su email supera el maximo permitido de 255 caracteres.");
formValidator.addValidation("email","email", "Su email no posee una direccion valida.");

formValidator.addValidation("password","req","Por favor, ingrese su contrase&ntilde;a.");
formValidator.addValidation("password","maxlen=30","Su contrase&ntilde;a supera el maximo permitido de 30 caracteres.");
formValidator.addValidation("password","minlen=6","Su contrase&ntilde;a debera ser de al menos 6 caracteres.");

formValidator.addValidation("confirmpassok","gt=1","Sus contrase&ntilde;as son diferentes.");

formValidator.addValidation("tos","shouldselchk=on","Debe aceptar los terminos y condiciones de Geelbe.");
</script>
<!-- Geelbe Validation Form -->

<!-- Google Analytics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script src="http://js.static.geelbe.com/co/registro/js/ga.js" type="text/javascript"></script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-6895743-6");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<!-- Google Analytics -->
    
</body>
</html>
