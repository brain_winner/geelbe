<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
       
    }
    catch(exception $e)
    {
        die(print_r($e));
    }

    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Geelbe Alianza Davivienda</title>

	<link href="<?=UrlResolver::getBaseUrl("registro/visa/css/landing-visa.css", true);?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getBaseUrl("logica/scripts/stylecortina.css", true);?>" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">
	    var DIRECTORIO_URL_JS = "http://js.static.geelbe.com/co/";
	</script>
	
	<script src="<?=UrlResolver::getBaseUrl("logica/scripts/cortina.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("logica/scripts/AC_RunActiveContent.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("logica/scripts/global_msj.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("logica/scripts/funcionesgenericas.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("js/jquery-1.4.2.min.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("js/jquery.simplemodal.js", true);?>" type="text/javascript"></script>
    <script src="<?=UrlResolver::getBaseUrl("js/jquery.geturlvars.js", true);?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getBaseUrl("registro/js/js.js", true);?>" type="text/javascript"></script>

</head>

<body>
	<div id="contenedor">
		<div id="header"><img src="<?=UrlResolver::getImgBaseUrl('registro/alianzadavivienda/header.jpg');?>" alt="Geelbe" /></div>
	  <div id="contenido">
        		<div>
                    <h1>¡Ahora puedes ser socio de Geelbe!</h1>
                    <p style="font-size:1.2em;">Solo por los días 12, 13 y 14 de febrero de 2014 puedes registrarte en Geelbe, tu Club Privado de Compras por Internet, donde puedes encontrar Marcas Premium con ¡descuentos hasta del 60%!</p>
   		  </div>
        	<div><img src="<?=UrlResolver::getImgBaseUrl('registro/visa/images/foto_mujerbolsasciudad_op.jpg');?>" /></div>
        	
        <!-- inicio formulario -->
            <div id="formulario">

                    <form method="post" name="registerForm" id="registerForm" action="/co/front/registro/register.php">
            	<div class="col_izquierda">
                      <input type="hidden" class="smaller" name="landingtype" id="landingtype" value="" />
                      <div id="registerForm_errorloc" class="error_strings"></div>
                      <div class="form-campo"><label for="firstname">Nombre</label></div>
                      <input style="margin-bottom:5px;" size="30" name="firstname" id="firstname" type="text" value="" /><br />
                      <div class="form-campo"><label for="lastname">Apellido</label></div>
                      <input style="margin-bottom:5px;" size="30" name="lastname" id="lastname" type="text" value="" /><br />
                      <div class="form-campo"><label for="gender">Sexo</label></div>
                      <input class="radio" id="gender" value="1" name="gender" type="radio">
                      <label class="moving">Hombre</label>
                      <input class="radio" id="gender" value="2" name="gender" type="radio">
                      <label class="moving">Mujer</label><br />
                      <input name="tos" id="tos" type="checkbox" checked="checked" />
                      <label for="tos">Acepto los <a href="/co/front/terminos/" target="_blank">T&eacute;rminos y Condiciones</a></label><br /><br />
                      Eres libre de ingresar la información aquí solicitada.
					</div>

					<div class="col_derecha">
                      <div class="form-campo"><label for="email">Email</label></div>
                      <input style="margin-bottom:5px;" size="30" name="email" id="email" type="text" value="" /><br />
                      <div class="unfloat"></div>
                      <input type="hidden" size="30" class="smaller" name="autopassword" value="1" />
                      <input type="hidden" size="30" class="smaller" name="activationcode" id="activationcode" value="" />
                      <div id="cta"><input type="image" name="submitForm" src="<?=UrlResolver::getImgBaseUrl('registro/visa/images/boton.jpg');?>" value="Aceptar" /></div>
                      <div id="member"><span>Ya soy miembro.<br /><a href="http://www.geelbe.com/co">Ingresar</a></span></div>
	                  </div>
                  </form>
                  <div class="unfloat"></div>
                </div>
        <div class="beneficios"><img width="290" src="<?=UrlResolver::getImgBaseUrl('registro/alianzadavivienda/logos2.jpg');?>" /></div>
		<div class="beneficios"><img src="<?=UrlResolver::getImgBaseUrl('registro/visa/images/benefits2.jpg');?>" /></div>
		<div class="unfloat"/></div>
</div>
	</div>
	
	<iframe id="iRegistro" name="iRegistro" style="display:none"></iframe>

<!-- Geelbe Form -->
<script type="text/javascript">
$(document).ready(function() {
    var codAct = unescape($.getUrlVar('codact'))
    var email = unescape($.getUrlVar('email'))
    if (email == 'undefined') email = '';
    if (codAct == 'undefined') codAct = '';
    $("#activationcode").val(codAct);
    $("#email").val(email);
});
</script>
<script language="javascript">
var Url = location.href;
Url = Url.replace(/\?.*(.*?)/, "");
Url = Url.replace(/.*\/co\/(.*?)/, "");
$("#landingtype").val(Url);
</script>
<!-- Geelbe Form -->
<!-- Geelbe Server Validation Form -->
<script type="text/javascript">
$(document).ready(function() {
    var errors = unescape($.getUrlVar('errors'))
    var errorValue = unescape($.getUrlVar('errorValue'))
    if (errors == 'undefined') errors = '';
    if (errorValue == 'undefined') errorValue = '';
    $("#registerForm_errorloc").html(errorValue);
});
</script>
<!-- Geelbe Server Validation Form -->
<!-- Geelbe Validation Form -->
<script type="text/javascript">
function ConfirmPasswordValidation() {
  var frm = document.forms["registerForm"];
  if(frm.password.value != frm.confirmpass.value) {
    sfm_show_error_msg('Sus contrase&ntilde;as son diferentes.',frm.password);
    return false;
  }
  else {
    return true;
  }
}

function BirthDayValidation() {
  var frm = document.forms["registerForm"];
  var currentDate = new Date() ;
  
  var birthDatePlus = new Date(parseInt(frm.birthdayyear.value)+18, parseInt(frm.birthdaymonth.value)-1, frm.birthdayday.value);

  var currentString = ''+currentDate.getFullYear()+currentDate.getMonth()+currentDate.getDate();
  var birthString = ''+birthDatePlus.getFullYear()+birthDatePlus.getMonth()+birthDatePlus.getDate();
  
  if (currentDate<birthDatePlus) {
    sfm_show_error_msg('Debe ser mayor de 18 a&ntilde;os.',frm.birthdayyear);
    return false;
  }
  else {
    return true;
  }
}

var formValidator  = new Validator("registerForm");
formValidator.EnableOnPageErrorDisplaySingleBox();
formValidator.EnableMsgsTogether();
formValidator.EnableFocusOnError(false);

formValidator.setAddnlValidationFunction("ConfirmPasswordValidation"); 
formValidator.setAddnlValidationFunction("BirthDayValidation"); 

formValidator.addValidation("firstname","req","Por favor, ingrese su nombre.");

formValidator.addValidation("lastname","req","Por favor, ingrese su apellido.");

formValidator.addValidation("gender","selone_radio","Por favor, indique su sexo.");

formValidator.addValidation("birthdayday","gt=0","Por favor, ingrese su d&iacute;a de nacimiento.");

formValidator.addValidation("birthdaymonth","gt=0","Por favor, ingrese su mes de nacimiento.");

formValidator.addValidation("birthdayyear","gt=0","Por favor, ingrese su a&ntilde;o de nacimiento.");

formValidator.addValidation("email","req","Por favor, ingrese su email.");
formValidator.addValidation("email","maxlen=255", "Su email supera el maximo permitido de 255 caracteres.");
formValidator.addValidation("email","email", "Su email no posee una direccion valida.");

formValidator.addValidation("password","req","Por favor, ingrese su contrase&ntilde;a.");
formValidator.addValidation("password","maxlen=30","Su contrase&ntilde;a supera el maximo permitido de 30 caracteres.");
formValidator.addValidation("password","minlen=6","Su contrase&ntilde;a debera ser de al menos 6 caracteres.");

formValidator.addValidation("confirmpassok","gt=1","Sus contrase&ntilde;as son diferentes.");

formValidator.addValidation("tos","shouldselchk=on","Debe aceptar los terminos y condiciones de Geelbe.");
</script>
<!-- Geelbe Validation Form -->

<!-- Google Analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39129349-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- Google Analytics -->
    
</body>
</html>
