onerror=handleErr;
function EsFechaValida(Dia, Mes, Anio)
{  
    // Valido el a�o   
    if (isNaN(Anio) || Anio.length<4 || parseFloat(Anio)<1900){   
        return false   
    }   
    // Valido el Mes   
    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){   
        return false   
    }   
    // Valido el Dia   
    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){   
        return false   
    }   
    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {   
        if (Mes==2 && Dia > 28 || Dia>30) {   
            return false   
        }   
    }   
    return true;       
}
function isArray(obj) 
{
    if (obj.constructor.toString().indexOf("function Array") == -1)
        return 0;
    else
        return 1;
}
function msjError(Error, title, pagina, showbutton)
{
    if (isArray(Error))
        oCortina.showError(Error, title, pagina, showbutton);    
    else
    {  
        vError = new Array();
        vError.push(Error);
        oCortina.showError(vError, title, pagina, showbutton);    
    }
}
function msjInfoJs(title, mensaje, textbutton, command, icon)
{
    if(isArray(mensaje))
        oCortina.showRunJs(title, mensaje, textbutton, command, icon);
    else
    {        
        vInfo = new Array();
        vInfo.push(mensaje);
        oCortina.showRunJs(title, vInfo, textbutton, command, icon);
    }
}
function msjInfoJs(title, mensaje, textbutton, command, icon)
{
    if(isArray(mensaje))
        oCortina.showRunJs(title, mensaje, textbutton, command, icon);
    else
    {        
        vInfo = new Array();
        vInfo.push(mensaje);
        oCortina.showRunJs(title, vInfo, textbutton, command, icon);
    }
}
function msjInfo(Info, title, pagina, showbutton)
{
    if(isArray(Info))
        oCortina.showInfo(Info, title, pagina, showbutton);
    else
    {        
        vInfo = new Array();
        vInfo.push(Info);
        oCortina.showInfo(vInfo, title, pagina, showbutton);
    }
}
function msjCargar(msj)
{
    oCortina.showCargar(msj);
}
function noCortina()
{
    oCortina.showOff();
}
function handleErr(msg,url,l)
{
	var txt="";
	txt="Ocurrio un error de JavaScript en esta pagina.\n\n";
	txt+="Error: " + msg + "\n";
	txt+="URL: " + url + "\n";
	txt+="Line: " + l + "\n\n";
	txt+="Click OK para continuar.\n\n";
	console.error(txt);
	return true;
}
function Redirect(sURL, sName)
{
    try
    {
        window.open(sURL, sName);
    }
    catch(e)
    {
        throw e;
    }
}
function ValidarEmail(email)
{
    try
    {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
            return true;
        else
            return false;
    }
    catch(e)
    {
        throw e;
    }
}
function ValidarFormCabecera(frm)
{
    try
    {                          
        var arrError = new Array();   
      
        
        if (!ValidarEmail(frm['frmEmailC'].value))
        {
            arrError.push(arrErrores["INVITACIONMANUAL"]["MAIL_INCORRECTO"]);
        }
        if (arrError.length>0)
        {
        
            msjError(arrError, arrErrores["INVITACIONMANUAL"]["TITLE_ERROR"]);
            return false;
        } 
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}
function Moneda(numero,decimales, stringFormato) 
{ 
    decimales = (!decimales ? 2 : decimales); 
    numero = Math.round(numero*Math.pow(10,decimales))/Math.pow(10,decimales);
    return stringFormato.replace("#importe#", parseFloat(numero).toFixed(2));
}
var oCortina = new Cortina();

function MM_swapImgRestore()
{ //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function solosIntegersEXPREG(jsValor)
{
    try
    {
        if(jsValor != "")
        {
            var jsRegExp = /^\d+$/;
            return jsRegExp.test(jsValor);
        }
        else
        {
            return false;
        }
    }
    catch(e)
    {
        throw e;
    }
}
function solosIntegersKEYCODE(evt)
{
    try
    {
        var nav4 = window.Event ? true : false;
        var key = nav4 ? evt.which : evt.keyCode; 
        return (key >= 48 && key <= 57);
    }
    catch(e)
    {
        throw e;
    }
}
function esInteger(valor)
{
    try
    {
        return (parseInt(valor,10) == valor);
    }
    catch(e)
    {
        throw e;
    }
}
function esFloat(valor)
{
    try
    {
        return (parseFloat(valor) == valor);
    }
    catch(e)
    {
        throw e;
    }
}
function solosFloatsKEYCODE(evt)
{
    try
    {
        var nav4 = window.Event ? true : false;
        var key = nav4 ? evt.which : evt.keyCode; 
        return (key <= 13 || (key >= 48 && key <= 57) || key == 46);
    }
    catch(e)
    {
        throw e;
    }
}
function popUp(pagina,nombre,w,h,scroll)
{
    LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
    TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
    settings = 'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+', scrollbars='+scroll+',resizable="no"';
    window.open(pagina,nombre,settings);
}
function ValidarEnter(e)
{
    try
    {
        tecla = (document.all)?e.keyCode:e.which;
        if(tecla == 13)
            return true;
        else
            return false;
    }
    catch(e)
    {
        throw e;
    }
}