<?
ob_start();
try
	{
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("newsletters_codigos"));

		if(isset($_GET['cod'], $_GET['campo']) && is_numeric($_GET['cod']) && is_numeric($_GET['campo']))
			dmNewsletterCodigos::visitar($_GET['cod'], $_GET['campo']);
	}
catch(exception $e)
    {
        die(print_r($e));
    }
    Includes::Scripts();

    class loginAutomatico{

    	protected $hash;
    	protected $id;
    	protected $where;

    	public function __construct(){
	    	$this->hash = $_GET["h"];
		    $this->id = $_GET["i"];
		    $this->where = $_GET["w"];
    	}

    	public function ejecutar(){
    		if($this->ejecutarLogin()){
    			$this->quieroVer();
    		}
    	}


    	protected function ejecutarLogin(){

    		/*if($this->hash == generar_hash_md5($this->id)){
				$login = new LoginAutomaticoController();				
				return $login->loguearById($this->id);
    		}
			*/
    		redirect(Aplicacion::getRootUrl()."front/login");
    		return false;
    	}

		protected function quieroVer(){
			echo("querio ver");
			if(strpos($this->where,"c")=== false && strpos($this->where,"p")=== false){

				switch($this->where){
			    	case 1: $this->where = "/".Aplicacion::getDirLocal()."front/vidriera/";
			    	break;

			    	case 2: $this->where = "/".Aplicacion::getDirLocal()."front/micuenta/";
			    	break;

			    	case 3: $this->where = "/".Aplicacion::getDirLocal()."front/referenciados/";
			    	break;

			    	case 4: $this->where = "/".Aplicacion::getDirLocal()."front/contacto-socio/";
			    	break;

			    	//Corresponde a las secciones de Acerca De
			    	case 5: $this->where = "/".Aplicacion::getDirLocal()."front/funcionamiento/";
			    	break;

			    	case 6: $this->where = "/".Aplicacion::getDirLocal()."front/acerca/";
			    	break;

			    	case 7: $this->where = "/".Aplicacion::getDirLocal()."front/quienes/";
			    	break;

			    	case 8: $this->where = "/".Aplicacion::getDirLocal()."front/compromisos/";
			    	break;

			    	case 9: $this->where = "/".Aplicacion::getDirLocal()."front/ventas/";
			    	break;

			    	case 10: $this->where = "/".Aplicacion::getDirLocal()."front/envio/";
			    	break;

			    	case 11: $this->where = "/".Aplicacion::getDirLocal()."front/atencion-miembros/";
			    	break;

			    	case 12: $this->where = "/".Aplicacion::getDirLocal()."front/invitar/";
			    	break;

			    	case 13: $this->where = "/".Aplicacion::getDirLocal()."front/envio/";
			    	break;
					//FIN Acerca De.

					//Corresponde a MiCuenta
			    	case 14: $this->where = "/".Aplicacion::getDirLocal()."front/micuenta/mi-credito.php";
			    	break;

			    	case 15: $this->where = "/".Aplicacion::getDirLocal()."front/micuenta/mi-perfil.php";
			    	break;

			    	case 16: $this->where = "/".Aplicacion::getDirLocal()."front/micuenta/mispedidos.php";
			    	break;

			    	case 17: $this->where = "/".Aplicacion::getDirLocal()."front/micuenta/mis-consultas.php";
			    	break;
			    	//FIN MiCuenta

			    	case 18: $this->where = "/".Aplicacion::getDirLocal()."front/terminos/";
			    	break;

			    	default: $this->where = "/".Aplicacion::getDirLocal()."front/vidriera/";
			    	break;
		    	}

			}elseif(strpos($this->where,"c")!== false){
				$this->where = intval(str_replace("c","",$this->where));
				$this->where = "/".Aplicacion::getDirLocal()."front/catalogo/index.php?IdCampania=".$this->where;
			}elseif(strpos($this->where,"p")!== false){				
				$this->where = intval(str_replace("p","",$this->where));
				$arrUrlProducto = dmUsuarios::getCampaniaCategoria($this->where);
				$this->where = "/".Aplicacion::getDirLocal()."front/catalogo/detalle.php?IdCampania=".$arrUrlProducto["IdCampania"]."&IdCategoria=".$arrUrlProducto["IdCategoria"]."&IdProducto=".$arrUrlProducto["IdProducto"];
			}
			redirect($this->where);
		}
	}

    $loginauto = new loginAutomatico();
    $loginauto->ejecutar();

ob_flush();
?>