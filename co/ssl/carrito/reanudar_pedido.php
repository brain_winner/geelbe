<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/dhl/datamappers/dmdhl.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1]);
		exit;
    }
    
    if(isset($_REQUEST["IdPedido"]) && is_numeric($_REQUEST["IdPedido"])) {
    	
		$oConexion = new Conexion();
		$oConexion->Abrir_Trans();
		$oConexion->setQuery("SELECT IdCampania FROM pedidos_temp WHERE IdPedido = ".$_REQUEST["IdPedido"]);
		$pedido = $oConexion->DevolverQuery();
		$oConexion->Cerrar_Trans();
		
		if(count($pedido) == 0)
			die;
		
		$oConexion = new Conexion();
		$oConexion->Abrir_Trans();
		$oConexion->setQuery("SELECT IdCodigoProdInterno, IdCodigoProducto, Cantidad FROM productos_x_pedidos_temp WHERE IdPedido = ".$_REQUEST["IdPedido"]);
		$prods = $oConexion->DevolverQuery();
		$oConexion->Cerrar_Trans();

		if(count($prods) == 0)
			die;

		Aplicacion::IniciarCarrito($pedido[0]['IdCampania']);
      $objCarrito = Aplicacion::getCarrito();
      
      //Vaciar
      $original = $objCarrito->getArticulos();
      foreach($original as $i => $p)
      	$objCarrito->removeArticulo($i); 
      
      foreach($prods as $i => $p) {
      	$prod = dmProductos::getById($p['IdCodigoProdInterno']);
      	$stock = $prod->getProductoProveedor(0)->getStock();
      	$objCarrito->addArticulo($p['IdCodigoProdInterno'], $p['IdCodigoProducto'], $p['Cantidad'], 0, $stock);
      }
      	
      Aplicacion::setCarrito($objCarrito, $IdCampania);
      
      //Guardar ID para eliminar
      $_SESSION['reanudarID'] = $_REQUEST['IdPedido'];
      
      header("Location: step_1.php");
      die;
    	
    } 
    
    die;
    
	
?>