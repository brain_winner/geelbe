<?
	ob_start();
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/ssl/carrito/logic/CartUtils.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("miscreditos"));

        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/dhl/datamappers/dmdhl.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");
        
    } catch(Exception $e) {
    	ob_clean();
		header('HTTP/1.1 500 Internal Server Error');
		exit;

    }
    try {
        ValidarUsuarioLogueado();
	} catch(Exception $e) {
		ob_clean();
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }

    if(!isset($_GET["Discount"]) || (isset($_GET["Discount"]) && !is_numeric($_GET["Discount"]))) {
		ob_clean();
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }
    
    try {
        $disponibles = dmMisCreditos::getCreditoDisponible(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        $_SESSION["CREDITO_APLICADO_CARRITO"] = ($_GET["Discount"]>$disponibles)?$disponibles:$_GET["Discount"];
    	
        $MiCarrito = Aplicacion::getCarrito(); 
        $objCampania = dmCampania::getByIdCampania($MiCarrito->getIdCampania());
        $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        $objDireccion = $objUsuario->getDirecciones(0);
        include "../includes/cart_info.php";
    } catch(Exception $e) {
    	ob_clean();
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }
    ob_flush();
?>