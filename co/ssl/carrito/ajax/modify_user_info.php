<?

try {
    $confRoot = explode("/", dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/conf/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica/url_resolver/UrlResolver.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/ssl/carrito/logic/CartUtils.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica_bo/validators/ValidatorUtils.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica_bo/clientes/datamappers/dmclientes.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica/micuenta/clases/clsmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica/micuenta/datamappers/dmmicuenta.php");
} catch (Exception $e) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}
try {
    ValidarUsuarioLogueado();
} catch (Exception $e) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

if ($_POST) {
    extract($_POST);
    try {
        $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));

        /* Get Customer Info Using the DocId of the Actual user
         * BW MVC /co/bw/
         */
        $userId = Aplicacion::Decrypter($_SESSION["User"]["id"]);
        $numDoc = $frmDaU["txtNroDoc"];
        // $customerController = new CustomerController();
        $custPersonalInfo = dmMicuenta::validateDoc($numDoc);//        echo '<pre>';
//        print_r($custPersonalInfo);
//        echo '</pre>';


        if (!$custPersonalInfo['IdUsuario'] || $custPersonalInfo['IdUsuario'] == $userId) { /* check if NroDoc is using for other Customer start */
            $objDatos = $objUsuario->getDatos(0);
            $objDatos->setNroDoc($frmDaU["txtNroDoc"]);
            $objDatos->setceluempresa($frmDaU["cmb_CelularComp"]);
            $objDatos->setcelucodigo($frmDaU["txt_CelularCodigo"]);
            $objDatos->setcelunumero($frmDaU["txt_CelularNumero"]);
            $objDatos->setTipoDoc(0);
            $objUsuario->setDatos($objDatos);

            $objDireccion = $objUsuario->getDirecciones(0);
            $objDireccion->setCP($frmDi1["txtCodigoPostal"]);
            $objDireccion->setDomicilio(htmlentities(addslashes(utf8_decode($frmDir["txtDomicilio"]))));
            $objDireccion->setEscalera($frmDi1["txtEscalera"]);
            $objDireccion->setIdPais($frmDi1["cbxPais"]);
            $objDireccion->setIdProvincia($frmDi1["cbxProvincia"]);
            $objDireccion->setIdCiudad($frmDi1["cbxCiudad"]);
            $objDireccion->setTipoDireccion(0);
            $objDireccion->setIdUsuario($objUsuario->getIdUsuario());
            $objDireccion->setNumero($frmDi1["txtNumero"]);
            $objDireccion->setPoblacion(htmlentities(addslashes(utf8_decode($frmDi1["txtPoblacion"]))));
            $objDireccion->setPiso(htmlentities(utf8_decode($frmDi1["txtPiso"])));
            $objDireccion->setPuerta(htmlentities(utf8_decode($frmDi1["txtPuerta"])));
            $objDireccion->setTelefono($objDatos->getTelefono());
            $objDireccion->setTipoCalle(htmlentities($frmDi1["cbxTipoCalle"]));
            $objDireccion->esCompleto();
            $objUsuario->setDirecciones(0, $objDireccion);

            dmMicuenta::Actualizar($objUsuario);

            echo 0;
        } else {
            echo 1;
        }
    } catch (Exception $e) {
        echo $e;
        header('HTTP/1.1 500 Internal Server Error');
        exit;
    }
}
?>