<?
	ob_start();
	$data = array('result' => true);
	
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/ssl/carrito/logic/CartUtils.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/clientes/datamappers/dmclientes.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
    } catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }

    try {
        ValidarUsuarioLogueado();
	} catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }
    
    try {
    	$objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        $objDatos = $objUsuario->getDatos(0);
        $objDireccion = $objUsuario->getDirecciones(0);

		if($objDatos->getNroDoc() == '')
			$data['result'] = false;
		/*else if($objDatos->getceluempresa() == '')
			$data['result'] = false;
		else if($objDatos->getcelucodigo() == '')
			$data['result'] = false;
		*/else if($objDatos->getcelunumero() == '')
			$data['result'] = false;
		else if($objDireccion->getDomicilio() == '')
			$data['result'] = false;
		else if($objDireccion->getIdProvincia() == '')
			$data['result'] = false;
		else if($objDireccion->getIdCiudad() == '')
			$data['result'] = false;
		        
    } catch(Exception $e) {
    	echo $e;
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }
  	
  	ob_end_clean();
  	echo json_encode($data);
  	  
	die;
?>