<?
    ob_start();
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/clientes/datamappers/dmclientes.php");
   		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
   		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/ssl/carrito/logic/CartUtils.php");
   		
        
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("mispedidos")); 
        
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/datamappers/dmdescuentos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/dhl/datamappers/dmdhl.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");

        $postURL = Aplicacion::getIncludes("post","micarrito_fincompra");
        $datosUser = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        
    } catch(Exception $e) {
    	ob_clean();
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }
    try {
        ValidarUsuarioLogueado();
	} catch(Exception $e) {
		ob_clean();
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }
    
    try {
        if(isset($_GET['IdPedido']) && is_numeric($_GET['IdPedido'])) {
	        $MiCarrito = dmPedidos::getPedidoById($_GET['IdPedido']);
			if($MiCarrito->getIDEstadoPedidos() == 0)
				$IdPedido = $_GET['IdPedido'];
		} else {
		    try {
		    	$MiCarrito = Aplicacion::getCarrito();
		    } catch(ACCESOException $e) {
		    	die;
		    } 
		}
		
        $objCampania = dmCampania::getByIdCampania($MiCarrito->getIdCampania());
        $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        
		$PedidoReanudado = false;
	    if(isset($IdPedido)) {
			$PedidoReanudado = true;
	        $Articulos = $MiCarrito->getProductos();
	    } else {
	   	    $Articulos = $MiCarrito->getArticulos();
	   	    
	    }
        
        $objDireccion = $objUsuario->getDirecciones(0);

		if(isset($_GET['IdPedido'])) {
		        
	        if(isset($_SESSION['lastPedidoId']) && is_numeric($_SESSION['lastPedidoId'])) {
		  	    $Articulos = $MiCarrito->getProductos();
		  	} else {
		  	   	$Articulos = $MiCarrito->getArticulos();
		  	}
		  	
		} else {
			$Articulos = $MiCarrito->getArticulos();
		}
	    $IdProvincia = $objUsuario->getDirecciones(0)->getIdProvincia();
	    $CantidadProductos = count($Articulos);
	    $SubTotal = 0;
	    $SubTotalPrecioPublico = 0;
	    $Ganancia = 0;
	    $Peso = 0;
	    $P = 0;
	    $CostoCompra = 0;
	    $PrecioVenta = 0;
	    $CreditosUtilizados = $_SESSION["CREDITO_APLICADO_CARRITO"];
	    $FreeShippingProduct = false;
	    foreach($Articulos as $IdCodigoProdInterno => $Articulo) {
    		
    		if(is_object($Articulo)) {
    			$q = $Articulo->getCantidad();
    			$IdCodigoProdInterno = $Articulo->getIdCodigoProdInterno();
    		} else
    			$q = $Articulo['Cantidad'];
   		
	    	$objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
            $objProducto = dmProductos::getById($objPP->getIdProducto());

            $SubTotal = $SubTotal + ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $q;
            
            $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
            $Peso += $P * $q;
        }
        
        $Descuento = $MiCarrito->getDescuento($SubTotal);
		if($Descuento > 0)
			$SubTotal -= $Descuento;
	
        $montoEnvioGratis = dmCampania::getMinMontoEnvioGratis($MiCarrito->getIdCampania());
         if ($montoEnvioGratis != 0 && $SubTotal >= $montoEnvioGratis) {
	         $Envio = 0;
         } else {
               $objDHL = dmDHL::getTarifaByPesoPcia($Peso, $objDireccion->getIdCiudad(), $objCampania);
	             $Envio = $objDHL->getImporte();
	         	 // Si el valor del envio es mayor a cero, le agrego el valor del seguro
				 if ($Envio > 0) {
				 	$Envio = $Envio + 300;
				 }
         	
            $dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($objCampania->getIdCampania());
            foreach($dtCampaniasPropiedades as $propiedad) {
                if($propiedad["IdPropiedadCampamia"]=='6') {
                  $Envio = 0;
                }
            }
         }
         
         //Descuento con envio gratis
         if(isset($_GET['IdPedido'])) {
	         if($MiCarrito->getIdDescuento() > 0) {
	         	$disc = dmDescuentos::getById($MiCarrito->getIdDescuento());
	         	if($disc->getEnvioGratis() == 1)
	         		$Envio = 0;
	         }
	     } else {
	         if($MiCarrito->getDescuentoId() > 0) {
	         	$disc = dmDescuentos::getById($MiCarrito->getDescuentoId());
	         	if($disc->getEnvioGratis() == 1)
	         		$Envio = 0;
	         }
	     }
         
         $Seguro = 0;
         
         $res = array(
         	'envio' => Moneda($Envio),
         	'total' => Moneda($SubTotal - $CreditosUtilizados + $Envio + $Seguro),
         	'fenvio' => ($Envio),
         	'ftotal' => ($SubTotal - $CreditosUtilizados + $Envio + $Seguro)
         );
         
         ob_end_clean();
         echo json_encode($res);
         die;
        
    } catch(Exception $e) {
    	ob_clean();
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }
    ob_flush();
?>