<?
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("mispedidos")); 

        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/dhl/datamappers/dmdhl.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/ssl/carrito/logic/CartUtils.php");
        
        $postURL = Aplicacion::getIncludes("post","micarrito_fincompra");
        $datosUser = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));
    }
    catch(exception $e) {
		header('Location: /'.$confRoot[1]);
		exit;
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1]);
		exit;
    }
    
    if(isset($_GET['IdPedido']) && is_numeric($_GET['IdPedido'])) {
        $MiCarrito = dmPedidos::getPedidoById($_GET['IdPedido']);
		if($MiCarrito->getIDEstadoPedidos() == 0)
			$IdPedido = $_GET['IdPedido'];
			$_SESSION['lastPedidoId'] = $IdPedido;
	} else {
	    try {
	    	$MiCarrito = Aplicacion::getCarrito();
	    } catch(ACCESOException $e) {
	    	header('Location: /'.$confRoot[1].'/front/vidriera/');
	       	exit;
	    } 
	}

	$PedidoReanudado = false;
    if(isset($IdPedido)) {
		$PedidoReanudado = true;
        $Articulos = $MiCarrito->getProductos();
       	    
        if(count($Articulos)==0) {
	       header('Location: /'.$confRoot[1].'/front/vidriera/');
	       exit;
	    }
	    
    	// Verifico stock ..
		$verificoCorrectamenteStock = CartUtils::verifyStockReanudarPago($IdPedido);
		if(!$verificoCorrectamenteStock) {
	       	$_SESSION["ProductosSinStock"] = $productosSinStock;
	       	$Pedido = dmPedidos::getPedidoById($IdPedido);
	        header('Location: /'.$confRoot[1].'/ssl/carrito/no_stock_reanudar_pago.php?IdCampania='.$Pedido->getIdCampania());
			exit;
		}
    } else {
   	    $Articulos = $MiCarrito->getArticulos();
   	    
        if(count($Articulos)==0 || $MiCarrito->Expiro()) {
	       header('Location: /'.$confRoot[1].'/front/vidriera/');
	       exit;
	    }
	    
	        // Verifico stock ..
		$productosSinStock = CartUtils::verifyStock();
		if(is_array($productosSinStock) && count($productosSinStock) > 0) {
	       	$_SESSION["ProductosSinStock"] = $productosSinStock;
	        header('Location: /'.$confRoot[1].'/ssl/carrito/no_stock.php');
			exit;
		}
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?=Aplicacion::getParametros("info", "nombre");?></title>

	<link href="<?="/".$confRoot[1]."/front/geelbe.css"?>" rel="stylesheet" type="text/css" />
	<link href="<?="/".$confRoot[1]."/front/confirmacion/confirmacion.css"?>" rel="stylesheet" type="text/css" />
	<link href="<?="/".$confRoot[1]."/css/forms_g.css"?>" rel="stylesheet" type="text/css" />
	<link href="<?="/".$confRoot[1]."/front/botones.css"?>" rel="stylesheet" type="text/css" />
	<link href="<?="/".$confRoot[1]."/ssl/carrito/css/cart.css"?>" rel="stylesheet" type="text/css" />
	<link href="<?="/".$confRoot[1]."/ssl/carrito/css/style.css"?>" rel="stylesheet" type="text/css" />
	
	<script src="<?="/".$confRoot[1]."/front/confirmacion/js/js.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/js/jquery-1.4.2.min.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/ssl/carrito/js/script.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/front/micarrito/js/js.js"?>" type="text/javascript"></script>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background_carrito.php";?>
</head>
<body>
<? require("../../front/menuess/menu_carrito.php"); ?>

<?	    
        $objCampania = dmCampania::getByIdCampania($MiCarrito->getIdCampania());
        $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        $objDireccion = $objUsuario->getDirecciones(0);

		$CantidadProductos = count($Articulos);
	    $SubTotal = 0;
	    $SubTotalPrecioPublico = 0;
	    $Ganancia = 0;
	    $Peso = 0;
	    $P = 0;
	    $CostoCompra = 0;
	    $PrecioVenta = 0;
	    $CreditosUtilizados = $_SESSION["CREDITO_APLICADO_CARRITO"];

	    foreach($Articulos as $IdCodigoProdInterno => $Articulo) {
	    	if(isset($IdPedido)) {
	    		$IdCodigoProdInterno = $Articulo->getIdCodigoProdInterno();
	    		
		    	$objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
	            $objProducto = dmProductos::getById($objPP->getIdProducto());
	            $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($IdCodigoProdInterno);
	
	            $SubTotal = $SubTotal + ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo->getCantidad();
	            $SubTotalPrecioPublico = $SubTotalPrecioPublico + ObtenerPrecioProductos($objProducto->getPVP(), $objPP->getIncremento()) * $Articulo->getCantidad();
	            $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
	            $Peso += $P * $Articulo->getCantidad();
	            $Ganancia += abs(($objProducto->getPVenta() - $objProducto->getPCompra())*$Articulo->getCantidad());
	            $CostosCompra += $objProducto->getPCompra();
	            $PreciosVenta += $objProducto->getPVenta();
	    	} else {
		    	$objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
	            $objProducto = dmProductos::getById($Articulo["IdProducto"]);
	            $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($IdCodigoProdInterno);
	
	            $SubTotal = $SubTotal + ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo["Cantidad"];
	            $SubTotalPrecioPublico = $SubTotalPrecioPublico + ObtenerPrecioProductos($objProducto->getPVP(), $objPP->getIncremento()) * $Articulo["Cantidad"];
	            $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
	            $Peso += $P * $Articulo["Cantidad"];
	            $Ganancia += abs(($objProducto->getPVenta() - $objProducto->getPCompra())*$Articulo["Cantidad"]);
	            $CostosCompra += $objProducto->getPCompra();
	            $PreciosVenta += $objProducto->getPVenta();
	    	}
	    	
	    }
	    
	    $montoEnvioGratis = dmCampania::getMinMontoEnvioGratis($MiCarrito->getIdCampania());
        if ($montoEnvioGratis != 0 && $SubTotal >= $montoEnvioGratis) {
	         $Envio = 0;
        } else {
             $objDHL = dmDHL::getTarifaByPesoPcia($Peso, $objDireccion->getIdCiudad(), $objCampania);
             $Envio = $objDHL->getImporte();
        	 // Si el valor del envio es mayor a cero, le agrego el valor del seguro
			 if ($Envio > 0) {
				$Envio = $Envio + 300;
			 }
             
			 $dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($objCampania->getIdCampania());
             foreach($dtCampaniasPropiedades as $propiedad) {
                if($propiedad["IdPropiedadCampamia"]=='6') {
                  $Envio = 0;
                }
             }
        }
?>
            
	<div id="container">
		<div id="contenido">
        	<div>
	            <div id="proc-reg">Selecciona Medio de Pago</div>
	            <div class="timeline-off"><p>Resumen<br />de tu<br />compra</p></div>
	            <div class="timeline-on" style="background-position:-85px 0;"><p>Selecciona medio<br />de pago</p></div>
	            <div class="timeline-off" style="background-position:-170px 0;"><p>Efectuar pago</p></div>
        	</div>
        	
        	<div class="contres">
	        	<div class="rescompra">
	        		<p><span class="titulo">Resumen de tu compra<br />
	        		</span></p>
                    <table id="resumen" cellpadding="0" cellspacing="0" >
                    	<tr>
                        	<td>Subtotal (<?=$CantidadProductos." ".(($CantidadProductos==1)?"producto":"productos");?>):</td>
                            <td><span class="price"><?=Moneda($SubTotal);?></span></td>
                        </tr>
                        <tr>
                        	<td>Cr&eacute;ditos utilizados de tu cuenta:</td>
                            <td><span class="price">-<?=Moneda($CreditosUtilizados);?></span></td>
                        </tr>
                        <tr>
                        	<td>Costo de Env&iacute;o:</td>
                            <td><span class="price"><?=Moneda($Envio);?></span></td>
                        </tr>
                        <? $TotalAPagar = 0 ?>
						<? if ($CreditosUtilizados > 0) { ?>
	                        <tr id="subtotal">
	                        	<td><span class="title">TOTAL DE LA COMPRA:</span></td>
	                            <td><span class="price"><?=Moneda(($SubTotal/1.16 - $CreditosUtilizados)*1.16 + $Envio);?></span></td>
	                        </tr>
                        <? $TotalAPagar = ($SubTotal/1.16 - $CreditosUtilizados)*1.16 + $Envio; ?>
						<? } else { ?>
	                        <tr id="subtotal">
	                        	<td><span class="title">TOTAL DE LA COMPRA:</span></td>
	                            <td><span class="price"><?=Moneda($SubTotal - $CreditosUtilizados + $Envio);?></span></td>
	                        </tr>
                        <? $TotalAPagar = $SubTotal - $CreditosUtilizados + $Envio; ?>
						<? }?>
                    </table>
	        	</div>
        	</div>
        	<div class="contslide">
	        	<div class="slides">
					<div>
						<div id="slider">
							<ul>
								<li><img src="img/banner_devolucion.gif" width="300" height="100" alt="Garantia de Devolucion" /></li>
								<li><img src="img/banner_entrega-domicilio.gif" width="300" height="100" alt="Entrega a Domicilio" /></li>
								<li><img src="img/banner_pago-seguro.gif" width="300" height="100" alt="Pago Seguro" /></li>
							</ul>
						</div>
					</div>
					<ul id="pagination" class="pagination">
						<li onclick="slideshow.pos(0)">1</li>
						<li onclick="slideshow.pos(1)">2</li>
						<li onclick="slideshow.pos(2)">3</li>
					</ul>
	        	</div>
        	</div>
        	
        	<div class="unfloat">
	        	<form action="<?="/".$confRoot[1]."/logica/micarrito/actionform/micarrito_fincompra_new.php"?>" name="paymentMethodForm" id="paymentMethodForm" method="POST">
	
					<?php
						if(isset($IdPedido))
							echo '<input type="hidden" name="IdPedido" value="'.$IdPedido.'" />';
					?>
						
		        	<div class="mediosdpago">
			        	<div class="dirnenvio" style="overflow:auto;">
			        		<span class="titulo">Confirma los datos y procede a realizar el pago</span>
			        		<?php if($TotalAPagar > 30000) {?>
							<div style="DISPLAY: inline-block;" >
							<div>
								<div class="creditcardin">
									<input type="radio" id="ppagosonline" value="1" name="paymentMethod" onchange="javascript:enableStep3();" onclick="javascript:enableStep3();">
									<img align="top" src="img/pagosonline.png" id="clkppagosonline">
								</div>
							</div>
							</div>
							<?php } else { ?>
							<div style="DISPLAY: inline-block;" >
							<div>
								<div class="creditcardin">
									<input type="radio" id="ppagosonlinecredito" value="2" name="paymentMethod" onchange="javascript:enableStep3();" onclick="javascript:enableStep3();">
									<img align="top" src="img/pagosonline-tarjetas.jpg" id="clkppagosonlinecredito">
								</div>
								<div class="creditcardin">
									<input type="radio" id="ppagosonlinepse" value="3" name="paymentMethod" onchange="javascript:enableStep3();" onclick="javascript:enableStep3();">
									<img align="top" src="img/pagosonline-pse.jpg" id="clkppagosonlinepse">
								</div>
							</div>
							</div>
							<?php } ?>
			        	</div>
		        	</div>
	        	
	        	</form>
        	</div>
        	
        	<table>
				<tbody>
					<tr>
						<td>
						    <? if (!$PedidoReanudado) {?>
							<table cellspacing="0" cellpadding="0" border="1px" class="btn btnViolet izquierda chico M">
								<tbody>
									<tr>
										<td bordercolorlight="#a50000" bordercolordark="#a50000" bordercolor="#9595aa">
											<a href="<?="/".$confRoot[1]."/ssl/carrito/step_1.php"?>"><strong>Volver al paso 1</strong></a>
										</td>
									</tr>
								</tbody>
							</table>
						    <? } ?>
						</td>
						<td>
							<table id="goToStep3Button" cellspacing="0" cellpadding="0" border="1px" class="btn btnRed derecha normal XL disabled">
								<tbody>
									<tr>
										<td bordercolorlight="#a50000" bordercolordark="#a50000" bordercolor="#9595aa">
											<a id="goToStep3Link" href="javascript:goToStep3();"><strong>Ir a paso 3 de 3</strong>&nbsp;&nbsp;&nbsp;<img border="none" src="/<?=$confRoot[1]?>/front/images/bot_ico_flecha_vidrierasmall.jpg" style="vertical-align: bottom;"></a>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
        	
		</div>
		<div id="contenido-bot"></div>
  	</div>  
<? require("../../front/menuess/footer_carrito.php")?>


<script>

var step3Disabled = true;
var slideshow=new TINY.slider.slide('slideshow',{
	id:'slider',
	auto:3,
	resume:true,
	vertical:false,
	navid:'pagination',
	activeclass:'current',
	position:0
});

<? if($TotalAPagar > 30000) {?>

function enableStep3() {
	if ($("input[@name='paymentMethod']:checked").val() == '1') {
		step3Disabled = false;
		$('#goToStep3Button').removeClass("disabled");
	} else {
		step3Disabled = true;
		$('#goToStep3Button').addClass("disabled");
	}
}

$('#clkppagosonline').click(function() {
	 $('#ppagosonline').click();
});

$(document).ready(function(){
	$("#ppagosonline").click();
});

<? } else {?>

function enableStep3() {
	if ($("input[@name='paymentMethod']:checked").val() == '2' || $("input[@name='paymentMethod']:checked").val() == '3') {
		step3Disabled = false;
		$('#goToStep3Button').removeClass("disabled");
	} else {
		step3Disabled = true;
		$('#goToStep3Button').addClass("disabled");
	}
}

$('#clkppagosonlinecredito').click(function() {
	 $('#ppagosonlinecredito').click();
});

$('#clkppagosonlinepse').click(function() {
	 $('#ppagosonlinepse').click();
});

<? } ?>

$(document).ready(function(){
	$("#ppagosonlineinfoa").hover(function() {
			$("#ppagosonlineinfoem").stop(true, true).animate({opacity: "show"}, "slow");
		}, function() {
			$("#ppagosonlineinfoem").animate({opacity: "hide"}, "fast");
		});
});

function goToStep3() {
	if(!step3Disabled) {
		$("#paymentMethodForm").submit();
	}
}

</script>

  </body>
</html>
