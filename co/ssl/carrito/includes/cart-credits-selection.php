        <div style="clear:both"></div>
        
        <div id="carr_credito" class="carr_talle addcred">
			<a id="usecredit" href="javascript:;">Quiero utilizar cr&eacute;dito para esta compra</a>
		</div>
        <div id="credform" style="display:none;">
			
			<table cellspacing="0" cellpadding="0" border="1px" class="btn btnRed derecha chico M" style="float:right;"> 
				<tbody>
					<tr>
						<td>
							<a id="applyDiscounTrigger" href="javascript:;"><strong>Aplicar Descuento</strong></a>
						</td>
					</tr>
				</tbody>
			</table>
			<input type="text" tabindex="3" id="descu" name="descu" gtbfieldid="65" class="blur" style="width:60px;margin-top:3px;margin-right:5px;" />
			<label for="descu" class="descu" style="float:right;margin-top:10px;margin-right:5px;">Descuento: $</label>
			<?
			    $disponibles = dmMisCreditos::getCreditoDisponible(Aplicacion::Decrypter($_SESSION["User"]["id"]));
		        $permitido = intval(($PreciosVenta - $CostosCompra)/2);
			?>
            	<p><strong>* Nota:</strong> Para conocer mas sobre los cr&eacute;ditos en Geelbe,<br />consulta los <a target="_blank" style="color:#000; font-style:italic;" href="http://www.geelbe.com/co/front/terminos/index-socio.php">T&eacute;rminos y Condiciones</a> de Geelbe.</p>
			<script type="text/javascript">
			    $("#descu").spinner({max: <?= min($disponibles, $permitido) ?>, min: 0});
				$('#applyDiscounTrigger').click(function() {
					$.ajax({
					  url: '<?="/".$confRoot[1]."/ssl/carrito/ajax/apply_discounts.php?Discount="; ?>' + $('#descu').val(),
					  success: function(data) {
						window.location.href="<?="/".$confRoot[1]."/ssl/carrito/step_1.php?CART_RELOADING=true"?>";
					  }
					});
				});
			</script>
			 <div style="clear:both"></div>
			
        </div>
        
        <div style="clear:both"></div>