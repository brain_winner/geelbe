 <?
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_service.php");

        $userId = Aplicacion::Decrypter($_SESSION["User"]["id"]);
        $GeelbeCashSaldo = GeelbeCashService::getEstadoGeelbeCash($userId)->getMontoSaldoActual();
        
        $objCampania = dmCampania::getByIdCampania($MiCarrito->getIdCampania());
        $objDireccion = $objUsuario->getDirecciones(0);

        $IdProvincia = $objUsuario->getDirecciones(0)->getIdProvincia();
        
        $DatosShippingCargados = CartUtils::isShippingInfoFilled($objUsuario);
        
		$CantidadProductos = count($Articulos);
	    $SubTotal = 0;
	    $SubTotalPrecioPublico = 0;
	    $Ganancia = 0;
	    $Peso = 0;
	    $P = 0;
	    $CostoCompra = 0;
	    $PrecioVenta = 0;
	    $CreditosUtilizados = $_SESSION["CREDITO_APLICADO_CARRITO"];
	    foreach($Articulos as $IdCodigoProdInterno => $Articulo) {
	    	if(isset($IdPedido)) {
	    		$IdCodigoProdInterno = $Articulo->getIdCodigoProdInterno();
	    		
		    	$objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
	            $objProducto = dmProductos::getById($objPP->getIdProducto());
	            $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($IdCodigoProdInterno);
	
	            $SubTotal = $SubTotal + ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo->getCantidad();
	            $SubTotalPrecioPublico = $SubTotalPrecioPublico + ObtenerPrecioProductos($objProducto->getPVP(), $objPP->getIncremento()) * $Articulo->getCantidad();
	            $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
	            $Peso += $P * $Articulo->getCantidad();
	            $Ganancia += abs(($objProducto->getPVenta() - $objProducto->getPCompra())*$Articulo->getCantidad());
	            $CostosCompra += $objProducto->getPCompra();
	            $PreciosVenta += $objProducto->getPVenta();
	    	} else {
		    	$objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
	            $objProducto = dmProductos::getById($Articulo["IdProducto"]);
	            $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($IdCodigoProdInterno);
	
	            $SubTotal = $SubTotal + ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo["Cantidad"];
	            $SubTotalPrecioPublico = $SubTotalPrecioPublico + ObtenerPrecioProductos($objProducto->getPVP(), $objPP->getIncremento()) * $Articulo["Cantidad"];
	            $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
	            $Peso += $P * $Articulo["Cantidad"];
	            $Ganancia += abs(($objProducto->getPVenta() - $objProducto->getPCompra())*$Articulo["Cantidad"]);
	            $CostosCompra += $objProducto->getPCompra();
	            $PreciosVenta += $objProducto->getPVenta();
	    	}
	    }
	    
	    $dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($MiCarrito->getIdCampania());
	    $montoEnvioGratis = dmCampania::getMinMontoEnvioGratis($MiCarrito->getIdCampania());
	    
	    $campaniaEnvioGratis = false;
        foreach ($dtCampaniasPropiedades as $propiedad) {
	        if($propiedad["IdPropiedadCampamia"] == '6') {
            	$campaniaEnvioGratis = true;
            }
        }
        
        if (($montoEnvioGratis != 0 && $SubTotal >= $montoEnvioGratis) || $campaniaEnvioGratis) {
	         $Envio = 0;
        } else {
        	$DatosShippingCargadosDHL = CartUtils::isShippingInfoFilled($objUsuario);
         	$DatosShippingCargadosOCA = CartUtils::isShippingInfoFilled($objUsuario);
        	
         	if (isset($IdProvincia) && $IdProvincia != null) {
				if ($_GET['envio'] == 5 && isset($_GET['id_sucursal'])) {
	     			$Envio = dmOCA::getTarifaByPesoSucursal($Peso, $_GET['id_sucursal']);
					// Valor para el seguro
	     			if($SubTotal > 500)
	     				$Envio += $SubTotal/100*2;
	     		} elseif($_GET['envio'] == 6 && $DatosShippingCargadosOCA) {
	     			$Envio = dmOCA::getTarifaByPesoLocalidad($Peso, $objDireccion->getPoblacion());
	     		} elseif($DatosShippingCargadosDHL) {
	            	$objDHL = dmDHL::getTarifaByPesoPcia($Peso, $objDireccion->getIdProvincia(), $objCampania);
	             	$Envio = $objDHL->getImporte();
	            } else {
	     			$Envio = 0;
	     		}

        	} else {
        		$Envio = 0;
        	}
        	
        	$dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($objCampania->getIdCampania());
            foreach($dtCampaniasPropiedades as $propiedad) {
                if($propiedad["IdPropiedadCampamia"]=='6') {
                  $Envio = 0;
                }
            }
        }
        
        if(isset($IdPedido)) {
        	$oConexion = Conexion::nuevo();
			$oConexion->setQuery('SELECT GastosEnvio FROM pedidos WHERE IdPedido = '.mysql_real_escape_string($IdPedido));
	        $data = $oConexion->DevolverQuery();
	        $Envio = $data[0]["GastosEnvio"];
        }
        
        $Descuento = $MiCarrito->getDescuento($SubTotal);
		if($Descuento > 0)
			$SubTotal -= $Descuento;
        
        $TotalCompra = $SubTotal - $CreditosUtilizados + $Envio;
        if($TotalCompra <= $GeelbeCashSaldo ) {
        	$ActivarGeelbeCash = true;
        } else {
        	$ActivarGeelbeCash = false;
        }
?>

		<div>

	        	<form action="<?="/".$confRoot[1]."/logica/micarrito/actionform/micarrito_fincompra_new.php"?>" name="paymentMethodForm" id="paymentMethodForm" method="POST">
	
					<?php
						if(isset($IdPedido))
							echo '<input type="hidden" name="IdPedido" value="'.$IdPedido.'" />';
					?>
						
		        	<div class="mediosdpago">
			        	<div class="mediospagosbox" id="mediosdepago-box" >
	        		
	        		
	        		<?php
	        			// Infinit 18-02-12 campaign
	        			if ($objCampania->getIdCampania() == 1180 || $objCampania->getIdCampania() == 1192) { 
	        		?>
	        		<div style="DISPLAY: inline-block;">
	        			<div class="carr_pago_option">
							<input type="radio" id="pmercadopago" value="12" name="paymentMethod">
							<label for="pmercadopago"><img src="img/mp2.jpg" name="Imagen8" width="102" height="40" border="0" id="Imagen8" /></label>
						</div>
	        		</div>
	        		<script type="text/javascript">
	        			$(document).ready(function() {
							$("#pmercadopago").click();
			        	});
	        		</script>
	        		<?php
	        			} else { 
	        		?>
	        		
					<div style="DISPLAY: inline-block;">
						
						<?php if($_SESSION['padrino']): ?>
							<div class="carr_pago_option">
								<input type="radio" id="ppagosonline" value="4" name="paymentMethod" >
								<label for="ppagosonline" ><img id="paymentImg1" class="payment" align="top" src="img/PoL-Visa2.jpg" /></label>
							</div>
						<?php else: ?>
							<?php if($TotalAPagar > 30000): ?>
							<div class="carr_pago_option">
								<input type="radio" id="ppagosonline" value="1" name="paymentMethod" >
								<label for="ppagosonline" ><img id="paymentImg1" class="payment" align="top" src="img/conpagosonline2.jpg" /></label>
							</div>
							<?php else: ?>
							<div class="carr_pago_option">
								<input type="radio" id="ppagosonlinecredito" value="2" name="paymentMethod">
								<label for="ppagosonlinecredito" ><img id="paymentImg2" class="payment" align="top" src="img/pagocon-credito2.jpg" /></label>
							</div>
							<div class="carr_pago_option">
								<input type="radio" id="ppagosonlinepse" value="3" name="paymentMethod">
								<label for="ppagosonlinepse" ><img id="paymentImg3" class="payment" align="top" src="img/transf-banc2.jpg" /></label>
							</div>
							<?php endif; ?>
						<?php endif; ?>
		
						<div class="carr_pago_option gcash">
						<?php if($ActivarGeelbeCash): ?>
							<input type="radio" id="pgeelbecash" value="5" name="paymentMethod">
							<label for="pgeelbecash" ><img src="img/gcash2.jpg" name="Imagen10" width="102" height="40" border="0" id="Imagen10" /></label>
						<?php else: ?>
							<a id="geelbecashImg">
								<img class="payment" align="top" src="img/gcash2.jpg" />
							</a>
							<div class="tooltip middle">
								<p>No ten&eacute;s dinero disponible en <strong>GCash</strong> para realizar esta compra.<br /><br />
								Con <strong>GCash</strong> pod&eacute;s precargar dinero en tu cuenta, <strong>pagando en efectivo o con tarjeta</strong>, para luego utilizarlo en la compra de productos.</p>
							</div>
							<script type="text/javascript">
								$(document).ready(function() {
									$("#geelbecashImg").tooltip({predelay:200});
								});
							</script>
						<?php endif; ?>
						</div>
						

						</div>
					<?php
	        			} 
	        		?>
	        	      </div>
		        	</div>
	        	
					<input type="hidden" id="txtTotal" name="txtTotal" value="<?=$SubTotal - $CreditosUtilizados + $Envio?>"/>
					<input type="hidden" id="txtValorEnvio" name="txtValorEnvio" value="<?=$Envio?>"/>
					<input type="hidden" id="txtBonoUtilizado" name="txtBonoUtilizado" value="<?=$CreditosUtilizados?>"/>
					<input type="hidden" name="IdSucursal" id="IdSucursal" value="<?=$_REQUEST["id_sucursal"] ?>" />
					<input type="hidden" name="IdFormaEnvio" id="IdFormaEnvio" value="" />
					<?php
						include($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/ssl/carrito/includes/userpass-form.php");
					?>
	        	</form>
        	</div>

<script>


$(document).ready(function() {

	$('.carr_pago_option img').bind('mouseover', function() {
		
		this.src = this.src.replace('2.jpg', '1.jpg')
		
	}).bind('mouseout', function() {
	
		if(!$(this).hasClass('checked'))
			this.src = this.src.replace('1.jpg', '2.jpg')
			
	})
	
	$('.carr_pago_option').bind('click', function() {
	
		var img = $(this).find('img');
		var input = $(this).find('input');
		
		$('.carr_pago_option img').removeClass('checked').each(function(k, el) {
			el.src = el.src.replace('1.jpg', '2.jpg');
		})
		
		enableStep3();
		
		if(input.attr('checked')) {
			
			img.addClass('checked');
			img.attr('src', img.attr('src').replace('2.jpg', '1.jpg'))
			
		} else {

			img.removeClass('checked');
			img.attr('src', img.attr('src').replace('1.jpg', '2.jpg'))
		
		}
	
	})

})

var step3Disabled = true;
var showUserPassform = false;
function enableStep3() {
	var btn = $('#carr_bt_pagar img').get(0);

	if(envioSeleccionado() && pagoSeleccionado()) {
		step3Disabled = false;
		btn.src = btn.src.replace('2.jpg', '1.jpg');
	} else {
		step3Disabled = true;
		btn.src = btn.src.replace('1.jpg', '2.jpg');
	}
	
	return false;
	
}

function editingAddress() {
	return ($('#modifyAddress:visible').size() > 0);
}

function envioSeleccionado() {
	var selected = validateAddress();
	
	if(selected && !editingAddress()) {
		$('#carr_envio_cont').removeClass('complete');
		$('#carr_pago_cont').addClass('complete');
		
		return true;
	} else {
		$('#carr_envio_cont').addClass('complete');
		$('#carr_pago_cont').removeClass('complete');
		
		return false;
	}
	
}

function pagoSeleccionado() {

	//Verificar forma de pago
	var val = $("input[name='paymentMethod']:checked").val();
	
	showUserPassform = (val == 5);
	
	return !isNaN(val)

}

$(document).ready(function(){
	 
	activarEnvios(0);
	envioSeleccionado();
	
	$('#cancelUserPassTrigger').click(function() {
		$('#userpass-container').hide();
	})
	
	$('#userPassSubmitTrigger').click(function() {
		$('#paymentMethodForm').submit();
	})
	
});

function activarEnvios(id) {
	
	if(!validateAddress())
		return false;
	
	enableStep3()
	getEnvio(id);

}

function getEnvio(id) {

	$.ajax({
		url: 'ajax/envio.php?id='+id+'&id_sucursal='+$('#oca_sucursal').val()+'<?=(isset($_GET['IdPedido']) ? '&IdPedido='.$_GET['IdPedido']:'');?>',
		dataType: 'json',
		success: function(data) {
			$('#txtValorEnvio').val(data.fenvio);
			$('#envioTabla').html(data.envio);
			$('#txtTotal').val(data.ftotal);
			$('#totalTabla').html(data.total);
			total = data.ftotal;
			//setTimeout('refreshCuotas()',250);
		}
	})

}

$(document).ready(function(){
	$("#secCodHelpTrigger").hover(function() {
		$("#secCodHelpInfo").stop(true, true).animate({opacity: "show"}, "slow");
	}, function() {
		$("#secCodHelpInfo").animate({opacity: "hide"}, "fast");
	});
});

  </script>
