<?php header( 'Content-type: text/html; charset=iso-8859-1' );?>

	  <?
	    $DatosShippingCargados = CartUtils::isShippingInfoFilled($objUsuario);
	    $IdProvincia = $objUsuario->getDirecciones(0)->getIdProvincia();
	    $CantidadProductos = count($MiCarrito->getArticulos());
	    $SubTotal = 0;
	    $SubTotalPrecioPublico = 0;
	    $Ganancia = 0;
	    $Peso = 0;
	    $P = 0;
	    $CostoCompra = 0;
	    $PrecioVenta = 0;
	    $FreeShippingProduct = false;
	    $CreditosUtilizados = $_SESSION["CREDITO_APLICADO_CARRITO"];
	    foreach($MiCarrito->getArticulos() as $IdCodigoProdInterno => $Articulo):
	    
	    	$objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
                $objProducto = dmProductos::getById($Articulo["IdProducto"]);
                $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($IdCodigoProdInterno);

            $SubTotal = $SubTotal + ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo["Cantidad"];
            $SubTotalPrecioPublico = $SubTotalPrecioPublico + ObtenerPrecioProductos($objProducto->getPVP(), $objPP->getIncremento()) * $Articulo["Cantidad"];
            $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
            
            // Hack loco para envio gratis de productos de nokia
            // TODO: hacer esto dinamico
            if ($IdCodigoProdInterno == 51312) {
 				$FreeShippingProduct = true;
            } else {
	            $Peso += $P * $Articulo["Cantidad"];
            }
            
            $Ganancia += abs(($objProducto->getPVenta() - $objProducto->getPCompra())*$Articulo["Cantidad"]);
            $CostosCompra += $objProducto->getPCompra()* $Articulo["Cantidad"];
            $PreciosVenta += $objProducto->getPVenta()* $Articulo["Cantidad"];
	  ?>
	  
	  	<!-- CONTENEDOR DEL PRODUCTO -->
	  	<div id="carr_prod_contenedor">
			<div id="carr_prod_img">
				<img height="50" width="50" class="prodImg" src="<?="/".$confRoot[1]."/front/productos/productos_".$objProducto->getIdProducto()."/imagen_1.jpg"?>" alt="<?=$objProducto->getNombre()?>">
			</div>
			<div id="carr_prod_detail">
				<span class="carr_tit_prod"><?=$objProducto->getNombre()?></span><br />
				<span class="carr_talle">
				<?php foreach($Atributos as $atributo): ?>
				<?=$atributo["Nombre"]?>: <?=$atributo["Valor"]?>
				<?php endforeach; ?>
				</span><br />
				<span class="carr_ahorro">Ahorr&aacute;s por unidad: <?=Moneda(ObtenerPrecioProductos($objProducto->getPVP() - $objProducto->getPVenta(), $objPP->getIncremento()))?></span>
			</div>
			<div id="carr_prod_precio">
				<span class="carr_price">
					<?=Moneda(ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()))?>
				</span><br />
				<span class="carr_price_before">
					<?=Moneda(ObtenerPrecioProductos($objProducto->getPVP(), $objPP->getIncremento()))?>
				</span>
			</div>
			<div id="carr_prod_cant">
			<form id="cartChangeQuantityForm" action="/cartChangeQuantity.do" method="post"> 
				<select class="carr_detail" id="product<?=$IdCodigoProdInterno?>Quantity">
				<?
				$disponible = min($Articulo["Stock"],$objCampania->getMaxProd());
                    /*for($i=1; $i<=$objCampania->getMaxProd(); $i++)*/
                    for($i=1; $i<=$disponible; $i++)
                    {
                        ?><option value="<?=$i?>" <?=($i == $Articulo['Cantidad'] ? 'selected="selected"' : '');?>><?=$i?></option><?
                    }
                ?>
				</select>
				<script type="text/javascript">
					$('#product<?=$IdCodigoProdInterno?>Quantity').change(function() {
						$.ajax({
						  url: '<?="/".$confRoot[1]."/ssl/carrito/ajax/change_number_of_products.php?IdProducto=".$objProducto->getIdProducto()."&IdCodigoProdInterno=".$IdCodigoProdInterno."&Cantidad="; ?>' + $('#product<?=$IdCodigoProdInterno?>Quantity').val(),
						  success: function(data) {
							  window.location.href="<?="/".$confRoot[1]."/ssl/carrito/step_1.php?CART_RELOADING=true"?>";
						  }
						});
					});
				</script>
				<br />
				<span class="carr_talle">
					<a class="remove" id="removeProduct<?=$IdCodigoProdInterno?>FromCartTrigger" href="javascript:;">Remover</a>
					<script type="text/javascript">
						$('#removeProduct<?=$IdCodigoProdInterno?>FromCartTrigger').click(function() {
							$.ajax({
							  url: '<?="/".$confRoot[1]."/ssl/carrito/ajax/remove_product_from_cart.php?IdCodigoProdInterno=".$IdCodigoProdInterno; ?>',
							  success: function(data) {
								  window.location.href="<?="/".$confRoot[1]."/ssl/carrito/step_1.php?CART_RELOADING=true"?>"
							  }
							});
						});
					</script>
				</span>
			</form>
			</div>
			<div id="carr_prod_tot" class="carr_price">
				<?=Moneda(ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo["Cantidad"])?>
			</div>
		</div>
		<!-- FIN CONTENEDOR DEL PRODUCTO-->
		
      <?
	    endforeach;
		
		/* DESCUENTOS */
		$Descuento = $MiCarrito->getDescuento($SubTotal);
		if($Descuento > 0):			
			$objDescuento = $MiCarrito->getDescuentoObj();
			
			if($objDescuento->getPorcentaje())
				$detalle = $objDescuento->getDescuento().'%';
			else
				$detalle = Moneda($objDescuento->getDescuento());
				
			$SubTotal -= $Descuento;
	?>
	  	<div id="carr_prod_contenedor">
			<div id="carr_prod_img">
				
			</div>
			<div id="carr_prod_detail">
				<span class="carr_tit_prod"><?=$objDescuento->getDescripcion()?></span><br />
				<span class="carr_talle">
					<?=$detalle?> de descuento
				</span><br />
			</div>
			<div id="carr_prod_precio">
				<span class="carr_price">
					- <?=$detalle?>
				</span>
			</div>
			<div id="carr_prod_cant">
			<form id="cartChangeQuantityForm" action="/cartChangeQuantity.do" method="post"> 
				<span class="carr_talle">
					<a class="remove" href="step_1.php?removeDiscount">Remover</a>
				</span>
			</form>
			</div>
			<div id="carr_prod_tot" class="carr_price">
				-<?=Moneda($Descuento)?>
			</div>
		</div>
	<?php		
		endif;
		/* FIN DESCUENTOS */
		
    	$Envio = 0;
         /*$montoEnvioGratis = dmCampania::getMinMontoEnvioGratis($MiCarrito->getIdCampania());
         if ($montoEnvioGratis != 0 && $SubTotal >= $montoEnvioGratis) {
	         $Envio = 0;
         } else {
         if($DatosShippingCargados) {
                
         		if ($FreeShippingProduct) {
					if ($CantidadProductos > 1) {
		                $objDHL = dmDHL::getTarifaByPesoPcia($Peso, $objDireccion->getIdProvincia(), $objCampania);
		             	$Envio = $objDHL->getImporte();
					} else {
		         		$Envio = 0;
					}      			
         		} else {
	                $objDHL = dmDHL::getTarifaByPesoPcia($Peso, $objDireccion->getIdProvincia(), $objCampania);
	             	$Envio = $objDHL->getImporte();
         		}
         	
                
         	} else {
         		$Envio = 0;
         	}
         	
            $dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($objCampania->getIdCampania());
            foreach($dtCampaniasPropiedades as $propiedad) {
                if($propiedad["IdPropiedadCampamia"]=='6') {
                  $Envio = 0;
                }
            }
         }*/
         
         $ahorro = Moneda($SubTotalPrecioPublico - $SubTotal + $CreditosUtilizados);
   ?>

			<!-- AHORRO EN GEELBE -->
			<div id="carr_ahorro_cont">
				<div id="carr_ahorro_tit" class="carr_compra_geelbe">Comprando en Geelbe est&aacute;s ahorrando:</div>
				<div id="carr_ahorro_price" class="carr_compra_g_price"><?=$ahorro?></div>
				<span class="carr_talle">Por la misma compra en las tiendas estar&iacute;as pagando <?=Moneda($SubTotalPrecioPublico)?></span>
			</div>
			<!--FIN AHORRO GEELBE-->
			
			<!-- SUBTOTALES-->
			<div id="carr_tot_cont">
				<div id="carr_tot_item" class="carr_detail">Subtotal</div>
				<div id="carr_tot_price" class="carr_detail"><?=Moneda($SubTotal);?></div>
				<div id="carr_tot_item" class="carr_detail">Cr&eacute;ditos utilizados en la compra</div>
				<div id="carr_tot_price" class="carr_detail">-<?=Moneda($CreditosUtilizados);?></div>
				<div id="carr_tot_item" class="carr_detail">Costo de env&iacute;o</div>
				<div id="carr_tot_price" class="carr_detail">
					<span id="envioTabla">
					<? if($DatosShippingCargados): ?>
                        <?=Moneda($Envio);?>
                    <?php else: ?>
                        $0,00
                    <?php endif; ?>
                    </span>
				</div>
				<div id="carr_tot_item_total" class="carr_total">Total</div>
				<div id="carr_tot_price_total" class="carr_total">
					<span id="totalTabla">
						<?=Moneda($SubTotal - $CreditosUtilizados + $Envio);?>
					</span>
				</div>
			</div>
			<!--FIN SUBTOTALES-->

<? $permitido = intval(($PreciosVenta - $CostosCompra)/2); ?>
<? $disponibles = dmMisCreditos::getCreditoDisponible(Aplicacion::Decrypter($_SESSION["User"]["id"])); ?>
<input type="hidden" name="maxPermitidoCreditos" id="maxPermitidoCreditos" value="<?= min($disponibles, $permitido) ?>" />

<?
if(count($MiCarrito->getArticulos())==0 || $MiCarrito->Expiro()) {
?>
<script type="text/javascript">
	window.location.href = "<?= 'http://'.$_SERVER['HTTP_HOST'].'/'.$confRoot[1].'/front/vidriera/'; ?>";
</script>
<?
}
?>