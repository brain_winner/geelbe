<?php header('Content-type: text/html; charset=iso-8859-1'); ?>
<?
$DescripcionProvincia = "";
$DescripcionPiso = "";

if ($objDireccion->getIdProvincia()) {
    $oProvincia = dmCliente::GetProvinciasById($objDireccion->getIdProvincia());
    $DescripcionProvincia = $oProvincia[0]["Nombre"];
}

if ($objDireccion->getIdCiudad()) {
    $dt = dmMicuenta::GetCiudadById($objDireccion->getIdCiudad());
    $DescripcionCiudad = $dt[0]["Descripcion"];
}
if ($objDireccion->getPiso()) {
    $DescripcionPiso = " " . $objDireccion->getPiso() . $objDireccion->getPuerta();
}

$DatosShippingCargados = CartUtils::isShippingInfoFilled($objUsuario);
?>   

<a name="shippingDataForm"></a>
<div id="envio6" class="envios">

    <? if (isset($_GET["incomplete_shipping_data"]) && !empty($_GET["incomplete_shipping_data"])): ?>
        <p style="color:red; font-weight:bold; margin: 10px 5px;">Por favor verifique los datos de contacto y de domicilio antes de continuar con el siguiente paso.</p>
    <? endif; ?> 

    <div id="adress">

        <span><?= $objDireccion->getDomicilio() . " " . $objDireccion->getNumero() . $DescripcionPiso ?><br />
            <?= $DescripcionCiudad ?><br />
            <?= $DescripcionProvincia . $DescripcionCP ?></span>

        <p class="moddir"><a href="javascript:modifyAddress()" id="moddatosenviolink2">Modificar direcci&oacute;n</a></p>

    </div>

    <div id="modifyAddress" style="display:none">

        <form action="javascript:;" method="post" id="saveAddress" onsubmit="return saveAddress()">
            <div id="showError" name="showError" style="display: none;">
                
            </div>

            <p><label>Direcci&oacute;n:</label><br /><input name="frmDir[txtDomicilio]" type="text" size="50" class="imputbportada" value="<?= $objUsuario->getDirecciones(0)->getDomicilio() ?>" id="frmDir[txtDomicilio]" gtbfieldid="52"></p>
            <p>
                <label>Departamento:</label>
                <br />
                <select name="frmDi1[cbxProvincia]" id="idProvincia" class="imputbportada" onchange="javascript:actualizarCiudades()"> 
                    <option value="0" >Departamentos</option>
                    <?
                    $dt = dmMicuenta::GetProvincias();
                    $objDireccion = $objUsuario->getDirecciones(0);
                    foreach ($dt as $value) {
                        if ($objDireccion->getIdProvincia() == $value['IdProvincia']) {
                            ?>
                            <option selected="selected" value='<?= $value['IdProvincia'] ?>'><?= $value['Nombre'] ?></option>
                            <?
                        } else {
                            ?>
                            <option value='<?= $value['IdProvincia'] ?>'><?= $value['Nombre'] ?></option>
                            <?
                        }
                    }
                    ?>
                </select>
            </p>  
            <div style="clear:both"></div>
            <p><label>Ciudad</label><br>
                <select name="frmDi1[cbxCiudad]" id="idCiudad" class="imputbportada"> 
                    <option value="0" >Ciudades</option>
                    <?
                    if ($objUsuario->getDirecciones(0)->getIdProvincia() != null) {
                        $dt = dmMicuenta::GetCiudades($objUsuario->getDirecciones(0)->getIdProvincia());
                        foreach ($dt as $value) {
                            if ($objUsuario->getDirecciones(0)->getIdCiudad() == $value['IdCiudad']) {
                                ?>
                                <option selected="selected" value='<?= $value['IdCiudad'] ?>'><?= $value['Descripcion'] ?></option>
                                <?
                            } else {
                                ?>
                                <option value='<?= $value['IdCiudad'] ?>'><?= $value['Descripcion'] ?></option>
                                <?
                            }
                        }
                    }
                    ?>                                            
                </select>

            <div style="clear:both"></div>

            <p>
                C&eacute;dula<br />
                <input name="frmDaU[txtNroDoc]" id="frmDaU[txtNroDoc]" type="text" size="11" maxlength="10" onkeypress="return isNumberKey(event)" value="<?= $objUsuario->getDatos()->getNroDoc(); ?>" />
            </p>

            <div style="clear:both"></div>

            <p>
                Tel&eacute;fono M&oacute;vil<br />
                <!--<select name='frmDaU[cmb_CelularComp]' id='frmDaU[cmb_CelularComp]'>
                    <option value="0">Compa&ntilde;ia</option>
                    <option value="1">COMCEL</option>
                    <option value="2">MOVISTAR</option>
                    <option value="3">TIGO</option>
                    <option value="4">OTRO</option>
                </select>
                <input name='frmDaU[txt_CelularCodigo]' type='text' id='frmDaU[txt_CelularCodigo]' size="3" maxlength="6" value="<?= $objUsuario->getDatos()->getcelucodigo() ?>" />-->
                <input name='frmDaU[txt_CelularNumero]' type='text' id='frmDaU[txt_CelularNumero]' size="14" maxlength="15" value="<?= $objUsuario->getDatos()->getcelunumero() ?>" />
            </p>

            <input type="hidden" name="frmDir[cbxProvincia]" value="<?= $objUsuario->getDirecciones(0)->getIdProvincia() ?>" />
            <input type="hidden" name="frmDi1[txtPoblacion]" value="<?= $objUsuario->getDirecciones(0)->getPoblacion() ?>" />
            <input type="hidden" name="frmDir[cbxPais]" id="frmDir[cbxPais]" value="15" />

            <table cellspacing="0" cellpadding="0" border="1px" class="btn btnRed derecha chico XXS"> 
                <tbody>
                    <tr>
                        <td>
                            <a id="updateUserInfoTrigger2" href="javascript:;" onclick="$('#saveAddress').submit(); return false;"><strong>Guardar</strong></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>

    </div>

</div>

<script type="text/javascript">
    
    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
 
         return true;
      }
    
    if(!validateAddress())
        modifyAddress();
</script>

