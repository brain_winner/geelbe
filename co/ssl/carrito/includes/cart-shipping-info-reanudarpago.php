<?php header( 'Content-type: text/html; charset=iso-8859-1' );?>
<?
$DescripcionProvincia = "";
$DescripcionPiso = "";

if ($objDireccion->getIdProvincia()) {
	$oProvincia = dmCliente::GetProvinciasById($objDireccion->getIdProvincia());
	$DescripcionProvincia = $oProvincia[0]["Nombre"];
}

if ($objDireccion->getPoblacion()) {
	$dt = dmMicuenta::GetCiudadById($objDireccion->getPoblacion());
	$DescripcionCiudad = $dt[0]["Descripcion"];
}
if ($objDireccion->getPiso()) {
	$DescripcionPiso = " ".$objDireccion->getPiso().$objDireccion->getPuerta();
}
$DescripcionCP = "";
if ($objDireccion->getCP()) {
	$DescripcionCP = " (CP:".$objDireccion->getCP().")";
}

$DatosShippingCargados = CartUtils::isShippingInfoFilled($objUsuario);

?>       
     <a name="shippingDataForm"></a>
     <?
        if (isset($_GET["incomplete_shipping_data"]) && !empty($_GET["incomplete_shipping_data"])) {
     ?>
         <p style="color:red; font-weight:bold; margin: 10px 5px;">Por favor verifique los datos de contacto y de domicilio antes de continuar con el siguiente paso.</p>
     <?
        }
     ?>
       <div class="direccionenvio">
        <div class="dirnenvio">
        	<?php 
        		$NombreCliente = $objUsuario->getDatos()->getNombre(); 
        		if ($NombreCliente == "Socio Geelbe") {
        			$NombreCliente = "";
        		}  
        	?>
        	<div id="adress">
								<?=$objDireccion->getDomicilio()." ".$objDireccion->getNumero().$DescripcionPiso?><br />
								<?=$DescripcionCiudad ?>
								<?=$DescripcionProvincia.$DescripcionCP ?>
			</div>
              <?
              $MiCarrito = dmPedidos::getPedidoById($_GET['IdPedido']);
        	$datosCampania = dmCampania::getByIdCampania($MiCarrito->getIdCampania());
        	$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
			$mesFn = $meses[date("n", strtotime($datosCampania->getTiempoEntregaFn())) - 1];
			$mesIn = $meses[date("n", strtotime($datosCampania->getTiempoEntregaIn())) - 1];
              ?>
        </div>
        </div>