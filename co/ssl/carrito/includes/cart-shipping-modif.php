<div id="simplemodal-container" class="simplemodal-container" style="display:none;background:white;position: fixed; _position:absolute; z-index: 10002; left: 343.5px; top: 85px; opacity: 1; width: 620px">
	<a title="Close" class="modalCloseImg simplemodal-close"></a><div class="findFriends simplemodal-data" id="grabContactsHolder" style="display: block;">

<form name="cartShippingInfoForm" id="cartShippingInfoForm" style="margin-bottom:0;">
<div id="formdatosenvio" class="dirnenvioform" /*style="display:none;"*/>
    <div id="cartShippingInfoForm_errorloc" class="error_strings"></div>
	<div class="col_izquierda" style="overflow:hidden;">
        		<h4>DATOS DEL CONTACTO</h4>
        			<?php 
        				$NombreCliente = $objUsuario->getDatos()->getNombre(); 
        				if ($NombreCliente == "Socio Geelbe") {
        					$NombreCliente = "";
        				}
        			?>                		
                		<p><label>Nombres:</label><br><input value="<?=$NombreCliente; ?>" id="frmDU[txtNombre]" name="frmDU[txtNombre]" size="30" gtbfieldid="44"></p>
        			<p><label>Apellidos:</label><br><input value="<?=$objUsuario->getDatos()->getApellido(); ?>" id="frmDU[txtApellido]" name="frmDU[txtApellido]" size="30" gtbfieldid="45"></p>
        			<p><label>N&uacute;mero de documento de identidad:</label><br><input value="<?=$objUsuario->getDatos()->getNroDoc(); ?>" maxlength="10" size="10" id="frmDU[txtDNI]" name="frmDU[txtDNI]" gtbfieldid="46"></p>
               		<p><label>Tel&eacute;fono <span style="font-style:italic;font-size:9px;">(Ejemplo: 1 3476043)</span>:</label><br>(<input name='frmDU[txtCodigoTelefono]' type='text' id='frmDU[txtCodigoTelefono]' size="3" maxlength="6" value="<?=$objUsuario->getDatos()->getCodTelefono()?>" />) <input name='frmDU[txtTelefono]' id='frmDU[txtTelefono]' type='text'  size="15" maxlength="15" value="<?=$objUsuario->getDatos()->getTelefono()?>" /></p>
					<p><label>Tel&eacute;fono m&oacute;vil <span style="font-style:italic;font-size:9px;">(Ejemplo: 320 2554522)</span>:</label><br>
					    <?
					    	$Companias = array(0 => "Compa&ntilde;ia", 1 => "COMCEL", 2 => "MOVISTAR", 3 => "TIGO", 4 => "OTRO"); 
					    ?>
						<select id="frmDU[cbxCeluEmpresa]" name="frmDU[cbxCeluEmpresa]" style="background-color:#CCCCCC;border:1px solid #999999" gtbfieldid="49">
						<?
						foreach($Companias as $key => $value) {
							if($key == $objUsuario->getDatos()->getceluempresa()) {
							?>
			                  <option selected="selected" value="<?=$key?>"><?=$value?></option>
						    <?
							} else {
							?>
			                  <option value="<?=$key?>"><?=$value?></option>
						    <?
							}
						}
						?>
                		</select>
                		(<input name='frmDU[txt_CelularCodigo]' type='text' id='frmDU[txt_CelularCodigo]' size="3" maxlength="6" value="<?=$objUsuario->getDatos()->getcelucodigo()?>" />) <input name='frmDU[txt_CelularNumero]' type='text' id='frmDU[txt_CelularNumero]' size="14" maxlength="15" value="<?=$objUsuario->getDatos()->getcelunumero()?>" /></p>
                		
                		
	</div>
    <div class="col_derecha" style="overflow:hidden;padding-left:10px">
                <h4>DOMICILIO DE ENTREGA DE LOS PRODUCTOS</h4>

      			 <p><label>Direcci&oacute;n <span style="font-style:italic;font-size:9px;">(Ejemplo: Carrera 15 #93a-62)</span>:</label><br><input name="frmDir[txtDomicilio]" id="frmDir[txtDomicilio]" type="text" size="50" class="imputbportada" value="<?=$objUsuario->getDirecciones(0)->getDomicilio()?>" id="frmDir[txtDomicilio]" name="frmDir[txtDomicilio]" gtbfieldid="52"></p>
                 <p><label>Departamento:</label><br><select class="imputbportada" id="idProvincia" style="background-color:#CCCCCC;border:1px solid #999999" name="frmDir[cbxProvincia]" onchange="javascript:actualizarCiudades()"> 
			   		<option value="0">Departamentos</option>
                 <?
                         $dt = dmMicuenta::GetProvincias();
                         foreach($dt as $value)
                         {
                         	if($objDireccion->getIdProvincia() == $value['IdProvincia']) {
                                ?>
                                   <option selected="selected" value='<?=$value['IdProvincia']?>'><?=$value['Nombre']?></option>
                                <?
                         	} else {
                                ?>
                                   <option value='<?=$value['IdProvincia']?>'><?=$value['Nombre']?></option>
                                <?
                         	}
                         }                                                 
                  ?>  
                  </select></p>
                 <p><label>Ciudad:</label><br>
                 <select name='frmDir[txtCiudad]' id='idCiudad' style="background-color:#CCCCCC;border:1px solid #999999" class='imputbportada'> 
                  <option value="0" >Ciudades</option>
                         <?
                         if($objUsuario->getDirecciones(0)->getIdProvincia() != null) { 
	                         $dt = dmMicuenta::GetCiudades($objUsuario->getDirecciones(0)->getIdProvincia());
	                         foreach($dt as $value)
	                         {
	                         	if($objUsuario->getDirecciones(0)->getPoblacion() == $value['IdCiudad']) {
		                         	?>
		                               <option selected="selected" value='<?=$value['IdCiudad']?>'><?=$value['Descripcion']?></option>
		                            <?
	                         	} else {
		                            ?>
		                               <option value='<?=$value['IdCiudad']?>'><?=$value['Descripcion']?></option>
		                            <?
	                         	}
	                         }
                         }                                                 
                  ?>                                            
                  </select></p>
                <input type="hidden" name="frmDir[cbxPais]" id="frmDir[cbxPais]" value="14" />
        	</div>
        	<div id="cart-info-modif-buttons" style="clear:both;float:left;overflow:auto;width:100%">
        	<?
        	$DatosShippingCargados = CartUtils::isShippingInfoFilled($objUsuario);
        	if ($DatosShippingCargados) {        	
        	?>
				<table style="float:left" cellspacing="0" cellpadding="0" border="1px" class="btn btnGrey izquierda chico XXS"> 
					<tbody>
						<tr>
							<td>
								<a id="cancelUserInfoTrigger" href="javascript:;"><strong>Cancelar</strong></a>
							</td>
						</tr>
					</tbody>
				</table>
        	<?
        	}      	
        	?>
				<table cellspacing="0" cellpadding="0" border="1px" class="btn btnRed derecha chico XXS"> 
					<tbody>
						<tr>
							<td>
								<a id="updateUserInfoTrigger" href="javascript:;"><strong>Guardar</strong></a>
							</td>
						</tr>
					</tbody>
				</table>
		      </div>
            </div>
            </form>
            </div>


<script type="text/javascript">
	$('#updateUserInfoTrigger').click(function() {
		if(validateFormFields()) {
		$.ajax({
		  type: "POST",
		  data: $("#cartShippingInfoForm").serialize().replace(/%5B%5D/g, '[]'),
		  url: '<?="/".$confRoot[1]."/ssl/carrito/ajax/modify_user_info.php" ?>',
		  success: function(data) {
			//window.location.href="<?="/".$confRoot[1]."/ssl/carrito/step_1.php?CART_RELOADING=true"?>"
			NOADDRESS = false;
			$('#simplemodal-container').hide();
			enableStep3();
		  }
		});
	}});
	<?
	if ($DatosShippingCargados) {        	
    ?>
	$('#cancelUserInfoTrigger').click(function() {
		$('#simplemodal-container').css("display", "none");
	});
	<?
	}        	
	?>
	function actualizarCiudades() {
	   $.ajax({ url: "/<?=$confRoot[1]?>/logica/micuenta/actionform/obtener_ciudades.php?IdProvincia="+$("#idProvincia").val(), context: document.body, success: function(data){
 			$("#idCiudad").html(data);
	   }});
   }
</script>