<?php header( 'Content-type: text/html; charset=iso-8859-1' );?>
	  <?
	  	if(isset($_SESSION['lastPedidoId']) && is_numeric($_SESSION['lastPedidoId'])) {
	  	    $Articulos = $MiCarrito->getProductos();
	  	} else {
	  	   	$Articulos = $MiCarrito->getArticulos();
	  	}
	  
	    $DatosShippingCargados = CartUtils::isShippingInfoFilled($objUsuario);
	    $IdProvincia = $objUsuario->getDirecciones(0)->getIdProvincia();
	    $CantidadProductos = count($Articulos);
	    $SubTotal = 0;
	    $SubTotalPrecioPublico = 0;
	    $Ganancia = 0;
	    $Peso = 0;
	    $P = 0;
	    $CostoCompra = 0;
	    $PrecioVenta = 0;
	    $CreditosUtilizados = dmMisCreditos::VIEWgetByIdPedido($_SESSION['lastPedidoId']);
	    $CreditosUtilizados = $CreditosUtilizados[0]["Bono"];
	    $_SESSION["CREDITO_APLICADO_CARRITO"] = $CreditosUtilizados;
	    $FreeShippingProduct = false;
	    foreach($Articulos as $IdCodigoProdInterno => $Articulo):
	    
    		$IdCodigoProdInterno = $Articulo->getIdCodigoProdInterno();
    		
	    	$objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
            $objProducto = dmProductos::getById($objPP->getIdProducto());
            $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($IdCodigoProdInterno);

            $SubTotal = $SubTotal + ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo->getCantidad();
            $SubTotalPrecioPublico = $SubTotalPrecioPublico + ObtenerPrecioProductos($objProducto->getPVP(), $objPP->getIncremento()) * $Articulo->getCantidad();
            $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
            
            // Hack loco para envio gratis de productos de nokia
            // TODO: hacer esto dinamico
            if ($IdCodigoProdInterno == 51312) {
 				$FreeShippingProduct = true;
            } else {
	            $Peso += $P * $Articulo->getCantidad();
            }
            $Ganancia += abs(($objProducto->getPVenta() - $objProducto->getPCompra())*$Articulo->getCantidad());
            $CostosCompra += $objProducto->getPCompra();
            $PreciosVenta += $objProducto->getPVenta();
	  ?>
	  	<!-- CONTENEDOR DEL PRODUCTO -->
	  	<div id="carr_prod_contenedor">
			<div id="carr_prod_img">
				<img height="50" width="50" class="prodImg" src="<?="/".$confRoot[1]."/front/productos/productos_".$objProducto->getIdProducto()."/imagen_1.jpg"?>" alt="<?=$objProducto->getNombre()?>">
			</div>
			<div id="carr_prod_detail">
				<span class="carr_tit_prod"><?=$objProducto->getNombre()?></span><br />
				<span class="carr_talle">
				<?php foreach($Atributos as $atributo): ?>
				<?=$atributo["Nombre"]?>: <?=$atributo["Valor"]?>
				<?php endforeach; ?>
				</span><br />
				<span class="carr_ahorro">Ahorr&aacute;s por unidad: <?=Moneda(ObtenerPrecioProductos($objProducto->getPVP() - $objProducto->getPVenta(), $objPP->getIncremento()))?></span>
			</div>
			<div id="carr_prod_precio">
				<span class="carr_price">
					<?=Moneda(ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()))?>
				</span><br />
				<span class="carr_price_before">
					<?=Moneda(ObtenerPrecioProductos($objProducto->getPVP(), $objPP->getIncremento()))?>
				</span>
			</div>
			<div id="carr_prod_cant">
				<?=$Articulo->getCantidad()?>
			</div>
			<div id="carr_prod_tot" class="carr_price">
				<?=Moneda(ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento()) * $Articulo->getCantidad())?>
			</div>
		</div>
		<!-- FIN CONTENEDOR DEL PRODUCTO-->

      <?
	    endforeach;
		
		/* DESCUENTOS */
		$Descuento = $MiCarrito->getDescuento();
		if($Descuento > 0):			
			$objDescuento = dmDescuentos::getById($MiCarrito->getIdDescuento());
			
			if($objDescuento->getPorcentaje())
				$detalle = $objDescuento->getDescuento().'%';
			else
				$detalle = Moneda($objDescuento->getDescuento());
				
			$SubTotal -= $Descuento;
	?>
	  	<div id="carr_prod_contenedor">
			<div id="carr_prod_img">
				
			</div>
			<div id="carr_prod_detail">
				<span class="carr_tit_prod"><?=$objDescuento->getDescripcion()?></span><br />
				<span class="carr_talle">
					<?=$detalle?> de descuento
				</span><br />
			</div>
			<div id="carr_prod_precio">
				<span class="carr_price">
					- <?=$detalle?>
				</span>
			</div>
			<div id="carr_prod_cant">
			
			</div>
			<div id="carr_prod_tot" class="carr_price">
				-<?=Moneda($Descuento)?>
			</div>
		</div>
	<?php		
		endif;
		/* FIN DESCUENTOS */
	    
         $montoEnvioGratis = dmCampania::getMinMontoEnvioGratis($MiCarrito->getIdCampania());
         if ($montoEnvioGratis != 0 && $SubTotal >= $montoEnvioGratis) {
	         $Envio = 0;
         } else {
         if($DatosShippingCargados) {
               	if ($FreeShippingProduct) {
					if ($CantidadProductos > 1) {
				    	$oConexion = new Conexion;
					    $oConexion->setQuery("SELECT * FROM pedidos_oca WHERE IdPedido = ".$_SESSION['lastPedidoId']);
					    $oca = $oConexion->DevolverQuery();
					    if(count($oca) > 0) {
		         			$Envio = dmOCA::getTarifaByPesoSucursal($Peso, $oca[0]['IdSucursal']);
							// Valor para el seguro
		         			//if($SubTotal > 500)
		         			//	$Envio += $SubTotal/100*2;
					    } else {
		                	$objDHL = dmDHL::getTarifaByPesoPcia($Peso, $objDireccion->getIdProvincia(), $objCampania);
			             	$Envio = $objDHL->getImporte();
			            }
					} else {
		         		$Envio = 0;
					}      			
         		} else {
         			$oConexion = new Conexion;
				    $oConexion->setQuery("SELECT * FROM pedidos_oca WHERE IdPedido = ".$_SESSION['lastPedidoId']);
				    $oca = $oConexion->DevolverQuery();
				    if(count($oca) > 0) {
	         			$Envio = dmOCA::getTarifaByPesoSucursal($Peso, $oca[0]['IdSucursal']);
						// Valor para el seguro
	         			//if($SubTotal > 500)
	         			//	$Envio += $SubTotal/100*2;
				    } else {
	                	$objDHL = dmDHL::getTarifaByPesoPcia($Peso, $objDireccion->getIdProvincia(), $objCampania);
		             	$Envio = $objDHL->getImporte();
		            }
         		}
         	} else {
         		$Envio = 0;
         	}
         	
            $dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($objCampania->getIdCampania());
            foreach($dtCampaniasPropiedades as $propiedad) {
                if($propiedad["IdPropiedadCampamia"]=='6') {
                  $Envio = 0;
                }
            }
         }
         
         $ahorro = Moneda($SubTotalPrecioPublico - $SubTotal + $CreditosUtilizados);
         $ahorro = substr($ahorro, 0, strpos($ahorro, ','));

   ?>

      <!-- AHORRO EN GEELBE -->
		<div id="carr_ahorro_cont">
			<div id="carr_ahorro_tit" class="carr_compra_geelbe">Comprando en Geelbe est&aacute;s ahorrando:</div>
			<div id="carr_ahorro_price" class="carr_compra_g_price"><?=$ahorro?></div>
			<span class="carr_talle">Por la misma compra en las tiendas estar&iacute;as pagando <?=Moneda($SubTotalPrecioPublico)?></span>
		</div>
		<!--FIN AHORRO GEELBE-->
		
		<!-- SUBTOTALES-->
		<div id="carr_tot_cont">
			<div id="carr_tot_item" class="carr_detail">Subtotal</div>
			<div id="carr_tot_price" class="carr_detail"><?=Moneda($SubTotal);?></div>
			<div id="carr_tot_item" class="carr_detail">Cr&eacute;ditos utilizados en la compra</div>
			<div id="carr_tot_price" class="carr_detail">-<?=Moneda($CreditosUtilizados);?></div>
			<div id="carr_tot_item" class="carr_detail">Costo de env&iacute;o</div>
			<div id="carr_tot_price" class="carr_detail">
				<span id="envioTabla">
				<? if($DatosShippingCargados): ?>
	                <?=Moneda($Envio);?>
	            <?php else: ?>
	                $0,00
	            <?php endif; ?>
	            </span>
			</div>
			<div id="carr_tot_item_total" class="carr_total">Total</div>
			<div id="carr_tot_price_total" class="carr_total">
				<span id="totalTabla">
					<?=Moneda($SubTotal - $CreditosUtilizados + $Envio);?>
				</span>
			</div>
		</div>
		<!--FIN SUBTOTALES-->
			
			
<? $permitido = intval(($PreciosVenta - $CostosCompra)/2); ?>
<? $disponibles = dmMisCreditos::getCreditoDisponible(Aplicacion::Decrypter($_SESSION["User"]["id"])); ?>
<input type="hidden" name="maxPermitidoCreditos" id="maxPermitidoCreditos" value="<?= min($disponibles, $permitido) ?>" />

<?
  	if(isset($_SESSION['lastPedidoId']) && is_numeric($_SESSION['lastPedidoId'])) {
        if(count($Articulos)==0) {
			?>
			<script type="text/javascript">
				window.location.href = "<?= 'http://'.$_SERVER['HTTP_HOST'].'/'.$confRoot[1].'/front/vidriera/'; ?>";
			</script>
			<?
	    }
  	} else {
        if(count($Articulos)==0 || $MiCarrito->Expiro()) {
			?>
			<script type="text/javascript">
				window.location.href = "<?= 'http://'.$_SERVER['HTTP_HOST'].'/'.$confRoot[1].'/front/vidriera/'; ?>";
			</script>
			<?
	    }
  	}
?>