function actualizarCiudades() {
    $.ajax({
        url: "/co/logica/micuenta/actionform/obtener_ciudades.php?IdProvincia="+$("#idProvincia").val(), 
        context: document.body, 
        success: function(data){
            $("#idCiudad").html(data);
        }
    });
}

function validateAddress() {
	
    var result = false;
	
    $.ajax({
        url: '/co/ssl/carrito/ajax/validate_address.php',
        async: false,
        dataType: 'json',
        success: function(data) {
            result = data.result;
        }
    })
	
    return result;
	
}

function modifyAddress() {
    $('#adress').hide();
    $('#modifyAddress').show();
    enableStep3();
}

function saveAddress() {

    $("#showError").hide();	
        
    if($('input[name=frmDir[txtDomicilio]]').val() == '') {
        alert('Debe completar el campo Direcci\u00F3n.');
        return false;
    }
	
    if($('select[name=frmDi1[cbxProvincia]]').val() == 0) {
        alert('Debe seleccionar un departamento.');
        return false;
    }
	
    if($('select[name=frmDi1[cbxCiudad]]').val() == 0) {
        alert('Debe seleccionar una ciudad.');
        return false;
    }
	
    /*if($('select[name=frmDaU[cmb_CelularComp]]').val() == 0) {
		alert('Debe seleccionar una empresa de telefonía.');
		return false;
	}
	
	if($('input[name=frmDaU[txt_CelularCodigo]]').val() == '') {
		alert('Debe completar el campo teléfono.');
		return false;
	}*/
	
    if($('input[name=frmDaU[txt_CelularNumero]]').val() == '') {
        
        alert('Debe completar el campo tel\u00E9fono.');
        return false;
    }
        
    if($('input[name=frmDaU[txtNroDoc]]').val() == '') {
        alert('Debe completar el campo C\u00E9dula');
        return false;
    }
    
        
    if($('input[name=frmDaU[txtNroDoc]]').val().length < 5) {
       // alert('El campo C\u00E9dula debe contner m\u00CDnimo 5 caracteres y un m\u00E1ximo de 10');
        msjError("Cédula inválida","Error al actualizar");   
       
        return false;
    }
	
    $.ajax({
        url: '/co/ssl/carrito/ajax/modify_user_info.php',
        data: $('#saveAddress').serialize(),
        type: 'POST',
        async: true,
        success: function(data) {
            //alert (data);
            if(data>0){
                $("#showError").show();
                $("#showError").html('<div class="error">Documento ya existe</div>');
            }else{
                var address = $('input[name=frmDir[txtDomicilio]]').val()+'<br />';
                address += $('select[name=frmDi1[cbxCiudad]] option[selected]').text()+'<br />';
                address += $('select[name=frmDi1[cbxProvincia]] option[selected]').text();
        
                $('#adress').show().find('span').html(address);
                $('#modifyAddress').hide();
        			
                $('.formaenvio.envio6').trigger('change');
                getEnvio(0);
                enableStep3();
            }            
        }
    })

    return false;
	
	
}