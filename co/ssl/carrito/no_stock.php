<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/dhl/datamappers/dmdhl.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1]);
		exit;
    }
    
    if(is_array($_SESSION["ProductosSinStock"]) && count($_SESSION["ProductosSinStock"]) > 0) {
    	if(count($_SESSION["ProductosSinStock"]) > 1) {
    		$DescProductos = "";
    		foreach($_SESSION["ProductosSinStock"] as $idx => $productosSinStock) {
    			$objProducto = dmProductos::getById($_SESSION["ProductosSinStock"][$idx]);
    			if ($idx == 0) {
	    			$DescProductos = '"'.$objProducto->getNombre().'"';
    			} else {
	    			$DescProductos .= ', "'.$objProducto->getNombre().'"';
    			}
    		}
    		$Titulo = "Productos sin stock";
    		$Mensaje = "Al intentar completar el pedido, los productos <b>".$DescProductos."</b> se han quedado sin stock.";
    	} else {
    		$Titulo = "Producto sin stock";
    		$objProducto = dmProductos::getById($_SESSION["ProductosSinStock"][0]);
    		$Mensaje = "Al intentar completar el pedido, el producto <b>\"".$objProducto->getNombre()."\"</b> se ha quedado sin stock.";
    	}
    } else {
    	$Titulo = "Producto sin stock";
    	$Mensaje = "Al intentar realizar el pedido, alguno de los productos se ha quedado sin stock.";
    }
    
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>

<link href="/<?=$confRoot[1]?>/front/micarrito/pago.css" rel="stylesheet" type="text/css" />
<link href="/<?=$confRoot[1]?>/front/geelbe.css" rel="stylesheet" type="text/css" />
<link href="/<?=$confRoot[1]?>/front/botones.css" rel="stylesheet" type="text/css" />

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
<? require("../../front/menuess/menu_carrito.php"); ?>
<div id="container">
  <div id="contenido-top">
  </div>
    <div id="contenido">

	<div class="sinstock">
	  <div class="tit" style="text-transform:none!important;"><?=$Titulo?></div>
	  <br />
	  <div class="text"><?=$Mensaje?></div>
	  <br />
				<table cellspacing="0" cellpadding="0" border="1px" class="btn btnRed normal XL">
					<tbody>
						<tr>
							<td bordercolorlight="#a50000" bordercolordark="#a50000" bordercolor="#9595aa">
								<a href="/<?=$confRoot[1]?>/ssl/carrito/step_1.php"><strong>Volver a mis compras</strong></a>
							</td>
						</tr>
					</tbody>
				</table>
	</div>
</div>
  <div id="contenido-bot" style="margin:0px 16px"></div>
</div>	
<? require("../../front/menuess/footer_carrito.php")?>
</body>
</html>
