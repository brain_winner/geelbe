<?
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/ssl/carrito/logic/CartUtils.php");
        
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");
    } catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }

    try {
        ValidarUsuarioLogueado();
	} catch(Exception $e) {
		echo $e;
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }
    
    try {
    	$MiCarrito = Aplicacion::getCarrito();
    } catch(ACCESOException $e) {
    	header('Location: /'.$confRoot[1].'/front/vidriera/');
       	exit;
    } 

    if(count($MiCarrito->getArticulos())==0 || $MiCarrito->Expiro()) {
       header('Location: /'.$confRoot[1].'/front/vidriera/');
       exit;
    }
    
    $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
    $DatosShippingCargados = CartUtils::isShippingInfoFilled($objUsuario);
    if($DatosShippingCargados) {
    	header('Location: /'.$confRoot[1].'/ssl/carrito/step_2.php');
		exit;
    } else {
    	header('Location: /'.$confRoot[1].'/ssl/carrito/step_1.php?incomplete_shipping_data=true#shippingDataForm');
		exit;
    }
    
?>