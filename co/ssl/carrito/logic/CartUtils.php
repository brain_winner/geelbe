<?
class CartUtils {

    public static function isShippingInfoFilled($objUsuario) {
    	$objDireccion = $objUsuario->getDirecciones(0);

    	$DatosShippingCargados = true;

    	$Nombre = $objUsuario->getDatos()->getNombre();
	    $Apellido = $objUsuario->getDatos()->getApellido();
	    $NroDoc = $objUsuario->getDatos()->getNroDoc();
	    $Telefono = $objUsuario->getDatos()->getTelefono();
	    $Celular = $objUsuario->getDatos()->getcelunumero();
	    $Domicilio = $objDireccion->getDomicilio();
	    $Provincia = $objDireccion->getIdProvincia();
	    $Poblacion = $objDireccion->getPoblacion();
		if (!isset($Nombre) || empty($Nombre)) {
			 $DatosShippingCargados = false;
		} else if (!isset($Apellido) || empty($Apellido)) {
			$DatosShippingCargados = false;
		} else if (!isset($NroDoc) || empty($NroDoc)) {
			$DatosShippingCargados = false;
		} else if (!isset($Domicilio) || empty($Domicilio)) {
			$DatosShippingCargados = false;
		} else if (!isset($Provincia) || empty($Provincia)) {
			$DatosShippingCargados = false;
		} else if (!isset($Poblacion) || empty($Poblacion)) {
			$DatosShippingCargados = false;
		}
		
		return $DatosShippingCargados;
    }
    
    public static function verifyStock() {
    	$MiCarrito = Aplicacion::getCarrito();  
        $productosSinStock = array();
        $quitarDelCarrito = array();
        foreach($MiCarrito->getArticulos() as $IdCodigoProdInterno => $Articulo) {
        	$hayStock = dmProductos::hayStock($IdCodigoProdInterno, $Articulo["Cantidad"]);
        	
        	if(!$hayStock) {
        		$productosSinStock[] = $Articulo["IdProducto"];
        		$quitarDelCarrito[] = $IdCodigoProdInterno;
        	}
        }
        if(count($quitarDelCarrito) > 0) {
        	foreach($quitarDelCarrito as $productoAQuitar) {
	        	$MiCarrito->removeArticulo($productoAQuitar);
		        Aplicacion::setCarrito($MiCarrito);
		        
		    	if(count($MiCarrito->getArticulos()) == 0) {
		        	Aplicacion::VaciarCarrito();
		        }
        	}
        }
        return $productosSinStock;
    }
    
    public static function verifyStockReanudarPago($IdPedido) {
    	$MiCarrito = dmPedidos::getPedidoById($IdPedido);
        $productosSinStock = array();
        $quitarDelCarrito = array();
        foreach($Articulos = $MiCarrito->getProductos() as $Articulo) {
        	$hayStock = dmProductos::hayStock($Articulo->getIdCodigoProdInterno(), $Articulo->getCantidad());
        	
        	if(!$hayStock) {
        		return false;
        	}
        }
        
        return true;
    }
}

?>