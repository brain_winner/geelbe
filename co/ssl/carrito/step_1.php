<?
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/clientes/datamappers/dmclientes.php");
   		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
   		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/ssl/carrito/logic/CartUtils.php");
   		
        
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("mispedidos")); 
        
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/dhl/datamappers/dmdhl.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/datamappers/dmdescuentos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/vitrinas/clases/clsvitrinas.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/vitrinas/datamappers/dmvitrinas.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");

        $postURL = Aplicacion::getIncludes("post","micarrito_fincompra");
        $datosUser = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));
    }
    catch(exception $e) {
		header('Location: /'.$confRoot[1]);
		exit;
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1]);
		exit;
    }

    try {
        
        if(isset($_GET['IdPedido']) && is_numeric($_GET['IdPedido'])) {
	        $MiCarrito = dmPedidos::getPedidoById($_GET['IdPedido']);
			if($MiCarrito->getIDEstadoPedidos() == 0)
				$IdPedido = $_GET['IdPedido'];
				$_SESSION['lastPedidoId'] = $IdPedido;
		} else {
		    try {
		    	$MiCarrito = Aplicacion::getCarrito();
		    } catch(ACCESOException $e) {
		       header('Location: /co/front/vidriera/');
		       exit;
                       
		    } 
		}
        
        $objCampania = dmCampania::getByIdCampania($MiCarrito->getIdCampania());
        $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));

        if (!isset($_REQUEST["CART_RELOADING"]) || $_REQUEST["CART_RELOADING"] === false) {
	        $_SESSION["CREDITO_APLICADO_CARRITO"] = 0;
        }
        
        $PedidoReanudado = false;
	    if(isset($IdPedido)) {
			$PedidoReanudado = true;
	        $Articulos = $MiCarrito->getProductos();
	       	    
	        if(count($Articulos)==0) {
		       header('Location: /co/front/vidriera/');		     
		    }
		    
	    	// Verifico stock ..
			$verificoCorrectamenteStock = CartUtils::verifyStockReanudarPago($IdPedido);
			if(!$verificoCorrectamenteStock) {
		       	$_SESSION["ProductosSinStock"] = $productosSinStock;
		       	$Pedido = dmPedidos::getPedidoById($IdPedido);
		        header('Location: /'.$confRoot[1].'/ssl/carrito/no_stock_reanudar_pago.php?IdCampania='.$Pedido->getIdCampania());
				exit;
			}
	    } else {
	   	    $Articulos = $MiCarrito->getArticulos();
	   	    
	        if(count($Articulos)==0 || $MiCarrito->Expiro()) {
		      header('Location: /co/front/vidriera/');
                      exit;
		    }
		    
		        // Verifico stock ..
			$productosSinStock = CartUtils::verifyStock();
			if(is_array($productosSinStock) && count($productosSinStock) > 0) {
		       	$_SESSION["ProductosSinStock"] = $productosSinStock;
		        header('Location: /'.$confRoot[1].'/ssl/carrito/no_stock.php');
				exit;
			}
			
			//Descuento
	        if(isset($_GET['removeDiscount']))
        		$MiCarrito->setDescuentoId(null);
        
			if($objCampania->getDescuento() && $CreditosUtilizados == 0 && isset($_POST['discount'])) {
				
				$disc = dmDescuentos::getByCodigo($_POST['discount'], $MiCarrito->getIdCampania());
				if($disc === false)
					$errorDisc = true;
				else
					$MiCarrito->setDescuentoId($disc->getIdDescuento());
								
			}
	    }
	    
        $objDireccion = $objUsuario->getDirecciones(0);
       
    }
    catch(ACCESOException $e) {
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/'.$confRoot[1]);
		exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?=Aplicacion::getParametros("info", "nombre");?></title>
	
	<link href="<?="/".$confRoot[1]."/front/geelbe.css"?>" rel="stylesheet" type="text/css" />
	<link href="<?="/".$confRoot[1]."/front/botones.css"?>" rel="stylesheet" type="text/css" />
	<link href="<?="/".$confRoot[1]."/front/confirmacion/confirmacion.css"?>" rel="stylesheet" type="text/css" />
	<link href="<?="/".$confRoot[1]."/ssl/carrito/css/style.css"?>" rel="stylesheet" type="text/css" />
	<link href="<?="/".$confRoot[1]."/css/jquery.ui.spinner.css"?>" rel="stylesheet" type="text/css" />
    
	<script src="<?="/".$confRoot[1]."/front/confirmacion/js/js.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/js/jquery-1.4.2.min.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/js/jquery.ui.core.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/js/jquery.ui.spinner.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/js/jquery.tools.min.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/ssl/carrito/js/shipping_info.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/front/micarrito/js/js.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/front/registro/js/jquery.simplemodal.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/front/micuenta/js/getCPData.js"?>" type="text/javascript"></script>
	<?
	    Includes::Scripts(true);
	?>
	<script type="text/javascript">
		jQuery.isJson = function(str) {
			if (jQuery.trim(str) == '') return false;
			str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
			return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
		}
	</script>
	<style type="text/css">
		#header{
			  background-color:#EEE!important;
		}
	</style>

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background_carrito.php";?>
</head>
<body>

<!-- CONTENEDOR GENERAL -->
<div id="carr_contenedor">
	<div id="carr_logo"><img src="img/logo.jpg" width="254" height="97" /></div>
	<? require("../../front/menuess/menu_carrito.php"); ?>
		
	<!-- CONTENIDO GENERAL DEL CARRIO -->
	<div id="carr_cont_ecomm">
		<div id="carr_subcont_ecomm">
			<?php
    			if (isset($_REQUEST["error_type"]) && $_REQUEST["error_type"] == "no_payment_method_selected") { 
    				$msg = 'Debe seleccionar un m&eacute;todo de pago.';
    			} else if (isset($_REQUEST["error_type"]) && $_REQUEST["error_type"] == "invalid_email_or_password") { 
        			$msg = 'El Email y/o Contrase&ntilde;a ingresados son inv&aacute;lidos.';
    			} else if (isset($_REQUEST["error_type"]) && $_REQUEST["error_type"] == "no_credit") { 
        			$msg = 'No ten&eacute;s cr&eacute;dito suficiente en GeelbeCash para esta compra.';
    			} 
    			if(isset($msg))
    				echo '<script>msjError("'.$msg.'", "Error en el pago");</script>';
    		?>

			<div id="carr_tit_ppal" class="carr_tit_ppal">Detalle de la Compra</div>
			<div id="carr_subtitulo" class="carr_tit_campa">1. Resumen de tu compra en <?=$objCampania->getNombre();?></div>
			<div id="carr_subt_precio" class="carr_detail">Precio unitario</div>
			<div id="carr_subt_precio" class="carr_detail">Cantidad</div>
			<div id="carr_subt_precio" class="carr_detail">Total</div>
			<div id="carr_separador"></div>
				
			<?
				if(isset($IdPedido))
					include "includes/cart_info_reanudarpago.php";
				else
					include "includes/cart_info.php";
			?>

			<?php if(!isset($IdPedido) && $Descuento == 0): ?>
			<div id="cart-credits-selection">
			  <? include "includes/cart-credits-selection.php"; ?>
			</div>
			
			<?php if($objCampania->getDescuento() && $CreditosUtilizados == 0): ?>
			<form id="discount" action="" method="post">
				<div class="carr_tit_campa">Tengo un c&oacute;digo de descuento</div>
				<?php if(isset($errorDisc)): ?>
				<p class="error">El c&oacute;digo de descuento ingresado es inv&aacute;lido.</p>
				<?php endif; ?>
				<input type="text" name="discount" />
				<table cellspacing="0" cellpadding="0" border="1px" class="btn btnRed derecha chico M" style="float:right;">
				  <tbody>
				    <tr>
				      <td>
				        <a id="applyDiscounTrigger" href="javascript:$('#discount').submit()">
				          <strong>Aplicar</strong>
				        </a>
				      </td>
				    </tr>
				  </tbody>
				</table>
			</form>
			<?php endif; ?>
			<?php endif; ?>
			
			<div style="clear:both"></div>
			
			<!--ENVIO-->
			<div id="carr_envio_cont" class="complete">
				<div id="carr_envio_tit" class="carr_tit_campa">2. Datos de env&iacute;o</div>
				
	              <?
	        	$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	        	
	        	$in = $objCampania->getTiempoEntregaInCorte(mktime());
	        	$fn = $objCampania->getTiempoEntregaFnCorte(mktime());
	        	
	        	$mesFn = $meses[date("n", strtotime($in)) - 1];
				$mesIn = $meses[date("n", strtotime($fn)) - 1];
	              ?>
				<div id="fecha-entrega">
					<span style="font-weight:normal">Fecha de entrega:</span>
					<span class="destacado"><?=date("d", strtotime($in))." de ".$mesIn?> al <?=date("d", strtotime($fn))." de ".$mesFn?></span>
				</div>
              
                 <? 
                  	if(isset($IdPedido))
                  		include "includes/cart-shipping-info-reanudarpago.php";
                  	else
                  		include "includes/cart-shipping-info.php";
                  ?>

				<div style="clear:both"></div>

			</div>
			<!--FIN ENVIO-->
	
			<!-- PAGO -->
			<div id="carr_pago_cont">
				<div id="carr_pago_tit" class="carr_tit_campa">3. Forma de Pago</div>
			
				<? include "includes/cart-payment-info.php"; ?>
				
			</div>
			<!--FIN PAGO-->
	
			<div style="clear:both"></div>
			
			<? 
				if(isset($IdPedido))
					include "includes/cart_step_1_buttons_reanudarpago.php";
				else
					include "includes/cart_step_1_buttons.php";
			?>

			<div id="carr_footer"><img src="img/footer.jpg" height="52" /></div>
	
		</div>
	
	</div>
	<!-- FIN CONTENIDO GENERAL DEL CARRITO -->

</div>
<!--FIN CONTENEDOR GENERAL -->

<script type="text/javascript">
	$('#credform').hide();
	$('#usecredit').click(function() {
			if($('#credform:visible').size() > 0)
			  $('#credform').slideUp();
			else
				$('#credform').slideDown();
		});
		
	<?php if(isset($_GET['envio'])): ?>
	$(document).ready(function() {
		$('input.envio<?=$_GET['envio']?>').attr('checked', true).trigger('change')
	})
	<?php endif; ?>
</script>

</body>
</html>
