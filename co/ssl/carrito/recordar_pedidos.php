<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/dhl/datamappers/dmdhl.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");
		  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
		  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
		  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php"; 
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1]);
		exit;
    }
    
	$oConexion = new Conexion();
	$oConexion->Abrir_Trans();
	$oConexion->setQuery("
	SELECT p.IdPedido, p.IdCampania, u.NombreUsuario, u.IdUsuario
	FROM pedidos_temp p
	LEFT JOIN usuarios u ON u.IdUsuario = p.IdUsuario
	ORDER BY p.Fecha DESC
	LIMIT 20");
	$pedidos = $oConexion->DevolverQuery();
	$oConexion->Cerrar_Trans();
    
	foreach($pedidos as $i => $p) {
	
		//Validar campa�a abierta
		$oConexion = new Conexion();
		$oConexion->Abrir_Trans();
		$oConexion->setQuery("
		SELECT IdCampania
		FROM campanias
		WHERE FechaInicio <= NOW() AND FechaFin >= NOW() AND IdCampania = ".$p['IdCampania']);
		$validar = $oConexion->DevolverQuery();
		$oConexion->Cerrar_Trans();		
		
		if(count($validar) == 0)
			continue;
			
		//Validar existencia de pedido pago			
		$oConexion = new Conexion();
		$oConexion->Abrir_Trans();
		$oConexion->setQuery("
		SELECT IdPedido
		FROM pedidos
		WHERE IdEstadoPedidos = 2 AND IdUsuario = ".$p['IdUsuario']." IdCampania = ".$p['IdCampania']);
		$validar = $oConexion->DevolverQuery();
		$oConexion->Cerrar_Trans();		
		
		if(count($validar) > 0)
			continue;
	
		$emailBuilder = new EmailBuilder();
		$emailData = $emailBuilder->generateRecordarPedidoEmail($p['IdPedido'], $p['IdUsuario'], $p['NombreUsuario']);
		
		$emailSender = new EmailSender();
		$emailSender->sendEmail($emailData);
		
	}
    
	
?>