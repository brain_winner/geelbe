<?
    try
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/dhl/datamappers/dmdhl.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");
    }
    catch(exception $e)
    {
        die(print_r($e));
    }
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1]);
		exit;
    }
    
    if(isset($_REQUEST["IdCampania"])) {
    	$objCampania = dmCampania::getByIdCampania($_REQUEST["IdCampania"]);
    	$UrlBoton = "/".$confRoot[1]."/front/catalogo/index.php?IdCampania=".$objCampania->getIdCampania();
    	$MensajeBoton = "Volver a realizar la compra";
    	$TipoBoton = "XXXL";
    } else {
    	$UrlBoton = "/".$confRoot[1]."/front/vidriera/index.php";
    	$MensajeBoton = "Seguir comprando";
    	$TipoBoton = "XL";
    }
    $Mensaje = "Al intentar realizar el pedido, alguno de los productos se ha quedado sin stock, por lo que no podr&aacute; continuar con el mismo.";
    
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>

<link href="/<?=$confRoot[1]?>/front/micarrito/pago.css" rel="stylesheet" type="text/css" />
<link href="/<?=$confRoot[1]?>/front/geelbe.css" rel="stylesheet" type="text/css" />
<link href="/<?=$confRoot[1]?>/front/botones.css" rel="stylesheet" type="text/css" />

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body> 
<? require("../../front/menuess/menu_carrito.php"); ?>
<div id="container">
  <div id="contenido-top">
  </div>
    <div id="contenido">

	<div class="sinstock">
	  <div class="tit" style="text-transform:none!important;"><?=$Titulo?></div>
	  <br />
	  <div class="text"><?=$Mensaje?></div>
	  <br />
				<table cellspacing="0" cellpadding="0" border="1px" class="btn btnRed normal <?=$TipoBoton?>">
					<tbody>
						<tr>
							<td bordercolorlight="#a50000" bordercolordark="#a50000" bordercolor="#9595aa">
								<a href="<?=$UrlBoton?>"><strong><?=$MensajeBoton?></strong></a>
							</td>
						</tr>
					</tbody>
				</table>
	</div>
</div>
  <div id="contenido-bot" style="margin:0px 16px"></div>
</div>	
<? require("../../front/menuess/footer_carrito.php")?>
</body>
</html>
