<?php
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_transaction.php";
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_service.php");
    } catch(exception $e) {
        die(print_r($e));
    }

    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: http://'.$_SERVER['HTTP_HOST']."/".$confRoot[1]);
		exit;
    }
	
	$userId = Aplicacion::Decrypter($_SESSION["User"]["id"]);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?=Aplicacion::getParametros("info", "nombre");?></title>

	<link href="<?='/'.$confRoot[1].'/front/geelbe_new.css'?>" rel="stylesheet" type="text/css" />
	<link href="<?='/'.$confRoot[1].'/ssl/geelbecash/css/dinero-n-cuenta.css'?>" rel="stylesheet" type="text/css" />
	
	<script src="<?='/'.$confRoot[1].'/front/confirmacion/js/js.js'?>" type="text/javascript"></script>
	<script src="<?='/'.$confRoot[1].'/js/jquery-1.4.2.min.js'?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/js/jquery.tools.min.js"?>" type="text/javascript"></script>
	<script src="<?='/'.$confRoot[1].'/ssl/carrito/js/script.js'?>" type="text/javascript"></script>
	<script src="<?='/'.$confRoot[1].'/front/micarrito/js/js.js'?>" type="text/javascript"></script>
	
	<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background_ssl.php";?>
</head>
<body>
    <? require("../../front/menuess/menu_ssl.php") ?>
	<div id="container">
		<div id="geelbecash-header">
    		<img id="gb-logo" src="<?='/'.$confRoot[1].'/front/images/geelbecash.png'?>" />
    		<p id="gb-description">Con <strong>GCash</strong> puedes precargar dinero en tu cuenta, <strong>pagando en efectivo o transferencia bancaria</strong>, para luego utilizarlo en la compra de productoss.</p>
		</div>
		
		<br class="separator" />
        
            <div class="recuadro">
	        	<form action="<?="/".$confRoot[1]."/logica/geelbecash/actionform/comprargeelbecash.php"?>" name="CreditChargeForm" id="CreditChargeForm" method="POST"> 
					<div class="mediosdpago">
		        		<span class="titulo">Carga tu cuenta</span>
		            	<?php
		            	if(isset($_REQUEST["error_type"]) && $_REQUEST['error_type'] == "ammount_not_selected") {
		            		echo '<p class="error">Por favor seleccione un importe a cargar</p>';
		            	}
						?>

                        <div id="sel-amount">
	                        <div class="creditamount">
	                        <input type="radio" id="credit7" value="<?=GeelbeCashTransaction::$TRX_10000_ID?>" name="CreditCharge" onchange="javascript:highlightCreditCharge();enableNextButton();">
	                        <label for="credit7">
	                        <img align="top" src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/10k_off.jpg'?>" class="creditChargeImg" id="creditChargeImg7" onMouseOver="showCashCharge(this);" onMouseOut="hideCashCharge(this);">
	                        </label>
	                        </div>
	                        <div class="creditamount">
	                        <input type="radio" id="credit8" value="<?=GeelbeCashTransaction::$TRX_25000_ID?>" name="CreditCharge" onchange="javascript:highlightCreditCharge();enableNextButton();">
	                        <label for="credit8">
	                        <img align="top" src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/25k_off.jpg'?>" class="creditChargeImg" id="creditChargeImg8" onMouseOver="showCashCharge(this);" onMouseOut="hideCashCharge(this);">
	                        </label>
	                        </div>
                            <div class="creditamount">
                                <input type="radio" id="credit1" value="<?=GeelbeCashTransaction::$TRX_50000_ID?>" name="CreditCharge" onchange="javascript:highlightCreditCharge();enableNextButton();">
                                <label for="credit1">
                                <img align="top" src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/50k_off.jpg'?>" class="creditChargeImg" id="creditChargeImg1" onMouseOver="showCashCharge(this);" onMouseOut="hideCashCharge(this);">
                                </label>
                            </div>
                            <div class="creditamount">
                                <input type="radio" checked="checked" id="credit2" value="<?=GeelbeCashTransaction::$TRX_85000_ID?>" name="CreditCharge" onchange="javascript:highlightCreditCharge();enableNextButton();">
                                <label for="credit2">
                                <img align="top" src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/85k_on.jpg'?>" class="creditChargeImg" id="creditChargeImg2" onMouseOver="showCashCharge(this);" onMouseOut="hideCashCharge(this);">
                                </label>
                            </div>
                            <div class="creditamount">
                                <input type="radio" id="credit3" value="<?=GeelbeCashTransaction::$TRX_100000_ID?>" name="CreditCharge" onchange="javascript:highlightCreditCharge();enableNextButton();">
                                <label for="credit3">
                                <img align="top" src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/100k_off.jpg'?>" class="creditChargeImg" id="creditChargeImg3" onMouseOver="showCashCharge(this);" onMouseOut="hideCashCharge(this);">
                                </label>
                            </div>
                            <div class="creditamount">
                                <input type="radio" id="credit4" value="<?=GeelbeCashTransaction::$TRX_150000_ID?>" name="CreditCharge" onchange="javascript:highlightCreditCharge();enableNextButton();">
                                <label for="credit4">
                                <img align="top" src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/150k_off.jpg'?>" class="creditChargeImg" id="creditChargeImg4" onMouseOver="showCashCharge(this);" onMouseOut="hideCashCharge(this);">
                                </label>
                            </div>
                            <div class="creditamount">
                                <input type="radio" id="credit5" value="<?=GeelbeCashTransaction::$TRX_200000_ID?>" name="CreditCharge" onchange="javascript:highlightCreditCharge();enableNextButton();">
                                <label for="credit5">
                                <img align="top" src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/200k_off.jpg'?>" class="creditChargeImg" id="creditChargeImg5" onMouseOver="showCashCharge(this);" onMouseOut="hideCashCharge(this);">
                                </label>
                            </div>
                            <div class="creditamount">
                                <input type="radio" id="credit6" value="<?=GeelbeCashTransaction::$TRX_300000_ID?>" name="CreditCharge" onchange="javascript:highlightCreditCharge();enableNextButton();">
                                <label for="credit6">
                                <img align="top" src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/300k_off.jpg'?>" class="creditChargeImg" id="creditChargeImg6" onMouseOver="showCashCharge(this);" onMouseOut="hideCashCharge(this);">
                                </label>
                            </div>
                        </div>

		        	</div>
               		<div id="cta">
                    	<a id="submitLink" href="javascript:submitForm()"><img id="nextButton" src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/carga-ahora.jpg'?>" /></a>
                    </div>
	        	</form>
                
                <?php /*
               	<div id="logos-pago">
                    <span id="pago-autorizados">M&eacute;todos de pago autorizados</span>
                    <img style="margin:10px auto 0;" src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/metodos-pago2.jpg'?>" />
                </div>
                */ ?>
        </div>

        	<div class="unfloat"></div>

        	<div class="recuadro col_izquierda segunda_fila">
	        		<span class="titulo">Resumen de uso <a class="header-help-link" href="javascript:;" id="resumendeusoTooltipTrigger">&#91;&#63;&#93;</a></span>
	        		<div class="tooltip">
						<p style="position:relative;top:-5px">Elige el monto que quieras cargar, y luego haz clic en el bot&oacute;n &ldquo;&iexcl;Carga ahora&#33;&rdquo;. En la siguiente pantalla podr�s elegir el medio de pago.</p>
					</div>
	        		<?php 
	        		$estadoGeelbeCash = GeelbeCashService::getEstadoGeelbeCash($userId);
	        		?>
                    <ul id="resumen-uso">
                    	<li>
                        	<div class="col_izquierda item">&Uacute;ltimo pago</div>
                            <div class="col_derecha numerico value">$<?=number_format(($estadoGeelbeCash->getMontoUltimoGasto())?$estadoGeelbeCash->getMontoUltimoGasto():0, 2, ',', '.')?></div>
                        </li>
                    	<li>
                        	<div class="col_izquierda item">Utilizado hist&oacute;rico</div>
                            <div class="col_derecha numerico value">$<?=number_format(($estadoGeelbeCash->getMontoGastosHistoricos())?$estadoGeelbeCash->getMontoGastosHistoricos():0, 2, ',', '.')?></div>
                        </li>
                    	<li>
                        	<div class="col_izquierda item">Cargado hist&oacute;rico</div>
                            <div class="col_derecha numerico value">$<?=number_format(($estadoGeelbeCash->getMontoCargasHistoricas())?$estadoGeelbeCash->getMontoCargasHistoricas():0, 2, ',', '.')?></div>
                        </li>
                    	<li style="border:none;">
                        	<div id="subtotal">
                        	<div class="col_izquierda item">Saldo actual</div>
                            <div class="col_derecha numerico value">$<?=number_format(($estadoGeelbeCash->getMontoSaldoActual())?$estadoGeelbeCash->getMontoSaldoActual():0, 2, ',', '.')?></div>
	                        <div class="unfloat"></div><strong></strong>
                            </div>
                        </li>
                    </ul>
                    
        	</div>
        	
        	<div class="col_derecha recuadro segunda_fila">
	        		<span class="titulo">Cargas pendientes <a class="header-help-link" href="javascript:;" id="cargaspendientesTooltipTrigger">&#91;&#63;&#93;</a></span>
	        		<div class="tooltip">
						<p>Son las cargas que a&uacute;n no se han acreditado en tu GCash.</p>
					</div>
                    
                    <?php
                    	$cargasPendientes = GeelbeCashService::getCargasPendientes($userId); 
                    ?>
                    <div id="pendientes-container" <?=(count($cargasPendientes) > 4)?'class="scrolling"':''?>>
	                    <table id="pendientes" cellspacing="0" cellpadding="3px" width="100%">
	                    	<thead>
	                        	<th>Fecha</th>
	                        	<th>Nro. de operaci&oacute;n</th>
	                        	<th class="ultima">Monto</th>
	                        </thead>
	                        <?
								foreach ($cargasPendientes as $cargaPendiente) {
									echo "<tr>";
									echo "  <td class='centrado'>".date("d/n/Y",strtotime($cargaPendiente->getFecha()))."</td>";
									echo "  <td>".$cargaPendiente->getId()."</td>";
									echo "  <td class='centrado'>$".number_format($cargaPendiente->getMonto(), 0, ',', '.')."</td>";
									echo "</tr>";
								}
	                        ?>
	                    </table>
                    </div>
			</div>
  	</div>  
    <? require("../../front/menuess/footer_ssl.php");?>
  </body>
  <script type="text/javascript">

  var nextButtonDisabled = true;
  function enableNextButton() {
	if ($("input[@name='CreditCharge']:checked").val() != null) {
	    var selectedCreditCharge = parseInt($("input[@name='CreditCharge']:checked").val());
	    if(selectedCreditCharge > 0 && selectedCreditCharge < 10) {
			nextButtonDisabled = false;
			$('#nextButton').removeClass("disabled");
	    } else {
			nextButtonDisabled = true;
			$('#nextButton').addClass("disabled");
	    }
			
	} else {
		nextButtonDisabled = true;
		$('#nextButton').addClass("disabled");
	}
  }

  var formSubmitted = false;
  function submitForm() {
	  if(!formSubmitted) {
		  formSubmitted = true;
		  $('#CreditChargeForm').submit();
	  }
  }

  var sources = new Object();
    sources["<?=GeelbeCashTransaction::$TRX_10000_ID?>"] = "<?="/".$confRoot[1]."/ssl/geelbecash/img/10k" ?>";
    sources["<?=GeelbeCashTransaction::$TRX_25000_ID?>"] = "<?="/".$confRoot[1]."/ssl/geelbecash/img/25k" ?>";
  sources["<?=GeelbeCashTransaction::$TRX_50000_ID?>"] = "<?="/".$confRoot[1]."/ssl/geelbecash/img/50k" ?>";
  sources["<?=GeelbeCashTransaction::$TRX_85000_ID?>"] = "<?="/".$confRoot[1]."/ssl/geelbecash/img/85k" ?>";
  sources["<?=GeelbeCashTransaction::$TRX_100000_ID?>"] = "<?="/".$confRoot[1]."/ssl/geelbecash/img/100k" ?>";
  sources["<?=GeelbeCashTransaction::$TRX_150000_ID?>"] = "<?="/".$confRoot[1]."/ssl/geelbecash/img/150k" ?>";
  sources["<?=GeelbeCashTransaction::$TRX_200000_ID?>"] = "<?="/".$confRoot[1]."/ssl/geelbecash/img/200k" ?>";
  sources["<?=GeelbeCashTransaction::$TRX_300000_ID?>"] = "<?="/".$confRoot[1]."/ssl/geelbecash/img/300k" ?>";

  function highlightCreditCharge() {
	$.each($(".creditChargeImg"), function(i, e) {
		var src = $("#"+e.id).attr("src").replace("on", "off");
		$("#"+e.id).attr("src", src);
	});

	if($("input[@name='CreditCharge']:checked").val() != null) {
		var imgLabel = "creditChargeImg" + $("input[@name='CreditCharge']:checked").val();
		var src = $("#"+imgLabel).attr("src").replace("off", "on");
		$("#"+imgLabel).attr("src", src);
	}
  }

  function showCashCharge(img) {
	  var src = $("#"+img.id).attr("src").replace("off", "on");
	  $("#"+img.id).attr("src", src);
  }

  function hideCashCharge(img) {
  	  var selectedCreditCharge = img.id.substring(15);
	  var creditChargeChecked = $("input[@name='CreditCharge']:checked").val();
	  if(creditChargeChecked == null || creditChargeChecked == undefined || creditChargeChecked == "" || creditChargeChecked != selectedCreditCharge) {
		  var src = $("#"+img.id).attr("src").replace("on", "off");
		  $("#"+img.id).attr("src", src);
	  }
  }

  $(document).ready(function() {
  	$("#resumendeusoTooltipTrigger").tooltip({predelay:200});
  	$("#cargaspendientesTooltipTrigger").tooltip({predelay:200});
  });
  </script>
</html>
