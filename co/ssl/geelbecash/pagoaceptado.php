<?
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	
    try {
        ValidarUsuarioLogueado();
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/mailer/clsMailer.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/dhl/datamappers/dmdhl.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");
	} catch (Exception $e) {
		header("Location: http://".$_SERVER['HTTP_HOST']."/".$confRoot[1]);
		exit;
    }
	
	if(isset($_SESSION['lastPedidoId'])) {
		$arrDatos = dmPedidos::getDatosMailByIdPedido($_SESSION['lastPedidoId']);
		$usuario = dmUsuario::getByIdUsuario($arrDatos['IdUsuario']);
		$pedido = dmPedidos::getPedidoById($_SESSION['lastPedidoId']);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

		<link href="<?='/'.$confRoot[1].'/front/geelbe_new.css'?>" rel="stylesheet" type="text/css" />
		<link href="<?='/'.$confRoot[1].'/ssl/carrito/css/cart.css'?>" rel="stylesheet" type="text/css" />
   		<link href="<?='/'.$confRoot[1].'/ssl/geelbecash/css/dinero-n-cuenta.css'?>" rel="stylesheet" type="text/css" />
		<link href="<?='/'.$confRoot[1].'/front/botones.css'?>" rel="stylesheet" type="text/css" />
		
		<? include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background_ssl.php"; ?>
	</head>
	<body> 
	
		<? require("../../front/menuess/menu_ssl.php") ?>
		<div id="container">
    	<img src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/GeelbeCash_header-width700px.jpg'?>" id="head-gc" />

            <div class="unfloat" style="text-align:left;">
			<div id="gracias">				
				<span class="titulo">Gracias por comprar en Geelbe</span><br />
				<p>En unos instantes recibir&aacute;s un email confirmando que se ha procesado correctamente tu pago.</p>
				</br><br>
				<table class="btn btnRed centro grande M" cellpadding="0" cellspacing="0" border="1px"> 
					<tr>
						<td><a href="http://<?=$_SERVER['HTTP_HOST']."/".$confRoot[1]?>/front/vidriera/"><strong>Ir a la vitrina</strong></a></td>
					</tr>
				</table>
                </div>
			</div>
		</div>
		<? require("../../front/menuess/footer_ssl.php");?>
		
		<?php if(isset($_SESSION['lastPedidoId'])): ?>
		<!-- MentAd Code for Purchase Notification -->
		<script type="text/javascript">
		
		    var mentad_website_id = "A0ElefGOE4_s"; // Unique ID supplied by MentAd
		    var mentad_purchase_total = "<?=$arrDatos['total']?>"; // The total purchase price as a number. For example: 123.45
		    var mentad_purchase_currency = "COP"; // The 3 letter currency code. For example: USD, EUR, ARS, NIS
		    var mentad_purchase_order_id = "<?=$_SESSION['lastPedidoId']?>"; // The unique order ID. For example: "AB-123456"
		    
		    var mentad_purchase_items = [];
		    // Iterate over the items in the cart and add them to the "mentad_purchase_items" array
		    // i.e. for (var item in cart) {
		    
		    <?php foreach($pedido->getProductos() as $prod):
		    		$p = dmProductos::getByIdCodigoProdInterno($prod->getIdCodigoProdInterno());
		    		$p = dmProductos::getById($p->getIdProducto());
		    ?>
		      mentad_purchase_items.push({ 
		          sku: "<?=$p->getIdProducto()?>", // The unique product ID. For example: "AB-00001234"
		          price: "<?=$prod->getPrecio()?>", // The price of one item as a number. For example: 12.34
		          quantity: "<?=$prod->getCantidad()?>", // The amount of items of this type. For example: 2
		          name: "<?=$p->getNombre()?>", // The name of this item (used for display). For example: "Kindle 3G"
		          image_url: "http://geelbe.com<?="/".$confRoot[1]."/front/productos/productos_".$p->getIdProducto()."/imagen_1.jpg"?>" // A URL of an image of this item (used for display). For example: "http://a.com/img/1234.jpg"
		      });
		    <?php endforeach; ?>
		    
		    //}
		    
		    // Add the details of the shopper for data mining purposes
		    var mentad_shopper_info = {
		        title: "<?=($usuario->getDatos()->getIdTratamiento() == 1 ? 'M' : 'F');?>", // The title of the purchaser. For example: "Mr"
		        first_name: "<?=$arrDatos['nombreUsuario']?>", // The first name of the purchaser. For example: "John"
		        last_name: "<?=$arrDatos['apellidoUsuario']?>", // The last name of the purchaser. For example: "Doe"
		        birthdate: "<?=date("d-m-Y", strtotime($usuario->getDatos()->getFechaNacimiento()))?>", // The birthdate of the purchaser in DD-MM-YYYY format. For example: "31-01-1970"
		        email: "<?=$arrDatos['emailUsuario']?>", // The email of the purchaser. For example: "a@b.com"
		        phone: "<?=$usuario->getDatos()->getTelefono()?>", // The phone number of the purchaser. For example: +123-4-5678901
		        mobile: "<?=$usuario->getDatos()->getCeluCodigo()?> <?=$usuario->getDatos()->getCeluNumero()?>" // The mobile phone number of the purchaser. For example: +123-4-5678901
		    };
		    
		    // Trigger the purchase notification asynchronously
		    (function(){
				var js, id = 'mentad-notification'; if (document.getElementById(id)) {return;}
				js = document.createElement('script'); js.id = id; js.async = true;
				js.src = "//mentad.com/static/js/mentad-purchase-notification.js";
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(js, s);
			})();
			
		</script>
		<?php endif; ?>
	</body>
</html>
