<?php
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    } catch(exception $e) {
        die(print_r($e));
    }

    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: http://'.$_SERVER['HTTP_HOST']."/".$confRoot[1]);
		exit;
    }
	
	$userId = Aplicacion::Decrypter($_SESSION["User"]["id"]);
	
	if (isset($_GET['estado_pol']) && $_GET['estado_pol'] == 6) {
		header("Location: /".$confRoot[1]."/ssl/geelbecash/post-pago-err.php");
		exit;
	}
	
	if (isset($_GET['estado_pol']) && $_GET['estado_pol'] == 5) {
		header("Location: /".$confRoot[1]."/ssl/geelbecash/");
		exit;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?=Aplicacion::getParametros("info", "nombre");?></title>

	<link href="<?='/'.$confRoot[1].'/front/geelbe_new.css'?>" rel="stylesheet" type="text/css" />
	<link href="<?='/'.$confRoot[1].'/ssl/geelbecash/css/dinero-n-cuenta.css'?>" rel="stylesheet" type="text/css" />
	
	<script src="<?='/'.$confRoot[1].'/front/confirmacion/js/js.js'?>" type="text/javascript"></script>
	<script src="<?='/'.$confRoot[1].'/js/jquery-1.4.2.min.js'?>" type="text/javascript"></script>
	<script src="<?='/'.$confRoot[1].'/ssl/carrito/js/script.js'?>" type="text/javascript"></script>
	<script src="<?='/'.$confRoot[1].'/front/micarrito/js/js.js'?>" type="text/javascript"></script>
	
	<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background_ssl.php";?>
</head>

<body>
    <? require("../../front/menuess/menu_ssl.php") ?>

	<div id="container">
    	<img src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/GeelbeCash_header-width700px.jpg'?>" id="head-gc" />

    	<div id="gracias">
        	<span class="titulo">Ya completaste la operaci&oacute;n</span>
        	<p>En este momento, tu pago est&aacute; siendo procesado,<br />y ser&aacute; confirmado a la brevedad.</p>
			<a id="continuar" href="<?='http://'.$_SERVER['HTTP_HOST'].'/'.$confRoot[1]?>"><img src="<?='/'.$confRoot[1].'/ssl/geelbecash/img/continuar_comprando.jpg'?>" /></a>
        </div>
    </div>
    <? require("../../front/menuess/footer_ssl.php");?>
</body>
</html>
