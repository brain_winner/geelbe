<html>
	<?
		require_once("../conf/configuracion.php");


		session_start();

		/*if($_SESSION["partners"]["Username"]!="geelbe"){
		redirect("index.php");
		exit();
		}
*/	
	
		if($_POST["encriptar"]){
			$claveEncrip = "La clave encriptada es: ".Aplicacion::Encrypter($_POST["encriptar"]);
		}
		
		try{
			if($_POST["desencriptar"]){
			
				$claveDesencrip = "La clave Desencriptada es: ".Aplicacion::Decrypter($_POST["desencriptar"]);
			}
		}catch(exception $e){
		
			$error = "Ocurrio un error en la desencriptacion";
		}

	?>
		
	<head>
		<title>Encrypter</title>
		<style>
			*{ font-family: Verdana; }
			body{ width: 800px; margin: 0px auto; border: 1px solid lightgray; padding: 10px; }
			body:hover{ border-color: lightgreen;}
			h1{ text-align: center; font-size: 14px;}
			p{ text-align: justify; font-size: 11px;}
			div{ border-bottom: 1px dotted gray;}
			.error{ color: red; }
			input{ font-size: 11px; color: green;}
			a{color: green;}
			a:hover{color: lightgreen;}
			
		</style>
	</head>
	
	<body>
		<div><h1 class="error"><? echo ($error); ?></h1></div>
		
		<div>
			<p>Ingrese la clave a encriptar en el siguiente campo de texto</p>
			<form method="post">
				<input type="text" name="encriptar" />
				<input type="submit" value="encriptar" />
			</form>
			<h1><?echo ($claveEncrip);?></h1>
		</div>
		
		<div>
			<p>Ingrese la clave a desencriptar en el siguiente campo de texto</p>
			<form method="post">
				<input type="text" name="desencriptar" />
				<input type="submit" value="desencriptar" />
			</form>
			<h1><?echo ($claveDesencrip);?></h1>
		</div>
		
		<div><p>Para realiazar una nueva consulta haga <a href="encrypter.php">click aqui</a></p></div>
		
		
	<body>

</html>
