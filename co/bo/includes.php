<?
    class Includes
    {
        public static function Scripts($ruta=false)
        {
            ?>
            <link rel="icon" type="image/png" href="<?=UrlResolver::getImgBaseUrl('favicon.png');?>" />
            
            <script src="<?=UrlResolver::getJsBaseUrl("logica/scripts/AC_RunActiveContent.js");?>" type="text/javascript"></script>
            <script src="<?=UrlResolver::getJsBaseUrl("logica_bo/scripts/global_msj.js");?>" type="text/javascript"></script>
            <script src="<?=UrlResolver::getJsBaseUrl("logica_bo/scripts/funcionesgenericas.js");?>" type="text/javascript"></script>
            <script type="text/javascript">
                var DIRECTORIO_URL_BO = "<?=Aplicacion::getRootUrl() . Aplicacion::getDirBackOffice()?>";
                var DIRECTORIO_URL = "<?=Aplicacion::getRootUrl()?>";
            </script>
            <?
        }
    }
    
?>