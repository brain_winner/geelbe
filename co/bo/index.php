<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("historico"));
    $postURL = Aplicacion::getIncludes("post", "usuarios");
    try
    {
        ValidarUsuarioLogueadoBo(0);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/login/','_self');</script>
    <?
        exit;
    }    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?></title>
</head>
<body>
    <table style="width:100%; height:100%">
        <tr>
            <td colspan="100%">
            <img src="<?=UrlResolver::getImgBaseUrl('bo/imagenes/logo_'.$confRoot[1].'.png');?>" border="0"/>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width:180px;">
                <?require_once("menu/index.php")?>
            </td>
            <td valign="top" style="width:100%;height:820px;">
                <iframe style="width:100%;height:100%;border:none" src="" frameborder="0" id="iPrincipal" name="iPrincipal"></iframe>
            </td>
        </tr>
            
    </table>
</body>
</html>
