<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("bonos2"));
		$postURL = Aplicacion::getIncludes("post", "bonos2");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(29);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }	
	try {
		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array('U.NombreUsuario', 'DUP.Apellido', 'DUP.Nombre'));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("B.`A.FechaCaducidad`", "DESC");

		//$count = dmBonos::getBonosCount($paramArray);
		$count = 0;
		$listPager = new ListadoPaginator($count);
		//$rows = dmBonos::getBonosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());
		$rows = array();  
	
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Bonos</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Bonos</h1>	
			
			<div id="mainStats">
				<table style="margin-bottom:10px;">
					<tr class="alt">
						<td style="text-align:left;font-weight:bold;">Nuevo: <a href="/<?=$confRoot[1];?>/bo/bonos2/ABM.php">Nuevo Bono</a></td>
					</tr>
				</table>
			</div>

			<?php
				$listadoPrinter = new ListadoPrinter($rows, "index.php", $listPager);
				$listadoPrinter->showColumn("B.NombreUsuario", "E-Mail");
				$listadoPrinter->showColumn("B.Apellido", "Apellido");
				$listadoPrinter->showColumn("B.Nombre", "Nombre");
				$listadoPrinter->showColumn("B.TR.Descripcion", "Bono obtenido por");
				$listadoPrinter->showColumn("B.FechaCaducidad", "Fecha de Caducidad");
				$listadoPrinter->showColumn("B.Aplica1", "Valor");
				$listadoPrinter->showColumn("B.Estado", "Estado");
				
				$listadoPrinter->showColumn("Acciones");

				$listadoPrinter->setShowFilter("B.Aplica1", false);
				$listadoPrinter->setShowFilter("B.FechaCaducidad", false);
				$listadoPrinter->setShowFilter("B.Estado", false);
				$listadoPrinter->setShowFilter("B.TR.Descripcion", false);
				$listadoPrinter->setShowFilter("Acciones", false);
				
				$listadoPrinter->setFilterName("B.NombreUsuario", "U.NombreUsuario");
				$listadoPrinter->setFilterName("B.Apellido", "DUP.Apellido");
				$listadoPrinter->setFilterName("B.Nombre", "DUP.Nombre");
				
				$listadoPrinter->setSortColumn("B.Aplica1", false);
				$listadoPrinter->setSortColumn("B.Estado", false);
				$listadoPrinter->setSortColumn("B.TR.Descripcion", false);
				$listadoPrinter->setSortColumn("Acciones", false);
				
				$listadoPrinter->setSortName("B.NombreUsuario", "B.`A.NombreUsuario`");
				$listadoPrinter->setSortName("B.Apellido", "B.`A.Apellido`");
				$listadoPrinter->setSortName("B.Nombre", "B.`A.Nombre`");
				$listadoPrinter->setSortName("B.FechaCaducidad", "B.`A.FechaCaducidad`");
				
				$listadoPrinter->linkColumn("B.NombreUsuario", "/".$confRoot[1]."/bo/clientes/Ver.php?IdUsuario=[[B.IdUsuario]]", "Ver Cliente");
								
				$listadoPrinter->addButtonToColumn("Acciones", "<a href=\"/".$confRoot[1]."/logica_bo/bonos2/actionform/eliminar_bono.php?IdBono=[[B.IdBono]]&IdUsuario=[[B.IdUsuario]]\">Borrar</a>");

				$listadoPrinter->printListado();
			?>
		</div>	
	</body>
</html>
