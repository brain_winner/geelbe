<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    	Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
    	
		$POST = Aplicacion::getIncludes("post", "pedidos");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(14);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }	
    
    if(!isset($_REQUEST['clearFilterButton'])) {
		$paramArray = ListadoUtils::generateFiltersParamArray(array('ME.idmotivo', 'ME.email', 'ME.mensaje', 'ME.IdCampania', 'U.NombreUsuario'));
	} else {
		$paramArray = array();
	}
	
	$sortArray = ListadoUtils::generateSortParamArray("ME.idMensaje", "DESC");
	
	if($_REQUEST["messageType"]) {
		if ($_REQUEST["messageType"] == 'open') {
			$count = dmMensajes::getMensajesUsuariosAbiertosCount($paramArray);
			$listPager = new ListadoPaginator($count);
			$rows = dmMensajes::getMensajesUsuariosAbiertosPaginados($paramArray, $sortArray, 0, $count);  
		}
	    else if ($_REQUEST["messageType"] == 'asign') {
			$count = dmMensajes::getMensajesUsuariosAbiertosAsignadosCount($paramArray);
			$listPager = new ListadoPaginator($count);
			$rows = dmMensajes::getMensajesUsuariosAbiertosAsignadosPaginados($paramArray, $sortArray, 0, $count); 
		}
	    else if ($_REQUEST["messageType"] == 'noasign') {
			$count = dmMensajes::getMensajesUsuariosAbiertosSinAsignarCount($paramArray);
			$listPager = new ListadoPaginator($count);
			$rows = dmMensajes::getMensajesUsuariosAbiertosSinAsignarPaginados($paramArray, $sortArray, 0, $count);  
		}
	    $parametro = "?messageType=".$_REQUEST["messageType"];
	}
	
	foreach ($rows as $aRow) {
		dmMensajes::asignUser($aRow["ME.idMensaje"], $_REQUEST["userToAsign"]);
	}
	
	header('Location: /'.$confRoot[1].'/bo/mensajes/index.php');
	exit;
	
?>