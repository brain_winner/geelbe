<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias")); 
    
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/historico/datamappers/dmhistoricologin.php");
    
    try {
        ValidarUsuarioLogueadoBo(14);
    }
    catch(ACCESOException $e) {
	    ?>  
	        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
	    <?
        exit;
    }     
    
    $POST = Aplicacion::getIncludes("post", "mensajes");
    
    if ($_REQUEST["mchange"] && $_REQUEST["idMensaje"] && $_REQUEST["txtMotivo"]){
    	if($_REQUEST["mchange"] == 'true') {
    		$objMensaje = new Mensajes();
    		$objMensaje = dmMensajes::getById($_REQUEST["idMensaje"]);
    		$objMensaje->setidmotivo($_REQUEST["txtMotivo"]);
    	    if (is_null($objMensaje->getboUserId())) {
            	$objMensaje->setboUserId("null");
            }
    		dmMensajes::Update($objMensaje);
    	}
    }
    
    if ($_REQUEST["cchange"] && $_REQUEST["idMensaje"] && $_REQUEST["txtCampania"]){
    	if($_REQUEST["cchange"] == 'true') {
    		$objMensaje = new Mensajes();
    		$objMensaje = dmMensajes::getById($_REQUEST["idMensaje"]);
    		$objMensaje->setIdCampania($_REQUEST["txtCampania"]);
    	    if (is_null($objMensaje->getboUserId())) {
            	$objMensaje->setboUserId("null");
            }
    		dmMensajes::Update($objMensaje);
    	}
    }
    
    $objMensaje = new Mensajes();
    if($_REQUEST["idMensaje"]){
        $objMensaje = dmMensajes::getById($_REQUEST["idMensaje"]);
    }
    
    $idUsuario = $objMensaje->getidUsuario();
    $objCliente = dmCliente::getByIdUsuario($idUsuario); 
    
    $Motivo = dmMensajes::getMotivoById($objMensaje->getidMotivo());
    
    if ($objMensaje->getIdCampania()==-2) {
    	$NombreCampania = 'Sin Campa&ntilde;a';
    }
    else if ($objMensaje->getIdCampania()==-1) {
    	$NombreCampania = 'Otra Campa&ntilde;a';
    }
    else {
    	$Campania = dmCampania::getMarcaById($objMensaje->getIdCampania());
    	$NombreCampania = $Campania[0]['Nombre'];
    }
		
	$oConexion = Conexion::nuevo();
	$oConexion->setQuery("SELECT FechaFin FROM campanias WHERE IdCampania = ".$objMensaje->getIdCampania());
	$Resultado = $oConexion->DevolverQuery();
	$FechaFin = date("d-m-Y H:i:s", strtotime($Resultado[0]["FechaFin"]));
	
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Informaci&oacute;n del Mensaje</title>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/js/protoaculous.1.8.1.js")?>"  type="text/javascript"></script>	    
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/js/autosubmit.js")?>"  type="text/javascript"></script>	    
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/js/tablesort.js")?>"  type="text/javascript"></script>	    
		<script src="<?=UrlResolver::getJsBaseUrl("bo/js/autotablesort.js")?>"  type="text/javascript"></script>
        <script src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.2.min.js");?>" type="text/javascript"></script>
		
    	<script src="<?=UrlResolver::getJsBaseUrl("registro/js/gen_validatorv31.js")?>" type="text/javascript"></script>
	</head>
	
	<body>
		<div id="container">
			<?php 
				if ($objMensaje->getidMotivo() == 71) {
			?>
			<h1>Mensaje <?=$objMensaje->getidMensaje()?> del no-socio <?=$objMensaje->getemail()?></h1>
			<?php } else { ?>
			<h1>Mensaje <?=$objMensaje->getidMensaje()?> del cliente <?=$objCliente->getNombreUsuario()?></h1>
			<?php } ?>
		    <div class="stats">
		    <div class="column">
			  	<h2>Informaci&oacute;n del Mensaje</h2>
			  	<table>
			    	<tr>
						<th width="25%">Id</th>
						<td width="75%"><?=$objMensaje->getidMensaje()?></td>
					</tr>
					<?php if ($objMensaje->getidMensajePadre() != 'NULL' && $objMensaje->getidMensajePadre() != 'null') { ?>
					<tr class="alt">
						<th>Id Mensaje Padre</th>
						<td><a href="<?="/".$confRoot[1]."/bo/mensajes/leer.php?idMensaje=".$objMensaje->getidMensajePadre()?>"><?=$objMensaje->getidMensajePadre()?></a></td>
					</tr>
					<?php } elseif ($objMensaje->getidMotivo() == 71) { ?>
					<tr class="alt">
						<th>Cliente</th>
						<td><? echo $objMensaje->getemail(); ?></td>
					</tr>
					<?php
						} else {
					?>
					<tr class="alt">
						<th>Cliente</th>
						<td><a href="<?="/".$confRoot[1]."/bo/clientes/Ver.php?IdUsuario=".$idUsuario?>"><?=$objCliente->getNombreUsuario()?></a></td>
					</tr>
					<?php } ?>
					<tr>
						<th>Fecha</th>
						<td><?=$objMensaje->getFecha()?></td>
					</tr>
					<tr class="alt">
						<th>Motivo</th>
						<td>
                        <div id="motivoShow">
                        <?=$Motivo;?> &nbsp; <a id="motivoChangeLink" style="font-size:10px;color:red;" href="javascript:;">[cambiar]</a>
                        
                        </div>
                        <div id="motivoChange" style="display:none:">
                        	<form id="motivoForm" method="post">
                        		<input type="hidden" id="idMensaje" name="idMensaje" value="<?=$objMensaje->getidMensaje()?>" />
	                        	<input type="hidden" id="mchange" name="mchange" value="true" />
	                        	<select name="txtMotivo" id="txtMotivo">
	                        		<?php 
    									$MensajesMotivos = dmMensajes::getMotivos();
										foreach ($MensajesMotivos as $unMotivo) {
									?>
											<option value="<?=$unMotivo["IdMotivo"];?>" <?=$unMotivo["IdMotivo"]==$objMensaje->getidMotivo()?'selected="selected"':'';?> ><?=$unMotivo["Descripcion"];?></option>
									<?php
										}
									?>
			          			</select>
		          			</form>
		          			</div>
		          		</td>
					</tr>
					<tr>
						<th>Campa&ntilde;a</th>
						<td>
						<div id="campaniaShow">
                        	<?=$NombreCampania;?> &nbsp; <a id="campaniaChangeLink" style="font-size:10px;color:red;" href="javascript:;">[cambiar]</a>
                        </div>
						<div id="campaniaChange" style="display:none:">
                        	<form id="campaniaForm" method="post">
                        		<input type="hidden" id="idCampania" name="idCampania" value="<?=$objMensaje->getIdCampania();?>" />
	                        	<input type="hidden" id="cchange" name="cchange" value="true" />
	                        	<select name="txtCampania" id="txtCampania">
	                        	    <option value="-2" <?='Sin Campa&ntilde;a'==$NombreCampania?'selected="selected"':'';?> ><?='Sin Campa&ntilde;a';?></option>
									<option value="-1" <?='Otra Campa&ntilde;a'==$NombreCampania?'selected="selected"':'';?> ><?='Otra Campa&ntilde;a';?></option>
									
                        	 		<?php 
	                        			$MensajesCampanias = dmCampania::getMarcas();
										foreach ($MensajesCampanias as $unaCampania) {
									?>
											<option value="<?=$unaCampania["IdCampania"];?>" <?=$unaCampania["Nombre"]==$NombreCampania?'selected="selected"':'';?> ><?=$unaCampania["Nombre"];?></option>
									<?php
										}
									?>
			          			</select>
		          			</form>
		          		</div>
						</td>
							<?
							if ($objMensaje->getIdCampania() > 1){
							$EstadoCampanias = dmCampania::EsCampaniaAccesible($objMensaje->getIdCampania());
								if ($EstadoCampanias  == False) {
									$EstadoFecha = "Cerrado";
								} else {
									$EstadoFecha = "Vigente";
								}
								echo 
								"<tr class='alt'>
									<th>Estado</th><td>".$EstadoFecha." - ".$FechaFin."</td>
								</tr>";
							}
							?>
					<?php if ($objMensaje->getboUserId()) {
						$objClienteBO = dmCliente::getByIdUsuario($objMensaje->getboUserId());
					?>
					<tr class="alt">
						<th>Cerrado por</th>
						<td><?=$objClienteBO->getNombreUsuario()?>
						</td>
					</tr>
					<?php } ?>
				</table>
				<table>
                	<?
                    $dtMensajes = dmMensajes::getConversacion($objMensaje->getidMensaje());
                    foreach($dtMensajes as $Mensaje) {
                    	if ($Mensaje["boUserId"]) {
                    		$objClienteBOr = dmCliente::getByIdUsuario($Mensaje["boUserId"]);
                    	}
                    ?>
                        <tr <?=$Mensaje["idMensajePadre"]? "class=\"alt\"" : ""?>>
                            <th width="25%;">
                            <?php 
                            if ($Mensaje["idMensajePadre"]) {
                            	echo "Respuesta:";
                    			if ($Mensaje["boUserId"]) {
                    				echo "<br/><span style=\"font-weight:normal;color:black;\">".$objClienteBOr->getNombreUsuario()."</span><br/><span style=\"font-weight:normal;color:black;\">".$Mensaje["Fecha"]."</span>";
                    			}
                            }
                            else {
                            	echo "Mensaje";
                            }
                            ?>
                            </th>
                            <td width="75%;"><? if(!$Mensaje["idMensajePadre"]) { 
                            	                	echo $objMensaje->getasunto();
                            	                	if (strlen($objMensaje->getasunto())>0) {
                            	                		echo "<br />";
                            	                	}
                                                }
                                             ?>
                            <?=stripcslashes(nl2br($Mensaje["mensaje"]))?></td>
                        </tr>
                    <?
                    }
                	?>
				</table>
			
			<br/>
			<?
			If ($objCliente->getDatos()->getNombre()){
			$usernamemsj = " ".$objCliente->getDatos()->getNombre();
			}
			else{
			$usernamemsj = "";
			}
			?>
			<?
			$userId = Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]);
			$saludo = dmMensajes::getSaludosMensajes($userId);
			?>
			<input type="button" value=" Editar saludo de despedida " id="botonMensajeDesp" name="botonMensajeDesp" />
				<div id="saludodespedida">
					<form id="mensajeDespedidaForm" name="mensajeDespedidaForm" method="post" action="<?=UrlResolver::getBaseUrl('logica_bo/mensajes/actionform/actualizar_saludo_despedida.php')?>" >
					    <input type="hidden" name="idMensaje" value="<?=$objMensaje->getidMensaje()?>" />
						<textarea id="saludo" name="saludo" cols="45" rows="2" value="<?=$saludo?>"><?=$saludo?></textarea>
						<input type="submit" value="Guardar" id="botonGuardar" name="botonGuardar" />
					</form>
				</div>
					<script>
					$('#saludodespedida').hide();
						$('#botonMensajeDesp').click(function() {
							$('#saludodespedida').toggle();
						});	
					$('#botonGuardar').click(function() {
							$('#saludodespedida').hide();
						});
			</script>
			<?
			$horactual  = strtotime(date("H:i:s"));
			$mediodia  = strtotime("12:00:00");
			IF ($horactual <= $mediodia){
			$dia = "Buen d&iacute;a";
			}else{
			$dia = "Buenas tardes";
			}

			?>
 			<form style="margin:0;padding:0;" id="frmResponder" name="frmResponder" method="post" action="<?=Aplicacion::getRootUrl().$POST["mensajes"]?>" >
            	<input type="hidden" id="txtIdMensajePadre" name="txtIdMensajePadre" value="<?=$objMensaje->getidMensaje()?>" />
            	<input type="hidden" id="txtAsunto" name="txtAsunto" value="" />
                <h2>Respuesta: </h2><br />
			    <div id="frmResponder_errorloc" class="error_strings"></div>
<textarea cols="45" rows="5" id="txtMensaje" name="txtMensaje" >Hola<?=$usernamemsj?>, <?=$dia?>.</textarea>
                <br/>
				<?=nl2br($saludo)?>
				<br/>
                <table style="border:0;">
                	<tr style="border:0;">
                		<td style="border:0;"><input type="submit" value="Responder" id="botonResponder" name="botonResponder" /></td>
                		<td style="border:0;text-align:right;"><input type="submit" value="Responder y Siguiente" id="botonResponderYSiguiente" name="botonResponderYSiguiente" /></td>
                	</tr>
                	<tr style="border:0;">
                		<td style="border:0;"><input type="submit" value="Cerrar" id="botonCerrar" name="botonCerrar"/></td>
                		<td style="border:0;text-align:right;"><input type="submit" value="Cerrar y Siguiente" id="botonCerrarYSiguiente" name="botonCerrarYSiguiente"/></td>
                	</tr>
                	<tr>
                		<td style="border:0;"><input type="submit" value="Responder y Cerrar" id="botonResponderYCerrar" name="botonResponderYCerrar"/></td>
                		<td style="border:0;"></td>
                	</tr>
                </table>
            </form>
			<form style="margin:0;padding:0;" id="frmSiguiente" name="frmSiguiente" method="post" action="<?=Aplicacion::getRootUrl().$POST["mensajes"];?>" >
            	<input type="hidden" id="txtIdMensajePadre" name="txtIdMensajePadre" value="<?=$objMensaje->getidMensaje();?>" />
            	<input type="hidden" id="txtAsunto" name="txtAsunto" value="" />
                <table style="border:0;margin:0;padding:0;">
                	<tr style="border:0;margin:0;padding:0;">
                		<td style="border:0;"><input type="submit" value="Reabrir mensaje" id="botonReabrir" name="botonReabrir"/></td>
                		<td style="border:0;text-align:right;"><input type="submit" value="Siguiente Mensaje" id="botonSiguiente" name="botonSiguiente"/></td>
                	</tr>
                </table>
            </form>
		</div>
		
				</div>
				
				<div class="column columnRight">
				
				<iframe src="/<?=$confRoot[1]?>/bo/mensajes/botones.php?IdUsuario=<?=$idUsuario;?>" name="botones" id="botones" width="100%" height="30" scrolling="no" frameborder="0" transparency></iframe>
				<iframe src="/<?=$confRoot[1];?>/bo/mensajes/cliente.php?IdUsuario=<?=$idUsuario;?>" name="sidebar" id="sidebar" width="100%" height="400" scrolling="auto" frameborder="0" transparency></iframe>
				
				</div>
		</div>
		
		
		<!-- Geelbe Validation Form -->
		<script type="text/javascript">
		var formValidator  = new Validator("frmResponder");
		formValidator.EnableOnPageErrorDisplaySingleBox();
		formValidator.EnableMsgsTogether();
		formValidator.EnableFocusOnError(false);
		
		formValidator.addValidation("txtMensaje","req","Por favor, ingrese el mensaje a responder.");
		
		</script>
		<!-- Geelbe Validation Form -->
		<script type="text/javascript">
		$("#motivoChange").hide();
    	$("#motivoChangeLink").click(function(){
    		$("#motivoChange").show();
			$("#motivoShow").hide();
		});
		$("#txtMotivo").change(function(){
			$("#motivoForm").submit();
		});

		$("#campaniaChange").hide();
    	$("#campaniaChangeLink").click(function(){
    		$("#campaniaChange").show();
			$("#campaniaShow").hide();
		});
		$("#txtCampania").change(function(){
			$("#campaniaForm").submit();
		});
		</script>

<script>

$(document).ready(function(){
    $('#sidebar').load(function(){

	<?php 
		$templates = dmMensajes::getTemplates(1);
		foreach ($templates as $unTemplate) {
    ?>	
    	$("#sidebar").contents().find("#addTemplate<?=$unTemplate['id_template'];?>").click(function() {
    		$("#txtMensaje").val($("#txtMensaje").val()+"\n\n"+$("#sidebar").contents().find("#addTemplateH<?=$unTemplate['id_template'];?>").val());
        });
    	$("#sidebar").contents().find("#addTemplateM<?=$unTemplate['id_template'];?>").click(function() {
    		$("#txtMensaje").val($("#txtMensaje").val()+"\n\n"+$("#sidebar").contents().find("#addTemplateH<?=$unTemplate['id_template'];?>").val());
        });	
    <?php 	
    	}
    ?>  

    <?php 
    	$templates = dmMensajes::getTemplates(2);
    	foreach ($templates as $unTemplate) {
    ?>	
    	$("#sidebar").contents().find("#addTemplate<?=$unTemplate['id_template'];?>").click(function() {
    		$("#txtMensaje").val($("#txtMensaje").val()+"\n\n"+$("#sidebar").contents().find("#addTemplateH<?=$unTemplate['id_template'];?>").val());
        });
    	$("#sidebar").contents().find("#addTemplateM<?=$unTemplate['id_template'];?>").click(function() {
    		$("#txtMensaje").val($("#txtMensaje").val()+"\n\n"+$("#sidebar").contents().find("#addTemplateH<?=$unTemplate['id_template'];?>").val());
        });	
    <?php 	
    	}
    ?> 

    <?php 
    	$templates = dmMensajes::getTemplates(3);
    	foreach ($templates as $unTemplate) {
	?>	
		$("#sidebar").contents().find("#addTemplate<?=$unTemplate['id_template'];?>").click(function() {
			$("#txtMensaje").val($("#txtMensaje").val()+"\n\n"+$("#sidebar").contents().find("#addTemplateH<?=$unTemplate['id_template'];?>").val());
		});
		$("#sidebar").contents().find("#addTemplateM<?=$unTemplate['id_template'];?>").click(function() {
			$("#txtMensaje").val($("#txtMensaje").val()+"\n\n"+$("#sidebar").contents().find("#addTemplateH<?=$unTemplate['id_template'];?>").val());
		});	
	<?php 	
		}
	?> 
    });
});

</script>
	</body>
	<script src="<?=UrlResolver::getJsBaseUrl("bo/clientes/mensajes.js")?>"  type="text/javascript"></script>
</html> 