<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/historico/datamappers/dmhistoricologin.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    
    try {
        ValidarUsuarioLogueadoBo(14);
    } catch(ACCESOException $e) {
	    ?>
	        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
	    <?
        exit;
    }

    $idUsuario = $_GET["IdUsuario"];
    
    $objCliente = dmCliente::getByIdUsuario((isset($idUsuario)) ? $idUsuario:0); 
    $idPadrino = dmUsuario::getIdByNombreUsuario($objCliente->getPadrino()); 
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/js/protoaculous.1.8.1.js")?>"  type="text/javascript"></script>	    
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/js/autosubmit.js")?>"  type="text/javascript"></script>	    
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/js/tablesort.js")?>"  type="text/javascript"></script>	    
		<script src="<?=UrlResolver::getJsBaseUrl("bo/js/autotablesort.js")?>"  type="text/javascript"></script>
	</head>

<body>
<div style="font-size:1.3em;">
					<h2>Informaci&oacute;n general</h2>
					<table>
						<tr class="alt">
							<th>Nombre Completo</th>
							<td><?=$objCliente->getDatos()->getApellido().", ".$objCliente->getDatos()->getNombre()?></td>
						</tr>
						<tr>
							<th>Edad</th>
							<td><?
								list($day,$month,$year) = explode("/",$objCliente->getDatos()->getFechaNacimiento());
	    						$year_diff  = date("Y") - $year;
	    						$month_diff = date("m") - $month;
	    						$day_diff   = date("d") - $day;
	    						if ($day_diff < 0 || $month_diff < 0)
	      							$year_diff--;
	      						echo $year_diff;
								?></td>
						</tr>
						<tr class="alt">
							<th>Fecha de nacimiento</th>
							<td><?=$objCliente->getDatos()->getFechaNacimiento()?></td>
						</tr>
						<tr>
							<th>Email</th>
							<td><?=$objCliente->getNombreUsuario()?></td>
						</tr>
						<tr class="alt">
                          <th>Sexo</th>
                          <td><?=($objCliente->getDatos()->getIdTratamiento()==1?"Masculino":"Femenino")?></td>
						</tr>
						<tr>
							<th>DNI</th>
							<td><?=$objCliente->getDatos()->getNroDoc()?></td>
						</tr>
						<tr class="alt">
				          <th>Tel&eacute;fono</th>
				          <td><?=$objCliente->getDatos()->getCodTelefono() . " - " . $objCliente->getDatos()->getTelefono()?></td>
				        </tr>
				        <tr>
				          <th>M&oacute;vil</th>
				          <td><?=$objCliente->getDatos()->getcelucodigo() . " - " . $objCliente->getDatos()->getcelunumero()?></td>
				        </tr>
				      </table>
<h2>Informaci&oacute;n Domicilio</h2>
 					<table>
					 <tr class="alt">
					   <th>Calle</th>
					   <td><?=$objCliente->getDirecciones(0)->getDomicilio()?></td>
					 </tr>
					 <tr>
					   <th>N&uacute;mero</th>
					   <td><?=$objCliente->getDirecciones(0)->getNumero()?></td>
					 </tr>
					 <tr class="alt">
					   <th>Piso</th>
					   <td><?=$objCliente->getDirecciones(0)->getPiso()?></td>
					 </tr>
					 <tr>
					   <th>Departamento</th>
					   <td><?=$objCliente->getDirecciones(0)->getPuerta()?></td>
					 </tr>
					 <tr class="alt">
					   <th>Ciudad</th>
					   <td><?=$objCliente->getDirecciones(0)->getPoblacion()?></td>
					 </tr>
					 <tr>
					   <th>C.P.</th>
					   <td><?=$objCliente->getDirecciones(0)->getCP()?></td>
					 </tr>
					 <tr class="alt">
					   <th>Provincia</th>
					   <td><?
			                if ($objCliente->getDirecciones(0)->getIdProvincia()) {
			                    $oProvincia = dmCliente::GetProvinciasById($objCliente->getDirecciones(0)->getIdProvincia());
			                    echo $oProvincia[0]["Nombre"];
			                }
			                ?>
					   </td>
					 </tr>
					 <tr>
					   <th>Pa&iacute;s</th>
					   <td><?
		                    if ($objCliente->getDirecciones(0)->getIdPais()) {
		                        $oPais = dmCliente::GetPaisesById($objCliente->getDirecciones(0)->getIdPais());
		                        echo $oPais[0]["Nombre"];
		                    }
		                    ?>
					   </td>
					 </tr>
				   </table>
				      <h2>Estado Geelbe</h2>
					  <table>
						<tr class="alt">
							<th>Padrino</th>
							<td><a target="iPrincipal" href="<?="/".$confRoot[1]."/bo/clientes/Ver.php?IdUsuario=".$idPadrino?>"><?=$objCliente->getPadrino()?></a></td>
						</tr>
						<tr>
							<th>Registrado el</th>
							<td><?=$objCliente->getFechaIngreso();?></td>
						</tr>
					    <tr class="alt">
					      <th>Estado</th>
					      <td><?
					      		if($objCliente->getes_Activa()) {
					      			if($objCliente->getes_Provisoria()) {
					      				if($objCliente->getPadrino()=="Huerfano") {
							      			echo "HUERFANO"; 
					      				} else {
							      			echo "INACTIVO"; 
					      				}
					      			} else {
						      			echo "DESUSCRIPTO"; 
					      			}
					      		} else {
					      			echo "ACTIVO";
					      		}
					      	  ?>
					      </td>
					    </tr>
					    <tr>
					      <th>&Uacute;ltimo login</th>
					      <td><?=dmLoginH::ObtenerUltimoLogin($objCliente->getNombreUsuario())?></td>
				        </tr>
                        <tr class="alt">
                          <th>Invitados</th>
					      <td><?
					        $oEstadistica = new dmEstadisticas();
					        echo $oEstadistica->Cliente()->CountUsuariosAhijados($objCliente->getIdUsuario())?>
					      </td>
					    </tr>
					    <tr>
					      <th>Tasa (Ahijados)</th>
					      <td><?
			                  if ($oEstadistica->Cliente()->CountUsuariosAhijados($objCliente->getIdUsuario()) > 0) {
			                      echo round($oEstadistica->Cliente()->CountUsuariosAhijadosRegistrados($objCliente->getIdUsuario()) / $oEstadistica->Cliente()->CountUsuariosAhijados($objCliente->getIdUsuario()), 4);
			                  } else {
			                      echo 0;
			                  }
			                  ?>
			              </td>
					    </tr>
					    <tr class="alt">
					      <th>Invitados registrados</th>
					      <td><?
					            echo $oEstadistica->Cliente()->CountUsuariosAhijadosRegistrados($objCliente->getIdUsuario());
					          ?>
					      </td>
					    </tr>
					    <tr>
					      <th>Invitados compradores</th>
					      <td><? echo $oEstadistica->Cliente()->CountUsuariosAhijadosCompradores($objCliente->getNombreUsuario());?></td>
					    </tr>
					</table>
				    <h2>Cr&eacute;ditos</h2>
					<table>
				      <tr class="alt">
				        <th>Cr&eacute;ditos Disponibles</th>
				        <td colspan="3">$ <?=dmMisCreditos::getCreditoDisponible($objCliente->getIdUsuario())?></td>
				      </tr>
				      <tr>
				        <th>Cr&eacute;ditos Potenciales</th>
				        <td colspan="3">$ <?=dmMisCreditos::getCreditoPotencial($objCliente->getIdUsuario(), $objCliente->getNombreUsuario())?></td>
				      </tr>
				      <tr class="alt">
				        <th>Cr&eacute;ditos Utilizados</th>
				        <td colspan="3">$ <?=(int) $oEstadistica->Cliente()->BonosUtilizados($objCliente->getIdUsuario())?></td>
				      </tr>
					</table>
					</div>
					</body>
					</html>