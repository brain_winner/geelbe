<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/historico/datamappers/dmhistoricologin.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/mailer/clsMailer.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/clases/clsusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/pedidos/clsinf.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmcupones.php");
    
    try {
        ValidarUsuarioLogueadoBo(3);
    } catch(ACCESOException $e) {
	    ?>
	        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
	    <?
        exit;
    }
	$postURL = Aplicacion::getIncludes("post", "pedidos");

    $idUsuario = $_GET["IdUsuario"]; 

	$oConexion = Conexion::nuevo();
    $oConexion->setQuery("SELECT p.IdPedido, p.Total, p.Fecha, c.Nombre, c.FechaFin, e.Descripcion as Estado FROM pedidos p	LEFT JOIN campanias c ON p.IdCampania = c.IdCampania LEFT JOIN estadospedidos e ON e.IdEstadoPedido = p.IdEstadoPedidos	WHERE p.IdUsuario = ".mysql_real_escape_string($idUsuario)." ORDER BY p.Fecha DESC");
    $pedidos = $oConexion->DevolverQuery();
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Informaci&oacute;n del Usuario</title>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
		<link href="<?=UrlResolver::getCssBaseUrl("bo/mensajes/infopedidos.css")?>" media="screen" rel="stylesheet" type="text/css" />
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/js/protoaculous.1.8.1.js")?>"  type="text/javascript"></script>	    
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/js/autosubmit.js")?>"  type="text/javascript"></script>	    
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/js/tablesort.js")?>"  type="text/javascript"></script>	    
		<script src="<?=UrlResolver::getJsBaseUrl("bo/js/autotablesort.js")?>"  type="text/javascript"></script>
		
        <script src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.2.min.js");?>" type="text/javascript"></script>
		<?
	        Includes::Scripts();
	    ?>
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/pedidos/js.js");?>" type="text/javascript"></script>
	</head>
	
	<body>

			  <h2>Pedidos</h2>
			  <table>
			    <tr>
				  <th>Id Pedido</th>
				  <th>Fecha</th>
				  <th>Campa�a</th>
				  <th>Estado</th>
				  <th>Total</th>
				</tr>
				<?
				    foreach($pedidos as $i => $data) {
						$fecha_fin_date = date("Y-m-d H:i:s", strtotime($data['FechaFin']));
						$todays_date = date("Y-m-d H:i:s");

						$today = strtotime($todays_date);
						$fecha_fin = strtotime($fecha_fin_date);

						if ($fecha_fin >= $today) {
							$EstadoFecha = "Vigente";
						} else {
							$EstadoFecha = "Cerrada";
						}
				?>
				<tr <?=($i%2 == 0 ? ' class="alt"' : '')?>>
					<td><a target="iPrincipal" href="javascript:;" id="link<?=$data['IdPedido'];?>"><?=$data['IdPedido'];?></a></td>
					<td><?=date("d/m/Y", strtotime($data['Fecha']));?></td>
					<td><?=$data['Nombre']." - ".$EstadoFecha;?></td>
					<td><?=$data['Estado'];?></td>
					<td>$<?=number_format($data['Total'], 2, '.', ',');?></td>
				</tr>
				<tr class="infopedidos" id="rta<?=$data['IdPedido'];?>" style="background-color:#CCC;">
				 <td colspan="5">
				 <?
						$objCliente = dmCliente::getByIdUsuario((isset($idUsuario)) ? $idUsuario:0); 
						$idPadrino = dmUsuario::getIdByNombreUsuario($objCliente->getPadrino()); 
						$objPedido = dmPedidos::getPedidoById($data['IdPedido']);
						$objUsuario = dmMicuenta::GetUsuario($objPedido->getIdUsuario());
						$InfoPedido = dmPedidos::getInfoPedido($data['IdPedido']);
						$arrayPedidosDirecciones = dmPedidos::getPedidosDirecciones($data['IdPedido']);

						$divInfcontPedido = new htmlInfContPedido($objPedido,$Bonos[0]["Bono"]);
						echo $divInfcontPedido->imprimir();

						$divInfPago = new htmlInfPago($InfoPedido["FormaPago"]["Descripcion"]);
						 echo $divInfPago->imprimir();

						$divInfEstPed = new htmlInfEstPed($InfoPedido["Estado"]["Descripcion"],$objPedido->getFechaEnvio(), dmCupones::getIdCuponByIdPedido($objPedido->getIdPedido()),$objPedido->getIdPedido(),$objPedido->getNroEnvio());
						echo $divInfEstPed->imprimir();
						

						$divInfDatComp = new htmlInfDatComp($objUsuario->getDatos(),$arrayPedidosDirecciones);
						echo $divInfDatComp->imprimir();

						$divInfDirEnv = new htmlInfDirEnv($objPedido->getDireccion());
						echo $divInfDirEnv->imprimir();

				 ?>
				 </td>
				</tr>
				<script>
					$('#rta<?=$data['IdPedido'];?>').hide();
					$('#link<?=$data['IdPedido'];?>').click(function() {
						$('#rta<?=$data['IdPedido'];?>').toggle();
						});						  
				</script>
				<? } ?>				
			  </table>
			<script>
				 function openTargetBlank(e){

					   var className = 'external';

					   if (!e) var e = window.event;
					   var clickedObj = e.target ? e.target : e.srcElement;

					   if(clickedObj.nodeName == 'A' )
						{
						  r=new RegExp("(^| )"+className+"($| )");
						  if(r.test(clickedObj.className)){
							 window.open(clickedObj.href);
							 return false;

						  }
						}
					}

					document.onclick = openTargetBlank;
			</script>
	</body>
</html>
