<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/historico/datamappers/dmhistoricologin.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    
    try {
        ValidarUsuarioLogueadoBo(14);
    } catch(ACCESOException $e) {
	    ?>
	        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
	    <?
        exit;
    }

	$idUsuario = $_GET["IdUsuario"];
	$mensajesdb = dmMensajes::getMensajes_ByIdUsuario($idUsuario, "");
    
    $objCliente = dmCliente::getByIdUsuario((isset($idUsuario)) ? $idUsuario:0); 
    $idPadrino = dmUsuario::getIdByNombreUsuario($objCliente->getPadrino()); 
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Informaci&oacute;n del Usuario</title>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />  
		
        <script src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.2.min.js");?>" type="text/javascript"></script>
	</head>
	
	<body>
		<div style="font-size:1.3em;">
				   <h2>Mensajes</h2>
				   <h3>(Presionar sobre las filas para ver el detalle de cada mensaje)</h3>
  				   <table>
				     <tr>
					   <th>Fecha</th>
					   <th>Motivo</th>
					   <th>Respondido</th>
					 </tr>
						<?php
						$j=1;
						foreach($mensajesdb as $i => $mensaje) {
							$j++;
							$Motivo = dmMensajes::getMotivoById($mensaje['IdMotivo']);
							



							if($j%2 == 0) {
								echo '<tr class="alt" id="m'.$mensaje['idMensaje'].'">';
							}
							else {
								echo '<tr id="m'.$mensaje['idMensaje'].'">';
							}
						?>
						<td><?=date("d/m/Y", strtotime($mensaje['Fecha']));?></td>
						<td><a href="javascript:;" id="link<?=$mensaje['idMensaje'];?>"><?=$Motivo;?></a></td>
					    <td><?=(!isset($mensaje['respuesta']) ? $mensaje['Respondido'] : '');?></td>
				      </tr>
					  <?php		
							$oConexion = Conexion::nuevo();
	    					$oConexion->setQuery("SELECT idMensaje, 
	           						                     idMensajePadre, 
						                                 Fecha, 
						                                 IF(Respondido = 0, 'No', 'S�') as Respondido, 
						                                 Mensaje as Mensaje, 
						                                 idmotivo as IdMotivo,
						                                 boUserId as boUserId 
						                          FROM mensajes 
						                          WHERE idMensajePadre = ".$mensaje['idMensaje']);
	    					$respuestas = $oConexion->DevolverQuery();
					  
						
					  ?>					
					  <tr id="rta<?=$mensaje['idMensaje'];?>" style="background-color:#CCC;">
					  <td colspan="3">
					  <?php 
					  echo "<span style=\"font-weight:bold;\">Mensaje:</span> ".$mensaje['Mensaje']."<br/>";
					  foreach($respuestas as $i => $respuesta) {
					  	
	    				
					  	echo "--<br/>";
					  	echo "<span style=\"font-weight:bold;\">Respuesta:</span> ".$respuesta['Mensaje']."<br/>";
					  	if ($respuesta['boUserId']) {
					  		$objClienteBO = dmCliente::getByIdUsuario($respuesta['boUserId']);
					  		echo $objClienteBO->getNombreUsuario()." - ";
					  	}
					  	echo $respuesta['Fecha']."<br/>";
					  }
					  
						
					  ?>
					 </td>
					  </tr>
					  <script>
					  $('#rta<?=$mensaje["idMensaje"];?>').hide();
					  $('#link<?=$mensaje["idMensaje"];?>').click(function() {
						  $('#rta<?=$mensaje["idMensaje"];?>').toggle();
						});
										  
					  </script>
					  <?php 
						}
						?>
					</table>
		</div>
	</body>
	<script src="<?=UrlResolver::getJsBaseUrl("bo/clientes/mensajes.js")?>"  type="text/javascript"></script>
</html>
