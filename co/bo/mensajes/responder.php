<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    try
    {
        ValidarUsuarioLogueadoBo(14);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $POST = Aplicacion::getIncludes("post", "mensajes");
    $objMensaje = new Mensajes();
    if($_GET["idMensaje"])
        $objMensaje = dmMensajes::getById(Aplicacion::Decrypter($_GET["idMensaje"]));
?>
<html>
    <head>
    </head>
    <body>
        <form id="frmResponder" name="frmResponder" method="post" action="<?=Aplicacion::getRootUrl() . $POST["mensajes"]?>">
            <table>
                    <input type="hidden" id="txtIdMensajePadre" name="txtIdMensajePadre" value="<?=$objMensaje->getidMensaje()?>" />
                <tr>
                    <td>Asunto</td>
                    <td><input size="60" type='text' id='txtAsunto' name='txtAsunto' value="RE: <?=stripcslashes($objMensaje->getAsunto())?>" /></td>
                </tr>
                <tr>
                    <td>Mensaje</td>
                    <td><textarea cols="60" rows="5" id="txtMensaje" name="txtMensaje"></textarea></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Responder" /></td>
                </tr>
            </table>
        </form>  
    </body>
</html>