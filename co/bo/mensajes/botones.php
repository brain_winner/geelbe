<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/mensajes/datamappers/dmmensajes.php");
    
    $idUsuario = $_GET["IdUsuario"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	<?		
		$abiertos = dmMensajes::getMensajesAbiertos($idUsuario);
		$totales = dmMensajes::getTotalMensajes($idUsuario);
	?>
	</head>

	<body>
	<div style="font-size:1.3em;">
		<table>
			<tr class="alt">
				<td style="text-align:left;font-weight:bold;">
					<b>
						<a href="/<?=$confRoot[1];?>/bo/mensajes/cliente.php?IdUsuario=<?=$idUsuario;?>" target="sidebar">Cliente</a> | 
					   	<a href="/<?=$confRoot[1];?>/bo/mensajes/pedidos.php?IdUsuario=<?=$idUsuario;?>" target="sidebar">Pedidos</a> | 
					   	<a href="/<?=$confRoot[1];?>/bo/mensajes/mensajes.php?IdUsuario=<?=$idUsuario;?>" target="sidebar">Mensajes (<?=$abiertos?>/<?=$totales?>)</a> | 
					   	<a href="/<?=$confRoot[1];?>/bo/mensajes/templates.php?IdUsuario=<?=$idUsuario;?>" target="sidebar">Templates</a>
					</b>
				</td>
			</tr>
		</table>
	</body>
	</div>
</html>
