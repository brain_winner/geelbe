<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(14);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }	
    
    $perfil = Aplicacion::Decrypter($_SESSION["BO"]["User"]["perfil"]);
    if($perfil != 2 && $perfil != 8 && $perfil != 10 && $perfil != 12 && $perfil != 15) {
    	
    	echo "<html><body><p>No cuentas con los permisos adecuados para ingresar a esta seccion</p></body></html>";
    	exit;
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Reporte mensual mensajes</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>

		<div id="container">
		    <form name="monthlyMsgsReportForm">
		    <label for="mes">Mes: </label>
		    <select name="mes" value="<?= $_GET["mes"] ?>">
		    	<?
		    	for ($i = 1; $i <= 12; $i++) {
		    		if($i == $_GET["mes"]) {
			    		echo '<option selected="selected" value="'.$i.'">'.$i.'</option>';
		    		} else {
			    		echo '<option value="'.$i.'">'.$i.'</option>';
		    		}
		    	}
		    	?>
		    </select>
		    &nbsp;
		    <label for="anio">A&ntilde;o: </label>
		    <select name="anio" value="<?= $_GET["anio"] ?>">
		    	<?
		    	for ($i = 2008; $i <= date("Y"); $i++) {
		    		
		    		if($i == $_GET["anio"]) {
			    		echo '<option selected="selected" value="'.$i.'">'.$i.'</option>';
		    		} else {
			    		echo '<option value="'.$i.'">'.$i.'</option>';
		    		}
		    	}
		    	?>
		    </select>
		    <br /><br />
		    <input type="submit" name="submitButton" value="Generar Reporte" />
		    </form>
		    <?
		    	if(isset($_GET["anio"]) && isset($_GET["mes"])) {
					echo "<h1>Mensajes del ".$_GET["mes"]."/".$_GET["anio"]."</h1>";	
					$oConexion = Conexion::nuevo();
					$oConexion->setQuery("select count(idMensaje) as msgs from mensajes where idMensajePadre IS NULL and month(fecha) = ".mysql_real_escape_string($_GET["mes"])." and year(fecha) = ".mysql_real_escape_string($_GET["anio"])." group by week(fecha)");
					$res = $oConexion->DevolverQuery();
					
					$week = 1;
					foreach($res as $result) {
						echo "<p>Semana ".$week.": ".$result["msgs"]."</p>";
						$week = $week + 1;
					}
		    	}
		    ?>

		</div>	
	</body>
</html>