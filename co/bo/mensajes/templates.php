<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
		<script src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.2.min.js");?>" type="text/javascript"></script>
	</head>
	
	<body>
	<div style="font-size:1.3em;">
	<table>
		<tr>
		 	<th>Templates de Introducci&oacute;n</th>
		</tr>
	<?php 
		$templates = dmMensajes::getTemplates(1);
		$i=0;
		foreach ($templates as $unTemplate) {
	?>	
		<tr <?=($i%2 == 0 ? ' class="alt"' : '')?>>
			<td>
				<a href="javascript:;" id="addTemplate<?=$unTemplate["id_template"];?>"><?=$unTemplate["title"];?></a>
				&nbsp; <a id="addTemplateM<?=$unTemplate["id_template"];?>" style="font-size:10px;color:red;" href="javascript:;">[agregar]</a>
				&nbsp; <a id="mensajeTemplateShow<?=$unTemplate["id_template"];?>" style="font-size:10px;color:red;" href="javascript:;">[mostrar]</a>
				<input type="hidden" id="addTemplateH<?=$unTemplate["id_template"];?>" value="<?=$unTemplate["message"];?>" />
			</td>
		</tr>
		
		<tr id="mTemplate<?=$unTemplate["id_template"];?>" style="background-color:#CCC;">
			<td><?=$unTemplate["message"];?></td>
		</tr>
		<script>
			$('#mTemplate<?=$unTemplate["id_template"];?>').hide();
			$('#mensajeTemplateShow<?=$unTemplate["id_template"];?>').click(function() {
				$('#mTemplate<?=$unTemplate["id_template"];?>').toggle();
			});
		</script>
		
	<?php 	
			$i++;
		}
	?>
	</table>
	<br/>
	<table>
		<tr>
		 	<th>Templates de Cuerpo</th>
		</tr>
	<?php 
		$templates = dmMensajes::getTemplates(2);
		$i=0;
		foreach ($templates as $unTemplate) {
	?>	
		<tr <?=($i%2 == 0 ? ' class="alt"' : '')?>>
			<td>
				<a href="javascript:;" id="addTemplate<?=$unTemplate["id_template"];?>"><?=$unTemplate["title"];?></a>
				&nbsp; <a id="addTemplateM<?=$unTemplate["id_template"];?>" style="font-size:10px;color:red;" href="javascript:;">[agregar]</a>
				&nbsp; <a id="mensajeTemplateShow<?=$unTemplate["id_template"];?>" style="font-size:10px;color:red;" href="javascript:;">[mostrar]</a>
				<input type="hidden" id="addTemplateH<?=$unTemplate["id_template"];?>" value="<?=$unTemplate["message"];?>" />
			</td>
		</tr>
		
		<tr id="mTemplate<?=$unTemplate["id_template"];?>" style="background-color:#CCC;">
			<td><?=$unTemplate["message"];?></td>
		</tr>
		<script>
			$('#mTemplate<?=$unTemplate["id_template"];?>').hide();
			$('#mensajeTemplateShow<?=$unTemplate["id_template"];?>').click(function() {
				$('#mTemplate<?=$unTemplate["id_template"];?>').toggle();
			});
		</script>
		
	<?php 	
			$i++;
		}
	?>
	</table>
	<br/>
	<table>
		<tr>
		 	<th>Templates de Pie</th>
		</tr>
	<?php 
		$templates = dmMensajes::getTemplates(3);
		$i=0;
		foreach ($templates as $unTemplate) {
	?>	
		<tr <?=($i%2 == 0 ? ' class="alt"' : '')?>>
			<td>
				<a href="javascript:;" id="addTemplate<?=$unTemplate["id_template"];?>"><?=$unTemplate["title"];?></a>
				&nbsp; <a id="addTemplateM<?=$unTemplate["id_template"];?>" style="font-size:10px;color:red;" href="javascript:;">[agregar]</a>
				&nbsp; <a id="mensajeTemplateShow<?=$unTemplate["id_template"];?>" style="font-size:10px;color:red;" href="javascript:;">[mostrar]</a>
				<input type="hidden" id="addTemplateH<?=$unTemplate["id_template"];?>" value="<?=$unTemplate["message"];?>" />
			</td>
		</tr>
		
		<tr id="mTemplate<?=$unTemplate["id_template"];?>" style="background-color:#CCC;">
			<td><?=$unTemplate["message"];?></td>
		</tr>
		<script>
			$('#mTemplate<?=$unTemplate["id_template"];?>').hide();
			$('#mensajeTemplateShow<?=$unTemplate["id_template"];?>').click(function() {
				$('#mTemplate<?=$unTemplate["id_template"];?>').toggle();
			});
		</script>
		
	<?php 	
			$i++;
		}
	?>
	</table>
	</div>
</body>
</html>