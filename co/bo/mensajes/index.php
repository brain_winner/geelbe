<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    	Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
		$POST = Aplicacion::getIncludes("post", "pedidos");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(14);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }	
	try {
		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array('ME.idmotivo', 'ME.email', 'ME.mensaje', 'ME.IdCampania', 'U.NombreUsuario'));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("ME.fecha", "DESC");
		
		$parametro = "";
		$showing = "";
		
		$motivosFilter = dmMensajes::getMotivosDeMensajes();
		$campaniaFilter;
 
		
	    if($_GET['messageType']) {
	    	if ($_GET['messageType'] == 'all') {
				$count = dmMensajes::getMensajesUsuariosCount($paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmMensajes::getMensajesUsuariosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
				$showing = "Todos";
				$campaniaFilter =  dmMensajes::getCampañasDeMensajes();
				
			}
			else if ($_GET['messageType'] == 'close') {
				$count = dmMensajes::getMensajesUsuariosCerradosCount($paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmMensajes::getMensajesUsuariosCerradosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
				$showing = "Cerrados";	
				$campaniaFilter =  dmMensajes::getCampañasDeMensajesCerrados();
			}
		    else if ($_GET['messageType'] == 'open') {
				$count = dmMensajes::getMensajesUsuariosAbiertosCount($paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmMensajes::getMensajesUsuariosAbiertosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
				$showing = "Abiertos";
				$campaniaFilter =  dmMensajes::getCampañasDeMensajesAbiertos();
		    }
		    else if ($_GET['messageType'] == 'pending') {
				$count = dmMensajes::getMensajesPendientesCount($paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmMensajes::getMensajesPendientesPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
				$showing = "Abiertos";
				$campaniaFilter =  dmMensajes::getCampañasDeMensajesPendientes();
		    }
		    else if ($_GET['messageType'] == 'asign') {
				$count = dmMensajes::getMensajesUsuariosAbiertosAsignadosCount($paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmMensajes::getMensajesUsuariosAbiertosAsignadosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
				$showing = "Abiertos Asignados";
				$campaniaFilter =  dmMensajes::getCampañasDeMensajesAbiertosAsignados();
		    }
		    else if ($_GET['messageType'] == 'noasign') {
				$count = dmMensajes::getMensajesUsuariosAbiertosSinAsignarCount($paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmMensajes::getMensajesUsuariosAbiertosSinAsignarPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
				$showing = "Abiertos Sin Asignar";
				$campaniaFilter =  dmMensajes::getCampañasDeMensajesAbiertosSinAsignar();
		    }
		    else if ($_GET['messageType'] == 'answer') {
				$count = dmMensajes::getMensajesUsuariosRespondidosCerradosCount($paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmMensajes::getMensajesUsuariosRespondidosCerradosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
				$showing = "Cerrados Respondidos";
				$campaniaFilter =  dmMensajes::getCampañasDeMensajesRespondidosCerrados();
		    }
		    else if ($_GET['messageType'] == 'noanswer') {
				$count = dmMensajes::getMensajesUsuariosNoRespondidosCerradosCount($paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmMensajes::getMensajesUsuariosNoRespondidosCerradosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
				$showing = "Cerrados No Respondidos";
				$campaniaFilter =  dmMensajes::getCampañasDeMensajesNoRespondidosCerrados();
		    }
		    else if ($_GET['messageType'] == 'mine') {
		    	$userId = Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]);
				$count = dmMensajes::getMensajesUsuariosAsignedToCount($userId, $paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmMensajes::getMensajesUsuariosAsignedToPaginados($userId, $paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
				$showing = "Abiertos Asignados a ".Aplicacion::Decrypter($_SESSION["BO"]["User"]["email"]);
				$campaniaFilter =  dmMensajes::getCampañasDeMensajesAsignedTo($userId);
		    }
		    $parametro = "?messageType=".$_GET["messageType"];
		}
		else {
				$userId = Aplicacion::Decrypter($_SESSION["BO"]["User"]["id"]);
				$count = dmMensajes::getMensajesUsuariosAsignedToCount($userId, $paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmMensajes::getMensajesUsuariosAsignedToPaginados($userId, $paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
				$showing = "Abiertos Asignados a ".Aplicacion::Decrypter($_SESSION["BO"]["User"]["email"]);
				$campaniaFilter =  dmMensajes::getCampañasDeMensajesAsignedTo($userId);
				$messageType="mine";
		}
	
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Mensajes</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Mensajes (<?=$showing;?>)</h1>	

			<div id="mainStats">
					<table style="margin-bottom:10px;">
						<tr class="alt">
							<td style="text-align:left;font-weight:bold;"><b>Mostrar Mensajes:</b> <a href="index.php?messageType=all">Todos</a> | 
			                  <a href="index.php?messageType=open">Abiertos</a> | 
			                  <a href="index.php?messageType=pending">Pendientes</a> | 
							  <a href="index.php?messageType=close">Cerrados</a> | 
							  <a href="index.php?messageType=mine">Mis Mensajes</a></td>
						</tr>
						<?php 
							if($_GET['messageType'] || isset($messageType)) {
								if (isset($messageType) || $_GET['messageType'] == 'open' || $_GET['messageType'] == 'asign' || $_GET['messageType'] == 'noasign') { ?>
						<tr>
							<td style="text-align:left;font-weight:bold;"><b>Abiertos:</b> <a href="index.php?messageType=open">Todos</a> | 
			                  <a href="index.php?messageType=noasign">Sin Asignar</a> | 
							  <a href="index.php?messageType=asign">Asignados</a></td>
						</tr>
						<?php } } ?>
						<?php 
							if($_GET['messageType']) {
								if ($_GET['messageType'] == 'close' || $_GET['messageType'] == 'answer' || $_GET['messageType'] == 'noanswer') { ?>
						<tr>
							<td style="text-align:left;font-weight:bold;"><b>Cerrados:</b> <a href="index.php?messageType=close">Todos</a> | 
			                  <a href="index.php?messageType=answer">Respondidos</a> | 
							  <a href="index.php?messageType=noanswer">No Respondidos</a></td>
						</tr>
						<?php } } ?>
					</table>
					
				</div>
			
			
			<?php
				$listadoPrinter = new ListadoPrinter($rows, "index.php$parametro", $listPager);
				
				$listadoPrinter->showColumn("MO.Descripcion", "Motivo", true, true, $motivosFilter);
				$listadoPrinter->showColumn("CA.Nombre", "Campa&ntilde;a", true, true, $campaniaFilter);
				$listadoPrinter->showColumn("ME.email", "E-Mail");
				$listadoPrinter->showColumn("ME.mensaje", "Mensaje");
				$listadoPrinter->showColumn("U.NombreUsuario", "Asignado a");
				
				$listadoPrinter->showColumn("Respondido", "Respondido");
				$listadoPrinter->showColumn("ME.fecha", "Fecha y Hora");
				
				$listadoPrinter->setShowFilter("ME.fecha", false);
				$listadoPrinter->setShowFilter("Respondido", false);
				
				$listadoPrinter->setSortColumn("Respondido", false);
				
				$listadoPrinter->setFilterName("MO.Descripcion", "ME.idmotivo");
				$listadoPrinter->setFilterName("CA.Nombre", "ME.IdCampania");
				
				
				$listadoPrinter->addHiddenValue("messageType", $_GET['messageType']);
				
				$listadoPrinter->linkColumn("ME.email", "/".$confRoot[1]."/bo/clientes/Ver.php?IdUsuario=[[ME.idUsuario]]&mail=[[ME.email]]", "Ver Cliente");
				$listadoPrinter->linkColumn("MO.Descripcion", "/".$confRoot[1]."/bo/mensajes/leer.php?idMensaje=[[ME.idMensaje]]", "Leer Mensaje");
				
				$listadoPrinter->printListado();

				$perfil = Aplicacion::Decrypter($_SESSION["BO"]["User"]["perfil"]);
				
				if($perfil == 2 || $perfil == 10 || $perfil == 12|| $perfil == 18) {
				
				if($_GET['messageType'] || isset($messageType)) {
					if (isset($messageType) || $_GET['messageType'] == 'noasign' || $_GET['messageType'] == 'asign' || $_GET['messageType'] == 'open') { 
						
						if(!isset($messageType) && $_GET['messageType']) {
							$messageType = $_GET['messageType'];
						}
						
						$usuariosBO = dmCliente::getUsuariosBOParaMensajes("3, 4, 5, 7, 9, 11, 12, 14");
					
						
						
				?>
						<br/>
						<b>Asignar todos los mensajes filtrados a: </b>
						<form style="display:inline;" action="asign_user.php" onSubmit="return confirm_entry();">
							<select id="userToAsign" name="userToAsign">
							<?php 
							foreach ($usuariosBO as $usuarioBO) {
							?>
								<option value="<?=$usuarioBO["IdUsuario"];?>" ><?=$usuarioBO["NombreUsuario"];?></option>
							<?php
							}
							?>
							</select>
							<input type="hidden" id="messageType" name="messageType" value="<?=$messageType?>" />
							
							<input type="hidden" id="filter[ME.idmotivo]" name="filter[ME.idmotivo]" value="<?=$_REQUEST["filter"]["ME.idmotivo"];?>" />
							<input type="hidden" id="filter[ME.IdCampania]" name="filter[ME.IdCampania]" value="<?=$_REQUEST["filter"]["ME.IdCampania"];?>" />
							<input type="hidden" id="filter[ME.email]" name="filter[ME.email]" value="<?=$_REQUEST["filter"]["ME.email"];?>" />
							<input type="hidden" id="filter[ME.mensaje]" name="filter[ME.mensaje]" value="<?=$_REQUEST["filter"]["ME.mensaje"];?>" />
							<input type="hidden" id="filter[U.NombreUsuario]" name="filter[U.NombreUsuario]" value="<?=$_REQUEST["filter"]["U.NombreUsuario"];?>" />
							
							<input type="submit" value="Asignar"/>
							<br/>
						</form>
						<script language="JavaScript">
						<!--
							function confirm_entry() {
								return confirm("Está seguro de asignar los <?=$count;?> mensajes al usuario seleccionado?");
							}
						-->
						</script>
						
				<?php } } } ?>
				<br />
				<br />
				<a href="<?="/".$confRoot[1]."/bo/mensajes/reporte_mensual_msjs.php"?>">Ver reporte mensual de mensajes recibidos</a>
		</div>	
	</body>
</html>
