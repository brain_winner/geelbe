var datos = "";
$(function(){


	$("#detalle").hide();
	$("#campanias").hide();
	$("#producto").hide();

	$("input[type=submit]").live("click",function(){
		$.finalizarCompra();
		return false;
	});

	$("#campanias li img").live("click",function(){
		$(this).irCamProd("getProductos","#producto");
	});

	$("#producto li img").live("click",function(){
		$(this).irCamProd("getAtributos","#detalle");
	});

	$(".botMas").live("click",function(){
		$(this).parents("div:first").agregarProducto();
	});

	$(".botQuitar").live("click",function(){
		$(this).parents("ul:first").eliminarProducto();
	});


	$.get("ajax.php?f=getCampanias", function(data){
		var data;
		$("#campanias").replaceWith(data);
	});

	$("input[name=para_otro]").click(function(){
		var text = $("input[type=text]", $(this).parents("div:first")).parent("p");

		$.actualizarPrecios();

		if($(this)[0].checked){
			text.fadeIn();
		}else{
			text.fadeOut().find("input[type=text]").attr("value","");
		}

	}).each(function(){

		$(this)[0].checked = false;
		var text = $("input[type=text]", $(this).parents("div:first")).parents("p");
		text.hide().find("input[type=text]").attr("value","");

		$.actualizarPrecios();
	});

});


$.actualizarPrecios = function(){
	if($.paraOtro()){
		$(".PInterno").hide();
		$(".PVenta").show();


	}else{
		$(".PInterno").show();
		$(".PVenta").hide();
	}

	$("#carrito").actualizarPreciosCarrito().actualizarSumatoria();
};

$.fn.actualizarPreciosCarrito = function(){
	return this.each(function(){
		$("ul",$(this)).each(function(){
			$(this).data("PFinal",($.paraOtro() ? $(this).data("PVenta") : $(this).data("PInterno"))  *  $(this).data("cantidad"));
			$(this).actualizarItem();
		});

	});
};


$.finalizarCompra = function(){
	var i = 0;
	var f = '<form action="/testing/logica_bo/carrito_bo/actionform/axCarrito_bo.php" method="post" name="fin_compra">';

	var ni = function(name,value){
		var i;
		var name;
		var value;
		return '<input type="hidden" name="'+name+'" value="'+value+'" />';
	};

	$("#carrito ul").each(function(){
		var t = $(this);
		f += ni('producto['+i+'][IdProducto]',t.data("IdProducto"));
		f += ni('producto['+i+'][IdCodigoProdInterno]',$(this).attr("id"));
		f += ni('producto['+i+'][Cantidad]',t.data("cantidad"));
		i++;
	});
	f += ni("paraOtro",($.paraOtro()?1:0));
	f += ni("Nombre",$("#datos input[name=nombre]").val());
	f += ni("Apellido",$("#datos input[name=apellido]").val());
	f += '</form>';
	alert(f);
	finCompraForm = $(f);
	$("#finCompra").empty().append(finCompraForm);
	finCompraForm.submit();
};


$.paraOtro = function(){
	return $("input[name=para_otro]")[0].checked;
};



$.fn.actualizarSumatoria = function(){
	var total = 0;
	return this.each(function(){

		$("ul",$(this)).each(function(){
			total = total + $(this).data("PFinal");
		});

		$("h4", this).data("total", total).html("Total: $ "+total);
	});
};

$.fn.actualizarItem = function(){
	return this.each(function(){
		$(this).html('<li class="carrProd">Producto: '+$(this).data("NombreProducto")+'</li>' +
		'<li class="carrCant">Cantidad:'+$(this).data("cantidad") +'</li>' +
		'<li class="carrAtri">'+ $(this).data("Atributos") +'</li>' +
		'<li class="carrPrec">Precio: $'+$(this).data("PFinal")+'</li>' +
		'<li class="botQuitar">Quitar Producto</li>');
	});
};


$.fn.agregarProducto = function(){
	return this.each(function(){

		//if(!$("#carrito:visible")[0]){$("#carrito").slideDown();}


		var Id = Number($("#Stock span").attr("class"));
		var Stock = $("#Stock span b").html();
		if(Id >0 && Stock >0){

			var datos_producto;

			//actualizo el objeto
			datos = $(datos).map(function(){
				if(this["IdCodigoProdInterno"] == Id){
					this["Stock"] = this["Stock"] - 1;
					datos_producto = this;
				}
				return this;
			});
			//alert(datos_producto["IdProducto"]);

			//Actualizo el LI
			$("#Stock span").html("Cantidad Disponible: <b>" + datos_producto["Stock"]+"</b>");


			var ulId = $("#carrito ul#"+Id);

			if(ulId[0]){

				ulId.data("cantidad", Number(ulId.data("cantidad"))+1);
				ulId.data("PFinal", ($.paraOtro() ? ulId.data("PVenta") : ulId.data("PInterno"))  *  ulId.data("cantidad"));

			}else{

				$("#carrito h3").after('<ul id="'+Id+'"></ul>');

				var atributos = "";
				$("#detalle select").each(function(){
					atributos += $(this).attr("id") +": "+ $(this).val()+"  ";
				});

				ulId = $("#carrito ul#"+Id);
				ulId.data("IdProducto", $("#detalle").data("IdProducto"));
				ulId.data("cantidad", 1);
				ulId.data("PInterno",$("#detalle").data("PInterno"));
				ulId.data("Atributos",atributos);
				ulId.data("PVenta",$("#detalle").data("PVenta"));
				ulId.data("NombreProducto",$("#detalle").data("NombreProducto"));
				ulId.data("PFinal", ($.paraOtro() ? ulId.data("PVenta") : ulId.data("PInterno"))  *  ulId.data("cantidad"));
			}

			ulId.actualizarItem();

			$("#carrito").actualizarSumatoria();
		}
	});
};

$.fn.eliminarProducto = function(){
	return this.each(function(){


		if($(this).data("IdProducto") == $("#detalle").data("IdProducto")){

			var datos_producto;
			var Id = Number($(this).attr("id"));
			var cantidad = $(this).data("cantidad");

			//actualizo el objeto
			datos = $(datos).map(function(){
				if(this["IdCodigoProdInterno"] == Id){
					this["Stock"] = this["Stock"] + cantidad;
					datos_producto = this;
				}
				return this;
			});
		}

		if($("#Stock span").attr("class") == $(this).attr("id")){
			//Actualizo el LI
			$("#Stock span").html("Cantidad Disponible: <b>" + datos_producto["Stock"]+"</b>");
		}

		$(this).fadeOut("normal",function(){
			$(this).remove();
			$("#carrito").actualizarSumatoria();
		});

	});
};



$.fn.irCamProd = function(axFuncion,div_camprod){
	return this.each(function(){
		var self = this;
		var camprod = $(this).attr("src");
		camprod = NumFoto(camprod);
		$.get("ajax.php?f="+axFuncion+"&p="+camprod, function(data){
			var data;
			var loaded = $(data);

			$(div_camprod).replaceWith(loaded);
			$.actualizarPrecios();

			if(axFuncion == "getAtributos"){
				$("#detalle").setearSelects();

				//	Guardo el precio en el titulo

				$("#detalle").data("PInterno",$(self).parents("ul:first").find(".PInterno").html());
				$("#detalle").data("PVenta",$(self).parents("ul:first").find(".PVenta").html());
				$("#detalle").data("NombreProducto",$(self).parents("ul").find("li:first").html());
				$("#detalle").data("IdProducto",camprod);

				//Stock
				datos = $(datos).map(function(){

					var thisdatos = this;

					var descontar_stock = 0;
					$("#carrito ul").each(function(){
						if(thisdatos["IdCodigoProdInterno"]==$(this).attr("id")){
							descontar_stock = $(this).data("cantidad");
						}
					});

					thisdatos["Stock"] = thisdatos["Stock"] - descontar_stock;

					return thisdatos;
				});


			}
			if(axFuncion == "getProductos"){
			}

		});

		$(this).parents("ul:first").addClass("seleccionado").siblings().removeClass("seleccionado");

	});
};


var NumFoto = function(string){
	var guion =  parseInt(string.indexOf("_"));
	var barra =  parseInt(string.indexOf("/",guion));
	var numero = string.substring(guion+1, barra);
	return numero;
};


//Setea los Selects en cascada
$.fn.setearSelects = function(){

	var self = $(this);
	var cant = $("select", self).size();

	for(i=1;i<cant;i++){

		$("select[name="+i+"]", self).cascade($("select[name="+(i-1)+"]", self),{
			list: datos,
			template: commonTemplate,
			match: commonMatch
		},i);

		$("select[name=1]", self).trigger("change");

	}

	$("#Stock", self).cascade($("select[name="+(cant-1)+"]", self),{
		list: datos,
		template: commonTemplate2,
		match: commonMatch
	},cant);
};

//Functiones para Cascada.
function commonTemplate(item,customI) {
	return "<option value='" + item[customI] + "'>" + item[customI] + "</option>";
};

function commonTemplate2(item,customI) {
	return "<span class=" + item["IdCodigoProdInterno"]+">Cantidad Disponible:<b>" + item["Stock"] + "</b></span>";
};


function commonMatch(selectedValue,customI){

	if(this["Stock"] < 1){
		return false;
	}

	for(i=0;customI>i;i++){
		if(this[i] != $("select[name="+i+"]").val()){
			return false;
		}
	}

	return true;
};