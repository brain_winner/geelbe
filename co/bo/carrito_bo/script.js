var datos = "";
$(function(){

	var estadoH3 = {
		"detalle": false,
		"campanias": false,
		"productos": false
	}

	$("#detalle").hide();
	$("#campanias").hide();
	$("#producto").hide();
	$("input[name=para_otro]:first")[0].checked = true;

	$.cargarCampanias();

	$("input[type=submit]").live("click",function(){
		$.finalizarCompra();
		return false;
	});

	$("#campanias ul:has(img)").live("click",function(){
		$(this).find("img").irCamProd("getProductos","#producto");
	});

	$("#producto ul:has(img)").live("click",function(){
		$(this).find("img").irCamProd("getAtributos","#detalle");
	});

	$(".botMas").live("click",function(){
		$(this).parents("div:first").agregarProducto();
	});

	$(".botQuitar").live("click",function(){
		$(this).parents("ul:first").eliminarProducto();
	});

	$("input[name=para_otro]").live("click",function(){
		$(this).actualizarComprador();
	}).each(function(){
		$(this).actualizarComprador();
	});

	$("h3").live("click",function(){
	});
});


$.fn.obtenerId = function(){
		var src = $(this).attr("src");
		var guion =  parseInt(src.indexOf("_"));
		var barra =  parseInt(src.indexOf("/",guion));
		var numero = src.substring(guion+1, barra);
		return numero;
};


$.fn.acordeon = function(accion,callback){
	var callback = callback?callback:function(){};
	var accion;
	return this.find("h3").each(function(){
		var self = this;
		var contenido = $(this).parents("div:first").children().not("h3");
		if(accion == "abrir"){
			contenido.slideDown("fast", callback.call(callback,this));
		}
		if(accion == "cerrar"){
			contenido.slideUp("fast", callback.call(callback,this));
		}
	});
};

$.fn.actualizarComprador = function(){
	return this.each(function(){
		var textBox = $("input[type=text]", $(this).parents("div:first")).parents("p");
		$.actualizarPrecios();
		if($.paraOtro()){
			textBox.fadeIn();
		}else{
			textBox.fadeOut().find("input[name=comprador]").attr("value","");
		}
	});
};

$.cargarCampanias = function(){
	$.get("ajax.php?f=getCampanias", function(data){
		var data;
		var loaded = $(data);
		//loaded = loaded.acordeon("cerrar");
		$("#campanias").replaceWith(loaded);
		//$("#campanias").acordeon("abrir");
	});
};

$.actualizarPrecios = function(){
	if($.paraOtro()){
		$(".PInterno").hide();
		$(".PVenta").show();
	}else{
		$(".PInterno").show();
		$(".PVenta").hide();
	}

		$("#carrito").actualizarPreciosCarrito().actualizarSumatoria();
};

$.fn.actualizarPreciosCarrito = function(){
	return this.each(function(){
		$("ul",$(this)).each(function(){
			$(this).data("PFinal",( $.paraOtro() ? $(this).data("PVenta") : $(this).data("PInterno"))  *  $(this).data("cantidad"));
			$(this).actualizarItem();
		});

	});
};


$.finalizarCompra = function(){
	var i = 0;
	var f = '<form action="/<?=$DIRLOCAL?>logica_bo/carrito_bo/actionform/axCarrito_bo.php" method="post" name="fin_compra">';

	var ni = function(name,value){
		var i;
		var name;
		var value;
		return '<input type="hidden" name="'+name+'" value="'+value+'" />';
	};

	$("#carrito ul").each(function(){
		var t = $(this);
		f += ni('producto['+i+'][IdProducto]',t.data("IdProducto"));
		f += ni('producto['+i+'][IdCodigoProdInterno]',$(this).attr("id"));
		f += ni('producto['+i+'][Cantidad]',t.data("cantidad"));
		i++;
	});
	f += ni("paraOtro",($.paraOtro()?1:0));
	f += ni("Nombre",$("#datos input[name=nombre]").val());
	f += ni("Apellido",$("#datos input[name=apellido]").val());
	f += '</form>';
//	alert(f);
	finCompraForm = $(f);
	$("#finCompra").empty().append(finCompraForm);
	finCompraForm.submit();
};

$.paraOtro = function(){
	return ($("input[name=para_otro]:checked").val() == "true");
};

$.fn.actualizarSumatoria = function(){
	var total = 0;
	return this.each(function(){

		$("ul",$(this)).each(function(){
			total = total + $(this).data("PFinal");
		});

		$("h4", this).data("total", total).html("Total: $ "+total);
	});
};

$.fn.actualizarItem = function(){
	return this.each(function(){
	   $(this).html('<li class="carrProd">Producto: '+$(this).data("NombreProducto")+'</li>' +
				   '<li class="carrCant">Cantidad:'+$(this).data("cantidad") +'</li>' +
				   '<li class="carrAtri">'+ $(this).data("Atributos") +'</li>' +
				   '<li class="carrPrec">Precio: $'+$(this).data("PFinal")+'</li>' +
				   '<li class="botQuitar">Quitar Producto</li>');
	});
};

$.fn.agregarProducto = function(){
	return this.each(function(){

		//if(!$("#carrito:visible")[0]){$("#carrito").slideDown();}

		var Id = Number($("#Stock span").attr("class"));
		var Stock = $("#Stock span b").html();
		if(Id >0 && Stock >0){

				var datos_producto;

				//actualizo el objeto
				datos = $(datos).map(function(){
					if(this["IdCodigoProdInterno"] == Id){
						this["Stock"] = this["Stock"] - 1;
						datos_producto = this;
					}
					return this;
				});
				//alert(datos_producto["IdProducto"]);

				//Actualizo el LI
				$("#Stock span").html("Cantidad Disponible: <b>" + datos_producto["Stock"]+"</b>");


				var ulId = $("#carrito ul#"+Id);

				if(ulId[0]){

					ulId.data("cantidad", Number(ulId.data("cantidad"))+1);
					ulId.data("PFinal", ($.paraOtro() ? ulId.data("PVenta") : ulId.data("PInterno"))  *  ulId.data("cantidad"));

				}else{

					$("#carrito h3").after('<ul id="'+Id+'"></ul>');

					var atributos = "";
					$("#detalle select").each(function(){
						atributos += $(this).attr("id") +": "+ $(this).val()+"  ";
					});

					   ulId = $("#carrito ul#"+Id);
					   ulId.data("IdProducto", $("#detalle").data("IdProducto"));
					   ulId.data("fotoThumb", $("#detalle").data("fotoThumb"));
					   ulId.data("cantidad", 1);
					   ulId.data("PInterno",$("#detalle").data("PInterno"));
					   ulId.data("Atributos",atributos);
					   ulId.data("PVenta",$("#detalle").data("PVenta"));
					   ulId.data("NombreProducto",$("#detalle").data("NombreProducto"));
					   ulId.data("PFinal", ($.paraOtro() ? ulId.data("PVenta") : ulId.data("PInterno"))  *  ulId.data("cantidad"));
				}

				ulId.actualizarItem();

				$("#carrito").actualizarSumatoria();
		}
	});
};

$.fn.eliminarProducto = function(){
	return this.each(function(){

		if($(this).data("IdProducto") == $("#detalle").data("IdProducto")){

			var datos_producto;
			var Id = Number($(this).attr("id"));
			var cantidad = $(this).data("cantidad");

			//actualizo el objeto
			datos = $(datos).map(function(){
				if(this["IdCodigoProdInterno"] == Id){
					this["Stock"] = this["Stock"] + cantidad;
					datos_producto = this;
				}
				return this;
			});
		}

		if($("#Stock span").attr("class") == $(this).attr("id")){
			//Actualizo el LI
			$("#Stock span").html("Cantidad Disponible: <b>" + datos_producto["Stock"]+"</b>");
		}

		$(this).fadeOut("normal",function(){
			$(this).remove();
			$("#carrito").actualizarSumatoria();
		});

	});
};

$.fn.irCamProd = function(axFuncion,div_camprod){
	return this.each(function(){
	var self = this;
	var fotoThumb = $(this).attr("src");
	camprod = $(self).obtenerId();
	$.get("ajax.php?f="+axFuncion+"&p="+camprod, function(data){
		var data;
		var loaded = $(data);

		$(div_camprod).replaceWith(loaded);
		$.actualizarPrecios();

		if(axFuncion == "getAtributos"){
			$("#detalle").setearSelects();

			//	Guardo el precio en el titulo

			$("#detalle").data("PInterno",$(self).parents("ul:first").find(".PInterno").html());
			$("#detalle").data("PVenta",$(self).parents("ul:first").find(".PVenta").html());
			$("#detalle").data("NombreProducto",$(self).parents("ul").find("li:first").html());
			$("#detalle").data("IdProducto",camprod);
			$("#detalle").data("fotoThumb",$(self).parents("ul:first").find("img").attr("fotoThumb"));

			//Stock
			datos = $(datos).map(function(){

				var thisdatos = this;

				var descontar_stock = 0;
				$("#carrito ul").each(function(){
					if(thisdatos["IdCodigoProdInterno"]==$(this).attr("id")){
						descontar_stock = $(this).data("cantidad");
					}
				});

				thisdatos["Stock"] = thisdatos["Stock"] - descontar_stock;

			return thisdatos;
			});
		}
		if(axFuncion == "getProductos"){
		}

	});

	$(this).parents("ul:first").addClass("seleccionado").siblings().removeClass("seleccionado");

	});
};

//Setea los Selects en cascada
$.fn.setearSelects = function(){

		var self = $(this);
		var cant = $("select", self).size();

			for(i=1;i<cant;i++){

				$("select[name="+i+"]", self).cascade($("select[name="+(i-1)+"]", self),{
					list: datos,
					template: setupCascade.commonTemplate,
					match: setupCascade.commonMatch
				},i);

				$("select[name=1]", self).trigger("change");
			}

			$("#Stock", self).cascade($("select[name="+(cant-1)+"]", self),{
				list: datos,
				template: setupCascade.commonTemplate2,
				match: setupCascade.commonMatch
			},cant);
};

var setupCascade = {
		"commonTemplate": function (item,customI) {
			return "<option value='" + item[customI] + "'>" + item[customI] + "</option>";
		},
		"commonTemplate2": function (item,customI) {
			return "<span class=" + item["IdCodigoProdInterno"]+">Cantidad Disponible:<b>" + item["Stock"] + "</b></span>";
		},
		"commonMatch": function (selectedValue,customI){
			if(this["Stock"] < 1){
				return false;
			}

			for(i=0;customI>i;i++){
				if(this[i] != $("select[name="+i+"]").val()){
					return false;
				}
			}
			return true;
		}
	};