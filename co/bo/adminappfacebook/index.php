<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/adminappfacebook/datamappers/dmappfacebook.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
    
   
    try
    {
        ValidarUsuarioLogueadoBo(45);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
    
    // Ejecuto la logica del POST ..
    include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/adminappfacebook/actionform/actualizar_banners_facebook.php";

    $banners = dmAdminAppFacebook::getAllBanners();
    $data = dmAdminAppFacebook::getAppData();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Banners App. Facebook</title>
    <link href="<?=UrlResolver::getCssBaseUrl("bo/style/css.css")?>" rel="stylesheet" type="text/css" />
    <style>
    	div#adm-facebook-banner-container input, div#adm-facebook-banner-container a {
    		margin-top: 5px;
    		margin-bottom: 5px;
    	}
    	h1 {
    		font-size:21px;
    		font-weight:bold;
    		margin-bottom:25px;
    	}
    	p {
    		font-size: 16px;
    	}
    </style>
</head>
<body>
<h1>Banners App. Facebook</h1>
<form id="frmAdmBanners" name="frmAdmBanners" method="POST" enctype="multipart/form-data">
<?php 
echo "<div id=\"adm-facebook-banner-container\">";
foreach($banners as $banner) {
	echo "<p>Secci&oacute;n: <b>".$banner["seccion"]."</b></p>";
	echo "<br />";
	echo "<label>Imagen del banner Top: </label><input type=\"file\" name=\"".$banner["seccion"]."TopFile\" /><br />";
	$previewUrl = UrlResolver::getImgBaseUrl("front/archivos/".$banner["seccion"]."top.jpg");
	echo "<label>Preview del banner Top: </label> <a href=\"".$previewUrl."\">".$previewUrl."</a><br />";
	echo "<label>Title del banner Top: </label><input size=\"50\" type=\"text\" name=\"".$banner["seccion"]."Top"."[titleBanner]\" maxlength=\"255\" value=\"".$banner["title_banner_top"]."\" /><br />";	
	echo "<label>Href del banner Top: </label><input size=\"50\" type=\"text\" name=\"".$banner["seccion"]."Top"."[hrefBanner]\" maxlength=\"255\" value=\"".$banner["href_banner_top"]."\" /><br />";
	echo "<br />";
	echo "<label>Imagen del banner Bottom: </label><input type=\"file\" name=\"".$banner["seccion"]."BottomFile\" /><br />";
	$previewUrl = UrlResolver::getImgBaseUrl("front/archivos/".$banner["seccion"]."bottom.jpg");
	echo "<label>Preview del banner Bottom: </label> <a href=\"".$previewUrl."\">".$previewUrl."</a><br />";
	echo "<label>Title del banner Bottom: </label><input size=\"50\" type=\"text\" name=\"".$banner["seccion"]."Bottom"."[titleBanner]\" maxlength=\"255\" value=\"".$banner["title_banner_bottom"]."\" /><br />";	
	echo "<label>Href del banner Bottom: </label><input size=\"50\" type=\"text\" name=\"".$banner["seccion"]."Bottom"."[hrefBanner]\" maxlength=\"255\" value=\"".$banner["href_banner_bottom"]."\" /><br />";

	$checked = (isset($banner["activo"])&&$banner["activo"])?"checked=\"checked\"":"";
	echo "<input type=\"checkbox\" name=\"".$banner["seccion"]."[activo]\" ".$checked." /> activo<br />";	
	echo "<br /><br />";
}
echo "</div>";
?>
<p>Datos API:</p><br />

<label for="app_id">APP ID</label>
<input type="text" name="app_id" id="app_id" size="50" value="<?=$data['app_id']?>" /><br />

<label for="app_secret">APP Secret</label>
<input type="text" name="app_secret" id="app_secret" size="50" value="<?=$data['app_secret']?>" />
<br /><br />

<input type="submit" name="finalizarAdmBanners" value="Finalizar" />
</form>
</body>
</html>
