<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_transaction.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/datamappers/dmgeelbecash.php";

	error_reporting(E_ERROR | E_PARSE);
	
	if (isset($_REQUEST["filter"])) {
		foreach ($_REQUEST["filter"] as $key => $value) {
			$_REQUEST["filter"][stripslashes($key)] = $value;
		}
	}
			
	if (isset($_REQUEST["sortBy"])) {
		$_REQUEST["sortBy"] = stripslashes($_REQUEST["sortBy"]);
	}
	
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php";


    try {
        ValidarUsuarioLogueadoBo(64);
	} catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
	
	if (!isset($_REQUEST['clearFilterButton'])) {
		$paramArray = ListadoUtils::generateFiltersParamArray(array(
			"gc.id", 
			"u.NombreUsuario", 
			"DATE_FORMAT(gc.fecha,'%d/%m/%Y %H:%i')", 
			"gts.descripcion",
			"bou.NombreUsuario" 
		));
	} else {
		$paramArray = array();
	}
	$sortArray = ListadoUtils::generateSortParamArray("fecha", "DESC");
	
	$count = GeelbeCashDAO::ObtenerCantidadTransacciones($paramArray);
	$listPager = new ListadoPaginator($count);
	$rows = GeelbeCashDAO::ObtenerTransaccionesPaginadas($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage()); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Pedidos</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div id="container" class="listadocontainer">
			<h1>GCash</h1>	
			<div id="mainStats">
				<table style="margin-bottom:10px;">
					<tr class="alt">
						<td style="text-align:left;font-weight:bold;"> <a href="<?="/".$confRoot[1]."/bo/geelbecash/anular_trx_form.php"?>">Anular Transacci&oacute;n</a>&nbsp;&nbsp;<a href="<?="/".$confRoot[1]."/bo/geelbecash/cargar_credito_form.php"?>">Cargar Cr&eacute;dito</a></td>
					</tr>
				</table>
			</div>	
			<?php
				$listadoPrinter = new ListadoPrinter($rows, "index.php$parametro", $listPager);
				$listadoPrinter->showColumn("gc.id", "Id Transacci&oacute;n");
				$listadoPrinter->showColumn("u.NombreUsuario", "Usuario");
				$listadoPrinter->showColumn("DATE_FORMAT(gc.fecha,'%d/%m/%Y %H:%i')", "Fecha transacci&oacute;n");
				$listadoPrinter->showColumn("gc.monto", "Monto");
				$listadoPrinter->showColumn("gc.motivo", "Motivo");
				$listadoPrinter->showColumn("bou.NombreUsuario", "Usuario BO");
				$listadoPrinter->showColumn("gts.descripcion", "Estado");
				
				$listadoPrinter->setSortColumn("gc.monto", false);
				$listadoPrinter->setSortColumn("gc.motivo", false);
				$listadoPrinter->setShowFilter("gc.motivo", false);
				$listadoPrinter->setShowFilter("gc.monto", false);
				$listadoPrinter->setShowFilter("bou.NombreUsuario", true);
				
				$listadoPrinter->printListado();
			?>
		</div>
	</body>
</html>
