<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php";
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_service.php");
    error_reporting(E_ERROR | E_PARSE);

    try {
        ValidarUsuarioLogueadoBo(110);
	} catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
    
    require_once dirname(__FILE__).'/../../logica_bo/usuarios/datamappers/dmusuarios.php';
    require_once dirname(__FILE__).'/../../logica_bo/geelbecash/actionform/cargar_credito_batch.php';
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Pedidos</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div id="container" class="listadocontainer">
			<h1>Cargar Cr&eacute;dito GCash</h1>	
			<form action="cargar_credito_batch.php" enctype="multipart/form-data" name="cargarCreditoForm" method="post">
				<?php
					if (isset($error))
						echo '<p class="error">'.$error.'</p>';
				?>
				<label for="csv">Archivo CSV: </label>
				<input type="file" name="csv" id="csv" />
				<p>Formato: email;monto;motivo
				<br /><br />
				<input type="submit" name="cargarCreditoSubmit" value="Cargar" />
			</form>
		</div>
	</body>
</html>
