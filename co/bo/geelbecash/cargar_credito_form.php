<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php";

    try {
        ValidarUsuarioLogueadoBo(63);
	} catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Pedidos</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div id="container" class="listadocontainer">
			<h1>Cargar Cr&eacute;dito GCash</h1>	
			<form action="/<?=$confRoot[1]?>/logica_bo/geelbecash/actionform/cargar_credito.php" name="cargarCreditoForm" onsubmit="return chequearForm(this)" method="post">
				<?php
					if (isset($_REQUEST["error_type"]) && $_REQUEST["error_type"] == "invalid_user_email") {
						echo '<p class="error">Ingrese un Email de usuario v&aacute;lido</p>';
					} else if (isset($_REQUEST["error_type"]) && $_REQUEST["error_type"] == "empty_motivo") {
						echo '<p class="error">Ingrese un motivo de carga</p>';
					} else if (isset($_REQUEST["error_type"]) && $_REQUEST["error_type"] == "empty_monto") {
						echo '<p class="error">Ingrese un monto de carga</p>';
					}
				?>
				<label for="email">Email usuario: </label>
				<input type="text" name="email" id="email" value="<?=$_REQUEST["email"]?>" />
				<br /><br />
				<label for="monto">Monto carga: </label>
				<input type="text" name="monto" id="monto" value="<?=$_REQUEST["monto"]?>" />
				<br /><br />
				<label for="motivo">Motivo carga: </label><br />
				<textarea rows="5" cols="50" name="motivo" id="motivo"><?=$_REQUEST["motivo"]?></textarea>
				<br /><br />
				<input type="submit" name="cargarCreditoSubmit" value="Cargar" />
			</form>
		</div>
	</body>
	<script type="text/javascript">
		function chequearForm(form) {
			var email = form.email.value;
			var motivo = form.motivo.value;
			var monto = form.monto.value;
			if (email == null || email == '' || email == undefined || email == 0) {
				alert("Debe ingresar un Email de usuario a cargar credito");
				return false;
			}

			if (monto == null || monto == '' || monto == undefined) {
				alert("Debe ingresar un monto de carga");
				return false;
			}

			if (motivo == null || motivo == '' || motivo == undefined) {
				alert("Debe ingresar un motivo de carga");
				return false;
			}

			if (!is_numeric(monto)) {
				alert("El monto debe ser numerico");
				return false;
			}

			return confirm("�Esta seguro que desea cargar $"+ monto +" al usuario " + email + "?");
		}
		function is_numeric(value) {
			if (value == null || !value.toString().match(/^[-]?\d*\.?\d*$/)) return false;
			return true;
		}
	</script>
</html>
