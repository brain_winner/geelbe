<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_transaction.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/datamappers/dmgeelbecash.php";

	error_reporting(E_ERROR | E_PARSE);
	
	if (isset($_REQUEST["filter"])) {
		foreach ($_REQUEST["filter"] as $key => $value) {
			$_REQUEST["filter"][stripslashes($key)] = $value;
		}
	}
			
	if (isset($_REQUEST["sortBy"])) {
		$_REQUEST["sortBy"] = stripslashes($_REQUEST["sortBy"]);
	}
	
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php";


    try {
        ValidarUsuarioLogueadoBo(65);
	} catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
	
	if (!isset($_REQUEST['clearFilterButton'])) {
		$paramArray = ListadoUtils::generateFiltersParamArray(array(
			"gc.id", 
			"u.NombreUsuario",
		));
	} else {
		$paramArray = array();
	}
	$sortArray = ListadoUtils::generateSortParamArray("gca.fecha", "DESC");

	$count = GeelbeCashDAO::ObtenerCantidadTransaccionesAnuladas($paramArray);
	$listPager = new ListadoPaginator($count);
	$rows = GeelbeCashDAO::ObtenerTransaccionesAnuladasPaginadas($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage()); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Pedidos</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div id="container" class="listadocontainer">
			<h1>Transacciones GCash Anuladas</h1>	

			<?php
				$listadoPrinter = new ListadoPrinter($rows, "trx_anuladas.php$parametro", $listPager);
				$listadoPrinter->showColumn("gc.id", "Id Transacci&oacute;n");
				$listadoPrinter->showColumn("u.NombreUsuario", "Usuario");
				$listadoPrinter->showColumn("DATE_FORMAT(gca.fecha,'%d/%m/%Y %H:%i')", "Fecha anulaci&oacute;n");
				$listadoPrinter->showColumn("gca.motivo", "Motivo");
				$listadoPrinter->showColumn("ubo.NombreUsuario", "Realizada por");
				
				$listadoPrinter->setSortColumn("gca.motivo", false);
				$listadoPrinter->setShowFilter("DATE_FORMAT(gca.fecha,'%d/%m/%Y %H:%i')", false);
				$listadoPrinter->setShowFilter("ubo.NombreUsuario", false);
				$listadoPrinter->setShowFilter("gca.motivo", false);
				
				$listadoPrinter->printListado();
			?>
		</div>
	</body>
</html>
