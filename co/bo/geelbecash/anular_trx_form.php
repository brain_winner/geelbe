<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php";

    try {
        ValidarUsuarioLogueadoBo(62);
	} catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Pedidos</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div id="container" class="listadocontainer">
			<h1>Anular transacci&oacute;n GCash</h1>	
			<form action="/<?=$confRoot[1]?>/logica_bo/geelbecash/actionform/anular_trx.php" name="anularTrxForm" onsubmit="return chequearForm(this)" method="post">
				<?php
					if (isset($_REQUEST["error_type"]) && $_REQUEST["error_type"] == "invalid_trx_id") {
						echo '<p class="error">Ingrese un Id de Transacci&oacute;n v&aacute;lido</p>';
					} else if (isset($_REQUEST["error_type"]) && $_REQUEST["error_type"] == "empty_motivo") {
						echo '<p class="error">Ingrese un motivo de anulaci&oacute;n</p>';
					} else if (isset($_REQUEST["error_type"]) && $_REQUEST["error_type"] == "already_revoke_trx") {
						echo '<p class="error">La transacci&oacute;n ya se encuentra anulada, o cancelada por el usuario.</p>';
					}
				?>
				<label for="idTrx">Id Transacci&oacute;n: </label>
				<input type="text" name="idTrx" id="idTrx" value="<?=$_REQUEST["idTrx"]?>" />
				<br /><br />
				<label for="motivo">Motivo anulaci&oacute;n: </label><br />
				<textarea rows="5" cols="50" name="motivo" id="motivo"><?=$_REQUEST["motivo"]?></textarea>
				<br /><br />
				<input type="submit" name="anularTrxSubmit" value="Anular" />
			</form>
		</div>
	</body>
	<script type="text/javascript">
		function chequearForm(form) {
			var idTrx = form.idTrx.value;
			var motivo = form.motivo.value;
			if (idTrx == null || idTrx == '' || idTrx == undefined || idTrx == 0) {
				alert("Debe ingresar un Id de transaccion a anular");
				return false;
			}

			if (motivo == null || motivo == '' || motivo == undefined) {
				alert("Debe ingresar un motivo de anulacion");
				return false;
			}

			if (!is_numeric(idTrx)) {
				alert("El Id de transaccion debe ser numerico");
				return false;
			}

			return confirm("�Esta seguro que desea anular la transaccion nro. " + idTrx + "?");
		}
		function is_numeric(value) {
			if (value == null || !value.toString().match(/^[-]?\d*\.?\d*$/)) return false;
			return true;
		}
	</script>
</html>
