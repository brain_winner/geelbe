<?  
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("partnerscod"));
    try
    {
        ValidarUsuarioLogueadoBo(4);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "partnerscod");
    $oLocal = dmPartnersCod::getById((isset($_GET["IdPartner"])) ? $_GET["IdPartner"]:0);

	//Usuario
	$c = Conexion::nuevo();
	$c->Abrir_Trans();
	$c->setQuery("SELECT * FROM partners ORDER BY NombrePartner ASC");
	$partners = $c->DevolverQuery();
	$c->Cerrar_Trans();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Partners</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/partnerscod/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMLocales" name="frmABMLocales" method="post" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="idCodigoBase" name="idCodigoBase" value="<?=$oLocal->getIdCodigoBase()?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["partnerscod"]);
            ?>
            <tr>
                <td valign="top">Nombre</td>
                <td><select id="txtPartId" name="txtPartId">
				<?php
					foreach($partners as $i => $p)
						echo '<option value="'.$p['IdPartner'].'"'.($oLocal->getIdPartner() == $p['IdPartner'] ? ' selected="selected"' : '').'>'.$p['NombrePartner'].'</option>';
				?>
                </select></td>
            </tr>
            
            <tr>
                <td valign="top">C&oacute;digo</td>
                <td><input type="text" id="txtCodigo" name="txtCodigo" maxlength="80" size=60 value="<?=$oLocal->getCodigo()?>"></td>
            </tr>
            
            <tr>
                <td valign="top">Cantidad</td>
                <td><input type="text" id="txtCantidad" name="txtCantidad" maxlength="80" size=60 value="<?=$oLocal->getCantidad()?>"></td>
            </tr>
                        
            <tr>
                <td valign="top">Pixel p&aacute;gina de confirmaci&oacute;n</td>
                <td><textarea rows="5" cols="50" id="txtRegistro" name="txtRegistro" ><?=$oLocal->getPixelRegistro()?></textarea>
                <br /><small>[[[IdUsuario]] identificador num&eacute;rico, &uacute;nico, para el usuario registrado<br />
	            [[[NombreUsuario]] email del usuario registrado<br />
                </small></td>
            </tr>
                        
            <tr>
                <td valign="top">Pixel p&aacute;gina de activaci&oacute;n</td>
                <td><textarea rows="5" cols="50" id="txtActivacion" name="txtActivacion" ><?=$oLocal->getPixelActivacion()?></textarea>
                <br /><small>[[[IdUsuario]] identificador num&eacute;rico, &uacute;nico, para el usuario activado<br />
	            [[[NombreUsuario]] email del usuario activado<br />
                </small></td>
            </tr>
                        
            <tr>
                <td valign="top">Activaci&oacute;n Autom&aacute;tica</td>
                <td><input type="checkbox" id="txtAuto" name="txtAuto" value="1" <?=($oLocal->getAuto() == 1 ? 'checked="checked"' : '')?>>
                <br /><small>Los usuarios registrados a trav&eacute;s de este c&oacute;digo, se activar&aacute;n autom&aacute;ticamente</small></td>
            </tr>
                        
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].submit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>