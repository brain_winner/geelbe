<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("proveedores"));
    try
    {
        ValidarUsuarioLogueadoBo(10);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "proveedores");
    $objProveedor = dmProveedor::getByIdProveedor((isset($_GET["IdProveedor"])) ? $_GET["IdProveedor"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM proveedores</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/proveedores/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMProveedors" name="frmABMProveedors" method="POST" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="txtIdProveedor" name="txtIdProveedor" value="<?=($objProveedor->getIdProveedor() >0)?$objProveedor->getIdProveedor():0?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["proveedores"]);
            ?>
            <tr>
                <td>Nombre</td>
                <td><input type="text" size=50  id="txtNombre" name="txtNombre" maxlength="80" size=50 value="<?=$objProveedor->getNombre()?>"></td>
            </tr>
            <tr>
                <td>Direccion</td>
                <td>
                <input type="text" size=50  id="txtDireccion" name="txtDireccion"  value="<?=$objProveedor->getDireccion()?>"/>
                </td>
            </tr>
            <tr>
                <td>CP</td>
                <td>
                <input type="text" size=50  id="txtCP" name="txtCP"  value="<?=$objProveedor->getCP()?>"/>
                </td>
            </tr>
            <tr>
                <td>Localidad</td>
                <td>
                <input type="text" size=50  id="txtLocalidad" name="txtLocalidad"  value="<?=$objProveedor->getLocalidad()?>"/>
                </td>
            </tr>
            <tr>
                <td>Provincia</td>
                <td>
                    <select id="cbxIdProvincia" name="cbxIdProvincia">
                        <option value=0>...Ninguna...</option>
                        <?
                          $dt_provincias = dmCliente::GetProvincias();
                          foreach($dt_provincias as $provincia)
                          {
                              ?><option value="<?=$provincia["IdProvincia"]?>"><?=$provincia["Nombre"]?></option><?
                          }
                        ?>
                    </select>
                    <?
                      if($objProveedor->getIdProvincia()!="")
                      {
                          ?>
                            <script>
                                document.getElementById("cbxIdProvincia").value="<?=$objProveedor->getIdProvincia()?>";
                            </script>
                          <?
                      }
                    ?>
                </td>
            </tr>
            <tr>
                <td>CIF</td>
                <td>
                <input type="text" size=50  id="txtCIF" name="txtCIF"  value="<?=$objProveedor->getCIF()?>"/>
                </td>
            </tr>
            <tr>
                <td>Telefono</td>
                <td>
                <input type="text" size=50  id="txtTelefono" name="txtTelefono"  value="<?=$objProveedor->getTelefono()?>"/>
                </td>
            </tr>
            <tr>
                <td>Fax</td>
                <td>
                <input type="text" size=50  id="txtFax" name="txtFax"  value="<?=$objProveedor->getFax()?>"/>
                </td>
            </tr>
            <tr>
                <td>WebSite</td>
                <td>
                <input type="text" size=50  id="txtWebSite" name="txtWebSite"  value="<?=$objProveedor->getWebSite()?>"/> 
                </td>
            </tr>
            <tr>
                <td>Habilitado</td>
                <td>
                    <input type="checkbox" id="chkHabilitado" name="chkHabilitado" value="OK">
                    <script>
                        <?
                            if($objProveedor->getHabilitado() == 1)
                            {
                                ?>document.getElementById("chkHabilitado").checked=true;<?
                            }
                        ?>
                    </script>
                </td>
            </tr>
            <tr>
                <td>Comentarios</td>
                <td>
                <textarea id="txtComentarios" name="txtComentarios" rows="5" style="width:90%"><?=$objProveedor->getComentarios()?></textarea>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/>
                    </a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/>
                </a>
                </td>
            </tr>
        </table>
    </form>  
    <iframe style="display:none" id="iProveedors" name="iProveedors" width="100%"></iframe>
</body>
</html>