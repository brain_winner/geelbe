function ErrorMSJ(code, def)
{
    if(arrErrores["PROVEEDORES"][code] != undefined)
        document.getElementById("msjError").innerHTML = arrErrores["PROVEEDORES"][code];
    else
        document.getElementById("msjError").innerHTML = code;
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtNombre.value.length == 0)
        {
            arrError.push(arrErrores["PROVEEDORES"]["NOMBRE"]);
        }                      
        if (arrError.length>0)
        {
            var error = ""
            for(i=0; i<arrError.length; i++)
            {
                error +="*" + arrError[i] + "\n";
            }
            alert(error);
            return false;
        }
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}
function irVolver()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"proveedores/index.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["PROVEEDORES"]["SELECCIONE"]);
        else
            window.location.href="ABM.php?IdProveedor="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVer()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["PROVEEDORES"]["SELECCIONE"]);
        else
            window.location.href="Ver.php?IdProveedor="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["PROVEEDORES"]["SELECCIONE"]);
        else
        {
            if(confirm(arrErrores["PROVEEDORES"]["ELIMINAR"]))
                window.location.href="../../logica_bo/PROVEEDORES/actionform/eliminar_proveedores.php?IdProveedor="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}
function irCategoria()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["PROVEEDORES"]["SELECCIONE"]);
        else
        {
            window.location.href="index_categorias.php?IdProveedor="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}