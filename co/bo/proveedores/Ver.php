<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("proveedores"));
    try
    {
        ValidarUsuarioLogueadoBo(10);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $objProveedor = dmProveedor::getByIdProveedor((isset($_GET["IdProveedor"])) ? $_GET["IdProveedor"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM proveedores</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/proveedores/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMProveedors" name="frmABMProveedors" target="iProveedors" action="../../../<?=Aplicacion::getDirLocal().$postURL["proveedores"]?>" method="POST" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="txtIdProveedor" name="txtIdProveedor" value="<?=($objProveedor->getIdProveedor() >0)?Aplicacion::Encrypter($objProveedor->getIdProveedor()):0?>">
        <table>
            <tr>
                <td>Nombre</td>
                <td><b><?=$objProveedor->getNombre()?></b></td>
            </tr>
            <tr>
                <td>Direccion</td>
                <td>
                <b><?=$objProveedor->getDireccion()?></b>
                </td>
            </tr>            
            <tr>
                <td>CP</td>
                <td>
                <b><?=$objProveedor->getCP()?></b>
                </td>
            </tr>
            <tr>
                <td>Localidad</td>
                <td>
                <b><?=$objProveedor->getLocalidad()?></b>
                </td>
            </tr>
            <tr>
                <td>Provincia</td>
                <td>
                <b><?=$objProveedor->getIdProvincia()?></b>
                </td>
            </tr>
            <tr>
                <td>CIF</td>
                <td>
                <b><?=$objProveedor->getCIF()?></b>
                </td>
            </tr>
            <tr>
                <td>Telefono</td>
                <td>
                <b><?=$objProveedor->getTelefono()?></b>
                </td>
            </tr>
            <tr>
                <td>Fax</td>
                <td>
                <b><?=$objProveedor->getFax()?></b>
                </td>
            </tr>
            <tr>
                <td>WebSite</td>
                <td>
                <b><?=$objProveedor->getWebSite()?></b> 
                </td>
            </tr>
            <tr>
                <td>Comentarios</td>
                <td><b><?=$objProveedor->getComentarios()?></b>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnVolver','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver.gif");?>" alt="Volver" name="btnVolver" id="btnVolver" border="0"/></a>
                </a>
                </td>
            </tr>
        </table>
    </form>  
    <iframe style="display:none" id="iProveedors" name="iProveedors" width="100%"></iframe>
</body>
</html>