<?php
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");

        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/hardbounces/datamappers/dmhardbounces.php");
    } 
    catch(Exception $e) {
        header('HTTP/1.1 500 Internal Server Error');
        include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
        exit;
    }
    try {
        ValidarUsuarioLogueadoBo(12);
    }
    catch(Exception $e) {
        header('Location: /'.$confRoot[1].'/bo');
        exit;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title><?=Aplicacion::getParametros("info", "nombre");?> Importar archivo HardBounces</title>
        <link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
        <script src="<?=UrlResolver::getJsBaseUrl("bo/hardbounces/js.js");?>" type="text/javascript"></script>

    </head>

    <body>

        <div id="container" class="listadocontainer">
            <h1>Importar archivo HardBounces</h1>    

        <?php 
            
            if(isset($_POST['import']) && is_uploaded_file($_FILES['csv']['tmp_name'])) {
                $hardBounces = explode("\n", file_get_contents($_FILES['csv']['tmp_name']));
                
                foreach ($hardBounces as $i => $email) {
                    if ($email != '') {
                        $oUsuario = dmHardBounces::insertEmail($email);
                        echo $email." - Set as Hard Bounce.<br/>";
                    }
                }
            }
        ?>

        <p style="clear: left; padding-top: 20px;">Se marcar&aacute;n los mails como hard bounces por una semana.</p>

        <form action="hard-bounces.php" method="post" enctype="multipart/form-data" style="margin: 20px">
            
            <label for="csv" style="display:block">Selecciona el archivo CSV:</label>
            <input type="file" name="csv" id="csv" />
            
            <input type="submit" name="import" value="Importar" />
            
        </form>
        
    </body>
</html>
