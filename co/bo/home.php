<?

	define("TODOS", 0);
	define("USUARIOS_ACTIVOS", 1);
	define("USUARIOS_INACTIVOS", 2);

    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/estadisticas/commons.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
    
    set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
	require_once 'Zend/Cache.php';
    
    try
    {
        ValidarUsuarioLogueadoBo(11);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    
    try {
		$countActivos = dmCliente::getClientesActivosCount(array());
    }
    catch(Exception $e) {
		echo $e;
		exit;
    }
    
    $_GET['mes'] = date("m");
    $_GET['dia'] = date("d");
    $_GET['anio'] = date("Y");
    
	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
	$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 600, 'automatic_serialization' => true), array('cache_dir' => $cacheDir));

	function registrados($tipoUsuario = null, $intervalDays = 0) {
		global $cache;

		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_registrados_tipodeusuario_'.$tipoUsuario.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"].'_dias_'.$intervalDays;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_registrados_tipodeusuario_'.$tipoUsuario.'_dias_'.$intervalDays;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if ($tipoUsuario == USUARIOS_ACTIVOS) {
			$tipoUsuarioWhere = ' AND u.es_activa = 0 AND u.IdPerfil = 1';
		} else if ($tipoUsuario == USUARIOS_INACTIVOS) {
			$tipoUsuarioWhere = ' AND u.es_activa = 1 AND u.es_Provisoria = 1 AND u.Padrino <> "Huerfano" AND u.IdPerfil = 1';
		} else {
			$tipoUsuarioWhere = '';
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT COUNT(u.IdUsuario) as registrados FROM usuarios u WHERE u.FechaIngreso BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'" '.$tipoUsuarioWhere;

		    $oConexion = Conexion::nuevo();
			$oConexion->setQuery($query);
			$res = $oConexion->DevolverQuery();
			
			if($res[0]["registrados"] == null) {
				$res[0]["registrados"] = 0;
			}
			
			$cache->save($res[0]["registrados"], $nombreCache);
			return $res[0]["registrados"];
		} else {
			return $contenidoCache;
		}
	}

	function pedidosPagoAceptado($intervalDays = 0) {
		global $cache;
		
		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_pedidos_pago_aceptado_dias_'.$intervalDays.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"];
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_pedidos_pago_aceptado_dias_'.$intervalDays;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT COUNT(p.IdPedido) as pedidos FROM pedidos p WHERE p.IdEstadoPedidos IN (2, 3, 6, 7, 9) AND p.Fecha BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'"';
	
		    $oConexion = Conexion::nuevo();
			$oConexion->setQuery($query);
			$res = $oConexion->DevolverQuery();
			
			if($res[0]["pedidos"] == null) {
				$res[0]["pedidos"] = 0;
			}
			
			$cache->save($res[0]["pedidos"], $nombreCache);
			return $res[0]["pedidos"];
		} else {
			return $contenidoCache;
		}
	}
	
	function montoPedidosPagoAceptado($intervalDays = 0, $SinCostoDeEnvio = false) {
		global $cache;
		
		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_monto_pedidos_pago_aceptado_dias_'.$intervalDays.'_costo_de_envio_'.$SinCostoDeEnvio.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"];
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_monto_pedidos_pago_aceptado_dias_'.$intervalDays.'_costo_de_envio_'.$SinCostoDeEnvio;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if($SinCostoDeEnvio) {
			$SinCostoDeEnvioText = " - p.GastosEnvio";
		} else {
			$SinCostoDeEnvioText = "";
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT SUM(p.Total'.$SinCostoDeEnvioText.') as pedidos FROM pedidos p WHERE p.IdEstadoPedidos IN (2, 3, 6, 7, 9) AND p.Fecha BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'"';
	
		    $oConexion = Conexion::nuevo();
			$oConexion->setQuery($query);
			$res = $oConexion->DevolverQuery();
			
			if($res[0]["pedidos"] == null) {
				$res[0]["pedidos"] = 0;
			}
			
			$cache->save($res[0]["pedidos"], $nombreCache);
			return $res[0]["pedidos"];
		} else {
			return $contenidoCache;
		}
	}

	function logins($intervalDays = 0) {
		global $cache;
		
		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_logins'.'_dias_'.$intervalDays.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"];
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_logins'.'_dias_'.$intervalDays;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT count(idLogin) as logins FROM logins WHERE Fecha BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'"';
			
			$usuarioHistorico = Aplicacion::getParametros("conexionh", "usuario");
		    $passwordHistorico = Aplicacion::Decrypter(Aplicacion::getParametros("conexionh", "password"));
		    $serverHistorico = Aplicacion::getParametros("conexionh", "server");
		    $baseHistorico = Aplicacion::getParametros("conexionh","base");
	
			$loginDB = mysql_connect($serverHistorico, $usuarioHistorico, $passwordHistorico, true);
			mysql_select_db($baseHistorico, $loginDB);
			$result = mysql_query($query, $loginDB);
			$res = array();
			
			while ($fila = mysql_fetch_assoc($result)) {
				array_push($res, $fila);
			}

			$registrados = array();
			foreach($res as $result) {
				$registrados[$result["mes"]] = $result["logins"];
			}
	
			if($res[0]["logins"] == null) {
				$res[0]["logins"] = 0;
			}
			
			$cache->save($res[0]["logins"], $nombreCache);
			return $res[0]["logins"];
		} else {
			return $contenidoCache;
		}
	}
	
	$registrados = array(
		75 => 'red',
		125 => '#FFCC00',
		'max' => 'green'
	);
	
	$pedidos = array(
		95 => 'red',
		145 => '#FFCC00',
		'max' => 'green'
	);
	
	$ventas = array(
		5000000 => 'red',
		10000000 => '#FFCC00',
		'max' => 'green'
	);
	
	$ratio = array(
		199 => 'green',
		299 => '#FFCC00',
		'max' => 'green'
	);	
	
	function color($array, $val) {
		
		$last = '';
		foreach($array as $max => $color) {
			
			if($val <= $max)
				return $color;
				
			$last = $color;
			
		}
		
		return $color;
		
	}
	
	$meses = array(1 => 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$mes = $meses[intval(date("m"))];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Estad&iacute;sticas</title>
	<link href="/<?=$confRoot[1]?>/bo/css/global.css" rel="stylesheet" type="text/css" />
	<link href="/<?=$confRoot[1]?>/css/listado_bo.css" rel="stylesheet" type="text/css" />
	<link href="/<?=$confRoot[1]?>/css/jquery.ui.datepicker/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
	
	<script src="<?="/".$confRoot[1]."/js/jquery-1.4.2.min.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/js/jquery.ui.core.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/js/jquery.ui.datepicker.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/js/jquery.datepick-es.js"?>" type="text/javascript"></script>
</head>

<body>
<style type="text/css">
	th {
		background-color: #ffefef;
	}
	
	td.value {
		text-align:center;
		font-weight: bold;
		font-size: 2em
	}
	
	p strong {
		color: #D33
	}
</style>

  <div id="container">
	<h1>Estad&iacute;sticas Geelbe Colombia</h1>
		
	<br /><br />
	<table>
	  <tr  style="text-align:center; font-weight:bold;color:#DD3333">
		<th NOWRAP width="50%">Concepto: <?=strftime("%d de ".$mes." de %Y")?></th>
		<th NOWRAP></th>
	  </tr>
	  <? 
		  $activos = registrados(USUARIOS_ACTIVOS);
		  $pedidosPagoAceptado = pedidosPagoAceptado();
		  $montoPedidosPagoAceptado = montoPedidosPagoAceptado();
		  $logins = logins();
		  $ratioVal = ($pedidosPagoAceptado==0)?0:number_format($logins/$pedidosPagoAceptado, 2, ',', '');
		  
		  next($registrados);
		  $restanUsuarios = key($registrados) - $activos;

		  next($pedidos);
		  $restasPedidos = key($pedidos) - $pedidosPagoAceptado;

		  next($ventas);
		  $restanVentas = key($ventas) - $montoPedidosPagoAceptado;
	  ?>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Registrados activos</td>
		<td NOWRAP class="value" style="color: <?=color($registrados, $activos)?>"><?= $activos ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Cantidad de Pedidos con Pago Aceptado</td>
		<td NOWRAP class="value" style="color: <?=color($pedidos, $pedidosPagoAceptado)?>"><?= $pedidosPagoAceptado ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Monto de Pedidos con Pago Aceptado</td>
		<td NOWRAP class="value" style="color: <?=color($ventas, $montoPedidosPagoAceptado)?>"><?= Moneda($montoPedidosPagoAceptado) ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Ratio de Logins/Cantidad de Ventas</td>
		<td NOWRAP class="value" style="color: <?=color($ratio, $ratioVal)?>"><?= $ratioVal ?></td>
	  </tr>
	</table>
  </div>

</body>
</html>