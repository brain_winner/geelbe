<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("cuotas"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("tarjetas"));
    try
    {
        ValidarUsuarioLogueadoBo(10);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "cuotas");
    $objProveedor = dmCuota::getByIdCuota((isset($_GET["IdCuota"])) ? $_GET["IdCuota"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Asociaciones</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/cuotas/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMBancos" name="frmABMBancos" method="POST" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="txtIdCuota" name="txtIdCuota" value="<?=($objProveedor->getIdCuota() >0)? $objProveedor->getIdCuota():0?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["cuotas"]);
            ?>
            <tr>
                <td>Cuota</td>
                <td><input type="text" id="txtCuota" name="txtCuota" value="<?=$objProveedor->getCuota();?>" /></td>
            </tr>
            <tr>
                <td>Tarjeta</td>
				<td><select id="txtIdTarjeta" name="txtIdTarjeta">
                	<?php
                		$bancos = dmTarjeta::getTarjetas('nombre', 'ASC');
                		foreach($bancos as $i => $banco)
                			echo '<option value="'.$banco['IdTarjeta'].'"'.($banco['IdTarjeta'] == $objProveedor->getIdTarjeta() ? ' selected="selected"': '').'>'.$banco['Nombre'].'</option>';
                	?>
                </select></td>
            </tr>
             <tr>
                <td>Intereses</td>
                <td><input type="text" id="txtIntereses" name="txtIntereses" value="<?=$objProveedor->getPorcentaje();?>" /> %</td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].submit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/>
                    </a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/>
                </a>
                </td>
            </tr>
        </table>
    </form>  
    <iframe style="display:none" id="iBancos" name="iBancos" width="100%"></iframe>
</body>
</html>