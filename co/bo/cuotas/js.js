function ErrorMSJ(code, def)
{
    if(arrErrores["CUOTAS"][code] != undefined)
        document.getElementById("msjError").innerHTML = arrErrores["CUOTAS"][code];
    else
        document.getElementById("msjError").innerHTML = code;
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtNombre.value.length == 0)
        {
            arrError.push(arrErrores["CUOTAS"]["NOMBRE"]);
        }                      
        if (arrError.length>0)
        {
            var error = ""
            for(i=0; i<arrError.length; i++)
            {
                error +="*" + arrError[i] + "\n";
            }
            alert(error);
            return false;
        }
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}
function irVolver()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"cuotas/index.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CUOTAS"]["SELECCIONE"]);
        else
            window.location.href="ABM.php?IdCuota="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVer()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CUOTAS"]["SELECCIONE"]);
        else
            window.location.href="Ver.php?IdCuota="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar(id)
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CUOTAS"]["SELECCIONE"]);
        else
        {
            if(confirm(arrErrores["CUOTAS"]["ELIMINAR"]))
                window.location.href="../../logica_bo/cuotas/actionform/eliminar_cuotas.php?IdCuota="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}