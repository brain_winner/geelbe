function ValidarForm(frm)
{
    try
    {
        var errores = "";
        var Estado = -1; 
        for(i=0; i<frm.cbxEstado.length; i++)
        {
            if(frm.cbxEstado[i].checked)
            {
                Estado = frm.cbxEstado[i].value;
            }
        }
        if(Estado == 5)
        {
            if(frm.txtMotivoAnulado.value.length == 0)
            {
            alert(arrErrores["PEDIDOS"]["MOTIVO"]);
                return false;
            }
            if(!confirm(arrErrores["PEDIDOS"]["ANULAR"]))
                return false
        }
        if(Estado == 6)
        {
            if(!confirm(arrErrores["PEDIDOS"]["ENTREGAR"]))
                return false
        }
        else if(Estado == 3)
        {
            if(frm['FechaEnvio[cbxDia]'].value == 0 || frm['FechaEnvio[cbxMes]'].value == 0 || frm['FechaEnvio[cbxAnio]'].value == 0)
            {
                errores +="*"+arrErrores["PEDIDOS"]["FECHA_ENVIO"]+"\n";
            }
            else
            {
                if(!EsFechaValida(frm['FechaEnvio[cbxDia]'].value,frm['FechaEnvio[cbxMes]'].value,frm['FechaEnvio[cbxAnio]'].value))
                {
                    errores += "*"+arrErrores["PEDIDOS"]["FECHAINVALIDA"]+"\n";   
                }
            }
            if(frm.txtNroEnvio.value.length == 0)
            {
                errores +="*"+arrErrores["PEDIDOS"]["NROENVIO"]+"\n";
            }
        }
        if(errores.length>0)
        {
            alert(errores);
            return false;
        }
        if(Estado == -1)
        {
            irVolver();
        }
        else
        {
            frm.submit();
        }
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["PEDIDOS"]["SELECCIONE"]);
        else
            window.location.href="ABM.php?IdPedido="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVer()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["PEDIDOS"]["SELECCIONE"]);
        else
            window.location.href="Ver.php?IdPedido="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVolver()
{
    window.location.href=DIRECTORIO_URL_BO+"pedidos/index.php";  
}
function verCliente(id)
{
    window.location.href=DIRECTORIO_URL_BO+"clientes/Ver.php?IdUsuario="+id;
}
function irVolverListado()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"pedidos/index.php";
    }
    catch(e)
    {
        throw e;
    }
}