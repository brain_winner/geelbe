<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/mailer/clsMailer.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/clases/clsusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/pedidos/clsinf.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmcupones.php");
    try
    {
        ValidarUsuarioLogueadoBo(12);
    }
    catch(ACCESOException $e)
    {
    ?>
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
    $postURL = Aplicacion::getIncludes("post", "pedidos");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es-ar" dir="ltr">
	<head>
	    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Pedidos</title>
	    <?
	        Includes::Scripts();
	    ?>
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/pedidos/js.js");?>" type="text/javascript"></script>
	    <link rel="stylesheet" type="text/css" href="<?=UrlResolver::getCssBaseUrl("bo/pedidos/AMB_pedidos.css")?>" media="screen" />
	</head>

	<body>
	    <?
	        require_once(Aplicacion::getRoot().$postURL["pedidos"]);
	    ?>

            <h3>C&oacute;digo de Compra: <?=str_pad($objPedido->getIdPedido(),Aplicacion::getParametros("pedidos", "cantidad"),0,0)?></h3>

            <?

		$divInfcontPedido = new htmlInfContPedido($objPedido,$Bonos[0]["Bono"]);
		echo $divInfcontPedido->imprimir();


		$divInfPago = new htmlInfPago($InfoPedido["FormaPago"]["Descripcion"]);
		 echo $divInfPago->imprimir();


		$divInfEstPed = new htmlInfEstPed($InfoPedido["Estado"]["Descripcion"],$objPedido->getFechaEnvio(), dmCupones::getIdCuponByIdPedido($objPedido->getIdPedido()),$objPedido->getIdPedido(),$objPedido->getNroEnvio(),$objPedido->getMotivoAnulacion());
		echo $divInfEstPed->imprimir();


		$divInfDatComp = new htmlInfDatComp($objUsuario->getDatos(),$arrayPedidosDirecciones);
		echo $divInfDatComp->imprimir();

		$divInfDirEnv = new htmlInfDirEnv($objPedido->getDireccion());
		echo $divInfDirEnv->imprimir();

 ?>

     	<div><a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnVolver','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver.gif");?>" alt="Volver" name="btnVolver" id="btnVolver" border="0"/></a></div>
</body>
</html>
