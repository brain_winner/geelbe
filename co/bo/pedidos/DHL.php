<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("pedidos-bo"));
		$POST = Aplicacion::getIncludes("post", "pedidos");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(12);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
	try {
	    if(isset($_POST['import']) && is_uploaded_file($_FILES['csv']['tmp_name'])) {
	    	$resultados = dmPedidos::DHLCSV(file_get_contents($_FILES['csv']['tmp_name']));
	    	header("Location: DHL.php?csv=".implode(',',$resultados["updated"])."&csvError=".implode(',',$resultados["pedidosError"]));
	    	die;
	    }
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		echo $e;
		exit;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Importar archivo Envio</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
    
		<? Includes::Scripts(); ?>
		<script src="<?=UrlResolver::getJsBaseUrl("bo/pedidos/js.js");?>" type="text/javascript"></script>

	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Importar archivo Envio</h1>	

<?php 
	if(isset($_GET['csv'])) {
		
		if($_GET['csv'] != '')	
		$pedidos = explode(',',$_GET['csv']);
		else
		$pedidos = array();
		
		if($_GET['csvError'] != '')
		$pedidoserror = explode(',',$_GET['csvError']);
		
		else
		$pedidoserror = array();
		
		if(count($pedidos) > 0 || count($pedidoserror) > 0) {
		if(count($pedidos) > 0) {
		echo '<p style="margin: 20px"><strong>Los siguientes '.count($pedidos).' pedidos fueron actualizados satisfactoriamente.</strong></p>
		<ul style=" margin: 20px 0 20px 50px">';
		foreach($pedidos as $i => $id)
		echo '<li style="font-size: 10px; float: left; width: 130px"> ID: '.$id.'</li>';
		echo '</ul>';
		echo '<br /><br />';
		}
		if(count($pedidoserror) > 0) {
		echo '<p style="margin: 20px"><strong>Los siguientes '.count($pedidoserror).' pedidos no fueron procesados (verifique los mismos).</strong></p>
					<ul style=" margin: 20px 0 20px 50px">';
		foreach($pedidoserror as $i => $iderror)
		echo '<li style="font-size: 10px; float: left; width: 130px"> ID: '.$iderror.'</li>';
		echo '</ul>';
		}
		} else {
		echo '<p style="margin-top: 20px"><strong>El env&iacute;o de todos los pedidos del archivo ya estaba programado. No se actualiz&oacute; ning&uacute;n pedido.</strong></p>';
		}
		}?>

		<p style="clear: left; padding-top: 20px;">Se marcar&aacute;n los pedidos como <?=(isset($_GET['entregados']) ? 'entregados' : 'enviados')?> y se les asignar&aacute; el Tracking Code especificado en el archivo CSV.<br />
		Formato: Id Pedido; Tracking Code.</p>

		<form action="<?=$_SERVER['REQUEST_URI']?>" method="post" enctype="multipart/form-data" style="margin: 20px">
			
			<label for="csv" style="display:block">Selecciona el archivo CSV:</label>
			<input type="file" name="csv" id="csv" />
			
			<input type="submit" name="import" value="Importar" />
			
		</form>
		
	</body>
</html>
