<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/mailer/clsMailer.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/clases/clsusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/pedidos/clsinf.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmcupones.php");
    
    try
    {
        ValidarUsuarioLogueadoBo(12);
    }
    catch(ACCESOException $e)
    {
    ?>
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
    $postURL = Aplicacion::getIncludes("post", "pedidos");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es-ar" dir="ltr">
	<head>
	    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Pedidos</title>
	    <?
	        Includes::Scripts();
	    ?>
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/pedidos/js.js");?>" type="text/javascript"></script>
	    <script src="http://code.jquery.com/jquery.js"></script>
	    <script src="<?=UrlResolver::getJsBaseUrl("ssl/carrito/js/shipping_info.js");?>" type="text/javascript"></script>
	    <link rel="stylesheet" type="text/css" href="<?=UrlResolver::getCssBaseUrl("bo/pedidos/AMB_pedidos.css")?>" media="screen" />
	</head>

	<body>
	    <?
	        require_once(Aplicacion::getRoot().$postURL["pedidos"]);
	    ?>

            <h3>C&oacute;digo de Compra: <?=str_pad($objPedido->getIdPedido(),Aplicacion::getParametros("pedidos", "cantidad"),0,0)?></h3>

            <?

		$divInfcontPedido = new htmlInfContPedido($objPedido,$Bonos[0]["Bono"]);
		echo $divInfcontPedido->imprimir();
		
		$divInfPago = new htmlInfPago($InfoPedido["FormaPago"]["Descripcion"]);
		 echo $divInfPago->imprimir();


		$divInfEstPed = new htmlInfEstPed($InfoPedido["Estado"]["Descripcion"],$objPedido->getFechaEnvio(), dmCupones::getIdCuponByIdPedido($objPedido->getIdPedido()),$objPedido->getIdPedido(),$objPedido->getNroEnvio(),$objPedido->getMotivoAnulacion());
		echo $divInfEstPed->imprimir();

		$divCambiarEstPed = new htmlCambiarEstPed($objPedido->getIdPedido(),$objPedido->getIdEstadoPedidos(),$fechaFinCampania,$objPedido->getFechaEnvio(),$objPedido->getNroEnvio());
		echo $divCambiarEstPed->imprimir();

		$divInfDatComp = new htmlInfDatComp($objUsuario->getDatos(),$arrayPedidosDirecciones);
		echo $divInfDatComp->imprimir();

		/*$divInfDirEnv = new htmlInfDirEnv($objPedido->getDireccion());
		echo $divInfDirEnv->imprimir();*/
		
		$dir = $objPedido->getDireccion();

 ?>
		<div id="dirEnv" style="float:left"> 
	 		<form action="ABM.php?IdPedido=<?=$objPedido->getIdPedido()?>" method="post" id="saveAddress">
	 			<h4>Direcci&oacute;n de env&iacute;o</h4>
	 					
			     <p><label>Direcci&oacute;n:</label><br />
			     <input name="frmDir[txtDomicilio]" type="text" size="50" class="imputbportada" value="<?=$dir->getDomicilio()?>" id="txtDomicilio" gtbfieldid="52"></p>
				
				 <p>
				 	<label>Departamento:</label>
				 	<br />
				 	<select name="frmDi1[cbxProvincia]" id="idProvincia" class="imputbportada" onchange="javascript:actualizarCiudades()"> 
                  <option value="0" >Departamentos</option>
					<? 
                        $dt = dmMicuenta::GetProvincias();
						$objDireccion = $dir;
                        foreach($dt as $value)
                        {
							if($objDireccion->getIdProvincia() == $value['IdProvincia']) {
								?>
									<option selected="selected" value='<?=$value['IdProvincia']?>'><?=$value['Nombre']?></option>
								<?
							} else {
								?>
								   <option value='<?=$value['IdProvincia']?>'><?=$value['Nombre']?></option>
								<?
							}  
						}
					?>
				</select>
				</p>  

				<p><label>Ciudad</label><br>
				<select name="frmDi1[cbxCiudad]" id="idCiudad" class="imputbportada"> 
                  <option value="0" >Ciudades</option>
                         <?
                         if($dir->getIdProvincia() != null) { 
	                         $dt = dmMicuenta::GetCiudades($dir->getIdProvincia());
	                         foreach($dt as $value)
	                         {
	                         	if($dir->getIdCiudad() == $value['IdCiudad']) {
		                         	?>
		                               <option selected="selected" value='<?=$value['IdCiudad']?>'><?=$value['Descripcion']?></option>
		                            <?
	                         	} else {
		                            ?>
		                               <option value='<?=$value['IdCiudad']?>'><?=$value['Descripcion']?></option>
		                            <?
	                         	}
	                         }
                         }                                                 
                  ?>                                            
                </select>
                                
                <p>
                	C&eacute;dula<br />
                	<input name="frmDaU[txtNroDoc]" id="txtNroDoc" type="text" size="11" maxlength="11" value="<?=$objUsuario->getDatos()->getNroDoc();?>" />
                </p>
                
                <p>
                	Tel&eacute;fono M&oacute;vil<br />
	                <!--<select name='frmDaU[cmb_CelularComp]' id='cmb_CelularComp'>
	                    <option value="0">Compa&ntilde;ia</option>
	                    <option value="1" <?=($objUsuario->getDatos()->getCeluEmpresa() == 1 ? 'selected' : '')?>>COMCEL</option>
	                    <option value="2" <?=($objUsuario->getDatos()->getCeluEmpresa() == 2 ? 'selected' : '')?>>MOVISTAR</option>
	                    <option value="3" <?=($objUsuario->getDatos()->getCeluEmpresa() == 3 ? 'selected' : '')?>>TIGO</option>
	                    <option value="4" <?=($objUsuario->getDatos()->getCeluEmpresa() == 4 ? 'selected' : '')?>>OTRO</option>
	                </select>-->
	                <input name='frmDaU[txt_CelularCodigo]' type='text' id='txt_CelularCodigo' size="3" maxlength="6" value="<?=$objUsuario->getDatos()->getcelucodigo()?>" />
	                <input name='frmDaU[txt_CelularNumero]' type='text' id='txt_CelularNumero' size="14" maxlength="15" value="<?=$objUsuario->getDatos()->getcelunumero()?>" />
	            </p>
	                            
		        <input type="hidden" id="txtIdPedido" name="txtIdPedido" value="<?=Aplicacion::Encrypter($objPedido->idPedido)?>" />
		        
		        <input type="submit" name="saveAddress" value="Guardar" />

	 		</form>
		</div>
		
		<script>
			$('#saveAddress').submit(function() {
				return saveBOAddress();
			})
			function saveBOAddress() {
				if($('#txtDomicilio').val() == '') {
					alert('Debe completar el campo Direcci�n.');
					return false;
				}
				
				if($('#cbxProvincia').val() == 0) {
					alert('Debe seleccionar un departamento.');
					return false;
				}
				
				if($('#cbxCiudad').val() == 0) {
					alert('Debe seleccionar una ciudad.');
					return false;
				}

				if($('#txtNroDoc').val() == '') {
					alert('Debe completar el campo c�dula.');
					return false;
				}
			
				/*if($('#cmb_CelularComp').val() == 0) {
					alert('Debe seleccionar una empresa de telefon�a.');
					return false;
				}*/
				
				if($('#txt_CelularCodigo').val() == '') {
					alert('Debe completar el campo tel�fono.');
					return false;
				}
				
				if($('#txt_CelularNumero').val() == '') {
					alert('Debe completar el campo tel�fono.');
					return false;
				}
				
				return true;
			}
		</script>
 
</body>
</html>