<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("pedidos-bo"));
		$POST = Aplicacion::getIncludes("post", "pedidos");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(12);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
	try {		
		
		if(isset($_REQUEST['padrino'])) {
	    	
	    	$oConexion = Conexion::nuevo();
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("SELECT 	P.IdPedido as 'P.IdPedido',
											U.NombreUsuario as 'U.NombreUsuario', 
											D.Nombre as 'D.Nombre',
											D.Apellido as 'D.Apellido', 
											DATE_FORMAT(P.Fecha, '%d/%m/%Y') as 'Fecha',
											C.Nombre as 'C.Nombre',
											P.Total as 'P.Total'
									FROM pedidos P
									JOIN campanias C ON C.IdCampania = P.IdCampania
									JOIN usuarios U ON U.IdUsuario = P.IdUsuario
									JOIN datosusuariospersonales D ON U.IdUsuario = D.IdUsuario
									WHERE U.Padrino = '".mysql_real_escape_string($_REQUEST['padrino'])."'
									AND P.IdEstadoPedidos IN (2,3,6)
									ORDER BY P.Fecha DESC");
									
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            
            if(isset($_GET['csv'])) {
            
            	header('Pragma: public'); 	// required
				header('Expires: 0');		// no cache
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($file_name)).' GMT');
				header('Cache-Control: private',false);
				header('Content-Type: '.$mime);
				header('Content-Disposition: attachment; filename="usuarios.csv"');
				header('Content-Transfer-Encoding: binary');
				header('Connection: close');
	            
	            echo "IdPedido,NombreUsuario,Nombre,Apellido,Fecha,Campa�a,Total\n";
	            foreach($Tabla as $datos)
	            	echo $datos['P.IdPedido'].','.$datos['U.NombreUsuario'].','.$datos['D.Nombre'].','.$datos['D.Apellido'].','.$datos['Fecha'].','.$datos['C.Nombre'].','.$datos['P.Total']."\n";
	            	
	            die;
            }
	    	
    	}
		
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Pedidos</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
    
		<? Includes::Scripts(); ?>
		<script src="<?=UrlResolver::getJsBaseUrl("bo/pedidos/js.js");?>" type="text/javascript"></script>

	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Pedidos</h1>	

			<form action="pagos.php" method="post" id="mainStats">
				<table style="margin-bottom:10px;">
					<tr class="alt">
						<td style="text-align:left;font-weight:bold;">
							Padrino: <input type="text" name="padrino" value="<?=$_POST['padrino']?>" />							
						</td>
					</tr>
				</table>
			</form>		
			
			<?php if(isset($Tabla)): ?>
			<a href="pagos.php?padrino=<?=$_REQUEST['padrino']?>&csv">Descargar CSV</a><br /><br />
			<table>
				<tr>
					<th>Pedido</th>
					<th>E-Mail</th>
					<th>Apellido</th>
					<th>Nombre</th>
					<th>Fecha</th>
					<th>Campa�a</th>
					<th>Monto</th>
				</tr>
				<?php foreach($Tabla as $i => $data): ?>
				<tr <?=($i%2==1 ? 'class="alt"' : '')?>>
					<td><?=$data['P.IdPedido']?></td>
					<td><?=$data['U.NombreUsuario']?></td>
					<td><?=$data['D.Apellido']?></td>
					<td><?=$data['D.Nombre']?></td>
					<td><?=$data['Fecha']?></td>
					<td><?=$data['C.Nombre']?></td>
					<td><?=$data['P.Total']?></td>
				</tr>
				<?php endforeach; ?>
			</table>
			<?php endif; ?>
			
		</div>
		
	</body>
</html>
