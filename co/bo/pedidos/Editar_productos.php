<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/mailer/clsMailer.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/clases/clsusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/pedidos/clsinf.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmcupones.php");
    
    try
    {
        ValidarUsuarioLogueadoBo(12);
    }
    catch(ACCESOException $e)
    {
    ?>
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
    
    $objPedido = dmPedidos::getPedidoById($_GET['IdPedido']);
    
    if(isset($_POST['addProduct'], $_POST['cbxProducto']) && $_POST['cbxProducto'] > 0) {
    	dmPedidos::addProduct($objPedido->getIdPedido(), $_POST['cbxProducto'], $_POST['q']);
    	header("Location: ".$_SERVER['REQUEST_URI']);
    	die;
    }
    
    if(isset($_POST['updateQ'], $_POST['q'])) {
	    dmPedidos::updateQuantities($objPedido->getIdPedido(), $_POST['qs']);
    	header("Location: ".$_SERVER['REQUEST_URI']);
    	die;
    }

    if(isset($_POST['borrar'])) {
	    dmPedidos::deleteProduct($objPedido->getIdPedido(), $_POST['borrar']);
    	header("Location: ".$_SERVER['REQUEST_URI']);
    	die;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es-ar" dir="ltr">
	<head>
	    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Pedidos</title>
	    <?
	        Includes::Scripts();
	    ?>
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/pedidos/js.js");?>" type="text/javascript"></script>
	    <link rel="stylesheet" type="text/css" href="<?=UrlResolver::getCssBaseUrl("bo/pedidos/AMB_pedidos.css")?>" media="screen" />
	</head>

	<body>

  	<form action="<?=$_SERVER['REQUEST_URI']?>" method="post">
           <h3>C&oacute;digo de Compra: <?=str_pad($objPedido->getIdPedido(),Aplicacion::getParametros("pedidos", "cantidad"),0,0)?></h3>

            <div id="contPedido">
				<h4>Contenido</h4>
				<table border="1" cellpadding="3" cellspacing="0">
				<tr>
					<td width="399" valign="top">Producto</td>
					<td width="64" valign="top">Cantidad</td>
					<td width="84" valign="top">Importe</td>
					<td width="84" valign="top">Borrar</td>
				</tr>
            <?
	           
            $Bonos = dmMisCreditos::VIEWgetByIdPedido($objPedido->getIdPedido());
			$SubTotal = 0;
			$bought = array();
			foreach($objPedido->getProductos() as $Articulo)
						{
						$objPP = dmProductos::getByIdCodigoProdInterno($Articulo->getIdCodigoProdInterno());
						$objProducto = dmProductos::getById($objPP->getIdProducto());
						$Atributos = dmProductos::getAtributosByIdCodigoProdInterno($Articulo->getIdCodigoProdInterno());
						$SubTotal = $SubTotal + ($Articulo->getPrecio() * $Articulo->getCantidad());
						$bought[] = $Articulo->getIdCodigoProdInterno();
						
			echo '<tr>
						<td valign="top">'.$objProducto->getNombre()." (ref: ".$objProducto->getReferencia().") ";

			foreach($Atributos as $atributo){
					echo $atributo["Nombre"]." = ".$atributo["Valor"] . " | ";
			};

			echo '</td>
						<td valign="top"><input type="text" style="text-align:center;width:50px" name="qs['.$Articulo->getIdCodigoProdInterno().']" value="'.$Articulo->getCantidad().'" /></p></td>
						<td valign="top"><p>'.Moneda($Articulo->getPrecio() * $Articulo->getCantidad()).'</p></td>
						<td valign="top"><button type="submit" name="borrar" value="'.$Articulo->getIdCodigoProdInterno().'">Borrar</button></td>
					</tr>';
						};
						
						
			 /* DESCUENTOS */
			$Descuento = $objPedido->getDescuento();
			if($Descuento > 0) {
	   		    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/clases/clsdescuentos.php");
        		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/datamappers/dmdescuentos.php");
        		
				$objDescuento = dmDescuentos::getById($objPedido->getIdDescuento());

				if(is_object($objDescuento)) {
					if($objDescuento->getPorcentaje())
						$detalle = $objDescuento->getDescuento().'%';
					else
						$detalle = Moneda($objDescuento->getDescuento());
					$descripcion = $objDescuento->getDescripcion();
				} else {
					$detalle = 'N/A';
					$descripcion = 'Descuento';
				}
					
				$SubTotal -= $Descuento;
				
				echo  '<tr>
						<td valign="top">'.$descripcion.' ('.$detalle.' de descuento) </td>
						<td valign="top"><p>1</p></td>
						<td valign="top"><p>-'.Moneda($Descuento).'</p></td>
					</tr>';
			}

			echo '<tr>
						<td valign="top">Sub-Total de la Compra </td>
						<td valign="top">&nbsp;</td>
						<td valign="top">'.Moneda($SubTotal).'</td>
					</tr>
					<tr>
						<td valign="top">Costo de Env&iacute;o</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">'.Moneda($objPedido->getGastosEnvio()).'</td>
					</tr>
					<tr>
						<td valign="top">Cr&eacute;ditos utilizados</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">'.Moneda($Bonos[0]["Bono"]).'</td>
					</tr>
					<tr>
						<td valign="top">Total de la Compra </td>
						<td valign="top">&nbsp;</td>
						<td valign="top">'.Moneda($SubTotal - $Bonos[0]["Bono"] + $objPedido->getGastosEnvio()).'</td>
					</tr>
				</table>
				<br />
				<input type="submit" name="updateQ" value="Actualizar cantidades" />
			</div>';
			
 ?>
 	<h3>Agregar producto</h3>
 	<table>
	 	<tr>
	        <td>
	        Producto &nbsp;
	            <select id="cbxProducto" name="cbxProducto">
	            <option value=0>...Seleccionar...</option>
	            <?
	              $collProductos = dmProductos::getProductosFilradosCamp($objPedido->getIdCampania());
	              foreach ($collProductos as $Producto)
	              {
	              	if(in_array($Producto["IdCodigoProdInterno"], $bought))
	              		continue;
	              	
					$Atributos = dmProductos::getAtributosByIdCodigoProdInterno($Producto["IdCodigoProdInterno"]);
					$contenido = '';
					foreach($Atributos as $atributo)
						$contenido .= $atributo["Nombre"]." = ".$atributo["Valor"] . " | ";
					if($contenido != '')
						$contenido = ' | '.$contenido;
	              			
	                ?><option value="<?=$Producto["IdCodigoProdInterno"]?>"><?=$Producto["Producto"].$contenido?></option><?
	              }
	            ?>
	        </select>
	        </td>
	        <td> 
	        Cantidad:
	        &nbsp;
	        <input type="text" name="q" value="1" />
	        </td>
	        <td> 
	        &nbsp;
	        <input id="btnAgregar" name="addProduct" value="Agregar" type="submit" />
	        </td>
	    </tr>
	</table>
 	</form>
</body>
</html>