<? 
		header("Content-Description: File Transfer");
		header("Content-Type: application/force-download");
		header("Content-Disposition: attachment; filename=".$datos["codigo_compra"].".csv");
		
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
   Aplicacion::CargarIncludes(Aplicacion::getIncludes("pedidos"));
   Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
    try
    {
        ValidarUsuarioLogueadoBo(12);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }    
   $postURL = Aplicacion::getIncludes("post", "pedidos");
	
    $objPedido = dmPedidos::getPedidoById((isset($_GET["IdPedido"])) ? $_GET["IdPedido"]:0);
	$Bonos = dmMisCreditos::VIEWgetByIdPedido((isset($_GET["IdPedido"])) ? $_GET["IdPedido"]:0);
	$objUsuario = dmMicuenta::GetUsuario($objPedido->getIdUsuario());
	$objCampania = dmCampania::getByIdCampania($objPedido->getIdCampania());
	$FechaFinCampania = $objCampania->getFechaFin();
	$FechaFinCampania = explode(" ", $FechaFinCampania);
	$FechaFinCampania = explode("/", $FechaFinCampania[0]);
	$InfoPedido = dmPedidos::getInfoPedido((isset($_GET["IdPedido"])) ? $_GET["IdPedido"]:0);

	$datos = array();
	$datos["codigo_compra"]=str_pad($objPedido->getIdPedido(),Aplicacion::getParametros("pedidos", "cantidad"),0,0);
	$datos["total"] = Moneda($objPedido->getTotal());      
	$datos["nombre"] = ucwords($objUsuario->getDatos()->getNombre()." ".$objUsuario->getDatos()->getApellido());
		
	$i=0;
	foreach($objPedido->getProductos() as $Articulo)
	{
		$objPP = dmProductos::getByIdCodigoProdInterno($Articulo->getIdCodigoProdInterno());
		$objProducto = dmProductos::getById($objPP->getIdProducto());
		$Atributos = dmProductos::getAtributosByIdCodigoProdInterno($Articulo->getIdCodigoProdInterno());
		$datos["productos"][$i]["nombre"] = $objProducto->getNombre();
		
		$datos["productos"][$i]["descripcion"] = "(";
		
		$j = 0;
		foreach($Atributos as $atributo){
			  $datos["productos"][$i]["descripcion"] .=($j!=0)?", ":"";
			  $datos["productos"][$i]["descripcion"] .= $atributo["Nombre"].": ".$atributo["Valor"];
		$j++;
		}
		
		$datos["productos"][$i]["descripcion"] .= ")";
		$datos["productos"][$i]["cantidad"] = $Articulo->getCantidad();
		$datos["productos"][$i]["precio"] = Moneda($Articulo->getPrecio() * $Articulo->getCantidad(),2);
		
		$i++;
		}
		
		$datos["productos"][$i]["nombre"] = "Gastos de Envio";
		$datos["productos"][$i]["descripcion"] = "";
		$datos["productos"][$i]["cantidad"] = "";
		$datos["productos"][$i]["precio"] = Moneda($objPedido->getGastosEnvio());
		$i++;
		
		$datos["productos"][$i]["nombre"] = "Descuento";
		$datos["productos"][$i]["descripcion"] = "";
		$datos["productos"][$i]["cantidad"] = "";
		$datos["productos"][$i]["precio"] = "-".Moneda($Bonos[0]["Bono"]);		
		
					
					
					
		$salida = "";
		$salida .= $datos["nombre"].";\n"; //Nombre
		$salida .= "'-----------------------------;\n"; //IVA
		$salida .= "'----------------------------------;\n"; //Condiciones de venta
		$salida .= "'--------------;\n"; //Fecha de vencimiento
		$salida .= $datos["total"].";\n"; //Total
		//Comienza listado de productos, descuentos y envio.
		foreach($datos["productos"] as $producto){
			$salida .= $producto["cantidad"]." ".$producto["nombre"]." ".$producto["descripcion"].";";
			$salida .= $producto["precio"]."\n";
		}
		
		echo $salida;
					
?>
	