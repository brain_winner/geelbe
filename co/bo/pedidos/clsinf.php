<?
	//INFORMACION DE CONTENIDO DEL PEDIDO
	class htmlInfContPedido{

		protected $objPedido;

		public function __construct($objPedido,$bonoUsado){
			$this->objPedido = $objPedido;
			$this->bonoUsado = $bonoUsado;
		}

		public function imprimir(){
			$contenido = '<div id="contPedido">
					<h4>Contenido</h4>
					<table border="1" cellpadding="3" cellspacing="0">
					<tr>
						<td width="399" valign="top">Producto</td>
						<td width="64" valign="top">Cantidad</td>
						<td width="84" valign="top">Importe</td>
					</tr>';
			$SubTotal = 0;
			foreach($this->objPedido->getProductos() as $Articulo)
						{
						$objPP = dmProductos::getByIdCodigoProdInterno($Articulo->getIdCodigoProdInterno());
						$objProducto = dmProductos::getById($objPP->getIdProducto());
						$Atributos = dmProductos::getAtributosByIdCodigoProdInterno($Articulo->getIdCodigoProdInterno());
						$SubTotal = $SubTotal + ($Articulo->getPrecio() * $Articulo->getCantidad());
						
			$contenido .='<tr>
						<td valign="top">'.$objProducto->getNombre()." (ref: ".$objProducto->getReferencia().") ";

			foreach($Atributos as $atributo){
					$contenido .= $atributo["Nombre"]." = ".$atributo["Valor"] . " | ";
			};

			$contenido .= '</td>
						<td valign="top"><p>'.$Articulo->getCantidad().'</p></td>
						<td valign="top"><p>'.Moneda($Articulo->getPrecio() * $Articulo->getCantidad()).'</p></td>
					</tr>';
						};
						
						
			 /* DESCUENTOS */
			$Descuento = $this->objPedido->getDescuento();
			if($Descuento > 0) {
				global $confRoot;
	   		    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/clases/clsdescuentos.php");
        		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/datamappers/dmdescuentos.php");
        		
				$objDescuento = dmDescuentos::getById($this->objPedido->getIdDescuento());

				if(is_object($objDescuento)) {
					if($objDescuento->getPorcentaje())
						$detalle = $objDescuento->getDescuento().'%';
					else
						$detalle = Moneda($objDescuento->getDescuento());
					$descripcion = $objDescuento->getDescripcion();
				} else {
					$detalle = 'N/A';
					$descripcion = 'Descuento';
				}
					
				$SubTotal -= $Descuento;
				
				$contenido .='<tr>
						<td valign="top">'.$descripcion.' ('.$detalle.' de descuento) </td>
						<td valign="top"><p>1</p></td>
						<td valign="top"><p>-'.Moneda($Descuento).'</p></td>
					</tr>';
			}

			$contenido .='<tr>
						<td valign="top">Sub-Total de la Compra </td>
						<td valign="top">&nbsp;</td>
						<td valign="top">'.Moneda($SubTotal).'</td>
					</tr>
					<tr>
						<td valign="top">Costo de Env&iacute;o</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">'.Moneda($this->objPedido->getGastosEnvio()).'</td>
					</tr>
					<tr>
						<td valign="top">Cr&eacute;ditos utilizados</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">'.Moneda($this->bonoUsado).'</td>
					</tr>
					<tr>
						<td valign="top">Total de la Compra </td>
						<td valign="top">&nbsp;</td>
						<td valign="top">'.Moneda($SubTotal - $this->bonoUsado + $this->objPedido->getGastosEnvio()).'</td>
					</tr>
				</table>
			</div>';
			return $contenido;
		}

	}




	//INFORMACION DE PAGO
	class htmlInfPago{

		protected $formaPago;

		public function __construct($formaPago){
			$this->formaPago = $formaPago;
		}

		public function imprimir(){
			return '<div id="infPago">
			<h4>Informaci&oacute;n de Pago </h4>
	        <p>Forma de Pago: <strong>'.$this->formaPago.'</strong></p>
			</div>';
		}
	}



	//INFORMACION DE ENVIO
	class htmlInfEstPed{

	protected $descripcionDeEstado;
	protected $fechaEnvio;
	protected $codigodebarra;
	protected $idPedido;
	protected $trackingDHL;
	protected $motivo;

	public function __construct($descripcionDeEstado, $fechaEnvio, $codigodebarra, $idPedido, $trackingDHL, $motivo){
		$this->descripcionDeEstado = $descripcionDeEstado;
		$this->fechaEnvio = $fechaEnvio;
		$this->codigodebarra = $codigodebarra;
		$this->idPedido = $idPedido;
		$this->trackingDHL = $trackingDHL;
		$this->motivo = $motivo;
	}

	public function imprimir(){
		return '<div id="infEstPed">
			<h4>Estado del Pedido</h4>
	            <p>Estado: <strong>'.$this->descripcionDeEstado.'</strong>'.
	           ($this->fechaEnvio != "" ?" el ".$this->fechaEnvio : $fecha) .	           
  	            ($this->motivo != '' ? '<br>Motivo: '.$this->motivo : '').
	            ($this->trackingDHL != '' ? "<br />Tracking N&uacute;mero: <a href='http://www.saferbo.com/verguiasn.php' class='external'>" .$this->trackingDHL."</a></strong></p>" : '').
	           (empty($this->codigodebarra)?"":"<p>Cupon asignado Nro: <a href='".Aplicacion::getRootUrl()."cupon.php?i=".$this->idPedido."&h=".generar_hash_md5($this->idPedido)."' target='_blank'>".$this->codigodebarra."</a></p>").'
	       '.$this->historial().' </div>';
	}
		
	public function historial() {
		
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
        $oConexion->setQuery("SELECT e.*, ee.Descripcion FROM pedidos_x_envios e JOIN estadospedidos ee ON ee.IdEstadoPedido = e.IdEstadoPedidos WHERE e.IdPedido = ".$this->idPedido.' ORDER BY FechaEnvio DESC');
        $Tabla = $oConexion->DevolverQuery();
        $oConexion->Cerrar_Trans();
		
		$html = '';
		if(count($Tabla)) {
			
			$html .= '<br /><h4>Historial de env�os</h4>';
			foreach($Tabla as $i)
				$html .= '<p>'.date("d/m/Y H:i", strtotime($i['FechaEnvio'])).' - <strong>'.$i['Descripcion'].'</strong>: <a href="http://www.saferbo.com/verguiasn.php" class="external">'.$i['NroEnvio'].'</a></p>';
			
		}
		
		return $html;
		
	}
		
		
}
//	<p>Correo: <strong>Envio</strong></p>


class htmlCambiarEstPed{

	protected $idEstado = "";
	protected $fechaEnvio = "";
	protected $trackig;
	protected $formCambioEstado;
	protected $fechaFinCampania;
	protected $idPedido;

	//$estado,$FechaFinCampania,$fechaEnvio="",$numeroEnvio="")


	public function __construct($idPedido, $idEstado, $fechaFinCampania=false, $fechaEnvio=false, $tracking=false){
		$this->fechaEnvio = $fechaEnvio;
		$this->idEstado = $idEstado;
		$this->fechaFinCampania = $fechaFinCampania;
		$this->trackig = $tracking;
		$this->idPedido = $idPedido;
	}


	public function setIdEstado($id){
		$this->idEstado = $id;
	}

	public function setFechaFinCampania($fecha){
		$this->fechaFinCampania = $fecha;
	}

	public function setFechaEnvio($fecha){
		$this->fechaEnvio = $fecha;
	}

	public function setTracking($tracking){
		$this->tracking = $tracking;
	}




	public function imprimir(){

/*
"IdEstadoPedido","Descripcion"
0,"Esperando Sistema de pago"
1,"Sistema de pago procesando"
2,"Pago aceptado"
3,"Envio programado"
4,"Sistema de pago rechazado"
5,"Anulado"
6,"Entregado"
7,"Pago Offline"
3: Programar envio
5: Anular Envio
6: Entregar Envio
7: Pago Offline
8: Esperando Cupon de pago
9: Pedido Interno
10: Stock insuficiente
11: Reversado
12: en garant�a
13: devoluci�n de dinero
14: devoluci�n gcash

*/
		//$estados[$actual][$opcion][$visible]
		$estados = array(
						     //Opciones visibles
			//        0  1  2  3  4  5  6  7  8  9	10	11	12	13	14
			0=> array(0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1),
			1=> array(0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1),
			2=> array(0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1),
			3=> array(0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1),
			4=> array(0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1),
			5=> array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1),
			6=> array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			7=> array(0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1),
			8=> array(0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1),
			9=> array(0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1),
			10=> array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1),
			11=> array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			12=> array(0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1),
			13=> array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
			14=> array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0),
		);

		$this->formCambioEstado = $this->getHeader();
		foreach($estados[$this->idEstado] as $opcion => $mostrar){
			if($mostrar){
				$this->formCambioEstado .= $this->getOpcion($opcion);
			}
		}

		$this->formCambioEstado .= $this->getFooter();

		return $this->formCambioEstado;
	}



	protected function getHeader(){
		$header = '<div id="cambiarEstPed">
				<h5>Cambiar estado del pedido</h5>
				<form id="frmABMPedidos" name="frmABMPedidos" method="POST" onSubmit="ValidarForm(this);">
				<input type="hidden" id="txtIdPedido" name="txtIdPedido" value="'.Aplicacion::Encrypter($this->idPedido).'" />';
		return $header;
	}


	protected function getOpcion($estado){
			switch($estado){
				case 0:
				$opcion = '<p><input type="radio" id="cbxEstado" name="cbxEstado" value="0" />Esperar Sistema de Pago</p>';
				break;

				case 2:
				$opcion = '<p><input type="radio" id="cbxEstado" name="cbxEstado" value="2" />Pago Aceptado</p>';
				break;

				case 3:
				$opcion ='<p><input type="radio" id="cbxEstado" name="cbxEstado" value="3" />
				Envio - &nbsp;Fecha de envio
	                	<select name="FechaEnvio[cbxDia]" id="FechaEnvio[cbxDia]">
	                    	<option value="0" >D&iacute;a</option>';

	  for ($i=1; $i<=31; $i++){
	  		   $opcion .= '<option value="'.str_pad($i,2,0,0).'">'.str_pad($i,2,0,0).'</option>';
	  }

	            $opcion .='</select>
	                    &nbsp;/&nbsp;
	                    <select name="FechaEnvio[cbxMes]" id="FechaEnvio[cbxMes]">
	                        <option value="00" >Mes</option>
	                        <option value="01" >enero</option>
	                        <option value="02" >febrero</option>
	                        <option value="03" >marzo</option>
	                        <option value="04" >abril</option>
	                        <option value="05" >mayo</option>
	                        <option value="06" >junio</option>
	                        <option value="07" >julio</option>
	                        <option value="08" >agosto</option>
	                        <option value="09" >septiembre</option>
	                        <option value="10" >octubre</option>
	                        <option value="11" >noviembre</option>
	                        <option value="12" >diciembre</option>
	                    </select>
	                    &nbsp;/&nbsp;
	                    <select name="FechaEnvio[cbxAnio]" id="FechaEnvio[cbxAnio]">
	                        <option value="0" >A&ntilde;o</option>';
	            for ($i=(date("Y")-99); $i<=date("Y"); $i++){
	            		$opcion .=	'<option value="'.$i.'">'.$i.'</option>';
	            };
	         $opcion .= '</select>';

	         if($fechaEnvio!=""){
	         		$Fecha = explode("/", $this->fechaEnvio);
	         }else{
	         	$Fecha = $this->fechaFinCampania;
	         };
	         $opcion .= '<script>
							document.getElementById("FechaEnvio[cbxDia]").value ="'.str_pad($Fecha[0],2,0,0).'";
							document.getElementById("FechaEnvio[cbxMes]").value ="'.str_pad($Fecha[1],2,0,0).'";
							document.getElementById("FechaEnvio[cbxAnio]").value ="'.$Fecha[2].'";
						 </script>
						(Fecha de envio sugerida 72hs despues de finalizar la vitrina.)
				</p>
				<p>Nro de Tracking&nbsp;<input id="txtNroEnvio" name="txtNroEnvio" maxlength="80" value="'.$this->trackig.'" />(Max 80 caracteres)</p>';
				break;

				case 5:
				$opcion = '<p><input type="radio" id="cbxEstado" name="cbxEstado" value="5" />Anular pedido - Motivo <input type="text" maxlength="80" size=40 id="txtMotivoAnulado" name="txtMotivoAnulado" value="" /></p>';
				break;

				case 6:
				$opcion = '<p><input type="radio" id="cbxEstado" name="cbxEstado" value="6" />Entregar pedido</p>';
				break;

				case 7:
				$opcion ='<p><input type="radio" id="cbxEstado" name="cbxEstado" value="7" />Pago Offline</p>';
				break;

				case 8:
				$opcion ='<p><input type="radio" id="cbxEstado" name="cbxEstado" value="8" /> Enviar Cupon de Pago</p>
						  <p><em>El cupon de pago debe ser enviado hasta 48hs antes que finalice la campa�a</em></p>';

				/*
						<p>Mensaje <input type="textarea" id="mensaje" name="mensaje" /></p>
						<p>Visible si <input type="radio" name="mensajeSiNo" value="1" checked="checked" /> / no <input type="radio" name="mensajeSiNo" value="0" /></p>
						<p>Observacion <input type="text" id="observacion" name="observacion" /></p>';*/
				break;

				case 9:
				$opcion ='<p><input type="radio" id="cbxEstado" name="cbxEstado" value="9" />Pedido Interno</p>';
				break;
				
				case 11:
				$opcion = '<p><input type="radio" id="cbxEstado" name="cbxEstado" value="11" />Reversar pedido - Motivo <input type="text" maxlength="80" size=40 id="txtMotivoReversado" name="txtMotivoReversado" value="" /></p>';
				break;

				case 12:
				$opcion = '<p><input type="radio" id="cbxEstado" name="cbxEstado" value="12" />En garant�a - Motivo <input type="text" maxlength="80" size=40 id="txtMotivoGarantia" name="txtMotivoGarantia" value="" /></p>';
				break;

				case 13:
				$opcion = '<p><input type="radio" id="cbxEstado" name="cbxEstado" value="13" />Devoluci�n de dinero - Motivo <input type="text" maxlength="80" size=40 id="txtMotivoDevolucion" name="txtMotivoDevolucion" value="" /></p>';
				break;

				case 14:
				$opcion = '<p><input type="radio" id="cbxEstado" name="cbxEstado" value="14" />Devoluci�n GCash - Motivo <input type="text" maxlength="80" size=40 id="txtMotivoGCash" name="txtMotivoGCash" value="" /></p>';
				break;

			}
			return $opcion;
		}

		protected function getFooter(){
			$footer = '<div id="botDec">
            <p><a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage(\'btnAceptar\',\'\',"'.UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif").'",1)"><img src="'.UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif").'" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a></p>
            <p><a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage(\'btnCancelar\',\'\',"'.UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif").'",1)"><img src="'.UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif").'" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a></p>
		</div>
		</form></div>';
			return $footer;
		}
}



	//INFORMACION DATOS DE COMPRA
		class htmlInfDatComp{

		protected $objDatos;
		protected $arrayPedidosDirecciones;

		public function __construct($objDatos, array $arrayPedidosDirecciones){
		$this->objDatos = $objDatos;
		$this->arrayPedidosDirecciones = $arrayPedidosDirecciones;
		}

		public function imprimir(){
				return '<div id="datComp">
					<h4>Datos del Comprador</h4>
					<p>Nombre: <strong>'.$this->arrayPedidosDirecciones["Nombre"].'</strong></p>
					<p>Apellido: <strong>'.$this->arrayPedidosDirecciones["Apellido"].'</strong></p>
		            <p>DNI: <strong>'.$this->objDatos->getNroDoc().'</strong></p>
					<p>Telefono: <strong>'.$this->objDatos->getCodTelefono()." - ".$this->objDatos->getTelefono().'</strong></p>
					<p>Telefono M&oacute;vil: <strong>'.$this->objDatos->getcelucodigo()." - ".$this->objDatos->getcelunumero().'</strong></p>

				</div>';
			}
		}

	//INFORMACION DIRECCION DE ENVIO
		class htmlInfDirEnv{

		protected $objDireccion;

		public function __construct($objDireccion){
		$this->objDireccion = $objDireccion;
		}

		public function imprimir(){
		
				$envio = '<div id="dirEnv" style="float:left">
				<h4>Direcci&oacute;n de Env&iacute;o</h4>
				<p>Calle:  <strong>'.$this->objDireccion->getDomicilio().'</strong></p>
				<p>N&uacute;mero: <strong>'.$this->objDireccion->getNumero().'</strong></p>
				<p>Piso: <strong>'.$this->objDireccion->getPiso().'</strong></p>
				<p>Depto:  <strong>'.$this->objDireccion->getPuerta().'</strong></p>
	            <p>Ciudad:  <strong>';
	            
   	            if($this->objDireccion->getIdCiudad() != ""){
   	            	$dt = dmMicuenta::GetCiudadById($this->objDireccion->getIdCiudad());
					$envio .= $dt[0]["Descripcion"];
	            };
	            
	            $envio .= '</strong></p>
	            <p>Provincia: <strong>';
	            if($this->objDireccion->getIdProvincia() != ""){
					$Prov = dmMicuenta::GetProvinciasById($this->objDireccion->getIdProvincia());
					$envio .= $Prov[0]["Nombre"];
	            };
	            $envio .='</strong></p>
				<p>C&oacute;digo Postal: <strong>'.$this->objDireccion->getCP().'</strong><br /></p>
				<p>Pa&iacute;s: <strong>';
				if($this->objDireccion->getIdPais() != ""){
					$Pais = dmMicuenta::GetPaisesById($this->objDireccion->getIdPais());
					$envio .= $Pais[0]["Nombre"];
				};
	            $envio .= '</strong></p>
	            </div>
	            <div style="clear:both"></div>';
	            return $envio;
			}
		}
?>