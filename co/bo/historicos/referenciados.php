<?php
	try {
	    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("historico"));
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(18);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
    try {
		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array('R.NombreUsuario', 'R.Apellido', 'R.Nombre', 'R.MailAhijado'));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("R.Fecha", "DESC");
	
	    $count = dmReferenciadoH::ObtenerReferenciadosCount($paramArray);
		$listPager = new ListadoPaginator($count);
		$rows = dmReferenciadoH::ObtenerReferenciadosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage()); 
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Histórico Referenciados</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Histórico Referenciados</h1>
			
			<?php
				$listadoPrinter = new ListadoPrinter($rows, 'referenciados.php', $listPager);
				$listadoPrinter->showColumn("R.NombreUsuario", "Nombre de usuario");
				$listadoPrinter->showColumn("R.Apellido", "Apellido");
				$listadoPrinter->showColumn("R.Nombre", "Nombre");
				$listadoPrinter->showColumn("R.MailAhijado", "Mail del ahijado");
				$listadoPrinter->showColumn("R.Fecha", "Fecha");
				
				$listadoPrinter->setShowFilter("R.Fecha", false);
				
				$listadoPrinter->printListado();
			?>		
		</div>	
	</body>
</html>
