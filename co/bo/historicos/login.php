<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("historico"));
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(17);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
    try {
		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array('L.NombreUsuario', 'L.Apellido', 'L.Nombre'));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("L.Fecha", "DESC");

		$count = dmLoginH::getLoginHistoricoCount($paramArray);
		$listPager = new ListadoPaginator($count);
		$rows = dmLoginH::getLoginHistoricoPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Histórico Login</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Histórico Login</h1>	
			
			<?php
				$listadoPrinter = new ListadoPrinter($rows, "login.php", $listPager);
				$listadoPrinter->showColumn("L.NombreUsuario", "E-Mail");
				$listadoPrinter->showColumn("L.Apellido", "Apellido");
				$listadoPrinter->showColumn("L.Nombre", "Nombre");				
				$listadoPrinter->showColumn("L.Fecha", "Fecha");
				
				$listadoPrinter->setShowFilter("L.Fecha", false);
				
				$listadoPrinter->printListado();
			?>
		</div>
		
	</body>
</html>