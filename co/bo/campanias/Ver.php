<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    try
    {
        ValidarUsuarioLogueadoBo(9);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }    
    $objCampania = dmCampania::getByIdCampania((isset($_GET["IdCampania"])) ? $_GET["IdCampania"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM campanias</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/campanias/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMCampanias" name="frmABMCampanias" target="iCampanias" action="../../../<?=Aplicacion::getDirLocal().$postURL["campanias"]?>" method="POST" onSubmit="ValidarForm(this);">
        <input type="hidden" id="txtIdCampania" name="txtIdCampania" value="<?=$objCampania->getIdCampania()?>">
        <table>
            <tr>
                <td>Nombre</td>
                <td><b><?=$objCampania->getNombre()?></b></td>
            </tr>
            <tr>
                <td>Descripcion</td>
                <td><b><?=$objCampania->getDescripcion()?></b></td>
            </tr>
            <tr>
                <td>Fecha de inicio</td>
                <td><b><?=$objCampania->getFechaInicio()?></b></td>
            </tr>
            <tr>
                <td>Fecha de fin</td>
                <td><b><?=$objCampania->getFechaFin()?></b></td>
            </tr>
            <tr>
                <td>Visible desde</td>
                <td><b><?=$objCampania->getVisibleDesde()?></b></td>
            </tr>
            <tr>
                <td>Maximos productos a vender</td>
                <td><b><?=$objCampania->getMaxProd()?></b></td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnVolver','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver.gif");?>" alt="Volver" name="btnVolver" id="btnVolver" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>