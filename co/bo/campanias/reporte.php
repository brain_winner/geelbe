<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(12);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
	try {
	    if(isset($_POST['descargar'])) {
	    	dmCampania::reporte($_POST['desde'], $_POST['hasta']);
	    	die;
	    }
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		echo $e;
		exit;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Importar archivo Envio</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
    
		<? Includes::Scripts(); ?>
		<script src="<?=UrlResolver::getJsBaseUrl("bo/pedidos/js.js");?>" type="text/javascript"></script>
		<script src="//code.jquery.com/jquery.js"></script>
		<script src="//code.jquery.com/ui/jquery-ui-git.js"></script>
		<link href="//code.jquery.com/ui/jquery-ui-git.css" type="text/css" rel="stylesheet" />

	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Reporte de campa&ntilde;as</h1>	

		<form action="reporte.php" method="post" enctype="multipart/form-data" style="margin: 20px">
			
			<label for="desde">Desde:</label>
			<input type="text" name="desde" id="desde" />
			
			<label for="hasta">Hasta:</label>
			<input type="text" name="hasta" id="hasta" />
			
			<input type="submit" name="descargar" value="Descargar" />
			
		</form>
		<script>
			$('input[type=text]').datepicker({ dateFormat: 'dd/mm/yy' })
		</script>
		
	</body>
</html>
