<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
		$POST = Aplicacion::getIncludes("post", "campanias");
	
		if(isset($_REQUEST["filter"])) {
			foreach ($_REQUEST["filter"] as $key => $value) {
				$_REQUEST["filter"][stripslashes($key)] = $value;
			}
		}
				
		if(isset($_REQUEST["sortBy"])) {
			$_REQUEST["sortBy"] = stripslashes($_REQUEST["sortBy"]);
		}
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(9);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
	
	try {		
		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array(
				"C.Nombre", 
				"DATE_FORMAT(FechaInicio,'%d/%m/%Y %H:%i')", 
				"DATE_FORMAT(FechaFin,'%d/%m/%Y %H:%i')", 
				"DATE_FORMAT(VisibleDesde,'%d/%m/%Y %H:%i')", 
				"IF(UNIX_TIMESTAMP(C.VisibleDesde)<=UNIX_TIMESTAMP(NOW()),'Si','No')", 
				"IF(UNIX_TIMESTAMP(C.FechaFin)<=UNIX_TIMESTAMP(NOW()),'Cerrada',IF(UNIX_TIMESTAMP(C.FechaInicio)<=UNIX_TIMESTAMP(NOW()),'Abierta','Proxima'))"
			));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("date(FechaFin)", "DESC");
		
		$count = dmCampania::getCampaniasCount($paramArray);
		$listPager = new ListadoPaginator($count);
		$rows = dmCampania::getCampaniasPaginadas($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage()); 
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Pedidos</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
    
		<? Includes::Scripts(); ?>
		<script src="<?=UrlResolver::getJsBaseUrl("bo/campanias/js.js");?>" type="text/javascript"></script>

	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Campa&ntilde;a</h1>	

				<div id="mainStats">
					<table style="margin-bottom:10px;">
						<tr class="alt">
							<td style="text-align:left;font-weight:bold;">
								<a href="<?="/".$confRoot[1]."/bo/campanias/ABM.php"?>">Nueva campa&ntilde;a de ventas</a> |
								<a href="<?="/".$confRoot[1]."/bo/importar/reimportar.php"?>">Reimportar productos</a> |
								<a href="<?="/".$confRoot[1]."/bo/campanias/reporte.php"?>">Reporte</a>
							</td>
						</tr>
					</table>
					
				</div>			
			
			<?php
				$listadoPrinter = new ListadoPrinter($rows, "index.php$parametro", $listPager);
				$listadoPrinter->showColumn("C.Nombre", "Nombre");
				$listadoPrinter->showColumn("DATE_FORMAT(FechaInicio,'%d/%m/%Y %H:%i')", "Fecha de inicio");
				$listadoPrinter->showColumn("DATE_FORMAT(FechaFin,'%d/%m/%Y %H:%i')", "Fecha de fin");
				$listadoPrinter->showColumn("DATE_FORMAT(VisibleDesde,'%d/%m/%Y %H:%i')", "Visible desde");				
				$listadoPrinter->showColumn("IF(UNIX_TIMESTAMP(C.VisibleDesde)<=UNIX_TIMESTAMP(NOW()),'Si','No')", "Visible");
				$listadoPrinter->showColumn("IF(UNIX_TIMESTAMP(C.FechaFin)<=UNIX_TIMESTAMP(NOW()),'Cerrada',IF(UNIX_TIMESTAMP(C.FechaInicio)<=UNIX_TIMESTAMP(NOW()),'Abierta','Proxima'))", "Estado");
				$listadoPrinter->showColumn("Acciones");
				
				$listadoPrinter->setSortColumn("Acciones", false);

				$listadoPrinter->setShowFilter("Acciones", false);
				
				$listadoPrinter->setFilterBoxWidth("IF(UNIX_TIMESTAMP(C.VisibleDesde)<=UNIX_TIMESTAMP(NOW()),'Si','No')", "16");
				$listadoPrinter->setFilterBoxWidth("IF(UNIX_TIMESTAMP(C.FechaFin)<=UNIX_TIMESTAMP(NOW()),'Cerrada',IF(UNIX_TIMESTAMP(C.FechaInicio)<=UNIX_TIMESTAMP(NOW()),'Abierta','Proxima'))", "50");
			
				
				$listadoPrinter->addButtonToColumn("Acciones", "<select id=\"C[[C.IdCampania]]\" name=\"menu\" style\"width:40px;\" onchange=\"process_choice(this, [[C.IdCampania]])\">
																	<option value=\"0\" selected>Seleccione:</option>
																	<option value=\"1\">Editar Campa&ntilde;a</option>
																	<option value=\"2\">Editar Categorias</option>
																	<option value=\"3\">Descargar Orden del pedido</option>
																	<option value=\"4\">Descargar Informe del pedido</option>
																	<option value=\"5\">Descargar Factura</option>
																	<option value=\"6\">Descargar CSV de DHL</option>
																	<option value=\"7\">Descargar factura en XLS</option>
																	<option value=\"8\">Descargar productos de la campa�a</option>
																	<option value=\"9\">Importar productos a la camapa�a</option>
																	<option value=\"10\">Exportar SIIGO inventario</option>
																	<option value=\"11\">Exportar SIIGO terceros</option>
																	<option value=\"12\">Exportar SIIGO pedidos</option>
																	<option value=\"13\">Exportar SIIGO devoluci&oacute;n cliente</option>
																	<option value=\"14\">Exportar SIIGO factura de venta</option>
																	<option value=\"15\">Exportar SIIGO orden de compra</option>
																</select>");

				$listadoPrinter->printListado();
			?>
			<script type="text/javascript">

				function process_choice(selection, IdCampania) {
					if (selection.value==1) {
						location.href="/<?=$confRoot[1]?>/bo/campanias/ABM.php?IdCampania="+IdCampania;
						document.getElementById("C"+IdCampania).disabled="disabled";
					}
					else if (selection.value==2) {
						window.location.href="/<?=$confRoot[1]?>/bo/campanias/index_categorias.php?IdCampania="+IdCampania;
						document.getElementById("C"+IdCampania).disabled="disabled";
					}
					else if (selection.value==3) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/campanias/actionform/ordenpedido.php?IdCampania="+IdCampania;
					}
					else if (selection.value==4) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/campanias/actionform/informepedido.php?IdCampania="+IdCampania;
					}
					else if (selection.value==5) {
						window.location.href="/<?=$confRoot[1]?>/bo/campanias/informes/facturas.php?IdCampania="+IdCampania;
					}
					else if (selection.value==6) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/campanias/actionform/csvdhl.php?IdCampania="+IdCampania;
					}
					else if (selection.value==7) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/campanias/actionform/csvdhl_new.php?IdCampania="+IdCampania;
					}
					else if (selection.value==8) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/campanias/actionform/informeprodcampania.php?IdCampania="+IdCampania;
					}	
					else if (selection.value==9) {
						window.location.href="/<?=$confRoot[1]?>/bo/importar/index.php?IdCampania="+IdCampania;
					}	
					else if (selection.value==10) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/campanias/actionform/csvsiigo.php?IdCampania="+IdCampania;
					}
					else if (selection.value==11) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/campanias/actionform/csvsiigo_clientes.php?IdCampania="+IdCampania;
					}
					else if (selection.value==12) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/campanias/actionform/csvsiigo_pedidos.php?IdCampania="+IdCampania;
					}
					else if (selection.value==13) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/campanias/actionform/csvsiigo_devolucion.php?IdCampania="+IdCampania;
					}
					else if (selection.value==14) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/campanias/actionform/csvsiigo_factura.php?IdCampania="+IdCampania;
					}
					else if (selection.value==15) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/campanias/actionform/csvsiigo_orden.php?IdCampania="+IdCampania;
					}
				}
			</script>
			
		</div>
		
	</body>
</html>
