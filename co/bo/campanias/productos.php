<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
		$POST = Aplicacion::getIncludes("post", "productos");
	
		if(!isset($_REQUEST["IdCampania"])) {
			header('HTTP/1.1 500 Internal Server Error');
			include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
			exit;
		}
		else {
			$IdCampania = $_REQUEST["IdCampania"];
		}
		
		if(isset($_REQUEST["filter"])) {
			foreach ($_REQUEST["filter"] as $key => $value) {
				$_REQUEST["filter"][stripslashes($key)] = $value;
			}
		}
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }

    try {
        ValidarUsuarioLogueadoBo(8);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }

	try {		
		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array('P.Nombre', 'CM.Nombre', 'C.Nombre', 'P.Referencia'));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("P.IdProducto", "DESC");
		
		$count = dmproductos::getProductosCampaniaCount($IdCampania, $paramArray);
		$listPager = new ListadoPaginator($count);
		$rows = dmproductos::getProductosCampaniaPaginadas($IdCampania, $paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage()); 
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Pedidos</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />

		<? Includes::Scripts(); ?>
		<script src="<?=UrlResolver::getJsBaseUrl("bo/productos/js.js");?>" type="text/javascript"></script>
		<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.4.min.js")?>"></script>

	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Productos</h1>	

				<div id="mainStats">
					<table style="margin-bottom:10px;">
						<tr class="alt">
							<td style="text-align:left;font-weight:bold;"> <a href="<?="/".$confRoot[1]."/bo/productos/ABM_new.php?IdCampania=".$IdCampania?>">Nuevos productos</a>&nbsp;|&nbsp;
							<a href="<?="/".$confRoot[1]."/bo/productos/prod_importer.php?IdCampania=".$IdCampania?>">Importar Productos</a></td>
						</tr>
					</table>
					
				</div>			
			
			<?php
				$listadoPrinter = new ListadoPrinter($rows, "productos.php?IdCampania=".$IdCampania, $listPager);
				$listadoPrinter->showColumn("P.Nombre", "Producto");
				$listadoPrinter->showColumn("CM.Nombre", "Campa�a");
				$listadoPrinter->showColumn("C.Nombre", "Clase");
				$listadoPrinter->showColumn("P.Referencia", "Referencia");
				$listadoPrinter->showColumn("Stock", "Stock");				
				$listadoPrinter->showColumn("SUM.Regalo", "Regalo");
				$listadoPrinter->showColumn("Oculto");
				$listadoPrinter->showColumn("Acciones");

				$listadoPrinter->setSortColumn("Stock", false);
				$listadoPrinter->setSortColumn("SUM.Regalo", false);
				$listadoPrinter->setSortColumn("Oculto", false);
				$listadoPrinter->setSortColumn("Acciones", false);
				
				$listadoPrinter->setShowFilter("Stock", false);
				$listadoPrinter->setShowFilter("SUM.Regalo", false);
				$listadoPrinter->setShowFilter("Oculto", false);
				$listadoPrinter->setShowFilter("Acciones", false);
				
				$listadoPrinter->addHiddenValue("IdCampania", $IdCampania);

				$listadoPrinter->addButtonToColumn("Oculto", '<input type="button" value="" class="ocultar" data-oculto="[[P.Oculto]]" data-idproducto="[[P.IdProducto]]" />');
				
				$listadoPrinter->addButtonToColumn("Acciones", "<select id=\"P[[P.IdProducto]]PC[[PC.IdCategoria]]CA[[CA.IdCampania]]\" name=\"menu\" style\"width:40px;\" onchange=\"process_choice(this, [[P.IdProducto]], '[[PC.IdCategoria]]', '[[CA.IdCampania]]', '[[P.Habilitado]]');\">
																	<option value=\"0\" selected>Seleccione:</option>
																	<option value=\"1\">Editar Producto</option>
																	<option value=\"2\">Stock</option>
																	<option value=\"3\">Previsualizar Producto</option>
																	<option value=\"5\">Exportar Producto</option>
																	<option id=\"O4P[[P.IdProducto]]\" value=\"4\"></option>
																</select>
																<script type=\"text/javascript\">
																	if ([[P.Habilitado]]==1) {
																		$(\"#O4P[[P.IdProducto]]\").html(\"Deshabilitar Producto\");
																	}
																	else {
													  					$(\"#O4P[[P.IdProducto]]\").html(\"Habilitar Producto\");
																	}
																</script>");

				$listadoPrinter->printListado();
			?>
			
			<script type="text/javascript">
				function process_choice(selection, IdProducto, IdCategoria, IdCampania, Habilitado) {
					if (selection.value==1) {
						window.location.href="/<?=$confRoot[1]?>/bo/productos/ABM_new.php?page=<?=$_GET['page']?>&IdProducto="+IdProducto+"&IdCampania="+IdCampania;
						document.getElementById("P"+IdProducto+"PC"+IdCategoria+"CA"+IdCampania).disabled="disabled";
					}
					else if (selection.value==2) {
						window.location.href="/<?=$confRoot[1]?>/bo/productos/ABM_stock_new.php?IdProducto="+IdProducto+"&IdCampania="+IdCampania;
						document.getElementById("P"+IdProducto+"PC"+IdCategoria+"CA"+IdCampania).disabled="disabled";
					}
					else if (selection.value==3) {
						window.open("/<?=$confRoot[1]?>/front/catalogo/detalle.php?IdCampania="+IdCampania+"&IdCategoria="+IdCategoria+"&IdProducto="+IdProducto );
					}
					else if (selection.value==4) {
						var textA = "Quieres Habilitar este Producto?";
						if (Habilitado == 1) {
							textA = "Quieres Deshabilitar este Producto?";
						}
						var answer = confirm(textA);
						window.location.href="/<?=$confRoot[1]?>/logica_bo/productos/actionform/productos_habilitar_deshabilitar.php?IdProducto="+IdProducto+"&IdCampania="+IdCampania+"&Habilitado="+Habilitado;
						document.getElementById("P"+IdProducto+"PC"+IdCategoria+"CA"+IdCampania).disabled="disabled";
					}
					else if (selection.value==5) {
						window.location.href="/<?=$confRoot[1]?>/bo/productos/prod_exporter.php?IdProducto="+IdProducto;
					}
				}
				$('input.ocultar').each(function(k, el) {
					el.value = ($(el).data('oculto') ? 'Activar' : 'Ocultar');
					var IdProducto = $(el).data('idproducto');
					
					$(el).click(function() {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/productos/actionform/productos_ocultar.php?page=<?=$_GET['page']?>&IdProducto="+IdProducto+"&IdCampania=<?=$_GET['IdCampania']?>&Ocultar="+($(this).data('oculto') ? '0' : '1');
					})
				})
			</script>
			
		</div>
		
	</body>
</html>
