function ErrorMSJ(code, def)
{
    if(arrErrores["CAMPANIAS"][code] != undefined)
        document.getElementById("msjError").innerHTML = arrErrores["CAMPANIAS"][code];
    else
        document.getElementById("msjError").innerHTML = code;
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtNombre.value.length == 0)
        {
            arrError.push(arrErrores["CAMPANIAS"]["NOMBRE"]);
        }
        if(frm['FechaInicio[cbxDia]'].value == 0 || frm['FechaInicio[cbxMes]'].value == 0 || frm['FechaInicio[cbxAnio]'].value == 0)
        {
            arrError.push(arrErrores["CAMPANIAS"]["FECHA DE INICIO"]);
        }
        if(frm['FechaFin[cbxDia]'].value == 0 || frm['FechaFin[cbxMes]'].value == 0 || frm['FechaFin[cbxAnio]'].value == 0)
        {
            arrError.push(arrErrores["CAMPANIAS"]["FECHA DE FIN"]);
        }
        if(frm['VisibleDesde[cbxDia]'].value == 0 || frm['VisibleDesde[cbxMes]'].value == 0 || frm['VisibleDesde[cbxAnio]'].value == 0)
        {
            arrError.push(arrErrores["CAMPANIAS"]["VISIBLE DESDE"]);
        }
        if(frm.txtMaxProd.value <= 0 || frm.txtMaxProd.value >= 6 )
        {
            arrError.push(arrErrores["CAMPANIAS"]["MAXPROD"]);
        }               
        if (arrError.length>0)
        {
            var error = ""
            for(i=0; i<arrError.length; i++)
            {
                error +="*" + arrError[i] + "\n";
            }
            alert(error);
            return false;
        }
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}
function irVolver()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"campanias/index.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CAMPANIAS"]["SELECCIONE"]);
        else
            window.location.href="ABM.php?IdCampania="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVer()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CAMPANIAS"]["SELECCIONE"]);
        else
            window.location.href="Ver.php?IdCampania="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CAMPANIAS"]["SELECCIONE"]);
        else
        {
            if(confirm(arrErrores["CAMPANIAS"]["ELIMINAR"]))
                //alert("../../logica_bo/campanias/actionform/eliminar_campania.php?IdCampania="+dt_getCeldaSeleccionada(0));
                window.location.href="../../logica_bo/campanias/actionform/eliminar_campania.php?IdCampania="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}
function irCategoria()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CAMPANIAS"]["SELECCIONE"]);
        else
        {
            window.location.href="index_categorias.php?IdCampania="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}
function irDescargarPedido()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CAMPANIAS"]["SELECCIONE"]);
        else
        {
            window.location.href="../../logica_bo/campanias/actionform/campaniacsv.php?IdCampania="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}
function irDescargarFactura()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CAMPANIAS"]["SELECCIONE"]);
        else
        {
            window.location.href="informes/facturas.php?IdCampania="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}
function irDescargarCSVDHL()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CAMPANIAS"]["SELECCIONE"]);
        else
        {
            window.location.href="../../logica_bo/campanias/actionform/csvdhl.php?IdCampania="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}

function irDescargarXLSFactura()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CAMPANIAS"]["SELECCIONE"]);
        else
        {
            window.location.href="../../logica_bo/campanias/actionform/csvdhl_new.php?IdCampania="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}

function irDescargarOrdenPedido()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CAMPANIAS"]["SELECCIONE"]);
        else
        {
            window.location.href="../../logica_bo/campanias/actionform/ordenpedido.php?IdCampania="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}
function irDescargarInformePedido()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CAMPANIAS"]["SELECCIONE"]);
        else
        {
            window.location.href="../../logica_bo/campanias/actionform/informepedido.php?IdCampania="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}

function irDescargarProdcampania()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CAMPANIAS"]["SELECCIONE"]);
        else
        {
            window.location.href="../../logica_bo/campanias/actionform/informeprodcampania.php?IdCampania="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}