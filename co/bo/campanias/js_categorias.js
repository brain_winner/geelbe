function ErrorMSJ(code, def)
{
    if(arrErrores["CATEGORIAS"][code] != undefined)
        document.getElementById("msjError").innerHTML = arrErrores["CATEGORIAS"][code];
    else
        document.getElementById("msjError").innerHTML = code;
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtNombre.value.length == 0)
        {
            arrError.push(arrErrores["CATEGORIAS"]["NOMBRE"]);
        }
        if(frm.txtOrden.value.length == 0)
        {
            arrError.push(arrErrores["CATEGORIAS"]["ORDEN"]);
        }
        if (arrError.length>0)
        {
            var error = ""
            for(i=0; i<arrError.length; i++)
            {
                error +="*" + arrError[i] + "\n";
            }
            alert(error);
            return false;
        }
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}
function irVolver()
{
    try
    {
        history.back(0);
    }
    catch(e)
    {
        throw e;
    }
}
function AgregarPPNuevo()
{
    try
    {
        var IdProducto = document.getElementById("cbxProducto").value;
        if(IdProducto == 0)
        {
            alert(arrErrores["CATEGORIAS"]["SELECCIONE_PRODUCTO"]);
            return false;
        }
        if(tbl_Buscar(IdProducto))
        {
            alert(arrErrores["CATEGORIAS"]["PRODUCTO_EXISTE"]);
            return false;
        }
        var Producto = document.getElementById("cbxProducto").options[document.getElementById("cbxProducto").selectedIndex].innerHTML;
        tbl_AgregarFila(IdProducto, Producto);
        document.getElementById("cbxProducto").focus();
    }
    catch(e)
    {
        throw e;
    }
}
function quitarAtributo()
{
    try
    {
        var Tabla = document.getElementById("tbl");
        if(confirm(arrErrores["CATEGORIAS"]["ELIMINAR_PRODUCTOS"]))
        {
            Tabla.deleteRow(tbl_getFIlaSeleccionada());
        }
        
    }
    catch(e)
    {
        throw e;
    }               
}
function irNuevo()
{
    try
    {
        document.getElementById('iCategorias').src="ABM_Categorias.php?IdPadre="+document.getElementById('txtIdPadre').value+"&IdCampania="+document.getElementById('txtIdCampania').value;
    }
    catch(e)
    {
        throw e;
    }
}
function Seleccionado()
{
    if(document.forms[0].opt)
    {
        if(document.forms[0].opt.length>0)
        {
            for(i=0; i<=document.forms[0].opt.length; i++)
            {
                if(document.forms[0].opt[i].checked == true)
                {
                    return document.forms[0].opt[i].value;
                }
            }
        }
        else
        {
            if(document.forms[0].opt.checked == true)
                return document.forms[0].opt.value;
        }   
    }
    return false;
}
function irAgregar()
{
    try
    {
        var Estado = Seleccionado();
        if(Estado == false)
            alert(arrErrores["CATEGORIAS"]["SELECCIONE"]);
        else
            document.getElementById('iCategorias').src="ABM_Categorias.php?IdPadre="+Estado+"&IdCampania="+document.getElementById('txtIdCampania').value;
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        var Estado = Seleccionado();
        if(Estado == false)
            alert(arrErrores["CATEGORIAS"]["SELECCIONE"]);
        else
            document.getElementById('iCategorias').src="ABM_Categorias.php?IdCategoria="+Estado;
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar()
{
    try
    {
        var Estado = Seleccionado();
        if(Estado == false)
            alert(arrErrores["CATEGORIAS"]["SELECCIONE"]);
        else
            if(confirm(arrErrores["CATEGORIAS"]["ELIMINAR"]))
                window.location.href="../../logica_bo/campanias/actionform/eliminar_categoria.php?IdCategoria="+Estado;
    }
    catch(e)
    {
        throw e;
    }
}