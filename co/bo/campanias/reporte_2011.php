<?
	header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=reporte_campanias_2011.csv");

try {
	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	ValidarUsuarioLogueadoBo(2);
 
} catch(ACCESOException $e) {
	die;
}   
 
try {
	$conexion = Conexion::nuevo();
	$conexion->setQuery('select c.Nombre, c.IdCampania, c.FechaFin, SUM(p.Total - p.GastosEnvio) as Facturado, SUM(p.GastosEnvio) as Envio, COUNT(IdPedido) as Pedidos from pedidos p inner join campanias c on p.IdCampania = c.IdCampania where p.IdEstadoPedidos IN (2,3,6,7,9) and date(c.FechaFin) BETWEEN "2011-01-01 00:00:00" and "2011-12-31 23:59:59" group by p.IdCampania');
	$campanias = $conexion->DevolverQuery();	

	$csv = "Nombre;Fecha Fin;Facturado sin Envio;Gastos de Envio;Margen;Pedidos\n";
		
	foreach($campanias AS $campania){
		$conexion->setQuery('select SUM((prod.PVenta - prod.PCompra)*pped.Cantidad) as Margen from pedidos p inner join productos_x_pedidos pped on p.IdPedido = pped.IdPedido inner join productos_x_proveedores pp on pped.IdCodigoProdInterno = pp.IdCodigoProdInterno inner join productos prod on prod.IdProducto = pp.IdProducto where p.IdCampania = '.$campania["IdCampania"].' and p.IdEstadoPedidos IN (2,3,6,7)');
		$datosCampania = $conexion->DevolverQuery();	
		$csv .=$campania["Nombre"].";".$campania["FechaFin"].";".$campania["Facturado"].";".$campania["Envio"].";".$datosCampania[0]["Margen"].";".$campania["Pedidos"]."\n";
	}	
} catch(Exception $e){
	echo $e;
}

echo $csv;
?>
