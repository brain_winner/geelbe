<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title></title>
		<style type="text/css">
			body {
				font-family: Verdana, Arial, Helvetica, sans-serif;
				font-size: 62.5%
			}

			table {
				border-collapse: collapse;
				border-bottom: 2px solid #000;
			}
			
			table.intervalos {
				border-collapse: collapse;
				border-bottom: 1px solid #000;
			}

			table td, table th {
				border: 2px double #000;
				padding: 10px;
			}
			
			table.intervalos td, table th {
				border: 1px solid #000;
				padding: 5px;
			}
			
			table td {
				border-bottom: 1px solid #000;
				vertical-align: top;
			}
			
			table td.single {
				border-top: 1px solid #000 !important;
			}
			
			table td.total {
				vertical-align: bottom;
			}
		</style>
	</head>
	<body>
	
<?

//	$IdCampania = $HTTP_GET_VARS["nroCamp"];

	if(!isset($_GET['IdCampania']))
   		die;
   		
   	preg_match('/[0-9 ,]+/', $_GET['IdCampania'], $matches);
   	if(!isset($matches[0]) || $matches[0] != $_GET['IdCampania'])
   		die;

	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));     
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
       
   	$oConexion = Conexion::nuevo();
	 
	//$IdCampania = '112';	
	
	function getAge($Birthdate)
	{
        // Explode the date into meaningful variables
        list($BirthYear,$BirthMonth,$BirthDay) = explode("-", $Birthdate);
        // Find the differences
        $YearDiff = date("Y") - $BirthYear;
        $MonthDiff = date("m") - $BirthMonth;
        $DayDiff = date("d") - $BirthDay;
        // If the birthday has not occured this year
        if ($DayDiff < 0 || $MonthDiff < 0)
          $YearDiff--;
        return $YearDiff;
	}
	
	//Inicializo Variables
	
	//Numero de Campania
	$IdCampania = $_GET['IdCampania'];
	$DatosAtributos = array();
	$DatosProductos = array();
		

	//$conexion = new Conexion();
	/*$conexion->setQuery('SELECT p.IdPedido, pd.Apellido, pd.Nombre, DATE_FORMAT(p.Fecha, "%e-%c-%Y") AS Fecha, pd.NroDoc, CONCAT(pd.codTelefono,"-",pd.Telefono) AS Telefono,
CONCAT(pd.celucodigo,"-",pd.celunumero) AS Celuluar, us.NombreUsuario AS Email,
CONCAT(pd.Domicilio," ",pd.Numero," ",IF(pd.Piso NOT LIKE "",CONCAT("P",pd.Piso),"")," ",pd.Puerta) AS Direccion,
pd.Poblacion AS Localidad, pv.Nombre AS Provincia, pd.CP, pa.Nombre AS Pais, cam.Nombre AS Campania, ep.Descripcion AS EstadoDelPedido,
pro.Referencia, pxp.Cantidad, pro.Nombre as Producto, pxp.Precio AS PU, pxp.IdCodigoProdInterno, p.GastosEnvio, bonos.Importe AS Bono, p.Total, IF(us.IdPerfil <> 1,"Si","No") AS PedidoInterno
FROM pedidos AS p
JOIN usuarios AS us ON us.IdUsuario = p.IdUsuario
JOIN campanias AS cam ON cam.IdCampania = p.IdCampania
JOIN productos_x_pedidos AS pxp ON p.IdPedido = pxp.IdPedido
JOIN productos_x_proveedores AS pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
JOIN productos_x_categorias AS pxc ON pxpr.IdProducto = pxc.IdProducto
JOIN categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
JOIN productos AS pro ON pro.IdProducto = pxpr.IdProducto
JOIN pedidos_direcciones AS pd ON pd.IdPedido = p.IdPedido
LEFT JOIN paises AS pa ON pa.IdPais = pd.IdPais
LEFT JOIN provincias AS pv ON pv.IdProvincia = pd.IdProvincia
LEFT JOIN estadospedidos AS ep ON ep.IdEstadoPedido = p.IdEstadoPedidos
LEFT JOIN (SELECT SUM(Importe) AS Importe, IdPedido FROM bonos_x_pedidos AS bxp GROUP BY bxp.IdPedido)bonos ON bonos.IdPedido = p.IdPedido
WHERE p.IdEstadoPedidos IN (2,7,9) AND cam.IdCampania IN ('.$IdCampania.')');*/


	//Modificado 2009-05-06
	$data = mysql_query('SELECT 
		p.IdPedido, p.IdEstadoPedidos, pd.Apellido, pd.Nombre, DATE_FORMAT(p.Fecha, "%e-%c-%Y") AS Fecha, 
		pd.NroDoc, CONCAT(pd.codTelefono,"-",pd.Telefono) AS Telefono, 
		CONCAT(pd.celucodigo,"-",pd.celunumero) AS Celuluar, 
		us.NombreUsuario AS Email, CONCAT(pd.Domicilio," ",pd.Numero," ",
		IF(pd.Piso NOT LIKE "",CONCAT("P",pd.Piso),"")," ",pd.Puerta) AS Direccion,
		pd.Poblacion AS Localidad, pv.Nombre AS Provincia, pd.CP, pa.Nombre AS Pais, 
		cam.FechaFin, cam.Nombre AS Campania, ep.Descripcion AS EstadoDelPedido, pro.Referencia, pxp.Cantidad, 
		pro.Alto, pro.Ancho, pro.Profundidad, pro.Peso, pro.Nombre as Producto, pxp.Precio AS PU, pxp.IdCodigoProdInterno, p.GastosEnvio, 
		bonos.Importe AS Bono, p.Total, IF(us.IdPerfil <> 1,"Si","No") AS PedidoInterno,
		dhlz.Nombre as NombreZona, dhlz.IdZona, per.FechaNacimiento, per.idTratamiento, us.IdUsuario
		
	FROM 
		pedidos AS p 
		
	LEFT JOIN 
		usuarios AS us ON us.IdUsuario = p.IdUsuario
		
	LEFT JOIN
		datosusuariosdirecciones AS dir ON us.IdUsuario = dir.IdUsuario

	LEFT JOIN
		datosusuariospersonales AS per ON us.IdUsuario = per.IdUsuario
		
	LEFT JOIN
		dhl_zonas_x_provincias AS dhlzp ON dhlzp.IdProvincia = dir.IdProvincia
		
	LEFT JOIN
		dhl_zonas AS dhlz ON dhlz.IdZona = dhlzp.IdZona
		
	LEFT JOIN	
		campanias AS cam ON cam.IdCampania = p.IdCampania
		
	LEFT JOIN
		productos_x_pedidos AS pxp ON p.IdPedido = pxp.IdPedido
		
	LEFT JOIN
		productos_x_proveedores AS pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
		
	LEFT JOIN
		productos_x_categorias AS pxc ON pxpr.IdProducto = pxc.IdProducto
		
	LEFT JOIN
		categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
		
	LEFT JOIN
		productos AS pro ON pro.IdProducto = pxpr.IdProducto
		
	LEFT JOIN
		pedidos_direcciones AS pd ON pd.IdPedido = p.IdPedido
		
	LEFT JOIN 
		paises AS pa ON pa.IdPais = pd.IdPais
		
	LEFT JOIN
		provincias AS pv ON pv.IdProvincia = pd.IdProvincia
		
	LEFT JOIN
		estadospedidos AS ep ON ep.IdEstadoPedido = p.IdEstadoPedidos
		
	LEFT JOIN
		(SELECT SUM(Importe) AS Importe, IdPedido FROM bonos_x_pedidos AS bxp GROUP BY bxp.IdPedido) bonos ON bonos.IdPedido = p.IdPedido
		
	WHERE
		p.IdEstadoPedidos IN (2,3,6,9) AND cam.IdCampania IN ('.mysql_real_escape_string($IdCampania).') AND dir.TipoDireccion = 0');

	/*
	
	$conexion->setQuery("SELECT pro.Referencia, SUM(pxp.Cantidad) as Cantidad, pro.Nombre, 
	pxp.IdCodigoProdInterno, pro.PCompra as PCU, SUM(pro.PCompra*pxp.Cantidad) AS CostoTotal
	FROM pedidos AS p
	JOIN campanias AS cam ON cam.IdCampania = p.IdCampania
	JOIN productos_x_pedidos AS pxp ON p.IdPedido = pxp.IdPedido
	JOIN productos_x_proveedores AS pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
	JOIN productos_x_categorias AS pxc ON pxpr.IdProducto = pxc.IdProducto
	JOIN categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
	JOIN productos AS pro ON pro.IdProducto = pxpr.IdProducto
	WHERE p.IdEstadoPedidos IN (2,7,9) AND cam.IdCampania IN (".$IdCampania.")
	GROUP By pxp.IdCodigoProdInterno");
	
	*/
	
	if(mysql_num_rows($data) == 0) {
		echo 'No hay pedidos para esta campa�a.';
		die();
	}
	
	$DatosProductos = array();
	while($d = mysql_fetch_assoc($data))
		$DatosProductos[] = $d;
	
	//Extraigo Los IdCodigoProdInterno
	foreach($DatosProductos AS $item){
		$item["IdCodigoProdInterno"] = intval($item["IdCodigoProdInterno"]);
		if($item["IdCodigoProdInterno"]>0) $Codigos[] = $item["IdCodigoProdInterno"];
	}
	
	$Codigos = array_unique($Codigos);
	
	//Busco los atributos para esos codigos
	if(count($Codigos)>0){	
	
		$data = mysql_query("SELECT pxa.IdCodigoProdInterno, at.Nombre as Atributo, pxa.Valor
		FROM productos_x_atributos AS pxa
		JOIN atributos_x_clases AS axc ON axc.idAtributoClase = pxa.idAtributoClase
		JOIN atributos AS at ON at.IdAtributo = axc.IdAtributo
		WHERE  pxa.IdCodigoProdInterno IN (".implode(",",$Codigos).") ORDER BY at.Nombre");		
		
		$Atributos = array();
		while($d = mysql_fetch_assoc($data))
			$Atributos[] = $d;
		
		$camposAtributos = array();
		
		//Armo un bonito array
		foreach($Atributos as $atrib){
			$DatosAtributos[$atrib["IdCodigoProdInterno"]][$atrib["Atributo"]] = $atrib["Valor"];
			$camposAtributos[] = $atrib["Atributo"];
		}
				
	}

	echo '<h1 style="font-size:25px; margin-bottom:10px;">Resultado de campa�a '.$DatosProductos[0]['Campania'].'</h1>';
	
	echo '<table>
		<tr>
			<th>ID Pedido</th>
			<th>Apellido</th>
			<th>Nombre</th>
			<th>Direcci�n</th>
			<th>Localidad</th>
			<th>Provincia</th>
			<th>Zona</th>
			<th>Pa�s</th>
			<th>CP</th>
			<th>Tel�fono</th>
			<th>Campa�a</th>
			<th>Cantidad</th>
			<th>Referencia</th>
			<th>Producto</th>
			<th>Atributos</th>
			<th>Alto</th>
			<th>Ancho</th>
			<th>Profundidad</th>
			<th>Peso Volum�trico</th>
			<th>Peso</th>
			<th>Intervalo Envio</th>
			<th>Precio Envio</th>
			<th>Precio Unitario</th>
			<th>Precio Total</th>
			<th>Bono Descuento</th>
			<th>Total</th>
			<th>Pedido Interno</th>
			<th>Estado Pedido</th>
		</tr>';
	
	//Constru�r array
	$Datos = null;
	foreach($DatosProductos as $i => $p) {
	
		$id = $p['IdPedido'];
	
		if(!isset($Datos[$id])) {
			$Datos[$id] = $p;
			unset(
				$Datos[$id]['Producto'],
				$Datos[$id]['Cantidad'],
				$Datos[$id]['PU']
			);
			$Datos[$id]['Productos'] = array();
			$Datos[$id]['TotalPedido'] = 0;
		}
		
		$nombre = '';
		if(isset($DatosAtributos[$p['IdCodigoProdInterno']])) {
			
			$nombre = '(';
			
			foreach($DatosAtributos[$p['IdCodigoProdInterno']] as $n => $valor)
				$nombre .= $n.': '.$valor.', ';
				
			$nombre = substr($nombre, 0, -2).')';
		}
		
		$Datos[$id]['Productos'][$p['IdCodigoProdInterno']] = array(
			'Cantidad' => $p['Cantidad'],
			'Producto' => $p['Producto'],
			'Referencia' => $p['Referencia'],
			'Alto' => $p['Alto'],
			'Ancho' => $p['Ancho'],
			'Profundidad' => $p['Profundidad'],
			'Peso' => $p['Peso'],
			'PU' => $p['PU'],
			'Atributo' => $nombre
		);
		
		$Datos[$id]['TotalPedido'] += $p['PU']*$p['Cantidad'];
	
	}
	
	$pedidos = 0;
	$productos = 0;
	$facturado = 0;
	$totaldhl = 0;
	$usuarios = array();
	$sexo = array(1 => array(), 2 => array());
	
	foreach($Datos as $i => $p) {
				
		echo '<tr>
			<td rowspan="'.count($p['Productos']).'">'.$i.'</td>
			<td rowspan="'.count($p['Productos']).'">'.$p['Apellido'].'</td>
			<td rowspan="'.count($p['Productos']).'">'.$p['Nombre'].'</td>
			<td rowspan="'.count($p['Productos']).'">'.$p['Direccion'].'</td>
			<td rowspan="'.count($p['Productos']).'">'.$p['Localidad'].'</td>
			<td rowspan="'.count($p['Productos']).'">'.$p['Provincia'].'</td>
			<td rowspan="'.count($p['Productos']).'">'.$p['NombreZona'].'</td>
			<td rowspan="'.count($p['Productos']).'">'.$p['Pais'].'</td>
			<td rowspan="'.count($p['Productos']).'">'.$p['CP'].'</td>
			<td rowspan="'.count($p['Productos']).'">'.$p['Telefono'].'</td>
			<td rowspan="'.count($p['Productos']).'">'.$p['Campania'].'</td>';
		
		$n = 0;
		
		$usuarios[$p['IdUsuario']] = getAge($p['FechaNacimiento']);
		$sexo[$p['idTratamiento']][$p['IdUsuario']] = 1;
		
		foreach($p['Productos'] as $a => $j) {
		
			if($n != 0)
				echo '<tr>';
				
			$peso = $j['Alto']*$j['Ancho']*$j['Profundidad']/5000;
	
			if($peso>$j['Peso'])		
				$max = $peso;
			else
				$max = $j['Peso'];
				
			$q = mysql_query("SELECT IdPeso, Hasta FROM dhl_pesos WHERE Desde <= ".$max." ORDER BY Desde DESC");

			if(mysql_num_rows($q) == 0) {
				$intervalo = 'N/A';
				$preciodhl = 'N/A';
			} else {
				$intervalo = mysql_fetch_assoc($q);
				$intervalo = $intervalo['IdPeso'];
				$limite = $intervalo['Hasta'];
				
				$noPrecio = false;
				
				if(is_numeric($p['IdZona'])) {
					$q = mysql_query("SELECT Tarifa, TarifaKG FROM dhl_precios WHERE IdPeso = ".$intervalo." AND IdZona = ".$p['IdZona']." LIMIT 1");
				} else
					$noPrecio = true;

				if($noPrecio || mysql_num_rows($q) == 0)
					$preciodhl = 'N/A';
				else {
				
					//Exceso de peso
					$exceso = $max - $limite;
					$preciodhl = mysql_fetch_assoc($q);
					
					if($exceso > 0)
						$exceso *= $preciodhl['TarifaKG'];
					else
						$exceso = 0;
					
					$preciodhl = $preciodhl['Tarifa'] + $exceso;
					$totaldhl += $preciodhl;
				}
			}			
		
			echo '<td '.($n > 0 ? 'class="single"' : '').'>'.$j['Cantidad'].'</td>
			<td '.($n > 0 ? 'class="single"' : '').'>'.$j['Referencia'].'</td>
			<td '.($n > 0 ? 'class="single"' : '').'>'.$j['Producto'].'</td>
			<td '.($n > 0 ? 'class="single"' : '').'>'.$j['Atributo'].'</td>
			<td '.($n > 0 ? 'class="single"' : '').'>'.$j['Alto'].' cm</td>
			<td '.($n > 0 ? 'class="single"' : '').'>'.$j['Ancho'].' cm</td>
			<td '.($n > 0 ? 'class="single"' : '').'>'.$j['Profundidad'].' cm</td>
			<td '.($n > 0 ? 'class="single"' : '').'>'.number_format($peso, 2).' kg</td>
			<td '.($n > 0 ? 'class="single"' : '').'>'.$j['Peso'].' kg</td>
			<td '.($n > 0 ? 'class="single"' : '').'>'.$intervalo.'</td>
			<td '.($n > 0 ? 'class="single"' : '').'>$'.number_format($preciodhl, 2).'</td>
			<td '.($n > 0 ? 'class="single"' : '').'>$'.number_format($j['PU'], 2).'</td>
			<td '.($n > 0 ? 'class="single"' : '').'>$'.number_format($j['PU']*$j['Cantidad'], 2).'</td>';
			
			if($n == 0) {
				echo '<td class="bono" rowspan="'.count($p['Productos']).'">$'.number_format($p['Bono'], 2).'</td>';
				echo '<td class="total" rowspan="'.count($p['Productos']).'">$'.number_format($p['TotalPedido']-$p['Bono'], 2).'</td>';
				echo '<td rowspan="'.count($p['Productos']).'">'.($p['IdEstadoPedidos'] == 9 ? 'S�' : 'No').'</td>';
				echo '<td rowspan="'.count($p['Productos']).'">'.$p['EstadoDelPedido'].'</td>';
			}
			
			echo '</tr>';
			
			$n++;
			
			$productos++;
			$facturado += $p['TotalPedido']-$p['Bono'];
			
		}
		
		$pedidos++;
		
	}
	
	echo '</table>';
	
	$time = strtotime($DatosProductos[0]['FechaFin']);

	echo '
	<div style="width:16%; float:left; margin-top:25px;">
	
	<table class="intervalos" border="0" cellpadding="0" cellspacing="0">
	<tr>
    	<td><strong>Intervalo Envio</strong></td>
        <td><strong>Peso</strong></td>
    </tr>
    <tr>
    	<td align="center">1</td>
        <td>Hasta 2 Kg.</td>
    </tr>
    <tr>
    	<td align="center">2</td>
        <td>De 2,1 a 5 Kg.</td>
    </tr>
    <tr>
    	<td align="center">3</td>
        <td>De 5,1 a 10 Kg.</td>
    </tr>
    <tr>
    	<td align="center">4</td>
        <td>De 10,1 a 15 Kg.</td>
    </tr>
    <tr>
    	<td align="center">5</td>
        <td>De 15,1 a 20 Kg.</td>
    </tr>
    <tr>
    	<td align="center">6</td>
        <td>De 20,1 a 25 Kg.</td>
    </tr>
    <tr>
    	<td align="center">7</td>
        <td>De 25,1 a 30 Kg.</td>
    </tr>
</table> </div>
	<div style="width:20%; margin-left:15px; float:left; font-size:12px; line-height:23px; margin-top:25px; padding:15px; border:1px solid #000000;">';			
	echo '<p>Fecha de finalizaci�n de campa�a: '.date('m-d-Y H:i:s', $time).'<br />';
	echo 'Total de pedidos: '.$pedidos.'<br />';
	echo 'Total de productos vendidos: '.$productos.'<br />';
	echo 'Total facturado: $'.number_format($facturado, 2).'<br />';
	echo 'Costo total Envio: $'.number_format($totaldhl, 2).'</p>';
	echo '</div>
		<div style="width:20%; float:left; margin-left:15px; font-size:12px; line-height:23px; margin-top:25px; padding:15px; border:1px solid #000000;">';
	
	$edad = 0;
	foreach($usuarios as $id => $ed)
		$edad += $ed;
		
	echo '<p>Edad promedio de los usuarios: '.number_format($edad/count($usuarios), 1).'<br />
	Cantidad de Hombres: '.count($sexo[1]).' ('.number_format(count($sexo[1])*100/count($usuarios), 2).'%)<br />
	Cantidad de Mujeres: '.count($sexo[2]).' ('.number_format(count($sexo[2])*100/count($usuarios), 2).'%)</p>
	</div>';
?>
</body>
</html>