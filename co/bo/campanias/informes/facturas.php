<?php

	if(!isset($_GET['IdCampania']))
   		die;

	header("Content-type: text/html;charset=UTF-8"); 

	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));     
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
       
   	$oConexion = Conexion::nuevo();    
   		
   	preg_match('/[0-9 ,]+/', $_GET['IdCampania'], $matches);
   	if(!isset($matches[0]) || $matches[0] != $_GET['IdCampania'])
   		die;
  
  
   	$IdCampania = $_GET['IdCampania'];

	require_once 'lib/Factura.php';
	
	//$IdCampania = '136';	
	
   	$query = mysql_query('SELECT 
		p.IdPedido, pd.IdUsuario, pd.Apellido, pd.Nombre, DATE_FORMAT(p.Fecha, "%e-%c-%Y") AS Fecha, 
		pd.NroDoc, CONCAT(pd.codTelefono,"-",pd.Telefono) AS Telefono, 
		CONCAT(pd.celucodigo,"-",pd.celunumero) AS Celuluar, 
		us.NombreUsuario AS Email, CONCAT(pd.Domicilio," ",pd.Numero," ",
		IF(pd.Piso NOT LIKE "",CONCAT("P",pd.Piso),"")," ",pd.Puerta) AS Direccion,
		c.Descripcion AS Localidad, pv.Nombre AS Provincia, pd.CP, pa.Nombre AS Pais, 
		cam.Nombre AS Campania, cam.FechaFin, ep.Descripcion AS EstadoDelPedido, pro.Referencia, pxp.Cantidad, 
		pro.Nombre as Producto, pxp.Precio AS PU, pxp.IdCodigoProdInterno, p.GastosEnvio, 
		bonos.Importe AS Bono, p.Total, IF(us.IdPerfil <> 1,"Si","No") AS PedidoInterno 
		
	FROM 
		pedidos AS p 
		
	LEFT JOIN 
		usuarios AS us ON us.IdUsuario = p.IdUsuario
		
	LEFT JOIN	
		campanias AS cam ON cam.IdCampania = p.IdCampania
		
	LEFT JOIN
		productos_x_pedidos AS pxp ON p.IdPedido = pxp.IdPedido
		
	LEFT JOIN
		productos_x_proveedores AS pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
		
	LEFT JOIN
		productos_x_categorias AS pxc ON pxpr.IdProducto = pxc.IdProducto
		
	LEFT JOIN
		categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
		
	LEFT JOIN
		productos AS pro ON pro.IdProducto = pxpr.IdProducto
		
	LEFT JOIN
		pedidos_direcciones AS pd ON pd.IdPedido = p.IdPedido
		
	LEFT JOIN
		ciudades AS c ON c.IdCiudad = pd.IdCiudad
		
	LEFT JOIN 
		paises AS pa ON pa.IdPais = pd.IdPais
		
	LEFT JOIN
		provincias AS pv ON pv.IdProvincia = pd.IdProvincia
		
	LEFT JOIN
		estadospedidos AS ep ON ep.IdEstadoPedido = p.IdEstadoPedidos
		
	LEFT JOIN
		(SELECT SUM(Importe) AS Importe, IdPedido FROM bonos_x_pedidos AS bxp GROUP BY bxp.IdPedido) bonos ON bonos.IdPedido = p.IdPedido
		
	WHERE
		p.IdEstadoPedidos IN (2,7,9) AND cam.IdCampania IN ('.mysql_real_escape_string($IdCampania).')
		
	GROUP BY
		CONCAT(p.IdPedido,"+",pxp.IdCodigoProdInterno)');
		
	if(mysql_num_rows($query) == 0) {
		echo 'No hay pedidos disponibles para la campaña seleccionada.';
		die;
	}
		
	$f = new Factura();	
	while($d = mysql_fetch_assoc($query)) {
		$data = mysql_query("SELECT pxa.IdCodigoProdInterno, at.Nombre as Atributo, pxa.Valor
		FROM productos_x_atributos AS pxa
		JOIN atributos_x_clases AS axc ON axc.idAtributoClase = pxa.idAtributoClase
		JOIN atributos AS at ON at.IdAtributo = axc.IdAtributo
		WHERE  pxa.IdCodigoProdInterno = ".$d['IdCodigoProdInterno']." ORDER BY at.Nombre");
		
		$att = '';
		while($d2 = mysql_fetch_assoc($data))
			$att .= $d2['Atributo'].': '.$d2['Valor'].', ';
			
		$att = substr($att, 0, -2);
		
		if($att != '')
			$d['Producto'] .= ' ('.$att.')';
	
		$f->AgregarPedido($d);
	}
	$f->generar();

?>