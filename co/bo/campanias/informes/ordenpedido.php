<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title></title>
		<style type="text/css">
			body {
				font-family: Helvetica;
				font-size: 62.5%
			}
			
			h1 {
				text-align: center;
			}
			
			.productos {
				border-collapse: collapse;
				border: 1px solid #000;
				width: 100%;
				margin: 20px 0;
			}
						
			.productos td, .productos th {
				border: 1px solid #000;
				padding: 5px;
			}
			
			.productos th {
				border-bottom-width: 2px;
			}
			
			.productos td.number {
				text-align: right;
			}
			
			hr {
				margin: 30px 0;
			}
		</style>

	</head>
	<body>
	
<?

	if(!isset($_GET['IdCampania']))
   		die;
   		
   	preg_match('/[0-9 ,]+/', $_GET['IdCampania'], $matches);
   	if(!isset($matches[0]) || $matches[0] != $_GET['IdCampania'])
   		die;
   		
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));     
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
       
   	$oConexion = Conexion::nuevo();
   		
///NOTA DE PEDIDO!
	
	
	
	//Inicializo Variables
	
	//Numero de Campania
	$IdCampania = $_GET['IdCampania'];
	$DatosAtributos = array();
	$DatosProductos = array();
		

	//$conexion = new Conexion();
	/*$conexion->setQuery('SELECT p.IdPedido, pd.Apellido, pd.Nombre, DATE_FORMAT(p.Fecha, "%e-%c-%Y") AS Fecha, pd.NroDoc, CONCAT(pd.codTelefono,"-",pd.Telefono) AS Telefono,
CONCAT(pd.celucodigo,"-",pd.celunumero) AS Celuluar, us.NombreUsuario AS Email,
CONCAT(pd.Domicilio," ",pd.Numero," ",IF(pd.Piso NOT LIKE "",CONCAT("P",pd.Piso),"")," ",pd.Puerta) AS Direccion,
pd.Poblacion AS Localidad, pv.Nombre AS Provincia, pd.CP, pa.Nombre AS Pais, cam.Nombre AS Campania, ep.Descripcion AS EstadoDelPedido,
pro.Referencia, pxp.Cantidad, pro.Nombre as Producto, pxp.Precio AS PU, pxp.IdCodigoProdInterno, p.GastosEnvio, bonos.Importe AS Bono, p.Total, IF(us.IdPerfil <> 1,"Si","No") AS PedidoInterno
FROM pedidos AS p
JOIN usuarios AS us ON us.IdUsuario = p.IdUsuario
JOIN campanias AS cam ON cam.IdCampania = p.IdCampania
JOIN productos_x_pedidos AS pxp ON p.IdPedido = pxp.IdPedido
JOIN productos_x_proveedores AS pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
JOIN productos_x_categorias AS pxc ON pxpr.IdProducto = pxc.IdProducto
JOIN categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
JOIN productos AS pro ON pro.IdProducto = pxpr.IdProducto
JOIN pedidos_direcciones AS pd ON pd.IdPedido = p.IdPedido
LEFT JOIN paises AS pa ON pa.IdPais = pd.IdPais
LEFT JOIN provincias AS pv ON pv.IdProvincia = pd.IdProvincia
LEFT JOIN estadospedidos AS ep ON ep.IdEstadoPedido = p.IdEstadoPedidos
LEFT JOIN (SELECT SUM(Importe) AS Importe, IdPedido FROM bonos_x_pedidos AS bxp GROUP BY bxp.IdPedido)bonos ON bonos.IdPedido = p.IdPedido
WHERE p.IdEstadoPedidos IN (2,7,9) AND cam.IdCampania IN ('.$IdCampania.')');*/


	//Modificado 2009-05-06
	$data = mysql_query('SELECT 
		p.IdPedido, pd.Apellido, pd.Nombre, DATE_FORMAT(p.Fecha, "%e-%c-%Y") AS Fecha, 
		pd.NroDoc, CONCAT(pd.codTelefono,"-",pd.Telefono) AS Telefono, 
		CONCAT(pd.celucodigo,"-",pd.celunumero) AS Celuluar, 
		us.NombreUsuario AS Email, CONCAT(pd.Domicilio," ",pd.Numero," ",
		IF(pd.Piso NOT LIKE "",CONCAT("P",pd.Piso),"")," ",pd.Puerta) AS Direccion,
		pd.Poblacion AS Localidad, pv.Nombre AS Provincia, pd.CP, pa.Nombre AS Pais, 
		cam.Nombre AS Campania, ep.Descripcion AS EstadoDelPedido, pro.Referencia, pxp.Cantidad, 
		pro.Nombre as Producto, pxp.Precio AS PU, pxp.IdCodigoProdInterno, p.GastosEnvio, 
		bonos.Importe AS Bono, p.Total, IF(us.IdPerfil <> 1,"Si","No") AS PedidoInterno 
		
	FROM 
		pedidos AS p 
		
	LEFT JOIN 
		usuarios AS us ON us.IdUsuario = p.IdUsuario
		
	LEFT JOIN	
		campanias AS cam ON cam.IdCampania = p.IdCampania
		
	LEFT JOIN
		productos_x_pedidos AS pxp ON p.IdPedido = pxp.IdPedido
		
	LEFT JOIN
		productos_x_proveedores AS pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
		
	LEFT JOIN
		productos_x_categorias AS pxc ON pxpr.IdProducto = pxc.IdProducto
		
	LEFT JOIN
		categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
		
	LEFT JOIN
		productos AS pro ON pro.IdProducto = pxpr.IdProducto
		
	LEFT JOIN
		pedidos_direcciones AS pd ON pd.IdPedido = p.IdPedido
		
	LEFT JOIN 
		paises AS pa ON pa.IdPais = pd.IdPais
		
	LEFT JOIN
		provincias AS pv ON pv.IdProvincia = pd.IdProvincia
		
	LEFT JOIN
		estadospedidos AS ep ON ep.IdEstadoPedido = p.IdEstadoPedidos
		
	LEFT JOIN
		(SELECT SUM(Importe) AS Importe, IdPedido FROM bonos_x_pedidos AS bxp GROUP BY bxp.IdPedido) bonos ON bonos.IdPedido = p.IdPedido
		
	WHERE
		p.IdEstadoPedidos IN (2,9) AND cam.IdCampania IN ('.mysql_real_escape_string($IdCampania).')
		
	ORDER BY pro.IdProducto');

	/*
	
	$conexion->setQuery("SELECT pro.Referencia, SUM(pxp.Cantidad) as Cantidad, pro.Nombre, 
	pxp.IdCodigoProdInterno, pro.PCompra as PCU, SUM(pro.PCompra*pxp.Cantidad) AS CostoTotal
	FROM pedidos AS p
	JOIN campanias AS cam ON cam.IdCampania = p.IdCampania
	JOIN productos_x_pedidos AS pxp ON p.IdPedido = pxp.IdPedido
	JOIN productos_x_proveedores AS pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
	JOIN productos_x_categorias AS pxc ON pxpr.IdProducto = pxc.IdProducto
	JOIN categorias AS cat ON pxc.IdCategoria = cat.IdCategoria
	JOIN productos AS pro ON pro.IdProducto = pxpr.IdProducto
	WHERE p.IdEstadoPedidos IN (2,9) AND cam.IdCampania IN (".$IdCampania.")
	GROUP By pxp.IdCodigoProdInterno");
	
	*/
	
	if(mysql_num_rows($data) == 0) {
		echo 'No hay pedidos para esta campa�a.';
		die();
	}
	
	$DatosProductos = array();
	while($d = mysql_fetch_assoc($data))
		$DatosProductos[] = $d;
	
	//Extraigo Los IdCodigoProdInterno
	foreach($DatosProductos AS $item){
		$item["IdCodigoProdInterno"] = intval($item["IdCodigoProdInterno"]);
		if($item["IdCodigoProdInterno"]>0) $Codigos[] = $item["IdCodigoProdInterno"];
	}
	
	$Codigos = array_unique($Codigos);
	
	//Busco los atributos para esos codigos
	if(count($Codigos)>0){	
	
		$data = mysql_query("SELECT pxa.IdCodigoProdInterno, at.Nombre as Atributo, pxa.Valor
		FROM productos_x_atributos AS pxa
		JOIN atributos_x_clases AS axc ON axc.idAtributoClase = pxa.idAtributoClase
		JOIN atributos AS at ON at.IdAtributo = axc.IdAtributo
		WHERE  pxa.IdCodigoProdInterno IN (".implode(",",$Codigos).") ORDER BY at.Nombre");		
		
		$Atributos = array();
		while($d = mysql_fetch_assoc($data))
			$Atributos[] = $d;
		
		$camposAtributos = array();
		
		//Armo un bonito array
		foreach($Atributos as $atrib){
			$DatosAtributos[$atrib["IdCodigoProdInterno"]][$atrib["Atributo"]] = $atrib["Valor"];
			$camposAtributos[] = $atrib["Atributo"];
		}
				
	}

	
	echo '<h1>Orden de pedido '.$DatosProductos[0]['Campania'].'</h1>';
	
	$aux = array('id' => null);
	
	echo '<table class="productos">
			<tr>
				<th>Cantidad</th>
				<th>Producto</th>
				<th>Atributo</th>
				<th>PU</th>
				<th>Precio Total</th>
			</tr>';

	$total = 0;
	foreach($DatosProductos as $i => $p) {
		
		$nombre = '';
		if(isset($DatosAtributos[$p['IdCodigoProdInterno']])) {
			
			$nombre = '(';
			
			foreach($DatosAtributos[$p['IdCodigoProdInterno']] as $n => $valor)
				$nombre .= $n.': '.$valor.', ';
				
			$nombre = substr($nombre, 0, -2).')';
		}
		
		if($aux['id'] != $p['IdCodigoProdInterno']) {

			if($aux['id'] != null) {	
				
				echo '<tr>
					<td>'.$aux['Cantidad'].'</td>
					<td>'.$aux['Producto'].'</td>
					<td>'.$aux['Att'].'</td>
					<td class="number">$'.number_format($aux['PU'], 2).'</td>
					<td class="number">$'.number_format($aux['Total'], 2).'</td>
				</tr>';
				
				$total += $aux['Total'];
				
				$aux['Cantidad'] = 0;
				$aux['Total'] = 0;
				
			}
			
		}
		
		$aux['id'] = $p['IdCodigoProdInterno'];
		$aux['Cantidad'] += $p['Cantidad'];
		$aux['Total'] += $p['Cantidad']*$p['PU'];
		$aux['PU'] = $p['PU'];
		$aux['Producto'] = $p['Producto'];
		$aux['Att'] = $nombre;
	}
	
	echo '<tr>
			<td>'.$aux['Cantidad'].'</td>
			<td>'.$aux['Producto'].'</td>
			<td>'.$aux['Att'].'</td>
			<td class="number">$'.number_format($aux['PU'], 2).'</td>
			<td class="number">$'.number_format($aux['Total'], 2).'</td>
		</tr>';
		
	$total += $aux['Total'];
	
	echo '</table>';
	
	echo '<p><strong>Total a pagar: </strong> $'.number_format($total, 2).'</p>';
	
?>
</body>
</html>