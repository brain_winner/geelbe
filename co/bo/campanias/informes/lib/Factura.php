<?php

/**
  * PDF class, PDF.php
  * PDF invoices and document management
  * @category classes
  *
  *
  */

require_once('fpdf/fpdf.php');

class Factura extends FPDF
{
	
	private $order;
	private $pagn = 0;
	private $aux = null;
	private $total = array('Total' => 0, 'Bono' => 0, 'PU' => 0, 'GastosEnvio' => 0);
	
	function __construct() {
		parent::__construct('P', 'mm', 'Letter');
		$this->SetTextColor(72, 66, 65);
		$this->setMargins(25, 0);
		
		require_once dirname(__FILE__).'/EnLetras.php';
	}
	
	/**
	* Invoice header
	*/
	function Header()
	{
		$this->Cell(115);
		$this->Image(dirname(__FILE__).'/factura.jpg', 0, 0, 210, 297);
		
		
		//$time = strtotime($this->order['FechaFin']);
		
		$this->SetFont('Arial', 'B', 10);
		/*$this->SetXY(45, 50.5);*/
		$this->SetXY(43, 54);
		$this->Cell(80, 10, date("d/m/Y"), 0, 0, 'L');
		
		$this->SetFont('Arial', '', 7);
		/*$this->SetXY(31, 78.7);*/
		$this->SetXY(31, 83);
		$this->MultiCell(120, 4, $this->order['Direccion'].
		"\n".$this->order['Localidad'].". ".$this->order['Provincia'].". CP: ".$this->order['CP'].
		" - ".$this->order['Pais'], 0);
		
	}

	/**
	* Main
	*
	*/
	public function AgregarPedido($order)
	{
	 	global $cookie;
	 	
	 	$this->order = $order;

		if($order['IdPedido'] != $this->aux) {
			
			$this->aux = $order['IdPedido'];
				
			if($this->pagn != 0) {
				$this->Pie();	
			}
			
			$this->AliasNbPages();
			$this->AddPage();
			
			/*$this->SetXY(31, 63);*/
			$this->SetXY(31, 66);
			$this->SetFont('Arial', '', 10);
			$this->Multicell($width, 5, $order['Nombre'].' '.$order['Apellido']."\n (".$order['IdUsuario'].')', 0, 'L');
/*			
			$this->SetXY(134, 62);
			$this->Cell($width, 10, 'Consumidor Final', 0, 'L');
			
			$this->SetXY(58, 71);
			$this->Cell($width, 10, 'Efectivo', 0, 'L');

			$this->SetXY(37, 78.6);
			$this->Cell($width, 10, 'Pedido #'.sprintf('%06d', $this->order['IdPedido']), 0, 'L');
*/			
			$this->SetXY(35, 100);
			
			$this->SetFont('Arial', '', 10);
			
		}
			
		$this->ProdTab();
		$this->pagn++;

	}

	function Pie() {
		/*
		 * Display price summation
		 */
		
		$this->total['Total'] += $this->total['GastosEnvio']-$this->total['Bono'];
		 
		$this->SetX(35);
		 
		$before = $this->GetY();
        $this->MultiCell(20, 10, '1', 0);
        
		$lineHeight = $this->GetY() - $before;
		$this->SetXY($this->GetX() + 25, $before);
		$this->Cell(20, $lineHeight, '', 0, 0, 'R');
		$this->Cell(80, $lineHeight, 'Gastos de envio', 0, 0, 'L');
		$this->Cell(30, $lineHeight, '$'.number_format(($this->total['GastosEnvio'] != '' ? $this->total['GastosEnvio'] : 0), 2), 0, 0, 'R');
		$this->Ln();
		
		$this->SetX(35);

		$before = $this->GetY();
        $this->MultiCell(20, 10, '1', 0);
        
		$lineHeight = $this->GetY() - $before;
		$this->SetXY($this->GetX() + 25, $before);
		$this->Cell(20, $lineHeight, '', 0, 0, 'R');
		$this->Cell(80, $lineHeight, 'Bono', 0, 0, 'L');
		$this->Cell(30, $lineHeight, '- $'.number_format(($this->total['Bono'] != '' ? $this->total['Bono'] : 0), 2), 0, 0, 'R');
		$this->Ln();

		$v = new EnLetras;

		$this->SetFont('Arial', 'B', 12);
		/*$this->SetXY(154, 215);*/
		$this->SetXY(154, 211.8);
		$this->Cell(28, 0, '$'.number_format($this->total['Total'], 2), 0, 0, 'R');
		
		$this->SetFont('Arial', 'B', 10);
		$this->SetXY(59, 230);
		$this->Cell(28, 0, str_replace('Un Mil', 'Mil', $v->ValorEnLetras($this->total['Total'], 'pesos')), 0, 0, 'L');
		$this->Ln(4);
		
		$this->total = array('Total' => 0, 'Bono' => 0, 'PU' => 0, 'GastosEnvio' => 0);
	}

	/**
	* Product table with price, quantities...
	*/
	function ProdTab()
	{
		
		$before = $this->GetY();
		$this->SetX(70);
        $this->MultiCell(60, 10, $this->order['Producto'], 0);
        
		$lineHeight = $this->GetY() - $before;
		$afterProduct = $this->GetX();
		
		$this->SetXY(35, $before);
		
		$this->Cell(20, $lineHeight, $this->order['Cantidad'], 0, 0, 'L');
		$this->Cell(20, $lineHeight, $this->order['IdCodigoProdInterno'], 0, 0, 'L');
		
		$this->SetX(130);
		$this->Cell(20, $lineHeight, '$'.number_format($this->order['PU'], 2), 0, 0, 'R');
		$this->Cell(30, $lineHeight, '$'.number_format($this->order['PU']*$this->order['Cantidad'], 2), 0, 0, 'R');
		$this->Ln();
		
		$this->total['GastosEnvio'] = $this->order['GastosEnvio'];
		$this->total['Bono'] = $this->order['Bono'];
		
		$this->total['PU'] += $this->order['PU']*$this->order['Cantidad'];
		$this->total['Total'] += $this->order['PU']*$this->order['Cantidad'];
			
	}
	
	function Generar() {
		$this->Pie();
		return $this->Output('pedidos.pdf', 'D');
	}
}

?>