<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("ordenamientos"));
    $postURL = Aplicacion::getIncludes("post", "campanias");
    $objCampania = dmCampania::getByIdCampania((isset($_GET["IdCampania"])) ? $_GET["IdCampania"]:0);
    try
    {
        ValidarUsuarioLogueadoBo(9);
    }
    catch(ACCESOException $e)
    {
    ?>
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM campanias</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/campanias/js.js");?>" type="text/javascript"></script>
    <script src="../../logica_bo/scripts/jscal/js/jscal2.js" type="text/javascript"></script>
    <script src="../../logica_bo/scripts/jscal/js/lang/es.js" type="text/javascript"></script>
    <link href="../../logica_bo/scripts/jscal/css/jscal2.css" rel="stylesheet" type="text/css" />
    <link href="../../logica_bo/scripts/jscal/css/steel/steel.css" rel="stylesheet" type="text/css" />
    <link href="<?=UrlResolver::getCssBaseUrl("bo/style/css.css")?>" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmABMCampanias" name="frmABMCampanias" method="POST" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="txtIdCampania" name="txtIdCampania" value="<?=($objCampania->getIdCampania() >0)?Aplicacion::Encrypter($objCampania->getIdCampania()):0?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="Error"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["campanias"]);
            ?>
            <tr>
                <td>Nombre</td>
                <td><input type="text" id="txtNombre" name="txtNombre" maxlength="80" size=50 value="<?=$objCampania->getNombre()?>"></td>
            </tr>
            <tr>
                <td>Descripcion</td>
                <td>
                <textarea id="txtDescripcion" name="txtDescripcion" cols="40" rows="5"><?=$objCampania->getDescripcion()?></textarea>
            </tr>
            <tr>
                <td>Fecha de inicio</td>
                <td>
                    <select name="FechaInicio[cbxDia]" id="FechaInicio[cbxDia]">
                      <option value="0" >D&iacute;a</option>
                        <?
                        for ($i=1; $i<=31; $i++)
                        {
                            ?><option value="<?=$i?>"><?=$i?></option><?
                        }
                        ?>
                    </select>
                    &nbsp;/&nbsp;
                    <select name="FechaInicio[cbxMes]" id="FechaInicio[cbxMes]">
                        <option value="0" >Mes</option>
                        <option value="1" >enero</option>
                        <option value="2" >febrero</option>
                        <option value="3" >marzo</option>
                        <option value="4" >abril</option>
                        <option value="5" >mayo</option>
                        <option value="6" >junio</option>
                        <option value="7" >julio</option>
                        <option value="8" >agosto</option>
                        <option value="9" >septiembre</option>
                        <option value="10" >octubre</option>
                        <option value="11" >noviembre</option>
                        <option value="12" >diciembre</option>
                    </select>
                    &nbsp;/&nbsp;
                    <select name="FechaInicio[cbxAnio]" id="FechaInicio[cbxAnio]">
                        <?
                        for ($i=(date("Y")-4); ($i<=date("Y")+4); $i++)
                        {
                            ?><option value="<?=$i?>"><?=$i?></option><?
                        }
                        ?>
                     </select>
					 <script>
						document.getElementById("FechaInicio[cbxAnio]").value="<?php echo date("Y") ?>";
                     </script>					 
                     &nbsp;&nbsp;
                     <input type="text" name="FechaInicio[txtHora]" id="FechaInicio[txtHora]" maxlength="2" size=1 value="0"/>
                     &nbsp;:&nbsp;
                     <input type="text" name="FechaInicio[txtMin]" id="FechaInicio[txtMin]" maxlength="2" size=1 value="0"/>
                    <?
                    if($objCampania->getFechaInicio() != "")
                    {
                        $Fecha = $objCampania->getFechaInicio();
                        $Fecha = explode("/", $Fecha);
                        $Hora = explode(" ",$Fecha[2]);
                        $Fecha[2] = $Hora[0];
                        $Hora = explode(":", $Hora[1]);
                        ?>
                        <script>
                            document.getElementById("FechaInicio[cbxDia]").value="<?=$Fecha[0]?>";
                            document.getElementById("FechaInicio[cbxMes]").value="<?=$Fecha[1]?>";
                            document.getElementById("FechaInicio[cbxAnio]").value="<?=$Fecha[2]?>";
                            document.getElementById("FechaInicio[txtHora]").value="<?=$Hora[0]?>";
                            document.getElementById("FechaInicio[txtMin]").value="<?=$Hora[1]?>";
                        </script>
                        <?
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>Fecha de fin</td>
                <td>
                    <select name="FechaFin[cbxDia]" id="FechaFin[cbxDia]" onchange="setTiempoEntrega()">
                      <option value="0" >D&iacute;a</option>
                        <?
                        for ($i=1; $i<=31; $i++)
                        {
                            ?><option value="<?=$i?>"><?=$i?></option><?
                        }
                        ?>
                    </select>
                    &nbsp;/&nbsp;
                    <select name="FechaFin[cbxMes]" id="FechaFin[cbxMes]" onchange="setTiempoEntrega()">
                        <option value="0" >Mes</option>
                        <option value="1" >enero</option>
                        <option value="2" >febrero</option>
                        <option value="3" >marzo</option>
                        <option value="4" >abril</option>
                        <option value="5" >mayo</option>
                        <option value="6" >junio</option>
                        <option value="7" >julio</option>
                        <option value="8" >agosto</option>
                        <option value="9" >septiembre</option>
                        <option value="10" >octubre</option>
                        <option value="11" >noviembre</option>
                        <option value="12" >diciembre</option>
                    </select>
                    &nbsp;/&nbsp;
                    <select name="FechaFin[cbxAnio]" id="FechaFin[cbxAnio]" onchange="setTiempoEntrega()">
                        <?
                        for ($i=(date("Y")-4); ($i<=date("Y")+4); $i++)
                        {
                            ?><option value="<?=$i?>"><?=$i?></option><?
                        }
                        ?>
                     </select>
					 <script>
						document.getElementById("FechaFin[cbxAnio]").value="<?php echo date("Y") ?>";
						
						function setTiempoEntrega() {
							if(document.getElementById('TiempoEntregaIn').value == '')
								updateTiempo('TiempoEntregaIn', 'in', 15)
								
							if(document.getElementById('TiempoEntregaFn').value == '')
								updateTiempo('TiempoEntregaFn', 'fn', 20)
						}
						
						function updateTiempo(visible, full, add) {
							
							var dia = document.getElementById('FechaFin[cbxDia]');
							var mes = document.getElementById('FechaFin[cbxMes]');
							var anno = document.getElementById('FechaFin[cbxAnio]');
							
							dia = dia.options[dia.selectedIndex].value
							mes = mes.options[mes.selectedIndex].value - 1
							anno = anno.options[anno.selectedIndex].value
							
							var fecha = new Date(anno, mes, dia);
							fecha.setDate(fecha.getDate() + add);
							
							document.getElementById(visible).value = fecha.getDate()+'/'+(fecha.getMonth()+1);
							document.getElementById(full).value = fecha.getFullYear()+'-'+(fecha.getMonth()+1)+'-'+fecha.getDate();
							
						}
                
                </script>		
                     &nbsp;&nbsp;
                     <input type="text" name="FechaFin[txtHora]" id="FechaFin[txtHora]" maxlength="2" size=1 value="0"/>
                     &nbsp;:&nbsp;
                     <input type="text" name="FechaFin[txtMin]" id="FechaFin[txtMin]" maxlength="2" size=1 value="0"/>
                    <?
                    if($objCampania->getFechaFin() != "")
                    {
                        $Fecha = $objCampania->getFechaFin();
                        $Fecha = explode("/", $Fecha);
                        $Hora = explode(" ",$Fecha[2]);
                        $Fecha[2] = $Hora[0];
                        $Hora = explode(":", $Hora[1]);
                        ?>
                        <script>
                            document.getElementById("FechaFin[cbxDia]").value="<?=$Fecha[0]?>";
                            document.getElementById("FechaFin[cbxMes]").value="<?=$Fecha[1]?>";
                            document.getElementById("FechaFin[cbxAnio]").value="<?=$Fecha[2]?>";
                            document.getElementById("FechaFin[txtHora]").value="<?=$Hora[0]?>";
                            document.getElementById("FechaFin[txtMin]").value="<?=$Hora[1]?>";
                        </script>
                        <?
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>Visible desde</td>
                <td>
                    <select name="VisibleDesde[cbxDia]" id="VisibleDesde[cbxDia]">
                      <option value="0" >D&iacute;a</option>
                        <?
                        for ($i=1; $i<=31; $i++)
                        {
                            ?><option value="<?=$i?>"><?=$i?></option><?
                        }
                        ?>
                    </select>
                    &nbsp;/&nbsp;
                    <select name="VisibleDesde[cbxMes]" id="VisibleDesde[cbxMes]">
                        <option value="0" >Mes</option>
                        <option value="1" >enero</option>
                        <option value="2" >febrero</option>
                        <option value="3" >marzo</option>
                        <option value="4" >abril</option>
                        <option value="5" >mayo</option>
                        <option value="6" >junio</option>
                        <option value="7" >julio</option>
                        <option value="8" >agosto</option>
                        <option value="9" >septiembre</option>
                        <option value="10" >octubre</option>
                        <option value="11" >noviembre</option>
                        <option value="12" >diciembre</option>
                    </select>
                    &nbsp;/&nbsp;
                    <select name="VisibleDesde[cbxAnio]" id="VisibleDesde[cbxAnio]">
                        <?
                        for ($i=(date("Y")-4); ($i<=date("Y")+4); $i++)
                        {
                            ?><option value="<?=$i?>"><?=$i?></option><?
                        }
                        ?>
                     </select>
					 <script>
						document.getElementById("VisibleDesde[cbxAnio]").value="<?php echo date("Y") ?>";
                     </script>		
                     &nbsp;&nbsp;
                     <input type="text" name="VisibleDesde[txtHora]" id="VisibleDesde[txtHora]" maxlength="2" size=1 value="0"/>
                     &nbsp;:&nbsp;
                     <input type="text" name="VisibleDesde[txtMin]" id="VisibleDesde[txtMin]" maxlength="2" size=1 value="0"/>
                    <?
                    if($objCampania->getVisibleDesde() != "")
                    {
                        $Fecha = $objCampania->getVisibleDesde();
                        $Fecha = explode("/", $Fecha);
                        $Hora = explode(" ",$Fecha[2]);
                        $Fecha[2] = $Hora[0];
                        $Hora = explode(":", $Hora[1]);
                        ?>
                        <script>
                            document.getElementById("VisibleDesde[cbxDia]").value="<?=$Fecha[0]?>";
                            document.getElementById("VisibleDesde[cbxMes]").value="<?=$Fecha[1]?>";
                            document.getElementById("VisibleDesde[cbxAnio]").value="<?=$Fecha[2]?>";
                            document.getElementById("VisibleDesde[txtHora]").value="<?=$Hora[0]?>";
                            document.getElementById("VisibleDesde[txtMin]").value="<?=$Hora[1]?>";
                        </script>
                        <?
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>Maximos productos a vender</td>
                <td><input type="text" id="txtMaxProd" name="txtMaxProd" maxlength="100" size=10 value="<?=$objCampania->getMaxProd()?>"/></td>
            </tr>
            <tr>
                <td>Descuento (%)</td>
                <td><input type="text" id="txtDescuentoMax" name="txtDescuentoMax" maxlength="100" size=10 value="<?=$objCampania->getDescuentoMax()?>"/></td>
            </tr>
            <tr>
                <td>Ordenamiento</td>
                <td>
                	<select id="cbxOrdenamiento" name="cbxOrdenamiento">
                     <!--<option value=1>...Seleccionar...</option>-->
                     <?
                       $collOrdenamientos = dmOrdenamientos::getAll();
                       foreach ($collOrdenamientos as $Ordenamiento)
                       {
                           if ($objCampania->getIdOrdenamiento() == $Ordenamiento["idOrdenamiento"])
                           {
                               ?><option selected value="<?=$Ordenamiento["idOrdenamiento"]?>"><?=$Ordenamiento["Descripcion"]?></option><?
                           }
                           else
                           {
                               ?><option value="<?=$Ordenamiento["idOrdenamiento"]?>"><?=$Ordenamiento["Descripcion"]?></option><?
                           }
                         
                       }
                     ?>
                 </select>
                </td>
            </tr>
			<tr>
					<td>Monto m&iacute;nimo para Envio Gratis (Valor 0.00 = Desactivado)</td>
					<td>
						<label>
							<input type="radio" id="txtMinMontoEnvioGratis1" name="txtMinMontoEnvioGratis" value="1" <?=($objCampania->getMinMontoEnvioGratis() == 0 ? 'checked="checked"' : '')?> onClick="mostrarOcultar(this)" />
							S&iacute;
						</label>
						<label>
							<input type="radio" id="txtMinMontoEnvioGratis0" name="txtMinMontoEnvioGratis" value="0" <?=($objCampania->getMinMontoEnvioGratis() == 0 ? 'checked="checked"' : '')?> onClick="mostrarOcultar(this)"/>
							No
						</label>						
					</td>
			</tr>
			<tr>
					<td>
					</td>
					<td><input type="text" id="txtMinMontoEnvioGratis" name="txtMinMontoEnvioGratis" maxlength="20" size=10 value="<?=$objCampania->getMinMontoEnvioGratis()?>"  style="display:none;"/></td>
					</td>
				</tr>
			<tr>
				<script>
				function mostrarOcultar(obj) {
				  txtMinMontoEnvioGratis.style.display = (obj.id == "txtMinMontoEnvioGratis1") ? 'block' : 'none';
				}  
				</script>				
            	<td>Fecha de despacho</td>
				
            	<td>entre : <input type="text" name="TiempoEntregaIn" id="TiempoEntregaIn" value="<?=date("d/m", strtotime($objCampania->getTiempoEntregaIn()))?>" />
            	&nbsp;y&nbsp;  <input type="text" name="TiempoEntregaFn" id="TiempoEntregaFn" value="<?=date("d/m", strtotime($objCampania->getTiempoEntregaFn()))?>" />
            	
            			<input type="hidden" name="TiempoEntregaIn" id="in" value="<?=$objCampania->getTiempoEntregaIn()?>" />
            			<input type="hidden" name="TiempoEntregaFn" id="fn" value="<?=$objCampania->getTiempoEntregaFn()?>" />
            			
            			
            			<script>
								Calendar.setup({
									trigger: "TiempoEntregaIn",
									inputField: "TiempoEntregaIn",
									dateFormat: "%d/%m",
									onSelect: function() {
										var date = Calendar.intToDate(this.selection.get());
										document.getElementById('in').value = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()
									}
								});

								Calendar.setup({
									trigger: "TiempoEntregaFn",
									inputField: "TiempoEntregaFn",
									dateFormat: "%d/%m",
									onSelect: function() {
										var date = Calendar.intToDate(this.selection.get());
										document.getElementById('fn').value = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()
									}
								});
							</script>
               	</td>
            </tr>
            <tr>
            	<td>Fecha de corte</td>
				
            	<td><input type="text" name="FechaCorte" id="FechaCorte" value="<?=date("d/m", strtotime($objCampania->getFechaCorte()))?>" />
            	
            			<input type="hidden" name="FechaCorte" id="corte" value="<?=$objCampania->getFechaCorte()?>" />
            			
            			
            			<script>
								Calendar.setup({
									trigger: "FechaCorte",
									inputField: "FechaCorte",
									dateFormat: "%d/%m",
									onSelect: function() {
										var date = Calendar.intToDate(this.selection.get());
										document.getElementById('corte').value = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()
									}
								});

							</script>
               	</td>
            </tr>
            <tr>
            	<td>Segunda fecha de despacho</td>
				
            	<td>entre : <input type="text" name="TiempoEntregaIn2" id="TiempoEntregaIn2" value="<?=date("d/m", strtotime($objCampania->getTiempoEntregaIn2()))?>" />
            	&nbsp;y&nbsp;  <input type="text" name="TiempoEntregaFn2" id="TiempoEntregaFn2" value="<?=date("d/m", strtotime($objCampania->getTiempoEntregaFn2()))?>" />
            	
            			<input type="hidden" name="TiempoEntregaIn2" id="in2" value="<?=$objCampania->getTiempoEntregaIn2()?>" />
            			<input type="hidden" name="TiempoEntregaFn2" id="fn2" value="<?=$objCampania->getTiempoEntregaFn2()?>" />
            			
            			
            			<script>
								Calendar.setup({
									trigger: "TiempoEntregaIn2",
									inputField: "TiempoEntregaIn2",
									dateFormat: "%d/%m",
									onSelect: function() {
										var date = Calendar.intToDate(this.selection.get());
										document.getElementById('in2').value = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()
									}
								});

								Calendar.setup({
									trigger: "TiempoEntregaFn2",
									inputField: "TiempoEntregaFn2",
									dateFormat: "%d/%m",
									onSelect: function() {
										var date = Calendar.intToDate(this.selection.get());
										document.getElementById('fn2').value = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()
									}
								});
							</script>
               	</td>
            </tr>
            <tr>
                <td>Pantalla de Bienvenida</td>
                <td>
                	<label>
	                	<input type="radio" id="welcome1" name="welcome" value="1" <?=($objCampania->getWelcome() == 1 ? 'checked="checked"' : '')?> />
    	            	S&iacute;
    	            </label>
    	            
    	            <label>
	                	<input type="radio" id="welcome0" name="welcome" value="0" <?=($objCampania->getWelcome() == 0 ? 'checked="checked"' : '')?> />
    	            	No
    	            </label>
                	
                </td>
            </tr>
            <tr>
                <td>Tipo de campa&ntilde;a</td>
                <td>
                	<label>
 	                	<input type="radio" id="coleccion" name="coleccion" value="0" <?=($objCampania->getColeccion() == 0 ? 'checked="checked"' : '')?> />
   	            	Descuento
    	            </label>
    	            
    	            <label>
	                	<input type="radio" id="coleccion" name="coleccion" value="1" <?=($objCampania->getColeccion() == 1 ? 'checked="checked"' : '')?> />
    	            	Colecci&oacute;n
    	            </label>
                	
                </td>
            </tr>
            <tr>
                <td colspan="2"><br/>Propiedades:
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table style="border-top:1px solid black;border-left:1px solid black;border-bottom:1px solid black;border-top:1px solid black;border-right:1px solid black" border="0">
                        <tr>
                        <?
                            $fila=0;
                            $dtPropiedades = dmCampania::getPropiedades($objCampania->getIdCampania());
                            foreach($dtPropiedades as $ID => $propiedad)
                            {
                                ?>
                                    <td>
                                        <img src="<?=UrlResolver::getImgBaseUrl('front/campanias/propImg/'.$propiedad["IdPropiedadCampamia"].'.gif')?>" title="<?=$propiedad["Nombre"]?>" width="30" height="30" />
                                        <input <?
                                        if($ID >= 0 && $ID <= 3)
                                        {
                                            echo "checked ='checked' onclick='this.checked = true;'";
                                        }

                                        ?> type="checkbox" id="chkPropiedad[<?=$propiedad["IdPropiedadCampamia"]?>]" name="chkPropiedad[<?=$propiedad["IdPropiedadCampamia"]?>]" value=1 <?=($propiedad["Esta"] == 1)?"checked=\"checked\"":""?> />
                                    </td>
                                <?
                                $fila++;
                                if($fila == 4)
                                {
                                    ?>
                                    </tr>
                                    <tr>
                                    <?
                                    $fila=0;
                                }
                            }
                        ?>
                        </tr>
                    </table>
                    <br/>
                </td>
            </tr>
            <tr>
                <td>Imagen de bienvenida</td>
                <td><input type="file" id="txtWelcome" name="txtWelcome" size=50/></td>
            </tr>
            <tr>
                <td>Trailer de la campa&ntilde;a</td>
                <td><input type="file" id="txtTrailer" name="txtTrailer" size=50/></td>
            </tr>
            <tr>
            <tr>
                <td>Logo de campa&ntilde;a</td>
                <td><input type="file" id="txtLogo" name="txtLogo" size=50/></td>
            </tr>
            <!--<tr>
                <td>Logo transparente (PNG)</td>
                <td><input type="file" id="txtLogo2" name="txtLogo2" size=50/></td>
            </tr>-->
            <tr>
                <td>Imagen principal de campa&ntilde;a</td>
                <td><input type="file" id="txtImagen" name="txtImagen" size=50/></td>
            </tr>
			 <tr>
                <td>Imagen principal de campa&ntilde;a gris (opcional)</td>
                <td><input type="file" id="txtGreyImagen" name="txtGreyImagen" size=50/></td>
            </tr>
			<tr>
                <td>Imagen principal de vidriera</td>
                <td><input type="file" id="txtVidrieraImagen" name="txtVidrieraImagen" size=50/></td>
            </tr>
			<tr>
                <td>Imagen principal de vidriera off</td>
                <td><input type="file" id="txtVidrieraOffImagen" name="txtVidrieraOffImagen" size=50/></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <!--<tr>
                <td>Imagen bienvenida (600px ancho)</td>
                <td><input type="file" id="txtNewsBienvenida" name="txtNewsBienvenida" size=50/></td>
            </tr>
            <tr>
            <tr>
                <td>Imagen despedida (600px ancho)</td>
                <td><input type="file" id="txtNewsDespedida" name="txtNewsDespedida" size=50/></td>
            </tr>
            <tr>
                <td>Imagen &uacute;ltimos d&iacute;as (300px ancho)</td>
                <td><input type="file" id="txtNewsUltimos" name="txtNewsUltimos" size=50/></td>
            </tr>-->
            <tr>
                <td colspan=2 align="center">
                <br/>
               	<table width="80%" style="border:1px solid" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="100%" style="border-bottom:1px solid">Imagenes de campa&ntilde;as</td>
                    </tr>
                <?
                    if(count($objCampania->getFiles())>0)
                    {
                        $i=0;
                        foreach($objCampania->getFiles() as $file)
                        {
                            ?>
                            <tr>
                                <td><?=$file["nombre"]?></td>
                                <td><a style="cursor:pointer" onClick="window.open('<?=$file['public_link'];?>')">Ver</a></td>
                            </tr>
                            <?
                            $i++;
                        }
                    }
                    else
                    {
                        ?><td colspan=2>No hay imagenes cargadas</td><?
                    }

                ?>
                </table>
                <br/>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>