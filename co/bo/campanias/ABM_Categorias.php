<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("ordenamientos"));
    try
    {
        ValidarUsuarioLogueadoBo(9);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }    
    $postURL = Aplicacion::getIncludes("post", "campanias");
    $objCategoria = dmCategoria::getByIdCategoria((isset($_GET["IdCategoria"])) ? $_GET["IdCategoria"]:0);
    if(isset($_GET["IdPadre"]))
    {
        $objCategoria->setIdPadre($_GET["IdPadre"]);
        $objPadre = dmCategoria::getByIdCategoria($_GET ["IdPadre"]);
    }
    $Profundidad =  (isset($objPadre))?$objPadre->getDeep()+1:$objCategoria->getDeep();
    
    if(isset($_GET["IdCampania"]))
        $objCategoria->setIdCampania($_GET["IdCampania"]);
    $objDT = new TablaDinamica();
    $objDT->addColumna(0,"IdProducto");
    $objDT->addColumna(1,"Producto");
    $objDT->addBotones(array("nombre"=>"aquitar", "tipo"=>"a", "title"=>"quitar", "innerHTML"=>"[x]", "funcion"=>"quitarAtributo"));    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM campanias</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/campanias/js_categorias.js");?>" type="text/javascript"></script>
    <link href="<?=UrlResolver::getCssBaseUrl("bo/style/css.css")?>" rel="stylesheet" type="text/css" />
</head>
<body>
<form id="frmABMCategorias" name="frmABMCategorias" method="POST" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="txtIdCategoria" name="txtIdCategoria" value="<?=($objCategoria->getIdCategoria() >0)?Aplicacion::Encrypter($objCategoria->getIdCategoria()):0?>">
        <input type="hidden" id="txtIdCampania" name="txtIdCampania" value="<?=($objCategoria->getIdCampania() >0)?Aplicacion::Encrypter($objCategoria->getIdCampania()):0?>">
        <input type="hidden" id="txtIdPadre" name="txtIdPadre" value="<?=($objCategoria->getIdPadre() >0)?Aplicacion::Encrypter($objCategoria->getIdPadre()):0?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="Error"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["categorias"]);
            ?>
            <tr>
                <td colspan="2">
                <strong>
                <?
                    if (isset($objPadre))
                    {
                        if ($objPadre->getIdPadre())
                            echo "Agregando subcategor&iacute;a a: " . $objPadre->getNombre(); 
                        else
                            echo "Agregando categor&iacute;a a: " . $objPadre->getNombre();
                    }
                        
                    else if(isset($objCategoria))
                        echo "Modificando categor&iacute;a: " . $objCategoria->getNombre(); 
                ?>
                </strong>
                </td>
            </tr>
            <tr>
                <td>Nombre</td>
                <td><input type="text" id="txtNombre" name="txtNombre" maxlength="<?=(16 - (($Profundidad - 1)*2) )?>" size=50 value="<?=$objCategoria->getNombre()?>"></td>
            </tr>
            <tr>
                <td>Habilitado</td>
                <td>
                    <input type="checkbox" id="chkHabilitado" name="chkHabilitado" value="OK">
                    <script>
                        <?
                            if($objCategoria->getHabilitado() == 1)
                            {
                                ?>document.getElementById("chkHabilitado").checked=true;<?
                            }
                        ?>
                    </script>
                </td>
            </tr>
            <tr>
                <td>Orden</td>
                <td><input type="text" id="txtOrden" name="txtOrden" maxlength="2" size=3 value="<?=$objCategoria->getOrden()?>"/></td>
            </tr>
            <br/>
            <tr>
            <td>
                
            </td>
            </tr>
            <br>
            <tr>
                <td colspan="2">
                    <!--form id="frmFiltro" name="frmFiltro" method="post"-->
                        <table style="width:100%">
                            <!--tr>
                                <td>Proveedor</td>
                                <td>Marca</td>
                                <td>Clase</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                <select id="cbxProveedores" name="cbxProveedores">
                                    <option value=0>...Seleccione...</option>
                                    <?
                                        $dtProveedores = dmProveedor::getProveedors(1,"ASC");
                                        foreach($dtProveedores as $proveedor)
                                        {
                                            ?><option value=<?=$proveedor["IdProveedor"]?>><?=$proveedor["Proveedor"]?></option><?
                                        }
                                    ?>
                                </select>
                                </td>
                                <td>
                                <select id="cbxMarca" name="cbxMarca">
                                    <option value=0>...seleccionar...</option>
                                    <?
                                      $collMarcas = dmMarcas::getMarcas(2,"ASC");
                                      foreach ($collMarcas as $Marca)
                                      {
                                        ?><option value="<?=$Marca["Codigo"]?>"><?=$Marca["Marca"]?></option><?
                                      }
                                    ?>
                                </select>
                                </td>
                                <td>
                                <select id="cbxClase" name="cbxClase">
                                    <option value=0>...seleccionar...</option>
                                    <?
                                      $collClases = dmClases::getClases(2,"ASC");
                                      foreach ($collClases as $Clase)
                                      {
                                        ?><option value="<?=$Clase["Codigo"]?>"><?=$Clase["Clase"]?></option><?
                                      }
                                    ?>
                                </select>
                                </td>
                                <td><input id="btnBuscar" name="btnBuscar" value="Buscar" type="button" onclick="document.forms['frmFiltro'].submit()" /></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="100%"><br/><strong>Agregar productos</strong></td>
                            </tr-->
                            <tr>
                                <td colspan="100%">
                                Ordenamiento &nbsp;
                                    <select id="cbxOrdenamiento" name="cbxOrdenamiento">
                                    <!--<option value=1>...Seleccionar...</option>-->
                                    <?
                                      $collOrdenamientos = dmOrdenamientos::getAll();
                                      foreach ($collOrdenamientos as $Ordenamiento)
                                      {
                                          if ($objCategoria->getIdOrdenamiento() == $Ordenamiento["idOrdenamiento"])
                                          {
                                              ?><option selected value="<?=$Ordenamiento["idOrdenamiento"]?>"><?=$Ordenamiento["Descripcion"]?></option><?
                                          }
                                          else
                                          {
                                              ?><option value="<?=$Ordenamiento["idOrdenamiento"]?>"><?=$Ordenamiento["Descripcion"]?></option><?
                                          }
                                        
                                      }
                                    ?>
                                </select>
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="100%">
                                Producto &nbsp;
                                    <select id="cbxProducto" name="cbxProducto">
                                    <option value=0>...Seleccionar...</option>
                                    <?
                                      $collProductos = dmProductos::getProductosFilrados(isset($_POST["cbxProducto"])?$_POST["cbxProducto"]:0, isset($_POST["cbxMarca"])?$_POST["cbxMarca"]:0, isset($_POST["cbxClase"])?$_POST["cbxClase"]:0);
                                      foreach ($collProductos as $Producto)
                                      {
                                        ?><option value="<?=$Producto["Codigo"]?>"><?=$Producto["Producto"]?></option><?
                                      }
                                    ?>
                                </select>
                                &nbsp;
                                <input id="btnAgregar" name="btnAgregar" value="Agregar" type="button" onclick="AgregarPPNuevo()" />
                                </td>
                            </tr>
                        </table>
                    <!--/form-->
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?
                    $objDT->show();
                    if($objCategoria->getIdCategoria()>0)
                    {
                        $dtCategorias = dmCategoria::getProductosByIdCategoria($objCategoria->getIdCategoria());
                        ?><script><?
                        foreach($dtCategorias as $categoria)
                        {
                            ?>tbl_AgregarFila(<?=$categoria["Codigo"]?>, "<?=addslashes($categoria["Producto"])?>");<?
                        }
                        ?></script><?
                    }
                ?>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                    <br/><a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo//imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>