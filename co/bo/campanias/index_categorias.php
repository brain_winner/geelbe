<?
    if(!isset($_GET["IdCampania"]))
        die("Error");
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    try
    {
        ValidarUsuarioLogueadoBo(9);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
    $postURL = Aplicacion::getIncludes("post", "categorias");
    $dtcategorias = dmCategoria::getCategorias($_GET["IdCampania"], "IS NULL");

    
    function ArmarArbolCategorias($Categorias)
    {
        ?><ol type="1"><?
        foreach($Categorias as $categoria)
        {
            ?>
                <li>
                    <input type="radio" id="opt" name="opt" value="<?=$categoria["IdCategoria"]?>"/>
                    <?
                        if($categoria["Habilitado"] == 0)
                        {
                            echo "<del title='Esta categoria esta deshabilitada'>".$categoria["Nombre"]."</del>";
                        }
                        else
                        {
                            echo $categoria["Nombre"];
                        }
                        if(is_array($categoria["subCategoria"]))
                        {
                            ArmarArbolCategorias($categoria["subCategoria"]);
                        }
                    ?>                
                </li>
            <?
        }
        ?></ol><?
    }
    /*function ArmarArraScript($Categorias)
    {
        ?><ol type="1"><?
        foreach($Categorias as $categoria)
        {
            ?>
                <li>
                    <input type="radio" id="opt" name="opt" value="<?=$categoria["IdCategoria"]?>"/>
                    <?=$categoria["Nombre"]?>
                    <?
                        if(is_array($categoria["subCategoria"]))
                        {
                            ArmarArraScript($categoria["subCategoria"]);
                        }
                    ?>                
                </li>
            <?
        }
        ?></ol><?
    }*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> categorias</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/campanias/js_categorias.js");?>" type="text/javascript"></script>
    <link href="<?=UrlResolver::getCssBaseUrl("bo/style/css.css")?>" rel="stylesheet" type="text/css" />
</head>
<body>
<input type="hidden" id="txtIdPadre" name="txtIdPadre" value="<?=$dtcategorias[0]["IdCategoria"]?>"/>
<input type="hidden" id="txtIdCampania" name="txtIdCampania" value="<?=$dtcategorias[0]["IdCampania"]?>"/>
<table style="width:100%; height:100%">
    <tr>
                <td colspan=2><span id="msjError" name="Error"></span>
                </td>
            </tr>
    <tr>
        <td width="40%" valign="top">
        Categorias de <b><?=$dtcategorias[0]["Nombre"]?></b>
        <br/><br/>
        <a href="javascript:irNuevo();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnNuevoMini','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnNuevoMini_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnNuevoMini.gif");?>" alt="Nueva categoria." name="btnNuevoMini" id="btnNuevoMini" border="0"/></a>
        <a href="javascript:irAgregar();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAgregarMini','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAgregarMini_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAgregarMini.gif");?>" alt="Agregarr una sub categoria." name="btnAgregarMini" id="btnAgregarMini" border="0"/></a>
        <a href="javascript:irEditar();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnEditarMini','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnEditarMini_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnEditarMini.gif");?>" alt="Editar categoria." name="btnEditarMini" id="btnEditarMini" border="0"/></a>
        <a href="javascript:irEliminar();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnEliminarMini','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnEliminarMini_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnEliminarMini.gif");?>" alt="Eliminar categoria." name="btnEliminarMini" id="btnEliminarMini" border="0"/></a>
        <form id="frmsubCategorias" name="frmsubCategorias">
        <?  
            (isset($dtcategorias[0]["subCategoria"]) && is_array($dtcategorias[0]["subCategoria"]))?ArmarArbolCategorias($dtcategorias[0]["subCategoria"]):"No hay categorias disponibles.";
        ?>
        </form>
        </td>
        <td width="60%" valign="top">
           <iframe style="width:100%; height:500px" frameborder="0" id="iCategorias" name="iCategorias"></iframe>
        </td>
    </tr>
</table>
<?
    if(isset($_GET["error"]))
    {
        ?>
        <script>
             ErrorMSJ("<?=$_GET["error"]?>");
        </script>
        <?
    }
?>
</body>
</html>