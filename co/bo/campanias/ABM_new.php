<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("ordenamientos"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/comerciales/clases/clscomerciales.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/comerciales/datamappers/dmcomerciales.php");
    $postURL = Aplicacion::getIncludes("post", "campanias");
    
   $listCategoriasGlobales = dmCategoriaGlobal::getCategorias();

   if(isset($_GET["IdCampania"])) {
    	$objCampania = dmCampania::getByIdCampania($_GET["IdCampania"]);
    	$listCategorias = dmCategoria::getCategoriasNew($_GET["IdCampania"]);
    	
	    list($oFechaInicio, $fiHorario) = split(' ', $objCampania->getFechaInicio());
	    list($fiHora, $fiMinutos) = split(':', $fiHorario);
	    
	    list($oFechaFin, $ffHorario) = split(' ', $objCampania->getFechaFin());
	    list($ffHora, $ffMinutos) = split(':', $ffHorario);
	    
	    list($oVisibleDesde, $fvHorario) = split(' ', $objCampania->getVisibleDesde());
	    list($fvHora, $fvMinutos) = split(':', $fvHorario);
	    
	    
	    list($inAnio, $inMes, $inDia) = split('[/.-]', $objCampania->getTiempoEntregaIn());
	    $oTiempoEntregaIn = $inDia."/".$inMes."/".$inAnio;
	    list($ffAnio, $ffMes, $ffDia) = split('[/.-]', $objCampania->getTiempoEntregaFn());
	    $oTiempoEntregaFn = $ffDia."/".$ffMes."/".$ffAnio; 
	    list($inAnio2, $inMes2, $inDia2) = split('[/.-]', $objCampania->getTiempoEntregaIn2());
	    $oTiempoEntregaIn2 = $inDia2."/".$inMes2."/".$inAnio2;
	    list($ffAnio2, $ffMes2, $ffDia2) = split('[/.-]', $objCampania->getTiempoEntregaFn2());
	    $oTiempoEntregaFn2 = $ffDia2."/".$ffMes2."/".$ffAnio2; 
	    list($ffAnio3, $ffMes3, $ffDia3) = split('[/.-]', $objCampania->getFechaCorte());
	    $oFechaCorte = $ffDia3."/".$ffMes3."/".$ffAnio3; 

    	$categoriasGlobalesProducto = dmCategoriaGlobal::getCategoriasCampanias($_GET["IdCampania"]);
    }
    else {
    	$listCategorias = array();
    	$objCampania = new MySQL_Campania();
    }
    
    
    
    try {
        ValidarUsuarioLogueadoBo(9);
    }
    catch(ACCESOException $e) {
    ?>
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>BackOffice Geelbe - Campa�a de Ventas - Alta de Campa�a de Ventas </title>
	<? Includes::Scripts(); ?>
	<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	
	<link type="text/css" href="<?=UrlResolver::getCssBaseUrl("css/ui-lightness/jquery-ui-1.8.9.custom.css")?>" rel="stylesheet" />	
	<link type="text/css" href="<?=UrlResolver::getCssBaseUrl("bo/css/ui.multiselect.css")?>" rel="stylesheet" />	
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.4.min.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery-ui-1.8.9.custom.min.js")?>"></script>
    <script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("registro/js/gen_validatorv31.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/ui.multiselect.js")?>"></script>
		
</head>
<body>
 		<form style="margin:0;padding:0;" id="frmABMCampanias" name="frmABMCampanias" method="post"  action="/<?=$confRoot[1]?>/logica_bo/campanias/actionform/campanias_new.php?page=<?=$_GET['page']?>" enctype="multipart/form-data">
   		<input type="hidden" id="txtIdCampania" name="txtIdCampania" value="<?=($objCampania->getIdCampania()>0)?$objCampania->getIdCampania():0 ?>">
        
		<div id="container">
						<h1>Nueva Campa�a de Ventas</h1>
					    <div class="stats">

			  	<h2>Informaci�n b�sica</h2>
				<p>Aqu� se ingresar� el nombre y la descripcion breve de la campa�a.</p>
			  	<table>
			    <tbody>
					<tr>
						<th width="35%">Nombre</th>
						<td width="65%"><input type="text" id="txtNombre" name="txtNombre" value="<?=$objCampania->getNombre()?>"></td>
					</tr>
					<tr class="alt">
						<th>Descripci�n</th>
						<td><input type="text" id="txtDescripcion" name="txtDescripcion" value="<?=$objCampania->getDescripcion()?>"></td>
					</tr>
					<tr>
						<th>Comercial</th>
						<td><select name="IdComercial" id="IdComercial">
							<option value="X">Seleccione</option>
							<?php
								foreach(dmComerciales::getAll() as $com)
									echo '<option value="'.$com['IdComercial'].'" '.($objCampania->getIdComercial() == $com['IdComercial'] ? 'selected' : '').'>'.$com['Nombre'].'</option>';
							?>
						</select></td>
					</tr>
				</tbody>
				</table>
				
			  	<h2>Fechas</h2>
				<p>Aqu� se ingresar�n las fechas de salida de la campa�a.</p>
			  	<table>
			    <tbody>
					<tr>
						<th width="35%">Fecha de inicio</th>
						<td style="vertical-align:middle;" width="65%">
							<input type="text"  id="FechaInicioDia" name="FechaInicio[txtDia]" value="<?=$oFechaInicio;?>" /> 
							<img style="display:inline; margin:auto 0;" id="FechaInicioDiaImg" src="<?=UrlResolver::getCssBaseUrl("bo/campanias/img/calendario_icono.gif")?>"/>
							&nbsp;&nbsp;
					 		<input type="text" name="FechaInicio[txtHora]" id="FechaInicioHora" maxlength="2" size="1" value="<?=$fiHora;?>"/>
                     		:
                     		<input type="text" name="FechaInicio[txtMin]" id="FechaInicioMin" maxlength="2" size="1" value="<?=$fiMinutos;?>"/>
						</td>
					</tr>
					<tr class="alt">
						<th>Fecha de fin</th>
						<td>
							<input type="text"  id="FechaFinDia" name="FechaFin[txtDia]" value="<?=$oFechaFin;?>"/> 
							<img style="display:inline; margin:auto 0;" id="FechaFinDiaImg" src="<?=UrlResolver::getCssBaseUrl("bo/campanias/img/calendario_icono.gif")?>"/>
							&nbsp;&nbsp;
					 		<input type="text" name="FechaFin[txtHora]" id="FechaFinHora" maxlength="2" size="1" value="<?=$ffHora;?>"/>
                     		:
                     		<input type="text" name="FechaFin[txtMin]" id="FechaFinMin" maxlength="2" size="1" value="<?=$ffMinutos;?>"/>
						</td>
					</tr>
					<tr>
						<th>Visible desde</th>
						<td>
							<input type="text"  id="FechaVisibleDia" name="FechaVisible[txtDia]" value="<?=$oVisibleDesde;?>" /> 
							<img style="display:inline; margin:auto 0;" id="FechaVisibleDiaImg" src="<?=UrlResolver::getCssBaseUrl("bo/campanias/img/calendario_icono.gif")?>"/>
					 		&nbsp;&nbsp;
					 		<input type="text" name="FechaVisible[txtHora]" id="FechaVisibleHora" maxlength="2" size="1" value="<?=$fvHora;?>"/>
                     		:
                     		<input type="text" name="FechaVisible[txtMin]" id="FechaVisibleMin" maxlength="2" size="1" value="<?=$fvMinutos;?>"/>
						</td>
					</tr>		
				</tbody>
				</table>
				
				
			  	<h2>Configuraci�n de Campa�a</h2>
				<p>Aqu� se ingresar�n configuraciones especificas de la campa�a incluyendo: configuraci�n de env�os, pantalla de bienvenida, ordenamiento y cantidad m�xima de venta de los productos.</p>
			  	<table>
			    <tbody>
					<tr>
						<th width="35%">Maximos productos a vender</th>
						<td width="65%"><input type="text" id="txtMaxProd" name="txtMaxProd" maxlength="100" size=10 value="<?=$objCampania->getMaxProd()?>"/></td>
					</tr>
					<tr>
						<th width="35%">Descuento (%)</th>
						<td width="65%"><input type="text" id="txtDescuentoMax" name="txtDescuentoMax" maxlength="100" size=10 value="<?=$objCampania->getDescuentoMax()?>"/></td>
					</tr>
					<tr class="alt">
						<th>Ordenamiento de productos</th>
						<td>
							<select id="cbxOrdenamiento" name="cbxOrdenamiento">
                    		<!--<option value=1>...Seleccionar...</option>-->
                     		<?
                       			$collOrdenamientos = dmOrdenamientos::getAll();
                       			foreach ($collOrdenamientos as $Ordenamiento) {
                           			if ($objCampania->getIdOrdenamiento() == $Ordenamiento["idOrdenamiento"]) {
                            ?>
                            		<option selected value="<?=$Ordenamiento["idOrdenamiento"]?>"><?=$Ordenamiento["Descripcion"]?></option>
                            <?
                           			}
                           			else {
                            ?>
                            		<option value="<?=$Ordenamiento["idOrdenamiento"]?>"><?=$Ordenamiento["Descripcion"]?></option>
                            <?
                           			}
                       			}
                     		?>
                 			</select>
						</td>
					</tr>
					<tr>
						<th>Env�o gratuito</th>
						<td>
							<input type="radio" value="1" id="txtEnvioGratuito1" name="txtEnvioGratuito"  <?=($objCampania->hasPropiedad(6) == true ? 'checked="checked"' : '')?> /> Habilitado
							<input type="radio" value="0" id="txtEnvioGratuito0" name="txtEnvioGratuito"  <?=($objCampania->hasPropiedad(6) == false ? 'checked="checked"' : '')?> /> Deshabilitado<br/>
						</td>
					</tr>
					<tr class="alt">
						<th>Monto m�nimo para env�o gratis</th>
						<td>
							<input type="radio" id="txtMinMontoEnvioGratis1" name="txtMinMontoEnvioGratis" value="1" <?=($objCampania->getMinMontoEnvioGratis() > 0 ? 'checked="checked"' : '')?> />
							Habilitado
							<input type="radio" id="txtMinMontoEnvioGratis0" name="txtMinMontoEnvioGratis" value="0" <?=($objCampania->getMinMontoEnvioGratis() == 0 ? 'checked="checked"' : '')?> />
							Deshabilitado
							<br/>
							<div id="montoEnvioGratis" <?=($objCampania->getMinMontoEnvioGratis() == 0 ? 'style="display:none;"' : '')?> >
							Valor: <input type="text" id="txtMinMontoEnvioGratis" name="txtMinMontoEnvioGratisVal" maxlength="20" size=10 value="<?=$objCampania->getMinMontoEnvioGratis()?>" />
							</div>
						</td>
					</tr>	
					<tr>
						<th>Rango de fechas de despacho</th>
						<td>
						Entre&nbsp; 
						<input type="text" name="TiempoEntregaIn" id="TiempoEntregaIn" value="<?=$oTiempoEntregaIn?>" />
						<img style="display:inline; margin:auto 0;" id="TiempoEntregaInImg" src="<?=UrlResolver::getCssBaseUrl("bo/campanias/img/calendario_icono.gif")?>"/>
						&nbsp;y&nbsp; 
						<input type="text" name="TiempoEntregaFn" id="TiempoEntregaFn" value="<?=$oTiempoEntregaFn?>" />
						<img style="display:inline; margin:auto 0;" id="TiempoEntregaFnImg" src="<?=UrlResolver::getCssBaseUrl("bo/campanias/img/calendario_icono.gif")?>"/>
						</td>
					</tr>	
					<tr>
						<th>Fecha de corte</th>
						<td>
						<input type="text" name="FechaCorte" id="FechaCorte" value="<?=$oFechaCorte?>" />
						<img style="display:inline; margin:auto 0;" id="FechaCorteImg" src="<?=UrlResolver::getCssBaseUrl("bo/campanias/img/calendario_icono.gif")?>"/>
						</td>
					</tr>	
					<tr>
						<th>Segundo rango de fechas de despacho</th>
						<td>
						Entre&nbsp; 
						<input type="text" name="TiempoEntregaIn2" id="TiempoEntregaIn2" value="<?=$oTiempoEntregaIn2?>" />
						<img style="display:inline; margin:auto 0;" id="TiempoEntregaIn2Img" src="<?=UrlResolver::getCssBaseUrl("bo/campanias/img/calendario_icono.gif")?>"/>
						&nbsp;y&nbsp; 
						<input type="text" name="TiempoEntregaFn2" id="TiempoEntregaFn2" value="<?=$oTiempoEntregaFn2?>" />
						<img style="display:inline; margin:auto 0;" id="TiempoEntregaFn2Img" src="<?=UrlResolver::getCssBaseUrl("bo/campanias/img/calendario_icono.gif")?>"/>
						</td>
					</tr>	
					<tr  class="alt">
						<th>Pantalla de Bienvenida</th>
						<td>
	                		<input type="radio" id="welcome1" name="welcome" value="1" <?=($objCampania->getWelcome() == 1 ? 'checked="checked"' : '')?> />
	    	            	Habilitado
	    	            	<input type="radio" id="welcome0" name="welcome" value="0" <?=($objCampania->getWelcome() == 0 ? 'checked="checked"' : '')?> />
	    	            	Deshabilitado
						</td>
					</tr>
				</tbody>
				</table>
		
				<div id="catcampania">
			  	<h2>Categor�as</h2>
				<p>Aqu� se ingresar�n las diferentes categor�as en las que, luego, se ingresaran los productos.</p>
				
				









				
				
				
				<table>
			    <tbody >
<?php 
		$last = null;
		$lsid = 0;
		foreach ($listCategorias as $fila) {
			$id = $fila["IdCategoria"];
    		if ($fila["IdPadre"] == null) { //categoria padre
    			$padre = $fila["IdCategoria"];
    			echo '<input value="'.$padre.'" name="fatherId" id="fatherId" type="hidden" />';
    		}
    		else {
    			if ($fila["IdPadre"] == $padre) {
    				if ($last != null) {
echo '<input type="hidden" value="'.$lsid.'" id="subcat_id'.$last.'"/>';
echo '<tr id="divTxt'.$last.'"><td><a href="javascript:;" onClick="addSubFormField('.$last.'); return false;">Agregar Sub-Categor&iacute;a</a></td></tr>';
    				}
echo '<tr id="row'.$id.'"><td><input type="text" name="txt[c'.$id.']" id="txt'.$id.'" value="'.$fila["Nombre"].'"/>&nbsp;<a href="javascript:;" onClick="removeFormField('.$id.'); return false;">Eliminar Categor&iacute;a</a></td></tr>';
$last = $id;
$lsid = 0;
    			}
    			else {
    				$pid = $fila["IdPadre"];
echo '<tr id="row'.$pid.'sid'.$id.'"><td style="padding-left:35px;" ><input type="text" name="txt[s'.$id.'c'.$pid.']" id="txt'.$pid.'sid'.$id.'" value="'.$fila["Nombre"].'"/>&nbsp;<a href="javascript:;" onClick="removeSubFormField('.$pid.','.$id.'); return false;">Eliminar Sub-Categor&iacute;a</a></td></tr>';
					$lsid = $id-1+2;
    			}
    		}
    	}
    	if ($listCategorias != null) {
echo '<input type="hidden" value="'.$lsid.'" id="subcat_id'.$last.'"/>';
echo '<tr id="divTxt'.$last.'"><td><a href="javascript:;" onClick="addSubFormField('.$last.'); return false;">Agregar Sub-Categor&iacute;a</a></td></tr>';
    	}
    	
?>
<input type="hidden" value="<?=$id?>" id="cat_id"/>
			    
					<tr class="alt" id="divTxt"><td colspan="3"><a href="javascript:;" onClick="addFormField(); return false;">Agregar Categor&iacute;a</a></td></tr>
				</table>		
 				
			  	<h2>Categor&iacute;as globales</h2>
				<p>Aqu� se seleccionaran las categor&iacute;as globales a las que pertenece el producto.</p>
			  	<table>
			    <tbody>
					<tr>
						<th width="35%">Categor&iacute;as seleccionadas</th>
						<td width="65%">
				   <select multiple size="15" style="width:50%;" id="cbxCategoriaGlobal" name="cbxCategoriaGlobal[]">
                        <?
                        
                        
                        
                          foreach ($listCategoriasGlobales as $fila) {
                            	$selected=false;
                          		foreach ($categoriasGlobalesProducto as $catProd) {
                          			if ($catProd["IdCategoriaGlobal"]==$fila["IdCategoriaGlobal"]) {
                          				$selected=true;
                          			}
                          		}
                            	
                                ?><option value="<?=$fila["IdCategoriaGlobal"]?>" <?=$selected?"selected=\"selected\" ":""?>><?=$fila["Nombre"]?></option><?
                          }
                        ?>
                    </select>
                    
                    	</td>
                    </tr>
					
					
				</tbody>
				</table>


				<h2>Im�genes</h2>
				<p>Aqu� se cargar�n las diferentes im�genes de una campa�a.</p>
			  	<table>
			    <tbody>
					<tr id="imgBienvenida"  <?=($objCampania->getWelcome() == 0 ? 'style="display:none;"' : '')?>>
						<th width="35%">Im�gen de bienvenida</th>
						<td width="65%"><input type="file" id="txtWelcome" name="txtWelcome"/></td>
					</tr>
					<tr class="alt">
						<th width="35%">Logo de campa�a</th>
						<td width="65%"><input type="file" id="txtLogo" name="txtLogo"/></td>
					</tr>
					<tr>
						<th width="35%">Im�gen principal de campa�a</th>
						<td width="65%"><input type="file" id="txtImagen" name="txtImagen"/></td>
					</tr>	
					<tr class="alt">
						<th width="35%">Im�gen principal de campa�a gris (opcional)</th>
						<td width="65%"><input type="file" id="txtGreyImagen" name="txtGreyImagen"/></td>
					</tr>
					<tr>
						<th width="35%">Im�gen principal de vidriera</th>
						<td width="65%"><input type="file" id="txtVidrieraImagen" name="txtVidrieraImagen"/></td>
					</tr>
					<tr class="alt">
						<th width="35%">Im�gen principal de vidriera off</th>
						<td width="65%"><input type="file" id="txtVidrieraOffImagen" name="txtVidrieraOffImagen"/></td>
					</tr>
					
					<tr class="alt">
						<th width="35%">Im�genes cargadas:</th>
						<td width="65%">
						<?
							if (count($objCampania->getFiles()) > 0) {
                        		$i=0;
                        		foreach($objCampania->getFiles() as $file) {
                        ?>
        				<?=$file["nombre"]?> <a id="motivoChangeLink" style="font-size:10px;color:red;" href="javascript:;" onClick="window.open('<?=Aplicacion::getRootUrl();?>front/campanias/archivos/campania_<?=$objCampania->getIdCampania();?>/imagenes/<?=$file["nombre"]?>')">[ver im&aacute;gen]</a><br/>
                        <?
                            		$i++;
                        		}
                    		} 
                    		else {
                        ?>
                        	No hay im�genes cargadas
                        <?
                    		}
                		?>
						</td>
					</tr>
				</tbody>
				</table>
				
				
				<div id="frmABMCampanias_errorloc" class="error_strings" ></div>
				
            	
                <table style="border:0;">
                	<tbody><tr style="border:0;">
                		<td style="border:0;"></td>
                		<td style="border:0;text-align:right;"><input type="submit" value="Guardar y Cargar Productos" id="botonResponderYSiguiente" name="botonResponderYSiguiente"></td>
                	</tr>
                	<tr style="border:0;">
                		<td style="border:0;"></td>
                		<td style="border:0;text-align:right;"><input type="submit" value="Guardar y Salir" id="botonGuardarYSalir" name="botonGuardarYSalir"></td>
                	</tr>
                </tbody></table>
            

		
				</div>
				
		</div>
		</form>
<script type="text/javascript">
$(function() {
	$("#FechaInicioDia").datepicker({dateFormat: 'dd/mm/yy'});
	$("#FechaFinDia").datepicker({dateFormat: 'dd/mm/yy'});
	$("#FechaVisibleDia").datepicker({dateFormat: 'dd/mm/yy'});

	$("#FechaInicioDiaImg").click(function() {
		$("#FechaInicioDia").datepicker("show");
	});
	
	$("#FechaFinDiaImg").click(function() {
		$("#FechaFinDia").datepicker("show");
	});
	
	$("#FechaVisibleDiaImg").click(function() {
		$("#FechaVisibleDia").datepicker("show");
	});


	$("#TiempoEntregaIn").datepicker({dateFormat: 'dd/mm/yy'});
	$("#TiempoEntregaFn").datepicker({dateFormat: 'dd/mm/yy'});
	$("#TiempoEntregaIn2").datepicker({dateFormat: 'dd/mm/yy'});
	$("#TiempoEntregaFn2").datepicker({dateFormat: 'dd/mm/yy'});
	$("#FechaCorte").datepicker({dateFormat: 'dd/mm/yy'});
	
	$("#TiempoEntregaInImg").click(function() {
		$("#TiempoEntregaIn").datepicker("show");
	});
	
	$("#TiempoEntregaFnImg").click(function() {
		$("#TiempoEntregaFn").datepicker("show");
	});

	$("#TiempoEntregaIn2Img").click(function() {
		$("#TiempoEntregaIn2").datepicker("show");
	});
	
	$("#TiempoEntregaFn2Img").click(function() {
		$("#TiempoEntregaFn2").datepicker("show");
	});

	$("#FechaCorteImg").click(function() {
		$("#FechaCorte").datepicker("show");
	});

	$("#txtMinMontoEnvioGratis1").click(function() {
		$("#montoEnvioGratis").show();
	});

	$("#txtMinMontoEnvioGratis0").click(function() {
		$("#montoEnvioGratis").hide();
	});


	$("#welcome1").click(function() {
		$("#imgBienvenida").show();
	});

	$("#welcome0").click(function() {
		$("#imgBienvenida").hide();
	});
	
});
</script>

<!-- Geelbe Validation Form -->
<script type="text/javascript">
var formValidator  = new Validator("frmABMCampanias");
formValidator.EnableOnPageErrorDisplaySingleBox();
formValidator.EnableMsgsTogether();
formValidator.EnableFocusOnError(false);

formValidator.addValidation("txtNombre","req","Por favor, ingrese un Nombre de Campa&ntilde;a de Venta.");
formValidator.addValidation("txtNombre","maxlen=255", "El Nombre supera el maximo permitido de 255 caracteres.");

formValidator.addValidation("txtDescripcion","req","Por favor, ingrese una Descripci&oacute;n.");
formValidator.addValidation("txtDescripcion","maxlen=255","Su Descripci&oacute;n supera el maximo permitido de 255 caracteres.");

formValidator.addValidation("IdComercial","req","Por favor, seleccione un Comercial.");
formValidator.addValidation("IdComercial","numeric","Por favor, seleccione un Comercial.");


formValidator.addValidation("FechaInicioDia","req","Por favor, ingrese una Fecha de Inicio.");
formValidator.addValidation("FechaFinDia","req","Por favor, ingrese una Fecha de Fin.");
formValidator.addValidation("FechaVisibleDia","req","Por favor, ingrese una Fecha de Visualizaci&oacute;n.");

formValidator.addValidation("FechaInicioHora","req","Por favor, ingrese una Hora de Inicio.");
formValidator.addValidation("FechaInicioHora","numeric","Por favor, ingrese una Hora de Inicio.");
formValidator.addValidation("FechaInicioMin","req","Por favor, ingrese los Minutos de Inicio.");
formValidator.addValidation("FechaInicioMin","numeric","Por favor, ingrese los Minutos de Inicio.");

formValidator.addValidation("FechaFinHora","req","Por favor, ingrese una Hora de Fin.");
formValidator.addValidation("FechaFinHora","numeric","Por favor, ingrese una Hora de Fin.");
formValidator.addValidation("FechaFinMin","req","Por favor, ingrese una Minutos de Fin.");
formValidator.addValidation("FechaFinMin","numeric","Por favor, ingrese una Minutos de Fin.");

formValidator.addValidation("FechaVisibleHora","req","Por favor, ingrese una Hora de Visualizaci&oacute;n.");
formValidator.addValidation("FechaVisibleHora","numeric","Por favor, ingrese una Hora de Visualizaci&oacute;n.");
formValidator.addValidation("FechaVisibleMin","req","Por favor, ingrese una Minutos de Visualizaci&oacute;n.");
formValidator.addValidation("FechaVisibleMin","numeric","Por favor, ingrese una Minutos de Visualizaci&oacute;n.");

formValidator.addValidation("txtMaxProd","req","Por favor, ingrese M&aacute;ximos Productos.");
formValidator.addValidation("txtMaxProd","numeric","Por favor, ingrese  M&aacute;ximos Productos.");

formValidator.addValidation("TiempoEntregaIn","req","Por favor, ingrese Tiempo de Entrega Inicial.");
formValidator.addValidation("TiempoEntregaFn","req","Por favor, ingrese Tiempo de Entrega Final.");


</script>
<!-- Geelbe Validation Form -->

<script type="text/javascript">
<?php if ($listCategorias == null) { ?>
addFormField();
<?php } ?>

function addFormField() {
	var id = $("#cat_id").val();
	id = (id-1)+2;
	$("#divTxt").before('<input type="hidden" value="0" id="subcat_id'+id+'"/>');
	$("#divTxt").before('<tr id="row'+id+'"><td><input type="text" name="txt[nc'+id+']" id="txt'+id+'"/>&nbsp;<a href="javascript:;" onClick="removeFormField('+id+'); return false;">Eliminar Categor&iacute;a</a></td></tr>');
	$("#divTxt").before('<tr id="divTxt'+id+'"><td><a href="javascript:;" onClick="addSubFormField('+id+'); return false;">Agregar Sub-Categor&iacute;a</a></td></tr>');
	
	$("#cat_id").val(id);
}

function addSubFormField(id) {
	var name = "#subcat_id"+id;
	var sid = $(name).val();

	$("#divTxt"+id).before('<tr id="row'+id+'sid'+sid+'"><td style="padding-left:35px;" ><input type="text" name="txt[ns'+sid+'c'+id+']" id="txt'+id+'sid'+sid+'"/>&nbsp;<a href="javascript:;" onClick="removeSubFormField('+id+','+sid+'); return false;">Eliminar Sub-Categor&iacute;a</a></td></tr>');

	sid = (sid-1)+2;

	$(name).val(sid);
}


function removeFormField(id) {
	var name = "#subcat_id"+id;
	var sid = $(name).val();
	var n = "";

	var answer = confirm("Quieres eliminar la Categor�a '"+$("#txt"+id).val()+"' y sus Sub-Categor�as?");
	if (answer){
		for (l=0; l<=sid; l=l+1) {
			$("#row"+id+"sid"+l).remove();
		}
		$("#divTxt"+id).remove();
		$("#row"+id).remove();
	}
}


function removeSubFormField(id, sid) {
	var answer = confirm("Quieres eliminar la Sub-Categor�a '"+$("#txt"+id+"sid"+sid).val()+"'?");
	if (answer){
		$("#row"+id+"sid"+sid).remove();
	}
}
</script>



<script>
$("#catcampaniatbody").html();

</script>
<script type="text/javascript">
$(function(){
  $("#cbxCategoriaGlobal").multiselect({sortable: false, searchable: false});
});
</script>
		
</body>
</html>