function ErrorMSJ(code, def)
{
    if(arrErrores["CLASES"][code] != undefined)
        document.getElementById("msjError").innerHTML = arrErrores["CLASES"][code];
    else
        document.getElementById("msjError").innerHTML = code;
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtNombre.value.length == 0)
        {
            arrError.push(arrErrores["CLASES"]["NOMBRE"]);
        }
        if(document.getElementById("tbl_atributos").rows.length <= 1)
        {
            //arrError.push(arrErrores["CLASES"]["ATRIBUTOS_0"]);
        }
        if(document.getElementById("tbl_impuestos").rows.length <= 1)
        {
            arrError.push(arrErrores["CLASES"]["IMPUESTOS_0"]);
        }
        if (arrError.length>0)
        {
            var error = ""
            for(i=0; i<arrError.length; i++)
            {
                error +="*" + arrError[i] + "\n";
            }
            alert(error);
            return false;
        }
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}
function AgregarAtributo()
{
    try
    {
        var Atributo = document.getElementById("txtNombreAtributo").value;
        if(Atributo.length == 0)
        {
            alert(arrErrores["CLASES"]["PARAMETRO_NOMBRE"]);
            return false;
        }
        if(tbl_atributos_Buscar(Atributo))
        {
            alert(arrErrores["CLASES"]["PARAMETRO_NOMBRE_EXISTE"]);
            return false;
        }
        tbl_atributos_AgregarFila(0, Atributo);
        document.getElementById("txtNombreAtributo").value="";
        document.getElementById("txtNombreAtributo").focus();
    }
    catch(e)
    {
        throw e;
    }
}
function quitarAtributo()
{
    try
    {
        var Tabla = document.getElementById("tbl_atributos");
        if(confirm(arrErrores["CLASES"]["ELIMINAR_ATRIBUTO"]))
        {
            Tabla.deleteRow(tbl_atributos_getFIlaSeleccionada());
        }
    }
    catch(e)
    {
        throw e;
    }               
}
function AgregarImpuesto()
{
    try
    {
        var IdImpuesto = document.getElementById("cbxImpuesto").value;
        if(IdImpuesto == 0)
        {
            alert(arrErrores["CLASES"]["SELECCIONE_IMPUESTO"]);
            return false;
        }
        if(tbl_impuestos_Buscar(IdImpuesto))
        {
            alert(arrErrores["CLASES"]["IMPUESTO_EXISTE"]);
            return false;
        }
        var Impuesto = document.getElementById("cbxImpuesto").options[document.getElementById("cbxImpuesto").selectedIndex].innerHTML;
        tbl_impuestos_AgregarFila(IdImpuesto, Impuesto);
        document.getElementById("cbxImpuesto").value=0;
        document.getElementById("cbxImpuesto").focus();
    }
    catch(e)
    {
        throw e;
    }
}
function quitarImpuesto()
{
    try
    {
        var Tabla = document.getElementById("tbl_impuestos");
        if(confirm(arrErrores["CLASES"]["ELIMINAR_ATRIBUTO"]))
        {
            Tabla.deleteRow(tbl_impuestos_getFIlaSeleccionada());
        }
    }
    catch(e)
    {
        throw e;
    }               
}


function irVolver()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"clases_de_productos/index.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CLASES"]["SELECCIONE"]);
        else
            window.location.href="ABM.php?IdClase="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVer()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CLASES"]["SELECCIONE"]);
        else
            window.location.href="Ver.php?IdClase="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar(id)
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["CLASES"]["SELECCIONE"]);
        else
        {
            if(confirm(arrErrores["CLASES"]["ELIMINAR"]))
                window.location.href="../../logica_bo/clases_de_productos/actionform/eliminar_clases_de_productos.php?IdClase="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}