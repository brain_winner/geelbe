<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clases_de_productos"));
    $postURL = Aplicacion::getIncludes("post", "clases_de_productos");
    try
    {
        ValidarUsuarioLogueadoBo(3);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $objClase = dmClases::getByIdClase((isset($_GET["IdClase"])) ? $_GET["IdClase"]:0);
    $objDTAtributos = new TablaDinamica("tbl_atributos");
    $objDTAtributos->addColumna(0,"IdAtributo", false);
    $objDTAtributos->addColumna(1,"Atributo");
    $objDTAtributos->addBotones(array("nombre"=>"aquitar", "tipo"=>"a", "title"=>"quitar", "innerHTML"=>"[x]", "funcion"=>"quitarAtributo"));
    
    $objDTImpuestos = new TablaDinamica("tbl_impuestos");
    $objDTImpuestos->addColumna(0,"IdImpuesto", false);
    $objDTImpuestos->addColumna(1,"Impuesto");
    $objDTImpuestos->addBotones(array("nombre"=>"aquitar", "tipo"=>"a", "title"=>"quitar", "innerHTML"=>"[x]", "funcion"=>"quitarImpuesto"));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Marcas</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/clases_de_productos/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMMarcas" name="frmABMMarcas" method="POST" onSubmit="ValidarForm(this);">
        <input type="hidden" id="txtIdClase" name="txtIdClase" value="<?=$objClase->getIdClase()?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["clases_de_productos"]);
            ?>
            <tr>
                <td>Nombre</td>
                <td><input type="text" id="txtNombre" name="txtNombre" maxlength="40" value="<?=$objClase->getNombre()?>"></td>
            </tr>
            <tr>
                <td>Habilitado</td>
                <td>
                    <input type="checkbox" id="chkHabilitado" name="chkHabilitado" value="OK">
                    <script>
                        <?
                            if($objClase->getHabilitado() == 1)
                            {
                                ?>document.getElementById("chkHabilitado").checked=true;<?
                            }
                        ?>
                    </script>
                </td>
            </tr>
            <tr>
                <td colspan="2">Impuestos</td>
            </tr>
            <tr>
                <td>Impuestos</td>
                <td>
                    <select id="cbxImpuesto" name="cbxImpuesto">
                        <option>Ninguno</option>
                        <?
                            foreach(dmImpuestos::getImpuestos(1,"ASC") as $impuesto)
                            {
                                ?><option value="<?=$impuesto["Codigo"]?>"><?=$impuesto["Impuesto"]?></option><?
                            }
                        ?>
                    </select>
                    &nbsp;
                    <input type="button" id="btnAgregarImpuesto" name="btnAgregarImpuesto" value="Agregar" onclick="AgregarImpuesto();"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?
                    $objDTImpuestos->show();
                ?>
                </td>
            </tr>
            
            <tr>
                <td colspan="2">Atributos</td>
            </tr>
            <tr>
                <td>Atributos</td>
                <td>
                    <input type="text" maxlength="16" size=16 id="txtNombreAtributo" name="txtNombreAtributo" />
                    &nbsp;
                    <input type="button" id="btnAgregarParametro" name="btnAgregarParametro" value="Agregar" onclick="AgregarAtributo();"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?
                    $objDTAtributos->show();
                ?>
                </td>
            </tr>
            <script>
            <?
              foreach($objClase->getParametros() as $parametro)
              {
                foreach($parametro->getAtributos() as $Atributo)
                {
                    ?>
                        tbl_atributos_AgregarFila(<?=$Atributo->getIdAtributo()?>,"<?=$Atributo->getNombre()?>");
                    <?
                }
              }
              foreach($objClase->getImpuestos() as $impuesto)
              {
                  $objImpuesto = dmImpuestos::getByIdImpuesto($impuesto->getIdImpuesto());
                  ?>
                    tbl_impuestos_AgregarFila(<?=$objImpuesto->getIdImpuesto()?>, "<?=$objImpuesto->getNombre()?>");
                <?
              }
            ?>
            </script>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo//imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo//imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
