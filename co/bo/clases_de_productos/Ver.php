<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clases_de_productos"));
    try
    {
        ValidarUsuarioLogueadoBo(3);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $objClase = dmClases::getByIdClase((isset($_GET["IdClase"])) ? $_GET["IdClase"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Clases</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/clases_de_productos/js.js");?>" type="text/javascript"></script>
</head>
<body>
        <input type="hidden" id="txtIdClase" name="txtIdClase" value="<?=$objClase->getIdClase()?>">
        <table>
            <tr>
                <td>Nombre</td>
                <td><b><?=$objClase->getNombre()?></b></td>
            </tr>
            <tr>
                <td>Descripci&oacute;n</td>
                <td><b><?=$objClase->getDescripcion()?></b></td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnVolver','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver.gif");?>" alt="Volver" name="btnVolver" id="btnVolver" border="0"/></a>
                </td>
            </tr>
        </table>
</body>
</html>
