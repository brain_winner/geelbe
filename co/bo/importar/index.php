<?php
	if(!isset($_GET['IdCampania']))
		die('Parámetro inválido: IdCampania');

	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(12);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
	try {
	    if(isset($_POST['import']) && is_uploaded_file($_FILES['csv']['tmp_name'])) {
	    	$resultados = dmProductos::ImportarCSV($_FILES['csv']['tmp_name']);
	    	header("Location: index.php?IdCampania=".$_GET['IdCampania']);
	    	die;
	    }
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		echo $e;
		exit;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Importar archivo Envio</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
    
		<? Includes::Scripts(); ?>
		<script src="<?=UrlResolver::getJsBaseUrl("bo/pedidos/js.js");?>" type="text/javascript"></script>

	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Importar archivo de Productos</h1>	
			<p><a href="ejemplo.csv">Descargar CSV ejemplo</a></p>

			<?php if(isset($_SESSION['resultados']) && count($_SESSION['resultados']) > 0): ?>
			<div style="margin: 20px">
				<p>Se guardaron los siguientes productos:</p>
				<ul>
					<?php foreach($_SESSION['resultados'] as $nombre => $id): ?>
					<li><a href="/<?=$confRoot[1]?>/bo/productos/ABM.php?IdProducto=<?=$id?>" target="_blank"><?=$nombre?></a></li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php
				unset($_SESSION['resultados']);	
				endif;
			?>

			<?php if(isset($_SESSION['resultadosErrorClase']) && count($_SESSION['resultadosErrorClase']) > 0): ?>
			<div style="margin: 20px">
				<p>Los siguientes productos no se guardaron porque la clase especificada no existe o no tiene atributos cargados:</p>
				<ul>
					<?php foreach($_SESSION['resultadosErrorClase'] as $nombre => $id): ?>
					<li><?=$nombre?> (clase "<?=$id?>")</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php
				unset($_SESSION['resultadosErrorClase']);	
				endif;
			?>

			<?php if(isset($_SESSION['resultadosErrorCategoria']) && count($_SESSION['resultadosErrorCategoria']) > 0): ?>
			<div style="margin: 20px">
				<p>Los siguientes productos no se guardaron porque la categoría especificada no existe:</p>
				<ul>
					<?php foreach($_SESSION['resultadosErrorCategoria'] as $nombre => $id): ?>
					<li><?=$nombre?> (categoría "<?=$id?>")</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php
				unset($_SESSION['resultadosErrorCategoria']);	
				endif;
			?>

		<form action="index.php?IdCampania=<?=$_GET['IdCampania']?>" method="post" enctype="multipart/form-data" style="margin: 20px">
			
			<label for="csv" style="display:block">Selecciona el archivo CSV:</label>
			<input type="file" name="csv" id="csv" />
			
			<input type="submit" name="import" value="Importar" />
			
		</form>
		
	</body>
</html>
