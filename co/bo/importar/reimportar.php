<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(12);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
	try {
	    if(isset($_POST['import']) && is_uploaded_file($_FILES['csv']['tmp_name'])) {
	    	dmProductos::ReimportarCSV(file_get_contents($_FILES['csv']['tmp_name']));
	    	header("Location: index.php");
	    	die;
	    }
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		echo $e;
		exit;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Importar archivo Envio</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
    
		<? Includes::Scripts(); ?>
		<script src="<?=UrlResolver::getJsBaseUrl("bo/pedidos/js.js");?>" type="text/javascript"></script>

	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Reimportar productos</h1>	
			<p><a href="reimportar.csv">Descargar CSV ejemplo</a></p>

		<form action="reimportar.php" method="post" enctype="multipart/form-data" style="margin: 20px">
			
			<label for="csv" style="display:block">Selecciona el archivo CSV:</label>
			<input type="file" name="csv" id="csv" />
			
			<input type="submit" name="import" value="Importar" />
			
		</form>
		
	</body>
</html>
