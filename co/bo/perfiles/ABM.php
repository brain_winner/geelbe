<?  
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("perfiles"));
    try
    {
        ValidarUsuarioLogueadoBo(6);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "perfiles");
    $oPerfil = dmPerfiles::getById((isset($_GET["IdPerfil"])) ? $_GET["IdPerfil"]:0);
 
// para listar secciones
	$secciones=$_SESSION['BO']['User']['secciones'];
	$access = array();
	foreach($secciones as $secc)
		$access[] = $secc['idSeccion'];
		
	$oConexion = Conexion::nuevo();
	$oConexion->setQuery("SELECT s.*, sp.seccion as padre FROM secciones s, seccionesPadre sp WHERE sp.idSeccionPadre = s.idSeccionPadre ORDER BY s.idSeccionPadre, s.idSeccion");                
	$secciones = $oConexion->DevolverQuery();
	$oConexion->Cerrar();
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Perfiles</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/perfiles/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMLocales" name="frmABMLocales" method="post" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="idPerfil" name="idPerfil" value="<?=$oPerfil->getIdPerfil()?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["perfiles"]);
            ?>
            <tr>
                <td>Nombre del Perfil</td>
                <td><input type="text" id="txtDescripcion" name="txtDescripcion" maxlength="80" size=60 value="<?=$oPerfil->getDescripcion()?>"></td>
            </tr>
            <tr>
            <td colspan="2">
            
	           	<table width="100%">
				<?php
                
                    $padre = null;
                    foreach($secciones as $i => $s) {
                                            
                        if($padre == null || $padre != $s['idSeccionPadre']) {
        
                                echo '
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="td_principal">'. $s['padre'] .'</td>
                                </tr>
                                ';
                        
                            $padre = $s['idSeccionPadre'];
                        }
                        
                        echo '<tr>
                                <td class="td_contenido">
									<input type="checkbox" name="chkSecciones[]" id="'. $s['idSeccion'] .'" value="'. $s['idSeccion'] .'" /> '. $s['seccion'] .'
								</td>
                            </tr>';   		
                        
                    }
                
                ?>
                </table>
            
            </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
   <?
   if($oPerfil->getIdPerfil()!=0)
    {
        $SeccionesPerfil = dmSecciones_x_perfiles::getByIdPerfil($oPerfil->getIdPerfil());
        foreach($SeccionesPerfil as $sp)
        {
            ?>    
                <script>
                    id = "<?=$sp['idSeccion']?>";
                 //   alert(id);
                    document.getElementById(id).checked = true;
                </script>
            <?
        }
    } ?>
</body>
</html>
