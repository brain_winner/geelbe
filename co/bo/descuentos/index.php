<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/clases/clsdescuentos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/datamappers/dmdescuentos.php");
    try
    {
        ValidarUsuarioLogueadoBo(4);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
    
    $postURL = Aplicacion::getIncludes("post", "descuentos");
    $dtLocales = dmDescuentos::getAll((isset($_GET["columna"])?$_GET["columna"]:0)+1, (isset($_GET["sentido"])?$_GET["sentido"]:"DESC"), (isset($_GET["q"])?$_GET["q"]:""));
    $objDT = new Tabla($dtLocales, (isset($_GET["tp"])?$_GET["tp"]:10));
    $objDT->setPalabraClave($_GET["q"]);
    $objDT->setPagina("index.php");
    $objDT->setPaginaNro((isset($_GET["irPagina"])?$_GET["irPagina"]:0));
    $objDT->setColumnaNro((isset($_GET["columna"])?$_GET["columna"]:0));
    $objDT->setColumnaSentido((isset($_GET["sentido"])?$_GET["sentido"]:"DESC"));
    $objDT->addBotones('<a href="javascript:downloadPedidos();" ><img src="'.UrlResolver::getImgBaseUrl("bo/imagenes/btnDownload.gif").'" alt="Editar" name="btnPedido" id="btnPedido" border="0"/></a>');
    $objDT->addBotones('<a href="javascript:irPedidos();" ><img src="'.UrlResolver::getImgBaseUrl("bo/imagenes/btnPedidoMini.gif").'" alt="Editar" name="btnPedido" id="btnPedido" border="0"/></a>');
    $objDT->addBotones('<a href="javascript:irEditar();" ><img src="'.UrlResolver::getImgBaseUrl("bo/imagenes/btnEditarMini.gif").'" alt="Editar" name="btnEditar" id="btnEditar" border="0"/></a>');
    $objDT->addBotones('<a href="javascript:irEliminar();" ><img src="'.UrlResolver::getImgBaseUrl("bo/imagenes/btnEliminarMini.gif").'" alt="Eliminar" name="btnEliminar" id="btnEliminar" border="0"/></a>&nbsp;');

    //$objDT->setPropiedadesColumna(0, array("style=\"display:none\""));
    //$objDT->setPropiedadesColumna(2, array("style=\"display:none\""));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> Descuentos</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/descuentos/js.js");?>" type="text/javascript"></script>
    <link href="<?=UrlResolver::getCssBaseUrl("bo/style/css.css")?>" rel="stylesheet" type="text/css" />
</head>
<body>
<table style="width:100%; height:100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="borde_top">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="borde_bottom ">
            <a href="javascript:irNuevo();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnNuevo','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnNuevo_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnNuevo.gif");?>" alt="Nuevo" name="btnNuevo" id="btnNuevo" border="0"/></a>          
        </td>
    </tr>
    <tr>
        <td colspan="100%">
            <?
            $objDT->show();
            ?>
        </td>
    </tr>
</table>

<form action="index.php" method="post">
	<br />
	<h1>Campa&ntilde;as sin descuentos</h1>
	<br />
	<?php
	
		if(isset($_POST['camp']) && is_array($_POST['camp'])) {
			$oConexion = Conexion::nuevo();  		
			$oConexion->Abrir();
            $oConexion->setQuery("UPDATE campanias SET descuento = 1");
            $oConexion->EjecutarQuery();
            $oConexion->Cerrar();
			$oConexion->Abrir();
            $oConexion->setQuery("UPDATE campanias SET descuento = 0 WHERE IdCampania IN (".implode(',', $_POST['camp']).")");
            $oConexion->EjecutarQuery();
            $oConexion->Cerrar();
            
            echo '<p style="color:green;font-weight:bold;margin-bottom:20px">Campa&ntilde;as actualizadas.</p>';
		}
	
		$camp = dmCampania::getCampaniasAx();
		foreach($camp as $c):
	?>
	<label>
		<input type="checkbox" name="camp[]" value="<?=$c['IdCampania']?>" <?=($c['Descuento'] == 0 ? 'checked' : '')?> />
		<?=$c['Nombre']?>
	</label><br />
	<?php endforeach; ?>
	<br />
	<input type="submit" name="send" value="Guardar" />
</form>

</body>
</html>
