function ValidarForm(frm)
{
    if (frm.txtNombre.length == 0)
    {
        return false;                                                                  
    }
    frm.submit();
}
function irVolver()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"descuentos/index.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar()
{
	try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="../../logica_bo/descuentos/actionform/eliminar_descuento.php?IdDescuento="+dt_getCeldaSeleccionada(0);
        
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="ABM.php?IdDescuento="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irPedidos()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="../pedidos/index.php?filterButton=Filtrar&filter%5BP.IdDescuento%5D="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function downloadPedidos()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="../../logica_bo/pedidos/actionform/descuentoscsv.php?IdDescuento="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}

