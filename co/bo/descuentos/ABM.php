<? 
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/clases/clsdescuentos.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/datamappers/dmdescuentos.php");
   try
    {
        ValidarUsuarioLogueadoBo(4);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    //$postURL = Aplicacion::getIncludes("post", "banners");
    $postURL = array('descuentos' => 'logica_bo/descuentos/actionform/descuentos.php');
    $oLocal = dmDescuentos::getById((isset($_GET["IdDescuento"])) ? $_GET["IdDescuento"]:0);
    if(!is_object($oLocal))
    	$oLocal = new descuentos;
    	
    $camps = dmDescuentos::getCampanias($_GET['IdDescuento']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Descuentos</title>
    <?
        Includes::Scripts();
    ?>
    <link href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" type="text/css" rel="stylesheet" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/descuentos/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMLocales" name="frmABMLocales" method="post" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="idDescuento" name="idDescuento" value="<?=$oLocal->getIdDescuento()?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["descuentos"]);
            ?>
            <tr>
                <td>Descripci&oacute;n</td>
                <td><input type="text" id="txtDescripcion" name="txtDescripcion" maxlength="80" size=60 value="<?=$oLocal->getDescripcion()?>" /></td>
            </tr>
            <tr>
                <td>C&oacute;digo</td>
                <td><input type="text" id="txtCodigo" name="txtCodigo" maxlength="80" size=60 value="<?=$oLocal->getCodigo()?>" /></td>
            </tr>
            <tr>
                <td>Descuento</td>
                <td><input type="text" id="txtDescuento" name="txtDescuento" maxlength="80" size=60 value="<?=$oLocal->getDescuento()?>" /></td>
            </tr>
            <tr>
                <td>Tipo</td>
                <td><select id="txtPorcentaje" name="txtPorcentaje">
                	<option value="0" <?=($oLocal->getPorcentaje() == 0 ? 'selected="selected"' : '');?>>Monto fijo</option>
                	<option value="1" <?=($oLocal->getPorcentaje() == 1 ? 'selected="selected"' : '');?>>Porcentaje</option>
                </select></td>
            </tr>
            <tr>
                <td>Fecha de inicio</td>
                <td><input type="text" id="txtInicioFecha" name="txtInicioFecha" maxlength="80" size=60 value="<?=$oLocal->getInicioFecha()?>" class="date" /></td>
            </tr>
            <tr>
                <td>Hora de inicio</td>
                <td><input type="text" id="txtInicioHora" name="txtInicioHora" maxlength="80" size=60 value="<?=$oLocal->getInicioHora()?>" /></td>
            </tr>            
            <tr>
                <td></td>
                <td><input type="checkbox" id="txtEnvioGratis" name="txtEnvioGratis" value="1" <?=($oLocal->getEnvioGratis() ? 'checked' : '')?> /> Envio gratis</td>
            </tr>
            <tr>
            	<td></td>
            	<td><input type="checkbox" id="txtUnico" name="txtUnico" value="1" <?=($oLocal->getUnico() ? 'checked' : '')?> /> Uno por persona</td>
            </tr>
            <tr>
                <td>Fecha de vencimiento</td>
                <td><input type="text" id="txtVencimiento" name="txtVencimiento" maxlength="80" size=60 value="<?=$oLocal->getVencimiento()?>" class="date" /></td>
            </tr>
            <tr>
                <td>Hora de vencimiento</td>
                <td><input type="text" id="txtVencimientoHora" name="txtVencimientoHora" maxlength="80" size=60 value="<?=$oLocal->getVencimientoHora()?>" /></td>
            </tr>            
            <tr>
            	<td>Campanias excluídas</td>
            	<td>
					<?php
						$camp = dmCampania::getCampaniasAx();
						foreach($camp as $c):
					?>
					<label>
						<input type="checkbox" name="camp[]" value="<?=$c['IdCampania']?>" <?=(in_array($c['IdCampania'], $camps) ? 'checked' : '')?>/>
						<?=$c['Nombre']?>
					</label><br />
					<?php endforeach; ?>
            	</td>
            </tr>
          </table>
          <script type="text/javascript">
          	$('.date').datepicker({
          		dateFormat: "yy-mm-dd"
          	})
          </script>
          

			<table>
            
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].submit();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>