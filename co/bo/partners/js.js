function ValidarForm(frm)
{
    if (frm.txtNombre.length == 0)
    {
        return false;                                                                  
    }
    frm.submit();
}
function irVolver()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"partners/index.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar()
{
	try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="../../logica_bo/partners/actionform/eliminar_partner.php?IdPartner="+dt_getCeldaSeleccionada(0);
        
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="ABM.php?IdPartner="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}