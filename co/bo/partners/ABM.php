<?  
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("partners"));
    try
    {
        ValidarUsuarioLogueadoBo(4);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "partners");
    $oLocal = dmPartners::getById((isset($_GET["IdPartner"])) ? $_GET["IdPartner"]:0);

	if(isset($_GET['IdPartner'])) {
	
		//Usuario
		$c = Conexion::nuevo();
		$c->Abrir_Trans();
		$c->setQuery("SELECT * FROM partners_users WHERE IdPartner = ".$_GET['IdPartner']);
		$datos = $c->DevolverQuery();
		$c->Cerrar_Trans();
		
		$nombreusuario = $datos[0]['Username'];
		$nombre = $datos[0]['Nombre'];
		$apellido = $datos[0]['Apellido'];
		
	} else {
		$nombreusuario = '';
		$nombew = '';
		$apellido = '';
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Partners</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/partners/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMLocales" name="frmABMLocales" method="post" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="idPartner" name="idPartner" value="<?=$oLocal->getIdPartner()?>">
        <input type="hidden" id="txtNombreU" name="txtNombreU" value="<?=$nombreusuario?>">
        <input type="hidden" id="txtPassword" name="txtPassword" value="******">
        <input type="hidden" id="txtNombre2" name="txtNombre2" value="<?=$nombre?>">
        <input type="hidden" id="txtApellido" name="txtApellido" value="<?=$apellido?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["partners"]);
            ?>
            <tr>
                <td>Nombre</td>
                <td><input type="text" id="txtNombre" name="txtNombre" maxlength="80" size=60 value="<?=$oLocal->getNombre()?>"></td>
            </tr>
            <tr>
                <td>P&iacute;xeles Registro</td>
                 <td><textarea id="txtPixelesRegistro" name="txtPixelesRegistro" cols="40" rows="5"><?=$oLocal->getPixelesRegistro()?></textarea> </td>
            </tr>
            <tr>
                <td>P&iacute;xeles Activaci&oacute;n</td>
                 <td><textarea id="txtPixelesActivacion" name="txtPixelesActivacion" cols="40" rows="5"><?=$oLocal->getPixelesActivacion()?></textarea> </td>
            </tr>
            
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>