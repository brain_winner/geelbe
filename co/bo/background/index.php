<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
      
    try
    {
        ValidarUsuarioLogueadoBo(46);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
    
	include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/datamappers/dmbackground.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Backgrounds</title>
   <link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
</head>
<body>
<?
//TODO: mejorar; poner todo en una misma query.
if($_POST){
	dmBackground::Insert($_POST["dayhour"], "day", $_POST["textColorDay"]); 
	dmBackground::Insert($_POST["nighthour"], "night", $_POST["textColorNight"]); 
	dmBackground::Background_save_img("imgday", "day", $confRoot[1]); 
	dmBackground::Background_save_img("imgnight", "night", $confRoot[1]); 
}
$dayhour  = dmBackground::getBackground_ById("day"); 
$nighthour = dmBackground::getBackground_ById("night"); 
$textColorDay  = dmBackground::getTextColor_ById("day"); 
$textColorNight = dmBackground::getTextColor_ById("night"); 

?>
<div id="container" class="listadocontainer">
	<h1>Backgrounds</h1>
	<form id="Backgrounds" name="frmBackgrounds" method="POST" enctype="multipart/form-data">
		<label>Imagen de Dia: </label><input type="file" name="imgday" /><br />
		<?$previewUrlday = UrlResolver::getImgBaseUrl("front/archivos/background_day.jpg");?>
		<label>Preview de la imagen de Dia: <label> <a href="<?=$previewUrlday?>"><?=$previewUrlday;?></a><br />
		<br />
		<label>Hora de inicio: </label><input type="text" name="dayhour" value="<?=$dayhour?>"/>
		<br />
		<br />
		<label>Color texto (Ejemplo: #cecece): </label><input type="text" name="textColorDay" value="<?=$textColorDay?>"/>
		<br />
		<br />
		<br />
		<label>Imagen de Noche: </label><input type="file" name="imgnight" /><br />
		<?$previewUrlnight = UrlResolver::getImgBaseUrl("front/archivos/background_night.jpg");?>
		<label>Preview de la imagen de Noche: <label> <a href="<?=$previewUrlnight?>"><?=$previewUrlnight;?></a><br />
		<br />
		<br />
		<label>Hora de inicio: </label><input type="text" name="nighthour" value="<?=$nighthour?>"/>
		<br />
		<label>Color texto (Ejemplo: #cecece): </label><input type="text" name="textColorNight" value="<?=$textColorNight?>"/>
		<br />
		<br />
		<input type="submit" name="finalizarBackgrounds" value="Finalizar" />
	</form>
</div>
</body>
</html>