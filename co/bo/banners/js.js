function ValidarForm(frm)
{
    if (frm.txtNombre.length == 0)
    {
        return false;                                                                  
    }
    frm.submit();
}
function irVolver()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"banners/index.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar()
{
	try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="../../logica_bo/banners/actionform/eliminar_banner.php?IdBanner="+dt_getCeldaSeleccionada(0);
        
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="ABM.php?IdBanner="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}

function accionTipo() {
	
	var val = document.getElementById('txtTipo').options[document.getElementById('txtTipo').selectedIndex].value;
	
	if(val == 0) {
		document.getElementById('camp').style.display = 'none';
		document.getElementById('prod').style.display = 'none';
		document.getElementById('txtTamanio1').style.display = 'block';
		document.getElementById('txtTamanio2').style.display = 'none';
		document.getElementById('txtTamanio2').selected = "";
	} else if(val == 1) {
		document.getElementById('camp').style.display = 'block';
		document.getElementById('prod').style.display = 'none';
		document.getElementById('txtTamanio1').style.display = 'none';
		document.getElementById('txtTamanio2').style.display = 'block';
		document.getElementById('txtTamanio1').selected = "";
	} else if(val == 2) {
		document.getElementById('camp').style.display = 'block';
		document.getElementById('prod').style.display = 'block';
		document.getElementById('txtTamanio1').style.display = 'none';
		document.getElementById('txtTamanio2').style.display = 'block';
		document.getElementById('txtTamanio1').selected = "";
	}
	
}

function accionLimitado() {
	
	var val = document.getElementById('txtLimitado').checked;
	
	if(val) {
		document.getElementById('limitado').style.display = 'block';
	} else {
		document.getElementById('limitado').style.display = 'none';
	}
	
}

function activarSelects() {
	
	document.getElementById('txtTipo').onchange = function() {
		accionTipo()
	}
	
	document.getElementById('txtLimitado').onchange = function() {
		accionLimitado()
	}
	
	accionLimitado()
	accionTipo();
	
}