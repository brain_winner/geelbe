<?  
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/banners/clases/clsbanners.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/banners/datamappers/dmbanners.php");
   try
    {
        ValidarUsuarioLogueadoBo(4);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    //$postURL = Aplicacion::getIncludes("post", "banners");
    $postURL = array('banners' => 'logica_bo/banners/actionform/banners.php');
    $oLocal = dmBanners::getById((isset($_GET["IdBanner"])) ? $_GET["IdBanner"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Banners</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/banners/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMLocales" name="frmABMLocales" method="post" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="idBanner" name="idBanner" value="<?=$oLocal->getIdBanner()?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["banners"]);
            ?>
            <tr>
                <td>Nombre</td>
                <td><input type="text" id="txtNombre" name="txtNombre" maxlength="80" size=60 value="<?=$oLocal->getNombre()?>" /></td>
            </tr>
            <tr>
                <td>Tipo</td>
                <td><select id="txtTipo" name="txtTipo">
                	<option value="0" <?=($oLocal->getTipo() == 0 ? 'selected="selected"' : '');?>>Vidriera</option>
                	<option value="1" <?=($oLocal->getTipo() == 1 ? 'selected="selected"' : '');?>>Campa&ntilde;a</option>
                	<option value="2" <?=($oLocal->getTipo() == 2 ? 'selected="selected"' : '');?>>Producto</option>
                </select></td>
            </tr>
            
            <tr>
                <td>Posici&oacute;n </td>
                <td><select id="txtSubtipo" name="txtSubtipo">
                	<option id="txtTamanio1" value="0" <?=($oLocal->getSubtipo() == 0 ? 'selected="selected"' : '');?>>Cuerpo (Ancho m�ximo: 700px)</option>
                	<option id="txtTamanio2" value="0" <?=($oLocal->getSubtipo() == 0 ? 'selected="selected"' : '');?>>Cuerpo (Ancho m�ximo: 680px)</option>
                	<option value="1" <?=($oLocal->getSubtipo() == 1 ? 'selected="selected"' : '');?>>Sidebar (En Vidriera - ancho: 186px // En Campa�a y Productos - ancho:150px)</option>
                </select></td>
            </tr>
            
            <tr>
                <td>Imagen</td>
                <td>
                	<input type="file" name="img" size="60" />
                </td>
            </tr>
            
            <tr>
                <td>Link</td>
                <td>
                	<input type="text" name="txtLink" size="60" value="<?=$oLocal->getLink()?>" />
                </td>
            </tr>
            
            <tr>
                <td>Tiempo limitado</td>
                <td>
                	<input type="checkbox" name="txtLimitado" id="txtLimitado" value="1" <?=($oLocal->getLimitado() == 1 ? 'checked="checked"' : '');?> />
                </td>
            </tr>
            
          </table>
          
          <table id="limitado" style="display: none; margin-left: 50px">    
            <tr>
            	<td colspan="2"><i>Formato de Fecha: YYYY-MM-DD HH:MM:SS</i></td>
            </tr>                  
            <tr>
            	<td>Desde</td>
               <td><input type="text" id="txtDesde" name="txtDesde" maxlength="80" size=60 value="<?=$oLocal->getDesde()?>" /></td>
				</tr>
				<tr>
				  	<td>Hasta</td>
               <td><input type="text" id="txtHasta" name="txtHasta" maxlength="80" size=60 value="<?=$oLocal->getHasta()?>" /></td>
            </tr>
          </table>

          <table id="camp" style="display: none">            
            <tr>
            	<td valign="top">Campa&ntilde;a</td>
            	<td>
            		<select id="txtIdCampania" name="txtIdCampania">
            			<option value="null">Seleccione</option>
         			<?php
         				$camps = dmCampania::getCampanias('Nombre', 'ASC');
         				foreach($camps as $i => $c)
         					echo '<option value="'.$c['IdCampania'].'" '.($c['IdCampania'] == $oLocal->getIdCampania() ? 'selected="selected"' : '').'><strong>'.$c['Nombre'].'</strong> - Fecha de Inicio: ' . $c['Fecha de inicio'] .'</option>';
         			?>
           		</select>
            </tr>
          </table>

			 <table id="prod" style="display: none">
            <tr>
            	<td valign="top">Producto</td>
            	<td>
            		<select id="txtIdProducto" name="txtIdProducto">
            			<option value="null">Seleccione</option>
         			<?php
         				$collProductos = dmProductos::getProductosAll('P.Nombre', 'ASC');
         				foreach($collProductos as $i => $c)
         					echo '<option value="'.$c['IdProducto'].'" '.($c['IdProducto'] == $oLocal->getIdProducto() ? 'selected="selected"' : '').'>'.$c['Producto'].'</option>';
         			?>
           		</select>
            </tr>
            
          </table>
          
          <script type="text/javascript">activarSelects();</script>

			<table>
            
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>