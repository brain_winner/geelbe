function ErrorMSJ(code, def)
{
    if(arrErrores["USUARIOS"][code] != undefined)
        document.getElementById("msjError").innerHTML = arrErrores["USUARIOS"][code];
    else
        document.getElementById("msjError").innerHTML = code;
}
function EliminarUsuario(idUsuario)
{
    if (confirm("Desea eliminar el usuario ?"))
        window.location.href = "../../logica_bo/clientes/actionform/eliminar_clientes.php?IdCliente=" + idUsuario;
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(frm['frmDaU[txtNombre]'].value.length == 0)
        {
            arrError.push(arrErrores["USUARIOS"]["NOMBRE"]);
        }
        if(frm['frmDaU[txtApellido]'].value.length == 0)
        {
            arrError.push(arrErrores["USUARIOS"]["APELLIDO"]);
        }
        if(frm['frmDaU[cbxDia]'].value == 0 || frm['frmDaU[cbxMes]'].value == 0 || frm['frmDaU[cbxAnio]'].value == 0)
        {
            arrError.push(arrErrores["USUARIOS"]["FECHA DE NACIMIENTO"]);
        }
        if(frm['frmU[txtClaveNuevo1]'].value.length > 0 || frm['frmU[txtIdUsuario]'].value == 0)
        {
            if(frm['frmU[txtClaveNuevo1]'].value.length < 8 || frm['frmU[txtClaveNuevo1]'].value.length > 16)
            {
                arrError.push(arrErrores["USUARIOS"]["CLAVE_INCORRECTO"]);
            }
            if(frm['frmU[txtClaveNuevo1]'].value != frm['frmU[txtClaveNuevo2]'].value)
            {
                arrError.push(arrErrores["USUARIOS"]["CLAVE_DIFERENTES"]);
            }
        }
        /*if(frm['frmU[txtEmailNuevo1]'].value.length >0 || frm['frmU[txtIdUsuario]'].value == 0)
        {
            if(!ValidarEmail(frm['frmU[txtEmailNuevo1]'].value))
            {
                arrError.push(arrErrores["USUARIOS"]["MAIL_INCORRECTO"]);
            }
            if(frm['frmU[txtEmailNuevo1]'].value != frm['frmU[txtEmailNuevo2]'].value)
            {
                arrError.push(arrErrores["USUARIOS"]["MAIL_DIFERENTES"]);
            }
        } */
        if (arrError.length>0)
        {
            var error = ""
            for(i=0; i<arrError.length; i++)
            {
                error +="*" + arrError[i] + "\n";
            }
            alert(error);
            return false;
        }
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}
function irVolver()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"clientes/";
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEditarPedido()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["USUARIOS"]["SELECCIONE"]);
        else
            window.location.href="../pedidos/ABM.php?IdPedido="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVerPedido()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["USUARIOS"]["SELECCIONE"]);
        else
            window.location.href="../pedidos/Ver.php?IdPedido="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["USUARIOS"]["SELECCIONE"]);
        else
            window.location.href="ABM.php?IdUsuario="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVer()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["USUARIOS"]["SELECCIONE"]);
        else
            window.location.href="Ver.php?IdUsuario="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar(id)
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["USUARIOS"]["SELECCIONE"]);
        else
        {
            if(confirm(arrErrores["USUARIOS"]["ELIMINAR"]))
                window.location.href="../../logica_bo/usuarios/actionform/eliminar_usuario.php?IdUsuario="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}
function irLeer()
{
    if(!dt_getCeldaSeleccionada(0))
    {
        alert("Debe seleccionar un mensaje para poder editarlo.");
    }
    else
    {
        window.location.href =  "../mensajes/leer.php?idMensaje=" + dt_getCeldaSeleccionada(0);
    }
}
function verCliente(id)
{
    window.location.href="Ver.php?IdUsuario="+dt_getCeldaSeleccionada(0);
}
function verPadrino(id)
{
    window.location.href="Ver.php?IdUsuario="+id;
}
function irActivos()
{
    try
    {
    }
    catch(e)
    {

    }
}
function iniciar(pais,prov,celue)
{
    setProvinciaDir(prov);
    setPaisDir(pais);
    setCeluEmpresa(celue); 
}

function setProvinciaDir(prov)
{
	if(document.getElementById('idProvincia')) {
		document.getElementById('idProvincia').value = prov;    
	}
}
function setPaisDir(pais)
{
	if(document.getElementById('frmDi1[cbxPais]')) {
		document.getElementById('frmDi1[cbxPais]').value = pais;
	}
}
function setCeluEmpresa(celue){
	if(document.getElementById('frmDaU[cmb_CelularComp]')) {
		document.getElementById('frmDaU[cmb_CelularComp]').value = celue;
	}
}                                  
