<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
		$postURL = Aplicacion::getIncludes("post", "clientes");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(11);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
    try {
		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array('U.NombreUsuario', 'DUP.Apellido', 'DUP.Nombre', 'U.Padrino'));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("U.FechaIngreso", "DESC");

		$parametro = "";
		$showing = "";
	    if ($_GET["userType"]) {
	        if(Aplicacion::Decrypter($_GET["userType"]) == "activos") {
				$count = dmCliente::getClientesActivosCount($paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmCliente::getClientesActivosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
				$showing = "Activos";
			}
	        else if (Aplicacion::Decrypter($_GET["userType"]) == "inactivos") {
				$count = dmCliente::getClientesInactivosCount($paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmCliente::getClientesInactivosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage()); 
				$showing = "Inactivos";
			} 
	        else if (Aplicacion::Decrypter($_GET["userType"]) == "huerfanos") {
				$count = dmCliente::getClientesHuerfanosCount($paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmCliente::getClientesHuerfanosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage()); 
				$showing = "Huerfanos";
			}
	        else if (Aplicacion::Decrypter($_GET["userType"]) == "desuscriptos") {
				$count = dmCliente::getClientesDesuscriptosCount($paramArray);
				$listPager = new ListadoPaginator($count);
				$rows = dmCliente::getClientesDesuscriptosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage()); 
				$showing = "Desuscriptos";
			}
	        $parametro = "?userType=".$_GET["userType"];
	    }
	    else {
			$count = dmCliente::getClientesCount($paramArray);
			$listPager = new ListadoPaginator($count);
			$rows = dmCliente::getClientesPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage()); 
			$showing = "Todos";
		}   
	
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }
?>	

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Clientes</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Clientes (<?=$showing;?>)</h1>	

			<div id="mainStats">
					<table style="margin-bottom:10px;">
						<tr class="alt">
							<td style="text-align:left;font-weight:bold;"><b>Mostrar Clientes:</b> <a href="index.php">Todos (<?=dmCliente::getClientesCount(array())?>)</a> | 
			                  <a href="index.php?userType=<?=Aplicacion::Encrypter("activos")?>">Activos (<?=dmCliente::getClientesActivosCount(array())?>)</a> | 
							  <a href="index.php?userType=<?=Aplicacion::Encrypter("inactivos")?>">Inactivos (<?=dmCliente::getClientesInactivosCount(array())?>)</a> | 
							  <a href="index.php?userType=<?=Aplicacion::Encrypter("huerfanos")?>">Huerfanos (<?=dmCliente::getClientesHuerfanosCount(array())?>)</a> | 
							  <a href="index.php?userType=<?=Aplicacion::Encrypter("desuscriptos")?>">Desuscriptos (<?=dmCliente::getClientesDesuscriptosCount(array())?>)</a></td>
						</tr>
					</table>
					
				</div>
			
			
			<?php
				$listadoPrinter = new ListadoPrinter($rows, "index.php$parametro", $listPager);
				$listadoPrinter->showColumn("U.NombreUsuario", "E-Mail");
				$listadoPrinter->showColumn("DUP.Apellido", "Apellido");
				$listadoPrinter->showColumn("DUP.Nombre", "Nombre");
				$listadoPrinter->showColumn("U.Padrino", "C�digo de Invitaci�n");
				$listadoPrinter->showColumn("FechaIngreso", "Fecha de Ingreso");
				$listadoPrinter->showColumn("MailSuscription", "Mail");
				$listadoPrinter->showColumn("Estado");
				
				$listadoPrinter->setSortName("FechaIngreso", "U.FechaIngreso");
				
				$listadoPrinter->setShowFilter("FechaIngreso", false);
				$listadoPrinter->setShowFilter("Estado", false);
				$listadoPrinter->setShowFilter("MailSuscription", false);
				
				$listadoPrinter->setSortColumn("Estado", false);
				$listadoPrinter->setSortColumn("MailSuscription", false);
				
				$listadoPrinter->addHiddenValue("userType", $_GET['userType']);
				
				$listadoPrinter->linkColumn("U.NombreUsuario", "/".$confRoot[1]."/bo/clientes/Ver.php?IdUsuario=[[U.IdUsuario]]", "Ver Cliente");
				$listadoPrinter->linkColumn("U.Padrino", "/".$confRoot[1]."/bo/clientes/Ver.php?NombreUsuario=[[U.Padrino]]", "Ver Cliente");

				$listadoPrinter->printListado();
			?>
			
		</div>	
	</body>
</html>