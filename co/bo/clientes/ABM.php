<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
    try
    {
        ValidarUsuarioLogueadoBo(3);
    }
    catch(ACCESOException $e)
    {
    ?>
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
    $postURL = Aplicacion::getIncludes("post", "clientes");
    $objCliente = dmCliente::getByIdUsuario((isset($_GET["IdUsuario"])) ? $_GET["IdUsuario"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Clientes</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/clientes/js.js");?>" type="text/javascript"></script>
	<script src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.2.min.js");?>" type="text/javascript"></script>
     <link rel="stylesheet" type="text/css" href="<?=UrlResolver::getCssBaseUrl("bo/pedidos/AMB_pedidos.css")?>" media="screen" />
</head>
<body>
    <form id="frmABMiClientes" name="frmABMiClientes" target="iClientes" action="<?=Aplicacion::getRootUrl().$postURL["clientes"]?>" method="POST" onSubmit="ValidarForm(this);">
        <input type="hidden" id="frmU[txtIdUsuario]" name="frmU[txtIdUsuario]" value="<?=($objCliente->getIdUsuario() >0)?Aplicacion::Encrypter($objCliente->getIdUsuario()):0?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <tr>
                <td>Nombre</td>
                <td><input name='frmDaU[txtNombre]' id='frmDaU[txtNombre]' type='text' value="<?=$objCliente->getDatos()->getNombre();?>" size="30"/></td>
            </tr>
            <tr>
                <td>Apellido</td>
                <td><input name='frmDaU[txtApellido]' id='frmDaU[txtApellido]' type='text' size="30 "value="<?=$objCliente->getDatos()->getApellido();?>" /></td>
            </tr>
            <tr>
                <td>Masculino <input type="radio" <?=($objCliente->getDatos()->getIdTratamiento() == 1)?"checked=\"checked\"":""?> id="frmDaU[optTratamiento]" name="frmDaU[optTratamiento]" value="1"/></td>
                <td>Femenino <input type="radio" <?=($objCliente->getDatos()->getIdTratamiento() == 2)?"checked=\"checked\"":""?> id="frmDaU[optTratamiento]" name="frmDaU[optTratamiento]" value="2"/></td>
            </tr>
            <tr>
                <td>N&uacute;mero de documento de identidad </td>
                <td><input name='frmDaU[txtNroDoc]' id='frmDaU[txtNroDoc]' type='text' size="30" value="<?=$objCliente->getDatos()->getNroDoc();?>" /></td>
            </tr>
            <tr>
                <td>Fecha de Nacimiento</td>
                <td>
                    <select name="frmDaU[cbxDia]" id="frmDaU[cbxDia]">
                      <option value="0" >D&iacute;a</option>
                        <?
                        for ($i=1; $i<=31; $i++)
                        {
                            ?><option value="<?=$i?>"><?=$i?></option><?
                        }
                        ?>
                    </select>
                    &nbsp;/&nbsp;
                    <select name="frmDaU[cbxMes]" id="frmDaU[cbxMes]">
                        <option value="0" >Mes</option>
                        <option value="1" >enero</option>
                        <option value="2" >febrero</option>
                        <option value="3" >marzo</option>
                        <option value="4" >abril</option>
                        <option value="5" >mayo</option>
                        <option value="6" >junio</option>
                        <option value="7" >julio</option>
                        <option value="8" >agosto</option>
                        <option value="9" >septiembre</option>
                        <option value="10" >octubre</option>
                        <option value="11" >noviembre</option>
                        <option value="12" >diciembre</option>
                    </select>
                    &nbsp;/&nbsp;
                    <select name="frmDaU[cbxAnio]" id="frmDaU[cbxAnio]">
                        <option value="0" >A&ntilde;o</option>
                        <?
                        for ($i=(date("Y")-99); $i<=date("Y"); $i++)
                        {
                            ?><option value="<?=$i?>"><?=$i?></option><?
                        }
                        ?>
                     </select>
                    <?
                    if($objCliente->getDatos()->getFechaNacimiento() != "")
                    {
                        $FechaNac = $objCliente->getDatos()->getFechaNacimiento();
                        $FechaNac = explode("/", $FechaNac);
                        ?>
                        <script>
                            document.getElementById("frmDaU[cbxDia]").value="<?=$FechaNac[0]?>";
                            document.getElementById("frmDaU[cbxMes]").value="<?=$FechaNac[1]?>";
                            document.getElementById("frmDaU[cbxAnio]").value="<?=$FechaNac[2]?>";
                        </script>
                        <?
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>Tel&eacute;fono Fijo</td>
                <td><input name='frmDaU[txtCodTelefono]' id='frmDaU[txtCodTelefono]' type="text" size="3" value="<?=$objCliente->getDatos()->getCodTelefono()?>"/>
                <input name='frmDaU[txtTelefono]' id='frmDaU[txtTelefono]' type='text' size="19" value="<?=$objCliente->getDatos()->getTelefono()?>" /></td>
            </tr>
            <tr>
                <td>N&uacute;mero de celular</td>
                <td>
                    <select name='frmDaU[cmb_CelularComp]' id='frmDaU[cmb_CelularComp]'>
					   <option value="0">Compa�ia</option>
							<option value="1">COMCEL</option>
							<option value="2">MOVISTAR</option>
							<option value="3">TIGO</option>
							<option value="4">OTRO</option>
						</select>
                    <script>
                        document.getElementById("frmDaU[cmb_CelularComp]").value="<?=$objCliente->getDatos()->getceluempresa()?>";
                    </script>
                <input name='frmDaU[txt_CelularCodigo]' type='text' id='frmDaU[txt_CelularCodigo]' size="3" value="<?=$objCliente->getDatos()->getcelucodigo()?>" />
                <input name='frmDaU[txt_CelularNumero]' type='text' id='frmDaU[txt_CelularNumero]' size="15" value="<?=$objCliente->getDatos()->getcelunumero()?>" />
                <br/>
                </td>
            </tr>
            <!--<tr>
                <td>Nueva Contrase&ntilde;a</td>
                <td><input name='frmU[txtClaveNuevo1]' id='frmU[txtClaveNuevo1]' type='password' size="20" />
                <br/><strong>MINIMO 8 DIGITOS </strong></td>
            </tr>
            <tr>
                <td>Nueva Contrase&ntilde;a</td>
                <td><input name='frmU[txtClaveNuevo2]' id='frmU[txtClaveNuevo2]' type='password' size="20" />
                <br/><strong>MINIMO 8 DIGITOS </strong></td>
            </tr>-->
            <?
                if($objCliente->getNombreUsuario() != "")
                {
                    ?>
                        <tr>
                            <td>eMail actual:</td>
                            <td><strong><?=$objCliente->getNombreUsuario();?></strong></td>
                        </tr>
                    <?
                }
            ?>
            <!--tr>
                <td>Nuevo Email</td>
                <td><input name='frmU[txtEmailNuevo1]' id='frmU[txtEmailNuevo1]' type='text' size="30" />
                <br/>Es importante que mantengas actualizada tu direcci&oacute;n de eMail, para asegurarte que recibir&aacute;s las comunicaciones con las propuestas de venta de <strong>Geelbe</strong>.</td>
            </tr>
            <tr>
                <td>Repetir Email</td>
                <td><input name='frmU[txtEmailNuevo2]' id='frmU[txtEmailNuevo2]' type='text' size="30"/></td>
            </tr-->
			<tr>
                <td>Departamentos</td>
                <td>
                    <select name='frmDi1[cbxProvincia]' id='idProvincia' class='imputbportada' onchange="javascript:actualizarCiudades()"> 
                    <option value="0" >Departamentos</option>
                    <?
						$dt = dmMicuenta::GetProvincias();
						$objDireccion = $objCliente->getDirecciones(0);
						foreach($dt as $value)
						{
						    if($objDireccion->getIdProvincia() == $value['IdProvincia']) {
							    ?>
							        <option selected="selected" value='<?=$value['IdProvincia']?>'><?=$value['Nombre']?></option>
							    <?
						    } else {
							    ?>
							        <option value='<?=$value['IdProvincia']?>'><?=$value['Nombre']?></option>
							    <?
						    }
						}                                                 
					?>  
                    </select>
                </td>
            </tr>
			<tr>
				<td>Ciudad:</td>
				<td>
					<select name='frmDi1[cbxCiudad]' id='idCiudad' class='imputbportada'> 
					<option value="0" >Ciudades</option>
                    <?
                         if($objCliente->getDirecciones(0)->getIdProvincia() != null) { 
	                         $dt = dmMicuenta::GetCiudades($objCliente->getDirecciones(0)->getIdProvincia());
	                         foreach($dt as $value)
	                         {
	                         	if($objCliente->getDirecciones(0)->getIdCiudad() == $value['IdCiudad']) {
		                         	?>
		                               <option selected="selected" value='<?=$value['IdCiudad']?>'><?=$value['Descripcion']?></option>
		                            <?
	                         	} else {
		                            ?>
		                               <option value='<?=$value['IdCiudad']?>'><?=$value['Descripcion']?></option>
		                            <?
	                         	}
	                         }
                         }                                                 
                  ?>                                            
					</select>
				</td>
            </tr>
			<tr>
                <td>Direcci&oacute;n</td>
                <td><input name='frmDi1[txtDomicilio]' id='frmDi1[txtDomicilio]' type='text' size='30' value="<?=$objCliente->getDirecciones(0)->getDomicilio()?>" /> <span style="font-style:italic;font-size:9px;"> (Ejemplo: Carrera 15 #93a-62)</span></td>
            </tr>
            <tr>
                <td>
                    <input type="button" value="Eliminar usuario" onclick="EliminarUsuario(<?=$objCliente->getIdUsuario()?>);" />
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].submit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
    <iframe style="display:none" id="iClientes" name="iClientes"></iframe>
	<script>
    <?
	  if ($objCliente->getDirecciones(0)->getIdProvincia()>0)
      {
        $Provincia = $objCliente->getDirecciones(0)->getIdProvincia();
      }
      else
      {
        $Provincia = 0;
      }
       if ($objCliente->getDirecciones(0)->getIdPais()>0)
      {
        $Pais = $objCliente->getDirecciones(0)->getIdPais();
      }
      else
      {
        $Pais = 0;
      }
      if($objCliente->getDatos()->getceluempresa()>0)
      {
        $celuempresa = $objCliente->getDatos()->getceluempresa();
      }
      else
      {
        $celuempresa = 0;
      }
     ?>  
   iniciar(<?=$Pais?>,<?=$Provincia?>,<?=$celuempresa?>);

  function actualizarCiudades() {
	   $.ajax({ url: "<?=Aplicacion::getRootURl()?>logica_bo/clientes/actionform/obtener_ciudades.php?IdProvincia="+$("#idProvincia").val(), 
	            context: document.body, 
				success: function(data){
 			$("#idCiudad").html(data);
	   }});
   }
    
</script>    
</body>
</html>
