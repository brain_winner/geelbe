<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    try
    {
        ValidarUsuarioLogueadoBo(3);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
	
	$parametro = "IdUsuario=".$_GET["IdUsuario"];
	
    $dtMensajes = dmMensajes::getMensajesUsuarios_ById($_GET["IdUsuario"], (isset($_GET["columna"])?$_GET["columna"]+1:null), (isset($_GET["sentido"])?$_GET["sentido"]:null), (isset($_GET["q"])?$_GET["q"]:""));
    $objDT = new Tabla($dtMensajes, (isset($_GET["tp"])?$_GET["tp"]:10));
    $objDT->setPagina("mensajes.php");
	$objDT->setParametro($parametro);
	
    $objDT->setPalabraClave($_GET["q"]);
    $objDT->setPaginaNro((isset($_GET["irPagina"])?$_GET["irPagina"]:0));
    $objDT->setColumnaNro((isset($_GET["columna"])?$_GET["columna"]:0));
    $objDT->setColumnaSentido((isset($_GET["sentido"])?$_GET["sentido"]:"DESC"));
    $objDT->setPropiedadesColumna(0, array("style=\"display:none\""));        
    $objDT->setPropiedadesColumna(1, array("style=\"display:none\""));
    $objDT->setPropiedadesColumna(2, array("style=\"display:none\""));
    $objDT->setPropiedadesColumna(5, array("style=\"display:none\""));
    //$objDT->setPropiedadesColumna(7, array("style=\"display:none\""));
    $objDT->addBotones('<a href="javascript:irLeer();" ><img src="'.UrlResolver::getImgBaseUrl("bo/imagenes/btnEditarMini.gif").'" title="Leer" name="btnLeer" id="btnLeer" border="0"/></a>');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> Mensajes</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/clientes/js.js");?>" type="text/javascript"></script>
    <link href="<?=UrlResolver::getCssBaseUrl("bo/style/css.css")?>" rel="stylesheet" type="text/css" />
</head>
<body>
<table style="width:100%; height:100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="borde_top">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="borde_bottom ">
            <!--<a href="javascript:irNuevo();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnNuevo','','../imagenes/btnNuevo_on.gif',1)"><img src="../imagenes/btnNuevo.gif" alt="Nuevo" name="btnNuevo" id="btnNuevo" border="0"/></a>-->
        </td>
    </tr>
    <tr>
        <td colspan="100%">
            <?
            $objDT->show();
            ?>
        </td>
    </tr>
</table>
</body>
</html>

