<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/clases/clsusuarios.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/historico/datamappers/dmhistoricologin.php");
    require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_transaction.php";
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_service.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
    
    try {
        ValidarUsuarioLogueadoBo(3);
    } catch(ACCESOException $e) {
	    ?>
	        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
	    <?
        exit;
    }

    if(!isset($_GET["NombreUsuario"])) {
    	$_GET["NombreUsuario"] = $_GET["mail"];
    }
    
    if(isset($_GET["NombreUsuario"])) {
    	$idUsuario = dmUsuario::getIdByNombreUsuario($_GET["NombreUsuario"]);
    } else {
    	If($_GET["IdUsuario"] == null){
			echo "<style type='text/css'>
					  h1 {
						color: #DD3333;
						font-size: 1.5em;
						font-weight: normal;
						margin: 20px 0;
						font-family: Arial,Helvetica,sans-serif;
					}
					</style>
			<h1>".$_GET["mail"]." no estaba logueado al formular la pregunta o no es un usuario registrado.</h1>";
			
			exit;
		}
    	
    	$idUsuario = $_GET["IdUsuario"]; 
    }
    
    if(isset($_POST['suscribir'])) {
	    $oConexion = Conexion::nuevo();
		try {
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("UPDATE usuarios SET mail_suscription = 1 WHERE IdUsuario = ".$idUsuario);
			$oConexion->EjecutarQuery();
			$oConexion->Cerrar_Trans();
		}
		catch (MySQLException $e) {
			$oConexion->RollBack();
			throw $e;
		}
    }
    
    if(isset($_POST['desuscribir'])) {
	    $oConexion = Conexion::nuevo();
		try {
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("UPDATE usuarios SET mail_suscription = 0 WHERE IdUsuario = ".$idUsuario);
			$oConexion->EjecutarQuery();
			$oConexion->Cerrar_Trans();
		}
		catch (MySQLException $e) {
			$oConexion->RollBack();
			throw $e;
		}
    }
    
   if(isset($_GET['deactivate'])) {
   	
   	dmUsuario::DesactivarCuenta($idUsuario);
   	
   	$oConexion = Conexion::nuevo();
		try {
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("INSERT INTO codigo_desuscripcion (IdUsuario, CodigoDesuscripcion, Fecha) VALUES (".$idUsuario.",'".time()."', NOW())");
			$oConexion->EjecutarQuery();
			$oConexion->Cerrar_Trans();
			
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("UPDATE usuarios SET MotivoBaja = '".$_POST['motivo']."', UsuarioBaja = '".Aplicacion::Decrypter($_SESSION['BO']['User']['id'])."' WHERE IdUsuario = ".$idUsuario);
			$oConexion->EjecutarQuery();
			$oConexion->Cerrar_Trans();
			
		}
		catch (MySQLException $e) {
			$oConexion->RollBack();
			throw $e;
		}
   	
   }
    
	$mensajesdb = dmMensajes::getMensajes_ByIdUsuario($idUsuario, "");
	
	$oConexion = Conexion::nuevo();
    $oConexion->setQuery("SELECT p.IdPedido, p.Total, p.Fecha, c.Nombre, e.Descripcion as Estado FROM pedidos p	LEFT JOIN campanias c ON p.IdCampania = c.IdCampania LEFT JOIN estadospedidos e ON e.IdEstadoPedido = p.IdEstadoPedidos	WHERE p.IdUsuario = ".mysql_real_escape_string($idUsuario)." ORDER BY p.Fecha DESC");
    $pedidos = $oConexion->DevolverQuery();
    
    $objCliente = dmCliente::getByIdUsuario((isset($idUsuario)) ? $idUsuario:0); 
    $idPadrino = dmUsuario::getIdByNombreUsuario($objCliente->getPadrino()); 
	
	$baja = ($objCliente->getes_Activa() == 1 && $objCliente->getUsuarioBaja() > 0);
	if($baja)
		$usuarioBaja = dmUsuario::getByIdUsuario($objCliente->getUsuarioBaja());

	$oConexion = Conexion::nuevo();
    $oConexion->setQuery("SELECT NombreUsuario, IdUsuario, es_activa, es_Provisoria from usuarios where Padrino='".$objCliente->getNombreUsuario()."'");
    $ahijados = $oConexion->DevolverQuery();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Informaci&oacute;n del Usuario</title>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/js/protoaculous.1.8.1.js")?>"  type="text/javascript"></script>	    
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/js/autosubmit.js")?>"  type="text/javascript"></script>	    
	    <script src="<?=UrlResolver::getJsBaseUrl("bo/js/tablesort.js")?>"  type="text/javascript"></script>	    
		<script src="<?=UrlResolver::getJsBaseUrl("bo/js/autotablesort.js")?>"  type="text/javascript"></script>
		
        <script src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.2.min.js");?>" type="text/javascript"></script>
		
	</head>
	
	<body>
		<div id="container">
			<h1><?=$objCliente->getNombreUsuario()?></h1>
			<form action="<?="/".$confRoot[1]."/bo/clientes/ABM.php"?>" method="GET" style="float:left">
			  <input type="hidden" name="IdUsuario" id="IdUsuario" value="<?=$idUsuario?>" />
			  <input type="submit" name="editarUsuario" id="editarUsuario" value="Editar usuario" /> 
			  <input type="submit" name="editarUsuario" id="editarUsuario" value="Editar usuario" /> 
			  <?php if(!$baja): ?>
			  <input type="button" value="Dar de baja" onclick="document.getElementById('baja').style.display = 'block'" />
			  <?php endif; ?>	
			  <input type="button" value="Enviar Mail" id="btnOcultarEnvio"/>
			</form>
			<form action="<?="/".$confRoot[1]."/bo/clientes/Ver.php?IdUsuario=".$objCliente->getIdUsuario()?>" method="POST" style="margin-left:4px;float:left">
				<?php if($objCliente->getMailSuscription() == 0): ?>
				  <input type="submit" name="suscribir" value="Suscribir usuario" />
				  <?php else: ?>
				  <input type="submit" name="desuscribir" value="Desuscribir usuario" />
				  <?php endif; ?>
			</form>
			<div style="clear:both"></div>
			 
				<div id="EnviarMail">
				<form id="EnviarMailfrm" name="EnviarMailfrm" method="post" action="<?=UrlResolver::getBaseUrl('logica_bo/clientes/actionform/enviarmail.php')?>">	
					<input type="hidden" name="email" id="email" value="<?=$objCliente->getNombreUsuario()?>" />
					<input type="hidden" name="IdUsuario" id="IdUsuario" value="<?=$idUsuario?>" />
						<table style="Border: 1px solid #BEBEBE;">
							<tr>
								<td style="Border: 1px solid #BEBEBE;">Asunto: <input name='subject' id='subject' type='text' size="120" value=""/></td>
							<tr>
							<tr>
								<td style="Border: 1px solid #BEBEBE;"><textarea id="body" name="body" cols="98" rows="10" value=""></textarea></td>
							<tr>
							<tr>
								<td style="Border: 1px solid #BEBEBE;"><input type="submit" value="Enviar" id="botonEnviarMail" name="botonEnviarMail" /></td>
							<tr>
						</table>		
				</form>
				</div>
			 <script>
				$('#EnviarMail').hide();
					$('#btnOcultarEnvio').click(function() {
						$('#EnviarMail').toggle();
					});	
				$('#botonEnviarMail').click(function() {
					$('#EnviarMail').hide();
				});
			</script>
			
			<?php if(!$baja): ?>
			<form action="<?="/".$confRoot[1]."/bo/clientes/Ver.php?IdUsuario=".$_GET['IdUsuario'].'&deactivate'?>" method="post" id="baja" style="display: none">
				<br />
				
				<label>
					Motivo de la baja:<br />
					<textarea rows="5" cols="30" name="motivo"></textarea>
				</label>
				
				<br />
				<br />
				
				<input type="submit" value="Confirmar" />
				<input type="reset" value="Cancelar" onclick="document.getElementById('baja').style.display = 'none'" />
				
			</form>
			<?php else: ?>
			<br />
			<p>El usuario fue dado de baja por <?=$usuarioBaja->getNombreUsuario();?>.</p>
			<p><strong>Motivo: </strong><?=$objCliente->getMotivoBaja();?></p>
			<?php endif; ?>

			<div class="column">
			    <div class="stats">
					<h2>Informaci&oacute;n general</h2>
					<table>
						<tr class="alt">
							<th>Nombre Completo</th>
							<td><?=$objCliente->getDatos()->getApellido().", ".$objCliente->getDatos()->getNombre()?></td>
						</tr>
						<tr>
							<th>Edad</th>
							<td><?
								list($day,$month,$year) = explode("/",$objCliente->getDatos()->getFechaNacimiento());
	    						$year_diff  = date("Y") - $year;
	    						$month_diff = date("m") - $month;
	    						$day_diff   = date("d") - $day;
	    						if ($day_diff < 0 || $month_diff < 0)
	      							$year_diff--;
	      						echo $year_diff;
								?></td>
						</tr>
						<tr class="alt">
							<th>Fecha de nacimiento</th>
							<td><?=$objCliente->getDatos()->getFechaNacimiento()?></td>
						</tr>
						<tr>
							<th>Email</th>
							<td><?=$objCliente->getNombreUsuario()?></td>
						</tr>
						<tr class="alt">
                          <th>Sexo</th>
                          <td><?=($objCliente->getDatos()->getIdTratamiento()==1?"Masculino":"Femenino")?></td>
						</tr>
						<tr>
							<th>DNI</th>
							<td><?=$objCliente->getDatos()->getNroDoc()?></td>
						</tr>
						<tr class="alt">
				          <th>Tel&eacute;fono</th>
				          <td><?=$objCliente->getDatos()->getCodTelefono() . " - " . $objCliente->getDatos()->getTelefono()?></td>
				        </tr>
				        <tr>
				          <th>M&oacute;vil</th>
				          <td><?=$objCliente->getDatos()->getcelucodigo() . " - " . $objCliente->getDatos()->getcelunumero()?></td>
				        </tr>
				      </table>
				      <h2>Estado Geelbe</h2>
					  <table>
						<tr class="alt">
							<th>Padrino</th>
							<td><a href="<?="/".$confRoot[1]."/bo/clientes/Ver.php?IdUsuario=".$idPadrino?>"><?=$objCliente->getPadrino()?></a></td>
						</tr>
						<tr>
							<th>Registrado el</th>
							<td><?=$objCliente->getFechaIngreso();?></td>
						</tr>
					    <tr class="alt">
					      <th>Estado</th>
					      <td><?
					      		if($objCliente->getes_Activa()) {
					      			if($objCliente->getes_Provisoria()) {
					      				if($objCliente->getPadrino()=="Huerfano") {
							      			echo "HUERFANO"; 
					      				} else {
							      			echo "INACTIVO"; 
					      				}
					      			} else {
						      			echo "DESUSCRIPTO"; 
					      			}
					      		} else {
					      			echo "ACTIVO";
					      		}
					      	  ?>
					      </td>
					    </tr>
					    <tr>
					      <th>&Uacute;ltimo login</th>
					      <td><?=dmLoginH::ObtenerUltimoLogin($objCliente->getNombreUsuario())?></td>
				        </tr>
                        <tr class="alt">
                          <th>Invitados</th>
					      <td><?
					        $oEstadistica = new dmEstadisticas();
					        echo $oEstadistica->Cliente()->CountUsuariosAhijados($objCliente->getIdUsuario())?>
					      </td>
					    </tr>
					    <tr>
					      <th>Tasa (Ahijados)</th>
					      <td><?
			                  if ($oEstadistica->Cliente()->CountUsuariosAhijados($objCliente->getIdUsuario()) > 0) {
			                      echo round($oEstadistica->Cliente()->CountUsuariosAhijadosRegistrados($objCliente->getIdUsuario()) / $oEstadistica->Cliente()->CountUsuariosAhijados($objCliente->getIdUsuario()), 4);
			                  } else {
			                      echo 0;
			                  }
			                  ?>
			              </td>
					    </tr>
					    <tr class="alt">
					      <th>Invitados registrados</th>
					      <td><?
					            echo $oEstadistica->Cliente()->CountUsuariosAhijadosRegistrados($objCliente->getIdUsuario());
					          ?>
					      </td>
					    </tr>
					    <tr>
					      <th>Invitados compradores</th>
					      <td><? echo $oEstadistica->Cliente()->CountUsuariosAhijadosCompradores($objCliente->getNombreUsuario());?></td>
					    </tr>
					</table>
				    <h2>Cr&eacute;ditos</h2>
					<table>
				      <tr class="alt">
				        <th>Cr&eacute;ditos Disponibles</th>
				        <td colspan="3">$ <?=dmMisCreditos::getCreditoDisponible($objCliente->getIdUsuario())?></td>
				      </tr>
				      <tr>
				        <th>Cr&eacute;ditos Potenciales</th>
				        <td colspan="3">$ <?=dmMisCreditos::getCreditoPotencial($objCliente->getIdUsuario(), $objCliente->getNombreUsuario())?></td>
				      </tr>
				      <tr class="alt">
				        <th>Cr&eacute;ditos Utilizados</th>
				        <td colspan="3">$ <?=(int) $oEstadistica->Cliente()->BonosUtilizados($objCliente->getIdUsuario())?></td>
				      </tr>
					</table>
					<h2>Estado GCash</h2>
					<?php $estadoGeelbeCash = GeelbeCashService::getEstadoGeelbeCash($objCliente->getIdUsuario()); ?>
					<table>
					  <tr class="alt">
				        <th>&Uacute;ltimo pago</th>
				        <td colspan="3">$<?=number_format(($estadoGeelbeCash->getMontoUltimoGasto())?$estadoGeelbeCash->getMontoUltimoGasto():0, 2, ',', '.')?></td>
				      </tr>
				      <tr>
				        <th>Utilizado hist&oacute;rico</th>
				        <td colspan="3">$<?=number_format(($estadoGeelbeCash->getMontoGastosHistoricos())?$estadoGeelbeCash->getMontoGastosHistoricos():0, 2, ',', '.')?></td>
				      </tr>
				      <tr class="alt">
				        <th>Cargado hist&oacute;rico</th>
				        <td colspan="3">$<?=number_format(($estadoGeelbeCash->getMontoCargasHistoricas())?$estadoGeelbeCash->getMontoCargasHistoricas():0, 2, ',', '.')?></td>
				      </tr>
				      <tr>
				        <th>Saldo actual</th>
				        <td colspan="3">$<?=number_format(($estadoGeelbeCash->getMontoSaldoActual())?$estadoGeelbeCash->getMontoSaldoActual():0, 2, ',', '.')?></td>
				      </tr>
					</table>
				  </div>
				</div>
				<div class="column columnRight">
				  <div class="stats">
 					<h2>Informaci&oacute;n Domicilio</h2>
 					<table>
					  <tr class="alt">
					   <th>Departamento</th>
					   <td><?
			                if ($objCliente->getDirecciones(0)->getIdProvincia()) {
			                    $oProvincia = dmCliente::GetProvinciasById($objCliente->getDirecciones(0)->getIdProvincia());
			                    echo $oProvincia[0]["Nombre"];
			                }
			                ?>
					   </td>
					 </tr>
					 <tr>
					   <th>Ciudad</th>
					   <td><?
			                if ($objCliente->getDirecciones(0)->getIdCiudad()) {
			                    $oCiudad = dmCliente::GetCiudadById($objCliente->getDirecciones(0)->getIdCiudad());
			                    echo $oCiudad[0]["Descripcion"];
			                }
			                ?></td>
					 </tr>
					 <tr class="alt">
					   <th>Direcci�n</th>
					   <td><?=$objCliente->getDirecciones(0)->getDomicilio()?></td>
					 </tr>
					 <tr>
					   <th>Pa&iacute;s</th>
					   <td>Colombia
					   </td>
					 </tr>
				   </table>
					
				  <h2>Mensajes</h2>
				   <h3>(Presionar sobre las filas para ver el detalle de cada mensaje)</h3>
  				   <table>
				     <tr>
					   <th>Fecha</th>
					   <th>Motivo</th>
					   <th>Respondido</th>
					 </tr>
						<?php
						$j=1;
						foreach($mensajesdb as $i => $mensaje) {
							$j++;
							$Motivo = dmMensajes::getMotivoById($mensaje['IdMotivo']);
							



							if($j%2 == 0) {
								echo '<tr class="alt" id="m'.$mensaje['idMensaje'].'">';
							}
							else {
								echo '<tr id="m'.$mensaje['idMensaje'].'">';
							}
						?>
						<td><?=date("d/m/Y", strtotime($mensaje['Fecha']));?></td>
						<td><a href="javascript:;" id="link<?=$mensaje['idMensaje'];?>"><?=$Motivo;?></a></td>
					    <td><?=(!isset($mensaje['respuesta']) ? $mensaje['Respondido'] : '');?></td>
				      </tr>
					  <?php		
							$oConexion = Conexion::nuevo();
	    					$oConexion->setQuery("SELECT idMensaje, 
	           						                     idMensajePadre, 
						                                 Fecha, 
						                                 IF(Respondido = 0, 'No', 'S�') as Respondido, 
						                                 Mensaje as Mensaje, 
						                                 idmotivo as IdMotivo,
						                                 boUserId as boUserId 
						                          FROM mensajes 
						                          WHERE idMensajePadre = ".$mensaje['idMensaje']);
	    					$respuestas = $oConexion->DevolverQuery();
					  
						
					  ?>					
					  <tr id="rta<?=$mensaje['idMensaje'];?>" style="background-color:#CCC;">
					  <td colspan="3">
					  <?php 
					  echo "<span style=\"font-weight:bold;\">Mensaje:</span> ".$mensaje['Mensaje']."<br/>";
					  foreach($respuestas as $i => $respuesta) {
					  	
	    				
					  	echo "--<br/>";
					  	echo "<span style=\"font-weight:bold;\">Respuesta:</span> ".$respuesta['Mensaje']."<br/>";
					  	if ($respuesta['boUserId']) {
					  		$objClienteBO = dmCliente::getByIdUsuario($respuesta['boUserId']);
					  		echo $objClienteBO->getNombreUsuario()." - ";
					  	}
					  	echo $respuesta['Fecha']."<br/>";
					  }
					  
						
					  ?>
					 </td>
					  </tr>
					  <script>
					  $('#rta<?=$mensaje["idMensaje"];?>').hide();
					  $('#link<?=$mensaje["idMensaje"];?>').click(function() {
						  $('#rta<?=$mensaje["idMensaje"];?>').toggle();
						});
										  
					  </script>
					  <?php 
						}
						?>
					</table>
					
					<h2>Mensajes BO</h2>
				   <h3>(Presionar sobre las filas para ver el detalle de cada mensaje)</h3>
  				   <table>
				     <tr>
					   <th>Fecha</th>
					   <th>Asunto</th>
					   <th>Usuario</th>
					 </tr>
						<?php
						$oConexion = Conexion::nuevo();
				        $oConexion->Abrir_Trans();
				        $oConexion->setQuery("SELECT m.idMensaje, m.fecha, m.asunto, m.mensaje, u.NombreUsuario FROM mensajes_bo m JOIN usuarios u ON u.IdUsuario = m.boUserId WHERE m.IdUsuario = ".$idUsuario." ORDER BY m.fecha ASC");
				        $Tabla = $oConexion->DevolverQuery();
				        $oConexion->Cerrar_Trans();
				        
				        $j = 1;
						foreach($Tabla as $i => $mensaje) {
							$j++;
							if($j%2 == 0) {
								echo '<tr class="alt" id="m'.$mensaje['idMensaje'].'">';
							}
							else {
								echo '<tr id="m'.$mensaje['idMensaje'].'">';
							}
						?>
						<td><?=date("d/m/Y", strtotime($mensaje['fecha']));?></td>
						<td><a href="javascript:;" id="link2<?=$mensaje['idMensaje'];?>"><?=$mensaje['asunto'];?></a></td>
					    <td><?=$mensaje['NombreUsuario']?></td>
				      </tr>
					  <tr id="rta2<?=$mensaje['idMensaje'];?>" style="background-color:#CCC;">
					  <td colspan="3">
					  <?php 
					  echo "<span style=\"font-weight:bold;\">Mensaje:</span> ".$mensaje['mensaje'];
					  ?>
					 </td>
					  </tr>
					  <script>
					  $('#rta2<?=$mensaje["idMensaje"];?>').hide();
					  $('#link2<?=$mensaje["idMensaje"];?>').click(function() {
						  $('#rta2<?=$mensaje["idMensaje"];?>').toggle();
						});
										  
					  </script>
					  <?php 
						}
						?>
					</table>
				</div>
			</div>
			<div class="stats">
			  <h2>Pedidos</h2>
			  <table>
			    <tr>
				  <th>Id Pedido</th>
				  <th>Fecha</th>
				  <th>Campa�a</th>
				  <th>Estado</th>
				  <th>Total</th>
				</tr>
				<?
				    foreach($pedidos as $i => $data) {
				?>
				<tr <?=($i%2 == 0 ? ' class="alt"' : '')?>>
					<td><a href="<?="/".$confRoot[1]."/bo/pedidos/Ver.php?IdPedido=".$data['IdPedido'];?>"><?=$data['IdPedido'];?></a></td>
					<td><?=date("d/m/Y", strtotime($data['Fecha']));?></td>
					<td><?=$data['Nombre'];?></td>
					<td><?=$data['Estado'];?></td>
					<td>$<?=number_format($data['Total'], 2, '.', ',');?></td>
				</tr>
				<? } ?>				
			  </table>
			  <h2>Ahijados</h2>
			  <table>
			    <tr>
				  <th>Email</th>
				  <th>Estado</th>
				</tr>
				<?
				    foreach($ahijados as $i => $data) {
				?>
				<tr <?=($i%2 == 0 ? ' class="alt"' : '')?>>
				  <td><a href="<?="/".$confRoot[1]."/bo/clientes/Ver.php?IdUsuario=".$data['IdUsuario'];?>"><?=$data['NombreUsuario'];?></a></td>
			      <td><?
			      		if($data["es_activa"]) {
			      			if($data["es_Provisoria"]) {
				      			echo "INACTIVO"; 
			      			} else {
				      			echo "DESUSCRIPTO"; 
			      			}
			      		} else {
			      			echo "ACTIVO";
			      		}
			      	  ?>
			      </td>
				</tr>
				<? } ?>				
			  </table>
			</div>
		</div>
	</body>
</html>
