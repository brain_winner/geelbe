var respuestas = document.getElementsByTagName('tr');
for(i = 0; i < respuestas.length; i++) {
	if(respuestas[i].className == 'respuesta') {
	
		respuestas[i].down().down().style.display = 'none';
		
		id = respuestas[i].id.substr(1);
		
		var pregunta = document.getElementById('m'+id);
		pregunta.addClassName('respondida');
		pregunta.onclick = function() {
			
			var id = this.id.substr(1);
			var row = document.getElementById('p'+id).down().down();
			
			if(row.style.display == 'none') {
				new Effect.BlindDown(row);
			} else {
				new Effect.BlindUp(row);
			}
			
		}
		
	}
}