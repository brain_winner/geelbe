<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("pedidos-bo"));
		$POST = Aplicacion::getIncludes("post", "pedidos");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(3);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
	try {		
		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array('P.IdPedido', 'U.NombreUsuario', 'DU.Apellido', 'DU.Nombre', 'C.Nombre', 'EP.Descripcion', 'FP.Descripcion'));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("P.Fecha", "DESC");

		$count = dmPedidos::getPedidosbyIdUsuarioCount($_GET["IdUsuario"], $paramArray);
		$listPager = new ListadoPaginator($count);
		$rows = dmPedidos::getPedidosbyIdUsuarioPaginados($_GET["IdUsuario"], $paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage()); 
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Pedidos</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
    
		<? Includes::Scripts(); ?>
		<script src="<?=UrlResolver::getJsBaseUrl("bo/clientes/js.js");?>" type="text/javascript"></script>

	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Pedidos</h1>		
			
			<?php
				$parametro = "?IdUsuario=".$_GET["IdUsuario"];
				$listadoPrinter = new ListadoPrinter($rows, "pedidos.php$parametro", $listPager);
				$listadoPrinter->showColumn("P.IdPedido", "Pedido");
				$listadoPrinter->showColumn("U.NombreUsuario", "E-Mail");
				$listadoPrinter->showColumn("DU.Apellido", "Apellido");
				$listadoPrinter->showColumn("DU.Nombre", "Nombre");				
				$listadoPrinter->showColumn("Fecha", "Fecha");
				$listadoPrinter->showColumn("C.Nombre", "Campa�a");
				//$listadoPrinter->showColumn("FechaEnvio", "Fecha de Envio");
				$listadoPrinter->showColumn("EP.Descripcion", "Estado del Pago");
				$listadoPrinter->showColumn("FP.Descripcion", "Forma de Pago");
				$listadoPrinter->showColumn("MontoTotal", "Monto Total");
				$listadoPrinter->showColumn("Acciones");
				
				$listadoPrinter->setSortName("Fecha", "P.Fecha");
				//$listadoPrinter->setSortName("FechaEnvio", "P.FechaEnvio");
				
				$listadoPrinter->setSortColumn("Acciones", false);
				
				$listadoPrinter->setShowFilter("Fecha", false);
				//$listadoPrinter->setShowFilter("FechaEnvio", false);
				$listadoPrinter->setShowFilter("MontoTotal", false);
				$listadoPrinter->setShowFilter("Acciones", false);
				$listadoPrinter->setShowFilter("DU.Apellido", false);
				$listadoPrinter->setShowFilter("DU.Nombre", false);
				
				$listadoPrinter->setFilterBoxWidth("P.IdPedido", "40");
				$listadoPrinter->setFilterBoxWidth("C.Nombre", "75");
				$listadoPrinter->setFilterBoxWidth("EP.Descripcion", "75");
				$listadoPrinter->setFilterBoxWidth("FP.Descripcion", "75");
				
				$listadoPrinter->addButtonToColumn("Acciones", "<select id=\"U[[U.IdUsuario]]P[[P.IdPedido]]\" name=\"menu\" style\"width:40px;\" onchange=\"process_choice(this, [[P.IdPedido]], [[U.IdUsuario]])\">
																	<option value=\"0\" selected>Seleccione:</option>
																	<option value=\"1\">Ver Pedido</option>
																	<option value=\"2\">Editar Pedido</option>
																	<option value=\"3\">Ver Cliente</option>
																</select>");

				
				$listadoPrinter->linkColumn("U.NombreUsuario", "/".$confRoot[1]."/bo/clientes/Ver.php?IdUsuario=[[U.IdUsuario]]", "Ver Cliente");
				$listadoPrinter->linkColumn("P.IdPedido", "/".$confRoot[1]."/bo/pedidos/Ver.php?IdPedido=[[P.IdPedido]]", "Ver Pedido");

				$listadoPrinter->printListado();
			?>
			<script type="text/javascript">

				function process_choice(selection, idPedido, idUsuario) {
					if (selection.value==1) {
						window.location.href="/<?=$confRoot[1]?>/bo/pedidos/Ver.php?IdPedido="+idPedido;
					}
					else if (selection.value==2) {
						window.location.href="/<?=$confRoot[1]?>/bo/pedidos/ABM.php?IdPedido="+idPedido;
					}
					else if (selection.value==3) {
						window.location.href="/<?=$confRoot[1]?>/bo/clientes/Ver.php?IdUsuario="+idUsuario;
					}
					document.getElementById("U"+idUsuario+"P"+idPedido).disabled="disabled";
				}
			</script>
			
		</div>
		
	</body>
</html>