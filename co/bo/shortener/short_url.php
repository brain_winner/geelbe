<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/includes.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/urlshorter/UrlShorterApiCall.php";	

    try {
        ValidarUsuarioLogueadoBo(61);
	} catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Pedidos</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div id="container" class="listadocontainer">
			<h1>Acortar Url</h1>	
			<form name="acortarUrlForm" onsubmit="return chequearForm(this)" method="post">
				<?php
					if (isset($_REQUEST["error_type"]) && $_REQUEST["error_type"] == "invalid_url") {
						echo '<p class="error">Ingrese una URL a acordar v&aacute;lida</p>';
					}
				?>
				<label for="url">Url: </label>
				<input type="text" name="url" id="url" value="<?=$_REQUEST["url"]?>" />
				<br /><br />
				<input type="submit" name="acortarUrlSubmit" value="Acortar" />
				<br /><br />
				
				<?
				if ($_POST) { 
					$UrlCorta = UrlShorterApiCall::getShortUrl($_REQUEST["url"]);
				?>
					<p><b>Url acortada:</b> <?=$UrlCorta?></p>
				<?php
				}
				?>				
			</form>
		</div>
	</body>
	<script type="text/javascript">
		function chequearForm(form) {
			var url = form.url.value;
			if (url == null || url == '' || url == undefined || url == 0) {
				alert("Debe ingresar una URL a acortar");
				return false;
			}

			return true;
		}
	</script>
</html>

