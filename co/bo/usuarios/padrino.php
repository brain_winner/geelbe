<?php
	try {
	    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
		$postURL = Aplicacion::getIncludes("post", "usuarios");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(68);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
    try {
    
    	if(isset($_REQUEST['padrino'])) {
	    	
	    	$desde = '';
	    	if(isset($_REQUEST['desde']) && $_REQUEST['desde'] != '')
	    		$desde .= " AND U.FechaIngreso >= '".$_REQUEST['desde']."'";
	    	if(isset($_REQUEST['hasta']) && $_REQUEST['hasta'] != '')
	    		$desde .= " AND U.FechaIngreso  <= '".$_REQUEST['hasta']."'";
	    	
	    	$oConexion = Conexion::nuevo();
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("SELECT 	U.IdUsuario as 'U.IdUsuario', 
											U.NombreUsuario as 'U.NombreUsuario', 
											D.Nombre as 'D.Nombre',
											D.Apellido as 'D.Apellido', 
											P.Descripcion as 'P.Descripcion', 
											DATE_FORMAT(U.FechaIngreso, '%d/%m/%Y') as 'FormatedFechaIngreso',
											IF(U.es_activa = 0, 'S�', 'No') as 'Activo'
									FROM usuarios U 
									INNER JOIN perfiles P ON U.IdPerfil = P.IdPerfil 
									INNER JOIN datosusuariospersonales D ON U.IdUsuario = D.IdUsuario 
									WHERE U.Padrino = '".mysql_real_escape_string($_REQUEST['padrino'])."'
									".$desde."  
									ORDER BY U.NombreUsuario");
									
            $Tabla = $oConexion->DevolverQuery();
            $oConexion->Cerrar_Trans();
            
            if(isset($_GET['csv'])) {
            
            	header('Pragma: public'); 	// required
				header('Expires: 0');		// no cache
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($file_name)).' GMT');
				header('Cache-Control: private',false);
				header('Content-Type: '.$mime);
				header('Content-Disposition: attachment; filename="usuarios.csv"');
				header('Content-Transfer-Encoding: binary');
				header('Connection: close');
	            
	            echo "IdUsuario,NombreUsuario,Nombre,Apellido,Perfil,FechaIngreso,Activo\n";
	            foreach($Tabla as $datos)
	            	echo $datos['U.IdUsuario'].','.$datos['U.NombreUsuario'].','.$datos['D.Nombre'].','.$datos['D.Apellido'].','.$datos['P.Descripcion'].','.$datos['FormatedFechaIngreso'].",".$datos['Activo']."\n";
	            	
	            die;
            }
	    	
    	}
		
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Hist�rico Login</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
		<link href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Usuarios</h1>	

			<form action="padrino.php" method="post" id="mainStats">
				<table style="margin-bottom:10px;">
					<tr class="alt">
						<td style="text-align:left;font-weight:bold;">
							Padrino: <input type="text" name="padrino" value="<?=$_POST['padrino']?>" />							
							Registro desde: <input type="text" name="desde" value="<?=$_POST['desde']?>" class="desde" />							
							hasta: <input type="text" name="hasta" value="<?=$_POST['hasta']?>" class="hasta" />
							<input type="submit" value="Enviar" />					
						</td>
					</tr>
				</table>
			</form>
			
			<?php if(isset($Tabla)): ?>
			<a href="padrino.php?padrino=<?=$_REQUEST['padrino']?>&desde=<?=$_REQUEST['desde']?>&hasta=<?=$_REQUEST['hasta']?>&csv">Descargar CSV</a><br /><br />
			<table>
				<tr>
					<th>E-Mail</th>
					<th>Apellido</th>
					<th>Nombre</th>
					<th>Perfil</th>
					<th>Fecha de ingreso</th>
					<th>Activo</th>
				</tr>
				<?php foreach($Tabla as $i => $data): ?>
				<tr <?=($i%2==1 ? 'class="alt"' : '')?>>
					<td><?=$data['U.NombreUsuario']?></td>
					<td><?=$data['D.Apellido']?></td>
					<td><?=$data['D.Nombre']?></td>
					<td><?=$data['P.Descripcion']?></td>
					<td><?=$data['FormatedFechaIngreso']?></td>
					<td><?=$data['Activo']?></td>
				</tr>
				<?php endforeach; ?>
			</table>
			<?php endif; ?>
		</div>
		
		<script>
			$(function($){
			    $.datepicker.regional['es'] = {
			        closeText: 'Cerrar',
			        prevText: '<Ant',
			        nextText: 'Sig>',
			        currentText: 'Hoy',
			        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
			        dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'S�bado'],
			        dayNamesShort: ['Dom','Lun','Mar','Mi�','Juv','Vie','S�b'],
			        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S�'],
			        weekHeader: 'Sm',
			        dateFormat: 'dd/mm/yy',
			        firstDay: 1,
			        isRTL: false,
			        showMonthAfterYear: false,
			        yearSuffix: ''
			    };
			    $.datepicker.setDefaults($.datepicker.regional['es']);
			    $('.desde, .hasta').datepicker({ dateFormat: "yy-mm-dd" })
			});

		</script>
		
	</body>
</html>
