<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
    try
    {
        ValidarUsuarioLogueadoBo(1);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $objUsuario = dmUsuario::getByIdUsuario((isset($_GET["IdUsuario"])) ? $_GET["IdUsuario"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Usuarios</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/usuarios/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <table>
        <tr>
            <td>Nombre</td>
            <td><b><?=$objUsuario->getDatos()->getNombre()?></b></td>
        </tr>
        <tr>
            <td>Apellido</td>
            <td><b><?=$objUsuario->getDatos()->getApellido()?></b></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><b><?=$objUsuario->getNombreUsuario()?></b></td>
        </tr>
        <tr>
            <td>Perfil</td>
            <td><b>
                <?$objPerfil = dmUsuario::getPerfilesById($objUsuario->getIdPerfil());?>
                <?=$objPerfil->getDescripcion();?>
               </b>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
            <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnVolver','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver.gif");?>" alt="Volver" name="btnVolver" id="btnVolver" border="0"/></a>
            </td>
        </tr>
    </table>
</body>
</html>