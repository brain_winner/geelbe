function ErrorMSJ(code, def)
{
    if(arrErrores["USUARIOS"][code] != undefined)
        document.getElementById("msjError").innerHTML = arrErrores["USUARIOS"][code];
    else
        document.getElementById("msjError").innerHTML = code;
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtNombre.value.length == 0)
        {
            arrError.push(arrErrores["USUARIOS"]["NOMBRE"]);
        }
        if(frm.txtApellido.value.length == 0)
        {
            arrError.push(arrErrores["USUARIOS"]["APELLIDO"]);
        }
        if(!ValidarEmail(frm.txtEmail.value))
        {
            arrError.push(arrErrores["USUARIOS"]["MAIL_INCORRECTO"]);
        }
        if(frm.txtClave.value.length < 6 || frm.txtClave.value.length > 16)
        {
            arrError.push(arrErrores["USUARIOS"]["CLAVE_INCORRECTO"]);
        }
        if(frm.txtClave.value != frm.txtClaveRepetir.value)
        {
            arrError.push(arrErrores["USUARIOS"]["CLAVE_DIFERENTES"]);
        }
         if(frm.cbxPerfil.value == 0)
        {
            arrError.push(arrErrores["USUARIOS"]["PERFIL"]);
        }
        if (arrError.length>0)
        {
            var error = ""
            for(i=0; i<arrError.length; i++)
            {
                error +="*" + arrError[i] + "\n";
            }
            alert(error);
            return false;
        }
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}
function ValidarFormEditado(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtNombre.value.length == 0)
        {
            arrError.push(arrErrores["USUARIOS"]["NOMBRE"]);
        }
        if(frm.txtApellido.value.length == 0)
        {
            arrError.push(arrErrores["USUARIOS"]["APELLIDO"]);
        }
        if(!ValidarEmail(frm.txtEmail.value))
        {
            arrError.push(arrErrores["USUARIOS"]["MAIL_INCORRECTO"]);
        }
        if(frm.cbxPerfil.value == 0)
        {
            arrError.push(arrErrores["USUARIOS"]["PERFIL"]);
        }
        if (arrError.length>0)
        {
            var error = ""
            for(i=0; i<arrError.length; i++)
            {
                error +="*" + arrError[i] + "\n";
            }
            alert(error);
            return false;
        }
        frm.submit(); 
        alert ("Los datos han sido modificados correctamente.");
    }
    catch(e)
    {
        throw e;
    }
}
function ValidarFormPass(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtClave.value.length < 6 || frm.txtClave.value.length > 16)
        {
            arrError.push(arrErrores["USUARIOS"]["CLAVE_INCORRECTO"]);
        }
        if(frm.txtClave.value != frm.txtClaveRepetir.value)
        {
            arrError.push(arrErrores["USUARIOS"]["CLAVE_DIFERENTES"]);
        }
        if (arrError.length>0)
        {
            var error = ""
            for(i=0; i<arrError.length; i++)
            {
                error +="*" + arrError[i] + "\n";
            }
            alert(error);
            return false;
        }
        frm.submit();
        
        return true;
    }
    catch(e)
    {
        throw e;
    }
}
function irVolver()
{
    try
    {
        //window.location.href="index.php";
        history.back(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["USUARIOS"]["SELECCIONE"]);
        else
            window.location.href="ABM_editar.php?IdUsuario="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVer()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["USUARIOS"]["SELECCIONE"]);
        else
            window.location.href="Ver.php?IdUsuario="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar(id)
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["USUARIOS"]["SELECCIONE"]);
        else
        {
            if(confirm(arrErrores["USUARIOS"]["ELIMINAR"]))
                window.location.href="../../logica_bo/usuarios/actionform/eliminar_usuario.php?IdUsuario="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}