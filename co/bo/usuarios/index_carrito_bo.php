<?php
	try {
	    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
		$postURL = Aplicacion::getIncludes("post", "usuarios");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(111);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
    try {
		if(isset($_GET['IdUsuario'], $_GET['do']) && is_numeric($_GET['IdUsuario'])) {
		
			$oConexion = Conexion::nuevo();
			$oConexion->Abrir_Trans();
			if($_GET['do'] == 'save')
				$oConexion->setQuery("INSERT INTO usuarios_carrito_bo VALUES (".$_GET['IdUsuario'].")");
			else
				$oConexion->setQuery("DELETE FROM usuarios_carrito_bo WHERE IdUsuario = ".$_GET['IdUsuario']);
	        $oConexion->EjecutarQuery();
	        $oConexion->Cerrar_Trans();
	    }


		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array('U.NombreUsuario', 'D.Apellido', 'D.Nombre'));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("D.Apellido", "ASC");
	
	    $count = dmUsuario::getUsuariosCount($paramArray);
		$listPager = new ListadoPaginator($count);
		$rows = dmUsuario::getUsuariosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());
		
	    
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Histórico Login</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Configurar Usuarios</h1>	

			<div id="mainStats">
				<table style="margin-bottom:10px;">
					<tr class="alt">
						<td style="text-align:left;font-weight:bold;">Crear: <a href="/<?=$confRoot[1];?>/bo/usuarios/ABM.php">Nuevo Usuario</a></td>
					</tr>
				</table>
			</div>
			
			<?php
				$listadoPrinter = new ListadoPrinter($rows, "index_carrito_bo.php", $listPager);
				$listadoPrinter->showColumn("U.NombreUsuario", "E-Mail");
				$listadoPrinter->showColumn("D.Apellido", "Apellido");
				$listadoPrinter->showColumn("D.Nombre", "Nombre");				
				$listadoPrinter->showColumn("CarritoBO", "Acceso al carrito");
				$listadoPrinter->showColumn("Acciones", "Acciones");

				
				$listadoPrinter->setShowFilter("CarritoBO", false);
				$listadoPrinter->setShowFilter("Acciones", false);
				
				$listadoPrinter->setSortColumn("Acciones", false);

				$listadoPrinter->addButtonToColumn("Acciones", "<input type=\"button\" class=\"bloquear\" value=\"\" data-id=\"[[U.IdUsuario]]\" >");
				
				$listadoPrinter->printListado();
			?>
			<script src="http://code.jquery.com/jquery.js"></script>
			<script type="text/javascript">
				$('input.bloquear').each(function(k, el) {
					if($(el).parent().prev().html() == 'No')
						el.value = 'Permitir acceso';
					else
						el.value = 'Bloquear acceso';
						
					$(el).click(function() {
						location.href = 'index_carrito_bo.php?IdUsuario='+$(this).data('id')+'&do='+(this.value == 'Permitir acceso' ? 'save' : 'delete');
					})
				})
			</script>
		</div>
		
	</body>
</html>
