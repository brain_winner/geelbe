<?php
	try {
	    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
		$postURL = Aplicacion::getIncludes("post", "usuarios");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(1);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
    try {
		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array('U.NombreUsuario', 'D.Apellido', 'D.Nombre'));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("D.Apellido", "ASC");
	
	    $count = dmUsuario::getUsuariosCount($paramArray);
		$listPager = new ListadoPaginator($count);
		$rows = dmUsuario::getUsuariosPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage()); 
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Histórico Login</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Configurar Usuarios</h1>	

			<div id="mainStats">
				<table style="margin-bottom:10px;">
					<tr class="alt">
						<td style="text-align:left;font-weight:bold;">Crear: <a href="/<?=$confRoot[1];?>/bo/usuarios/ABM.php">Nuevo Usuario</a></td>
					</tr>
				</table>
			</div>
			
			<?php
				$listadoPrinter = new ListadoPrinter($rows, "index.php", $listPager);
				$listadoPrinter->showColumn("U.NombreUsuario", "E-Mail");
				$listadoPrinter->showColumn("D.Apellido", "Apellido");
				$listadoPrinter->showColumn("D.Nombre", "Nombre");				
				$listadoPrinter->showColumn("P.Descripcion", "Perfil");
				$listadoPrinter->showColumn("FormatedFechaIngreso", "Fecha de ingreso");
				$listadoPrinter->showColumn("Acciones", "Acciones");

				
				$listadoPrinter->setShowFilter("FormatedFechaIngreso", false);
				$listadoPrinter->setShowFilter("P.Descripcion", false);
				$listadoPrinter->setShowFilter("Acciones", false);
				
				$listadoPrinter->setSortColumn("P.Descripcion", false);
				$listadoPrinter->setSortColumn("Acciones", false);

				$listadoPrinter->setSortName("FormatedFechaIngreso", "U.FechaIngreso");
				
				$listadoPrinter->addButtonToColumn("Acciones", "<select id=\"U[[U.IdUsuario]]\" name=\"menu\" style\"width:40px;\" onchange=\"process_choice(this, [[U.IdUsuario]])\">
																	<option value=\"0\" selected>Seleccione:</option>
																	<option value=\"1\">Editar Usuario</option>
																	<option value=\"2\">Eliminar Usuario</option>
																</select>");
				
				$listadoPrinter->printListado();
			?>
			<script type="text/javascript">

				function process_choice(selection, idUsuario) {
					if (selection.value==1) {
						window.location.href="/<?=$confRoot[1]?>/bo/usuarios/ABM_editar.php?IdUsuario="+idUsuario;
					}
					else if (selection.value==2) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/usuarios/actionform/eliminar_usuario.php?IdUsuario="+idUsuario;
					}
					document.getElementById("U"+idUsuario).disabled="disabled";
				}
			</script>
		</div>
		
	</body>
</html>
