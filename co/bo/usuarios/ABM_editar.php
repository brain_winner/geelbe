<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
    try
    {
        ValidarUsuarioLogueadoBo(1);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "usuarios");
    $objUsuario = dmUsuario::getByIdUsuario((isset($_GET["IdUsuario"])) ? $_GET["IdUsuario"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Usuarios</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/usuarios/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMUsuarios" name="frmABMUsuarios" target="iUsuarios" action="../../../<?=Aplicacion::getDirLocal().$postURL["usuarios"]?>" method="POST" onSubmit="ValidarFormEditado(this);">
        <input type="hidden" id="hTitulo" name="hTitulo" value="Usuarios">
        <input type="hidden" id="txtIdUsuario" name="txtIdUsuario" value="<?=$objUsuario->getIdUsuario()?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <tr>
                <td>Nombre</td>
                <td><input type="text" id="txtNombre" name="txtNombre" maxlength="40" value="<?=$objUsuario->getDatos()->getNombre()?>"></td>
            </tr>
            <tr>
                <td>Apellido</td>
                <td><input type="text" id="txtApellido" name="txtApellido" maxlength="40" value="<?=$objUsuario->getDatos()->getApellido()?>"></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><input type="text" id="txtEmail" name="txtEmail" maxlength="80" value="<?=$objUsuario->getNombreUsuario()?>"></td>
            </tr>
            <tr>
                <td>Perfil</td>
                <td>
                    <select id="cbxPerfil" name="cbxPerfil">
                        <option value=0>...seleccionar...</option>
                        <?
                          $collPerfiles = dmUsuario::getPerfiles();
                          foreach ($collPerfiles as $objPerfil)
                          {
                            ?><option value="<?=$objPerfil->getIdPerfil()?>"><?=$objPerfil->getDescripcion()?></option><?
                          }
                        ?>
                    </select>
                    <script>
                        document.getElementById("cbxPerfil").value = value="<?=$objUsuario->getIdPerfil()?>";
                    </script>
                </td>
            </tr>
            <tr>
	            <td>
	            	<a href="<?=UrlResolver::getBaseUrl("bo/usuarios/ABM_pass.php?IdUsuario=".$_GET["IdUsuario"]);?>" >Cambiar contraseņa</a>
	            </td>
            <tr>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
    <iframe style="display:none" id="iUsuarios" name="iUsuarios"></iframe>
</body>
</html>
