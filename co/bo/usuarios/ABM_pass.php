<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
    try
    {
        ValidarUsuarioLogueadoBo(1);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "usuarios");
    $objUsuario = dmUsuario::getByIdUsuario((isset($_GET["IdUsuario"])) ? $_GET["IdUsuario"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Usuarios</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/usuarios/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMUsuarios" name="frmABMUsuarios" action="/<?=$confRoot[1]?>/logica_bo/usuarios/actionform/changepass.php" method="POST" onSubmit="ValidarFormPass(this);">
        <input type="hidden" id="txtIdUsuario" name="txtIdUsuario" value="<?=$objUsuario->getIdUsuario()?>">
        <input type="hidden" id="txtIdUsuarioAdm" name="txtIdUsuarioAdm" value="<?=$objUsuario->getIdUsuario()?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <tr>
                <td>Contrase&ntilde;a</td>
                <td><input type="password" id="txtClave" name="txtClave" maxlength="16"  value=""/></td>
            </tr>
            <tr>
                <td>Repetir contrase&ntilde;a</td>
                <td><input type="password" id="txtClaveRepetir" name="txtClaveRepetir" maxlength="16" value=""></td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
