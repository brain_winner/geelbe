<?  
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("dhl"));
    try
    {
        ValidarUsuarioLogueadoBo(5);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "dhl");
    $peso = ((isset($_GET["IdPeso"])) ? $_GET["IdPeso"]:0);
    $zona = ((isset($_GET["IdZona"])) ? $_GET["IdZona"]:0);
    $provincia = ((isset($_GET["IdCiudad"])) ? $_GET["IdCiudad"]:0);
    $oDHL = dmDHL::getByPesoZona($peso, $zona, $provincia);
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Envio</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/dhl/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMLocales" name="frmABMLocales" method="post" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type='hidden' name='txtPeso' id='txtPeso' value='<?=$oDHL[0]["idpeso"]?>' />
        <input type='hidden' name='txtZona' id='txtZona' value='<?=$oDHL[0]["idzona"]?>' />
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["dhl"]);
            ?>
            <tr>
                <td>Zona: </td>
                <td><?=$oDHL[0]["zona"]?></td>
            </tr>
            <tr>
                <td>Provincia: </td>
                <td><?=$oDHL[0]["ciudad"]?></td>
            </tr>
            <tr>
                <td>Desde: </td>
                <td><?=$oDHL[0]["Desde"]?></td>
            </tr>
            <tr>
                <td>Hasta: </td>
                <td><?=$oDHL[0]["Hasta"]?></td>
            </tr>
            <tr>
                <td>Tarifa x Kilo</td>
                <td><input type='text' name='txtTarifaKG' id='txtTarifaKG' value='<?=$oDHL[0]["TarifaKG"]?>' /></td>
            </tr>
            <tr>
                <td>Tarifa</td>
                <td><input type='text' name='txtTarifa' id='txtTarifa' value='<?=$oDHL[0]["Tarifa"]?>' /></td>
            </tr>
            
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>