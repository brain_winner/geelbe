<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("dhl"));
		$postURL = Aplicacion::getIncludes("post", "dhl");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(5);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }	
	try {
		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array('p.Descripcion'));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("p.Descripcion", "ASC");

		$count = dmDHL::getAllDHLCount($paramArray);
		$listPager = new ListadoPaginator($count);
		$rows = dmDHL::getAllDHLPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
	
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Envio</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>
	

	<body>

		<div id="container" class="listadocontainer">
			<h1>Envio</h1>	
			
			<?php
				$listadoPrinter = new ListadoPrinter($rows, "index.php", $listPager);
				$listadoPrinter->showColumn("p.Descripcion", "Descripcion");
				$listadoPrinter->showColumn("dp.Desde", "Desde");
				$listadoPrinter->showColumn("dp.Hasta", "Hasta");
				$listadoPrinter->showColumn("dpr.Tarifa", "Tarifa");
				$listadoPrinter->showColumn("dpr.TarifaKG", "Tarifa en KG");
				
				$listadoPrinter->showColumn("Acciones");

				$listadoPrinter->setShowFilter("dp.Desde", false);
				$listadoPrinter->setShowFilter("dp.Hasta", false);
				$listadoPrinter->setShowFilter("dpr.Tarifa", false);
				$listadoPrinter->setShowFilter("dpr.TarifaKG", false);
				$listadoPrinter->setShowFilter("Acciones", false);
				
				$listadoPrinter->setSortColumn("Acciones", false);
				
				$listadoPrinter->addButtonToColumn("Acciones", "<a href=\"ABM.php?IdPeso=[[dpr.IdPeso]]&IdZona=[[dpr.IdZona]]&IdCiudad=[[p.IdCiudad]]\">Editar</a>");

				$listadoPrinter->printListado();
			?>
			
		</div>	
	</body>
</html>
