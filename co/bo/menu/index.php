<?

	$secciones=$_SESSION['BO']['User']['secciones'];
	$access = array();
	foreach($secciones as $secc)
		$access[] = $secc['idSeccion'];
		
	$oConexion = Conexion::nuevo();
	$oConexion->setQuery("SELECT s.*, sp.seccion as padre FROM secciones s, seccionesPadre sp WHERE sp.idSeccionPadre = s.idSeccionPadre ORDER BY s.idSeccionPadre, s.idSeccion");                
	$secciones = $oConexion->DevolverQuery();
	$oConexion->Cerrar();
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Menu</title>
<link href="<?=UrlResolver::getCssBaseUrl("bo/menu/css.css")?>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("bo/menu/js.js");?>"></script>
</head>
<body>
<table cellpadding="0" cellspacing="0" class="contenido">
	
	

    <tr>
        <td align="center">
        <table cellpadding="0" cellspacing="0"> 
            <?php
            
	         	$padre = null;
	         	foreach($secciones as $i => $s) {
	         		
	         		if(!in_array($s['idSeccion'], $access))
	         			continue;
	         		            		
	         		if($padre == null || $padre != $s['idSeccionPadre']) {
	
						  	echo '
						  	<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="td_principal">'. $s['padre'] .'</td>
							</tr>
							';
	       			
	         			$padre = $s['idSeccionPadre'];
	         		}
	         		
	         		echo '<tr>
							<td class="td_contenido"><a href="'.$s['href'].'" target="iPrincipal">'. $s['seccion'] .'</a></td>
						</tr>';   		
	         		
	         	}
            
           	?>
				<tr>                                                                                 
				<td >&nbsp;</td>
				</tr>
				<tr>
					<td class="td_contenido" id="Salir" name="Salir" onclick="irLocacion('login/logout.php', document.getElementById('iPrincipal'), this.id)" title="Salir del Sistema">SALIR</td>
				</tr>                       
				<tr>                                                                                 
					<td>&nbsp;</td>
				</tr>
        </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;
        </td>
    </tr>
</table>
</body>
</html>
