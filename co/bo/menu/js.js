var MenuSeleccionado="Salir";
function clickMenu(id)
{
    try
    {
        document.getElementById(MenuSeleccionado).className="td_contenido";
        document.getElementById(id).className="td_seleccionada";
        MenuSeleccionado=id;
    }
    catch(e)
    {
        throw e;
    }
}
function irLocacion(url, iframe, id)
{
    try
    {
        clickMenu(id);
        iframe.src=url;
    }
    catch(e)
    {
        throw e;
    }
}