<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("landings"));
    try
    {
        ValidarUsuarioLogueadoBo(33);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
    
    if(isset($_GET['download'], $_GET['IdLanding']) && is_numeric($_GET['IdLanding'])) {
    	
    	// Set headers
	    header("Cache-Control: public");
	    header("Content-Description: File Transfer");
	    header("Content-Disposition: attachment; filename=landing.html");
	    header("Content-Type: text/html");
	    header("Content-Transfer-Encoding: binary");
    	echo file_get_contents('http://'.$_SERVER['HTTP_HOST'].'/co/registro/marcas.php?id='.$_GET['IdLanding']);
    	die;
    }
    
    $postURL = Aplicacion::getIncludes("post", "landings");
    $dtLocales = dmLandings::getAll((isset($_GET["columna"])?$_GET["columna"]:0)+1, (isset($_GET["sentido"])?$_GET["sentido"]:"DESC"), (isset($_GET["q"])?$_GET["q"]:""));
    $objDT = new Tabla($dtLocales, (isset($_GET["tp"])?$_GET["tp"]:10));
    $objDT->setPalabraClave($_GET["q"]);
    $objDT->setPagina("index.php");
    $objDT->setPaginaNro((isset($_GET["irPagina"])?$_GET["irPagina"]:0));
    $objDT->setColumnaNro((isset($_GET["columna"])?$_GET["columna"]:0));
    $objDT->setColumnaSentido((isset($_GET["sentido"])?$_GET["sentido"]:"DESC"));
    $objDT->addBotones('<a href="javascript:irEditar();" ><img src="'.UrlResolver::getImgBaseUrl("bo/imagenes/btnEditarMini.gif").'" alt="Editar" name="btnEditar" id="btnEditar" border="0"/></a>');
    $objDT->addBotones('<a href="javascript:irVer();" ><img src="'.UrlResolver::getImgBaseUrl("bo/imagenes/btnVerMini.gif").'" alt="Ver" name="btnVer" id="btnVer" border="0"/></a>');
    $objDT->addBotones('<a href="javascript:irLinks();" ><img src="'.UrlResolver::getImgBaseUrl("bo/imagenes/link_ico.png").'" alt="Links" name="btnDownload" id="btnDownload" border="0"/></a>');
    $objDT->addBotones('<a href="javascript:irDownload();" ><img src="'.UrlResolver::getImgBaseUrl("bo/imagenes/btnDownload.gif").'" alt="Descargar" name="btnDownload" id="btnDownload" border="0"/></a>');
    $objDT->addBotones('<a href="javascript:irEliminar();" ><img src="'.UrlResolver::getImgBaseUrl("bo/imagenes/btnEliminarMini.gif").'"  alt="Eliminar" name="btnEliminar" id="btnEliminar" border="0"/></a>&nbsp;');

    $objDT->setPropiedadesColumna(0, array("style=\"display:none\""));
    $objDT->setPropiedadesColumna(2, array("style=\"display:none\""));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> Landings</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/landings/js.js");?>" type="text/javascript"></script>
    <link href="<?=UrlResolver::getCssBaseUrl("bo/style/css.css")?>" rel="stylesheet" type="text/css" />
</head>
<body>
<table style="width:100%; height:100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="borde_top">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="borde_bottom ">
            <a href="javascript:irNuevo();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnNuevo','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnNuevo_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnNuevo.gif");?>" alt="Nuevo" name="btnNuevo" id="btnNuevo" border="0"/></a>          
        </td>
    </tr>
    <tr>
        <td colspan="100%">
            <?
            $objDT->show();
            ?>
        </td>
    </tr>
</table>
</body>
</html>
