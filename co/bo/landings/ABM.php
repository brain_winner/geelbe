<?  
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("landings"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
   try
    {
        ValidarUsuarioLogueadoBo(33);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "landings");
    $oLocal = dmLandings::getById((isset($_GET["IdLanding"])) ? $_GET["IdLanding"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Landings</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/landings/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMLocales" name="frmABMLocales" method="post" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="idLanding" name="idLanding" value="<?=$oLocal->getIdLanding()?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["landings"]);
            ?>
            <tr>
                <td>Nombre</td>
                <td><input type="text" id="txtNombre" name="txtNombre" maxlength="80" size=60 value="<?=$oLocal->getNombre()?>" /></td>
            </tr>
            <tr>
                <td>Texto</td>
                <td><textarea id="txtTexto" name="txtTexto" cols="60" rows="10"><?=$oLocal->getTexto()?></textarea></td>
            </tr>
            
            <tr>
                <td>Logo </td>
                <td>
                	<input type="file" name="logo" size="60" />
                </td>
            </tr>
            
            <tr>
                <td>Im&aacute;genes (270x165)</td>
                <td>
                	<input type="file" name="imgs[0]" size="60" /><br />
                	<input type="file" name="imgs[1]" size="60" /><br />
                	<input type="file" name="imgs[2]" size="60" /><br />
                	<input type="file" name="imgs[3]" size="60" /><br />
                </td>
            </tr>
            
            <tr>
            	<td valign="top">Campa&ntilde;a</td>
            	<td>
            		<table>
            			<tr>
            				<th></th>
            				<th>Campa&ntilde;a</th>
            				<th>Estado</th>
            			</tr>
            			<?php
            				if(is_numeric($oLocal->getIdLanding())) {
            					$oConexion = Conexion::nuevo();
					            $oConexion->Abrir();
						        $oConexion->setQuery('SELECT IdCampania FROM landings_x_campanias WHERE IdLanding = '.$oLocal->getIdLanding());
						        $Tabla = $oConexion->DevolverQuery();
            				}
            				
            				$camps = dmCampania::getCampanias('Nombre', 'ASC');
            				foreach($camps as $i => $c) {
            					if(in_array($c['Estado'], array('Abierta', 'Proxima')))
            						echo '<tr>
            							<td><input '.($Tabla[0]['IdCampania'] == $c['IdCampania'] ? 'checked="checked"' : '').' type="radio" name="cid" value="'.$c['IdCampania'].'" id="c'.$c['IdCampania'].'" /></td>
            							<td><label for="c'.$c['IdCampania'].'">'.$c['Nombre'].'</label></td>
            							<td>'.$c['Estado'].'</td>
            						</tr>';
            				}
            			?>
            		</table>
            	</td>
            </tr>
            
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src=<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?> alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>