<?
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("landings"));
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("partnerscod"));
	try {
		ValidarUsuarioLogueadoBo(33);
	}
	catch(ACCESOException $e) {
	?>
		<script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
	<?
		exit;
	}
	$postURL = Aplicacion::getIncludes("post", "landings");
	$oLocal = dmLandings::getById((isset($_GET["IdLanding"])) ? $_GET["IdLanding"]:0);
	$dtLocales = dmPartnersCod::getAll((isset($_GET["columna"])?$_GET["columna"]:0)+1, (isset($_GET["sentido"])?$_GET["sentido"]:"DESC"), (isset($_GET["q"])?$_GET["q"]:""));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> ABM Landings</title>
		<? Includes::Scripts(); ?>
		<script src="<?=UrlResolver::getJsBaseUrl("bo/landings/js.js");?>" type="text/javascript"></script>
	</head>

	<body>
		<form id="frmABMLocales" name="frmABMLocales" method="post" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
    		<input type="hidden" id="idLanding" name="idLanding" value="<?=$oLocal->getIdLanding()?>">
			<table>
				<tr><td colspan=2><span id="msjError" name="msjError"></span></td></tr>
				<?
					require_once("../../../".Aplicacion::getDirLocal().$postURL["landings"]);
				?>
				<tr>
                	<td>C&oacute;digo</td>
                	<td>
                		<select id="codigo">
                			<option value="x">Seleciona un c&oacute;digo</option>
                			<?php 
                				foreach($dtLocales as $i => $cod) {
                					echo '<option value="'.$cod['Cantidad'].'">'.$cod['Codigo'].'</option>';
                				}
                			?>
                		</select>
                	</td>
                </tr>
                
                <tr>
                	<td valign="top">Links</td>
                	<td id="links" style="padding: 10px; background-color: #EAEAEA">
                </td>
			
			</tr>
            
            <tr>
                <td colspan=2 align=center>
            			<a  href="javascript:irVolver();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
		</table>
		
<script type="text/javascript">
	document.getElementById('codigo').onchange = function() {
		if(this.selectedIndex != 0) {
			var cant = this.options[this.selectedIndex].value;
			var cod = this.options[this.selectedIndex].innerHTML;

			document.getElementById('links').innerHTML = '';
			for(i = 1; i <= cant; i++) {
				document.getElementById('links').innerHTML += '<?=UrlResolver::getBaseUrl("registro/marca/marca_index.php")?>?id=<?=$_GET['IdLanding'];?>&codact='+cod+i+'<br />';
			}
        }
	}
</script>

		</form>
	</body>
</html>