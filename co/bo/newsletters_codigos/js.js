function ValidarForm(frm)
{
    if (frm.txtNombre.length == 0)
    {
        return false;                                                                  
    }
    frm.submit();
}
function irVolver()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"newsletters_codigos/index.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar()
{
	try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="../../logica_bo/newsletters_codigos/actionform/eliminar_codigo.php?IdCodigo="+dt_getCeldaSeleccionada(0);
        
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="ABM.php?IdCodigo="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irLinks()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.open("links.php?IdCodigo="+dt_getCeldaSeleccionada(0), '', 'width=600,height=530');
    }
    catch(e)
    {
        throw e;
    }
}