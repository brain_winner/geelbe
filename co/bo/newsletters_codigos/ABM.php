<?  
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("newsletters_codigos"));
    try
    {
        ValidarUsuarioLogueadoBo(4);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "newsletters_codigos");
    $oLocal = dmNewsletterCodigos::getById((isset($_GET["IdCodigo"])) ? $_GET["IdCodigo"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Newsletter C�digos</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/newsletters_codigos/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMLocales" name="frmABMLocales" method="post" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="idCodigo" name="idCodigo" value="<?=$oLocal->getIdCodigo()?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["newsletters_codigos"]);
            ?>
            <tr>
                <td>Nombre</td>
                <td><input type="text" id="txtNombre" name="txtNombre" maxlength="80" size=60 value="<?=$oLocal->getNombre()?>"></td>
            </tr>
            
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>