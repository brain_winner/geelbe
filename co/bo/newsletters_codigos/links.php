<?  
	if(!isset($_GET['IdCodigo'])) die;
	
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("newsletters_codigos"));
    try
    {
        ValidarUsuarioLogueadoBo(4);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "newsletters_codigos");
    $oLocal = dmNewsletterCodigos::getLinks((isset($_GET["IdCodigo"])) ? $_GET["IdCodigo"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Newsletter C&oacute;digos</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/newsletters_codigos/js.js");?>" type="text/javascript"></script>
	<link href="<?=UrlResolver::getCssBaseUrl("bo/newsletters_codigos/css/global.css")?>" rel="stylesheet" type="text/css" />
	<style type="text/css">
		table,
		input {
			width: 100% !important;
		}
		
		#container {
			width: auto;
			padding: 10px;
		}
		
		.visitas {
			text-align: center;
			width: 100px
		}
		
		.campo {
			width: 200px
		}
	</style>
</head>
<body>
    <form id="container" name="frmABMLocales" method="post" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <table>
            <tr>
                <th>Campo</th>
                <th>Visitas</th>
                <th>Link</th>
            </tr>
          	<?php 
          		$out = array();
          		
          		$n = 0;
          		foreach($oLocal as $i => $row):
          		
          			//Remove duplicates
          			if(in_array($row['IdCampo'], $out))
          				continue;
          				
          			$out[] = $row['IdCampo'];
          			$n++;
          			
          		?>
            <tr<?=($n%2 ? ' class="alt"' : '');?>>
                <td class="campo"><?=$row['nombre'];?></td>
                <td class="visitas"><?=(is_null($row['visitas']) ? '0' : $row['visitas']);?></td>
                <td><input type="text" readonly="readonly" value="<?=UrlResolver::getBaseUrl("logica/autologin/login.php");?>?i=[[[id]]]&amp;h=[[[hash]]]&amp;cod=<?=$_GET['IdCodigo'];?>&amp;campo=<?=$row['IdCampo'];?>&amp;w=1" /></td>
            </tr>
            <?php endforeach; ?>            
        </table>
    </form>
</body>
</html>