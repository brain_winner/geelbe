<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("ordenamientos"));
    try
    {
        ValidarUsuarioLogueadoBo(60);
    }
    catch(ACCESOException $e)
    {
    ?>
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
    
    $campanias = false;
    
    if(isset($_POST['dia'], $_POST['mes'], $_POST['anno'])) {
    
    	$campanias = true;
    	$time = mktime(0, 0, 0, $_POST['mes'], $_POST['dia'], $_POST['anno']);
    
		$oConexion = Conexion::nuevo();  		
	    $oConexion->Abrir();
		$oConexion->setQuery("SET NAMES utf8");
		$oConexion->EjecutarQuery();
	    $oConexion->setQuery("SELECT IdCampania, Nombre, Descripcion AS Slogan, FechaInicio, FechaFin FROM campanias 
		WHERE FechaInicio LIKE '".date("Y-m-d", $time)."%'");
	    $inician = $oConexion->DevolverQuery();
	    $oConexion->Cerrar();
	    
	    $oConexion->Abrir();
		$oConexion->setQuery("SET NAMES utf8");
		$oConexion->EjecutarQuery();
	    $oConexion->setQuery("SELECT IdCampania, Nombre, Descripcion AS Slogan, FechaInicio, FechaFin FROM campanias 
		WHERE FechaFin LIKE '".date("Y-m-d", $time)."%'");
	    $finalizan = $oConexion->DevolverQuery();
	    $oConexion->Cerrar();
	
	    $oConexion->Abrir();
		$oConexion->setQuery("SET NAMES utf8");
		$oConexion->EjecutarQuery();
	    $oConexion->setQuery("SELECT IdCampania, Nombre, Descripcion AS Slogan, FechaInicio, FechaFin FROM campanias 
		WHERE FechaInicio >= '".date("Y-m-d", $time + 24*60*60)."%'");
	    $proximamente = $oConexion->DevolverQuery();
	    $oConexion->Cerrar();
	
	    $oConexion->Abrir();
		$oConexion->setQuery("SET NAMES utf8");
		$oConexion->EjecutarQuery();
	    $oConexion->setQuery("SELECT IdCampania, Nombre, Descripcion AS Slogan, FechaInicio, FechaFin FROM campanias 
		WHERE FechaInicio <= '".date("Y-m-d", $time)."%' AND FechaFin >= '".date("Y-m-d", $time)."%'");
	    $abiertas = $oConexion->DevolverQuery();
	    $oConexion->Cerrar();
	    
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> E-Mails de campa�as</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/campanias/js.js");?>" type="text/javascript"></script>
    <script src="../../logica_bo/scripts/jscal/js/jscal2.js" type="text/javascript"></script>
    <script src="../../logica_bo/scripts/jscal/js/lang/es.js" type="text/javascript"></script>
    <link href="../../logica_bo/scripts/jscal/css/jscal2.css" rel="stylesheet" type="text/css" />
    <link href="../../logica_bo/scripts/jscal/css/steel/steel.css" rel="stylesheet" type="text/css" />
    <link href="<?=UrlResolver::getCssBaseUrl("bo/style/css.css")?>" rel="stylesheet" type="text/css" />
    <style>
    	td {
	    	vertical-align: top;
    	}
    	
    	ul {
	    	padding-bottom: 2em;
    	}
    	
    	li {
	    	padding: 0.5em 0;
	    	list-style-type: none;
	    	cursor: move;
	    	border-bottom: 1px dotted #CCC
    	}
    	
    	li:hover {
	    	background-color: #EAEAEA
    	}
    	
    	input[type=checkbox] {
	    	margin-right: 5px
    	}
    	
    	input[type=text],
    	input[type=file] {
	    	display: none;
	    	margin-left: 20px;
	    	margin-top: 5px;
	    	width: 400px
    	}
    	
    	small {
	    	font-size: 0.9em;
	    	display: block;
	    	margin-bottom: 2em
    	}
    </style>
</head>
<body>
    <form method="POST" action="index.php" enctype="multipart/form-data">
        <table width="550">
            <tr>
                <td colspan=2><span id="msjError" name="Error"></span>
                </td>
            </tr>
            <tr width="120">
                <td>Fecha del email</td>
                <td>
                    <select name="dia" id="dia">
                      <option value="0" >D&iacute;a</option>
                        <?
                        for ($i=1; $i<=31; $i++)
                        {
                            ?><option value="<?=$i?>" <?=(date("d") == $i ? 'selected="selected"' : '');?>><?=$i?></option><?
                        }
                        ?>
                    </select>
                    &nbsp;/&nbsp;
                    <select name="mes" id="mes">
                        <option value="0" >Mes</option>
                        <option value="1"  <?=(date("m") == 1 ? 'selected="selected"' : '');?>>enero</option>
                        <option value="2"  <?=(date("m") == 2 ? 'selected="selected"' : '');?>>febrero</option>
                        <option value="3"  <?=(date("m") == 3 ? 'selected="selected"' : '');?>>marzo</option>
                        <option value="4"  <?=(date("m") == 4 ? 'selected="selected"' : '');?>>abril</option>
                        <option value="5"  <?=(date("m") == 5 ? 'selected="selected"' : '');?>>mayo</option>
                        <option value="6"  <?=(date("m") == 6 ? 'selected="selected"' : '');?>>junio</option>
                        <option value="7"  <?=(date("m") == 7 ? 'selected="selected"' : '');?>>julio</option>
                        <option value="8"  <?=(date("m") == 8 ? 'selected="selected"' : '');?>>agosto</option>
                        <option value="9"  <?=(date("m") == 9 ? 'selected="selected"' : '');?>>septiembre</option>
                        <option value="10"  <?=(date("m") == 10 ? 'selected="selected"' : '');?>>octubre</option>
                        <option value="11"  <?=(date("m") == 11 ? 'selected="selected"' : '');?>>noviembre</option>
                        <option value="12"  <?=(date("m") == 12 ? 'selected="selected"' : '');?>>diciembre</option>
                    </select>
                    &nbsp;/&nbsp;
                    <select name="anno" id="anno">
                        <?
                        for ($i=(date("Y")-4); ($i<=date("Y")+4); $i++)
                        {
                            ?><option value="<?=$i?>" <?=(date("Y") == $i ? 'selected="selected"' : '');?>><?=$i?></option><?
                        }
                        ?>
                     </select>
                     <input type="submit" value="Cargar campa�as" style="margin-left:15px" />
                </td>
            </tr>
        </table>
    </form>
    <?php if($campanias): ?>
    <form id="frmABMCampanias" name="frmABMCampanias" method="POST" action="generate.php" enctype="multipart/form-data" style="margin-top:20px">

        <table width="550">
            <tr>
            	<td width="120">Campa�a destacada</td>
            	<td><select name="especial">
            		<option>Ninguna</option>
            	<?php
            		$camp = dmCampania::getCampaniasAx();
            		foreach($camp as $i => $c)
            			echo '<option value="'.$c['IdCampania'].'">'.$c['Nombre'].'</option>';
            	?>
            	</select>
            	<input type="text" name="especialTit" value="" placeholder="t�tulo del segmento" style="display:block" />
            	<input type="text" name="specialT" value="" placeholder="t�tulo de la campa�a" style="display:block" />
            	</td>
            </tr>
            <tr>
            	<td>
            		Banners header<br />
            		<a href="javascript:void(newBanner('header', true))"><small>Nuevo</small></a>
            	</td>
            	<td class="header">
	            	<ul>
	            	</ul>
 
 	            	<input type="hidden" name="headerOrden" id="headerOrden" />
            	</td>
            </tr>
            <tr>
            	<td>
            		Inician hoy<br />
            		<a href="javascript:void(newBanner('inician', true))"><small>Nuevo</small></a>
            	</td>
            	<td class="inician">
 	   	        	<input type="text" name="inicianTit" value="" placeholder="t�tulo del segmento" style="display:block" />
	            	<ul>
            			<?php foreach($inician as $camp): ?>
            			<li>
            				<input type="checkbox" name="inician[]" class="inician" value="<?=$camp['IdCampania']?>" /><?=$camp['Nombre']?>
            				<input type="text" name="inicianT[]" value="" placeholder="t�tulo de la campa�a" />
            			</li>
            			<?php endforeach; ?>
	            	</ul>
 
 	            	<input type="hidden" name="inicianOrden" id="inicianOrden" />
            	</td>
            </tr>
            <tr>
            	<td>
            		Finalizan hoy<br />
            		<a href="javascript:void(newBanner('finalizan', true))"><small>Nuevo</small></a>
            	</td>
            	<td class="finalizan">
 	   	        	<input type="text" name="finalizanTit" value="" placeholder="t�tulo del segmento" style="display:block" />
	            	<ul>
            			<?php foreach($finalizan as $camp): ?>
            			<li>
            				<input type="checkbox" name="finalizan[]" class="finalizan" value="<?=$camp['IdCampania']?>" /><?=$camp['Nombre']?>
            				<input type="text" name="finalizanT[]" value="" placeholder="t�tulo" />
            			</li>
            			<?php endforeach; ?>
	            	</ul>
 
 	            	<input type="hidden" name="finalizanOrden" id="finalizanOrden" />
           	</td>
            </tr>
            <tr>
            	<td>
            		Abiertas<br />
            		<a href="javascript:void(newBanner('abiertas', true))"><small>Nuevo</small></a>
            	</td>
            	<td class="abiertas">
 	   	        	<input type="text" name="abiertasTit" value="" placeholder="t�tulo del segmento" style="display:block" />
	            	<ul>
            			<?php foreach($abiertas as $camp): ?>
            			<li>
            				<input type="checkbox" name="abiertas[]" class="abiertas" value="<?=$camp['IdCampania']?>" /><?=$camp['Nombre']?>
            				<input type="text" name="abiertasT[]" value="" placeholder="t�tulo de la campa�a" />
            			</li>
            			<?php endforeach; ?>
	            	</ul>
	            	
	            	<input type="hidden" name="abiertasOrden" id="abiertasOrden" />
            	</td>
            </tr>
            <tr>
            	<td>
            		Pr�ximamente<br />
            		<a href="javascript:void(newBanner('proximamente', false))"><small>Nuevo</small></a>
            	</td>
            	<td class="proximamente">
 	   	        	<input type="text" name="proximamenteTit" value="" placeholder="t�tulo del segmento" style="display:block" />
	            	<ul>
            			<?php foreach($proximamente as $camp): ?>
            			<li>
            				<input type="checkbox" name="proximamente[]" class="proximamente" value="<?=$camp['IdCampania']?>" /><?=$camp['Nombre']?>
            			</li>
            			<?php endforeach; ?>
	            	</ul>
	            	
	            	<input type="hidden" name="proximamenteOrden" id="proximamenteOrden" />
            	</td>
            </tr>
            <tr>
            	<td>
            		Banners footer<br />
            		<a href="javascript:void(newBanner('footer', true))"><small>Nuevo</small></a>
            	</td>
            	<td class="footer">
	            	<ul>
	            	</ul>
 
 	            	<input type="hidden" name="footerOrden" id="footerOrden" />
            	</td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[1].submit();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>

    <?php endif; ?>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    	$('ul').sortable({
	    	items: 'li',
	    	stop: function(event, ui) {
		    	ordenar('inician');
		    	ordenar('finalizan');
		    	ordenar('proximamente');
		    	ordenar('abiertas');
		    	ordenar('header');
		    	ordenar('footer');
	    	}
    	})
    	
    	function ordenar(cls) {
	    	
	    	var ids = '';
	    	var data = new Array();
	    	
	    	$('.'+cls).each(function(k, el) {
	    		if(el.checked)
	    			if(isNaN(el.value)) {
		    			
		    			data.push({
		    				type: 'Image',
		    				id: $(el).parent().find('input.tit').data('id'),
		    				title: $(el).parent().find('input.tit').val(),
		    				link: $(el).parent().find('input.link').val(),
		    			});
		    			
	    			} else {
		    			
		    			data.push({
		    				type: 'Campaign',
		    				id: el.value,
		    				title: $(el).next().val(),
		    				link: ''
		    			});
		    			
	    			}
	    	})

	    	$('#'+cls+'Orden').val(JSON.stringify(data));
	    	
    	}
    	
    	var INDEX = 0;
    	function newBanner(where, image) {
	    	
	    	var html = '<li>';
	    	
	    	html += '<input type="checkbox" checked="checked" name="'+where+'[]" class="'+where+'" value="img" style="display:none" />';
	    	if(image)
		    	html += '<input type="file" name="uploaded[]" style="display:block" />';
	    	html += '<input type="text" name="'+where+'T[]" value="" placeholder="t�tulo" style="display:block" class="tit" data-id="'+INDEX+'" />';
	    	html += '<input type="text" name="'+where+'L[]" value="" placeholder="URL" style="display:block" class="link" data-id="'+INDEX+'" />';
	    	
	    	html += '</li>';
	    	
	    	$('td.'+where+' ul').append(html)
	    	$('td.'+where+' ul').find('li:last input[type=checkbox], li:last input[type=text]').change(function() {
	    		ordenar(where);
	    	});
	    	INDEX++;
	    	
    	}
    	
    	$('input[type=checkbox]').change(function() {
		    ordenar('inician');
		   	ordenar('finalizan');
		   	ordenar('proximamente');
		   	ordenar('abiertas');
		   	ordenar('header');
		   	ordenar('footer');
		   	
		   	var input = $(this).parent().find('input[type=text]');
		   	if(this.checked)
		   		input.css('display', 'block')
		   	else
		   		input.hide();
    	}).next('input[type=text]').change(function() {
	    	ordenar('inician');
		   	ordenar('finalizan');
		   	ordenar('proximamente');
		   	ordenar('abiertas');
		   	ordenar('header');
		   	ordenar('footer');
    	})
    	
    </script>
    <?php
	    $dir = Aplicacion::getRoot()."front/archivos/emails/";
	    echo '<!--file_exists: '.file_exists($dir)."-->\n";
	    echo '<!--is_dir: '.is_dir($dir)."-->\n";
	    echo '<!--is_writable: '.is_writable($dir)."-->\n";
    ?>
</body>
</html>