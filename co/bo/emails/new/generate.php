<?php

	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));     

	try
    {
        ValidarUsuarioLogueadoBo(60);
    }
    catch(ACCESOException $e)
    {
    ?>
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
    
    function cargar($campanias) {

		$oConexion = Conexion::nuevo();  		
	    
	    foreach($campanias as $i => $c) {
		    
		    $campanias[$i]->title = htmlentities(utf8_decode($campanias[$i]->title));
		    
		    if($c->type == 'Campaign' && is_numeric($c->id)) {
			 
			 	$oConexion->Abrir();
				$oConexion->setQuery("SET NAMES utf8");
				$oConexion->EjecutarQuery();
			    $oConexion->setQuery("SELECT Nombre FROM campanias WHERE IdCampania = ".$c->id);
			    $data = current($oConexion->DevolverQuery());
			    $oConexion->Cerrar(); 
			    
			    $campanias[$i]->name = $data['Nombre'];
			 
				$arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$c->id."/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));
				
				if($arrArchivos["banner_camp-on"] != '')
					$campanias[$i]->image = "http://img.static.geelbe.com/co/front/campanias/archivos/campania_".$c->id."/imagenes/".$arrArchivos["banner_camp-on"];
					
				$campanias[$i]->link = 'http://www.geelbe.com/co/logica/autologin/login.php?IdUsuario=*|IDCO|*&Hash=*|HASHCO|*&Type=CAMPAIGN&IdCampania='.$c->id.'&utm_source=mail_doppler&utm_medium=email&utm_content=[[[IdCampania]]]&utm_campaign=[[[Date]]]';
				
		    } else if($c->type == 'Image' && is_numeric($c->id)) {
		    
		    	$dir = Aplicacion::getRoot()."front/archivos/emails/";
		    	if(!is_dir($dir))
		    		mkdir($dir);
		    	
			    if(is_uploaded_file($_FILES['uploaded']['tmp_name'][$c->id]) && move_uploaded_file($_FILES['uploaded']['tmp_name'][$c->id], $dir.$_FILES['uploaded']['name'][$c->id]))
			    	$campanias[$i]->image = "http://img.static.geelbe.com/co/front/archivos/emails/".$_FILES['uploaded']['name'][$c->id];
			    

			    $campanias[$i]->id = 'BANNER';
		    	$campanias[$i]->name = $c->title;
		    	$campanias[$i]->link = $c->link;
			    
		    } else {
			    unset($campanias[$i]);
		    }
		    
	    }
	    
	    return $campanias;
	    
    }  
    
    //strip escaping
    if(ini_get('magic_quotes_gpc') == 1) {
    
	    $fields = array('headerOrden', 'footerOrden', 'inicianOrden', 'finalizanOrden', 'proximamenteOrden', 'abiertasOrden');
	    foreach($fields as $f)
	    	$_POST[$f] = stripslashes($_POST[$f]);
    
    }
    
	$header = json_decode(utf8_encode($_POST['headerOrden']));
	$footer = json_decode(utf8_encode($_POST['footerOrden']));
	$inician = json_decode(utf8_encode($_POST['inicianOrden']));
	$finalizan = json_decode(utf8_encode($_POST['finalizanOrden']));
	$proximamente = json_decode(utf8_encode($_POST['proximamenteOrden']));
	$abiertas = json_decode(utf8_encode($_POST['abiertasOrden']));    
	$special = array((object) array('id' => $_POST['especial'], 'title' => $_POST['specialT'], 'type' => 'Campaign'));

	$header = cargar($header);
	$footer = cargar($footer);
	$inician = cargar($inician);
	$finalizan = cargar($finalizan);	
	$proximamente = cargar($proximamente);	
	$abiertas = cargar($abiertas);	
	$special = cargar($special);
	
	//Cargar html
	$email = file_get_contents(dirname(__FILE__).'/mail.html');
	require_once dirname(__FILE__).'/html.php';
		
	//Inician hoy
	$in_head = '';
	$in = '';
	if(count($inician) > 0) {
		$in_head = str_replace('[[[Tit]]]', $_POST['inicianTit'], $html['inician_head']);
		
		foreach($inician as $i => $camp) {
		   $inicianFormated = str_replace('{img}', $camp->image, $html['inician']);
		   $inicianFormated = str_replace('{titulo}', $camp->title, $inicianFormated);
		   $inicianFormated = str_replace('{link}', $camp->link, $inicianFormated);
		   $in .= str_replace('[[[IdCampania]]]', $camp->id, $inicianFormated);
		}
	}
	
	$email = str_replace('{HOY_HEAD}', $in_head, $email);
	$email = str_replace('{HOY}', $in, $email);

	//Header
	$in = '';
	if(count($header) > 0) {
		
		foreach($header as $i => $camp) {
		   $inicianFormated = str_replace('{img}', $camp->image, $html['header']);
		   $inicianFormated = str_replace('{tit}', $camp->title, $inicianFormated);
		   $in .= str_replace('{link}', $camp->link, $inicianFormated);			
		}
	}
	
	$email = str_replace('{HEADER}', $in, $email);

	//Footer
	$in = '';
	if(count($footer) > 0) {
		
		foreach($footer as $i => $camp) {
		   $inicianFormated = str_replace('{img}', $camp->image, $html['footer']);
		   $inicianFormated = str_replace('{tit}', $camp->title, $inicianFormated);
		   $in .= str_replace('{link}', $camp->link, $inicianFormated);			
		}
	}
	
	$email = str_replace('{FOOTER}', $in, $email);

	//Abiertas
	$in_head = '';
	$in = '';
	if(count($abiertas) > 0) {
		$in_head = str_replace('[[[Tit]]]', $_POST['abiertasTit'], $html['abiertas_head']);
		
		foreach($abiertas as $i => $camp) {
		   $inicianFormated = str_replace('{img}', $camp->image, $html['abiertas']);
		   $inicianFormated = str_replace('{titulo}', $camp->title, $inicianFormated);
		   $inicianFormated = str_replace('{link}', $camp->link, $inicianFormated);
		   $in .= str_replace('[[[IdCampania]]]', $camp->id, $inicianFormated);
			
		}
	}
	
	$email = str_replace('{ABIERTA_HEAD}', $in_head, $email);
	$email = str_replace('{ABIERTA}', $in, $email);

	//Finalizan hoy
	$in_head = '';
	$in = '';
	if(count($finalizan) > 0) {
		$in_head = str_replace('[[[Tit]]]', $_POST['finalizanTit'], $html['finaliza_head']);
		
		foreach($finalizan as $i => $camp) {
			$finFormated = str_replace('{img}', $camp->image, $html['inician']);
			$finFormated = str_replace('{titulo}', $camp->title, $finFormated);
		   $finFormated = str_replace('{link}', $camp->link, $finFormated);
			$in .= str_replace('[[[IdCampania]]]', $camp->id, $finFormated);
		}
	}
	
	$email = str_replace('{FINALIZA_HEAD}', $in_head, $email);
	$email = str_replace('{FINALIZA}', $in, $email);

	//Especial
	$in = '';
	if(isset($special)) {

		foreach($special as $i => $camp) {
			$html['especial'] = str_replace('[[[Tit]]]', $_POST['especialTit'], $html['especial']);
			$html['especial'] = str_replace('{img}', $camp->image, $html['especial']);
			$html['especial'] = str_replace('{titulo}', $camp->title, $html['especial']);
			$html['especial'] = str_replace('{link}', $camp->link, $html['especial']);
			$in .= str_replace('[[[IdCampania]]]', $camp->id, $html['especial']);
		}
	}
	
	$email = str_replace('{ESPECIAL}', $in, $email);

	//proximamente
	$in = '';
	if(count($proximamente) > 0) {
		
		foreach($proximamente as $i => $camp) {
		
			$proximamenteSetted = str_replace('{name}', htmlentities($camp->name, ENT_COMPAT, 'UTF-8'), $html['proximamente']).' | ';
			$in .= str_replace('[[[IdCampania]]]', $camp->id, $proximamenteSetted);
		}
		
		
	
		
	}
	$in = substr($in, 0, -3);
	$email = str_replace('{PROXIMAMENTE}', $in, $email);
	
	$email = str_replace('[[[Date]]]', date("Y-m-d"), $email);
	$email = str_replace('[[[proximamenteTit]]]', $_POST['proximamenteTit'], $email);
	
	//Guardar
	header('Pragma: public'); 	// required
	header('Expires: 0');		// no cache
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($file_name)).' GMT');
	header('Cache-Control: private',false);
	header('Content-Type: text/html');
	header('Content-Disposition: attachment; filename="email.html"');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: '.strlen($email));	// provide file size
	header('Connection: close');
	
	echo $email;
	die;

?>