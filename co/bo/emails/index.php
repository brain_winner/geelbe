<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("ordenamientos"));
    try
    {
        ValidarUsuarioLogueadoBo(60);
    }
    catch(ACCESOException $e)
    {
    ?>
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> E-Mails de campa�as</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/campanias/js.js");?>" type="text/javascript"></script>
    <script src="../../logica_bo/scripts/jscal/js/jscal2.js" type="text/javascript"></script>
    <script src="../../logica_bo/scripts/jscal/js/lang/es.js" type="text/javascript"></script>
    <link href="../../logica_bo/scripts/jscal/css/jscal2.css" rel="stylesheet" type="text/css" />
    <link href="../../logica_bo/scripts/jscal/css/steel/steel.css" rel="stylesheet" type="text/css" />
    <link href="<?=UrlResolver::getCssBaseUrl("bo/style/css.css")?>" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmABMCampanias" name="frmABMCampanias" method="POST" action="generate.php" enctype="multipart/form-data">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="Error"></span>
                </td>
            </tr>
            <tr>
                <td>Fecha del email</td>
                <td>
                    <select name="FechaInicio[cbxDia]" id="FechaInicio[cbxDia]">
                      <option value="0" >D&iacute;a</option>
                        <?
                        for ($i=1; $i<=31; $i++)
                        {
                            ?><option value="<?=$i?>" <?=(date("d") == $i ? 'selected="selected"' : '');?>><?=$i?></option><?
                        }
                        ?>
                    </select>
                    &nbsp;/&nbsp;
                    <select name="FechaInicio[cbxMes]" id="FechaInicio[cbxMes]">
                        <option value="0" >Mes</option>
                        <option value="1"  <?=(date("m") == 1 ? 'selected="selected"' : '');?>>enero</option>
                        <option value="2"  <?=(date("m") == 2 ? 'selected="selected"' : '');?>>febrero</option>
                        <option value="3"  <?=(date("m") == 3 ? 'selected="selected"' : '');?>>marzo</option>
                        <option value="4"  <?=(date("m") == 4 ? 'selected="selected"' : '');?>>abril</option>
                        <option value="5"  <?=(date("m") == 5 ? 'selected="selected"' : '');?>>mayo</option>
                        <option value="6"  <?=(date("m") == 6 ? 'selected="selected"' : '');?>>junio</option>
                        <option value="7"  <?=(date("m") == 7 ? 'selected="selected"' : '');?>>julio</option>
                        <option value="8"  <?=(date("m") == 8 ? 'selected="selected"' : '');?>>agosto</option>
                        <option value="9"  <?=(date("m") == 9 ? 'selected="selected"' : '');?>>septiembre</option>
                        <option value="10"  <?=(date("m") == 10 ? 'selected="selected"' : '');?>>octubre</option>
                        <option value="11"  <?=(date("m") == 11 ? 'selected="selected"' : '');?>>noviembre</option>
                        <option value="12"  <?=(date("m") == 12 ? 'selected="selected"' : '');?>>diciembre</option>
                    </select>
                    &nbsp;/&nbsp;
                    <select name="FechaInicio[cbxAnio]" id="FechaInicio[cbxAnio]">
                        <?
                        for ($i=(date("Y")-4); ($i<=date("Y")+4); $i++)
                        {
                            ?><option value="<?=$i?>" <?=(date("Y") == $i ? 'selected="selected"' : '');?>><?=$i?></option><?
                        }
                        ?>
                     </select>
                </td>
            </tr>
            <tr>
            	<td>Campa�a destacada</td>
            	<td><select name="especial">
            		<option>Ninguna</option>
            	<?php
            		$camp = dmCampania::getCampaniasAx();
            		foreach($camp as $i => $c)
            			echo '<option value="'.$c['IdCampania'].'">'.$c['Nombre'].'</option>';
            	?>
            	</select></td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].submit();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>