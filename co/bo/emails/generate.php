<?php

	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));     

	$time = date("YmdHis");
	//$cached = dirname(__FILE__).'/cache/'.$time.'/';
	$cached = $_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/archivos/cache/ '.$time.'/';
	
	mkdir($cached);

	function loadImage($campanias) {
		global $cached;
				
		foreach($campanias as $i => $c) {
			
			$arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$c["IdCampania"]."/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));
			
			if($arrArchivos["banner_camp-on"] != '') {		
				$campanias[$i]['origial_image'] = Aplicacion::getRoot()."front/campanias/archivos/campania_".$c["IdCampania"]."/imagenes/".$arrArchivos["banner_camp-on"];
				$campanias[$i]['image'] = $c["IdCampania"]."_".$arrArchivos["banner_camp-on"];
				
				//crop image
				$save = $cached.$c["IdCampania"]."_".$arrArchivos["banner_camp-on"];
				$image = $campanias[$i]['origial_image'];
				
				$ext = pathinfo($image);
				$ext = $ext['extension'];
				switch($ext) {
					case 'jpg':
					case 'jpeg':
						$image = imagecreatefromjpeg($image);
					break;
					case 'gif':
						$image = imagecreatefromgif($image);
					break;
					case 'png':
						$image = imagecreatefrompng($image);
					break;
				}
				
				$width = 507;
				$height = $width*imagesy($image)/imagesx($image);
				
				$new = imagecreatetruecolor($width, $height);
				imagecopyresampled($new, $image, 0, 0, 0, 0, $width, $height, imagesx($image), imagesy($image));
				imagejpeg($new, $save, 100);
				

			} else
				unset($campanias[$i]);
			
		}
		
		return $campanias;
		
	}    

	//Crear timestamps
	$_POST['FechaFin'] = $_POST['FechaInicio'];
	$from = mktime(0, 0, 0, $_POST['FechaInicio']['cbxMes'], $_POST['FechaInicio']['cbxDia'], $_POST['FechaInicio']['cbxAnio']);
	$to = mktime(0, 0, 0, $_POST['FechaFin']['cbxMes'], $_POST['FechaFin']['cbxDia'], $_POST['FechaFin']['cbxAnio']);

	//Cargar campa�as
	$oConexion = Conexion::nuevo();  		
	
	$oConexion->Abrir();
	$oConexion->setQuery("SET NAMES utf8");
	$oConexion->EjecutarQuery();
    $oConexion->setQuery("SELECT IdCampania, Nombre, Descripcion AS Slogan, FechaInicio, FechaFin FROM campanias 
	WHERE FechaInicio LIKE '".date("Y-m-d", $from)."%'");
    $inician = $oConexion->DevolverQuery();
    $oConexion->Cerrar();
    /*$inician[] = array('IdCampania' => 637);*/
    
    $oConexion->Abrir();
	$oConexion->setQuery("SET NAMES utf8");
	$oConexion->EjecutarQuery();
    $oConexion->setQuery("SELECT IdCampania, Nombre, Descripcion AS Slogan, FechaInicio, FechaFin FROM campanias 
	WHERE FechaFin LIKE '".date("Y-m-d", $to)."%'");
    $finalizan = $oConexion->DevolverQuery();
    $oConexion->Cerrar();

	if(is_numeric($_POST['especial'])) {
	    
	    $oConexion->Abrir();
		$oConexion->setQuery("SET NAMES utf8");
		$oConexion->EjecutarQuery();
	    $oConexion->setQuery("SELECT IdCampania, Nombre, Descripcion AS Slogan, FechaInicio, FechaFin FROM campanias 
		WHERE IdCampania = ".$_POST['especial']);
	    $special = $oConexion->DevolverQuery();
	    $oConexion->Cerrar();
	    
	}
    
    $oConexion->Abrir();
	$oConexion->setQuery("SET NAMES utf8");
	$oConexion->EjecutarQuery();
    $oConexion->setQuery("SELECT IdCampania, Nombre, Descripcion AS Slogan, FechaInicio, FechaFin FROM campanias 
	WHERE FechaInicio >= '".date("Y-m-d", $from + 24*60*60)."%'");
    $proximamente = $oConexion->DevolverQuery();
    $oConexion->Cerrar();

	//Cargar im�genes    
    $inician = loadImage($inician);
    $finalizan = loadImage($finalizan);
    $special = loadImage($special);    

	//Cargar html
	$email = file_get_contents(dirname(__FILE__).'/mail.html');
	require_once dirname(__FILE__).'/html.php';
		
	//Inician hoy
	$in_head = '';
	$in = '';
	$campanias = array();
	if(count($inician) > 0) {
		$in_head = $html['inician_head'];
		
		foreach($inician as $i => $camp) {
		/*	$html['inician'] = str_replace('{img}', $camp['image'], $html['inician']);
			$in .= str_replace('[[[IdCampania]]]', $camp['IdCampania'], $html['inician']);*/
			
		   $inicianFormated = str_replace('{img}', $camp['image'], $html['inician']);
		   $in .= str_replace('[[[IdCampania]]]', $camp['IdCampania'], $inicianFormated);
		   $campanias[] = $camp['IdCampania'];
			
		}
	}
	
	$email = str_replace('{HOY_HEAD}', $in_head, $email);
	$email = str_replace('{HOY}', $in, $email);

	//Finalizan hoy
	$in_head = '';
	$in = '';
	if(count($finalizan) > 0) {
		$in_head = $html['finaliza_head'];
		
		foreach($finalizan as $i => $camp) {
			/*$html['finaliza'] = str_replace('{img}', $camp['image'], $html['finaliza']);
			$in .= str_replace('[[[IdCampania]]]', $camp['IdCampania'], $html['finaliza']);*/
			
			$finFormated = str_replace('{img}', $camp['image'], $html['inician']);
		   $in .= str_replace('[[[IdCampania]]]', $camp['IdCampania'], $finFormated);
		}
	}
	
	$email = str_replace('{FINALIZA_HEAD}', $in_head, $email);
	$email = str_replace('{FINALIZA}', $in, $email);

	//Especial
	$in = '';
	if(isset($special)) {

		foreach($special as $i => $camp) {
			$html['especial'] = str_replace('{img}', $camp['image'], $html['especial']);
			$in .= str_replace('[[[IdCampania]]]', $camp['IdCampania'], $html['especial']);
		}
	}
	
	$email = str_replace('{ESPECIAL}', $in, $email);

	//proximamente
	$in = '';
	if(count($proximamente) > 0) {
		
		foreach($proximamente as $i => $camp) {
		
			if(in_array($camp['IdCampania'], $campanias))
				continue;
				

			/*$html['proximamente'] = str_replace('{name}', $camp['Nombre'], $html['proximamente']).' | ';
			$in .= str_replace('[[[IdCampania]]]', $camp['IdCampania'], $html['proximamente']); */
			
			$proximamenteSetted = str_replace('{name}', htmlentities($camp['Nombre'], ENT_COMPAT, 'UTF-8'), $html['proximamente']).' | ';
		   $in .= str_replace('[[[IdCampania]]]', $camp['IdCampania'], $proximamenteSetted);
		}
		
		
	
		
	}
	$in = substr($in, 0, -3);
	$email = str_replace('{PROXIMAMENTE}', $in, $email);
	
	$email = str_replace('[[[Date]]]', date("Y-m-d", $from), $email);
	
	//Guardar
	file_put_contents($cached.'mail.html', $email);	

	//Zip!
	$zip = new ZipArchive;
	//$zip->open(dirname(__FILE__).'/cache/'.$time.'.zip', ZIPARCHIVE::CREATE);
	$zip->open($_SERVER['DOCUMENT_ROOT'].'/' .$confRoot[1].'/front/archivos/cache/'.$time.'.zip', ZIPARCHIVE::CREATE);
	
	
	$images = dirname(__FILE__).'/email/';
	$files = scandir($images);
	foreach($files as $i => $file)
		if(!is_dir($images.$file))
			$zip->addFile($images.$file, $file);
			
	$files = scandir($cached);
	foreach($files as $i => $file)
		if(!is_dir($cached.$file))
			$zip->addFile($cached.$file, $file);
	
	$zip->close();
	
	header("Location: /".$confRoot[1].'/front/archivos/cache/'.$time.'.zip');
	
	


?>