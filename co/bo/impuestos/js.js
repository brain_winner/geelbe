function ErrorMSJ(code, def)
{
    if(arrErrores["IMPUESTOS"][code] != undefined)
        document.getElementById("msjError").innerHTML = arrErrores["IMPUESTOS"][code];
    else
        document.getElementById("msjError").innerHTML = code;
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtNombre.value.length == 0)
        {
            arrError.push(arrErrores["IMPUESTOS"]["NOMBRE"]);
        }
        if(frm.optTipo[0].checked==false && frm.optTipo[1].checked==false)
        {
            arrError.push(arrErrores["IMPUESTOS"]["TIPO"]);
        }
        if (arrError.length>0)
        {
            var error = ""
            for(i=0; i<arrError.length; i++)
            {
                error +="*" + arrError[i] + "\n";
            }
            alert(error);
            return false;
        }
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}
function irVolver()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"impuestos/index.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["IMPUESTOS"]["SELECCIONE"]);
        else
            window.location.href="ABM.php?IdImpuesto="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVer()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["IMPUESTOS"]["SELECCIONE"]);
        else
            window.location.href="Ver.php?IdImpuesto="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar(id)
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["IMPUESTOS"]["SELECCIONE"]);
        else
        {
            if(confirm(arrErrores["IMPUESTOS"]["ELIMINAR"]))
                window.location.href="../../logica_bo/impuestos/actionform/eliminar_impuestos.php?IdImpuesto="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}