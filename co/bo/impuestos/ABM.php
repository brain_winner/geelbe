<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("impuestos"));
    try
    {
        ValidarUsuarioLogueadoBo(2);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "impuestos");
    $objImpuesto = dmImpuestos::getByIdImpuesto((isset($_GET["IdImpuesto"])) ? $_GET["IdImpuesto"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Impuestos</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/impuestos/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMImpuestos" name="frmABMImpuestos" method="POST" onSubmit="ValidarForm(this);">
        <input type="hidden" id="txtIdImpuesto" name="txtIdImpuesto" value="<?=$objImpuesto->getIdImpuesto()?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["impuestos"]);
            ?>
            <tr>
                <td>Nombre</td>
                <td><input type="text" id="txtNombre" name="txtNombre" maxlength="40" value="<?=$objImpuesto->getNombre()?>"></td>
            </tr>
            <tr>
                <td>Tipo</td>
                <td>
                    <input type="radio" id="optTipo" name="optTipo" value="0" <?=($objImpuesto->getTipo() == 0)?"checked=checked":""?>>
                    Impuesto de valor porcentual 
                    <input type="radio" id="optTipo" name="optTipo" value="1" <?=($objImpuesto->getTipo() == 1)?"checked=checked":""?>>
                    Impuesto de valor fijo
                </td>
            </tr>
            <tr>
                <td>Habilitado</td>
                <td>
                    <input type="checkbox" id="chkHabilitado" name="chkHabilitado" value="OK">
                    <script>
                        <?
                            if($objImpuesto->getHabilitado() == 1)
                            {
                                ?>document.getElementById("chkHabilitado").checked=true;<?
                            }
                        ?>
                    </script>
                </td>
            </tr>
            <tr>
                <td colspan="2">Provincias
                </td>
            </tr>
            <tr>
                <td>Provincias
                </td>
                <td>Valor
                </td>
            </tr>
            <?
                $i=0;
                foreach(dmImpuestos::getProvinciasByIdImpuesto($objImpuesto->getIdImpuesto()) as $impuesto)
                {
                    ?>
                    <tr>
                        <td>
                            <input type="hidden" id="frmImpuestos[<?=$i?>][IdProvincia]" name="frmImpuestos[<?=$i?>][IdProvincia]" value="<?=$impuesto["IdProvincia"]?>"/>
                            <?=$impuesto["Nombre"]?>
                        </td>
                        <td>
                            <input size=5 type="text" onkeypress="return solosFloatsKEYCODE(event);" onblur="(!esFloat(this.value))?this.value = 0:''" id="frmImpuestos[<?=$i?>][Valor]" name="frmImpuestos[<?=$i?>][Valor]" value="<?=($impuesto["Valor"] != "")?$impuesto["Valor"]:0?>"/>
                        </td>
                    </tr>
                    <?
                    $i++;
                }
            ?>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
    <iframe style="display:none" id="iImpuestos" name="iImpuestos"></iframe>
</body>
</html>
