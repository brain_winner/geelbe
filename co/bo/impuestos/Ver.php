<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("impuestos"));
    try
    {
        ValidarUsuarioLogueadoBo(2);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $objImpuesto = dmImpuestos::getByIdImpuesto((isset($_GET["IdImpuesto"])) ? $_GET["IdImpuesto"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Impuestos</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/impuestos/js.js");?>" type="text/javascript"></script>
</head>
<body>
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <tr>
                <td>Nombre</td>
                <td><b><?=$objImpuesto->getNombre()?></b></td>
            </tr>
            <tr>
                <td>Tipo</td>
                <td><b><?=($objImpuesto->getTipo() == 0)?"Impuesto de valor porcentual":"Impuesto de valor fijo"?></b>
                </td>
            </tr>
            <tr>
                <td colspan="2">Provincias
                </td>
            </tr>
            <tr>
                <td>Provincias
                </td>
                <td>Valor
                </td>
            </tr>
            <?
                foreach(dmImpuestos::getProvinciasByIdImpuesto($objImpuesto->getIdImpuesto()) as $impuesto)
                {
                    ?>
                    <tr>
                        <td>
                            <b><?=$impuesto["Nombre"]?></b>
                        </td>
                        <td>
                            <b><?=$impuesto["Valor"]?></b>
                        </td>
                    </tr>
                    <?
                }
            ?>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnVolver','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver.gif");?>" alt="Volver" name="btnVolver" id="btnVolver" border="0"/></a>
                </td>
            </tr>
        </table>
</body>
</html>
