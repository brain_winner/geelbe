<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");  
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("perfiles"));   
    try
    {
        ValidarUsuarioLogueadoBo(71);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
    
    if(isset($_POST['notif'])) {
    
    	$oConexion = Conexion::nuevo();
        try
        {
            $oConexion->Abrir_Trans();
            $oConexion->setQuery("UPDATE perfiles SET notificacion_stock = 0, notificacion_gcash = 0");
            $oConexion->EjecutarQuery();
            $oConexion->Cerrar_Trans();
        }
        catch(MySQLException $e)
        {
            throw $e;
        }

    
    	foreach($_POST['notif'] as $i => $notif) {
		    
		    $oConexion = Conexion::nuevo();
	        try
	        {
	            $oConexion->Abrir_Trans();
	            $oConexion->setQuery("UPDATE perfiles SET notificacion_stock = ".(isset($notif['stock']) ? '1' : '0').", notificacion_gcash = ".(isset($notif['gcash']) ? '1' : '0')." WHERE IdPerfil = ".intval($i));
	            $oConexion->EjecutarQuery();
	            $oConexion->Cerrar_Trans();
	        }
	        catch(MySQLException $e)
	        {
	            throw $e;
	        }
	        
		}
	    
    } 

    $dtPerfiles = dmPerfiles::getAll((isset($_GET["columna"])?$_GET["columna"]:0)+1, (isset($_GET["sentido"])?$_GET["sentido"]:"DESC"), (isset($_GET["q"])?$_GET["q"]:""));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> Perfiles</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/perfiles/js.js");?>" type="text/javascript"></script>
    <link href="<?=UrlResolver::getCssBaseUrl("bo/style/css.css")?>" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmABMLocales" name="frmABMLocales" method="post" action="index.php" enctype="multipart/form-data">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <tr>
                <th>Perfiles</th>
                <th>Stock insuficiente</th>
                <th>Carga GCash</th>
            </tr>
			<?php
            
              foreach($dtPerfiles as $i => $s) {

                  echo '<tr>
                            <td class="td_contenido">
								'.$s['Descripcion'].'
							</td>
                            <td class="td_contenido" align="center">
								<input type="checkbox" name="notif['.$s['IdPerfil'].'][stock]" value="1" '.($s['notificacion_stock'] == 1 ? 'checked' : '').' /> 
							</td>
                            <td class="td_contenido" align="center">
								<input type="checkbox" name="notif['.$s['IdPerfil'].'][gcash]" value="1" '.($s['notificacion_gcash'] == 1 ? 'checked' : '').' /> 
							</td>
                        </tr>';   		
                    
              }
            
            ?>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].submit();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
