<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
	
	if(isset($_REQUEST["IdTemplate"])) {
		$Template = dmMensajes::getTemplate($_REQUEST["IdTemplate"]);
	}
    
	try {
        ValidarUsuarioLogueadoBo(14);
    }
    catch(ACCESOException $e) {
?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
<?php
        exit;
    }     
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Templates</title>
    <?php Includes::Scripts(); ?>
    <script src="http://js.static.geelbe.com/ar/registro/js/gen_validatorv31.js" type="text/javascript"></script>
    
</head>
<body>
    <form id="frmABMTemplates" name="frmABMTemplates" action="/<?=$confRoot[1];?>/logica_bo/mensajes/actionform/templates.php" method="POST">
        <?php 
        if(isset($_REQUEST["IdTemplate"])) {
        ?>
        <input type="hidden" id="idTemplate" name="idTemplate" value="<?=$Template["id_template"]?>"/>
        <?php 
		}
        ?>
        <table>
            <tr>
                <td colspan=2>
                	<div id="frmABMTemplates_errorloc" class="error_strings"></div>
                </td>
            </tr>
            <tr>
                <td>Titulo</td>
                <td><input type="text" id="txtTitle" name="txtTitle" maxlength="40" value="<?=$Template["title"]?>"></td>
            </tr>
            <tr>
                <td>Mensaje</td>
                <td><textarea id="txtMensaje" name="txtMensaje" cols="50" rows="10"><?=$Template["message"]?></textarea></td>
            </tr>
            <tr>
                <td>Tipo</td>
                <td>
                <select id="txtTipo" name="txtTipo">
                <option value="1" <?php if($Template["type"]==1) echo 'selected="selected"';?>>Introducci&oacute;n</option>
                <option value="2" <?php if($Template["type"]==2) echo 'selected="selected"';?>>Cuerpo</option>
                <option value="3" <?php if($Template["type"]==3) echo 'selected="selected"';?>>Pie</option>
                </select>
                </td>
            </tr>
           
            <tr>
                <td align=center></td>
            	<td align=center><input type="submit" value="Aceptar"/></td>
            </tr>
        </table>
    </form>

<!-- Geelbe Validation Form -->
<script type="text/javascript">
var formValidator  = new Validator("frmABMTemplates");
formValidator.EnableOnPageErrorDisplaySingleBox();
formValidator.EnableMsgsTogether();
formValidator.EnableFocusOnError(false);

formValidator.addValidation("txtTitle","req","Por favor, ingrese el Titulo.");
formValidator.addValidation("txtMensaje","req","Por favor, ingrese el Mensaje.");
</script>
<!-- Geelbe Validation Form -->
</body>
</html>
