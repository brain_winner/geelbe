<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
		$POST = Aplicacion::getIncludes("post", "pedidos");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(14);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }	
	try {
		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array('id_template', 'title', 'message', 'type'));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("id_template", "ASC");

		$parametro = "";

		$count = dmMensajes::getTemplatesMensajesCount($paramArray);
		$listPager = new ListadoPaginator($count);
		$rows = dmMensajes::getTemplatesMensajesPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
		$typeFilter =  array(array("id" => "1", "val" => "Introducci&oacute;n"), 
		                     array("id" => "2", "val" => "Cuerpo"),
		                     array("id" => "3", "val" => "Pie"));
	
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Templates</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Templates</h1>	

			<div id="mainStats">
					<table style="margin-bottom:10px;">
						<tr class="alt">
							<td style="text-align:left;font-weight:bold;"><b>Crear:</b> <a href="crear.php">Nuevo Template</a></td>
						</tr>
					</table>
					
				</div>
			
			
			<?php
			
				$listadoPrinter = new ListadoPrinter($rows, "index.php$parametro", $listPager);
				$listadoPrinter->showColumn("id_template", "Id");
				$listadoPrinter->showColumn("title", "T&iacute;tulo");
				$listadoPrinter->showColumn("messagel", "Mensaje");
				$listadoPrinter->showColumn("typed", "Tipo", true, true, $typeFilter);
				$listadoPrinter->showColumn("Acciones", "Acciones");
				
				$listadoPrinter->setFilterName("messagel", "message");
				$listadoPrinter->setFilterName("typed", "type");
				
				$listadoPrinter->setShowFilter("Acciones", false);
				$listadoPrinter->setSortColumn("Acciones", false);
				
				$listadoPrinter->addButtonToColumn("Acciones", "<select id=\"U[[id_template]]\" name=\"menu\" style\"width:40px;\" onchange=\"process_choice(this, [[id_template]])\">
																	<option value=\"0\" selected>Seleccione:</option>
																	<option value=\"1\">Editar Template</option>
																	<option value=\"2\">Eliminar Template</option>
																</select>");
				
				$listadoPrinter->printListado();
			?>
			<script type="text/javascript">
				function process_choice(selection, idTemplate) {
					if (selection.value==1) {
						window.location.href="/<?=$confRoot[1]?>/bo/templates/crear.php?IdTemplate="+idTemplate;
					}
					else if (selection.value==2) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/mensajes/actionform/eliminar_template.php?IdTemplate="+idTemplate;
					}
					document.getElementById("U"+idTemplate).disabled="disabled";
				}
			</script>
			
		</div>	
	</body>
</html>