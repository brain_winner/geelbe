<?  
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/newsletters/clases/clsnewsletters.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/newsletters/datamappers/dmnewsletters.php");
   try
    {
        ValidarUsuarioLogueadoBo(100);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = array('newsletters' => 'logica_bo/newsletters/actionform/newsletters.php');
    $oLocal = dmNewsletters::getById((isset($_GET["IdNewsletter"])) ? $_GET["IdNewsletter"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Newsletters</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/newsletters/js.js");?>" type="text/javascript"></script>
	<link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
</head>
<body>
    <form id="frmABMLocales" name="frmABMLocales" method="post" enctype="multipart/form-data">
        <input type="hidden" id="idNewsletter" name="idNewsletter" value="<?=$oLocal->getIdNewsletter()?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["newsletters"]);
            ?>
            <tr>
                <td valign="top">Asunto</td>
                <td><input type="text" id="txtAsunto" name="txtAsunto" size=60 value="<?=$oLocal->getAsunto()?>" /></td>
            </tr>
            <tr>
                <td valign="top">Fecha de envio</td>
                <td><input type="text" id="txtFechaEnvio" name="txtFechaEnvio" maxlength="80" size=60 value="<?=$oLocal->getFechaEnvio()?>" /></td>
            </tr>
            <tr>
                <td valign="top">Cuerpo</td>
                <td><textarea id="txtCuerpo" name="txtCuerpo" cols="60" rows="30"><?=$oLocal->getCuerpo()?></textarea></td>
            </tr>
            <tr>
                <td valign="top">Cargar cuerpo desde HTML</td>
                <td><input type="file" id="html" /></td>
            </tr>
            <tr>
                <td valign="top"></td>
                <td><input type="checkbox" name="test" value="1" <?=($oLocal->getTest() ? 'checked' : '')?> /> Enviar a lista de pruebas</td>
            </tr>
            <tr>
                <td valign="top"></td>
                <td><input type="checkbox" name="huerfanos" value="1" <?=($oLocal->getHuerfanos() ? 'checked' : '')?> /> Enviar a usuarios hu&eacute;rfanos</td>
            </tr>
            <tr>
                <td valign="top"></td>
                <td><input type="checkbox" name="inactivos" value="1" <?=($oLocal->getInactivos() ? 'checked' : '')?> /> Enviar a usuarios inactivos</td>
            </tr>
            <tr>
                <td valign="top"></td>
                <td><input type="checkbox" name="enviar" value="1" <?=(!$oLocal->getEnviado() ? 'checked' : '')?> /> Enviar</td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].submit();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
    <script>
    	$('#txtFechaEnvio').datepicker({ dateFormat: 'yy-mm-dd' })
    	$('#html').change(function(e) {
	    	var reader = new FileReader();
			reader.onload = function(theFile) {
				$('#txtCuerpo').val(theFile.target.result);		
			};
			reader.readAsText(e.target.files[0]);
    	})
    </script>
</body>
</html>