function ValidarForm(frm)
{
    if (frm.txtAsunto.length == 0)
    {
        return false;                                                                  
    }
    frm.submit();
}
function irVolver()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"newsletters/index.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEliminar()
{
	try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="../../logica_bo/newsletters/actionform/eliminar_newsletter.php?IdNewsletter="+dt_getCeldaSeleccionada(0);
        
    }
    catch(e)
    {
        throw e;
    }
}
function irEnviar(id)
{
	try
    {
            window.location.href="../../logica_bo/newsletters/actionform/encolar.php?IdNewsletter="+id;
        
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="ABM.php?IdNewsletter="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVer()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(0);
        else
            window.location.href="Ver.php?IdNewsletter="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}