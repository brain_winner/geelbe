function enableCalendar() {
	
	$('calendarPrev').onclick = function() {
		loadCalendar($('calendarPrevYear').value, $('calendarPrevMonth').value);
		return false;
	}
	
	$('calendarNext').onclick = function() {
		loadCalendar($('calendarNextYear').value, $('calendarNextMonth').value);
		return false;
	}
	
}

function loadCalendar(year, month) {

	url = 'calendar.php?month='+month+'&year='+year;
	$('calendarContainer').innerHTML = '';
	
	new Ajax.Request(url, {
		method: 'get',
		onSuccess: function(transport) {
			$('calendarContainer').innerHTML = transport.responseText
			enableCalendar();
		}
	});

}