<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
    try
    {
        ValidarUsuarioLogueadoBo(38);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
    	
	require_once '../lib/connect.php';

	if(isset($_GET['from']))
		$from = $_GET['from']."-01";
	else
		$from = date("Y-m-01");
	
	if(isset($_GET['to']))
		$to = $_GET['to']."-31";
	else
		$to = date("Y-m-31");

	function pedidosConcretados($from, $to) {
		global $mainDB;
	
		$q = mysql_query("SELECT COUNT(IdPedido) as Total FROM pedidos WHERE IdEstadoPedidos in(2,3,6,9) AND Fecha BETWEEN '".$from."' AND '".$to."'", $mainDB);
		$q = mysql_fetch_assoc($q);
		return ($q['Total'] > 0 ? $q['Total'] : 0);
	}

	function totalFacturado($from, $to) {
		global $mainDB;
	
		$q = mysql_query("SELECT SUM(Total) as Total FROM pedidos WHERE IdEstadoPedidos in(2,3,6,9) AND Fecha BETWEEN '".$from."' AND '".$to."'", $mainDB);
		$q = mysql_fetch_assoc($q);
		return number_format(($q['Total'] > 0 ? $q['Total'] : 0), 2, ',', '.');
	}

	function usuariosRegistrados($from, $to) {
		global $mainDB;
	
		$q = mysql_query("SELECT COUNT(NombreUsuario) as Total
		FROM usuarios
		WHERE FechaIngreso BETWEEN '".$from."' AND '".$to."'", $mainDB);
		$q = mysql_fetch_assoc($q);
		return $q['Total'];
	}

	function cantidadLogins($from, $to) {
		global $loginDB;
	
		$q = mysql_query("SELECT COUNT(l.idlogin) as Total
		FROM logins l
		WHERE l.Fecha BETWEEN '".$from."' AND '".$to."'", $loginDB);
		$q = mysql_fetch_assoc($q);
		return $q['Total'];
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title></title>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
		<link href="<?=UrlResolver::getCssBaseUrl("bo/estadisticas/css/calendar.css")?>" rel="stylesheet" type="text/css" />
		<style type="text/css">
			td { background-color: #ffefef }
		</style>
	</head>
	
	<body>

		<div id="container">
			<div id="main">
			
				<h1>Estad�sticas personalizadas</h1>
			
				<div id="mainStats">
					<table>
						<tr>
							<th>N&uacute;mero de pedidos concretados</th>
							<td><?=pedidosConcretados($from, $to);?></td>
						</tr>
						<tr>
							<th>Monto total facturado</th>
							<td>$<?=totalFacturado($from, $to);?></td>
						</tr>
						<tr>
							<th>Cantidad de usuarios registrados</th>
							<td><?=usuariosRegistrados($from, $to);?></td>
						</tr>
						<tr>
							<th>Cantidad de logins</th>
							<td><?=cantidadLogins($from, $to);?></td>
						</tr>
					</table>
					
					<br /><br />

					<form action="index.php" method="get" style="float: left;">
				
						<fieldset style=" padding: 10px">
							<legend>Per�odo</legend>
							
							<label>
								Entre
								<select name="from">
									<?php
										for($i = 2008; $i <= date("Y"); $i++)
											for($j = 1; $j <= 12; $j++) {
												if($j < 10) $j = '0'.$j;
											
												echo '<option value="'.$i.'-'.$j.'" '.($_GET['from'] == $i.'-'.$j ? 'selected="selected"' : '').'>'.strftime("%B de %Y", mktime(0, 0, 0, $j, 1, $i)).'</option>';
											}
									?>
								</select>
							</label>
							
							<label>
								y
								<select name="to">
									<?php
										for($i = 2008; $i <= date("Y"); $i++)
											for($j = 1; $j <= 12; $j++) {
												if($j < 10) $j = '0'.$j;
											
												echo '<option value="'.$i.'-'.$j.'" '.($_GET['to'] == $i.'-'.$j ? 'selected="selected"' : '').'>'.strftime("%B de %Y", mktime(0, 0, 0, $j, 1, $i)).'</option>';
											}
									?>
								</select>
							</label>
							
							<br /><br />
							
							<input type="submit" value=" Filtrar " />
						</fieldset>
						
					</form>
					
				</div>
				
			</div>

						
		</div>
        
	</body>
</html>
