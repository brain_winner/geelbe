<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
		$postURL = Aplicacion::getIncludes("post", "clientes");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(15);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }

    try {
		$countActivos = dmCliente::getClientesActivosCount(array());
		$countInactivos = dmCliente::getClientesInactivosCount(array());
		$countHuerfanos = dmCliente::getClientesHuerfanosCount(array());
		$countDesuscriptos = dmCliente::getClientesDesuscriptosCount(array());
		$countTotal = dmCliente::getClientesCount(array());
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }
?>	

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Clientes</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Clientes</h1>	

			<div id="mainStats">
					<table style="margin-bottom:10px;">
						<tr class="alt">
							<td style="text-align:left;font-weight:bold;">Descripción</td>
							<td style="font-weight:bold;">Total</td>
							<td style="font-weight:bold;">Porcentaje (%)</td>
						</tr>
						<tr>
							<td style="text-align:left;font-weight:bold;"><a href="/<?=$confRoot[1];?>/bo/clientes/index.php">Todos los Clientes</a></td>
							<td><?=$countTotal?></td>
							<td><?=round($countTotal*100/$countTotal,2)?>%</td>
						</tr>
						<tr class="alt">
							<td style="text-align:left;font-weight:bold;"><a href="/<?=$confRoot[1];?>/bo/clientes/index.php?userType=<?=Aplicacion::Encrypter("activos")?>">Clientes Activos</a></td>
							<td><?=$countActivos?></td>
							<td><?=round($countActivos*100/$countTotal,2)?>%</td>
						</tr>
						<tr>
							<td style="text-align:left;font-weight:bold;"><a href="/<?=$confRoot[1];?>/bo/clientes/index.php?userType=<?=Aplicacion::Encrypter("inactivos")?>">Clientes Inactivos</a></td>
							<td><?=$countInactivos?></td>
							<td><?=round($countInactivos*100/$countTotal,2)?>%</td>
						</tr>
						<tr class="alt">
							<td style="text-align:left;font-weight:bold;"><a href="/<?=$confRoot[1];?>/bo/clientes/index.php?userType=<?=Aplicacion::Encrypter("huerfanos")?>">Clientes Huerfanos</a></td>
							<td><?=$countHuerfanos?></td>
							<td><?=round($countHuerfanos*100/$countTotal,2)?>%</td>
						</tr>
						<tr>
							<td style="text-align:left;font-weight:bold;"><a href="/<?=$confRoot[1];?>/bo/clientes/index.php?userType=<?=Aplicacion::Encrypter("desuscriptos")?>">Clientes Desuscriptos</a></td>
							<td><?=$countDesuscriptos?></td>
							<td><?=round($countDesuscriptos*100/$countTotal,2)?>%</td>
						</tr>
					</table>
					
				</div>
		</div>	
	</body>
</html>
