<? 
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
?>
<div id="calendar">
	<?php
	
		echo '<script type="text/javascript" src="'.UrlResolver::getJsBaseUrl("bo/estadisticas/scripts/prototype.js").'"></script>';
		echo '<script type="text/javascript" src="'.UrlResolver::getJsBaseUrl("bo/estadisticas/scripts/calendar.js").'"></script>';
		
		if(!isset($_GET['month']) || !is_numeric($_GET['month']))
			$month = date("m");
		else
			$month = $_GET['month'];
			
		if(!isset($_GET['year']) || !is_numeric($_GET['year']))
			$year = date("Y");
		else
			$year = $_GET['year'];
		
		$time = mktime(0, 0, 0, $month, 1, $year);
		
		$nextMonth = $month+1;
		$nextYear = $year;
		
		if($nextMonth > 12) {
			$nextMonth = 1;
			$nextYear++;
		}
		
		$prevMonth = $month-1;
		$prevYear = $year;
		
		if($prevMonth < 1) {
			$prevMonth = 12;
			$prevYear--;
		}
		
		$monthNames = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

	?>
	
	<a href="?month=<?=$prevMonth;?>&amp;year=<?=$prevYear;?>" class="prev" id="calendarPrev">&lt;</a>
	
	<h2><?=$monthNames[($month-1)].' '.date("Y", $time);?></h2>
	
	<a href="?month=<?=$nextMonth;?>&amp;year=<?=$nextYear;?>" class="next" id="calendarNext">&gt;</a>
	
	<div class="content">
		<ul id="days">
			<li>
				<span title="Sunday">SU</span>
				<span title="Monday">MO</span>
				<span title="Tuesday">TU</span>
				<span title="Wednesday">WE</span>
				<span title="Thursday">TH</span>
				<span title="Friday">FR</span>
				<span title="Saturday">SA</span>
			</li>
		</ul>
		<!-- On links Below: NU = Not Used; NA = Not Active; AL = Active Link -->
		<?php
			
			$firstDay = date("w", $time);
			
			$day = 1;
			
			echo '<ul class="weeks"><li>';
			for($i = 1; $i <= 42; $i++) {
			
				if(($i-1) % 7 == 0) {
					if($i != 1)
						echo '</ul>';
						
					echo '<ul class="weeks"><li>';
				}
								
				if($day < 10)
					$pday = '0'.$day;
				else
					$pday = $day;
								
				if($i > $firstDay && checkdate($month, $day, $year)) {
				
					echo '<a class="al" href="?month='.$month.'&amp;year='.$year.'&amp;day='.$pday.'" title="'.$title.'">'.$pday.'</a>';
					$day++;
					
				} else {
					echo '<a class="nu" href="#" title="">--</a>';
				}
			
			}
			
			echo '</ul>';
		
		?>
	</div>
	
	<input type="hidden" id="calendarNextMonth" value="<?=$nextMonth;?>" />
	<input type="hidden" id="calendarNextYear" value="<?=$nextYear;?>" />
	<input type="hidden" id="calendarPrevMonth" value="<?=$prevMonth;?>" />
	<input type="hidden" id="calendarPrevYear" value="<?=$prevYear;?>" />
</div>
<script type="text/javascript">enableCalendar()</script>