<?
   	
	function printHeader($fecha) {
		echo '<tr>';
		echo '	<th NOWRAP>&nbsp;</th>';
		$idx = 1;
		$firstMonth = intval($fecha["mes"]);
		for ($i = intval($fecha["anio"]); $i <= intval(date("Y")); $i++) {
			$actualYear = ($i == intval(date("Y")));
			for ($j = $firstMonth; ((!$actualYear && $j <= 12) || ($actualYear && $j <= intval(date("n")))); $j++) {
				echo "<th NOWRAP>".$j."/".$i."</th>";
				$idx++;
			}
			$firstMonth = 1;
		}
		echo '	<th NOWRAP>Total</th>';
		echo '</tr>';
	}
	
		function printInfo($title, $list, $fechaCodInvitacion, $listTotal = null, $total = null) {
		
		echo '<tr>';
		echo '    <th NOWRAP>'.$title.'</th>';
		
		$sumTotal = 0;
		$firstMonth = intval($fechaCodInvitacion["mes"]);
		for ($i = intval($fechaCodInvitacion["anio"]); $i <= intval(date("Y")); $i++) {
			$actualYear = ($i == intval(date("Y")));
			for ($j = $firstMonth; ((!$actualYear && $j <= 12) || ($actualYear && $j <= intval(date("n")))); $j++) {
				$valorMensual = $list[$i."-".$j];
				if(!isset($valorMensual)) {
					$valorMensual = 0;
				}
				echo "    <td NOWRAP>".$valorMensual."</td>";
				$sumTotal += $valorMensual;
				if(isset($listTotal)) {
					$listTotal[$i."-".$j] += $valorMensual; 
				}
				$idx++;
			}
			$firstMonth = 1;
		}
		if(isset($total)) {
			echo '    <td NOWRAP>'.$total.'</td>';
		} else {
			echo '    <td NOWRAP>'.$sumTotal.'</td>';
			$total = $sumTotal;
		}
		echo '</tr>';
	}

	function printCurrencyInfo($title, $list, $fechaCodInvitacion, $listTotal = null, $total = null) {
		
		echo '<tr>';
		echo '    <th NOWRAP>'.$title.'</th>';
		
		$sumTotal = 0;
		$firstMonth = intval($fechaCodInvitacion["mes"]);
		for ($i = intval($fechaCodInvitacion["anio"]); $i <= intval(date("Y")); $i++) {
			$actualYear = ($i == intval(date("Y")));
			for ($j = $firstMonth; ((!$actualYear && $j <= 12) || ($actualYear && $j <= intval(date("n")))); $j++) {
				$valorMensual = $list[$i."-".$j];
				if(!isset($valorMensual)) {
					$valorMensual = 0;
				}
				echo "    <td NOWRAP>".Moneda($valorMensual)."</td>";
				$sumTotal += $valorMensual;
				if(isset($listTotal)) {
					$listTotal[$i."-".$j] += $valorMensual; 
				}
				$idx++;
			}
			$firstMonth = 1;
		}
		if(isset($total)) {
			echo '    <td NOWRAP>'.Moneda($total).'</td>';
		} else {
			echo '    <td NOWRAP>'.Moneda($sumTotal).'</td>';
			$total = $sumTotal;
		}
		echo '</tr>';
	}
	
	function printAverageInfo($title, $list, $fechaCodInvitacion = null, $total = null) {
		
		echo '<tr>';
		echo '    <th NOWRAP>'.$title.'</th>';
		
		$sumTotal = 0;
		$productosTotal = 0;
		$firstMonth = intval($fechaCodInvitacion["mes"]);
		for ($i = intval($fechaCodInvitacion["anio"]); $i <= intval(date("Y")); $i++) {
			$actualYear = ($i == intval(date("Y")));
			for ($j = $firstMonth; ((!$actualYear && $j <= 12) || ($actualYear && $j <= intval(date("n")))); $j++) {
				if($list[$i."-".$j]["productos"] == 0) {
					$valorMensual = 0;
				} else {
					if($list[$i."-".$j]["productos"] == 0) {
						$valorMensual = 0;
					} else {
						$valorMensual = $list[$i."-".$j]["valor"]/$list[$i."-".$j]["productos"];
						$sumTotal += $list[$i."-".$j]["valor"];
						$productosTotal += $list[$i."-".$j]["productos"];
					}
				}
				if(!isset($valorMensual)) {
					$valorMensual = 0;
				}
				echo "    <td NOWRAP>".number_format($valorMensual, 4, ',', '')."</td>";
				$idx++;
			}
			$firstMonth = 1;
		}
		if(isset($total)) {
			echo '    <td NOWRAP>'.number_format($total, 4, ',', '').'</td>';
		} else {
			if($productosTotal == 0) {
				echo '    <td NOWRAP>'.number_format(0, 4, ',', '').'</td>';
			} else {
				echo '    <td NOWRAP>'.number_format($sumTotal/$productosTotal, 4, ',', '').'</td>';
			}
		}
		echo '</tr>';
	}
	
	function printAverageInfoInCurrencyFormat($title, $list, $fechaCodInvitacion = null, $total = null) {
		
		echo '<tr>';
		echo '    <th NOWRAP>'.$title.'</th>';
		
		$sumTotal = 0;
		$productosTotal = 0;
		$firstMonth = intval($fechaCodInvitacion["mes"]);
		for ($i = intval($fechaCodInvitacion["anio"]); $i <= intval(date("Y")); $i++) {
			$actualYear = ($i == intval(date("Y")));
			for ($j = $firstMonth; ((!$actualYear && $j <= 12) || ($actualYear && $j <= intval(date("n")))); $j++) {
				if($list[$i."-".$j]["productos"] == 0) {
					$valorMensual = 0;
				} else {
					$valorMensual = $list[$i."-".$j]["valor"]/$list[$i."-".$j]["productos"];
					$sumTotal += $list[$i."-".$j]["valor"];
					$productosTotal += $list[$i."-".$j]["productos"];
				}
				if(!isset($valorMensual)) {
					$valorMensual = 0;
				}
				echo "    <td NOWRAP>".Moneda($valorMensual)."</td>";
				$idx++;
			}
			$firstMonth = 1;
		}
		if(isset($total)) {
			echo '    <td NOWRAP>'.Moneda($total).'</td>';
		} else {
			if($productosTotal == 0) {
				echo '    <td NOWRAP>'.Moneda(0).'</td>';
			} else {
				if($productosTotal == 0) {
					echo '    <td NOWRAP>'.Moneda(0).'</td>';
				} else {
					echo '    <td NOWRAP>'.Moneda($sumTotal/$productosTotal).'</td>';
				}
			}
		}
		echo '</tr>';
	}
	
	function avg($ammounts, $quantity) {
		if($quantity == 0) {
			return 0;
		} else {
			return number_format($ammounts / $quantity, 4, ',', '');
		}
	}
	
	function calculateAverage($ammounts, $quantity) {
		$average = array();
		foreach($ammounts as $key => $value) {
			if($quantity[$key] == 0) {
				$average[$key] = 0;
			} else {
				if($quantity[$key] == 0) {
					$average[$key] = number_format(0, 4, ',', ''); 
				} else {
					$average[$key] = number_format($ammounts[$key] / $quantity[$key], 4, ',', ''); 
				}
			}
		}
		
		return $average;
	}
	
	function obtenerValorRecord($tipoEstadistica = null, $fechaEstadistica = null) {
		$query = 'SELECT valor_estadistica, fecha_record FROM records_estadisticas where tipo_estadistica = "'.$tipoEstadistica.'" and fecha_estadistica = "'.$fechaEstadistica.'"';

	    $oConexion = Conexion::nuevo();
		$oConexion->setQuery($query);
		$res = $oConexion->DevolverQuery();

		if($res[0]["valor_estadistica"] == null) {
			return null;
		}
		
		return $res[0];
	}
	
	function guardarValorRecord($tipoEstadistica = null, $fechaEstadistica = null, $valorEstadistica = null, $fechaRecord = null) {
		$query = 'INSERT INTO records_estadisticas (tipo_estadistica, fecha_estadistica, valor_estadistica, fecha_record) VALUES ("'.$tipoEstadistica.'", "'.$fechaEstadistica.'", "'.$valorEstadistica.'", "'.$fechaRecord.'")';

	    $oConexion = Conexion::nuevo();
		$oConexion->setQuery($query);
		$oConexion->EjecutarQuery();
	}
?>