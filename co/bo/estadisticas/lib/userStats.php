<?php
    set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
	require_once 'Zend/Cache.php';

	//Estadisticas
	function totalUsuarios($db, $dia = null) {
		if($dia != null)
		    $w2 = " WHERE `FechaIngreso` BETWEEN '".$dia." 00:00:00' AND '".$dia." 23:59:59'";
		else
			$w2 = '';
			
		$q = mysql_query('SELECT count(`idUsuario`) as total FROM `usuarios`'.$w2, $db);
		$d = mysql_fetch_assoc($q);
		
		return $d['total'];
	}
	
	function totalUsuariosActivos($db) {
		$q = mysql_query('
			select count(usuarios.IdUsuario) as totalUsuarioActivos
			from usuarios inner join datosusuariospersonales on usuarios.IdUsuario=datosusuariospersonales.IdUsuario 
			where usuarios.es_activa=0 and usuarios.IdPerfil=1', $db);
		$d = mysql_fetch_assoc($q);
		
		return $d['totalUsuarioActivos'];
	}
	
	function totalUsuariosMasculinos($db) {
		$q = mysql_query('
			select count(usuarios.IdUsuario) as totalUsuariosHombre 
			from usuarios inner join datosusuariospersonales on usuarios.IdUsuario=datosusuariospersonales.IdUsuario 
			where usuarios.es_activa=0 and usuarios.IdPerfil=1 and datosusuariospersonales.idTratamiento = 1', $db);
		$d = mysql_fetch_assoc($q);
		
		return $d['totalUsuariosHombre'];
	}
	
	function totalUsuariosFemeninos($db) {
		$q = mysql_query('
			select count(usuarios.IdUsuario) as totalUsuariosMujer
			from usuarios inner join datosusuariospersonales on usuarios.IdUsuario=datosusuariospersonales.IdUsuario 
			where usuarios.es_activa=0 and usuarios.IdPerfil=1 and datosusuariospersonales.idTratamiento = 2', $db);
		$d = mysql_fetch_assoc($q);
		
		return $d['totalUsuariosMujer'];
	}
	
	function totalUsuariosGeneroDesconocido($db) {
		$q = mysql_query("
			select count(usuarios.IdUsuario) as totalUsuariosDesconocidos
			from usuarios inner join datosusuariospersonales on usuarios.IdUsuario=datosusuariospersonales.IdUsuario 
			where usuarios.es_activa=0 and usuarios.IdPerfil=1 and (datosusuariospersonales.idTratamiento IS NULL or datosusuariospersonales.idTratamiento='')", $db);
		$d = mysql_fetch_assoc($q);
		
		return $d['totalUsuariosDesconocidos'];
	}
	
	function promedioDeEdadUsuariosActivos($db) {
		$q = mysql_query("SELECT AVG(DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(FechaNacimiento)), '%Y')+0) AS promedio FROM datosusuariospersonales", $db);
		$d = mysql_fetch_assoc($q);
		
		return $d['promedio'];
	}
	
	function desviacionEstandarDeEdadUsuariosActivos($db) {
		$q = mysql_query("SELECT STDDEV(DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(FechaNacimiento)), '%Y')+0) AS dsvest FROM datosusuariospersonales", $db);
		$d = mysql_fetch_assoc($q);
		
		return $d['dsvest'];
	}
	
	function totalUsuariosConDireccionCargada($db) {
		$q = mysql_query("select count(datosusuariosdirecciones.IdProvincia) as total from datosusuariosdirecciones", $db);
		$d = mysql_fetch_assoc($q);
		
		return $d['total'];
	}
	
	function totalUsuariosPorProvincia($db) {
		$q = mysql_query("
			select provincias.Nombre as NombreProvincia, count(provincias.IdProvincia) as TotalUsuarios
			from usuarios 
			inner join datosusuariosdirecciones on usuarios.IdUsuario=datosusuariosdirecciones.IdUsuario
			inner join provincias on provincias.IdProvincia=datosusuariosdirecciones.IdProvincia
			group by provincias.IdProvincia
			order by provincias.Nombre", $db);
		return $q;	
	}
	
	function invitacionesTotales($db, $activas = false, $dia = null) {
		if($dia != null)
			$w2 = " WHERE invitation_date BETWEEN '".$dia." 00:00:00' AND '".$dia." 23:59:59'";
		else
			$w2 = '';

		$q = mysql_query('SELECT count(id) as total FROM geelbe_sent_invitations '.$w2, $db);
		$d = mysql_fetch_assoc($q);
		
		return $d['total'];
	}
	
	function invitacionesTotalesPorDia($db, $returnData = false) {

		$q = mysql_query('SELECT COUNT(`email`) as `total`, `fecha` FROM `social_invites` GROUP BY `fecha` ORDER BY `fecha` ASC', $db);
		
		if($returnData)
			return $q;
		else
			return mysql_num_rows($q);
	}
	
	function invitacionesUnicas($db, $activas = false, $dia = null) {
		if($dia != null)
			$w2 = " WHERE invitation_date BETWEEN '".$dia." 00:00:00' AND '".$dia." 23:59:59'";
		else
			$w2 = '';
			
		$q = mysql_query('SELECT count(distinct(invitation_to)) as total FROM geelbe_sent_invitations '.$w2, $db);
		$d = mysql_fetch_assoc($q);
		
		return $d['total'];
	}
	
	function invitacionesUnicasPorDia($db, $returnData = false) {
			
		$q = mysql_query('SELECT COUNT(DISTINCT(`email`)) as `total`, `fecha` FROM `social_invites` GROUP BY `fecha` ORDER BY `fecha` ASC', $db);
		
		if($returnData)
			return $q;
		else
			return mysql_num_rows($q);
		
	}
	
	function porcentajeActivasTotales($db, $dia = null) {
		
		$total = invitacionesTotales($db, false, $dia);
		$activas = invitacionesTotales($db, true, $dia);
	
		if($total > 0)	
			return ($activas*100)/$total;
		else
			return 0;
		
	}
	
	function porcentajeActivasUnicas($db, $dia = null) {
		
		$total = invitacionesUnicas($db, false, $dia);
		$activas = invitacionesUnicas($db, true, $dia);
	
		if($total > 0)	
			return ($activas*100)/$total;
		else
			return 0;
		
	}
	
	function diaMasInvitaciones($db, $activas = false) {
	   	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 28800, 'automatic_serialization' => true), array('cache_dir' => $cacheDir));
			
		if(!$contenidoCache = $cache->load('cache_dias_mas_invitaciones')) {
			$q = mysql_query('SELECT date(invitation_date) as fecha, count(id) as total FROM geelbe_sent_invitations where invitation_date != "0000-00-00 00:00:00" group by date(invitation_date) order by total desc limit 1', $db);
			$d = mysql_fetch_assoc($q);
			$cache->save($d, 'cache_dias_mas_invitaciones');
		} else {
			$d = $contenidoCache;
		}

		$time = strtotime($d['fecha']);
		return array(date("d-m-y", $time), $d['total']);
	}
	
	function diaMasUsuarios($db) {
		
	   	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 28800, 'automatic_serialization' => true), array('cache_dir' => $cacheDir));
		
		if(!$contenidoCache = $cache->load('cache_dias_mas_usuarios')) {
			$q = mysql_query('SELECT date(`FechaIngreso`) as fecha, count(`IdUsuario`) as total FROM usuarios group by date(`FechaIngreso`) order by total desc limit 1', $db);
			$d = mysql_fetch_assoc($q);
			$cache->save($d, 'cache_dias_mas_usuarios');
		} else {
			$d = $contenidoCache;
		}

		$time = strtotime($d['fecha']);
		return array(date("d-m-y", $time), $d['total']);
	}
	
	//Invitaciones que salieron y sus padrinos se activaron
	function salidasActivas($db) {
		$q = mysql_query("SELECT COUNT(email) FROM `social_invites` WHERE `sent` = 1 AND `es_activa` = 0", $db);
		
		return mysql_num_rows($q);
	}
	
	//Invitaciones a Hotmail que salieron y sus padrinos se activaron
	function salidasActivasHotmail($db) {
		$q = mysql_query("SELECT COUNT(email) FROM `social_invites` WHERE `sent` = 1 AND `es_activa` = 0 AND `email` like '%hotmail%'", $db);
		
		return mysql_num_rows($q);
	}
	
	//Invitaciones que no salieron y deberian haber salido de todos los usuarios
	function noSalidas($db) {
		$q = mysql_query("SELECT COUNT(email) FROM `social_invites` WHERE `sent` = 0 AND `es_activa` = 0", $db);
		
		return mysql_num_rows($q);
	}
	
	//Invitaciones que no salieron y deberian haber salido de y que son usuarios hotmail
	function noSalidasHotmail($db) {
		$q = mysql_query("SELECT COUNT(email) FROM `social_invites` WHERE `sent` = 0 AND `es_activa` = 0 AND `email` like '%hotmail%'", $db);
		
		return mysql_num_rows($q);
	}
	
	//Función Auxiliar
	function graficoHistorico($data, $width = 680) {
		
	    include_once('phpMyGraph4.0.php');
	    header("Content-type: image/png");
	    
	    $graph = new phpMyGraph();
	    $graph->parseVerticalColumnGraph($data, array('width' => $width, 'random-column-color'=>1));
	    
	}
	
	//Gráficos
	function invitacionesTotalesG($db) {
	     
	    $total = invitacionesTotales($db);
	    $active = invitacionesTotales($db, true);
	     
	    //Datos para generación de gráfico
	    $data = array 
	    ( 
	        'Activas' => $active, 
	        'Inactivas' => ($total-$active)
	    ); 
	     
	    graficoHistorico($data);
	    
	}
	
	function invitacionesUnicasG($db) {
	     
	    $total = invitacionesUnicas($db);
	    $active = invitacionesUnicas($db, true);
	     
	    //Datos para generación de gráfico
	    $data = array 
	    ( 
	        'Activas' => $active, 
	        'Inactivas' => ($total-$active)
	    ); 
	     
		graficoHistorico($data);
	    
	}
	
	function invitacionesUnicasPorDiaG($db) {
	
		$total = invitacionesUnicasPorDia($db, true);
		
		//Datos para generación de gráfico
	    $data = array();
	    while($d = mysql_fetch_assoc($total)) {
	    	$fecha = explode('-', $d['fecha']);
	    	$data[$fecha[2].'/'.$fecha[1].'/'.$fecha[0]] = $d['total'];
	    }
		
		graficoHistorico($data);
		
	}
	
	function invitacionesTotalesPorDiaG($db) {
	
		$total = invitacionesTotalesPorDia($db, true);
		
		//Datos para generación de gráfico
	    $data = array();
	    while($d = mysql_fetch_assoc($total)) {
	    	$fecha = explode('-', $d['fecha']);
	    	$data[$fecha[2].'/'.$fecha[1].'/'.$fecha[0]] = $d['total'];
	    }
	    
		graficoHistorico($data);
		
	}
	
	function salidasActivasG($db) {
		
		$data = array(
			'Total' => invitacionesTotales($db),
			'Salidas Activas' => salidasActivas($db)
		);
		
		graficoHistorico($data);
		
	}
	
	function salidasActivasUnicasG($db) {
		
		$data = array(
			'Total Únicas' => invitacionesUnicas($db),
			'Salidas Activas' => salidasActivas($db)
		);
		
		graficoHistorico($data);
		
	}
	
	function salidasActivasHotmailG($db) {
		
		$data = array(
			'Total' => invitacionesTotales($db),
			'Salidas Activas Hotmail' => salidasActivasHotmail($db)
		);
		
		graficoHistorico($data);
		
	}
	
	function noSalidasG($db) {
		
		$data = array(
			'Total' => invitacionesTotales($db),
			'No salidas' => noSalidas($db)
		);
		
		graficoHistorico($data);
		
	}
	
	function noSalidasHotmailG($db) {
		
		$data = array(
			'Total' => invitacionesTotales($db),
			'No salidas Hotmail' => noSalidasHotmail($db)
		);
		
		graficoHistorico($data);
		
	}
	
?>