<?php

	function recaudacionPedidos($db, $dia = null) {
		if($dia != null) {
		    $w2 = " AND `pedidos`.`FechaSistemaPago` BETWEEN '".$dia." 00:00:00' AND '".$dia." 23:59:59'";
		    $w = " AND `pedidos`.`Fecha` BETWEEN '".$dia." 00:00:00' AND '".$dia." 23:59:59'";
	    } else {
			$w = '';
			$w2 = '';
		}
	
		$q = mysql_query('SELECT SUM(pedidos.Total) as total, COUNT(pedidos.IdPedido) as nroPedidos
					FROM pedidos
					WHERE ((pedidos.IdEstadoPedidos = 2 OR pedidos.IdEstadoPedidos = 3 OR pedidos.IdEstadoPedidos = 6 OR pedidos.IdEstadoPedidos = 6) '.$w2.')'.
			              ' OR (pedidos.IdEstadoPedidos = 9 '.$w.')', $db);
				
		$d = mysql_fetch_assoc($q);
		
		return $d;
	}
	
	function diaMasRecaudacionPedidos($db) {
				
	   	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 28800, 'automatic_serialization' => true), array('cache_dir' => $cacheDir));
			
		if(!$contenidoCache = $cache->load('cache_dias_mas_recaudaciones_pedidos')) {
			$q = mysql_query('SELECT date(FechaSistemaPago) as fecha, SUM(Total) as total FROM pedidos where FechaSistemaPago IS NOT NULL AND (IdEstadoPedidos = 2 OR IdEstadoPedidos = 3 OR IdEstadoPedidos = 6 OR IdEstadoPedidos = 9) group by date(FechaSistemaPago) order by total desc limit 1', $db);
			$d = mysql_fetch_assoc($q);
			$cache->save($d, 'cache_dias_mas_recaudaciones_pedidos');
		} else {
			$d = $contenidoCache;
		}

		$time = strtotime($d['fecha']);
		return array(date("d-m-y", $time), $d['total']);
	}
	

	// @deprecated
	
	//Estadísticas
	function productosVendidos($db, $dia = null) {
			
		if($dia != null)
			$w2 = " AND `pedidos`.`Fecha` LIKE '".$dia."%'";
		else
			$w2 = '';
	
		$q = mysql_query('SELECT COUNT(`productos_x_pedidos`.`IdCodigoProdInterno`) as total
		FROM `productos_x_pedidos`, `pedidos`
		WHERE `productos_x_pedidos`.`IdPedido` = `pedidos`.`IdPedido` '.$w2, $db);
				
		$d = mysql_fetch_assoc($q);
		
		return $d['total'];
	}
	
	function pedidosConfirmados($db, $dia = null) {
			
		if($dia != null)
			$w2 = " AND `pedidos`.`Fecha` LIKE '".$dia."%'";
		else
			$w2 = '';
	
		$q = mysql_query('SELECT COUNT(`pedidos`.`IdPedido`) as total FROM `pedidos` WHERE `pedidos`.`IdEstadoPedidos` IN (2, 9) '.$w2, $db);
				
		$d = mysql_fetch_assoc($q);
		
		return $d['total'];
	}
	
	function totalEnVentas($db, $dia = null) {
			
		if($dia != null)
			$w2 = " WHERE `pedidos`.`Fecha` LIKE '".$dia."%'";
		else
			$w2 = '';
	
		$q = mysql_query('SELECT SUM(`pedidos`.`total`) as total FROM `pedidos` '.$w2, $db);
				
		$d = mysql_fetch_assoc($q);
		
		return (is_numeric($d['total']) ? $d['total'] : 0);
	}
	
	function gananciasNetas($db, $dia = null) {
			
		if($dia != null)
			$w2 = " AND `pedidos`.`Fecha` LIKE '".$dia."%'";
		else
			$w2 = '';
	
		$q = mysql_query('SELECT SUM(`productos`.`PVenta`-`productos`.`PCompra`) as total
		FROM `pedidos`, `productos_x_pedidos`, `productos`, `productos_x_proveedores`
		WHERE `productos_x_pedidos`.`IdPedido` = `pedidos`.`IdPedido`
		AND `productos_x_proveedores`.`IdCodigoProdInterno` = `productos_x_pedidos`.`IdCodigoProdInterno`
		AND `productos`.`IdProducto` = `productos_x_proveedores`.`IdProducto` '.$w2, $db);
				
		$d = mysql_fetch_assoc($q);
		
		return (is_numeric($d['total']) ? $d['total'] : 0);
	}

?>