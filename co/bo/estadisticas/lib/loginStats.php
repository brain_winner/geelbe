<?php
    set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
	require_once 'Zend/Cache.php';

	//Estadisticas
	function totalLogins($db, $dia = null) {
		if($dia != null)
			$w2 = " WHERE `Fecha` BETWEEN '".$dia." 00:00:00' AND '".$dia." 23:59:59'";
		else
			$w2 = '';
			
		$q = mysql_query('SELECT count(`idLogin`) as total FROM `logins`'.$w2, $db);
		$d = mysql_fetch_assoc($q);
		
		return $d['total'];
	}
	
	function diaMasLogins($db) {

		$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 28800, 'automatic_serialization' => true), array('cache_dir' => $cacheDir));
			
		if(!$contenidoCache = $cache->load('cache_dias_mas_logins')) {
			$q = mysql_query('SELECT `Fecha` as fecha, count(`idLogin`) as total FROM `logins` group by date(`Fecha`) order by total desc limit 1', $db);
			$d = mysql_fetch_assoc($q);
			$cache->save($d, 'cache_dias_mas_logins');
		} else {
			$d = $contenidoCache;
		}
					
		$time = strtotime($d['fecha']);
		return array(date("d-m-y", $time), $d['total']);
	}

	
?>