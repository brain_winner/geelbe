<?php 
function post($key, $array = null) {
	
	if(is_null($array))
		return (isset($_REQUEST[$key]) ? $_REQUEST[$key] : null);
	else
		return (isset($_REQUEST[$key], $_REQUEST[$key][$array]) ? $_REQUEST[$key][$array] : null);
	
}

function backToDailyStatsUrl() {
	return "../diarias.php";
}

function getUserDetailUrl() {
	return "users.php?".
		"month=".$_GET['month'].
		"&year=".$_GET['year'].
		"&day=".$_GET['day'];
}

function getLoginsDetailUrl() {
	return "logins.php?".
		"month=".$_GET['month'].
		"&year=".$_GET['year'].
		"&day=".$_GET['day'];	
}

function getInvitesDetailUrl() {
	return "invites.php?".
		"month=".$_GET['month'].
		"&year=".$_GET['year'].
		"&day=".$_GET['day'];
}

?>