<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
    try
    {
        ValidarUsuarioLogueadoBo(19);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
    	
	require_once 'lib/connect.php';
	require_once 'lib/loginStats.php';
	require_once 'lib/salesStats.php';
	require_once 'lib/userStats.php';

	if(isset($_GET['month'], $_GET['year'], $_GET['day']))
		$time = mktime(0, 0, 0, $_GET['month'], $_GET['day'], $_GET['year']);
	else
		$time = mktime();
	
	$qry = '?day='.date("d", $time).'&amp;month='.date("m", $time).'&amp;year='.date("Y", $time);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title></title>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
		<link href="<?=UrlResolver::getCssBaseUrl("bo/estadisticas/css/calendar.css")?>" rel="stylesheet" type="text/css" />
	</head>
	
	<body>

		<div id="container">
			<div id="main">
			
				<h1>Estadísticas <?=strftime("%A %d de %B de %Y", $time);?></h1>
			
				<div id="mainStats">
					<table>
						<tr>
							<th></th>
							<th>Hoy (total)</th>
							<th>Hoy (únicas)</th>
							<th>Total</th>
							<th>Mayor actividad <span style="margin-top:5px;color:red;font-weight:bold;">*</span></th>
						</tr>
						
						<tr class="alt">
							<th class="left"><a href="details/users.php<?=$qry;?>">Cantidad de registrados</a></th>
							<td><?=totalUsuarios($mainDB, date("Y-m-d", $time));?></td>
							<td class="disabled">N/A</td>
							<td><?=totalUsuarios($mainDB);?></td>
							<td><?php $d = diaMasUsuarios($mainDB); echo $d[0].'<br />('.$d[1].')'; ?></td>
						</tr>
						
						<tr>
							<th class="left"><a href="details/logins.php<?=$qry;?>">Cantidad de logins</a></th>
							<td><?=totalLogins($loginDB, date("Y-m-d", $time));?></td>
							<td class="disabled">N/A</td>
							<td><?=totalLogins($loginDB);?></td>
							<td><?php $d = diaMasLogins($loginDB); echo $d[0].'<br />('.$d[1].')'; ?></td>
						</tr>
						
						<tr class="alt">
							<th class="left"><a href="details/invites.php<?=$qry;?>">Cantidad de invitaciones</a></th>
							<td><?=invitacionesTotales($mainDB, false, date("Y-m-d", $time));?></td>
							<td><?=invitacionesUnicas($mainDB, false, date("Y-m-d", $time));?></td>
							<td><?=invitacionesTotales($mainDB, false);?></td>
							<td><?php $d = diaMasInvitaciones($mainDB); echo $d[0].'<br />('.$d[1].')'; ?></td>
						</tr>
						
						<tr>
							<th class="left">$ Pedidos con pago aceptado</th>
							<td><? $d=recaudacionPedidos($mainDB, date("Y-m-d", $time)); echo "$".$d['total']."<br />(".$d['nroPedidos']." pedidos)";?></td>
							<td class="disabled">N/A</td>
							<td><? $d=recaudacionPedidos($mainDB); echo "$".$d['total']."<br />(".$d['nroPedidos']." pedidos)";?></td>
							<td><?php $d = diaMasRecaudacionPedidos($mainDB); echo $d[0].'<br />('."$".$d[1].')'; ?></td>
						</tr>
					</table>
					
					<p style="margin-top:5px;color:red;font-weight:bold;">* NOTA: la informaci&oacute;n del campo "Mayor Actividad" se actualiza cada 8 horas.</p>
					<br /><br />
					<p><a href="details/usersByGenre.php">Ver reporte de usuarios activos por g&eacute;nero</a></p>
					<p><a href="details/usersByAge.php">Ver reporte de usuarios activos por edades</a></p>
					<p><a href="details/usersByProvince.php">Ver reporte de usuarios activos por provincia/estado</a></p>
					<p><a href="details/usersxdia.php">Ver reporte de usuarios activos por d&iacute;a</a></p>
				</div>
				
			</div>

			<div id="extra">
				
				<div id="calendarContainer">
					<?php require 'calendar.php'; ?>
				</div>
						
			</div>
						
		</div>
        
	</body>
</html>
