<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
    try
    {
        ValidarUsuarioLogueadoBo(36);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
    	
	require_once '../lib/connect.php';

	if(isset($_GET['month'], $_GET['year']))
		$time = mktime(0, 0, 0, $_GET['month'], 01, $_GET['year']);
	else {
		$time = mktime();
		$_GET['month'] = date("m");
		$_GET['year'] = date("Y");
	}
	
	function ventaAcumulada($m, $y) {
		global $time, $mainDB;
	
		$q = mysql_query("SELECT SUM(Total) as Total FROM pedidos WHERE IdEstadoPedidos in(2,3,6,9) AND Fecha BETWEEN '".date("Y-m-01", $time)."' AND NOW()", $mainDB);
		$q = mysql_fetch_assoc($q);
		return number_format(($q['Total'] > 0 ? $q['Total'] : 0), 2, ',', '.');
	}

	function margenPromedio($m, $y) {
		global $time, $mainDB;
	
		$q = mysql_query("SELECT AVG(pd.PVenta - pd.PCompra) as Total
		FROM pedidos p
		INNER JOIN productos_x_pedidos pxp ON pxp.IdPedido = p.IdPedido
		INNER JOIN productos_x_proveedores pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
		INNER JOIN productos pd ON pd.IdProducto = pxpr.IdProducto
		WHERE p.IdEstadoPedidos in(2,3,6,9) AND Fecha BETWEEN '".date("Y-m-01", $time)."' AND NOW()", $mainDB);
		$q = mysql_fetch_assoc($q);
		return number_format(($q['Total'] > 0 ? $q['Total'] : 0), 2, ',', '.');
	}

	function descuentoPromedio($m, $y) {
		global $time, $mainDB;
	
		$q = mysql_query("SELECT AVG(pd.PVP - pd.PVenta) as Total
		FROM pedidos p
		INNER JOIN productos_x_pedidos pxp ON pxp.IdPedido = p.IdPedido
		INNER JOIN productos_x_proveedores pxpr ON pxpr.IdCodigoProdInterno = pxp.IdCodigoProdInterno
		INNER JOIN productos pd ON pd.IdProducto = pxpr.IdProducto
		WHERE p.IdEstadoPedidos in(2,3,6,9) AND Fecha BETWEEN '".date("Y-m-01", $time)."' AND NOW()", $mainDB);
		$q = mysql_fetch_assoc($q);
		return number_format(($q['Total'] > 0 ? $q['Total'] : 0), 2, ',', '.');
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title></title>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
		<link href="<?=UrlResolver::getCssBaseUrl("bo/estadisticas/css/calendar.css")?>" rel="stylesheet" type="text/css" />
	</head>
	
	<body>

		<div id="container">
			<div id="main">
			
				<h1>Estad�sticas comerciales <?=strftime("%B de %Y", $time);?></h1>
			
				<div id="mainStats">
					<table>
						<tr>
							<th>Venta acumulada</th>
							<th>Margen de Ganancia Promedio</th>
							<th>Descuento promedio</th>
						</tr>
						
						<tr class="alt">
							<td>$<?=ventaAcumulada(date("m", $time), date("Y", $time));?></td>
							<td>$<?=margenPromedio(date("m", $time), date("Y", $time));?></td>
							<td>$<?=descuentoPromedio(date("m", $time), date("Y", $time));?></td>
						</tr>
					</table>
					
					<br /><br />

					<form action="index.php" method="get" style="float: left;">
				
						<fieldset style=" padding: 10px">
							<legend>Per�odo</legend>
							
							<label for="month" style="display: block; float: left; margin-right: 10px;">
								Mes<br />
								<select name="month" id="month">								
									<?php
										for($i = 1; $i <= 12; $i++)
											echo '<option value="'.$i.'" '.($i == $_GET['month'] ? 'selected="selected"' : '').'>'.strftime("%B", mktime(0, 0, 0, $i)).'</option>';
									?>								
								</select>
							</label>
							
							<label for="year" style="display: block">
								A&ntilde;o<br />
								<select name="year" id="year">								
									<?php
										for($i = 2008; $i <= date("Y"); $i++)
											echo '<option value="'.$i.'" '.($i == $_GET['year'] ? 'selected="selected"' : '').'>'.$i.'</option>';
									?>								
								</select>
							</label>
							
							<br /><br />
							
							<input type="submit" value=" Filtrar " />
						</fieldset>
						
					</form>
					
				</div>
				
			</div>

						
		</div>
        
	</body>
</html>
