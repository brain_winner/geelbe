<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(16);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
    try {
		if(!isset($_REQUEST['clearFilterButton'])) {
			$paramArray = ListadoUtils::generateFiltersParamArray(array('U.NombreUsuario', 'DUP.Apellido', 'DUP.Nombre', 'U.Padrino'));
		} else {
			$paramArray = array();
		}
		$sortArray = ListadoUtils::generateSortParamArray("count(A.idUsuario)", "DESC");

		$oEstadistica = new dmEstadisticas();
		$count = $oEstadistica->Invitaciones()->UsuariosQueInvitaronCount($paramArray);
		$listPager = new ListadoPaginator($count);
		$rows = $oEstadistica->Invitaciones()->UsuariosQueInvitaronPaginados($paramArray, $sortArray, $listPager->getOffset(), $listPager->getItemsPerPage());  
    }
    catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		echo $e;
		exit;
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=Aplicacion::getParametros("info", "nombre");?> Estadisticas</title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
    
		<? Includes::Scripts(); ?>
		<script src="<?=UrlResolver::getJsBaseUrl("bo/estadisticas/js.js");?>" type="text/javascript"></script>

	</head>

	<body>

		<div id="container" class="listadocontainer">
			<h1>Estadisticas</h1>	
			
			<?php
				$listadoPrinter = new ListadoPrinter($rows, "usuariosinvitaciones.php", $listPager);
				$listadoPrinter->showColumn("U.NombreUsuario", "E-Mail");
				$listadoPrinter->showColumn("DUP.Apellido", "Apellido");
				$listadoPrinter->showColumn("DUP.Nombre", "Nombre");				
				$listadoPrinter->showColumn("Invitaciones", "Invitaciones");
				
				$listadoPrinter->setSortName("Invitaciones", "count(A.idUsuario)");
				
				$listadoPrinter->setShowFilter("Invitaciones", false);
				
				$listadoPrinter->linkColumn("U.NombreUsuario", "/".$confRoot[1]."/bo/clientes/Ver.php?IdUsuario=[[A.idUsuario]]", "Ver Cliente");

				$listadoPrinter->printListado();
			?>
		</div>
		
	</body>
</html>
