<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
   	Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticasgenerales"));

	if(!isset($_GET['month'], $_GET['year'], $_GET['day'])) {
		die('Faltan parámetros.');
	}	
		
	$time = mktime(0, 0, 0, $_GET['month'], $_GET['day'], $_GET['year']);

	if(!isset($_REQUEST['clearFilterButton'])) {
		$paramArray = ListadoUtils::generateFiltersParamArray(array('Apellido', 'Nombre', 'NombreUsuario', 'Padrino'));
	} else {
		$paramArray = array();
	}
	$sortArray = ListadoUtils::generateSortParamArray("invitation_date", "DESC");
	
	$dataMapper = new dmEstadisticasGenerales();
    $count = $dataMapper->getInvitacionesPorFechaCount($_REQUEST['day'], $_REQUEST['month'], $_REQUEST['year'], $paramArray);
    $listadoPaginator = new ListadoPaginator($count['numberOfRows']);
    $invites = $dataMapper->getInvitacionesPorFecha($_REQUEST['day'], $_REQUEST['month'], $_REQUEST['year'], $listadoPaginator->getOffset(), $listadoPaginator->getItemsPerPage(), $paramArray, $sortArray);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title></title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
		<link href="<?=UrlResolver::getCssBaseUrl("bo/estadisticas/css/calendar.css")?>" rel="stylesheet" type="text/css" />
	</head>
	
	<body>

		<div id="container" class="cusers">
			
			<h1>Invitaciones enviadas el <?=strftime("%A %d de %B de %Y", $time);?></h1>
			
			<?php
				$listadoPrinter = new ListadoPrinter($invites, getInvitesDetailUrl(), $listadoPaginator);
				
				$listadoPrinter->showColumn("invitation_from", "Usuario");
				$listadoPrinter->showColumn("invitation_to", "Destinatario");
				$listadoPrinter->showColumn("Hora");
				$listadoPrinter->setShowFilter("Hora", false);
				$listadoPrinter->linkColumn("invitation_to", "/".$confRoot[1]."/bo/clientes/Ver.php?NombreUsuario=[[invitation_from]]");
				$listadoPrinter->addHiddenValue("day", $_REQUEST['day']);
				$listadoPrinter->addHiddenValue("month", $_REQUEST['month']);
				$listadoPrinter->addHiddenValue("year", $_REQUEST['year']);
				
				$listadoPrinter->printListado();
			?>
			<br />
			<a href="<?=backToDailyStatsUrl();?>">&lt;&lt; Volver</a>		
		</div>	
	</body>
</html>
