<?php

	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
   	Aplicacion::CargarIncludes(Aplicacion::getIncludes("partnerscod"));
		$postURL = Aplicacion::getIncludes("post", "clientes");
		
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
	} 
	catch(Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(19);
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1].'/bo');
		exit;
    }
    
    if(isset($_POST['cod'])) {
    	$_SESSION['codFilter'] = $_POST['cod'];
    }
	
    if(isset($_POST['cod'])) {
    	$_SESSION['codFilter'] = $_POST['cod'];
    }
    
    if(!isset($_SESSION['cod']))
    	$_SESSION['cod'] = 0;
	
	$dataMapper = new dmCliente();
    $count = $dataMapper->getClientesRegistradosPorFechaCount($_SESSION['codFilter']);
    $listadoPaginator = new ListadoPaginator($count);
    $users = $dataMapper->getClientesRegyActxFecha($listadoPaginator->getOffset(), $listadoPaginator->getItemsPerPage(), $_SESSION['codFilter']);
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title></title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
		<link href="<?=UrlResolver::getCssBaseUrl("bo/estadisticas/css/calendar.css")?>" rel="stylesheet" type="text/css" />
	</head>
	
	<body>
<div id="container" class="listadocontainer">
			<h1>Reporte de usuarios activos por d&iacute;a</h1>					
</div>
		<div id="container" class="listadocontainer">
		
			<?php
				$listadoPrinter = new ListadoPrinter($users,"usersxdia.php", $listadoPaginator);
				
				$listadoPrinter->setDisableFiltering(true);	
				
				$listadoPrinter->showColumn("FechaIngreso", "Fecha");
				$listadoPrinter->showColumn("Users", "Cantidad de Registrados");		
				$listadoPrinter->showColumn("UsersAct", "Cantidad de Activos");		
				$listadoPrinter->showColumn("Percent", "Porcentaje de Activos");
				
				$listadoPrinter->setShowFilter("FechaIngreso", false);
				$listadoPrinter->setShowFilter("Users", false);
				$listadoPrinter->setShowFilter("UsersAct", false);
				$listadoPrinter->setShowFilter("Percent", false);
				
				$listadoPrinter->setSortColumn("FechaIngreso", false);
				$listadoPrinter->setSortColumn("Users", false);
				$listadoPrinter->setSortColumn("UsersAct", false);
				$listadoPrinter->setSortColumn("Percent", false);
								
				$listadoPrinter->printListado();
			?>
			<br />
			
			<form action="usersxdia.php" method="post" style="float: left;margin-right: 20px">
				
				<fieldset style=" padding: 10px">
					<legend>C&oacute;digos de invitaci&oacute;n</legend>
					
					<label for="cod" style="display: block">Seleccione un c&oacute;digo de invitaci&oacute;n</label>
					<select name="cod">
						<option value="0">Todos</option>
						
						<?php
							$all = dmPartnersCod::getAll(3, 'ASC');
							foreach($all as $i => $cod)
								echo '<option value="'.$cod['Codigo'].'" '.($_SESSION['codFilter'] == $cod['Codigo'] ? 'selected="selected"' : '').'>'.$cod['Codigo'].'</option>
						';
						?>
						
					</select>
					
					<br /><br />
					
					<input type="submit" value=" Filtrar " />
				</fieldset>
				
			</form>
			
			<form action="usersxdia.php" method="post" style="float: left; width 150px;">
				
				<fieldset style=" padding: 10px">
					<legend>Filtro manual</legend>
					
					<label for="cod" style="display: block">Ingrese un c&oacute;digo de invitaci&oacute;n</label>
					<input type="text" name="cod" value="<?=$_SESSION['codFilter'];?>" />
					
					<br /><br />
					
					<input type="submit" value=" Filtrar " />
				</fieldset>
				
			</form>
			
			<div class="clear"></div>
			<br />
			
			<a href="../diarios.php">&lt;&lt; Volver</a>		
		</div>	
	</body>
</html>
