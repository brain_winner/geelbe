<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
    try
    {
        ValidarUsuarioLogueadoBo(19);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
    	
	require_once '../lib/connect.php';
	require_once '../lib/userStats.php';
	
	$totalUsuariosActivos = totalUsuariosActivos($mainDB);
	$totalUsuariosConDireccion = totalUsuariosConDireccionCargada($mainDB);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title></title>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
	</head>
	
	<body>

		<div id="container">
			<div id="main">
				<h1>Reporte de usuarios por provincia/estado</h1>
				<p>Total de usuarios activos: <?=$totalUsuariosActivos;?></p>
				<br />
				<table>
					<tr>
						<th>Provincia</th>
						<th>Total</th>
						<th>Porcentaje del total</th>
					</tr>
					<?
					echo '<tr class="alt">';
				    echo '<td>Desconocido</td>';
                    echo '<td>'.($totalUsuariosActivos - $totalUsuariosConDireccion).'</td>';
                    echo '<td>'.round((($totalUsuariosActivos - $totalUsuariosConDireccion)*100)/$totalUsuariosActivos, 2)."%".'</td>';
					echo '</tr>';
					
					$datosPorProvincia = totalUsuariosPorProvincia($mainDB);
					$idx = 0;
					while ($datos = mysql_fetch_array($datosPorProvincia, MYSQL_ASSOC)) {
						if($idx%2==0) {
							echo '<tr>';
						} else {
							echo '<tr class="alt">';
						}
                        echo '<td>'.$datos['NombreProvincia'].'</td>';
                        echo '<td>'.$datos['TotalUsuarios'].'</td>';
                        echo '<td>'.round(($datos['TotalUsuarios']*100)/$totalUsuariosActivos, 2)."%".'</td>';
						echo '</tr>';
						$idx = $idx + 1;
                    }
                    ?>

				</table>
				<br />
				<a href="../diarios.php">&lt;&lt; Volver</a>
			</div>
		</div>
	</body>
</html>
