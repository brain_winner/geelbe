<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
    try
    {
        ValidarUsuarioLogueadoBo(19);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
    	
	require_once '../lib/connect.php';
	require_once '../lib/userStats.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title></title>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
	</head>
	
	<body>

		<div id="container">
			<div id="main">
				<h1>Reporte de usuarios por edad</h1>
				<table>
					<tr>
						<th>Promedio de edad de usuarios activos</th>
						<td><?=round(promedioDeEdadUsuariosActivos($mainDB), 2);?></td>
					</tr>
					
					<tr class="alt">
						<th>Desviaci&oacute;n est&aacute;ndar de edad de usuarios activos</th>
						<td><?=round(desviacionEstandarDeEdadUsuariosActivos($mainDB), 2);?></td>
					</tr>
				</table>
				<br />
				<a href="../diarios.php">&lt;&lt; Volver</a>
			</div>
		</div>
	</body>
</html>
