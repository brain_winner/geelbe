<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticasgenerales"));
	
	if(!isset($_REQUEST['month'], $_REQUEST['year'], $_REQUEST['day'])) {
		die('Faltan par&aacute;metros.');
	}
		
	$time = mktime(0, 0, 0, $_REQUEST['month'], $_REQUEST['day'], $_REQUEST['year']);
	

	if(!isset($_REQUEST['clearFilterButton'])) {
		$paramArray = ListadoUtils::generateFiltersParamArray(array('Apellido', 'Nombre', 'NombreUsuario', 'Estado', 'Padrino'));
	} else {
		$paramArray = array();
	}
	$sortArray = ListadoUtils::generateSortParamArray("FechaIngreso");
	
	$dia = $_REQUEST['day'];
	$mes = $_REQUEST['month'];
	$anio = $_REQUEST['year'];
		
	$parametro = "?".$_GET["userType"]."&month=".$_REQUEST['month']."&year=".$_REQUEST['year']."&day=".$_REQUEST['day'];
	$showing = "";
	$dataMapper = new dmEstadisticasGenerales();
	
	$count = $dataMapper->getUsuariosActivosPorFechaCount($_REQUEST['day'], $_REQUEST['month'], $_REQUEST['year'], $paramArray);
	$countAct = $dataMapper->getUsuarioActivoPorFechaCount($_REQUEST['day'], $_REQUEST['month'], $_REQUEST['year'], $paramArray);
	$countInact = $dataMapper->getUsuarioInactivoPorFechaCount($_REQUEST['day'], $_REQUEST['month'], $_REQUEST['year'], $paramArray);
	$countHuer = $dataMapper->getUsuarioHuerfanoPorFechaCount($_REQUEST['day'], $_REQUEST['month'], $_REQUEST['year'], $paramArray);
	$countDes = $dataMapper->getUsuarioDesuscriptoPorFechaCount($_REQUEST['day'], $_REQUEST['month'], $_REQUEST['year'], $paramArray);
	
	   if ($_GET["userType"]) {
	        if($_GET["userType"] == "activos") {
				$listadoPaginator = new ListadoPaginator($countAct['numberOfRows']);
				$users = $dataMapper->getUsuarioActivoPorFecha($_REQUEST['day'], $_REQUEST['month'], $_REQUEST['year'], $listadoPaginator->getOffset(), $listadoPaginator->getItemsPerPage(), $paramArray, $sortArray);
				$showing = "Activos";
			}
			 if($_GET["userType"] == "inactivos") {
				$listadoPaginator = new ListadoPaginator($countInact['numberOfRows']);
				$users = $dataMapper->getUsuarioInactivoPorFecha($_REQUEST['day'], $_REQUEST['month'], $_REQUEST['year'], $listadoPaginator->getOffset(), $listadoPaginator->getItemsPerPage(), $paramArray, $sortArray);
				$showing = "Inactivos";
			}
			 if($_GET["userType"] == "huerfanos") {
				$listadoPaginator = new ListadoPaginator($countHuer['numberOfRows']);
				$users = $dataMapper->getUsuarioHuerfanoPorFecha($_REQUEST['day'], $_REQUEST['month'], $_REQUEST['year'], $listadoPaginator->getOffset(), $listadoPaginator->getItemsPerPage(), $paramArray, $sortArray);
				$showing = "Huerfanos";
			}
			 if($_GET["userType"] == "desuscriptos") {
				$listadoPaginator = new ListadoPaginator($countDes['numberOfRows']);
				$users = $dataMapper->getUsuarioDesuscriptoPorFecha($_REQUEST['day'], $_REQUEST['month'], $_REQUEST['year'], $listadoPaginator->getOffset(), $listadoPaginator->getItemsPerPage(), $paramArray, $sortArray);
				$showing = "Desuscriptos";
			}
			$parametro = "?userType=".$_GET["userType"]."&month=".$_REQUEST['month']."&year=".$_REQUEST['year']."&day=".$_REQUEST['day'];
	    }
	    else {			
			$listadoPaginator = new ListadoPaginator($count['numberOfRows']);
			$users = $dataMapper->getUsuariosActivosPorFecha($_REQUEST['day'], $_REQUEST['month'], $_REQUEST['year'], $listadoPaginator->getOffset(), $listadoPaginator->getItemsPerPage(), $paramArray, $sortArray);
			$showing = "Todos";
		}   
	
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title></title>
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
		<link href="<?=UrlResolver::getCssBaseUrl("bo/estadisticas/css/calendar.css")?>" rel="stylesheet" type="text/css" />
	</head>
	
	<body>

		<div id="container" class="listadocontainer">
			
			<h1>Usuarios registrados el <?=strftime("%A %d de %B de %Y", $time);?> (<?=$showing;?>)</h1>
			<div id="mainStats">
					<table style="margin-bottom:10px;">
						<tr class="alt">
							<td style="text-align:left;font-weight:bold;"><b>Mostrar Clientes:</b> <a href="users.php?day=<?echo $dia;?>&month=<?echo $mes;?>&year=<?echo $anio;?>">Todos (<?=$count['numberOfRows']?>)</a> | 
			                  <a href="users.php?day=<?echo $dia;?>&month=<?echo $mes;?>&year=<?echo $anio;?>&userType=activos">Activos (<?=$countAct['numberOfRows']?>)</a> | 
							  <a href="users.php?day=<?echo $dia;?>&month=<?echo $mes;?>&year=<?echo $anio;?>&userType=inactivos">Inactivos (<?=$countInact['numberOfRows']?>)</a> | 
							  <a href="users.php?day=<?echo $dia;?>&month=<?echo $mes;?>&year=<?echo $anio;?>&userType=huerfanos">Huerfanos (<?=$countHuer['numberOfRows']?>)</a> | 
							  <a href="users.php?day=<?echo $dia;?>&month=<?echo $mes;?>&year=<?echo $anio;?>&userType=desuscriptos">Desuscriptos (<?=$countDes['numberOfRows']?>)</a></td>
						</tr>
					</table>
				</div>
			
			<?php
				$listadoPrinter = new ListadoPrinter($users, "users.php$parametro", $listadoPaginator);
				
				$listadoPrinter->showColumn("Apellido");
				$listadoPrinter->showColumn("Nombre");
				$listadoPrinter->showColumn("NombreUsuario", "Usuario");
				$listadoPrinter->showColumn("Estado");
				$listadoPrinter->showColumn("Padrino");
				$listadoPrinter->showColumn("Hora");
				
				$listadoPrinter->setShowFilter("Hora", false);
				$listadoPrinter->setShowFilter("Estado", false);
				
				$listadoPrinter->linkColumn("NombreUsuario", "/".$confRoot[1]."/bo/clientes/Ver.php?IdUsuario=[[IdUsuario]]");
								
				$listadoPrinter->addHiddenValue("userType", $_GET['userType']);
				$listadoPrinter->addHiddenValue("day", $_REQUEST['day']);
				$listadoPrinter->addHiddenValue("month", $_REQUEST['month']);
				$listadoPrinter->addHiddenValue("year", $_REQUEST['year']);
				
				$listadoPrinter->printListado();
			?>
			<br />
			<a href="<?=backToDailyStatsUrl();?>">&lt;&lt; Volver</a>			
		</div>	
	</body>
</html>
