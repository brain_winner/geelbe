<?php
  $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
  Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
    try
    {
        ValidarUsuarioLogueadoBo(16);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
  $oEstadistica = new dmEstadisticas();
  if (!($_GET["IdUsuario"]))
    throw new Exception();
  $DatosEstadisticos = $oEstadistica->Cliente()->UsuariosAhijados($_GET["IdUsuario"], (isset($_GET["columna"])?$_GET["columna"]:0)+1, (isset($_GET["sentido"])?$_GET["sentido"]:"DESC"));
  /*$DatosEstadisticos = $oEstadistica->Invitaciones()->UsuarioQueMasInvitarons(5, (isset($_GET["columna"])?$_GET["columna"]:2)+1, (isset($_GET["sentido"])?$_GET["sentido"]:"DESC"));*/
    $objDT = new Tabla($DatosEstadisticos, 10);
    $objDT->setPagina("invitadosdeusuario.php");
    $objDT->setParametro("IdUsuario=".$_GET["IdUsuario"]);
    $objDT->setPaginaNro((isset($_GET["irPagina"])?$_GET["irPagina"]:0));
    $objDT->setColumnaNro((isset($_GET["columna"])?$_GET["columna"]:0));
    $objDT->setColumnaSentido((isset($_GET["sentido"])?$_GET["sentido"]:"DESC"));
  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> Estadisticas</title>
    <?
        Includes::Scripts();
    ?>
    
    <link href="<?=UrlResolver::getCssBaseUrl("bo/style/css.css")?>" rel="stylesheet" type="text/css" />
</head>
<body>
<table style="width:100%; height:100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="borde_top">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="borde_bottom ">&nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="100%">
            <?
            $objDT->show();
            ?>
        </td>
    </tr>
</table>
</body>
</html>

