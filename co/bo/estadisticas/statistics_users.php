<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
	require_once 'lib/connect.php';

	if(!isset($_GET['month'], $_GET['year'], $_GET['day']))
		die('Faltan parámetros.');
		
	$time = mktime(0, 0, 0, $_GET['month'], $_GET['day'], $_GET['year']);
	
	$users = mysql_query("SELECT `usuarios`.`NombreUsuario`, `usuarios`.`Padrino`, `usuarios`.`FechaIngreso`, datos.`Nombre`, datos.`Apellido`, datos.`Email`
	FROM `usuarios`
	LEFT JOIN `datosusuariospersonales` datos ON `usuarios`.IdUsuario = datos.IdUsuario
	WHERE `usuarios`.`FechaIngreso` LIKE '".$_GET['year']."-".$_GET['month']."-".$_GET['day']."%' ORDER BY datos.Apellido, datos.Nombre ASC");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title></title>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
		<link href="<?=UrlResolver::getCssBaseUrl("bo/estadisticas/css/calendar.css")?>" rel="stylesheet" type="text/css" />
	</head>
	
	<body>

		<div id="container" class="cusers">
			
			<h1>Usuarios registrados el <?=strftime("%A %d de %B de %Y", $time);?></h1>
			
			<?php
				if(mysql_num_rows($users) == 0) {
					
					echo '<p>No hay usuarios registrados.</p>';
					
				} else {
				
					echo '<table class="users">
						<tr>
							<th>Apellido</th>
							<th>Nombre</th>
							<th>Usuario</th>
							<th>Padrino</th>
							<th>Hora</th>
						</tr>';
						
					for($i = 0; $user = mysql_fetch_assoc($users); $i++) {
					
						$time = strtotime($user['FechaIngreso']);
					
						echo '<tr'.($i%2 != 0 ? ' class="alt"': '').'>
							<td>'.$user['Apellido'].'</td>
							<td>'.$user['Nombre'].'</td>
							<td>'.$user['NombreUsuario'].'</td>
							<td>'.$user['Padrino'].'</td>
							<td>'.date("H:i:s", $time).'</td>
						</tr>';
						
					}
						
					echo '</table>';
				
				}
			?>
						
		</div>	
	</body>
</html>
