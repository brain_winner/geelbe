<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    try
    {
        ValidarUsuarioLogueadoBo(42);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
    
    if(isset($_POST['IdCampania'])) {
    	$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		
		$oConexion->setQuery("SET NAMES utf8");
		$oConexion->EjecutarQuery();
		
		$oConexion->setQuery("SELECT p.*, d.*, u.NombreUsuario, e.Descripcion as estado
		FROM pedidos p
		LEFT JOIN datosusuariospersonales d ON p.IdUsuario = d.IdUsuario
		LEFT JOIN usuarios u ON u.IdUsuario = d.IdUsuario
		LEFT JOIN estadospedidos e ON e.IdEstadoPedido = p.IdEstadoPedidos
		WHERE p.IdCampania = ".$_POST['IdCampania']);
		
		$DT = $oConexion->DevolverQuery();
		$oConexion->Cerrar();
		
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=pedidos.csv");
		header("Content-Type: text/csv");
		header("Content-Transfer-Encoding: binary");
		
		foreach($DT as $i => $d)
			echo $d['Nombre'].', '.$d['Apellido'].', '.$d['NombreUsuario'].', '.$d['estado'].'
';
		die;
    }
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title></title>
	<style type="text/css">
		body {
			font-family: Helvetica, Arial, sans-serif;
			font-size: 80%;
		}
	
		table {
			border-collapse: collapse;
			margin-bottom: 20px
		}
	
		table th {
			text-align: left;
		}
		
		table th, table td {
			border: 1px solid #999;
			padding: 2px 10px 2px 5px;
		}
		
		table td {
			text-align: right;
		}
	</style>
</head>
<body>

	<form action="reporte.php" method="post">
		<label>
			Campa&ntilde;a:
			<select name="IdCampania">
				<?php
					$oConexion = Conexion::nuevo();
					$oConexion->Abrir();
					
					$oConexion->setQuery("SET NAMES utf8");
					$oConexion->EjecutarQuery();
					
					$oConexion->setQuery("SELECT IdCampania, Nombre, FechaInicio FROM campanias ORDER BY FechaInicio DESC");
					
					$DT = $oConexion->DevolverQuery();
					$oConexion->Cerrar();
				
					foreach($DT as $i => $d)
						echo '<option value="'.$d['IdCampania'].'">'.$d['Nombre'].' - '.$d['FechaInicio'].'</option>';
				?>
			</select>
		</label>

		<br />
		<br />
		
		<input type="submit" value="Enviar" />
	</form>
		
</body>
</html>
