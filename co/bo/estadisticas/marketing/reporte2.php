<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    try
    {
        ValidarUsuarioLogueadoBo(41);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
    
    if(isset($_POST['from'], $_POST['to'])) {
    	$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		
		$oConexion->setQuery("SET NAMES utf8");
		$oConexion->EjecutarQuery();
		
		$oConexion->setQuery("SELECT p.*, c.Nombre as campania, e.Descripcion as estado
		FROM pedidos p
		LEFT JOIN campanias c ON c.IdCampania = p.IdCampania
		LEFT JOIN estadospedidos e ON e.IdEstadoPedido = p.IdEstadoPedidos
		WHERE p.Fecha BETWEEN '".$_POST['from']."-01' AND '".$_POST['to']."-31'");

		
		$DT = $oConexion->EjecutarQuery();
		$oConexion->Cerrar();
		
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=reporte_ventas.csv");
		header("Content-Type: text/csv");
		header("Content-Transfer-Encoding: binary");
		
		while($d = mysql_fetch_assoc($DT))
			echo date("d/m/Y", strtotime($d['Fecha'])).';'.$d['campania'].';'.$d['Total'].';'.date("H:i:s", strtotime($d['Fecha'])).';'.$d['estado'].'
';
			
		die;
    }
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title></title>
	<style type="text/css">
		body {
			font-family: Helvetica, Arial, sans-serif;
			font-size: 80%;
		}
	
		table {
			border-collapse: collapse;
			margin-bottom: 20px
		}
	
		table th {
			text-align: left;
		}
		
		table th, table td {
			border: 1px solid #999;
			padding: 2px 10px 2px 5px;
		}
		
		table td {
			text-align: right;
		}
	</style>
</head>
<body>

	<form action="reporte2.php" method="post">
		<label>
			Entre
			<select name="from">
				<?php
					for($i = 2008; $i <= date("Y"); $i++)
						for($j = 1; $j <= 12; $j++) {
							if($j < 10) $j = '0'.$j;
						
							echo '<option value="'.$i.'-'.$j.'">'.strftime("%B de %Y", mktime(0, 0, 0, $j, 1, $i)).'</option>';
						}
				?>
			</select>
		</label>
		
		<label>
			y
			<select name="to">
				<?php
					for($i = date("Y"); $i >= 2008 ; $i--)
						for($j = date("m"); $j >= 1; $j--) {
							if($j < 10) $j = '0'.$j;
						
							echo '<option value="'.$i.'-'.$j.'">'.strftime("%B de %Y", mktime(0, 0, 0, $j, 1, $i)).'</option>';
						}
				?>
			</select>
		</label>

		<br />
		<br />
		
		<input type="submit" value="Enviar" />
	</form>
		
</body>
</html>
