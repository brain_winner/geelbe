<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    try
    {
        ValidarUsuarioLogueadoBo(70);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
    
    if(isset($_REQUEST['from1'], $_REQUEST['from2'], $_REQUEST['to1'], $_REQUEST['to2'])) {
    	
    	$_REQUEST['from'] = $_REQUEST['from2'].'-'.$_REQUEST['from1'];
    	$_REQUEST['to'] = $_REQUEST['to2'].'-'.$_REQUEST['to1'];
    	
    	$oConexion = Conexion::nuevo();
		$oConexion->Abrir();
		
		$oConexion->setQuery("SET NAMES utf8");
		$oConexion->EjecutarQuery();
		
		$oConexion->setQuery("SELECT COUNT(IdUsuario) total, CONCAT(YEAR(FechaIngreso),MONTH(FechaIngreso)) as Fecha, Padrino
		FROM usuarios
		WHERE FechaIngreso BETWEEN '".$_REQUEST['from']."-01' AND '".$_REQUEST['to']."-31' AND Padrino IN (
			SELECT c.Codigo 
			FROM partners_codigos c
			JOIN partners_codigos_base b ON c.IdCodigoBase = b.IdCodigoBase
			WHERE b.Fecha BETWEEN '".$_REQUEST['from']."-01' AND '".$_REQUEST['to']."-31'
		)
		GROUP BY Padrino, Fecha
		ORDER BY Padrino ASC");
		$usuarios = $oConexion->DevolverQuery();
		
		$oConexion->setQuery("SELECT COUNT(IdUsuario) total, CONCAT(YEAR(FechaIngreso),MONTH(FechaIngreso)) as Fecha, Padrino
		FROM usuarios
		WHERE FechaIngreso BETWEEN '".$_REQUEST['from']."-01' AND '".$_REQUEST['to']."-31' AND es_activa = 0 AND Padrino IN (
			SELECT c.Codigo 
			FROM partners_codigos c
			JOIN partners_codigos_base b ON c.IdCodigoBase = b.IdCodigoBase
			WHERE b.Fecha BETWEEN '".$_REQUEST['from']."-01' AND '".$_REQUEST['to']."-31'
		)
		GROUP BY Padrino, Fecha
		ORDER BY Padrino ASC");
		$activos = $oConexion->DevolverQuery();
		
		$oConexion->setQuery("SELECT COUNT(p.IdPedido) total, SUM(p.Total) monto, CONCAT(YEAR(p.Fecha),MONTH(p.Fecha)) as Fecha, u.Padrino
		FROM pedidos p
		JOIN usuarios u ON p.IdUsuario = u.IdUsuario
		WHERE p.Fecha BETWEEN '".$_REQUEST['from']."-01' AND '".$_REQUEST['to']."-31' AND p.IdEstadoPedidos IN (2,3,6)
		AND u.Padrino IN (
			SELECT c.Codigo 
			FROM partners_codigos c
			JOIN partners_codigos_base b ON c.IdCodigoBase = b.IdCodigoBase
			WHERE b.Fecha BETWEEN '".$_REQUEST['from']."-01' AND '".$_REQUEST['to']."-31'
		)
		GROUP BY u.Padrino, Fecha
		ORDER BY Padrino ASC");
		$compras = $oConexion->DevolverQuery();
		
		$DT = array();
		foreach($usuarios as $data)
			$DT[$data['Padrino']][$data['Fecha']]['usuarios'] = $data['total'];		

		foreach($activos as $data)
			$DT[$data['Padrino']][$data['Fecha']]['activos'] = $data['total'];		

		foreach($compras as $data) {
			$DT[$data['Padrino']][$data['Fecha']]['compras'] = $data['total'];		
			$DT[$data['Padrino']][$data['Fecha']]['monto'] = $data['monto'];		
		}
		
		$oConexion->Cerrar();
		
		$from = strtotime($_REQUEST['from'].'-01');
		$to = strtotime($_REQUEST['to'].'-01');
		
		$total = count($DT);
		$ppp = 10;
		$pages = ceil($total / $ppp);
		
		$page = (isset($_GET['p']) ? $_GET['p'] : 1);
		$start = ($page-1)*$ppp;
		$end = $page*$ppp - 1;
		
    }
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title></title>
	<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	<style type="text/css">
		input { font-size: 1.4em; text-align: center; vertical-align: middle}
		h2 { font-weight: normal; margin: 1em 0; }
		ul { display: block; margin: 1em 0}
		ul li {display: inline; margin-right: 0.5em; list-style-type: none}
	</style>
	<script src="http://code.jquery.com/jquery.js"></script>
</head>
<body>

	<div id="container" class="listadocontainer">
		<h1>Usuarios</h1>	

		<form action="padrino.php" method="get" id="mainStats">
			<table style="margin-bottom:10px;">
				<tr class="alt">
					<td style="text-align:left;font-weight:bold;">
						Entre
						<input type="text" placeholder="mes" name="from1" value="<?=$_REQUEST['from1']?>" maxlength="2" size="3" /> - <input type="text" placeholder="a&ntilde;o" name="from2" value="<?=$_REQUEST['from2']?>"  maxlength="4" size="4" />
					
						y
						<input type="text" placeholder="mes" name="to1" value="<?=$_REQUEST['to1']?>" maxlength="2" size="3" /> - <input type="text" placeholder="a&ntilde;o" name="to2" value="<?=$_REQUEST['to2']?>" maxlength="4" size="4" />

						<input type="submit" value="Enviar" />						
					</td>
				</tr>
			</table>
		</form>
			
		<?php if(isset($DT)): ?>
		<?php 
			$n = -1;
			foreach($DT as $Padrino => $data):
				$n++;
				if($n < $start) continue;
				if($n > $end) break;
		?>
		<h2><?=$Padrino?></h2>
		<table>
			<tr>
				<th></th>
				<?php for($i = $from; $i <= $to; $i += 31*24*60*60): ?>
				<th><?=strftime("%B %Y", $i)?></th>
				<?php endfor; ?>
				<th>Total</th>
			</tr>
			<tr class="alt">
				<th>Usuarios registrados</th>
				<?php for($i = $from, $total = 0; $i <= $to; $i += 31*24*60*60): $date = date("Yn", $i); $total += $data[$date]['usuarios']; ?>
				<td><?=intval($data[$date]['usuarios'])?></td>
				<?php endfor; ?>
				<td><strong><?=intval($total)?></strong></td>
			</tr>
			<tr>
				<th>Usuarios activos</th>
				<?php for($i = $from, $total = 0; $i <= $to; $i += 31*24*60*60): $date = date("Yn", $i); $total += $data[$date]['activos']; ?>
				<td><?=intval($data[$date]['activos'])?></td>
				<?php endfor; ?>
				<td><strong><?=intval($total)?></strong></td>
			</tr>
			<tr class="alt">
				<th>Compras generadas</th>
				<?php for($i = $from, $total = 0; $i <= $to; $i += 31*24*60*60): $date = date("Yn", $i); $total += $data[$date]['compras']; ?>
				<td><?=intval($data[$date]['compras'])?></td>
				<?php endfor; ?>
				<td><strong><?=intval($total)?></strong></td>
			</tr>
			<tr>
				<th>Total de $ en compras</th>
				<?php for($i = $from, $total = 0; $i <= $to; $i += 31*24*60*60): $date = date("Yn", $i); $total += $data[$date]['monto']; ?>
				<td>$<?=floatval($data[$date]['monto'])?></td>
				<?php endfor; ?>
				<td><strong>$<?=floatval($total)?></strong></td>
			</tr>
		</table>
		<?php endforeach; ?>
		
		<?php if($pages > 1): ?>
		<ul>
			<?php for($i = 1; $i <= $pages; $i++):
					if($i == $page):
			?>
			<li><?=$i?></li>
			<?php else: ?>
			<li><a href="?p=<?=$i?>&from1=<?=$_GET['from1']?>&from2=<?=$_GET['from2']?>&to1=<?=$_GET['to1']?>&to2=<?=$_GET['to2']?>"><?=$i?></a></li>
			<?php endif;
				endfor; 
			?>
		</ul>
		<?php endif; ?>
		
		<?php endif; ?>
		
		<script>
			$('input').keyup(function() {

				if(this.value.length == this.maxLength)
					$(this).next().focus()
				
			})
		</script>
		
</body>
</html>
