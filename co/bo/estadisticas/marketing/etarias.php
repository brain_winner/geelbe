<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("estadisticas"));
    try
    {
        ValidarUsuarioLogueadoBo(37);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    } 
    	
	require_once '../lib/connect.php';

	function datos($genero, $desde, $hasta = null) {
	
		global $mainDB;
		
		if($hasta == null)
			$between = '>= '.$desde;
		else
			$between = 'BETWEEN '.$desde.' AND '.$hasta;
			
		if($genero == 'Hombre')
			$genero = 1;
		else
			$genero = 2;
			
		if(isset($_GET['cod']) || isset($_GET['from'], $_GET['to'])) {
			
			$extraWhere = 'u.Padrino LIKE "'.$_GET['cod'].'" AND u.FechaIngreso BETWEEN "'.$_GET['from'].'-01" AND "'.$_GET['to'].'-31" AND ';
			
		}
			
		$q = mysql_query("
		SELECT
			COUNT(DISTINCT du.IdUsuario) as usuarios,
			COUNT(DISTINCT p.IdUsuario) as compradores,
			SUM(p.Total) as monto
				
		FROM usuarios u 
		LEFT JOIN pedidos p ON p.IdUsuario = u.IdUsuario
		LEFT JOIN datosusuariospersonales du ON u.IdUsuario = du.IdUsuario
		
		WHERE 
			du.IdTratamiento = ".$genero." AND ".$extraWhere."
			(YEAR(CURDATE())-YEAR(du.FechaNacimiento)) - (RIGHT(CURDATE(),5) < RIGHT(du.FechaNacimiento, 5)) ".$between, $mainDB);
			
		$d = mysql_fetch_assoc($q);
		return $d;
		
	}
	
	function generateTable($genero) {
		
		$edades = array(
			array(0, 17),
			array(18, 20),
			array(21, 23),
			array(24, 28),
			array(29, 34),
			array(35, 39),
			array(40, 50),
			array(51)
		);
		
		$montos = $franjas = array();
		$total = 0;
		
		foreach($edades as $i => $rango) {
		
			echo '<tr>';
		
			if(count($rango) == 1) {
				$franja = $rango[0].' en adelante';
				$datos = datos($genero, $rango[0]);
			} else {
				$franja = $rango[0].' a '.$rango[1];
				$datos = datos($genero, $rango[0], $rango[1]);
			}
			
			echo '<th>'.$franja.'</th>';			
			echo '<td>'.$datos['usuarios'].' usuarios</td>';
			echo '<td>'.$datos['compradores'].' usuarios</td>';
			
			if($datos['usuarios'] > 0)
				echo '<td>'.number_format(($datos['compradores'] * 100 / $datos['usuarios']), 2, ',', '.').'%</td>';
			else
				echo '<td>0</td>';
				
			echo '<td>$'.number_format($datos['monto'], 2, ',', '.').'</td>';
			
			echo '</tr>';
			
			$montos[] = round($datos['monto']);
			$franjas[] = $franja;
			$total += $datos['monto'];
				
		
		}
		
		foreach($montos as $i => $m)
			$montos[$i] = round($m*100/$total);
		
		return '<img src="http://chart.apis.google.com/chart?cht=p3&chd=t:'.implode(',', $montos).'&chs=450x300&chdl='.implode('|', $franjas).'&chco=FF0000&chf=bg,s,FFFFFF00&chtt=Monto por franja etaria" alt="Grafico" style="width:450px; margin: 20px auto; display:block">';
		
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title></title>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
		<link href="<?=UrlResolver::getCssBaseUrl("bo/estadisticas/css/calendar.css")?>" rel="stylesheet" type="text/css" />
		<style type="text/css">
			th {
				background-color: #ffefef;
			}
		</style>
	</head>
	
	<body>

		<div id="container">
			<div id="main">
			
				<h1>Mujeres</h1>
			
				<table>
					<tr>
						<th >Edad</th>
						<th>Usuarios</th>
						<th>Compradoes</th>
						<th>% de compradores</th>
						<th>$ Monto</th>
					</tr>
					
					<?php
						$graph = generateTable('Mujer');
					?>
				</table>			

				<?php echo $graph; ?>

				<h1>Hombres</h1>
			
				<table>
					<tr>
						<th>Edad</th>
						<th>Usuarios</th>
						<th>Compradoes</th>
						<th>% de compradores</th>
						<th>$ Monto</th>
					</tr>
					
					<?php
						$graph = generateTable('Hombre');
					?>
				</table>	

				<?php echo $graph; ?>		

			</div>

						
		</div>
        
	</body>
</html>
