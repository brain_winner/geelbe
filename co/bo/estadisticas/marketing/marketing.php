<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    try
    {
        ValidarUsuarioLogueadoBo(37);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
   
   /* Funciones */
   function toDate($d) {
   	$meses = array(
   		'01' => 'Enero',
   		'02' => 'Febrero',
   		'03' => 'Marzo',
   		'04' => 'Abril',
   		'05' => 'Mayo',
   		'06' => 'Junio',
   		'07' => 'Julio',
   		'08' => 'Agosto',
   		'09' => 'Septiembre',
   		'10' => 'Octubre',
   		'11' => 'Noviembre',
   		'12' => 'Diciembre'   		
   	);
   
   	$d = explode('-', $d);
   	
   	return $meses[$d[1]].' '.$d[0];
   }
   
   if(isset($_GET['mes']))
   	$_GET['from'] = $_GET['to'] = date("Y-m", strtotime($_GET['mes']));
   
   function registrados($padrino) {
   	if(is_array($padrino)) {
   		if(count($padrino) == 0)
   			return array();
   		
   		$p = '';
   		foreach($padrino as $i => $k)
   			if($k != '')
   				$p .= "'".$k."', ";
   				
   		$padrino = 'IN ('.substr($p, 0, -2).')';
   	} else {
   		if($padrino == '')
   			return array();
   		
   		$padrino = 'LIKE "'.$padrino.'"';
   	}
   	
   	if(isset($_GET['mes']))
   		$group = 'Dia';
   	else
   		$group = 'Fecha';
   
	   $query = 'SELECT COUNT(IdUsuario) as N, GROUP_CONCAT(NombreUsuario) as SUB, CONCAT(YEAR(FechaIngreso),"-",DATE_FORMAT(FechaIngreso, "%m"),"-01") as Fecha, SUM(es_activa) as Inactivos, DAY(FechaIngreso) as Dia
	   FROM usuarios
	   WHERE Padrino '.$padrino.' AND FechaIngreso BETWEEN "'.$_GET['from'].'-01" AND "'.$_GET['to'].'-31"
	   GROUP BY '.$group.'
	   ORDER BY FechaIngreso ASC';

	   $oConexion = Conexion::nuevo();
	   $oConexion->Abrir();
		$oConexion->setQuery($query);
		$res = $oConexion->DevolverQuery();
		$oConexion->Cerrar();
		
		return $res;
	}

   function pedidos($padrino) { 	
   	if(is_array($padrino)) {
   		if(count($padrino) == 0)
   			return array();
   		
   		$p = '';
   		foreach($padrino as $i => $k)
   			if($k != '')
   				$p .= "'".$k."', ";
   				
   		$padrino = 'IN ('.substr($p, 0, -2).')';
   	} else {
   		if($padrino == '')
   			return array();
   		
   		$padrino = 'LIKE "'.$padrino.'"';
   	}
   	
   	if(isset($_GET['mes']))
   		$group = 'Dia';
   	else
   		$group = 'Fecha';
   	   
	   $query = 'SELECT COUNT(p.IdPedido) as N, GROUP_CONCAT(u.NombreUsuario) as SUB, CONCAT(YEAR(p.Fecha),"-",DATE_FORMAT(p.Fecha, "%m"),"-01") as Fecha, SUM(u.es_activa) as Inactivos, DAY(p.Fecha) as Dia
	   FROM pedidos p
	   LEFT JOIN usuarios u ON u.IdUsuario = p.IdUsuario
    	WHERE u.Padrino '.$padrino.' AND p.Fecha BETWEEN "'.$_GET['from'].'-01" AND "'.$_GET['to'].'-31"
    	GROUP BY '.$group.'
    	ORDER BY p.Fecha ASC';

	   $oConexion = Conexion::nuevo();
	   $oConexion->Abrir();
		$oConexion->setQuery($query);
		$res = $oConexion->DevolverQuery();
		$oConexion->Cerrar();
		
		return $res;
	}

   function facturado($padrino) { 	
   	if(is_array($padrino)) {
   		if(count($padrino) == 0)
   			return array();
   		
   		$p = '';
   		foreach($padrino as $i => $k)
   			if($k != '')
   				$p .= "'".$k."', ";
   				
   		$padrino = 'IN ('.substr($p, 0, -2).')';
   	} else {
   		if($padrino == '')
   			return array();
   		
   		$padrino = 'LIKE "'.$padrino.'"';
   	}
   	
   	if(isset($_GET['mes']))
   		$group = 'Dia';
   	else
   		$group = 'Fecha';
   
	   $query = 'SELECT SUM(p.Total) as N, GROUP_CONCAT(u.NombreUsuario) as SUB, CONCAT(YEAR(p.Fecha),"-",DATE_FORMAT(p.Fecha, "%m"),"-01") as Fecha, SUM(u.es_activa) as Inactivos, DAY(p.Fecha) as Dia
	   FROM pedidos p
	   LEFT JOIN usuarios u ON u.IdUsuario = p.IdUsuario
    	WHERE u.Padrino '.$padrino.' AND p.Fecha BETWEEN "'.$_GET['from'].'-01" AND "'.$_GET['to'].'-31"
    	GROUP BY '.$group.'
    	ORDER BY p.Fecha ASC';

	   $oConexion = Conexion::nuevo();
	   $oConexion->Abrir();
		$oConexion->setQuery($query);
		$res = $oConexion->DevolverQuery();
		$oConexion->Cerrar();
		
		return $res;
	}
	
	/* Principal */	
	if(isset($_GET['cod']))
   	$cod = $_GET['cod'];
   else
   	$cod = false;  
   	
  	function getData($tipo, $cod) {

		$fechas = array();
		$d1 = $d2 = $d3 = array();

		$reg1 = call_user_func($tipo, $cod);
		$sub = array();
		for($i = 0; $i < count($reg1); $i++) {
				
			$sub1 = explode(',', $reg1[$i]['SUB']);
			$sub = array_merge($sub, $sub1);

			if(isset($_GET['mes']))
				$fecha = $reg1[$i]['Dia'];
			else
				$fecha = $reg1[$i]['Fecha'];

			if(!in_array($fecha, $fechas))
				$fechas[] = $fecha;
				
			$d1[$fecha] = array('N' => $reg1[$i]['N'], 'Inactivos' => $reg1[$i]['Inactivos'], 'Activos' => $reg1[$i]['N'] - $reg1[$i]['Inactivos'], 'Fecha' => $reg1[$i]['Fecha']);
		
		} 
		$sub = array_unique($sub);
		
		$reg2 = call_user_func($tipo, $sub);
		$sub = array();
		for($i = 0; $i < count($reg2); $i++) {
			
			$sub1 = explode(',', $reg2[$i]['SUB']);
			$sub = array_merge($sub, $sub1);
			
			if(isset($_GET['mes']))
				$fecha = $reg2[$i]['Dia'];
			else
				$fecha = $reg2[$i]['Fecha'];
			
			if(!in_array($fecha, $fechas))
				$fechas[] = $fecha;
			
			$d2[$fecha] = array('N' => $reg2[$i]['N'], 'Inactivos' => $reg2[$i]['Inactivos'], 'Activos' => $reg2[$i]['N'] - $reg2[$i]['Inactivos'], 'Fecha' => $reg2[$i]['Fecha']);
		
		}
		
		$reg3 = call_user_func($tipo, $sub);
		
		for($i = 0; $i < count($reg3); $i++) {
		
			if(isset($_GET['mes']))
				$fecha = $reg3[$i]['Dia'];
			else			
				$fecha = $reg3[$i]['Fecha'];

			if(!in_array($fecha, $fechas))
				$fechas[] = $fecha;
						
			$d3[$fecha] = array('N' => $reg3[$i]['N'], 'Inactivos' => $reg3[$i]['Inactivos'], 'Activos' => $reg3[$i]['N'] - $reg3[$i]['Inactivos'], 'Fecha' => $reg3[$i]['Fecha']);
		
		}
				
		sort($fechas);
		return array($fechas, $d1, $d2, $d3);
		
	}

   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title></title>
	<style type="text/css">
		body {
			font-family: Helvetica, Arial, sans-serif;
			font-size: 80%;
		}
	
		table {
			border-collapse: collapse;
			margin-bottom: 20px
		}
	
		table th {
			text-align: left;
		}
		
		table th, table td {
			border: 1px solid #999;
			padding: 2px 10px 2px 5px;
		}
		
		table td {
			text-align: right;
		}
	</style>
</head>
<body>

	<?php	if($cod === false): ?>
	
	<form action="marketing.php" method="get">
		<label>
			C&oacute;digo de invitaci&oacute;n:
			<input type="text" name="cod" id="cod" />
		</label>
		
		<br />
		<br />
		
		<label>
			Entre
			<select name="from">
				<?php
					for($i = 2008; $i <= date("Y"); $i++)
						for($j = 1; $j <= 12; $j++) {
							if($j < 10) $j = '0'.$j;
						
							echo '<option value="'.$i.'-'.$j.'">'.strftime("%B de %Y", mktime(0, 0, 0, $j, 1, $i)).'</option>';
						}
				?>
			</select>
		</label>
		
		<label>
			y
			<select name="to">
				<?php
					for($i = date("Y"); $i >= 2008 ; $i--)
						for($j = date("m"); $j >= 1; $j--) {
							if($j < 10) $j = '0'.$j;
						
							echo '<option value="'.$i.'-'.$j.'">'.strftime("%B de %Y", mktime(0, 0, 0, $j, 1, $i)).'</option>';
						}
				?>
			</select>
		</label>
	
		<br />
		<br />
		
		<input type="submit" value="Enviar" />
	</form>
		
	<?php else: ?>

	<table>
		<tr>
			<th>Cant. Registrados</th>
			<?php
				$data = getData('registrados', $cod);
				$n = array();
				foreach($data[0] as $i => $mes) {
					if(isset($_GET['mes']))
						echo '<th>'.$mes.' de '.toDate($_GET['mes']).'</th>';
					else
						echo '<th><a href="'.$_SERVER['REQUEST_URI'].'&mes='.$mes.'">'.toDate($mes).'</a></th>';
					$n[$mes] = 0;
				}
			?>
			<th>Total</th>
		</tr>		
		<tr>
			<th>Registrados</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[1][$mes])) {
						echo '<td>'.$data[1][$mes]['N'].'</td>';
						$n[$mes] += $data[1][$mes]['N'];
						$m += $data[1][$mes]['N'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Invitados Registrados</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[2][$mes])) {
						echo '<td>'.$data[2][$mes]['N'].'</td>';
						$n[$mes] += $data[2][$mes]['N'];
						$m += $data[2][$mes]['N'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Invitados Registrados Activos</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[2][$mes])) {
						echo '<td>'.$data[2][$mes]['Activos'].'</td>';
						$n[$mes] += $data[2][$mes]['Activos'];
						$m += $data[2][$mes]['Activos'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Invitados Registrados Inactivos</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[2][$mes])) {
						echo '<td>'.$data[2][$mes]['Inactivos'].'</td>';
						$n[$mes] += $data[2][$mes]['Inactivos'];
						$m += $data[2][$mes]['Inactivos'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Invitados Registrados 2do Nivel</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[3][$mes])) {
						echo '<td>'.$data[3][$mes]['N'].'</td>';
						$n[$mes] += $data[3][$mes]['N'];
						$m += $data[3][$mes]['N'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Invitados Registrados 2do Nivel Activos</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[3][$mes])) {
						echo '<td>'.$data[3][$mes]['Activos'].'</td>';
						$n[$mes] += $data[3][$mes]['Activos'];
						$m += $data[3][$mes]['Activos'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Invitados Registrados 2do Nivel Inactivos</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[3][$mes])) {
						echo '<td>'.$data[3][$mes]['Inactivos'].'</td>';
						$n[$mes] += $data[3][$mes]['Inactivos'];
						$m += $data[3][$mes]['Inactivos'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Total</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes) {
					echo '<td>'.$n[$mes].'</td>';
					$m += $n[$mes];
				}
			?>
			<td><?=$m;?></td>
		</tr>
	</table>
	
	<table>
		<tr>
			<th>Cant. Pedidos</th>
			<?php
				$data = getData('pedidos', $cod);
				$n = array();
				foreach($data[0] as $i => $mes) {
					if(isset($_GET['mes']))
						echo '<th>'.$mes.' de '.toDate($_GET['mes']).'</th>';
					else
						echo '<th><a href="'.$_SERVER['REQUEST_URI'].'&mes='.$mes.'">'.toDate($mes).'</a></th>';
					$n[$mes] = 0;
				}
			?>
			<th>Total</th>
		</tr>		
		<tr>
			<th>Registrados</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[1][$mes])) {
						echo '<td>'.$data[1][$mes]['N'].'</td>';
						$n[$mes] += $data[1][$mes]['N'];
						$m += $data[1][$mes]['N'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Invitados Registrados</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[2][$mes])) {
						echo '<td>'.$data[2][$mes]['N'].'</td>';
						$n[$mes] += $data[2][$mes]['N'];
						$m += $data[2][$mes]['N'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Invitados Registrados 2do Nivel</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[3][$mes])) {
						echo '<td>'.$data[3][$mes]['N'].'</td>';
						$n[$mes] += $data[3][$mes]['N'];
						$m += $data[3][$mes]['N'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Total</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes) {
					echo '<td>'.$n[$mes].'</td>';
					$m += $n[$mes];
				}
			?>
			<td><?=$m;?></td>
		</tr>
	</table>
	
	<table>
		<tr>
			<th>$ Facturada</th>
			<?php
				$data = getData('facturado', $cod);
				$n = array();
				foreach($data[0] as $i => $mes) {
					if(isset($_GET['mes']))
						echo '<th>'.$mes.' de '.toDate($_GET['mes']).'</th>';
					else
						echo '<th><a href="'.$_SERVER['REQUEST_URI'].'&mes='.$mes.'">'.toDate($mes).'</a></th>';
					$n[$mes] = 0;
				}
			?>
			<th>Total</th>
		</tr>		
		<tr>
			<th>Registrados</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[1][$mes])) {
						echo '<td>'.$data[1][$mes]['N'].'</td>';
						$n[$mes] += $data[1][$mes]['N'];
						$m += $data[1][$mes]['N'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Invitados Registrados</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[2][$mes])) {
						echo '<td>'.$data[2][$mes]['N'].'</td>';
						$n[$mes] += $data[2][$mes]['N'];
						$m += $data[2][$mes]['N'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Invitados Registrados 2do Nivel</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes)
					if(isset($data[3][$mes])) {
						echo '<td>'.$data[3][$mes]['N'].'</td>';
						$n[$mes] += $data[3][$mes]['N'];
						$m += $data[3][$mes]['N'];
					} else
						echo '<td>0</td>';
			?>
			<td><?=$m;?></td>
		</tr>
		<tr>
			<th>Total</th>
			<?php
				$m = 0;
				foreach($data[0] as $i => $mes) {
					echo '<td>'.$n[$mes].'</td>';
					$m += $n[$mes];
				}
			?>
			<td><?=$m;?></td>
		</tr>
	</table>
	
	<p><a href="etarias.php?<?=$_SERVER['QUERY_STRING'];?>">Generar informe por g&eacute;nero y edades</a></p>
		
	<?php endif; ?>

</body>
</html>
