<?

	define("TODOS", 0);
	define("USUARIOS_ACTIVOS", 1);
	define("USUARIOS_INACTIVOS", 2);

    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/bo/estadisticas/commons.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPrinter.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoUtils.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/listado/ListadoPaginator.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("clientes"));
    
    set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
	require_once 'Zend/Cache.php';
    
    try
    {
        ValidarUsuarioLogueadoBo(40);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    
    try {
		$countActivos = dmCliente::getClientesActivosCount(array());
		$countInactivos = dmCliente::getClientesInactivosCount(array());
		$countHuerfanos = dmCliente::getClientesHuerfanosCount(array());
		$countDesuscriptos = dmCliente::getClientesDesuscriptosCount(array());
		$countTotal = dmCliente::getClientesCount(array());
    }
    catch(Exception $e) {
		echo $e;
		exit;
    }
    
	$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
	$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 600, 'automatic_serialization' => true), array('cache_dir' => $cacheDir));

	function registrados($tipoUsuario = null, $intervalDays = 0) {
		global $cache;

		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_registrados_tipodeusuario_'.$tipoUsuario.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"].'_dias_'.$intervalDays;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_registrados_tipodeusuario_'.$tipoUsuario.'_dias_'.$intervalDays;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if ($tipoUsuario == USUARIOS_ACTIVOS) {
			$tipoUsuarioWhere = ' AND u.es_activa = 0 AND u.IdPerfil = 1';
		} else if ($tipoUsuario == USUARIOS_INACTIVOS) {
			$tipoUsuarioWhere = ' AND u.es_activa = 1 AND u.es_Provisoria = 1 AND u.Padrino <> "Huerfano" AND u.IdPerfil = 1';
		} else {
			$tipoUsuarioWhere = '';
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT COUNT(u.IdUsuario) as registrados FROM usuarios u WHERE u.FechaIngreso BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'" '.$tipoUsuarioWhere;

		    $oConexion = Conexion::nuevo();
			$oConexion->setQuery($query);
			$res = $oConexion->DevolverQuery();
			
			if($res[0]["registrados"] == null) {
				$res[0]["registrados"] = 0;
			}
			
			$cache->save($res[0]["registrados"], $nombreCache);
			return $res[0]["registrados"];
		} else {
			return $contenidoCache;
		}
	}

	function pedidosPagoAceptado($intervalDays = 0) {
		global $cache;
		
		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_pedidos_pago_aceptado_dias_'.$intervalDays.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"];
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_pedidos_pago_aceptado_dias_'.$intervalDays;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT COUNT(p.IdPedido) as pedidos FROM pedidos p WHERE p.IdEstadoPedidos IN (2, 3, 6, 7, 9) AND p.Fecha BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'"';
	
		    $oConexion = Conexion::nuevo();
			$oConexion->setQuery($query);
			$res = $oConexion->DevolverQuery();
			
			if($res[0]["pedidos"] == null) {
				$res[0]["pedidos"] = 0;
			}
			
			$cache->save($res[0]["pedidos"], $nombreCache);
			return $res[0]["pedidos"];
		} else {
			return $contenidoCache;
		}
	}
	
	function pedidosEsperandoSistemadePago($intervalDays = 0) {
		global $cache;
		
		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_pedidos_esperando_sistema_de_pago_'.$intervalDays.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"];
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_pedidos_esperando_sistema_de_pago_dias_'.$intervalDays;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT COUNT(p.IdPedido) as pedidos FROM pedidos p WHERE p.IdEstadoPedidos = (0) AND p.Fecha BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'"';
	
		    $oConexion = Conexion::nuevo();
			$oConexion->setQuery($query);
			$res = $oConexion->DevolverQuery();
			
			if($res[0]["pedidos"] == null) {
				$res[0]["pedidos"] = 0;
			}
			
			$cache->save($res[0]["pedidos"], $nombreCache);
			return $res[0]["pedidos"];
		} else {
			return $contenidoCache;
		}
	}

	function montoPedidosPagoAceptado($intervalDays = 0, $SinCostoDeEnvio = false) {
		global $cache;
		
		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_monto_pedidos_pago_aceptado_dias_'.$intervalDays.'_costo_de_envio_'.$SinCostoDeEnvio.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"];
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_monto_pedidos_pago_aceptado_dias_'.$intervalDays.'_costo_de_envio_'.$SinCostoDeEnvio;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if($SinCostoDeEnvio) {
			$SinCostoDeEnvioText = " - p.GastosEnvio";
		} else {
			$SinCostoDeEnvioText = "";
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT SUM(p.Total'.$SinCostoDeEnvioText.') as pedidos FROM pedidos p WHERE p.IdEstadoPedidos IN (2, 3, 6, 7, 9) AND p.Fecha BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'"';
	
		    $oConexion = Conexion::nuevo();
			$oConexion->setQuery($query);
			$res = $oConexion->DevolverQuery();
			
			if($res[0]["pedidos"] == null) {
				$res[0]["pedidos"] = 0;
			}
			
			$cache->save($res[0]["pedidos"], $nombreCache);
			return $res[0]["pedidos"];
		} else {
			return $contenidoCache;
		}
	}

	function costoDeEnvio($intervalDays = 0) {
		global $cache;
		
		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_costo_de_envio_dias_'.$intervalDays.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"];
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_costo_de_envio_dias_'.$intervalDays;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT SUM(p.GastosEnvio) as costo FROM pedidos p WHERE p.IdEstadoPedidos IN (2, 3, 6, 7, 9) AND p.Fecha BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'"';
	
		    $oConexion = Conexion::nuevo();
			$oConexion->setQuery($query);
			$res = $oConexion->DevolverQuery();
			
			if($res[0]["costo"] == null) {
				$res[0]["costo"] = 0;
			}
			
			$cache->save($res[0]["costo"], $nombreCache);
			return $res[0]["costo"];
		} else {
			return $contenidoCache;
		}
	}
	
	function invitaciones($intervalDays = 0, $unicas = false) {
		global $cache;
		
		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_invitaciones'.'_dias_'.$intervalDays.'_unicas_'.$unicas.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"];
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_invitaciones'.'_dias_'.$intervalDays.'_unicas_'.$unicas;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if($unicas) {
			$countText = " distinct(invitation_to) ";
		} else {
			$countText = " id ";
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT count('.$countText.') as invitaciones FROM geelbe_sent_invitations WHERE invitation_date BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'"';

		    $oConexion = Conexion::nuevo();
			$oConexion->setQuery($query);
			$res = $oConexion->DevolverQuery();
			
			if($res[0]["invitaciones"] == null) {
				$res[0]["invitaciones"] = 0;
			}
			
			$cache->save($res[0]["invitaciones"], $nombreCache);
			return $res[0]["invitaciones"];
		} else {
			return $contenidoCache;
		}
	}
		
	function cantidadMensajes($intervalDays = 0) {
		global $cache;
		
		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_cantidad_de_mensajes_'.$intervalDays.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"];
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_cantidad_de_mensajes_dias_'.$intervalDays;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT COUNT(m.idMensaje) as mensajes FROM mensajes m WHERE m.idMensajePadre IS NULL and m.Fecha BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'"';
	
		    $oConexion = Conexion::nuevo();
			$oConexion->setQuery($query);
			$res = $oConexion->DevolverQuery();
			
			if($res[0]["mensajes"] == null) {
				$res[0]["mensajes"] = 0;
			}
			
			$cache->save($res[0]["mensajes"], $nombreCache);
			return $res[0]["mensajes"];
		} else {
			return $contenidoCache;
		}
	}

	function logins($intervalDays = 0) {
		global $cache;
		
		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_logins'.'_dias_'.$intervalDays.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"];
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_logins'.'_dias_'.$intervalDays;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT count(idLogin) as logins FROM logins WHERE Fecha BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'"';
			
			$usuarioHistorico = Aplicacion::getParametros("conexionh", "usuario");
		    $passwordHistorico = Aplicacion::Decrypter(Aplicacion::getParametros("conexionh", "password"));
		    $serverHistorico = Aplicacion::getParametros("conexionh", "server");
		    $baseHistorico = Aplicacion::getParametros("conexionh","base");
	
			$loginDB = mysql_connect($serverHistorico, $usuarioHistorico, $passwordHistorico, true);
			mysql_select_db($baseHistorico, $loginDB);
			$result = mysql_query($query, $loginDB);
			$res = array();
			
			while ($fila = mysql_fetch_assoc($result)) {
				array_push($res, $fila);
			}

			$registrados = array();
			foreach($res as $result) {
				$registrados[$result["mes"]] = $result["logins"];
			}
	
			if($res[0]["logins"] == null) {
				$res[0]["logins"] = 0;
			}
			
			$cache->save($res[0]["logins"], $nombreCache);
			return $res[0]["logins"];
		} else {
			return $contenidoCache;
		}
	}
	
	function productosPagoAceptado($intervalDays = 0) {
		global $cache;
		
		if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
			$nombreCache = 'cache_estadisticas_diarias_productos_pago_aceptado_dias_'.$intervalDays.'_fecha_'.$_GET["anio"].'_'.$_GET["mes"].'_'.$_GET["dia"];
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, $_GET["mes"], $_GET["dia"] - $intervalDays, $_GET["anio"]));
		} else {
			$nombreCache = 'cache_estadisticas_diarias_productos_pago_aceptado_dias_'.$intervalDays;
			$toDateText = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d") - $intervalDays, date("Y")));
			$fromDateText = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - $intervalDays, date("Y")));
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$query = 'SELECT SUM(pxp.Cantidad) as productos FROM pedidos p inner join productos_x_pedidos pxp on p.IdPedido = pxp.IdPedido WHERE p.IdEstadoPedidos IN (2, 3, 6, 7, 9) AND p.Fecha BETWEEN "'.$fromDateText.'" AND "'.$toDateText.'"';
	
		    $oConexion = Conexion::nuevo();
			$oConexion->setQuery($query);
			$res = $oConexion->DevolverQuery();
			
			if($res[0]["productos"] == null) {
				$res[0]["productos"] = 0;
			}
			
			$cache->save($res[0]["productos"], $nombreCache);
			return $res[0]["productos"];
		} else {
			return $contenidoCache;
		}
	}
	
	function recordRegistrados($tipoUsuario = null) {
		global $cache;
		$nombreCache = 'cache_estadisticas_diarias_records_registrados_tipodeusuario_'.$tipoUsuario;
		
		$recordDiscriminator = "";
		if ($tipoUsuario == USUARIOS_ACTIVOS) {
			$recordDiscriminator = "Activo";
			$tipoUsuarioWhere = ' u.es_activa = 0 AND u.IdPerfil = 1';
		} else if ($tipoUsuario == USUARIOS_INACTIVOS) {
			$recordDiscriminator = "Inactivo";
			$tipoUsuarioWhere = ' u.es_activa = 1 AND u.es_Provisoria = 1 AND u.Padrino <> "Huerfano" AND u.IdPerfil = 1';
		} else {
			$tipoUsuarioWhere = ' 1 = 1 ';
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$fechaRecord = date("Y-m-d");
			$valorRecord = obtenerValorRecord("recordRegistrados".$recordDiscriminator, $fechaRecord);
			if(!isset($valorRecord)) {
				$query = 'SELECT COUNT(u.IdUsuario) as registrados, date(u.FechaIngreso) as fecha FROM usuarios u WHERE '.$tipoUsuarioWhere.' GROUP BY date(u.FechaIngreso) ORDER BY registrados desc limit 1';
		
			    $oConexion = Conexion::nuevo();
				$oConexion->setQuery($query);
				$res = $oConexion->DevolverQuery();
				
				if($res[0]["registrados"] == null) {
					$res[0]["registrados"] = 0;
				}
				
				$cache->save($res[0], $nombreCache);
				guardarValorRecord("recordRegistrados".$recordDiscriminator, $fechaRecord, $res[0]["registrados"], $res[0]["fecha"]);
				return $res[0];
			} else {
				$resultado = array();
				$resultado["registrados"] = $valorRecord["valor_estadistica"];
				$resultado["fecha"] = $valorRecord["fecha_record"];
				$cache->save($resultado, $nombreCache);
				return $resultado;
			}
		} else {
			return $contenidoCache;
		}
	}
	
	function recordInvitaciones($unicas = false) {
		global $cache;
		$nombreCache = 'cache_estadisticas_diarias_record_invitaciones'.'_unicas_'.$unicas;
		
		$recordDiscrimator = "";
		if($unicas) {
			$recordDiscrimator = "Unicas";
			$countText = " distinct(invitation_to) ";
		} else {
			$countText = " id ";
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$fechaRecord = date("Y-m-d");
			$valorRecord = obtenerValorRecord("recordInvitaciones".$recordDiscrimator, $fechaRecord);
			if(!isset($valorRecord)) {
				$query = 'SELECT date(invitation_date) as fecha, count('.$countText.') as invitaciones FROM geelbe_sent_invitations where invitation_date != "0000-00-00 00:00:00" group by date(invitation_date) order by invitaciones desc limit 1';
	
				$oConexion = Conexion::nuevo();
				$oConexion->setQuery($query);
				$res = $oConexion->DevolverQuery();
				
				if($res[0]["invitaciones"] == null) {
					$res[0]["invitaciones"] = 0;
				}
				
				$cache->save($res[0], $nombreCache);
				guardarValorRecord("recordInvitaciones".$recordDiscrimator, $fechaRecord, $res[0]["invitaciones"], $res[0]["fecha"]);
				return $res[0];
			} else {
				$resultado = array();
				$resultado["invitaciones"] = $valorRecord["valor_estadistica"];
				$resultado["fecha"] = $valorRecord["fecha_record"];
				$cache->save($resultado, $nombreCache);
				return $resultado;
			}
		} else {
			return $contenidoCache;
		}
	}
	
	function recordPedidosPagoAceptado() {
		global $cache;
		$nombreCache = 'cache_estadisticas_diarias_record_pedidos_pago_aceptado';

		if(!$contenidoCache = $cache->load($nombreCache)) {
			$fechaRecord = date("Y-m-d");
			$valorRecord = obtenerValorRecord("recordPedidosPagoAceptado", $fechaRecord);
			if(!isset($valorRecord)) {
				$query = 'SELECT date(p.Fecha) as fecha, count(p.IdPedido) as pedidos FROM pedidos p where p.IdEstadoPedidos IN (2, 3, 6, 7, 9) group by date(p.Fecha) order by pedidos desc limit 1';
		
			    $oConexion = Conexion::nuevo();
				$oConexion->setQuery($query);
				$res = $oConexion->DevolverQuery();
				
				if($res[0]["pedidos"] == null) {
					$res[0]["pedidos"] = 0;
				}
				
				$cache->save($res[0], $nombreCache);
				guardarValorRecord("recordPedidosPagoAceptado", $fechaRecord, $res[0]["pedidos"], $res[0]["fecha"]);
				return $res[0];
			} else {
				$resultado = array();
				$resultado["pedidos"] = $valorRecord["valor_estadistica"];
				$resultado["fecha"] = $valorRecord["fecha_record"];
				$cache->save($resultado, $nombreCache);
				return $resultado;
			}
		} else {
			return $contenidoCache;
		}
	}
	
	function recordPedidosEsperandoSistemadePago() {
		global $cache;
		$nombreCache = 'cache_estadisticas_diarias_record_pedidos_esperando_sistema_de_pago';

		if(!$contenidoCache = $cache->load($nombreCache)) {
			$fechaRecord = date("Y-m-d");
			$valorRecord = obtenerValorRecord("recordPedidosEsperandoSistemadePago", $fechaRecord);
			if(!isset($valorRecord)) {
				$query = 'SELECT date(p.Fecha) as fecha, count(p.IdPedido) as pedidos FROM pedidos p where p.IdEstadoPedidos = (0) group by date(p.Fecha) order by pedidos desc limit 1';
		
			    $oConexion = Conexion::nuevo();
				$oConexion->setQuery($query);
				$res = $oConexion->DevolverQuery();
				
				if($res[0]["pedidos"] == null) {
					$res[0]["pedidos"] = 0;
				}
				
				$cache->save($res[0], $nombreCache);
				guardarValorRecord("recordPedidosEsperandoSistemadePago", $fechaRecord, $res[0]["pedidos"], $res[0]["fecha"]);
				return $res[0];
			} else {
				$resultado = array();
				$resultado["pedidos"] = $valorRecord["valor_estadistica"];
				$resultado["fecha"] = $valorRecord["fecha_record"];
				$cache->save($resultado, $nombreCache);
				return $resultado;
			}
		} else {
			return $contenidoCache;
		}
	}
	
	function recordMontoPedidosPagoAceptado($SinCostoDeEnvio = false) {
		global $cache;
		$nombreCache = 'cache_estadisticas_diarias_record_monto_pedidos_pago_aceptado_costo_de_envio_'.$SinCostoDeEnvio;
		
		$recordDiscriminator = "";
		if($SinCostoDeEnvio) {
			$recordDiscriminator = "ConEnvio";
			$SinCostoDeEnvioText = " - p.GastosEnvio";
		} else {
			$SinCostoDeEnvioText = "";
		}

		if(!$contenidoCache = $cache->load($nombreCache)) {
			$fechaRecord = date("Y-m-d");
			$valorRecord = obtenerValorRecord("recordMontoPedidosPA".$recordDiscriminator, $fechaRecord);
			if(!isset($valorRecord)) {
				$query = 'SELECT date(p.Fecha) as fecha, SUM(p.Total'.$SinCostoDeEnvioText.') as pedidos FROM pedidos p where p.IdEstadoPedidos IN (2, 3, 6, 7, 9) group by date(p.Fecha) order by pedidos desc limit 1';
		
			    $oConexion = Conexion::nuevo();
				$oConexion->setQuery($query);
				$res = $oConexion->DevolverQuery();
				
				if($res[0]["pedidos"] == null) {
					$res[0]["pedidos"] = 0;
				}
				
				$cache->save($res[0], $nombreCache);
				guardarValorRecord("recordMontoPedidosPA".$recordDiscriminator, $fechaRecord, $res[0]["pedidos"], $res[0]["fecha"]);
				return $res[0];
			} else {
				$resultado = array();
				$resultado["pedidos"] = $valorRecord["valor_estadistica"];
				$resultado["fecha"] = $valorRecord["fecha_record"];
				$cache->save($resultado, $nombreCache);
				return $resultado;
			}
		} else {
			return $contenidoCache;
		}
	}
	
	function recordCostoDeEnvio() {
		global $cache;
		$nombreCache = 'cache_estadisticas_diarias_record_costo_de_envio';
			
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$fechaRecord = date("Y-m-d");
			$valorRecord = obtenerValorRecord("recordCostoDeEnvio", $fechaRecord);
			if(!isset($valorRecord)) {
				$query = 'SELECT date(p.Fecha) as fecha, SUM(p.GastosEnvio) as costo FROM pedidos p where p.IdEstadoPedidos IN (2, 3, 6, 7, 9) group by date(p.Fecha) order by costo desc limit 1';
		
			    $oConexion = Conexion::nuevo();
				$oConexion->setQuery($query);
				$res = $oConexion->DevolverQuery();
				
				if($res[0]["costo"] == null) {
					$res[0]["costo"] = 0;
				}
				
				$cache->save($res[0], $nombreCache);
				guardarValorRecord("recordCostoDeEnvio", $fechaRecord, $res[0]["costo"], $res[0]["fecha"]);
				return $res[0];
			} else {
				$resultado = array();
				$resultado["costo"] = $valorRecord["valor_estadistica"];
				$resultado["fecha"] = $valorRecord["fecha_record"];
				$cache->save($resultado, $nombreCache);
				return $resultado;
			}
		} else {
			return $contenidoCache;
		}
	}
	
	function recordCantidadMensajes() {
		global $cache;
		$nombreCache = 'cache_estadisticas_diarias_record_cantidad_de_mensajes';

		if(!$contenidoCache = $cache->load($nombreCache)) {
			$fechaRecord = date("Y-m-d");
			$valorRecord = obtenerValorRecord("recordCantidadMensajes", $fechaRecord);
			if(!isset($valorRecord)) {
				$query = 'SELECT date(m.fecha) as fecha, count(m.idMensaje) as mensajes FROM mensajes m where m.idMensajePadre IS NULL group by date(m.fecha) order by mensajes desc limit 1';
		
			    $oConexion = Conexion::nuevo();
				$oConexion->setQuery($query);
				$res = $oConexion->DevolverQuery();
				
				if($res[0]["mensajes"] == null) {
					$res[0]["mensajes"] = 0;
				}
				
				$cache->save($res[0], $nombreCache);
				guardarValorRecord("recordCantidadMensajes", $fechaRecord, $res[0]["mensajes"], $res[0]["fecha"]);
				return $res[0];
			} else {
				$resultado = array();
				$resultado["mensajes"] = $valorRecord["valor_estadistica"];
				$resultado["fecha"] = $valorRecord["fecha_record"];
				$cache->save($resultado, $nombreCache);
				return $resultado;
			}
		} else {
			return $contenidoCache;
		}
	}
	
	function recordLogins() {
		global $cache;
		$nombreCache = 'cache_estadisticas_diarias_record_logins';

		if(!$contenidoCache = $cache->load($nombreCache)) {
			$fechaRecord = date("Y-m-d");
			$valorRecord = obtenerValorRecord("recordLogins", $fechaRecord);
			if(!isset($valorRecord)) {
				$query = 'SELECT date(l.Fecha) as fecha, COUNT(l.idLogin) as logins FROM logins l group by date(l.fecha) order by logins desc limit 1';
		
				$usuarioHistorico = Aplicacion::getParametros("conexionh", "usuario");
			    $passwordHistorico = Aplicacion::Decrypter(Aplicacion::getParametros("conexionh", "password"));
			    $serverHistorico = Aplicacion::getParametros("conexionh", "server");
			    $baseHistorico = Aplicacion::getParametros("conexionh","base");
		
				$loginDB = mysql_connect($serverHistorico, $usuarioHistorico, $passwordHistorico, true);
				mysql_select_db($baseHistorico, $loginDB);
				$result = mysql_query($query, $loginDB);
				$res = array();
	
				while ($fila = mysql_fetch_assoc($result)) {
					array_push($res, $fila);
				}
				
				if($res[0]["logins"] == null) {
					$res[0]["logins"] = 0;
				}
				
				$cache->save($res[0], $nombreCache);
				guardarValorRecord("recordLogins", $fechaRecord, $res[0]["logins"], $res[0]["fecha"]);
				return $res[0];
			} else {
				$resultado = array();
				$resultado["logins"] = $valorRecord["valor_estadistica"];
				$resultado["fecha"] = $valorRecord["fecha_record"];
				$cache->save($resultado, $nombreCache);
				return $resultado;
			}
		} else {
			return $contenidoCache;
		}
	}
	
	function recordTicketPromedio($SinCostoDeEnvio = false) {
		global $cache;
		$nombreCache = 'cache_estadisticas_diarias_record_ticket_promedio_costo_de_envio_'.$SinCostoDeEnvio;
		
		$recordDiscriminator = "";
		if($SinCostoDeEnvio) {
			$recordDiscriminator = "ConEnvio";
			$SinCostoDeEnvioText = " - p.GastosEnvio";
		} else {
			$SinCostoDeEnvioText = "";
		}
		
		if(!$contenidoCache = $cache->load($nombreCache)) {
			$fechaRecord = date("Y-m-d");
			$valorRecord = obtenerValorRecord("recordTicketPromedio".$recordDiscriminator, $fechaRecord);
			if(!isset($valorRecord)) {
				$query = 'SELECT date(p.Fecha) as fecha, SUM(p.Total'.$SinCostoDeEnvioText.')/COUNT(p.IdPedido) as pedidos FROM pedidos p where p.IdEstadoPedidos IN (2, 3, 6, 7, 9) group by date(p.Fecha) order by pedidos desc limit 1';
		
			    $oConexion = Conexion::nuevo();
				$oConexion->setQuery($query);
				$res = $oConexion->DevolverQuery();
				
				if($res[0]["pedidos"] == null) {
					$res[0]["pedidos"] = 0;
				}
				
				$cache->save($res[0], $nombreCache);
				guardarValorRecord("recordTicketPromedio".$recordDiscriminator, $fechaRecord, $res[0]["pedidos"], $res[0]["fecha"]);
				return $res[0];
			} else {
				$resultado = array();
				$resultado["pedidos"] = $valorRecord["valor_estadistica"];
				$resultado["fecha"] = $valorRecord["fecha_record"];
				$cache->save($resultado, $nombreCache);
				return $resultado;
			}
		} else {
			return $contenidoCache;
		}
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Estad&iacute;sticas</title>
	<link href="/<?=$confRoot[1]?>/bo/css/global.css" rel="stylesheet" type="text/css" />
	<link href="/<?=$confRoot[1]?>/css/listado_bo.css" rel="stylesheet" type="text/css" />
	<link href="/<?=$confRoot[1]?>/css/jquery.ui.datepicker/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
	
	<script src="<?="/".$confRoot[1]."/js/jquery-1.4.2.min.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/js/jquery.ui.core.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/js/jquery.ui.datepicker.js"?>" type="text/javascript"></script>
	<script src="<?="/".$confRoot[1]."/js/jquery.datepick-es.js"?>" type="text/javascript"></script>
</head>

<body>
<style type="text/css">
	th {
		background-color: #ffefef;
	}
	
	td.value {
		text-align:center;
	}
</style>

	<div style="position: absolute; right: 20%" id="calendario"></div>
	<script type="text/javascript">
	  $('#calendario').datepicker({
		    maxDate: "+0",
			changeMonth: true,
			changeYear: true,
			onSelect: function(dateText, inst) { 
				window.location.href = "/<?=$confRoot[1]?>/bo/estadisticas/diarias.php?dia="+inst.currentDay+"&mes="+(inst.currentMonth+1)+"&anio="+inst.currentYear;
		    }
	  });
	  <?php
	  	if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
		  $formatDate = sprintf("%02d/%02d/%02d",  $_GET["mes"],  $_GET["dia"],  $_GET["anio"]);
	  ?>
	  $( "#calendario").datepicker( "option", "defaultDate", "<?=$formatDate?>" );
	  $( "#calendario").datepicker( "setDate", "<?=$formatDate?>" );
	  $( "#calendario").datepicker( "refresh" );
	  <?php
	  	}
	  ?>
	</script>
  <div id="container">
	<h1>Diarias</h1>
		
	<p><b>Nota:</b> los datos son actualizados cada 10 minutos.</p><br />
	<p><b>Nota 2:</b> los records son actualizados cada 24 horas.</p><br />
	<br /><br />
	<br /><br />
	<br />
	<table>
	  <tr  style="text-align:center; font-weight:bold;color:#DD3333">
		<td NOWRAP>&nbsp;</td>
		<?
 	  	  if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
		      $mesHoy = $_GET["mes"];
		      $diaHoy = $_GET["dia"];
		      $anioHoy = $_GET["anio"];
		      $mesAyer = date("m", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - 1, $_GET["anio"]));
		      $diaAyer = date("d", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - 1, $_GET["anio"]));
		      $anioAyer = date("Y", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - 1, $_GET["anio"]));
		      $mesAntesDeAyer = date("m", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - 2, $_GET["anio"]));
		      $diaAntesDeAyer = date("d", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - 2, $_GET["anio"]));
		      $anioAntesDeAyer = date("Y", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - 2, $_GET["anio"]));
	   ?>
			<td NOWRAP><?=$diaHoy."/".$mesHoy."/".$anioHoy?></td>
			<td NOWRAP><?=$diaAyer."/".$mesAyer."/".$anioAyer?></td>
			<td NOWRAP><?=$diaAntesDeAyer."/".$mesAntesDeAyer."/".$anioAntesDeAyer?></td>
			<td NOWRAP>Records</td>
	    <?
	  	  } else {
	   ?>
			<td NOWRAP>Hoy</td>
			<td NOWRAP>Ayer</td>
			<td NOWRAP>Antes de ayer</td>
			<td NOWRAP>Records</td>
	    <?
	  	  }
	  	?>
	  </tr>
	  <? 
		  $recordRegistrados = recordRegistrados(TODOS);
		  $recordActivos = recordRegistrados(USUARIOS_ACTIVOS);
		  $recordInactivos = recordRegistrados(USUARIOS_INACTIVOS);
		  $registrados = registrados();
		  $activos = registrados(USUARIOS_ACTIVOS);
		  $inactivos = registrados(USUARIOS_INACTIVOS);
		  $registradosAyer = registrados(TODOS, 1);
		  $activosAyer = registrados(USUARIOS_ACTIVOS, 1);
		  $inactivosAyer = registrados(USUARIOS_INACTIVOS, 1);
		  $registradosAntesDeAyer = registrados(TODOS, 2);
		  $activosAntesDeAyer = registrados(USUARIOS_ACTIVOS, 2);
		  $inactivosAntesDeAyer = registrados(USUARIOS_INACTIVOS, 2);
		  
		  // $recordInvitaciones = recordInvitaciones();
		  // $recordInvitacionesUnicas = recordInvitaciones(true);
		  
		  $pedidosPagoAceptado = pedidosPagoAceptado();
		  $pedidosPagoAceptadoAyer = pedidosPagoAceptado(1);
		  $pedidosPagoAceptadoAntesDeAyer = pedidosPagoAceptado(2);
		  $recordPedidosPagoAceptado = recordPedidosPagoAceptado();
		  
		  $montoPedidosPagoAceptado = montoPedidosPagoAceptado();
		  $montoPedidosPagoAceptadoAyer = montoPedidosPagoAceptado(1);
		  $montoPedidosPagoAceptadoAntesDeAyer = montoPedidosPagoAceptado(2);
		  $recordMontoPedidosPagoAceptado = recordMontoPedidosPagoAceptado();
		  
		  $recordTicketPromedio = recordTicketPromedio();
		  $recordTicketPromedioSinCostoDeEnvio = recordTicketPromedio(true);
		  
		  $montoPedidosPagoAceptadoSinCostoDeEnvio = montoPedidosPagoAceptado(0, true);
		  $montoPedidosPagoAceptadoAyerSinCostoDeEnvio = montoPedidosPagoAceptado(1, true);
		  $montoPedidosPagoAceptadoAntesDeAyerSinCostoDeEnvio = montoPedidosPagoAceptado(2, true);
		  $recordMontoPedidosPagoAceptadoSinCostoDeEnvio = recordMontoPedidosPagoAceptado(true);
		  
		  $costoDeEnvio = costoDeEnvio();
		  $costoDeEnvioAyer = costoDeEnvio(1);
		  $costoDeEnvioAntesDeAyer = costoDeEnvio(2);
		  $recordCostoDeEnvio = recordCostoDeEnvio();
		  		  
		  $pedidosEsperandoSistemadePago = pedidosEsperandoSistemadePago();
		  $pedidosEsperandoSistemadePagoAyer = pedidosEsperandoSistemadePago(1);
		  $pedidosEsperandoSistemadePagoAntesDeAyer = pedidosEsperandoSistemadePago(2);
		  $recordPedidosEsperandoSistemadePagoAceptado = recordPedidosEsperandoSistemadePago();
		  
		  $cantidadMensajes = cantidadMensajes();
		  $cantidadMensajesAyer = cantidadMensajes(1);
		  $cantidadMensajesAntesDeAyer = cantidadMensajes(2);
		  $recordCantidadMensajes = recordCantidadMensajes();
		  
		  $logins = logins();
		  $loginsAyer = logins(1);
		  $loginsAntesDeAyer = logins(2);
		  $recordLogins = recordLogins();
		  
		  $cantidadProductos = productosPagoAceptado();
		  $cantidadProductosAyer = productosPagoAceptado(1);
		  $cantidadProductosAntesDeAyer = productosPagoAceptado(2);
	  ?>
	  <tr>
	    <th NOWRAP>Registros</th>
	    <?
 	  	  if(isset($_GET["dia"]) && isset($_GET["mes"]) && isset($_GET["anio"])) {
		      $anioHoy = $_GET["anio"];
		      $mesHoy = $_GET["mes"];
		      $diaHoy = $_GET["dia"];
		      $anioAyer = date("Y", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - 1, $_GET["anio"]));
		      $mesAyer = date("m", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - 1, $_GET["anio"]));
		      $diaAyer = date("d", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - 1, $_GET["anio"]));
		      $anioAntesDeAyer = date("Y", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - 2, $_GET["anio"]));
		      $mesAntesDeAyer = date("m", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - 2, $_GET["anio"]));
		      $diaAntesDeAyer = date("d", mktime(23, 59, 59, $_GET["mes"], $_GET["dia"] - 2, $_GET["anio"]));
	  	  } else {
		      $anioHoy = date("Y");
		      $mesHoy = date("m");
		      $diaHoy = date("d");
		      $anioAyer = date("Y", mktime(23, 59, 59, date("m"), date("d") - 1, date("Y")));
		      $mesAyer = date("m", mktime(23, 59, 59, date("m"), date("d") - 1, date("Y")));
		      $diaAyer = date("d", mktime(23, 59, 59, date("m"), date("d") - 1, date("Y")));
		      $anioAntesDeAyer = date("Y", mktime(23, 59, 59, date("m"), date("d") - 2, date("Y")));
		      $mesAntesDeAyer = date("m", mktime(23, 59, 59, date("m"), date("d") - 2, date("Y")));
		      $diaAntesDeAyer = date("d", mktime(23, 59, 59, date("m"), date("d") - 2, date("Y")));
	  	  }
	    
	    ?>
		<td NOWRAP style="text-align:center"><a href="details/users.php?day=<?=$diaHoy?>&month=<?=$mesHoy?>&year=<?=$anioHoy?>">Ver detalle</a></td>
		<td NOWRAP style="text-align:center"><a href="details/users.php?day=<?=$diaAyer?>&month=<?=$mesAyer?>&year=<?=$anioAyer?>">Ver detalle</a></td>
		<td NOWRAP style="text-align:center"><a href="details/users.php?day=<?=$diaAntesDeAyer?>&month=<?=$mesAntesDeAyer?>&year=<?=$anioAntesDeAyer?>">Ver detalle</a></td>
		<td NOWRAP>&nbsp;</td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Cantidad de Registrados</td>
		<td NOWRAP class="value"><?= $registrados ?></td>
		<td NOWRAP class="value"><?= $registradosAyer ?></td>
		<td NOWRAP class="value"><?= $registradosAntesDeAyer ?></td>
		<td NOWRAP class="value"><?=  $recordRegistrados["registrados"]." (".$recordRegistrados["fecha"].")" ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Activos en Cantidad</td>
		<td NOWRAP class="value"><?= $activos ?></td>
		<td NOWRAP class="value"><?= $activosAyer ?></td>
		<td NOWRAP class="value"><?= $activosAntesDeAyer ?></td>
		<td NOWRAP class="value"><?= $recordActivos["registrados"]." (".$recordActivos["fecha"].")" ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Activos en Porcentaje</td>
		<td NOWRAP class="value"><?= number_format($activos/$registrados*100, 1, ',', '')."%"  ?></td>
		<td NOWRAP class="value"><?= number_format($activosAyer/$registradosAyer*100, 1, ',', '')."%"  ?></td>
		<td NOWRAP class="value"><?= number_format($activosAntesDeAyer/$registradosAntesDeAyer*100, 1, ',', '')."%"  ?></td>
		<td NOWRAP class="value">N/A</td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Inactivos en Cantidad</td>
		<td NOWRAP class="value"><?= $inactivos ?></td>
		<td NOWRAP class="value"><?= $inactivosAyer ?></td>
		<td NOWRAP class="value"><?= $inactivosAntesDeAyer ?></td>
		<td NOWRAP class="value"><?= $recordInactivos["registrados"]." (".$recordInactivos["fecha"].")" ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Inactivos en Porcentaje</td>
		<td NOWRAP class="value"><?= number_format($inactivos/$registrados*100, 1, ',', '')."%" ?></td>
		<td NOWRAP class="value"><?= number_format($inactivosAyer/$registradosAyer*100, 1, ',', '')."%" ?></td>
		<td NOWRAP class="value"><?= number_format($inactivosAntesDeAyer/$registradosAntesDeAyer*100, 1, ',', '')."%" ?></td>
		<td NOWRAP class="value">N/A</td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Cantidad de Invitaciones</td>
		<td NOWRAP class="value"><?= invitaciones() ?></td>
		<td NOWRAP class="value"><?= invitaciones(1) ?></td>
		<td NOWRAP class="value"><?= invitaciones(2) ?></td>
		<td NOWRAP class="value"><?= "N/A" ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Cantidad de Invitaciones &Uacute;nicas</td>
		<td NOWRAP class="value"><?= invitaciones(0, true) ?></td>
		<td NOWRAP class="value"><?= invitaciones(1, true) ?></td>
		<td NOWRAP class="value"><?= invitaciones(2, true) ?></td>
		<td NOWRAP class="value"><?= "N/A" ?></td>
	  </tr>
	  <tr>
	    <th NOWRAP>Ventas</th>
		<td NOWRAP>&nbsp;</td>
		<td NOWRAP>&nbsp;</td>
		<td NOWRAP>&nbsp;</td>
		<td NOWRAP>&nbsp;</td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Cantidad de Pedidos con Pago Aceptado</td>
		<td NOWRAP class="value"><?= $pedidosPagoAceptado ?></td>
		<td NOWRAP class="value"><?= $pedidosPagoAceptadoAyer ?></td>
		<td NOWRAP class="value"><?= $pedidosPagoAceptadoAntesDeAyer ?></td>
		<td NOWRAP class="value"><?=$recordPedidosPagoAceptado["pedidos"]." (".$recordPedidosPagoAceptado["fecha"].")" ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Monto de Pedidos con Pago Aceptado</td>
		<td NOWRAP class="value"><?= "$ ".number_format($montoPedidosPagoAceptado, 2, ',', '.') ?></td>
		<td NOWRAP class="value"><?= "$ ".number_format($montoPedidosPagoAceptadoAyer, 2, ',', '.') ?></td>
		<td NOWRAP class="value"><?= "$ ".number_format($montoPedidosPagoAceptadoAntesDeAyer, 2, ',', '.') ?></td>
		<td NOWRAP class="value"><?= "$ ".number_format($recordMontoPedidosPagoAceptado["pedidos"], 2, ',', '.')." (".$recordMontoPedidosPagoAceptado["fecha"].")" ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Ticket Promedio con Costo de Env&iacute;o</td>
		<td NOWRAP class="value"><?= ($pedidosPagoAceptado != 0)?"$ ".number_format($montoPedidosPagoAceptado/$pedidosPagoAceptado, 2, ',', '.'):MonedaConDecimales(0) ?></td>
		<td NOWRAP class="value"><?= ($pedidosPagoAceptadoAyer != 0)?"$ ".number_format($montoPedidosPagoAceptadoAyer/$pedidosPagoAceptadoAyer, 2, ',', '.'):MonedaConDecimales(0) ?></td>
		<td NOWRAP class="value"><?= ($pedidosPagoAceptadoAntesDeAyer != 0)?"$ ".number_format($montoPedidosPagoAceptadoAntesDeAyer/$pedidosPagoAceptadoAntesDeAyer, 2, ',', '.'):MonedaConDecimales(0) ?></td>
		<td NOWRAP class="value"><?= "$ ".number_format($recordTicketPromedio["pedidos"], 2, ',', '.')." (".$recordTicketPromedio["fecha"].")" ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Monto de Pedidos con Pago Aceptado sin Costo de Env&iacute;o</td>
		<td NOWRAP class="value"><?= "$ ".number_format($montoPedidosPagoAceptadoSinCostoDeEnvio, 2, ',', '.') ?></td>
		<td NOWRAP class="value"><?= "$ ".number_format($montoPedidosPagoAceptadoAyerSinCostoDeEnvio, 2, ',', '.') ?></td>
		<td NOWRAP class="value"><?= "$ ".number_format($montoPedidosPagoAceptadoAntesDeAyerSinCostoDeEnvio, 2, ',', '.') ?></td>
		<td NOWRAP class="value"><?= "$ ".number_format($recordMontoPedidosPagoAceptadoSinCostoDeEnvio["pedidos"], 2, ',', '.')." (".$recordMontoPedidosPagoAceptadoSinCostoDeEnvio["fecha"].")" ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Ticket Promedio sin Costo de Env&iacute;o</td>
		<td NOWRAP class="value"><?= ($pedidosPagoAceptado != 0)?"$ ".number_format($montoPedidosPagoAceptadoSinCostoDeEnvio/$pedidosPagoAceptado, 2, ',', '.'):MonedaConDecimales(0) ?></td>
		<td NOWRAP class="value"><?= ($pedidosPagoAceptadoAyer != 0)?"$ ".number_format($montoPedidosPagoAceptadoAyerSinCostoDeEnvio/$pedidosPagoAceptadoAyer, 2, ',', '.'):MonedaConDecimales(0) ?></td>
		<td NOWRAP class="value"><?= ($pedidosPagoAceptadoAntesDeAyer != 0)?"$ ".number_format($montoPedidosPagoAceptadoAntesDeAyerSinCostoDeEnvio/$pedidosPagoAceptadoAntesDeAyer, 2, ',', '.'):MonedaConDecimales(0) ?></td>
		<td NOWRAP class="value"><?= "$ ".number_format($recordTicketPromedioSinCostoDeEnvio["pedidos"], 2, ',', '.')." (".$recordTicketPromedioSinCostoDeEnvio["fecha"].")" ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Monto Total del Costo de Env&iacute;o</td>
		<td NOWRAP class="value"><?= "$ ".number_format($costoDeEnvio, 2, ',', '.') ?></td>
		<td NOWRAP class="value"><?= "$ ".number_format($costoDeEnvioAyer, 2, ',', '.') ?></td>
		<td NOWRAP class="value"><?= "$ ".number_format($costoDeEnvioAntesDeAyer, 2, ',', '.') ?></td>
		<td NOWRAP class="value"><?= "$ ".number_format($recordCostoDeEnvio["costo"], 2, ',', '.')." (".$recordCostoDeEnvio["fecha"].")" ?></td>
	  </tr>	  
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Cantidad de Pedidos en Esperando Sistema de Pago</td>
		<td NOWRAP class="value"><?= $pedidosEsperandoSistemadePago ?></td>
		<td NOWRAP class="value"><?= $pedidosEsperandoSistemadePagoAyer ?></td>
		<td NOWRAP class="value"><?= $pedidosEsperandoSistemadePagoAntesDeAyer ?></td>
		<td NOWRAP class="value"><?= $recordPedidosEsperandoSistemadePagoAceptado["pedidos"]." (".$recordPedidosEsperandoSistemadePagoAceptado["fecha"].")" ?></td>
	  </tr>
	  
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Cantidad de Mensajes</td>
		<td NOWRAP class="value"><?= $cantidadMensajes ?></td>
		<td NOWRAP class="value"><?= $cantidadMensajesAyer ?></td>
		<td NOWRAP class="value"><?= $cantidadMensajesAntesDeAyer ?></td>
		<td NOWRAP class="value"><?= $recordCantidadMensajes["mensajes"]." (".$recordCantidadMensajes["fecha"].")" ?></td>
	  </tr>
	  
	  <tr>
	    <th NOWRAP>Logins</th>
		<td NOWRAP>&nbsp;</td>
		<td NOWRAP>&nbsp;</td>
		<td NOWRAP>&nbsp;</td>
		<td NOWRAP>&nbsp;</td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Cantidad de Logins</td>
		<td NOWRAP class="value"><?= $logins ?></td>
		<td NOWRAP class="value"><?= $loginsAyer ?></td>
		<td NOWRAP class="value"><?= $loginsAntesDeAyer ?></td>
		<td NOWRAP class="value"><?= $recordLogins["logins"]." (".$recordLogins["fecha"].")" ?></td>
	  </tr>
	  <tr>
	    <th NOWRAP>Ratios</th>
		<td NOWRAP>&nbsp;</td>
		<td NOWRAP>&nbsp;</td>
		<td NOWRAP>&nbsp;</td>
		<td NOWRAP>&nbsp;</td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Ratio de Logins/Cantidad de Ventas</td>
		<td NOWRAP class="value"><?= ($pedidosPagoAceptado==0)?0:number_format($logins/$pedidosPagoAceptado, 2, ',', '') ?></td>
		<td NOWRAP class="value"><?= ($pedidosPagoAceptadoAyer==0)?0:number_format($loginsAyer/$pedidosPagoAceptadoAyer, 2, ',', '') ?></td>
		<td NOWRAP class="value"><?= ($pedidosPagoAceptadoAntesDeAyer==0)?0:number_format($loginsAntesDeAyer/$pedidosPagoAceptadoAntesDeAyer, 2, ',', '') ?></td>
		<td NOWRAP class="value"><?= "N/A" ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Ratio de Logins/Monto de Pago Aceptado</td>
		<td NOWRAP class="value"><?= "$ ".(($montoPedidosPagoAceptado==0)?0:number_format($logins/$montoPedidosPagoAceptado, 4, ',', '.')) ?></td>
		<td NOWRAP class="value"><?= "$ ".(($montoPedidosPagoAceptadoAyer==0)?0:number_format($loginsAyer/$montoPedidosPagoAceptadoAyer, 4, ',', '.')) ?></td>
		<td NOWRAP class="value"><?= "$ ".(($montoPedidosPagoAceptadoAntesDeAyer==0)?0:number_format($loginsAntesDeAyer/$montoPedidosPagoAceptadoAntesDeAyer, 4, ',', '.')) ?></td>
		<td NOWRAP class="value"><?= "N/A" ?></td>
	  </tr>
	  <tr>
		<td NOWRAP>&nbsp;&nbsp;&nbsp;&nbsp;Cantidad de Productos por Pedido</td>
		<td NOWRAP class="value"><?= ($cantidadProductos==0)?0:number_format($cantidadProductos/$pedidosPagoAceptado, 2, ',', '') ?></td>
		<td NOWRAP class="value"><?= ($cantidadProductosAyer==0)?0:number_format($cantidadProductosAyer/$pedidosPagoAceptadoAyer, 2, ',', '') ?></td>
		<td NOWRAP class="value"><?= ($cantidadProductosAntesDeAyer==0)?0:number_format($cantidadProductosAntesDeAyer/$pedidosPagoAceptadoAntesDeAyer, 2, ',', '') ?></td>
		<td NOWRAP class="value"><?= "N/A" ?></td>
	  </tr>
	</table>
	<br /><br />
	<table style="margin-bottom:10px;">
		<tr class="alt">
			<td style="text-align:left;font-weight:bold;">Descripción</td>
			<td style="font-weight:bold;">Total</td>
			<td style="font-weight:bold;">Porcentaje (%)</td>
		</tr>
		<tr>
			<td style="text-align:left;font-weight:bold;">Todos los Clientes</td>
			<td><?=$countTotal?></td>
			<td><?=round($countTotal*100/$countTotal,2)?>%</td>
		</tr>
		<tr class="alt">
			<td style="text-align:left;font-weight:bold;">Clientes Activos</td>
			<td><?=$countActivos?></td>
			<td><?=round($countActivos*100/$countTotal,2)?>%</td>
		</tr>
		<tr>
			<td style="text-align:left;font-weight:bold;">Clientes Inactivos</td>
			<td><?=$countInactivos?></td>
			<td><?=round($countInactivos*100/$countTotal,2)?>%</td>
		</tr>
		<tr class="alt">
			<td style="text-align:left;font-weight:bold;">Clientes Huerfanos</td>
			<td><?=$countHuerfanos?></td>
			<td><?=round($countHuerfanos*100/$countTotal,2)?>%</td>
		</tr>
		<tr>
			<td style="text-align:left;font-weight:bold;">Clientes Desuscriptos</td>
			<td><?=$countDesuscriptos?></td>
			<td><?=round($countDesuscriptos*100/$countTotal,2)?>%</td>
		</tr>
	</table>
  </div>

</body>
</html>
