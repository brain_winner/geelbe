<?  
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    require_once '../../logica_bo/secciones/clases/clssecciones.php';
    require_once '../../logica_bo/secciones/datamappers/dmsecciones.php';
    require_once '../includes.php';
    try
    {
        ValidarUsuarioLogueadoBo(4);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = 'logica_bo/secciones/actionform/secciones.php';
    $oLocal = dmSecciones::getById((isset($_GET["IdSeccion"])) ? $_GET["IdSeccion"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Secciones</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/secciones/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMLocales" name="frmABMLocales" method="post" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="idSeccion" name="idSeccion" value="<?=$oLocal->getIdSeccion()?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL);
            ?>
            <tr>
                <td>Padre</td>
                <td><select id="txtIdSeccionPadre" name="txtIdSeccionPadre">
                <?php
                	$q = mysql_query("SELECT * FROM seccionesPadre ORDER BY seccion ASC");
                	while($d = mysql_fetch_assoc($q))
                		echo '<option value="'.$d['idSeccionPadre'].'" '.($d['idSeccionPadre'] == $oLocal->getIdSeccionPadre() ? 'selected="selected"' : '').'">'.$d['seccion'].'</option>';
                ?>
                </select></td>
            </tr>
            
            <tr>
                <td>Nombre</td>
                <td><input type="text" id="txtSeccion" name="txtSeccion" maxlength="80" size=60 value="<?=$oLocal->getSeccion()?>"></td>
            </tr>
            
            <tr>
                <td>Link</td>
                <td><input type="text" id="txtHREF" name="txtHREF" maxlength="80" size=60 value="<?=$oLocal->getHref()?>"></td>
            </tr>
            
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>