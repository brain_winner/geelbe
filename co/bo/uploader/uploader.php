<?php
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    } 
    catch(Exception $e) {
        header('HTTP/1.1 500 Internal Server Error');
        include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/500.php');
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/url_resolver/UrlResolver.php");
        exit;
    }
    try {
        ValidarUsuarioLogueadoBo(28);
    }
    catch(Exception $e) {
        header('Location: /'.$confRoot[1].'/bo');
        exit;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title><?=Aplicacion::getParametros("info", "nombre");?>Subir archivos</title>
        <link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <div id="container" class="listadocontainer">
            <h1>Subir archivos</h1>    
<form action="" method="post" enctype="multipart/form-data">
   <input type="file" name="file" /> 
   <input type="submit" name="submit" value="Subir imagen" /> 
</form> 
	<? 
	$destino = $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/archivos/";
	// Leemos la extenci�n del fichero 
	$tipo = $_FILES['file']['type']; 
	if(isset($_POST[submit])!=""){ 
		// Comprovamos la extenci�n
		if( $tipo == "image/jpeg" or $tipo == "image/gif" or $tipo == "image/png"){
		// Lo movemos al servidor
		move_uploaded_file ( $_FILES [ 'file' ][ 'tmp_name' ], $destino . '/' . $_FILES [ 'file' ][ 'name' ]); 
		$previewUrl = UrlResolver::getJsBaseUrl("front/archivos/".$_FILES['file']['name']);
		echo "<br><br><label>Preview de la imagen: </label> <a href=\"".$previewUrl."\">".$previewUrl."</a><br />";
		}
		else echo "<br><br><h1>No es una extenci&oacute;n de imagen permitida<h1>" ; 
		}
	?> 
	</body>
</html>
