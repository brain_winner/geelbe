<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
    try
    {
        ValidarUsuarioLogueadoBo(8);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $objProductos = dmProductos::getByIdProducto((isset($_GET["IdProducto"])) ? $_GET["IdProducto"]:0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Productos</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/productos/js.js");?>" type="text/javascript"></script>
</head>
<body>
        <input type="hidden" id="txtIdProducto" name="txtIdProducto" value="<?=$objProductos->getIdProducto()?>">
        <table>
            <tr>
                <td>Nombre</td>
                <td><b><?=$objProductos->getNombre()?></b></td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnVolver','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnVolver_.gif");?>" alt="Volver" name="btnVolver" id="btnVolver" border="0"/></a>
                </td>
            </tr>
        </table>
</body>
</html>
