<?       
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("ordenamientos"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
    
    $postURL = Aplicacion::getIncludes("post", "productos");
    if(isset($_GET["IdProducto"])) {
    	$objProducto = dmProductos::getById($_GET["IdProducto"]);
    	$categoriasProducto = dmCategoria::getCategoriasProductos($_GET["IdProducto"]);
    }
    else {
		header('HTTP/1.1 404 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(9);
    }
    catch(ACCESOException $e) {
		header('HTTP/1.1 404 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
    
    header("Content-Description: File Transfer");
    header("Content-Type: application/force-download");
    header("Content-Disposition: attachment; filename=".$_GET["IdProducto"]."_".str_replace(" ", "_", $objProducto->getNombre())."_producto_export.json");
    
    $respuesta = array(	"IdProducto" => $objProducto->getIdProducto(), 
    					"Nombre" => utf8_encode($objProducto->getNombre()),
    					"Referencia" => utf8_encode($objProducto->getReferencia()),
    					"DBreve" => utf8_encode($objProducto->getDescripcion()),
    					"DAmpliada" => utf8_encode($objProducto->getDescripcionAmpliada()),
    					"IdClase" => $objProducto->getIdClase(),
    					"PVenta" => $objProducto->getPVenta(),
    					"PCompra" => $objProducto->getPCompra(),
    					"PVMercado" => $objProducto->getPVP(),
    					"Peso" => $objProducto->getPeso(),
    					"Alto" => $objProducto->getAlto(),
    					"Ancho" => $objProducto->getAncho(),
    					"Profundidad" => $objProducto->getProfundidad(),
    					"Ultimos" => $objProducto->getUltimos());
    
    echo json_encode($respuesta);
    exit;
?>






<!-- 
<h2>Categor&iacute;as</h2>
<?   
foreach ($listCategorias as $fila) {
	if ($fila["IdPadre"] != null) { //categoria padre
    	$selected=false;
        foreach ($categoriasProducto as $catProd) {
        	if ($catProd["IdCategoria"]==$fila["IdCategoria"]) {
            	$selected=true;
        	}
        }
        ?><option value="<?=$fila["IdCategoria"]?>" <?=$selected?"selected=\"selected\" ":""?>><?=$fila["Nombre"]?></option><?
	}
}
?>
-->