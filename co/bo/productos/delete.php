<?	       
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("ordenamientos"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
    
    $postURL = Aplicacion::getIncludes("post", "productos");
    
    try {
        ValidarUsuarioLogueadoBo(9);
    }
    catch(ACCESOException $e) {
		header('HTTP/1.1 404 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
	try {
    	@unlink($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1].'/front/productos/productos_'.$_GET['IdProducto'].'/'.$_GET['FileName']);
	}
	catch (Exception $e) {
		echo $e;
		exit;
	}
    
    // ABM_new.php?IdProducto=3197&IdCampania=430
    
	header("Location: ABM_new.php?IdProducto=".$_GET['IdProducto']."&IdCampania=".$_GET['IdCampania']);
    
?>