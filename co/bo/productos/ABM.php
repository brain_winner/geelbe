<?
    define("MAX_IMAGENES",8);
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
    try
    {
        ValidarUsuarioLogueadoBo(8);
    }
    catch(ACCESOException $e)
    {
    ?>
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }
    $postURL = Aplicacion::getIncludes("post", "productos");
    $objProducto = dmProductos::getById((isset($_GET["IdProducto"])) ? $_GET["IdProducto"]:0);
    $tblLocales = new TablaDinamica("tbl_locales");
    $tblLocales->addColumna(0,"idLocal", false);
    $tblLocales->addColumna(1, "Descripcion");
    $tblLocales->addBotones(array("nombre"=>"aquitar", "tipo"=>"a", "title"=>"quitar", "innerHTML"=>"[x]", "funcion"=>"quitarLocal"));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Productos</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/productos/js.js");?>" type="text/javascript"></script>
    <script src="<?=UrlResolver::getBaseUrl("bo/htmlarea/scripts/innovaeditor.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMProductos" name="frmABMProductos" method="post" onSubmit="ValidarForm(this);" enctype="multipart/form-data">
        <input type="hidden" id="txtIdProducto" name="txtIdProducto" value="<?=Aplicacion::Encrypter($objProducto->getIdProducto())?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["productos"]);
            ?>
            <tr>
                <td>Nombre del Producto</td>
                <td><input type="text" id="txtNombre" name="txtNombre" maxlength="80" size=60 value="<?=$objProducto->getNombre()?>"></td>
            </tr>
            <tr>
                <td>Descripci&oacute;n Breve</td>
                <td><input id="txtDescripcion" name="txtDescripcion" maxlength="255" size=100 value="<?=$objProducto->getDescripcion()?>" /></td>
            </tr>
            <tr>
                <td>Descripci&oacute;n ampliada</td>
                <td>
                    <pre id="idTemporary1" name="idTemporary1" style="display:none">
                        <?
                            if($objProducto->getDescripcionAmpliada() != "")
                            {
                                echo(htmlspecialchars_decode($objProducto->getDescripcionAmpliada()));
                            }
                        ?>
                    </pre>
                    <script>
                            var oEdit1 = new InnovaEditor("oEdit1");
                            oEdit1 = EstablecerWYSIWYG(oEdit1);
                            oEdit1.RENDER(document.getElementById("idTemporary1").innerHTML);
                        </script>
                        <input type="hidden" value='' id="txtDescripcionAmpliada"  name="txtDescripcionAmpliada"/>
                    </td>
            </tr>
            <tr>
                <td>Gu&iacute;a de talles</td>
                <td>
                    <pre id="idTemporary2" name="idTemporary2" style="display:none">
                        <?
                            if($objProducto->getIdProducto())
                            {
                                $URL = DirectorytoArray(Aplicacion::getRoot() . "/front/productos/productos_" . $objProducto->getIdProducto(), 3, array("html"));
                                if (count($URL) > 0)
                                {
                                    $oTexto = new Texto(Aplicacion::getRoot() . "/front/productos/productos_" . $objProducto->getIdProducto() ."/". "talles." . clsFile::ObtenerExtension($URL["talles"]));
                                    $oTexto->Read();
                                    $oTexto->ReemplazarString(chr(92) . chr(34), chr(39));
                                    echo($oTexto->getText());
                                }
                            }
                        ?>
                    </pre>
                    <script>
                            var oEdit2 = new InnovaEditor("oEdit2");
                            oEdit2 = EstablecerWYSIWYG(oEdit2);
                            oEdit2.RENDER(document.getElementById("idTemporary2").innerHTML);
                        </script>
                        <input type="hidden" value='' id="txtTalles"  name="txtTalles"/>
                    </td>
            </tr>
            <tr>
                <td>Locales</td>
                <td>
                    <select id="cbxLocal" name="cbxLocal" >
                        <option value=0>...seleccionar...</option>
                        <?
                            $collLocales = dmLocales::getAll("Nombre", "ASC", "");
                            foreach($collLocales as $Local)
                            {
                                ?>
                                    <option value="<?=$Local["idLocal"]?>"><?=$Local["Descripcion"]?></option>
                                <?
                            }
                        ?>
                    </select>
                        <input type="button" value="Agregar local" onclick="AgregarLocal();" />
                </td>

            <tr>
                <td colspan="2"><?=$tblLocales->show()?></td>
            </tr>

            </tr>
            <tr>
                <td>Marca</td>
                <td>
                    <select id="cbxMarca" name="cbxMarca">
                        <option value=0>...seleccionar...</option>
                        <?
                          $collMarcas = dmMarcas::getMarcas(2,"ASC","");
                          foreach ($collMarcas as $Marca)
                          {
                              if($Marca["Habilitado"] == "Si")
                              {
                                  ?><option value="<?=$Marca["Codigo"]?>"><?=$Marca["Marca"]?></option><?
                              }
                          }
                        ?>
                    </select>
                    <script>
                        document.getElementById("cbxMarca").value = value="<?=$objProducto->getIdMarca()?>";
                    </script>
                </td>
            </tr>
            <tr>
                <td>Clase</td>
                <td>
                    <select id="cbxClase" name="cbxClase" <?=($objProducto->getIdProducto() > 0)?"disabled=\"disabled\"":""?>>
                        <option value=0>...seleccionar...</option>
                        <?
                          $collClases = dmClases::getClases(2,"ASC");
                          foreach ($collClases as $Clase)
                          {
                            if($Clase["Habilitado"] == "Si")
                            {
                                ?><option value="<?=$Clase["Codigo"]?>"><?=$Clase["Clase"]?></option><?
                            }
                          }
                        ?>
                    </select>
                    <script>
                        document.getElementById("cbxClase").value = value="<?=$objProducto->getIdClase()?>";
                    </script>
                </td>
            </tr>
            <tr>
                <td>Precio Venta Geelbe</td>
                <td><input type="text" id="txtPVenta" name="txtPVenta" onblur="(esFloat(this.value)?this.value = this.value:this.value=0)" onkeypress="return solosFloatsKEYCODE(event);" maxlength="10" value="<?=$objProducto->getPVenta()?>"></td>
            </tr>
            <tr>
                <td>Precio Costo Geelbe</td>
                <td><input type="text" id="txtPCompra" name="txtPCompra" onblur="(esFloat(this.value)?this.value = this.value:this.value=0)" onkeypress="return solosFloatsKEYCODE(event);" maxlength="10" value="<?=$objProducto->getPCompra()?>"></td>
            </tr>
            <tr>
                <td>Precio Venta Mercado</td>
                <td><input type="text" id="txtPVP" name="txtPVP" onblur="(esFloat(this.value)?this.value = this.value:this.value=0)" onkeypress="return solosFloatsKEYCODE(event);" maxlength="10" value="<?=$objProducto->getPVP()?>"></td>
            </tr>
            <tr>
                <td>&Uacute;ltimos productos</td>
                <td><input type="text" id="txtUltimos" name="txtUltimos" maxlength="10" value="<?=$objProducto->getUltimos()?>"></td>
            </tr>
            <tr>
                <td>Peso</td>
                <td><input type="text" id="txtPeso" name="txtPeso" onblur="(esFloat(this.value)?this.value = this.value:this.value=0)" onkeypress="return solosFloatsKEYCODE(event);" maxlength="10" value="<?=$objProducto->getPeso()?>">
                (kilogramos)</td>
            </tr>
            <tr>
                <td>Referencia</td>
                <td><input type="text" id="txtReferencia" name="txtReferencia" maxlength="40" value="<?=$objProducto->getReferencia()?>"></td>
            </tr>
            <tr>
                <td>Alto</td>
                <td><input type="text" id="txtAlto" name="txtAlto" maxlength="10" onblur="(esFloat(this.value)?this.value = this.value:this.value=0)" onkeypress="return solosFloatsKEYCODE(event);" value="<?=$objProducto->getAlto()?>">
                (cent&iacute;metros)</td>
            </tr>
            <tr>
                <td>Ancho</td>
                <td><input type="text" id="txtAncho" name="txtAncho" maxlength="10" onblur="(esFloat(this.value)?this.value = this.value:this.value=0)" onkeypress="return solosFloatsKEYCODE(event);" value="<?=$objProducto->getAncho()?>">
                (cent&iacute;metros)</td>
            </tr>
            <tr>
                <td>Profundidad</td>
                <td><input type="text" id="txtProfundidad" name="txtProfundidad" maxlength="10" onblur="(esFloat(this.value)?this.value = this.value:this.value=0)" onkeypress="return solosFloatsKEYCODE(event);" value="<?=$objProducto->getProfundidad()?>">
                (cent&iacute;metros)</td>
            </tr>
            <tr>
                <td>Habilitado</td>
                <td>
                    <input type="checkbox" id="chkHabilitado" name="chkHabilitado" value="OK">
                    <script>
                        <?
                            if($objProducto->getHabilitado() == 1)
                            {
                                ?>document.getElementById("chkHabilitado").checked=true;<?
                            }
                        ?>
                    </script>
                </td>
            </tr>
            <tr>
                <td>Oculto</td>
                <td>
                    <input type="checkbox" id="chkOculto" name="chkOculto" value="OK">
                    <script>
                        <?
                            if($objProducto->getOculto() == 1)
                            {
                                ?>document.getElementById("chkOculto").checked=true;<?
                            }
                        ?>
                    </script>
                </td>
            </tr>
            <tr>
                <td>Imagen principal</td>
                <td><input type="file" style="width:90%" id="txtImagenPrincipal" name="txtImagenPrincipal" /></td>
            </tr>
            <?for($i=0; $i<MAX_IMAGENES;$i++)
            {
                ?>
                <tr>
                    <td>Imagen <?=$i+1?></td>
                    <td><input type="file" style="width:90%" id="txtImagen[<?=$i?>]" name="txtImagen[<?=$i?>]" /></td>
                </tr>
                <?
            }
            ?>
            <tr>
                <td colspan=2 align="center">
                <br/>
                <table width="80%" style="border:1px solid" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="100%" style="border-bottom:1px solid">Im&aacute;genes de productos</td>
                    </tr>
                <?
                if($objProducto->getIdProducto())
                {
                    if(count($objProducto->getFiles())>0)
                    {
                        $i=0;
                        foreach($objProducto->getFiles() as $file)
                        {
                            ?>
                            <tr>
                                <td><?=$file["nombre"]?></td>
                                <td><a style="cursor:pointer"onclick="window.open('<?=Aplicacion::getRootUrl();?>front/productos/productos_<?=$objProducto->getIdProducto();?>/<?=$file["nombre"]?>')">Ver Im&aacute;gen</a></td>
                            <?
                            $i++;
                        }
                    }
                    else
                    {
                        ?>
                              <td colspan=2>No hay im&aacute;genes subidas</td><?
                    }
                }
                ?>
                </table>
                <br/>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
        <script>
            <?
                $Locales = dmProductos::getLocales($objProducto->getIdProducto());
                foreach($Locales as $Local)
                {
                    ?>
                        tbl_locales_AgregarFila(<?=$Local["IdLocal"]?>,"<?=$Local["Descripcion"]?>");
                    <?
                }
            ?>
        </script>
    </form>
</body>
</html>
