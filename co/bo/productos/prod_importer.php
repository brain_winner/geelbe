<?
	define("MAX_IMAGENES",8); 	       
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("ordenamientos"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
    
    $postURL = Aplicacion::getIncludes("post", "productos");
    if(isset($_GET["IdCampania"])) {
    	$objCampania = dmCampania::getByIdCampania($_GET["IdCampania"]);
    	$listCategorias = dmCategoria::getCategoriasNew($_GET["IdCampania"]);
    }
    else {
		header('HTTP/1.1 404 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
    try {
        ValidarUsuarioLogueadoBo(9);
    }
    catch(ACCESOException $e) {
		header('HTTP/1.1 404 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>BackOffice Geelbe - Productos - Importer </title>
	<? Includes::Scripts(); ?>
	<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	
	<link type="text/css" href="<?=UrlResolver::getCssBaseUrl("bo/css/ui.multiselect.css")?>" rel="stylesheet" />	
	<link type="text/css" href="<?=UrlResolver::getCssBaseUrl("css/ui-lightness/jquery-ui-1.8.9.custom.css")?>" rel="stylesheet" />	
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.4.min.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery.MultiFile.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery.MetaData.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery.MultiFile.pack.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery-ui-1.8.9.custom.min.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/ui.multiselect.js")?>"></script>



    <script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("registro/js/gen_validatorv31.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/nicEdit-latest.js")?>"></script>
		
</head>
<body>
		<form style="margin:0;padding:0;" id="frmABMProductos" name="frmABMProductos" method="post"  action="/<?=$confRoot[1]?>/logica_bo/productos/actionform/prod_importer_new.php" enctype="multipart/form-data">
 		<input type="hidden" id="txtIdCampania" name="txtIdCampania" value="<?=$_GET["IdCampania"]?>">
        
        
        
        
		<div id="container">
		
			<h1>Nuevo Producto [campa�a: <?=$objCampania->getNombre();?>]</h1>
			<div class="stats">

			  	<h2>Importar Json</h2>
				<p>Aqu� se ingresar� el archivo para importar el producto.</p>
			  	<input type="file" id="txtJson" name="txtJson" size=50/>
				
				<?php 
				foreach ($listCategorias as $fila) {
					if ($fila["IdPadre"]==null) {
						?>
						<input type="hidden" id="idCategoria" name="idCategoria" value="<?=$fila["IdCategoria"]?>" />
						<?
					}
				}
				?>
				
                <table style="border:0;">
                	<tbody><tr style="border:0;">
                		<td style="border:0;"></td>
                		<td style="border:0;text-align:right;"><input type="submit" value="Importar" id="Importar" name="Importar"></td>
                	</tr>
                </tbody></table>
            </form>
		
</body>
</html>
