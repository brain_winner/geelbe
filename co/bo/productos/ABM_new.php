<?
	define("MAX_IMAGENES",8); 	       
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("ordenamientos"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
    
    $listCategoriasGlobales = dmCategoriaGlobal::getCategorias();
    
    $postURL = Aplicacion::getIncludes("post", "productos");
    if(isset($_GET["IdCampania"])) {
    	$objCampania = dmCampania::getByIdCampania($_GET["IdCampania"]);
    	$listCategorias = dmCategoria::getCategoriasNew($_GET["IdCampania"]);
    }
    else {
		header('HTTP/1.1 404 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
    	
    if(isset($_GET["IdProducto"])) {
    	$objProducto = dmProductos::getById($_GET["IdProducto"]);
    	$categoriasProducto = dmCategoria::getCategoriasProductos($_GET["IdProducto"]);
    	$categoriasGlobalesProducto = dmCategoriaGlobal::getCategoriasProductos($_GET["IdProducto"]);
    }
    else {
    	$objProducto = new MySQL_Productos();
    }
    try {
        ValidarUsuarioLogueadoBo(9);
    }
    catch(ACCESOException $e) {
		header('HTTP/1.1 404 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>BackOffice Geelbe - Productos - Alta de Productos </title>
	<? Includes::Scripts(); ?>
	<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
	<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
	
	<link type="text/css" href="<?=UrlResolver::getCssBaseUrl("bo/css/ui.multiselect.css")?>" rel="stylesheet" />	
	<link type="text/css" href="<?=UrlResolver::getCssBaseUrl("css/ui-lightness/jquery-ui-1.8.9.custom.css")?>" rel="stylesheet" />	
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.4.min.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery.MultiFile.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery.MetaData.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery.MultiFile.pack.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery-ui-1.8.9.custom.min.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/ui.multiselect.js")?>"></script>



    <script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("registro/js/gen_validatorv31.js")?>"></script>
	<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/nicEdit-latest.js")?>"></script>
		
</head>
<body>
		<form style="margin:0;padding:0;" id="frmABMProductos" name="frmABMProductos" method="post"  action="/<?=$confRoot[1]?>/logica_bo/productos/actionform/productos_new.php?page=<?=$_GET['page']?>" enctype="multipart/form-data">
 		<input type="hidden" id="txtIdProducto" name="txtIdProducto" value="<?=($objProducto->getIdProducto()>0)?$objProducto->getIdProducto():0 ?>">
        <input type="hidden" id="txtIdCampania" name="txtIdCampania" value="<?=$_GET["IdCampania"]?>">
        
        
        
        
		<div id="container">
		
			<h1>Nuevo Producto [campa�a: <?=$objCampania->getNombre();?>]</h1>
			<div class="stats">

			  	<h2>Informaci�n b�sica</h2>
				<p>Aqu� se ingresar� el nombre y la descripcion del producto. Tamb�en se incluir�, de ser necesario, la gu�a de talles.</p>
			  	<table>
			    <tbody>
					<tr>
						<th width="35%">Nombre del producto</th>
						<td width="65%"><input type="text" id="txtNombre" name="txtNombre" value="<?=$objProducto->getNombre()?>"></td>
					</tr>
					<tr class="alt">
						<th>Referencia</th>
						<td><input type="text" id="txtReferencia" name="txtReferencia" value="<?=$objProducto->getReferencia()?>"> [referencia interna]</td>
					</tr>
					<tr>
						<th>Descripci�n breve</th>
						<td><input id="txtDescripcion" name="txtDescripcion" maxlength="255" size="50" value="<?=$objProducto->getDescripcion()?>" /></td>
					</tr>
					<tr class="alt">
						<th>Descripci�n ampliada</th>
						<td><textarea name="txtDescripcionAmpliada" id="txtDescripcionAmpliada" cols="75" rows="10"><?=htmlspecialchars_decode($objProducto->getDescripcionAmpliada())?></textarea></td>
					</tr>
					<tr>
						<th>Gu�a de talles</th>
						<td><textarea name="txtTalles" id="txtTalles" cols="75" rows="10">
						 <?
                            if($objProducto->getIdProducto())
                            {
                                $URL = DirectorytoArray(Aplicacion::getRoot() . "/front/productos/productos_" . $objProducto->getIdProducto(), 3, array("html"));
                                if (count($URL) > 0)
                                {
                                    $oTexto = new Texto(Aplicacion::getRoot() . "/front/productos/productos_" . $objProducto->getIdProducto() ."/". "talles." . clsFile::ObtenerExtension($URL["talles"]));
                                    $oTexto->Read();
                                    $oTexto->ReemplazarString(chr(92) . chr(34), chr(39));
                                    echo($oTexto->getText());
                                }
                            }
                        ?>
						</textarea></td>
					</tr>
					<tr>
						<th>Ocultar producto</th>
						<td><input type="checkbox" id="chkOculto" name="chkOculto" value="OK" <?=($objProducto->getOculto() ? 'checked' : '')?>></td>
					</tr>
				</tbody>
				</table>
				<input type="hidden" id="cbxLocal" name="cbxLocal" value="0" />
				<input type="hidden" id="cbxMarca" name="cbxMarca" value="0" />

			  	<h2>Atributos</h2>
				<p>Aqu� se ingresar�n los atributos del producto.</p>
			  	<table>
			    <tbody>
					<tr>
						<th width="35%">Clase</th>
						<td width="65%">
						
						
                    <select id="cbxClase" name="cbxClase">
                        <option value=0>[seleccionar]</option>
                        <?
                          $collClases = dmClases::getClases(2,"ASC");
                          foreach ($collClases as $Clase) {
                            if($Clase["Habilitado"] == "Si") {
                                ?><option value="<?=$Clase["Codigo"]?>" <?=$objProducto->getIdClase()==$Clase["Codigo"]?"selected=\"selected\"":"" ?>><?=$Clase["Clase"]?></option><?
                            }
                          }
                        ?>
                    </select>
						</td>
					</tr>
					<tr class="alt">
						<th>Precio de venta de Geelbe</th>
						<td><input type="text" id="txtPVenta" name="txtPVenta" value="<?=$objProducto->getPVenta()?>"></td>
					</tr>
					<tr>
						<th>Costo de Geelbe</th>
						<td><input type="text" id="txtPCompra" name="txtPCompra" value="<?=$objProducto->getPCompra()?>"></td>
					</tr>
					<tr class="alt">
						<th>Precio de venta en mercado</th>
						<td><input type="text" id="txtPVP" name="txtPVP" value="<?=$objProducto->getPVP()?>"></td>
					</tr>	
					<tr>
						<th>Peso</th>
						<td><input type="text" id="txtPeso" name="txtPeso" value="<?=$objProducto->getPeso()?>"> [kilogramos]</td>
					</tr>
					<tr class="alt">
						<th>Alto</th>
						<td><input type="text" id="txtAlto" name="txtAlto" value="<?=$objProducto->getAlto()?>"> [centimetros]</td>
					</tr>
					<tr>
						<th>Ancho</th>
						<td><input type="text" id="txtAncho" name="txtAncho" value="<?=$objProducto->getAncho()?>"> [centimetros]</td>
					</tr>	
					<tr class="alt">
						<th>Profundidad</th>
						<td><input type="text" id="txtProfundidad" name="txtProfundidad" value="<?=$objProducto->getProfundidad()?>"> [centimetros]</td>
					</tr>
					<tr>
						<th>�ltimos productos</th>
						<td><input type="text" id="txtUltimos" name="txtUltimos" value="<?=$objProducto->getUltimos()?>"> [desde que stock mostrar este mensaje]</td>
					</tr>	
					
					
				</tbody>
				</table>
				

			  	<h2>Categor&iacute;as</h2>
				<p>Aqu� se seleccionaran las categor&iacute;as a las que pertenece el producto.</p>
			  	<table>
			    <tbody>
					<tr>
						<th width="35%">Categor&iacute;as seleccionadas</th>
						<td width="65%">
                   <?php $msize = count($listCategorias)*1.5;?>
				   <select multiple size="<?=($msize<5)?5:$msize?>" style="width:50%;" id="cbxCategoria" name="cbxCategoria[]">
                        <?
                        
                        
                        
                          foreach ($listCategorias as $fila) {
                            if ($fila["IdPadre"] != null) { //categoria padre
                            	$selected=false;
                          		foreach ($categoriasProducto as $catProd) {
                          			if ($catProd["IdCategoria"]==$fila["IdCategoria"]) {
                          				$selected=true;
                          			}
                          		}
                            	
                                ?><option value="<?=$fila["IdCategoria"]?>" <?=$selected?"selected=\"selected\" ":""?>><?=$fila["Nombre"]?></option><?
                            }
                          }
                        ?>
                    </select>
                    
                    	</td>
                    </tr>
					
					
				</tbody>
				</table>
				
				
			  	<h2>Categor&iacute;as globales</h2>
				<p>Aqu� se seleccionaran las categor&iacute;as globales a las que pertenece el producto.</p>
			  	<table>
			    <tbody>
					<tr>
						<th width="35%">Categor&iacute;as seleccionadas</th>
						<td width="65%">
				   <select multiple size="15" style="width:50%;" id="cbxCategoriaGlobal" name="cbxCategoriaGlobal[]">
                        <?
                        
                        
                        
                          foreach ($listCategoriasGlobales as $fila) {
                            	$selected=false;
                          		foreach ($categoriasGlobalesProducto as $catProd) {
                          			if ($catProd["IdCategoriaGlobal"]==$fila["IdCategoriaGlobal"]) {
                          				$selected=true;
                          			}
                          		}
                            	
                                ?><option value="<?=$fila["IdCategoriaGlobal"]?>" <?=$selected?"selected=\"selected\" ":""?>><?=$fila["Nombre"]?></option><?
                          }
                        ?>
                    </select>
                    
                    	</td>
                    </tr>
					
					
				</tbody>
				</table>
				
				
				<h2>Im�genes</h2>
				<p>Aqu� se cargar�n las diferentes im�genes de un producto.</p>
			  	<table>
			    <tbody>
					<tr>
						<th width="35%">Im�gen principal</th>
						<td width="65%"><input type="file" id="txtImagenPrincipal" name="txtImagenPrincipal" size=50/></td>
					</tr>
					<tr class="alt">
						<th width="35%">Otras im�genes</th>
						<td width="65%"><input type="file" class="multi" accept="gif|jpg" maxlength="8" id="txtImagenAdicional" name="txtImagenAdicional[]" size=50/></td>
					</tr>
					
					<tr>
						<th width="35%">Im�genes cargadas:</th>
						<td width="65%">
				<?
                if($objProducto->getIdProducto()) {
                    if(count($objProducto->getFiles())>0) {
                        $i=0;
                        foreach($objProducto->getFiles() as $file) {
                            echo $file["nombre"]; ?>
                            <a href="javascript:;" onclick="window.open('<?=$file["link"]?><?=$file["nombre"]?>')">[ver im&aacute;gen]</a> 
                            <a href="delete.php?IdProducto=<?=$objProducto->getIdProducto();?>&IdCampania=<?=$_GET["IdCampania"];?>&FileName=<?=$file["nombre"];?>">[eliminar im&aacute;gen]</a> 
                            <br/>
                            <?
                            $i++;
                        }
                    }
                    else {
                        ?>No hay im&aacute;genes subidas<?
                    }
                }
                ?>
						</td>
					</tr>
				</tbody>
				</table>
				
				
                <table style="border:0;">
                	<tbody><tr style="border:0;">
                		<td style="border:0;"></td>
                		<td style="border:0;text-align:right;"><input type="submit" value="Guardar y Cargar Stock" id="botonGuardarStock" name="botonGuardarStock"></td>
                	</tr>
                	<tr style="border:0;">
                		<td style="border:0;"></td>
                		<td style="border:0;text-align:right;"><input type="submit" value="Guardar y Salir" id="botonGuardarSalir" name="botonGuardarSalir"></td>
                	</tr>
                </tbody></table>
            </form>

<!-- Geelbe Validation Form -->
<script type="text/javascript">

var formValidator  = new Validator("frmABMProductos");
formValidator.EnableOnPageErrorDisplaySingleBox();
formValidator.EnableMsgsTogether();
formValidator.EnableFocusOnError(false);

formValidator.addValidation("txtNombre","req","Por favor, ingrese un Nombre de Campa&ntilde;a de Venta.");
formValidator.addValidation("txtNombre","maxlen=255", "El Nombre supera el maximo permitido de 255 caracteres.");

formValidator.addValidation("txtDescripcion","req","Por favor, ingrese una Descripci&oacute;n.");
formValidator.addValidation("txtDescripcion","maxlen=255","Su Descripci&oacute;n supera el maximo permitido de 255 caracteres.");


formValidator.addValidation("FechaInicioDia","req","Por favor, ingrese una Fecha de Inicio.");
formValidator.addValidation("FechaFinDia","req","Por favor, ingrese una Fecha de Fin.");
formValidator.addValidation("FechaVisibleDia","req","Por favor, ingrese una Fecha de Visualizaci&oacute;n.");

formValidator.addValidation("FechaInicioHora","req","Por favor, ingrese una Hora de Inicio.");
formValidator.addValidation("FechaInicioHora","numeric","Por favor, ingrese una Hora de Inicio.");
formValidator.addValidation("FechaInicioMin","req","Por favor, ingrese los Minutos de Inicio.");
formValidator.addValidation("FechaInicioMin","numeric","Por favor, ingrese los Minutos de Inicio.");

formValidator.addValidation("FechaFinHora","req","Por favor, ingrese una Hora de Fin.");
formValidator.addValidation("FechaFinHora","numeric","Por favor, ingrese una Hora de Fin.");
formValidator.addValidation("FechaFinMin","req","Por favor, ingrese una Minutos de Fin.");
formValidator.addValidation("FechaFinMin","numeric","Por favor, ingrese una Minutos de Fin.");

formValidator.addValidation("FechaVisibleHora","req","Por favor, ingrese una Hora de Visualizaci&oacute;n.");
formValidator.addValidation("FechaVisibleHora","numeric","Por favor, ingrese una Hora de Visualizaci&oacute;n.");
formValidator.addValidation("FechaVisibleMin","req","Por favor, ingrese una Minutos de Visualizaci&oacute;n.");
formValidator.addValidation("FechaVisibleMin","numeric","Por favor, ingrese una Minutos de Visualizaci&oacute;n.");

formValidator.addValidation("txtMaxProd","req","Por favor, ingrese M&aacute;ximos Productos.");
formValidator.addValidation("txtMaxProd","numeric","Por favor, ingrese  M&aacute;ximos Productos.");

formValidator.addValidation("TiempoEntregaIn","req","Por favor, ingrese Tiempo de Entrega Inicial.");
formValidator.addValidation("TiempoEntregaFn","req","Por favor, ingrese Tiempo de Entrega Final.");*/

</script>
<!-- Geelbe Validation Form -->

<script type="text/javascript">
//<![CDATA[
	new nicEditor({buttonList : ['fontFamily','fontSize','forecolor','bold','italic','underline','strikeThrough', 'left', 'center', 'right', 'justify', 'image', 'link', 'unlink', 'xhtml']}).panelInstance('txtDescripcionAmpliada');
	new nicEditor({buttonList : ['fontFamily','fontSize','forecolor','bold','italic','underline','strikeThrough', 'left', 'center', 'right', 'justify', 'image', 'link', 'unlink', 'xhtml']}).panelInstance('txtTalles');
//]]>
</script>

<script type="text/javascript">
$(function(){
  $("#cbxCategoria").multiselect({sortable: false, searchable: false});
  $("#cbxCategoriaGlobal").multiselect({sortable: false, searchable: false});
});
</script>
		
</body>
</html>
