function EstablecerWYSIWYG(Obj, W, H)
{
    if (!W)
        W = 720;
    if (!H)
        H = 350;
        
    Obj.width=W;
    Obj.height=H;
    Obj.btnSave=false;
    Obj.btnPrint=false;
    Obj.btnSpellCheck=true;
    Obj.btnCssBuilder=true;
    Obj.btnStyles=true; //Show "Styles/Style Selection" button
    Obj.btnPasteText=true;
    Obj.btnStrikethrough=true;
    Obj.btnSuperscript=true;
    Obj.btnSubscript=true;
    Obj.btnLTR=true;
    Obj.btnRTL=true;
    Obj.btnFlash=true;
    Obj.btnMedia=true;
    Obj.btnClearAll=true;
    Obj.btnInternalLink=true;
    Obj.btnCustomTag=false; // TODO: Sacar datos de las tablas de usuario
    Obj.cmdAssetManager="modalDialogShow('/app/abm/htmlarea/assetmanager/assetmanager.php',640,600)";
    Obj.mode="HTMLBody";
    return Obj;    
}
function ErrorMSJ(code, def)
{
    if(arrErrores["PRODUCTOS"][code] != undefined)
        document.getElementById("msjError").innerHTML = arrErrores["PRODUCTOS"][code];
    else
        document.getElementById("msjError").innerHTML = code;
}
function ValidarForm(frm)
{
    try
    {
        var arrError = new Array();
        if(frm.txtNombre.value.length == 0)
        {
            arrError.push(arrErrores["PRODUCTOS"]["NOMBRE"]);
        }
        if(frm.cbxMarca.value == 0)
        {
            arrError.push(arrErrores["PRODUCTOS"]["MARCA"]);
        }
        if(frm.cbxClase.value == 0)
        {
            arrError.push(arrErrores["PRODUCTOS"]["CLASE"]);
        }
        if (arrError.length>0)
        {
            var error = ""
            for(i=0; i<arrError.length; i++)
            {
                error +="*" + arrError[i] + "\n";
            }
            alert(error);
            return false;
        }
        document.getElementById("cbxClase").disabled=false;
        document.forms["frmABMProductos"].txtDescripcionAmpliada.value = oEdit1.getHTMLBody();
        document.forms["frmABMProductos"].txtTalles.value = oEdit2.getHTMLBody();
        frm.submit();
        document.getElementById("cbxClase").disabled=true;
    }
    catch(e)
    {
        throw e;
    }
}
function ValidarFormStock(frm)
{
    try
    {
        var arrError = new Array();
        if(document.getElementById("tbl").rows.length <= 1)
        {
            arrError.push(arrErrores["PRODUCTOS"]["INTERNO"]);
        }
        if (arrError.length>0)
        {
            var error = ""
            for(i=0; i<arrError.length; i++)
            {
                error +="*" + arrError[i] + "\n";
            }
            alert(error);
            return false;
        }
        frm.submit();
    }
    catch(e)
    {
        throw e;
    }
}
function AgregarPPNuevo()
{
    try
    {
        var IdProveedor = document.getElementById("cbxProveedores").value;
        if(IdProveedor == 0)
        {
            alert(arrErrores["PRODUCTOS"]["SELECCIONE_PROVEEDOR"]);
            return false;
        }
        for(i=0; i<arrIdAtributos.length; i++)
        {
            if(document.getElementById("txtAtributo["+arrIdAtributos[i]["Id"]+"]").value.length == 0)
            {
                alert(arrIdAtributos[i]["Nombre"] + arrErrores["PRODUCTOS"]["ATRIBUTOS_VALOR_VACIO"])
                return false;
            }
        }
        var Proveedor = document.getElementById("cbxProveedores").options[document.getElementById("cbxProveedores").selectedIndex].innerHTML;
        var Stock = document.getElementById("txtStock").value;
        //var Regalo = document.getElementById("txtRegalo").value;
        var Regalo = 0;
        var Incremento =document.getElementById("cbxTipo").value + parseFloat(document.getElementById("txtIncremento").value).toFixed(2);
        var Comentario = document.getElementById("txtComentario").value;
        var Habilitado = (document.getElementById("chkHabilitado").checked == true)?"Si":"No";
        var IdAtributoClase = "";
        var AtributoClase = "<ul>";
        var AtributoClaseValores = "";
        var i=0;
        for(i=0; i<arrIdAtributos.length; i++)
        {
            IdAtributoClase +=arrIdAtributos[i]["Id"]+"|";
            AtributoClase += "<li>"+arrIdAtributos[i]["Nombre"]+" = "+ document.getElementById("txtAtributo["+arrIdAtributos[i]["Id"]+"]").value +"</li>";
            AtributoClaseValores += document.getElementById("txtAtributo["+arrIdAtributos[i]["Id"]+"]").value+"|";
        }
        AtributoClase += "</ul>";
        if(document.getElementById("txtIdCodigoProdInterno").value ==0)
        {
            tbl_AgregarFila(0, IdProveedor, Proveedor, IdAtributoClase.substring(0,IdAtributoClase.length-1), AtributoClase, AtributoClaseValores.substring(0,AtributoClaseValores.length-1), Stock, Regalo, Incremento, Comentario, Habilitado);
        }
        else
        {
            tbl_ModificarFila(document.getElementById("tbl_Posicion").value, document.getElementById("txtIdCodigoProdInterno").value, IdProveedor, Proveedor, IdAtributoClase.substring(0,IdAtributoClase.length-1), AtributoClase, AtributoClaseValores.substring(0,AtributoClaseValores.length-1), Stock, Regalo, Incremento, Comentario, Habilitado)
        }
        //NuevoPP();
        document.getElementById("cbxProveedores").focus();
    }
    catch(e)
    {
        throw e;
    }
}
function quitarAtributo()
{
    try
    {
        var Tabla = document.getElementById("tbl");
        if(confirm(arrErrores["PRODUCTOS"]["ELIMINAR_ATRIBUTOS"]))
        {
            Tabla.deleteRow(tbl_getFIlaSeleccionada());
            alert(arrErrores["PRODUCTOS"]["ELIMINAR_ATRIBUTOS_CONFIRMADO"]);
        }
        
    }
    catch(e)
    {
        throw e;
    }               
}
function AgregarLocal()
{
    var Local = document.getElementById("cbxLocal").value;
    if (Local == 0)
    {
         alert("seleccione");
         return false;
    }    
    if (tbl_locales_Buscar(Local))
    {
        alert("existe");
        return false;
    }
    var Descripcion = document.getElementById("cbxLocal").options[document.getElementById("cbxLocal").selectedIndex].innerHTML;
    tbl_locales_AgregarFila(Local, Descripcion);
}
function quitarLocal()
{
    try
    {
        var Tabla = document.getElementById("tbl_locales");
        if(confirm("Quitar Local?"))
        {
            Tabla.deleteRow(tbl_locales_getFIlaSeleccionada());
        }
    }
    catch(e)
    {
        throw e;
    }               
}
function NuevoPP()
{
    try
    {
        document.getElementById("txtIdCodigoProdInterno").value = 0;
        document.getElementById("cbxProveedores").selectedIndex=0;
        document.getElementById("txtStock").value = 0;
        //document.getElementById("txtRegalo").value = 0;
        document.getElementById("cbxTipo").selectedIndex=0;
        document.getElementById("txtIncremento").value = "0.00";
        document.getElementById("txtComentario").value = "";
        for(i=0; i<arrIdAtributos.length; i++)
        {
            document.getElementById("txtAtributo["+arrIdAtributos[i]["Id"]+"]").value = "";
        }
    }
    catch(e)
    {
        throw e;
    }
}
function modificarAtributo()
{
    try
    {
        var Valores = tbl_getFila(tbl_getFIlaSeleccionada());
        document.getElementById("txtIdCodigoProdInterno").value = Valores[0];
        document.getElementById("cbxProveedores").value = Valores[1];
        document.getElementById("txtStock").value = Valores[6];
        //document.getElementById("txtRegalo").value = Valores[7];
        document.getElementById("cbxTipo").value = Valores[8].substring(0,1);
        document.getElementById("txtIncremento").value = Valores[8].substring(1,Valores[8].length);
        document.getElementById("txtComentario").value = Valores[9];
        document.getElementById("chkHabilitado").checked = (Valores[10] == "Si")?true:false;
        document.getElementById("tbl_Posicion").value = tbl_getFIlaSeleccionada();
        var ValoresAtributos = new Array();
        ValoresAtributos = Valores[5].split("|");
        for(i=0; i<ValoresAtributos.length; i++)
        {
            document.getElementById("txtAtributo["+arrIdAtributos[i]["Id"]+"]").value = ValoresAtributos[i];
        }
    }
    catch(e)
    {
        throw e;
    }
}
function irVolver()
{
    try
    {
        window.location.href=DIRECTORIO_URL_BO+"productos/index.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irNuevo()
{
    try
    {
        window.location.href="ABM.php";
    }
    catch(e)
    {
        throw e;
    }
}
function irEditar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["PRODUCTOS"]["SELECCIONE"]);
        else
            window.location.href="ABM.php?IdProducto="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irStock()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["PRODUCTOS"]["SELECCIONE"]);
        else
            window.location.href="ABM_stock.php?IdProducto="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVer()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["PRODUCTOS"]["SELECCIONE"]);
        else
            window.location.href="Ver.php?IdProducto="+dt_getCeldaSeleccionada(0);
    }
    catch(e)
    {
        throw e;
    }
}
function irVisualizar()
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["PRODUCTOS"]["SELECCIONE"]);
        else
        {  
           window.open("../../front/catalogo/detalle_prev.php?IdCampania="+dt_getCeldaSeleccionada(8)+"&IdCategoria="+dt_getCeldaSeleccionada(7)+"&IdProducto="+dt_getCeldaSeleccionada(0),"Previsualizar","") ;
        }  
          
    }
    catch(e)
    {
        throw e;
        
    }
}
function irEliminar(id)
{
    try
    {
        if(!dt_getCeldaSeleccionada(0))
            alert(arrErrores["PRODUCTOS"]["SELECCIONE"]);
        else
        {
            if(confirm(arrErrores["PRODUCTOS"]["ELIMINAR"]))
                window.location.href="../../logica_bo/productos/actionform/eliminar_productos.php?IdProducto="+dt_getCeldaSeleccionada(0);
        }
    }
    catch(e)
    {
        throw e;
    }
}