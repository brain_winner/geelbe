<?  
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
    try
    {
        ValidarUsuarioLogueadoBo(8);
    }
    catch(ACCESOException $e)
    {
    ?>  
        <script>window.open('<?=Aplicacion::getRootUrl()?>bo/','_parent');</script>
    <?
        exit;
    }     
    $postURL = Aplicacion::getIncludes("post", "productos");
    $objProducto = dmProductos::getById((isset($_GET["IdProducto"])) ? $_GET["IdProducto"]:0);
    $objDT = new TablaDinamica();
    $objDT->addColumna(0,"IdCodigoProdInterno",false);
    $objDT->addColumna(1,"IdProveedor", false);
    $objDT->addColumna(2,"Proveedor");
    $objDT->addColumna(3,"IdAtributos", false);
    $objDT->addColumna(4,"Atributos");
    $objDT->addColumna(5,"ValoresAtributos", false);
    $objDT->addColumna(6,"Stock");
    $objDT->addColumna(7,"Regalo", false);
    $objDT->addColumna(8,"Incremento");
    $objDT->addColumna(9,"Comentario");
    $objDT->addColumna(10,"Habilitado");
    $Atributos = dmProductos::AtributosByIdProducto($objProducto->getIdProducto());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?=Aplicacion::getParametros("info", "nombre");?> ABM Productos</title>
    <?
        Includes::Scripts();
    ?>
    <script src="<?=UrlResolver::getJsBaseUrl("bo/productos/js.js");?>" type="text/javascript"></script>
</head>
<body>
    <form id="frmABMProductos" name="frmABMProductos" method="POST" onSubmit="ValidarFormStock(this);">
        <input type="hidden" id="txtIdProducto" name="txtIdProducto" value="<?=Aplicacion::Encrypter($objProducto->getIdProducto())?>">
        <table>
            <tr>
                <td colspan=2><span id="msjError" name="msjError"></span>
                </td>
            </tr>
            <?
                require_once("../../../".Aplicacion::getDirLocal().$postURL["productosStock"]);
            ?>
            <tr>
                <td>Nombre</td>
                <td><span><?=$objProducto->getNombre()?></span></td>
            </tr>
            <tr>
                <td>
                    <input id="txtIdCodigoProdInterno" name="txtIdCodigoProdInterno" type="hidden" value=0 />
                    <input id="tbl_Posicion" name="tbl_Posicion" type="hidden" value=0 />
                    Proveedor
                </td>
                <td>
                    <select id="cbxProveedores" name="cbxProveedores">
                        <option value=0>...Seleccione...</option>
                        <?
                            $dtProveedores = dmProveedor::getProveedors(1,"ASC");
                            foreach($dtProveedores as $proveedor)
                            {
                                ?><option value=<?=$proveedor["IdProveedor"]?>><?=$proveedor["Proveedor"]?></option><?
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Stock
                </td>
                <td>
                    <input type="text" id="txtStock" name="txtStock" onblur="(esFloat(this.value)?this.value = this.value:this.value=0)" onkeypress="return solosFloatsKEYCODE(event);" maxlength="10" size=5 value=0 />
                </td>
            </tr>
            <!--tr>
                <td>
                    Regalo
                </td>
                <td>
                    <input type="text" id="txtRegalo" name="txtRegalo" onblur="(esFloat(this.value)?this.value = this.value:this.value=0)" onkeypress="return solosFloatsKEYCODE(event);" maxlength="10" size=5 value=0 />
                </td>
            </tr-->
            <tr>
                <td>
                    Incremento
                </td>
                <td>
                    <select id="cbxTipo" name="cbxTipo">
                        <option value="+">+</option>
                        <option value="*">*</option>
                    </select>
                    <input type="text" id="txtIncremento" name="txtIncremento"  onblur="(esFloat(this.value)?this.value = this.value:this.value=0)" onkeypress="return solosFloatsKEYCODE(event);" maxlength="10" size=5 value=0  maxlength="10" size=5/>
                </td>
            </tr>
            <tr>
                <td>Habilitado</td>
                <td>
                    <input type="checkbox" id="chkHabilitado" name="chkHabilitado" value="OK">                   
                </td>
            </tr>
            <tr>
                <td>
                    Comentario
                </td>
                 <td>
                    <input type="text" id="txtComentario" name="txtComentario" maxlength="255" size=50/>
                </td>
            </tr>
            <script>
                var arrIdAtributos = new Array();
            </script>
            <?
                $IdAtributos ="";
                foreach($Atributos as $i =>$Atributo)
                {
                    $IdAtributos.=$Atributo["IdAtributoClase"]."|";
                    ?>
                    <script>
                        arrIdAtributos[<?=$i?>] = new Array();
                        arrIdAtributos[<?=$i?>]["Id"] = <?=$Atributo["IdAtributoClase"]?>;
                        arrIdAtributos[<?=$i?>]["Nombre"] = "<?=$Atributo["Nombre"]?>";
                    </script>
                    <tr>
                        <td>
                        <?=$Atributo["Nombre"]?></td>
                        <td><input type="text" maxlength="20" size=20 id="txtAtributo[<?=$Atributo["IdAtributoClase"]?>]" name="txtAtributo[<?=$Atributo["IdAtributoClase"]?>]" value=""/></td>
                    </tr>
                    <?
                }
            ?>
            <tr>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <input type="button" id="btnNuevo" name="btnNuevo" value="Nuevo" onclick="NuevoPP()"/>
                    <input type="button" id="btnAgregar" name="btnAgregar" value="Guardar" onclick="AgregarPPNuevo()"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?
                    $objDT->addBotones(array("nombre"=>"amodificar", "tipo"=>"a", "title"=>"modificar", "innerHTML"=>"[m]", "funcion"=>"modificarAtributo"));
                    $objDT->addBotones(array("nombre"=>"aquitar", "tipo"=>"a", "title"=>"quitar", "innerHTML"=>"[x]", "funcion"=>"quitarAtributo"));
                    $objDT->show();
                    ?>
                </td>
            </tr>
            <script>
                <?
                foreach ($objProducto->getProductosProveedores() as $PP)
                {
                    ?>var Proveedor = "";<?
                    foreach($dtProveedores as $prov)
                    {
                        if($prov["IdProveedor"] == $PP->getIdProveedor())
                        {
                            ?>Proveedor = "<?=$prov["Proveedor"]?>";<?
                        }
                    }
                    $Atributos = dmProductos::getAtributosByIdCodigoProdInterno($PP->getIdCodigoProdInterno());
                    $Valores=array();
                    $Valores["Nombre"] ="<ul>";
                    foreach($Atributos as $Atributo)
                    {
                        $Valores["Id"] .=$Atributo["idAtributoClase"]."|";
                        $Valores["Nombre"] .="<li>".$Atributo["Nombre"]." = ".$Atributo["Valor"]."</li>";
                        $Valores["Valores"] .=$Atributo["Valor"]."|";
                    }
                    $Valores["Nombre"] .="</ul>";
                    $Valores["Id"] = substr($Valores["Id"], 0, strlen($Valores["Id"] )-1);
                    ?>
                        tbl_AgregarFila(<?=$PP->getIdCodigoProdInterno()?>, <?=$PP->getIdProveedor()?>, Proveedor, "<?=$Valores["Id"]?>", "<?=$Valores["Nombre"]?>", "<?=substr($Valores["Valores"],0,strlen($Valores["Valores"])-1)?>", "<?=$PP->getStock()?>", "<?=$PP->getRegalo()?>", "<?=$PP->getIncremento()?>", "<?=$PP->getComentarios()?>", "<?=($PP->getHabilitado() == 1)?"Si":"No"?>");
                    <?
                }
                ?>
            </script>
            <tr>
                <td colspan=2 align=center>
                    <a href="javascript:document.forms[0].onsubmit();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnAceptar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnAceptar.gif");?>" alt="Aceptar" name="btnAceptar" id="btnAceptar" border="0"/></a>
                </td>
            </tr>
            <tr>
                <td colspan=2 align=center>
                <a  href="javascript:irVolver();" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('btnCancelar','','<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar_on.gif");?>',1)"><img src="<?=UrlResolver::getImgBaseUrl("bo/imagenes/btnCancelar.gif");?>" alt="Cancelar" name="btnCancelar" id="btnCancelar" border="0"/></a>
                </td>
            </tr>
        </table>
    </form>
    <iframe style="display:none" id="iProductos" name="iProductos"></iframe>
</body>
</html>
