<?  
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
    try {
        ValidarUsuarioLogueadoBo(8);
    }
    catch(ACCESOException $e) {
		header('HTTP/1.1 404 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }   
	if(isset($_GET["IdCampania"])) {
    	$objCampania = dmCampania::getByIdCampania($_GET["IdCampania"]);
    	$listCategorias = dmCategoria::getCategoriasNew($_GET["IdCampania"]);
    }
    else {
		header('HTTP/1.1 404 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
    if(isset($_GET["IdProducto"])) {
    	$objProducto = dmProductos::getById($_GET["IdProducto"]);
    	$categoriasProducto = dmCategoria::getCategoriasProductos($_GET["IdProducto"]);
    }
    else {
		header('HTTP/1.1 404 Internal Server Error');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
    
    $postURL = Aplicacion::getIncludes("post", "productos");
    $objProducto = dmProductos::getById((isset($_GET["IdProducto"])) ? $_GET["IdProducto"]:0);
    $Atributos = dmProductos::AtributosByIdProducto($objProducto->getIdProducto());
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!-- saved from url=(0061)http://www.geelbe.com/ar/bo/mensajes/leer.php?idMensaje=74210 -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>BackOffice Geelbe - Campa�as - Productos - Stock</title>
		<? Includes::Scripts(); ?>
		<link href="<?=UrlResolver::getCssBaseUrl("bo/css/global.css")?>" rel="stylesheet" type="text/css" />
		<link href="<?=UrlResolver::getCssBaseUrl("css/listado_bo.css")?>" rel="stylesheet" type="text/css" />
		
		<link type="text/css" href="<?=UrlResolver::getCssBaseUrl("bo/css/ui.multiselect.css")?>" rel="stylesheet" />	
		<link type="text/css" href="<?=UrlResolver::getCssBaseUrl("css/ui-lightness/jquery-ui-1.8.9.custom.css")?>" rel="stylesheet" />	
		<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery-1.4.4.min.js")?>"></script>
		<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery.MultiFile.js")?>"></script>
		<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery.MetaData.js")?>"></script>
		<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery.MultiFile.pack.js")?>"></script>
		<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/jquery-ui-1.8.9.custom.min.js")?>"></script>
		<script type="text/javascript" src="<?=UrlResolver::getJsBaseUrl("js/ui.multiselect.js")?>"></script>
	</head>
	
	<body>
		
	<div id="glass-slide-container-div" class="glass-reset"></div>
		<div id="container">
		
		<form id="frmABMProductos" name="frmABMProductos" method="POST" action="/<?=$confRoot[1]?>/logica_bo/productos/actionform/productos_atributos_new.php">
		<input type="hidden" id="txtIdProducto" name="txtIdProducto" value="<?=$objProducto->getIdProducto()?>">
		<input type="hidden" id="txtIdCampania" name="txtIdCampania" value="<?=$_GET["IdCampania"]?>">
		<?  
			
			if (isset($_GET["IdCodigoProdInterno"]) && $_GET["IdCodigoProdInterno"]>0) {  ?>
        <input type="hidden" id="txtCodigoProdInterno" name="txtCodigoProdInterno" value="<?=$_GET["IdCodigoProdInterno"]?>">
		<?  	foreach ($objProducto->getProductosProveedores() as $PP) { 
                	if ($PP->getIdCodigoProdInterno()==$_GET["IdCodigoProdInterno"]) { 
                		$ProdInterno = $PP;	
                	}
				}
			}
        ?>
		
			<h1>Stock [producto: <?=$objProducto->getNombre()?>] [campa�a: <?=$objCampania->getNombre();?>]</h1>
			<div class="stats">

			  	<h2>Informaci�n b�sica</h2>
				<p>Aqu� se mostrar� la informaci�n b�sica del producto al que se le cargar� el stock.</p>
			  	<table>
			    <tbody>
					<tr>
						<th width="35%">Nombre del producto</th>
						<td width="65%"><?=$objProducto->getNombre()?></td>
					</tr>
					<tr class="alt">
						<th>Nombre de Campa�a</th>
						<td><?=$objCampania->getNombre();?></td>
					</tr>
				</tbody>
				</table>
				
			  	<h2><?=isset($ProdInterno)?"Modificaci&oacute;n":"Carga"?> de Stock</h2>
				<p>Aqu� se ingresar� el stock.</p>
			  	<table>
			    <tbody>
					<tr>
					<?
						$valueAttr = "0";
                        if (isset($ProdInterno)) { 
                        	$valueAttr = $ProdInterno->getStock();
                        }
					?>
					
						<th width="35%">Stock <?=isset($ProdInterno)?"[Valor Anterior: $valueAttr]":""?></th>
						<td width="65%"><input type="text" id="txtStock" name="txtStock" value="<?=$valueAttr?>" /></td>
					</tr>
			<?
				$odd = 0;
                foreach($Atributos as $i => $Atributo) {
                	$odd += 1;
                	if ($odd%2==0) {
                		?><tr><?
                	}
                	else {
                		?><tr class="alt"><?
                	}
                	
                        $valueAttr = "";
                        if (isset($ProdInterno)) { 
                        	$AtributosC = dmProductos::getAtributosByIdCodigoProdInterno($ProdInterno->getIdCodigoProdInterno());
                			foreach($AtributosC as $AtributoC) {
                    			if ($AtributoC["Nombre"]==$Atributo["Nombre"]) { 
                					$valueAttr = $AtributoC["Valor"];
                				}
                			}
                        }
                        ?>
                        <th><?=$Atributo["Nombre"]?> <?=isset($ProdInterno)?" [Valor Anterior: $valueAttr]":""?></th>
                        <td><input type="text" id="txtAtributo[<?=$Atributo["IdAtributoClase"]?>]" name="txtAtributo[<?=$Atributo["IdAtributoClase"]?>]" value="<?=$valueAttr?>"/></td>
                    </tr>
                    <?
                }
            ?>

				</tbody>
				</table>
<table style="border:0;">
                	<tbody>
                	<tr style="border:0;">
                		<td style="border:0;" width="35%">
<? if (isset($ProdInterno)) {
	?> <input type="submit" value="Cancelar" id="cancelar" name="cancelar"> <?
	
} ?>
                		</td>
                		<td style="border:0;" width="65%"><input type="submit" value="<?=isset($ProdInterno)?"Modificar":"Guardar"?>" id="botonCerrarYSiguiente" name="botonCerrarYSiguiente"></td>
                	</tr>
                </tbody></table>
				</form>
				
				<h2>Stock Cargado</h2>
				<p>Aqu� se mostrar� el stock ya cargado.</p>
			  	<table>
			    <tbody>
					<tr>
						<th>Producto</th>
			<?
				foreach($Atributos as $i => $Atributo) {
                	?><th><?=$Atributo["Nombre"]?></th> <?
                }
            ?>
						<th>Stock</th>
						<th>Acciones</th>
					</tr>
					
           	<?
           		$odd = 0;
                foreach ($objProducto->getProductosProveedores() as $PP) {
                    $AtributosC = dmProductos::getAtributosByIdCodigoProdInterno($PP->getIdCodigoProdInterno());
                    
                    $odd += 1;
                	if ($odd%2==0) {
                		?><tr><?
                	}
                	else {
                		?><tr class="alt"><?
                	}
                    ?>
                    	<td><?=$objProducto->getNombre()?></td>
                    
					<?
                    foreach($Atributos as $Atributo) {
                    	foreach($AtributosC as $AtributoC) {
                    		if ($Atributo["Nombre"]==$AtributoC["Nombre"]) {
                    			?><td><?=$AtributoC["Valor"]?></td><?
                    		}
                    	}
                    }
                    ?><td><?=$PP->getStock()?></td>
                    <td>
							<select id="U<?=$objProducto->getIdProducto().$_GET["IdCampania"]?><?=$PP->getIdCodigoProdInterno()?>" name="menu" style"width:40px;"="" onchange="process_choice(this, '<?=$PP->getIdCodigoProdInterno()?>')">
								<option value="0" selected="">Seleccione:</option>
								<option value="1">Modificar</option>
								<option value="2">Eliminar</option>
							</select>
					</td><?
                   
                    ?></tr><?
                }
                ?>
					
					
					
					
					
				</tbody>
				</table>
			 			<form style="margin:0;padding:0;" id="frmResponder" name="frmResponder" method="post" action="/<?=$confRoot[1]?>/bo/campanias/productos.php?IdCampania=<?=$_GET["IdCampania"]?>">
            	
                <table style="border:0;">
                	<tbody><tr style="border:0;">
                		<td style="border:0;"></td>
                		<td style="border:0;text-align:right;"><input type="submit" value="Guardar y Volver a Productos" id="botonResponderYSiguiente" name="botonResponderYSiguiente"></td>
                	</tr>
                </tbody></table>
            

		
				</div>
				
		</div>
		  
		  	<script type="text/javascript">
				function process_choice(selection, IdCodigoProdInterno) {
					if (selection.value==2) {
						window.location.href="/<?=$confRoot[1]?>/logica_bo/productos/actionform/productos_atributos_delete.php?IdProducto=<?=$objProducto->getIdProducto()?>&IdCampania=<?=$_GET["IdCampania"]?>&IdCodigoProdInterno="+IdCodigoProdInterno;
					}
					else if (selection.value==1) {
						window.location.href="/<?=$confRoot[1]?>/bo/productos/ABM_stock_new.php?IdProducto=<?=$objProducto->getIdProducto()?>&IdCampania=<?=$_GET["IdCampania"]?>&IdCodigoProdInterno="+IdCodigoProdInterno;
					}
					document.getElementById("U<?=$objProducto->getIdProducto().$_GET["IdCampania"]?>"+IdCodigoProdInterno).disabled="disabled";
				}
			</script>

		</body></html>