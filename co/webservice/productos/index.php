<?php

	ini_set('display_errors', 'Off');	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
   
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("catalogo"));
	require_once dirname(__FILE__).'/../ws.php';
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/core/marcas.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/marcas/clases/clsmarcas.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/marcas/datamappers/dmmarcas.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcategorias.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/core/clases_de_productos.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/clases_de_productos/clases/clsclases_de_productos.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/clases_de_productos/datamappers/dmclases_de_productos.php");

	//completar detalle de producto
	function product($p, $atributos) {
	
		$dir = "front/productos/productos_".$p["IdProducto"]."/";
		$arrImg = DirectorytoArray(Aplicacion::getRoot().$dir, 3, array("jpg", "jpeg", "gif", "png"));
		$p['Imagenes'] = array();
		foreach($arrImg as $img)
			$p['Imagenes'][] = '/co/'.$dir.$img;
		
		$p['Ultimos'] = $p['ultimos'];
		unset($p['ultimos']);
		
		$marca = dmMarcas::getByIdMarca($p['IdMarca']);
		$p['Marca'] = $marca->getNombre();
		
		$clase = dmClases::getByIdClase($p['IdClase']);
		$p['Clase'] = $clase->getNombre();

		//incluir categorias
		$p['Categorias'] = dmCategoria::getCategoriasFullProductos($p['IdProducto']);
		
		//incluir atributos
		if($atributos) {
			$p['Variantes'] = dmProductos::getProductosInternos($p['IdProducto']);
			foreach($p['Variantes'] as $i => $var)
				$p['Variantes'][$i]['Atributos'] = dmProductos::getAtributosByIdCodigoProdInterno($var["IdCodigoProdInterno"]);

		}

		return $p;
		
	}
	 
	 //Listado de productos por campaña  
	if(isset($_REQUEST['IdCampania']) && is_numeric($_REQUEST['IdCampania'])) {
		
		$objCampania = dmCampania::getByIdCampania($_REQUEST["IdCampania"]);
	
		if (isset($_REQUEST['IdCategoria']) && is_numeric($_REQUEST['IdCategoria'])) {
	        $oCategoria = dmCategoria::getByIdCategoria($_REQUEST['IdCategoria']);
	        $oOrdenamiento = dmOrdenamientos::getById($oCategoria->getIdOrdenamiento()); 
	    } else {
		    $oOrdenamiento = dmOrdenamientos::getById($objCampania->getIdOrdenamiento()); 
	    }
	    
		$dtCategorias = dmCategoria::getCategorias($_REQUEST["IdCampania"], (!isset($oCategoria) ? "IS NULL" : " = ".$oCategoria->getIdCategoria()));
		if(isset($oCategoria)) {
	
	        array_push($dtCategorias, array("IdCategoria" => $_REQUEST['IdCategoria'], "Nombre" => "", "Descripcion" => "", "IdCampania" => $_REQUEST["IdCampania"], "IdPadre" => 0,  "Orden" => 0, "subCategoria" => array ()));
	
	        for($j=0; $j<count($dtCategorias); $j++)
	        	$dtCategorias[$j]["IdOrdenamiento"] = $oCategoria->getIdOrdenamiento();
	        	
		}
		
		$productos = dmProductos::getProductosOrdenadosPaginados($dtCategorias, false, false, $oOrdenamiento->getCampo(), $oOrdenamiento->getOrdenamiento());
		
		$output = array();
		
		foreach($productos as $p)
			$output[] = product($p, isset($_GET['atributos']));
			
	//Detalle de producto
	} elseif(isset($_REQUEST['IdProducto']) && is_numeric($_REQUEST['IdProducto'])) {
		
		$producto = dmProductos::getDataById($_REQUEST['IdProducto']);
		$output = product($producto, isset($_GET['atributos']));
		
	}
		
	
	echo wsOutput($output);
	
?>