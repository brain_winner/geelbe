<?php
	
	ini_set('display_errors', 'Off');	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	require_once dirname(__FILE__).'/../ws.php';
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("catalogo"));
	   
	if(!isset($_REQUEST['IdCampania']) || !is_numeric($_REQUEST['IdCampania']))
		die;
		
	$dtCategorias = dmCategoria::getCategorias($_REQUEST["IdCampania"], "IS NULL");
	echo wsOutput($dtCategorias);
	
?>