<?php

	ini_set('display_errors', 'Off');	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT']);
	
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));     
	require_once dirname(__FILE__).'/../ws.php';
 	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcategoriasglobales.php");
 	
 	//Una campaña por ID
 	if(isset($_REQUEST['IdCampania']) && is_numeric($_REQUEST['IdCampania'])) {
	 	
	 	$objCampania = dmCampania::getByIdCampania($_REQUEST['IdCampania']);
	 	$dir = Aplicacion::getRoot()."front/campanias/archivos/campania_".$_REQUEST["IdCampania"]."/imagenes/";
	 	$arrArchivos = DirectorytoArray($dir, 3, array("jpg", "jpeg", "gif", "png"));
	 	
	 	$output = array(
	 		'IdCampania' => $objCampania->getIdCampania(),
			'Nombre' => $objCampania->getNombre(),
			'Descripcion' => $objCampania->getDescripcion(),
			'FechaInicio' => $objCampania->getFechaInicio(),
			'FechaFin' => $objCampania->getFechaFin(),
			'VisibleDesde' => $objCampania->getVisibleDesde(),
			'TiempoEntregaIn' => $objCampania->getTiempoEntregaIn(),
			'TiempoEntregaFn' => $objCampania->getTiempoEntregaFn(),
			'TiempoEntregaIn2' => $objCampania->getTiempoEntregaIn2(),
			'TiempoEntregaFn2' => $objCampania->getTiempoEntregaFn2(),
			'FechaCorte' => $objCampania->getFechaCorte(),
			'MinMontoEnvioGratis' => $objCampania->getMinMontoEnvioGratis(),
			'Welcome' => $objCampania->getWelcome(),
			'Coleccion' => $objCampania->getColeccion(),
			'Imagenes' => array(
				'Logo' => $dir.(isset($arrArchivos["logo2"]) ? $arrArchivos["logo2"] : $arrArchivos["thumb_logo"]),
				'Banner' => $dir.$arrArchivos['banner_camp-on'],
				'Bienvenida' => $dir.$arrArchivos['welcome']
			)
	 	);
	 
	//Listado de campañas, opcionalmente filtrado por categoría		
 	} else {
 	
	    if(isset($_REQUEST['IdCategoriaGlobal'])) {
	    	$idCatGlobal = dmCategoriaGlobal::getChildrenIds($_REQUEST['IdCategoriaGlobal']);
	    	$idCatGlobal[] = $_REQUEST['IdCategoriaGlobal'];
	    } else
	    	$idCatGlobal = null;
	
	    $dtcampanias = dmCampania::getCampaniasWebservice($idCatGlobal);
		$output = array();
		foreach($dtcampanias as $c) {
			
			$arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$c["IdCampania"]."/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));
			
			$c['Imagenes'] = array(
				'Logo' => (isset($arrArchivos["logo2"]) ? $arrArchivos["logo2"] : $arrArchivos["thumb_logo"]),
				'Banner' => $arrArchivos['banner_camp-on'],
				'Bienvenida' => $arrArchivos['welcome']
			);
			
			$output[] = $c;
				
		}
		
	}
		
	echo wsOutput($output);
	
?>