<?php

	function wsOutput($array) {
		$array = arrayToUTF8($array);
		echo json_encode($array);
	}
	
	function arrayToUTF8($array) {
		
		foreach($array as $i => $v)
			if(is_array($v))
				$array[$i] = arrayToUTF8($v);
			else
				$array[$i] = utf8_encode($v);
						
		return $array;
	}

?>