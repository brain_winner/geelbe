<?php

	ini_set('display_errors', 'Off');	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once dirname(__FILE__).'/../ws.php';
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/registro/formvalidator.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/registro.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/datamappers/dmregistro.php";
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
	
	$activationCode = $_POST['txtPadrino'] = 'appMayo20141';
	$registro = new RegistroController;
	$email = (isset($_POST['email']) ? $_POST['email'] : '');
	$pwd = (isset($_POST['password']) ? $_POST['password'] : '');
	$error = array();
	
	//Validación
	if(!filter_var($email, FILTER_VALIDATE_EMAIL))
		$error['email'] = 'El e-mail ingresado es inválido.';
	else if($registro->isRegisteredUser($email))
		$error['email'] = 'El e-mail ingresado ya está registrado.';
		
	if(strlen($pwd) < 6 || strlen($pwd) > 30)
		$error['password'] = 'La contraseña debe tener entre 6 y 30 caracteres.';
		
	if(count($error)) {
		echo wsOutput(array('ok' => false, 'error' => $error));
		die;
	}	
	
	//Registro
	$registro->insertUser($_POST["email"], $_POST["password"], $_POST["firstname"], $_POST["lastname"], $_POST["gender"], $_POST["birthdayday"], $_POST["birthdaymonth"], $_POST["birthdayyear"]);
	$objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($_POST["email"]);
	$_POST['IdUsuario'] = $objUsuarios->getIdUsuario();
	$registro->updateUserToInactive($objUsuarios);
	$registro->updatePassword($objUsuarios, $_POST["password"]);
	if ($registro->isValidCode($activationCode)) {
		$registro->updatePadrino2($objUsuarios, $activationCode);
		$registro->enviarActivacion();
	}
	
	echo wsOutput(array('ok' => true));
	die;
	
?>