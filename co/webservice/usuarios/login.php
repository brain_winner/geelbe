<?php

	ini_set('display_errors', 'Off');	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once dirname(__FILE__).'/../ws.php';
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php";
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login"));
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("registro"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/conexion/Conectar.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php";
	
	$key = 'Pjv1XhT9tE_pJB5JEWUu9w';
	$email = (isset($_POST['email']) ? $_POST['email'] : '');
	$pwd = (isset($_POST['password']) ? $_POST['password'] : '');
	$error = array();
	
	//Validación
	if(!isset($_POST['key']) || $_POST['key'] != $key) {
		echo wsOutput(array('ok' => false, 'error' => 'Clave inválida.'));
		die;
	}
	
	if(!filter_var($email, FILTER_VALIDATE_EMAIL))
		$error['email'] = 'El e-mail ingresado es inválido.';
		
	if(strlen($pwd) < 6 || strlen($pwd) > 30)
		$error['password'] = 'La contraseña debe tener entre 6 y 30 caracteres.';
		
	if(count($error)) {
		echo wsOutput(array('ok' => false, 'error' => $error));
		die;
	}	
	
	//Login
	$login = new LoginController();
	$login->setobjUsuarios(dmUsuarios::Login($email,$pwd));
	
	//Inexistente
	if($login->getobjUsuarios() === false){
		echo wsOutput(array('ok' => false, 'error' => 'Credenciales inválidas.'));
		die;
	}

	//No Activo Sin Codigo
	//  Redirect Poner Codigo
	elseif($login->getobjUsuarios()->getPadrino() == "Huerfano"){
		echo wsOutput(array('ok' => false, 'error' => 'Usuario inactivo.'));
		die;
	}

	//No Activo con C�digo
	//  Redirect Activa tu cuenta
	elseif($login->getobjUsuarios()->getes_Activa() == 1 && $login->getobjUsuarios()->getPadrino() != "Huerfano"){
		echo wsOutput(array('ok' => false, 'error' => 'Usuario inactivo.'));
		die;
	}

	//Activo
	elseif($login->getobjUsuarios()->getes_Activa() == 0){
		$login->guardarHistorico();
		echo wsOutput(array('ok' => true));
		die;

	} else {
		echo wsOutput(array('ok' => false, 'error' => 'Credenciales inválidas.'));
		die;
	}
	
?>