<?php

	ini_set('display_errors', 'Off');	
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	set_include_path(get_include_path().PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT']);
	
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));     
	require_once dirname(__FILE__).'/../ws.php';
 	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcategoriasglobales.php");
 	
 	//Una campaña por ID
 	if(isset($_REQUEST['IdCategoriaGlobal']))
 		$parent = $_REQUEST['IdCategoriaGlobal'];
 	else
 		$parent = null;
 		
 	$output = dmCategoriaGlobal::getCategorias($parent, 0, false);
	echo wsOutput($output);
	
?>