<?php
    try {
      $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
      require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/registro/formvalidator.php";
	  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/registro.php";
	  require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
	  Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
	  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/usuarios/datamappers/dmusuariosfacebook.php";
    } catch(exception $e) {
        die(print_r($e));
    }
	
	if(!isset($_REQUEST["key"])) {
	    $respuesta = array("error" => "true", "error_type" => "key_value_not_found", "error_desc" => "Key de autenticacion no especificada.");
	    echo json_encode($respuesta);
		exit;
	} else {
        $oConexion = Conexion::nuevo();
        $oConexion->Abrir();
        $oConexion->setQuery('SELECT 1 AS validKey FROM geelbe_allowed_api_users WHERE api_key = "'.$_REQUEST["key"].'"');
		$KeyValidation = $oConexion->DevolverQuery();
		
		if(!$KeyValidation[0]["validKey"]) {
		    $respuesta = array("error" => "true", "error_type" => "invalid_key_value", "error_desc" => "Key de autenticacion invalida");
		    echo json_encode($respuesta);
			exit;
		} 
	}

	if($_REQUEST['user_data'] == null) {
	    $respuesta = array("error" => "true", "error_type" => "user_data_is_required");
	    echo json_encode($respuesta);
		exit;
    }
	
	$userData = json_decode(stripslashes($_REQUEST['user_data']));
	
    if($userData->uid == null || $userData->uid == "") {
	    $respuesta = array("error" => "true", "error_type" => "facebook_id_required");
	    echo json_encode($respuesta);
		exit;
    }

    if($userData->email == null || $userData->email == "") {
	    $respuesta = array("error" => "true", "error_type" => "user_email_required");
	    echo json_encode($respuesta);
		exit;
    }

    if(strrpos($userData->email, "@proxymail.facebook.com") > 0) {
	    $respuesta = array("error" => "true", "error_type" => "invalid_email");
	    echo json_encode($respuesta);
		exit;
    }
    
    if ($_REQUEST["codact"] == "") {
    	$_REQUEST["codact"] = null;
    }

	$registro = new RegistroController();
    $respuesta = $registro->coregistrarUsuarioFacebook($userData, $_REQUEST["codact"], true);

    dmUsuariosFacebook::saveOrUpdateFacebookId($respuesta["id_usuario"], $userData->uid);
    
    if($respuesta["usuario_existente"]) {
    	$objUsuario = dmUsuario::getByIdUsuario($respuesta["id_usuario"]);
	    $actualizarUsuario = dmUsuariosFacebook::updateUserInfo($objUsuario, $userData);
    } else {
    	// Inserto el usuario en la lista de ems
    	
    	$nombre = null;
		$apellido = null;
		$anioNacimiento = "0000";
		$mesNacmimiento = "00";
		$diaNacimiento = "00";
		$genero = null;
		
	    if ($userData->first_name != null && $userData->first_name != "") {
	    	$nombre = utf8_decode($userData->first_name);
	    }
	    if ($userData->last_name != null && $userData->last_name != "") {
	    	$apellido = utf8_decode($userData->last_name);
	    }
	    if ($userData->birthday != null && $userData->birthday != "" && $userData->birthday != "0000-00-00") {
			$pieces = explode("/", $userData->birthday);
			$diaNacimiento = $pieces[1];
			$mesNacmimiento = $pieces[0];
			$anioNacimiento = $pieces[2];
	    }
	    if ($userData->gender != null && $userData->gender != "") {
	    	if($userData->gender == "male") {
		    	$genero = 1;
	    	} else {
		    	$genero = 0;
	    	}
	    }
    	
    	$oConexion = Conexion::nuevo();
        $oConexion->setQuery('INSERT INTO acciones_listas_ems (email, nombre, apellido, tipo_accion, fecha_accion, accion_encolada, accion_commiteada, id_usuario, hash_usuario, fechanacimiento_usuario, sexo_usuario, provincia_usuario) 
            VALUES ("'.htmlspecialchars(mysql_real_escape_string($userData->email), ENT_QUOTES).'", "'.htmlspecialchars(mysql_real_escape_string($nombre), ENT_QUOTES).'", "'.htmlspecialchars(mysql_real_escape_string($apellido), ENT_QUOTES).'", "A", NOW(), 0, 0, '.
            $respuesta["id_usuario"].', "'.Hash::getHash($respuesta["id_usuario"].$userData->email).'", "'.$anioNacimiento."-".$mesNacmimiento."-".$diaNacimiento.'", "'.
            $genero.'", "'.$provinciaUsuario.'");');
        $oConexion->EjecutarQuery();
    }

    $FacebookHash = Hash::getHash($respuesta["id_usuario"].$userData->uid);
    $UrlRedirect = UrlResolver::getBaseUrl("logica/autologin/login.php?IdUsuario=".$respuesta["id_usuario"]."&Hash=".$FacebookHash."&Type=FACEBOOKUSER");
    
    $respuesta = array("error" => "false", "usuario_existente" => $respuesta["usuario_existente"], "url_redirect" => $UrlRedirect);
    echo json_encode($respuesta);
    
?>

