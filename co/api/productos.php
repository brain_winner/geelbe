<?php
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));     
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));     
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));

	    function xml_entities($text, $charset = 'Windows-1252'){
		    $text = htmlentities($text, ENT_COMPAT, $charset, false);
		    
		    $arr_xml_special_char = array("&quot;","&amp;","&apos;","&lt;","&gt;");
		    
		    $arr_xml_special_char_regex = "(?";
		    foreach($arr_xml_special_char as $key => $value){
		        $arr_xml_special_char_regex .= "(?!$value)";
		    }
		    $arr_xml_special_char_regex .= ")";
		    
		    $pattern = "/$arr_xml_special_char_regex&([a-zA-Z0-9]+;)/";
		    
		    $replacement = '&amp;${1}';
		    return preg_replace($pattern, $replacement, $text);
		}
	   		
    }
    catch(exception $e) {
        die(print_r($e));
    }
	
	if(!isset($_GET["key"])) {
		echo '<error>Key de autenticacion no especificada.</error>';
		die;
	} else {
        $oConexion = Conexion::nuevo();
        $oConexion->Abrir();
        $oConexion->setQuery('SELECT 1 AS validKey FROM geelbe_allowed_api_users WHERE api_key = "'.$_GET["key"].'"');
		$KeyValidation = $oConexion->DevolverQuery();
		
		if(!$KeyValidation[0]["validKey"]) {
			echo '<error>Key de autenticacion invalida.</error>';
			die;
		} 
	}

		$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 300), array('cache_dir' => $cacheDir));

		if(!$contenidoCache = $cache->load('cache_xml_productos')) {
			ob_clean();
			ob_start();
			
			$dtcampanias = dmCampania::getCampaniasOrdenadas();
			echo '<?xml version="1.0"?>';
			echo '<tienda>';
			foreach ($dtcampanias as $campania) {
				if($campania["Visible"] == "Si") {
	     			$dtproductos = dmProductos::getProductosDeCampa˝a($campania["IdCampania"]);
					foreach ($dtproductos as $producto) {
						$dir = "../front/productos/productos_".$producto["IdProducto"]."/thumbs/";
						$arrImg = DirectorytoArray($dir, 1, array("jpg", "jpeg", "gif", "png"));
						$productImageUrl = UrlResolver::getImgBaseUrl("front/productos/productos_".$producto["IdProducto"]."/producto_1.jpg");
						$CategoryId = dmProductos::getCategoria_byIdProducto($producto["IdProducto"], $campania["IdCampania"]);
						$productUrl = UrlResolver::getBaseUrl("front/catalogo/detalle_public.php?IdCampania=".$campania["IdCampania"]."&IdCategoria=".$CategoryId."&IdProducto=".$producto["IdProducto"]."&codact=buscape-feed-feb-2011-1");
						$Discount = 100 - ($producto["PrecioGeelbe"]/$producto["PrecioLista"]*100);
						
						echo '<producto>';
						echo '<producto>'.xml_entities($producto["Nombre"]).'</producto>';
						echo '<link_producto>'.xml_entities($productUrl).'</link_producto>';
						echo '<precio>'.xml_entities($producto["PrecioGeelbe"]).'</precio>';
						echo '<id_oferta>'.$producto["IdProducto"].'</id_oferta>';
						echo '<categoria>Promociones</categoria>';
						echo '<img_oferta>'.xml_entities($productImageUrl).'</img_oferta>';
						echo '<descuento>'.((int)$Discount).'% OFF</descuento>';
	     				echo '</producto>';
					}
				}
			}
			echo '</tienda>';
			
			$contenidoCache = trim(ob_get_clean());
            $cache->save($contenidoCache, 'cache_xml_productos');
        }
		echo $contenidoCache;
?>

