<?php
/*
 * * Datos: 
 * ** Requeridos:
 * *** email - formato email *@*.*
 * *** haspassword - true/false
 * *** activationcode - valid activation code
 * ** Opcionales: 
 * *** password
 * *** firstname
 * *** lastname
 * *** gender
 * *** birthdayday
 * *** birthdaymonth
 * *** birthdayyear
 */
  ob_start();
  $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php";
  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/registro/formvalidator.php";
  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/registro.php";
  require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
  Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
  
  $validationError = false;
  if (!isset($_REQUEST["email"]) || empty($_REQUEST["email"])) { // Email vacio
  	$validationError = true;
  } else if (!ValidatorUtils::validateEmail($_REQUEST["email"])) { // Formato del email incorrecto
	$validationError = true;
  } else if (strlen($_REQUEST["email"])>255) { // Mail demaciado largo
  	$validationError = true;
  } else if (isset($_REQUEST["haspassword"]) && $_REQUEST["haspassword"] == "true") {
  	if (!isset($_REQUEST["password"]) || empty($_REQUEST["password"])) { // Repetir Clave vacia
		$validationError = true;
  	} else if (strlen($_REQUEST["password"])>30) {
  		$validationError = true;
  	} else if (strlen($_REQUEST["password"])<6) {
  		$validationError = true;
  	}
  }

  if(!$validationError) {
      $registro = new RegistroController();
      $registro->coregistrar();
  }
  else {
      echo "false";
  }
  ob_flush();
?>