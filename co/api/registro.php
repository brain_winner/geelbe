<?php
	define("ERROR_POS_DESCRIPTOR", "ErrorMSJ(\"Error de registro.\",\"");

	try {
		ob_start();
		
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
		
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("login"));
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("emails"));
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("registro"));
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados","dm"));	

		// Validando los parametros de entrada ...
		$validationError = false;
		$validationErrorKey = "";
		if (!isset($_REQUEST["txtNombre"]) || empty($_REQUEST["txtNombre"])) { // Nombre vacio
			$validationError = true;
			$validationErrorKey = "NOMBRE";
		} else if (!isset($_REQUEST["txtApellido"]) || empty($_REQUEST["txtApellido"])) { // Apellido vacio
			$validationError = true;
			$validationErrorKey = "APELLIDO";
		} else if (!isset($_REQUEST["txtEmail"]) || empty($_REQUEST["txtEmail"])) { // Email vacio
			$validationError = true;
			$validationErrorKey = "MAIL_INCORRECTO";
		} else if (!ValidatorUtils::validateEmail($_REQUEST["txtEmail"])) { // Formato del email incorrecto
			$validationError = true;
			$validationErrorKey = "MAIL_INCORRECTO";
		} else if (!isset($_REQUEST["txtClave"]) || empty($_REQUEST["txtClave"])) { // Clave vacia
			$validationError = true;
			$validationErrorKey = "CLAVE_INCORRECTO";
		} else if (!isset($_REQUEST["txtConfClave"]) || empty($_REQUEST["txtConfClave"])) { // Repetir Clave vacia
			$validationError = true;
			$validationErrorKey = "REPETIR_CLAVE_INCORRECTO";
		} else if (!(strlen($_REQUEST["txtClave"]) > 5 && strlen($_REQUEST["txtClave"]) < 17)) { // Tama�o incorrecto de clave
			$validationError = true;
			$validationErrorKey = "CLAVE_INCORRECTO";
		} else if (!(strlen($_REQUEST["txtConfClave"]) > 5 && strlen($_REQUEST["txtConfClave"]) < 17)) { // Tama�o incorrecto de repeticion de clave
			$validationError = true;
			$validationErrorKey = "REPETIR_CLAVE_INCORRECTO";
		} else if ($_REQUEST["txtConfClave"] != $_REQUEST["txtClave"]) { // Clave y Repetir Clave de distinto tama�o
			$validationError = true;
			$validationErrorKey = "CLAVE_DIFERENTES";
		} else if (!isset($_REQUEST["cmb_nacimientoAnio"]) || empty($_REQUEST["cmb_nacimientoAnio"])) { // A�o nacimiento vacio
			$validationError = true;
			$validationErrorKey = "FECHA DE NACIMIENTO";
		} else if ($_REQUEST["cmb_nacimientoAnio"] == 0) { // A�o nacimiento igual a cero
			$validationError = true;
			$validationErrorKey = "FECHA DE NACIMIENTO";
		} else if (!isset($_REQUEST["cmb_nacimientoMes"]) || empty($_REQUEST["cmb_nacimientoMes"])) { // Mes nacimiento vacio
			$validationError = true;
			$validationErrorKey = "FECHA DE NACIMIENTO";
		} else if ($_REQUEST["cmb_nacimientoMes"] == 0) { // Mes nacimiento igual a cero
			$validationError = true;
			$validationErrorKey = "FECHA DE NACIMIENTO";
		} else if (!isset($_REQUEST["cmb_nacimientoDia"]) || empty($_REQUEST["cmb_nacimientoDia"])) { // Dia nacimiento vacio
			$validationError = true;
			$validationErrorKey = "FECHA DE NACIMIENTO";
		} else if ($_REQUEST["cmb_nacimientoDia"] == 0) { // Dia nacimiento igual a cero
			$validationError = true;
			$validationErrorKey = "FECHA DE NACIMIENTO";
		} else if (!ValidatorUtils::validateDate($_REQUEST["cmb_nacimientoAnio"]."-".$_REQUEST["cmb_nacimientoMes"]."-".$_REQUEST["cmb_nacimientoDia"])) { // Verifica que sea una fecha valida
			$validationError = true;
			$validationErrorKey = "FECHA INVALIDA";
		} else if (!ValidatorUtils::validteOlderUser($_REQUEST["cmb_nacimientoAnio"]."-".$_REQUEST["cmb_nacimientoMes"]."-".$_REQUEST["cmb_nacimientoDia"], 18)) { // Verifica que el usuario sea mayor de edad
			$validationError = true;
			$validationErrorKey = "MENOR DE EDAD";
		} else if (!isset($_REQUEST["acepto2"]) || empty($_REQUEST["acepto2"])) { // Terminos y condiciones aceptados
			$validationError = true;
			$validationErrorKey = "TERMINOS";
		}    
		
		if($validationError) {
			ob_clean();
			echo "{\"estado\":\"0\",\"error_key\":\"".$validationErrorKey."\"}";
			exit;
		}
		
		// Realizando el registro ...
		$registro = new RegistroController();
		$registro->registrar();
		
		$result = ob_get_contents();
	
		// Verificando que no haya ningun error ...
		$error = false;
		$errorKey = "";
		if(stripos($result, "ErrorMSJ") > 0) {
			$error = true;
			$errorKey = substr($result, stripos($result, ERROR_POS_DESCRIPTOR) + strlen(ERROR_POS_DESCRIPTOR));
			$errorKey = substr($errorKey, 0, stripos($errorKey, '"'));
		}
	    ob_clean();
	    
	    if($error) {
	    	echo "{\"estado\":\"0\",\"error_key\":\"".$errorKey."\"}";
	    } else {
		    echo "{\"estado\":\"1\",\"error_key\":\"\"}";
	    }
    } catch(Exception $e){
		echo "{\"estado\":\"0\",\"error_key\":\"ERROR_GENERAL\"}";
	}
?>