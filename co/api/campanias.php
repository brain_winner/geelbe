<?php
    try {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));     
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("productos"));     
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
        
   		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/adminappfacebook/datamappers/dmappfacebook.php");
    }
    catch(exception $e) {
        die(print_r($e));
    }
	
	if(!isset($_GET["key"])) {
		echo '{"error":"Key de autenticacion no especificada."}';
		die;
	} else {
        $oConexion = Conexion::nuevo();
        $oConexion->Abrir();
        $oConexion->setQuery('SELECT 1 AS validKey FROM geelbe_allowed_api_users WHERE api_key = "'.$_GET["key"].'"');
		$KeyValidation = $oConexion->DevolverQuery();
		
		if(!$KeyValidation[0]["validKey"]) {
			echo '{"error":"Key de autenticacion invalida."}';
			die;
		} 
	}

		$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => 300), array('cache_dir' => $cacheDir));

		if(!$contenidoCache = $cache->load('cache_json_campanias')) {
			ob_start();
			
			$timezone = substr(Aplicacion::getParametros("info", "mysql_timezone"), 0, strrpos(Aplicacion::getParametros("info", "mysql_timezone"), ":"));
			
			echo '{';
			
			echo '"regional" : [';
			echo '{';
			echo '"country": "'.strtoupper($confRoot[1]).'",';
			echo '"gmt": '.$timezone;
			echo '}';
			echo '],';

			echo '"banners" : [';
			$banners = dmAdminAppFacebook::getAllBanners();
			$isFirstBanner = true;
			foreach($banners as $banner) {
				if (!$isFirstBanner) {
      				echo ', ';
      			}
      			
      			$isFirstBanner = false;
      			
				echo '{';
				echo '"category": '.'"'.$banner["seccion"].'",';
				if(isset($banner["activo"]) && $banner["activo"]) {
					echo '"banner_top_url": "'.UrlResolver::getBaseUrl("front/archivos/".$banner["seccion"]."top.jpg").'",';
					echo '"banner_top_href": "'.$banner["href_banner_top"].'",';
					echo '"banner_top_title": "'.$banner["title_banner_top"].'",';
					echo '"banner_bottom_url": "'.UrlResolver::getBaseUrl("front/archivos/".$banner["seccion"]."bottom.jpg").'",';
					echo '"banner_bottom_href": "'.$banner["href_banner_bottom"].'",';
					echo '"banner_bottom_title": "'.$banner["title_banner_bottom"].'"';
				} else {
					echo '"banner_top_url": "",';
					echo '"banner_top_href": "",';
					echo '"banner_top_title": "",';
					echo '"banner_bottom_url": "",';
					echo '"banner_bottom_href": "",';
					echo '"banner_bottom_title": ""';
				}
				echo '}';
			}
			echo '],';
			
			$dtcampanias = dmCampania::getCampaniasOrdenadas();
			$isFirstCampaign = true;
			echo '"campaigns" : [ ';
			foreach ($dtcampanias as $campania) {
				if($campania["Visible"] == "Si") {
					if ($campania["IdCampania"] == 270) {
						continue;
					}
					
					$idLanding = dmCampania::getIdLandingDeCampania($campania["IdCampania"]);
					if($idLanding != null) {
						$textoLanding = dmCampania::getTextoLandingDeCampania($idLanding);
					}
					
					$arrArchivos = DirectorytoArray(Aplicacion::getRoot()."front/campanias/archivos/campania_".$campania["IdCampania"]."/imagenes/", 3, array("jpg", "jpeg", "gif", "png"));
	      			$isActive = ($campania["Estado"] == "Abierta")?1:0;
	      			$campaignUrl = json_encode(UrlResolver::getBaseUrl("registro/marca/marca_index.php?id=".$idLanding."&codact=appfacebook-campaigns1"));
	      			$brandName = json_encode(utf8_encode($campania["Nombre"]));
	      			$description = json_encode (utf8_encode($campania["Descripcion"]));
	      			$campaingBanner = json_encode(UrlResolver::getImgBaseUrl("front/campanias/archivos/campania_".$campania["IdCampania"]."/imagenes/".$arrArchivos["thumb_imagen"]));
	      			$brandLogo = json_encode(UrlResolver::getImgBaseUrl("front/campanias/archivos/campania_".$campania["IdCampania"]."/imagenes/".$arrArchivos["logo2"]));
	      			$campaingBannerInactive = json_encode(UrlResolver::getImgBaseUrl("front/campanias/archivos/campania_".$campania["IdCampania"]."/imagenes/".$arrArchivos["thumb_no_imagen"]));
	      			$brandLogoInactive = json_encode(UrlResolver::getImgBaseUrl("front/campanias/archivos/campania_".$campania["IdCampania"]."/imagenes/".$arrArchivos["no-logo2"]));
	      			
	      			$pieces = explode("/", $campania["Fecha de fin"]);
					$d = $pieces[0];
					$m = $pieces[1];
					$yearAndHour = explode(" ", $pieces[2]);
					$y = $yearAndHour[0];
					$h = $yearAndHour[1];
	      			$fechaFin = $y."-".$m."-".$d." ".$h.":00";
	
	      			$pieces = explode("/", $campania["Fecha de inicio"]);
					$d = $pieces[0];
					$m = $pieces[1];
					$yearAndHour = explode(" ", $pieces[2]);
					$y = $yearAndHour[0];
					$h = $yearAndHour[1];
	      			$fechaInicio = $y."-".$m."-".$d." ".$h.":00";
	      			
	      			if (!$isFirstCampaign) {
	      				echo ', ';
	      			}
	      			
	     			echo '{ ';
	     			echo '"id": "'.$campania["IdCampania"].'", ';
	     			
	     			echo '"campaign_banner" : '.$campaingBanner.', ';
	     			echo '"campaign_banner_inactive" : '.$campaingBannerInactive.', ';
	     			echo '"brand_logo" : '.$brandLogo.', ';
	     			echo '"brand_logo_inactive" : '.$brandLogoInactive.', ';
	     			
	     			echo '"brand_name" : '.$brandName.', ';
	     			echo '"description" : '.$description.', ';
	     			echo '"expire_date" : "'.$fechaFin.'", ';
	            	echo '"start_date" : "'.$fechaInicio.'", ';
	            	echo '"is_active" : "'.$isActive.'", ';
	     			echo '"campaign_url" : '.$campaignUrl.', ';
	     			echo '"campaign_landing_text" : '.json_encode($textoLanding).', ';
	     			
	     			$dtproductos = dmProductos::getProductosDeCampa˝a($campania["IdCampania"]);
	     			$isFirstProduct = true;
					echo '"products" : [ ';
					foreach ($dtproductos as $producto) {
						$dir = "../front/productos/productos_".$producto["IdProducto"]."/thumbs/";
						$arrImg = DirectorytoArray($dir, 1, array("jpg", "jpeg", "gif", "png"));
						$prodThumbImageName = "thumb_producto_1.jpg";
						$prodImageName = "producto_1.jpg";
						if (count($arrImg) > 0) {
							$prodThumbImageName = $arrImg[0];
							if (stripos($prodThumbImageNamem, "thumb_") !== false) {
								$prodImageName = substr($prodThumbImageName, 6);
							}
						}
						
						$productImageUrl = json_encode(UrlResolver::getImgBaseUrl("front/productos/productos_".$producto["IdProducto"]."/".$prodImageName));
						$productThumImageUrl = json_encode(UrlResolver::getImgBaseUrl("front/productos/productos_".$producto["IdProducto"]."/thumbs/".$prodThumbImageName));
						$productUrl = json_encode(UrlResolver::getBaseUrl("front/catalogo/detalle.php?IdCampania=".$campania["IdCampania"]."&IdProducto=".$producto["IdProducto"]));
						$CategoryId = dmProductos::getCategoria_byIdProducto($producto["IdProducto"], $campania["IdCampania"]);
						$Discount = number_format(100 - ($producto["PrecioGeelbe"]/$producto["PrecioLista"]*100), 0, '.', '')."%";
						
					    if (!$isFirstProduct) {
	      					echo ', ';
	      				}
						
						echo '{ ';
						echo '"product_id" : "'.$producto["IdProducto"].'", ';
						echo '"category_id" : "'.$CategoryId.'", ';
	     				echo '"name" : '.json_encode(utf8_encode($producto["Nombre"])).', ';
	     				echo '"discount" : '.json_encode($Discount).', ';
	     				echo '"image" : '.$productImageUrl.', ';
	     				echo '"thumb_image" : '.$productThumImageUrl.', ';
	     				echo '"description" : '.json_encode(utf8_encode($producto["Descripcion"])).', ';
	     				echo '"producto_url" : '.$productUrl.' ';
	     				echo '} ';
	     				$isFirstProduct = false;
					}
					echo ']';
	
					echo '} ';
	     			
	     			$isFirstCampaign = false;
				}
			}
			echo '] } ';	
			
			$contenidoCache = ob_get_clean();
            $cache->save($contenidoCache, 'cache_json_campanias');
        }
		echo $contenidoCache;
?>

