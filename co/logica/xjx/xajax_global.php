<?
	define("VIP_TIEMPO_EXPIRACION", 15*60 );
	require_once("Sistema/Global.php");
	require_once($dirLo."Usuarios.php");
	require_once($dirDM."DMUsuarios.php");	
	require_once("Sistema/Funciones/LogXML.php");
	require_once("bo/". $dirLoBo . "ParserSTR.php");
	require_once($dirXAJAX . "xajax.inc.php");
	require_once($dirPre . "html_Carritos.php");
	require_once($dirLo . "AccesosRapidos.php");
	require_once($dirDM ."DMAccesosRapidos.php");
	require_once($dirDM ."DMAccesosClaves.php");
	require_once($dirLo . "Productos.php");
	require_once($dirDM . "DMInscripcion.php");
	require_once($dirDM . "DMApadrinar.php");
    require_once($dirDM . "DMReglasNegocios.php");
	
	$xajax = new xajax();
	$xajax->setCharEncoding("ISO-8859-1");
	$xajax->decodeUTF8InputOn();
	$xajax->errorHandlerOn();
	$xajax->exitAllowedOn();
	$xajax->setLogFile("Ajax_Errores.log");
	$xajax->cleanBufferOn();
	$xajax->debugOff();
	$xajax->statusMessagesOn();
	$xajax->registerFunction("VUsuario");
	$xajax->registerFunction("GetAccesosRapidos");
	$xajax->registerFunction("CargarAtributosCombos_ACC");
	$xajax->registerFunction("PreCompra_ACC");
	$xajax->registerFunction("ValidarClavesPromo");
	$xajax->registerFunction("Apadrina_Global");
	
	function ParsearString($string)
	{
		$objParser = new ParserSTR();
		return $objParser->getString_a_MySQL($string);
	}
	function ParsearDateUNIX($string)
	{
		$objParser = new ParserSTR();
		return $objParser->getDateUNIX_a_MySQL($string);
	}
	function ParsearDateDMY($string)
	{
		$objParser = new ParserSTR();
		return $objParser->getDateDMY_a_MySQL($string);
	}
	function ParsearTimeDMY($date, $hora)
	{
		$objParser = new ParserSTR();
		return $objParser->getTimeDMY_a_MySQL($date, $hora);
	}
	
	function Apadrina_Global($txtEmail)
	{
		$objResponse = new xajaxResponse("ISO-8859-1");
		try
		{			
			DMReglasNegocio_Aplicar_08($_SESSION["Usuario"][0]->IdUsuario, DMA_InvitacionGlobal($txtEmail,$_SESSION["Usuario"][0]->IdUsuario));//Invitacion				
		}
		catch(exception $e)
		{
			$objResponse->addAlert("Apadrina_Global - ERROR \n\n" . $e->getMessage());
		}				
		return $objResponse;
	}
	function GetAccesosRapidos($IdSeccion)
	{
		$objResponse = new xajaxResponse("ISO-8859-1");
		try
		{
			$htmlAccRapidos = DMAccesosRapidos_getApadrinar($IdSeccion);
			$ArtBlog = DMAccesosRapidos_getBlog($IdSeccion);
			$htmlAccRapidos .= $ArtBlog->getDibujar();
			$PromoPubli = DMAccesosRapidos_getPromo($IdSeccion);
			$htmlAccRapidos .=$PromoPubli;
			$ClavesPromo = DMAccesosRapidos_getClavesPromo($IdSeccion);
			$htmlAccRapidos .=$ClavesPromo;							
			if ($_SESSION["Usuario"][0]->IdPerfil != 2)
			{
				$Prod = DMAccesosRapidos_getArticulos($IdSeccion);
				foreach ($Prod as $value)
				{
					if (!isset($_SESSION["CampaniaCompra"]) || $_SESSION["CampaniaCompra"] == $value->getIdCampania())
					{				
						$htmlAccRapidos .= $value->getDibujar();
						$objResponse->addScript("xajax_CargarAtributosCombos_ACC(0,". $value->getIdProducto() .");");	
					}			
				}	
			}			
			$objResponse->addAssign("lstAccesosRapidos","innerHTML", $htmlAccRapidos);			
		}
		catch(exception $e)
		{
			$objResponse->addAlert("CargarCampanias - ERROR \n\n" . $e->getMessage());
		}				
		return $objResponse;
	}
	//VALIDA SI EL USUARIO ESTA LOGUEDO
	function VUsuario()
	{
		$objResponse = new xajaxResponse("ISO-8859-1");		
		try
		{
			if (!isset($_SESSION["Usuario"]) || !$_SESSION["Usuario"][0]->Provisoria == 0 && !$_SESSION["Usuario"][0]->Provisoria == 2 )
			{
				$objResponse->addRedirect("index.php");
			}
			else
			{
				$objUsuario = new UsuariosLogs();
				$objUsuario = DMUsuario_getUltimoHistorico($_SESSION["Usuario"][0]->IdUsuario);
				if (!$objUsuario->UsuarioExpirado())
				{
					$objUsuario->setTimeIN(time());
					DMUsuario_ActualizarHistorico($objUsuario);
				}
				else
				{
					$objUsuario->setTimeOUT(time());
					DMUsuario_FinalizarHistorico($objUsuario);
					$objResponse->addRedirect("login.php");
				}
			}
		}
		catch (exception $e)
		{
			$objResponse->addAlert("VUsuario - ERROR\n\n" . $e->getMessage());
		}
		return $objResponse;
	}
	function PreCompra_ACC($frm, $IdCampania)
	{
		$objResponse = new xajaxResponse("ISO-8859-1");
		try
		{	
			try
			{
				$Prod = unserialize($_SESSION["Seleccion_ACC"][0]);
				$producto =$Prod->getPPMAXStock();
				if(!DMEsCatalogo($IdCampania))
					DMArticulos_PreCompraComprobarStock($producto->getIdCodigoProdInterno(), $frm["cbx_accCantidad".$Prod->getIdProducto()]);
				if(!isset($_SESSION["micarrito"]))
				{
					session_register("micarrito");
					session_register("tiempoCompra");
					session_register("CampaniaCompra");
					$_SESSION["CampaniaCompra"] = $IdCampania;
					$_SESSION["tiempoCompra"] = time() + (15*60); 
					$_SESSION["micarrito"]=array();			
				}
				$_SESSION["micarrito"][$producto->getIdCodigoProdInterno()] = array("IdCodigoProdInterno" => $producto->getIdCodigoProdInterno(), "Cantidad" => $frm["cbx_accCantidad".$Prod->getIdProducto()], "Precio" => $producto->getPrecio($Prod->getPVenta()), "PVenta" => $Prod->getPVenta(), "PCompra" => $Prod->getPCompra(), "Peso" => $Prod->getPeso(), "Volumen" => $Prod->getVolumen());
				unset($_SESSION["Seleccion_ACC"]);
				$objResponse->redirect("micarrito.php");
			}
			catch(exception $e)
			{
				$objResponse->addScript("objCortina.showAlert('".$e->getMessage()."', '');");
			}
		}
		catch (exception $e)
		{
			$objResponse->addAlert("PreCompra - ERROR\n\n" . $e->getMessage());
		}
		return $objResponse;
	}
function CargarAtributosCombos_ACC($Pos, $IdProducto, $valor = -1)
	{
		$objResponse = new xajaxResponse("ISO-8859-1");
		try
		{
			$Prod = DMArticulos_Producto($IdProducto);
			$P = $Prod->getProductos(0);					
			if($valor == -1)
			{
				for($i=$Pos ; $i<count($P->getPP(0)->getAllAtributos()) ; $i++)
				{
					$objResponse->addClear("cbx_acc" . $P->getPP(0)->getAtributos($i)->getIdAtributoClase(), "innerHTML");
					unset($_SESSION["FP_ACC"][$i]);
				}
				$Atributos = $P->getAtributosById($P->getPP(0)->getAtributos($Pos)->getIdAtributoClase());
				$html .="document.getElementById('PrecioProducto_ACC').innerHTML='Precio: ".$P->getPVenta()." &euro;';
				var op = document.createElement('option');
				op.setAttribute('value', '-1');
				op.appendChild(document.createTextNode('...Seleccionar...'));
				document.getElementById('cbx_acc".$IdProducto.$P->getPP(0)->getAtributos($Pos)->getIdAtributoClase()."').appendChild(op);";	
				
				foreach ($Atributos as $value)
				{
					$html .="var op = document.createElement('option');
						op.setAttribute('value', '".$value["Valor"]."');
						var innerHTML = '". $value["Valor"]. "';
						op.appendChild(document.createTextNode(innerHTML));	
						document.getElementById('cbx_acc".$IdProducto.$value["IdAtributoClase"]."').appendChild(op);";					
				}
				$objResponse->addScript($html);
			}
			else
			{
				$_SESSION["FP_ACC"][$Pos]=array("IdAtributoClase" => $P->getPP(0)->getAtributos($Pos)->getIdAtributoClase(), "Valor" => $valor);				
				$Pos++;				
				if($Pos < count($P->getPP(0)->getAllAtributos()))
				{
					for($i=$Pos ; $i<count($P->getPP(0)->getAllAtributos()) ; $i++)
					{
						$objResponse->addClear("cbx_acc".$IdProducto. $P->getPP(0)->getAtributos($i)->getIdAtributoClase(), "innerHTML");
					}
					$Atributos = $P->getAtributosByFiltro($_SESSION["FP_ACC"]);
					$html .="var op = document.createElement('option');
					op.setAttribute('value', '-1');
					op.appendChild(document.createTextNode('...Seleccionar...'));
					document.getElementById('cbx_acc".$IdProducto.$P->getPP(0)->getAtributos($Pos)->getIdAtributoClase()."').appendChild(op);";	
					foreach ($Atributos as $value)
					{
						$html .="var op = document.createElement('option');
							op.setAttribute('value', '".$value["Valor"]."');
							var innerHTML = '".$value["Valor"]."';
							op.appendChild(document.createTextNode(innerHTML));
							
							document.getElementById('cbx_acc".$IdProducto.$value["IdAtributoClase"]."').appendChild(op);
							document.getElementById('cbx_acc".$IdProducto.$value["IdAtributoClase"]."').focus();";
					}
					
					$objResponse->addScript($html);
					$P->getAtributosByFiltro($_SESSION["FP_ACC"]);
				}
				else
				{
					$objP = new productos();
					$objP->setDescripcion($P->getDescripcion());
					$objP->setIdClase($P->getIdClase());
					$objP->setIdMarca($P->getIdMarca());
					$objP->setIdProducto($P->getIdProducto());
					$objP->setNombre($P->getNombre());
					$objP->setPCompra($P->getPCompra());
					$objP->setPVP($P->getPVP());
					$objP->setPVenta($P->getPVenta());
					$objP->setPeso($P->getPeso());
					$objP->setReferencia($P->getReferencia());
					$objP->setVolumen($P->getVolumen());
					foreach ($P->getProductosByFiltro($_SESSION["FP_ACC"]) as $PP)
					{
						$objP->setPP($PP);
					}
					unset($_SESSION["Seleccion_ACC"]);					
					$_SESSION["Seleccion_ACC"][0]= serialize($objP);
					unset($_SESSION["FP_ACC"]);
					$objResponse->addScript("document.getElementById('PrecioProducto_ACC').innerHTML='Precio: ".$objP->getPP(0)->getPrecio($objP->getPVenta())." &euro;';");
					$objResponse->addScript("document.getElementById('cbxCantidad').focus();");
					$objResponse->addScript("document.getElementById('cbx_accCantidad".$IdProducto."').focus();");
				}				
			}
		}
		catch (exception $e)
		{
			$objResponse->addAlert("CargarAtributosCombos - ERROR\n\n" . $e->getMessage());
		}
		return $objResponse;
	}
	function ValidarClavesPromo($Clave,$IdUsuario)
	{
		$objResponse = new xajaxResponse("ISO-8859-1");
		$Clave= md5($Clave);
		$Tabla=DMAccesoClave_ValidarClave($Clave);
		
		if($Tabla)
		{
			if($Tabla[0]->Consumida==0)
			{
				if(DMAccesoClave_ConsumirClave($Clave,$IdUsuario))
				{
					$objResponse->addScript("objCortina.showInfo('La clave fue ingresada con exito','Clave Satisfactoria')");
					$objResponse->addClear("txtClavePromocion","value");
				}
				else
				{
					$objResponse->addScript("objCortina.showError('La clave no pudo ser validada, por favor reingrese la clave','Error de Clave')");
					$objResponse->addClear("txtClavePromocion","value");
				}
			}
			else
			{
				$objResponse->addScript("objCortina.showAlert('La clave ya fue consumida, por favor revise si ingreso la clave correcta','Clave Consumida')");
				$objResponse->addClear("txtClavePromocion","value");				
			}
		}
		else
		{
			$objResponse->addScript("objCortina.showError('La clave no Existe, por favor reingrese la clave','Error de Clave')");
		}
		
	//	$objResponse->addAlert($IdUsuario);
		
		return $objResponse;
	}
?>