<?
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
	class dmRegistro
    {
        
        public static function RegistroBasico(MySQL_registro $objUsuario)
        {          
        	$objUsuario->setClave(Hash::getHash($objUsuario->getClave()));
        	
        	$oConexion = Conexion::nuevo();
            $oConexion->Abrir();
            $oConexion->setQuery($objUsuario->getInsert());
            $oConexion->EjecutarQuery();
            $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
            $ID = $oConexion->DevolverQuery();
            $IdUsuario = $ID[0]["Id"];
            $_SESSION['IdRegistro'] = $ID;
            $objUsuario->getDatos()->setIdUsuario($ID[0]["Id"]);
            $objUsuario->getDirecciones(0)->setIdUsuario($ID[0]["Id"]);
            $objUsuario->getDirecciones(0)->setTipoDireccion(0);
            $objUsuario->getDirecciones(1)->setIdUsuario($ID[0]["Id"]);
            $objUsuario->getDirecciones(1)->setTipoDireccion(1);
            $oConexion->setQuery($objUsuario->getInsertDatosPersonales());
            $oConexion->EjecutarQuery();
            $oConexion->setQuery($objUsuario->getInsertDatosDirecciones(0));
            $oConexion->EjecutarQuery();
            $oConexion->setQuery($objUsuario->getInsertDatosDirecciones(1));
            $oConexion->EjecutarQuery();
		                
            return $IdUsuario;            
        }
        
        public static function updatePadrino($IdUsuario,$Padrino){
        	$oConexion = Conexion::nuevo();
            $oConexion->Abrir();
            $oConexion->setQuery("UPDATE usuarios SET Padrino = \"".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($Padrino)), ENT_QUOTES)."\" WHERE IdUsuario = \"".mysql_real_escape_string($IdUsuario)."\" LIMIT 1");
        	$oConexion->EjecutarQuery();
        }

        
        public static function RegistrarAhiajdo($IdUsuario, $Email, $IdPadrino){
        	
        	$IdUsuario = intval($IdUsuario);
        	$IdPadrino = intval($IdPadrino);
        	//echo($IdUsuario."-".$Email."-".$IdPadrino);
        	
        	if($IdUsuario <= 1 || $IdPadrino <1 || !checkemail($Email)){
        		throw new MySQLException("ERROR EN DATOS INGRESADOS: IdUsuario: ".(($IdUsuario <= 1)?"true":"false")." IdPadrino: ".(($IdPadrino <1)?"true":"false")." NombreUsuario:".((!checkemail($Email))?"true":"false"));
        		return false;
        	}
        	$oConexion = Conexion::nuevo();
        	$oConexion->Abrir();
        	//1) Ver si ya exisiste linea en ahijados
        	$oConexion->setQuery("SELECT COUNT(*) AS C FROM ahijados WHERE IdUsuario = \"".mysql_real_escape_string($IdPadrino)."\" AND Email LIKE '".$Email."'");
			$Existe = $oConexion->DevolverQuery();
			
			if(esCero($Existe[0]["C"]))
			{
			//2) Insertar
	        	$oConexion = Conexion::nuevo();
	            $oConexion->setQuery("SELECT SUM(Aplica1) AS Importe FROM reglasnegocio WHERE IdTipoRegla = 6;");
	            $DataBono = $oConexion->DevolverQuery();
	            $ValorBono = $DataBono[0]["Importe"];
	            
				$oConexion->setQuery("INSERT INTO ahijados (IdUsuario, Email, IdUsuarioReg, Valor) VALUES(".mysql_real_escape_string($IdPadrino).",\"".htmlspecialchars(mysql_real_escape_string($Email), ENT_QUOTES)."\",".mysql_real_escape_string($IdUsuario).", ".mysql_real_escape_string($ValorBono).")");
				$oConexion->EjecutarQuery();
			
			}    	        
        	//3) Actualizar IdUsuarioReg      	        		            
			$oConexion->setQuery("UPDATE ahijados SET IdUsuarioReg = ".mysql_real_escape_string($IdUsuario)." WHERE email = \"".$Email."\"");
		    $oConexion->EjecutarQuery();
		    return true;
		}				
	}
?>