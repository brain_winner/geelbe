<?php
	ob_start();

	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/ws/MovistarWSProxy.php");
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("login"));
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("emails"));
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("registro"));
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados","dm"));	
	

	// Valido el codigo de invitacion contra Movistar
	$proxy = new MovistarWSProxy();
	$resultConsulta = $proxy->sendConsultaMessage($_POST["txtTelefono"]);

	if($resultConsulta == 0){
		echo generarJS("window.parent.ErrorMSJ","Error de registro.", "CODIGO_INVITACION_INCORRECTO");
	} else if($resultConsulta == 1) {
		echo generarJS("window.parent.ErrorMSJ","Error de registro.", "CODIGO_INVITACION_YA_UTILIZADO");
	} else {
		// Registro el usuario
		$registro = new RegistroController();
		$registro->registrarUsuarioMovistar();
	}
    ob_flush();
?>