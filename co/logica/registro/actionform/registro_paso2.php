<?php
	ob_start();
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("login"));
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("emails"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("registro"));
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados","dm"));

	$registro = new RegistroController();
	$registro->registrarPaso2();
    ob_flush();
?>