<?php
class Hash {

	protected static $beginRandomChars = 13;
	protected static $endRandomChars = 19;
	
	protected static $beginPText = "?'\\(/f$<^-";
	protected static $endPText = "+#n0=�)&>%";
	
	public static function getHash($string=""){
		$hash = "";
		$hash = $hash.self::getRandomString(self::$beginRandomChars);
		$hash = $hash.md5(self::$beginPText.$string.self::$endPText);
		$hash = $hash.self::getRandomString(self::$endRandomChars);
		return $hash;
	}
	
	public static function encriptar($valor = "", $key = "") {
		return base64_encode(mcrypt_encrypt(MCRYPT_TRIPLEDES,
				              md5($key),
				              $valor,
				              MCRYPT_MODE_CBC, 
							  $endPText.$beginPText));
	}
	
	public static function desencriptar($valorEncodeado = "", $key = "") {
		return mcrypt_decrypt(MCRYPT_TRIPLEDES, 
					   		  md5($key), 
							  base64_decode($valorEncodeado), 
							  MCRYPT_MODE_CBC, 
							  $endPText.$beginPText);
	}

	public static function compareHash($hash1, $hash2) {
		return (self::removeExtra($hash1)==self::removeExtra($hash2));
	}
	
	public static function removeExtra($hash) {
		return substr($hash, self::$beginRandomChars, - self::$endRandomChars);
	}
	
	public static function getRandomString($size = 3) {
		$string = "";
		for ($i=0; $i<$size; $i++) {
    		if(rand(1,30)%2) {
    			$string = $string.chr(rand(97,122));
    		}
    		else {
    			$string = $string.chr(rand(48,57));
    		}
		} 
		return $string;
	}
	
}

?>
