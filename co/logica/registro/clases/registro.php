<?

$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));

require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";

class urlVariables{

	protected $nombre;

	public function __construct($nombre=""){
		$nombre = str_ireplace(".php","",$nombre);
		$this->nombre = empty($nombre)?"index":$nombre;
	}

	public function sendInvitesUrl() {
		return UrlResolver::getBaseURL("front/confirmacion/index_noinvites.php");
	}
	
	public function step2(){
		return UrlResolver::getBaseURL("front/registro/step2_no_invites.php");
	}

	public function registro(){
		return UrlResolver::getBaseURL("registro/index.html");
	}	
	
	public function link_registro(){
		return UrlResolver::getBaseURL("registro/index.html");
	}

	public function confirmacion(){
		return UrlResolver::getBaseURL("front/confirmacion/".$this->nombre.".php");
	}

	public function confirmacion2(){
		return UrlResolver::getBaseURL("front/confirmacion/confirmacion.php");
	}

	public function activacion(){
		return UrlResolver::getBaseURL("front/login/activar2.php");
	}
}

class RegistroController{

	public function __construct(){
		$this->objUrl = new urlVariables($_POST["urlVariables"]);
	}

	public $objUrl;
	public $estadoInicial = false;
	public $estadoFinal = false;
	public $inputPadrino = false;
	public $objUsuarioPadrino = false;
	public $objUsuarios = false;

	static public function randomPassword($length) {
		
		$string = '';
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		for($i = 0; $i < $length; $i++)
			$string .= substr($chars, rand(0, strlen($chars)-1), 1);
			
		return $string;
		
	}

	public function pre_registro(){
		try{
			Conexion::nuevo()->Abrir_Trans();
			$this->objUsuarios = dmUsuarios::getObjUsuarioById($_GET["email"],"Email");
			$this->objUsuarioPadrino = dmUsuarios::getObjUsuarioById($_GET["codact"],"NombreUsuario");
			//var_dump($this->objUsuarios);
			//var_dump($this->objUsuarioPadrino);
			//exit();
			
			//Todo OK;
			if($this->objUsuarios && $this->objUsuarios->getes_Activa() == 1 && $this->objUsuarios->getPadrino() != "Huerfano"){
				//echo generarJs("window.parent.alert","caso 1: OK!");				
				$this->enviarActivacion();
				redirect($this->objUrl->confirmacion());
			}
			 //Registro 1 Precompleto + padrino NO DEBERIA EJECUTARSE NUNCA ESTE CASO!
			elseif($this->objUsuarios && $this->objUsuarioPadrino && $this->objUsuarios->getPadrino() == "Huerfano" && esCero($this->objUsuarioPadrino->getes_Provisoria())){
				//echo generarJs("window.parent.alert","caso 2: OK!");
				$this->registrarAhijado();
				$this->updatePadrino();
				$this->enviarActivacion();
				redirect($this->objUrl->confirmacion());
			}
			//Registro 1 Precompleto, sin padrino.
			elseif($this->objUsuarios && $this->objUsuarios->getPadrino() == "Huerfano" && (!$this->objUsuarioPadrino || !esCero($this->objUsuarioPadrino->getes_Provisoria()))){
				$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
				redirect($this->objUrl->step2());		
			}
			//Usuario inexistente, codigo de activacion pre-activado
			elseif(!$this->objUsuarios && $this->objUsuarioPadrino && esCero($this->objUsuarioPadrino->getes_Provisoria())){
				?>
				<html>
				<body onLoad="document.pre.submit()">
               		<form action="/<?=Aplicacion::getDirLocal()?>registro/index.html" method="post" id='pre' name='pre'>
				
				    <input type="hidden" name="codact" value="<?=$this->objUsuarioPadrino->getNombreUsuario()?>" />
				    <input type="hidden" name="email" value="<?=(checkemail($_GET["email"])?$_GET["email"]:"")?>" />
				</form>
				</body>
				</html>
				<?

				foreach($_SESSION as $key => $value)
					if(substr($key, 0, 7) == 'bandera')
						unset($_SESSION[$key]);
						
			}
			//Registro normal!
			else{				
				redirect($this->objUrl->registro()."?email=".$_GET["email"]."&codact=".$_GET["codact"]);
			}
			Conexion::nuevo()->Cerrar_Trans();
		}
		catch (MySQLException $e)
		{
			Conexion::nuevo()->RollBack();
			redirect($this->objUrl->registro());			
		}

	}

	public function isRegisteredUser($email) {
		$user = dmUsuarios::ObtenerUsuarioCompleto($email);
		return ($user != null);
	}
	
	public function isActiveUser($email) {
		return dmUsuarios::isUsuarioActivo($email);
	}

	public function isInactiveUser($email) {
		return dmUsuarios::isUsuarioInactivo($email);
	}

	public function isOrphanUser($email) {
		return dmUsuarios::isUsuarioHuerfano($email);
	}

	public function isDesuscriptedUser($email) {
		return dmUsuarios::isUsuarioDesuscripto($email);
	}

	public function updatePassword($objUsuarios, $password) {
		if ($password != "HD25h253jlk6jsjfhhkas9HJKSDFG") {
			$hashPassword = Hash::getHash($password);
		}
		else {
			$hashPassword = "HD25h253jlk6jsjfhhkas9HJKSDFG";
		}
		
		return dmUsuarios::updatePassword($objUsuarios->getIdUsuario(), $hashPassword);
	}

	public function updatePadrino2($objUsuarios, $activationcode) {
		return dmUsuarios::updatePadrino($objUsuarios->getIdUsuario(), $activationcode);
	}

	public function registrarAhijado2($objUsuarios, $activationcode, $idactivationcode) {
		dmRegistro::RegistrarAhiajdo($this->objUsuarios->getIdUsuario(), $activationcode, $idactivationcode);
	}

	public function updateLanding2($objUsuarios, $landingUrl) {
		return dmUsuarios::updateLanding($objUsuarios->getIdUsuario(), $landingUrl);
	}
	
	public function updateUserToHorphan($objUsuarios) {
		return dmUsuarios::updateUserToHorphan($objUsuarios->getIdUsuario());
	}

	public function updateUserToInactive($objUsuarios) {
		return dmUsuarios::updateUserToInactive($objUsuarios->getIdUsuario());
	}

	public function isValidCode($activationcode) {
		if ($activationcode!=null || $activationcode!="") {
			return dmUsuarios::isValidCode($activationcode);
		}
		else {
			return false;
		}
	}
	
	
	// Datos: email, password, firstname, lastname, gender, birthdayday, birthdaymonth, birthdayyear
	public function insertUser($email, $password, $firstname, $lastname, $gender, $birthdayday, $birthdaymonth, $birthdayyear) {
		if ($password != "HD25h253jlk6jsjfhhkas9HJKSDFG") {
			$hashPassword = Hash::getHash($password);
		}
		else {
			$hashPassword = "HD25h253jlk6jsjfhhkas9HJKSDFG";
		}
		return dmUsuarios::insertUser($email, $hashPassword, $firstname, $lastname, $gender, $birthdayday, $birthdaymonth, $birthdayyear);
	}
	public function coregistrar(){
		try{
			$password = $_REQUEST["password"];
			if (isset($_REQUEST["haspassword"]) && $_REQUEST["haspassword"] == "false") {
  				$password = "HD25h253jlk6jsjfhhkas9HJKSDFG";
  			}
			// Verifico si el usuario ya existe.
			if ($this->isRegisteredUser($_REQUEST["email"])) {
				$obj = dmUsuarios::ObtenerUsuarioCompleto($_REQUEST["email"]);
				$_POST['IdUsuario'] = $obj->getIdUsuario();
				
				// Usuario ya existente.
				if ($this->isActiveUser($_REQUEST["email"])) {
					echo "false";
					exit;
				}
				elseif ($this->isInactiveUser($_REQUEST["email"])) {
					// Usuario inactivo.
					$this->objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($_REQUEST["email"]);
					$this->updatePassword($this->objUsuarios, $password);
					if ($this->isValidCode($_REQUEST["activationcode"])) {
						$this->updatePadrino2($this->objUsuarios, $_REQUEST["activationcode"]);
					}
					else {
						$this->updatePadrino2($this->objUsuarios, "default1");
					}
					$this->enviarActivacion();
					echo "true";
					exit;
				}
				elseif ($this->isOrphanUser($_REQUEST["email"])) {
					// Usuario huerfano.
					$this->objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($_REQUEST["email"]);
					$this->updateUserToInactive($this->objUsuarios);
					$this->updatePassword($this->objUsuarios, $password);
					if ($this->isValidCode($_REQUEST["activationcode"])) {
						$this->updatePadrino2($this->objUsuarios, $_REQUEST["activationcode"]);
					}
					else {
						$this->updatePadrino2($this->objUsuarios, "default1");
					}
					$this->enviarActivacion();
					echo "true";
					exit;
				}
				elseif ($this->isDesuscriptedUser($_REQUEST["email"])) {
					// Usuario desuscripto.
					$this->objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($_REQUEST["email"]);
					$this->updateUserToInactive($this->objUsuarios);
					$this->updatePassword($this->objUsuarios, $password);
					if ($this->isValidCode($_REQUEST["activationcode"])) {
						$this->updatePadrino2($this->objUsuarios, $_REQUEST["activationcode"]);
					}
					else {
						$this->updatePadrino2($this->objUsuarios, "default1");
					}
					$this->enviarActivacion();
					echo "true";
					exit;
				}
			}
			else {
				// Usuario inexistente.
				// Datos: email, password, firstname, lastname, gender, birthdayday, birthdaymonth, birthdayyear
				$this->insertUser($_REQUEST["email"], $password, $_REQUEST["firstname"], $_REQUEST["lastname"], $_REQUEST["gender"], $_REQUEST["birthdayday"], $_REQUEST["birthdaymonth"], $_REQUEST["birthdayyear"]);
				
				// Usuario inexistente con codigo.
				$this->objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($_REQUEST["email"]);
				$_POST['IdUsuario'] = (string) $this->objUsuarios->getIdUsuario();
				$this->updateUserToInactive($this->objUsuarios);
				$this->updatePassword($this->objUsuarios, $password);
				if ($this->isValidCode($_REQUEST["activationcode"])) {
					$this->updatePadrino2($this->objUsuarios, $_REQUEST["activationcode"]);
				}
				else {
					$this->updatePadrino2($this->objUsuarios, "default1");
				}
				$this->enviarActivacion();
				echo "true";
				exit;
			}
		}
		catch(Exception $e){
			echo "false";
			exit;
		}

	}
	
	public function coregistrarUsuarioFacebook($userData = null, $codActFacebook = "appfacebook-registro1", $activacion_auto = false){
		try{
  			$password = "HD25h253jlk6jsjfhhkas9HJKSDFG";
  			$respuesta = array("usuario_existente" => true, "id_usuario" => null);
  			
  			// Verifico si el usuario ya existe.
			if ($this->isRegisteredUser($userData->email)) {
				$obj = dmUsuarios::ObtenerUsuarioCompleto($userData->email);
				$IdUsuario = $obj->getIdUsuario();
				
				// Usuario ya existente.
				if ($this->isActiveUser($userData->email)) {
					$respuesta["id_usuario"] = $IdUsuario;
					return $respuesta;
				}
				elseif ($this->isInactiveUser($userData->email)) {
					// Usuario inactivo.
					$this->objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($userData->email);
					$this->updatePassword($this->objUsuarios, $password);
					if ($this->isValidCode($codActFacebook)) {
						$this->updatePadrino2($this->objUsuarios, $codActFacebook);
					}
					else {
						$this->updatePadrino2($this->objUsuarios, "default1");
					}
					$this->realizarActivacionUsuario($IdUsuario, $codActFacebook);
					$respuesta["id_usuario"] = $IdUsuario;
					return $respuesta;
				}
				elseif ($this->isOrphanUser($userData->email)) {
					// Usuario huerfano.
					$this->objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($userData->email);
					$this->updateUserToInactive($this->objUsuarios);
					$this->updatePassword($this->objUsuarios, $password);
					if ($this->isValidCode($codActFacebook)) {
						$this->updatePadrino2($this->objUsuarios, $codActFacebook);
					}
					else {
						$this->updatePadrino2($this->objUsuarios, "default1");
					}
					$this->realizarActivacionUsuario($IdUsuario, $codActFacebook);
					$respuesta["id_usuario"] = $IdUsuario;
					return $respuesta;
				}
				elseif ($this->isDesuscriptedUser($userData->email)) {
					// Usuario desuscripto.
					$this->objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($userData->email);
					$this->updateUserToInactive($this->objUsuarios);
					$this->updatePassword($this->objUsuarios, $password);
					if ($this->isValidCode($codActFacebook)) {
						$this->updatePadrino2($this->objUsuarios, $codActFacebook);
					}
					else {
						$this->updatePadrino2($this->objUsuarios, "default1");
					}
					$this->realizarActivacionUsuario($IdUsuario, $codActFacebook);
					$respuesta["id_usuario"] = $IdUsuario;
					return $respuesta;
				}
			}
			else {
				$nombre = null;
				$apellido = null;
				$anioNacimiento = null;
				$mesNacmimiento = null;
				$diaNacimiento = null;
				$genero = null;
				
			    if ($userData->first_name != null && $userData->first_name != "") {
			    	$nombre = utf8_decode($userData->first_name);
			    }
			    if ($userData->last_name != null && $userData->last_name != "") {
			    	$apellido = utf8_decode($userData->last_name);
			    }
			    if ($userData->birthday != null && $userData->birthday != "" && $userData->birthday != "0000-00-00") {
					$pieces = explode("/", $userData->birthday);
					$diaNacimiento = $pieces[1];
					$mesNacmimiento = $pieces[0];
					$anioNacimiento = $pieces[2];
			    }
			    if ($userData->gender != null && $userData->gender != "") {
			    	if($userData->gender == "male") {
				    	$genero = 1;
			    	} else {
				    	$genero = 0;
			    	}
			    }
				
				// Usuario inexistente.
				// Datos: email, password, firstname, lastname, gender, birthdayday, birthdaymonth, birthdayyear
				$this->insertUser($userData->email, $password, $nombre, $apellido, $genero, $diaNacimiento, $mesNacmimiento, $anioNacimiento);
				
				// Usuario inexistente con codigo.
				$this->objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($userData->email);
				$IdUsuario = $this->objUsuarios->getIdUsuario();
				$this->updateUserToInactive($this->objUsuarios);
				$this->updatePassword($this->objUsuarios, $password);
				if ($this->isValidCode($codActFacebook)) {
					$this->updatePadrino2($this->objUsuarios, $codActFacebook);
				}
				else {
					$this->updatePadrino2($this->objUsuarios, "default1");
				}
				
				if($activacion_auto) {
					$oConexion = Conexion::nuevo();
					$oConexion->Abrir_Trans();
					$oConexion->setQuery("UPDATE usuarios SET es_activa = 0 WHERE IdUsuario = ".mysql_real_escape_string($IdUsuario));
					$a = $oConexion->EjecutarQuery();
					$oConexion->Cerrar_Trans();	
				} else {
					$this->realizarActivacionUsuario($IdUsuario, $codActFacebook);
				}
				
				$oConexion = Conexion::nuevo();
           		$oConexion->setQuery("INSERT INTO reglas_aplicadas (IdUsuario, Fecha, IdRegla) VALUES ($IdUsuario, NOW(), 1)");
           		$oConexion->EjecutarQuery();
           		$id = mysql_insert_id();
           		$oConexion->setQuery("INSERT INTO bonos_x_usuarios (IdReglaAplicada, IdUsuario, Fecha, IdEstadoBono) VALUES ($id, $IdUsuario, NOW(), 0)");
                $oConexion->EjecutarQuery();
				
				$respuesta["usuario_existente"] = false;
				$respuesta["id_usuario"] = $IdUsuario;
				return $respuesta;
			}
		}
		catch(Exception $e){
			return null;
		}

	}
	
	public function registrarV2(){
		try{
				
			// Verifico si el usuario ya existe.
			if ($this->isRegisteredUser($_POST["email"])) {
				// Usuario ya existente.
				if ($this->isActiveUser($_POST["email"])) {
					// Usuario activo.
					$objUsuarios = dmUsuarios::Login($_POST["email"], $_POST["password"]);
					if($objUsuarios === false){
						$uri="/".Aplicacion::getDirLocal()."front/login/index.php";
						header("Location: $uri");
						exit;
					}
					else {
						$login = new LoginController();
						$login->setobjUsuarios($objUsuarios);
						$login->guardarHistorico();
						Aplicacion::loginUser($objUsuarios);
						$login->setCookie();
						$uri="/".Aplicacion::getDirLocal()."front/vidriera/index.php";
						header("Location: $uri");
						exit;
					}
				}
				elseif ($this->isInactiveUser($_POST["email"])) {
					// Usuario inactivo.
					$this->objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($_POST["email"]);
					$this->updatePassword($this->objUsuarios, $_POST["password"]);
					if ($this->isValidCode($_POST["activationcode"])) {
						$this->updatePadrino2($this->objUsuarios, $_POST["activationcode"]);
						$this->updateLanding2($this->objUsuarios, $_POST["landingtype"]);
//						$IdActivationCode = dmUsuario::getIdByNombreUsuario($_POST["activationcode"]);
//						$this->registrarAhijado2($this->objUsuarios, $_POST["activationcode"], $IdActivationCode);
						$this->enviarActivacion();
						$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
						$_SESSION['inviteEmail'] = $_POST["email"];
						$uri="/".Aplicacion::getDirLocal()."front/confirmacion/index_noinvites.php";
						header("Location: $uri");
					}
					else {
						if (isset($_POST["landingtype"]) && ($_POST["landingtype"]=="registro/index.html" || $_POST["landingtype"]=="registro/index.php" || $_POST["landingtype"]=="registro/")) {
							$this->updateUserToHorphan($this->objUsuarios);
							$this->updateLanding2($this->objUsuarios, $_POST["landingtype"]);
							$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
							$_SESSION['inviteEmail'] = $_POST["email"];
							$uri="/".Aplicacion::getDirLocal()."front/registro/step2_no_invites.php";
							header("Location: $uri");
						}
						else {
							$this->updatePadrino2($this->objUsuarios, "default1");
							$this->updateLanding2($this->objUsuarios, $_POST["landingtype"]);
							$this->enviarActivacion();
							$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
							$_SESSION['inviteEmail'] = $_POST["email"];
							$uri="/".Aplicacion::getDirLocal()."front/confirmacion/index_noinvites.php";
							header("Location: $uri");
						}
					}
					exit;
				}
				elseif ($this->isOrphanUser($_POST["email"])) {
					// Usuario huerfano.
					$this->objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($_POST["email"]);
					$this->updateUserToInactive($this->objUsuarios);
					$this->updatePassword($this->objUsuarios, $_POST["password"]);
					if ($this->isValidCode($_POST["activationcode"])) {
						$this->updatePadrino2($this->objUsuarios, $_POST["activationcode"]);
						$this->updateLanding2($this->objUsuarios, $_POST["landingtype"]);
//						$IdActivationCode = dmUsuario::getIdByNombreUsuario($_POST["activationcode"]);
//						$this->registrarAhijado2($this->objUsuarios, $_POST["activationcode"], $IdActivationCode);
						$this->enviarActivacion();
						$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
						$_SESSION['inviteEmail'] = $_POST["email"];
						$uri="/".Aplicacion::getDirLocal()."front/confirmacion/index_noinvites.php";
						header("Location: $uri");
					}
					else {
						if (isset($_POST["landingtype"]) && ($_POST["landingtype"]=="registro/index.html" || $_POST["landingtype"]=="registro/index.php" || $_POST["landingtype"]=="registro/")) {
							$this->updateUserToHorphan($this->objUsuarios);
							$this->updateLanding2($this->objUsuarios, $_POST["landingtype"]);
							$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
							$_SESSION['inviteEmail'] = $_POST["email"];
							$uri="/".Aplicacion::getDirLocal()."front/registro/step2_no_invites.php";
							header("Location: $uri");
						}
						else {
							$this->updatePadrino2($this->objUsuarios, "default1");
							$this->updateLanding2($this->objUsuarios, $_POST["landingtype"]);
							$this->enviarActivacion();
							$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
							$_SESSION['inviteEmail'] = $_POST["email"];
							$uri="/".Aplicacion::getDirLocal()."front/confirmacion/index_noinvites.php";
							header("Location: $uri");
						}
					}
					exit;
				}
				elseif ($this->isDesuscriptedUser($_POST["email"])) {
					// Usuario desuscripto.
					$this->objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($_POST["email"]);
					$this->updateUserToInactive($this->objUsuarios);
					$this->updatePassword($this->objUsuarios, $_POST["password"]);
					if ($this->isValidCode($_POST["activationcode"])) {
						$this->updatePadrino2($this->objUsuarios, $_POST["activationcode"]);
						$this->updateLanding2($this->objUsuarios, $_POST["landingtype"]);
						$this->enviarActivacion();
						$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
						$_SESSION['inviteEmail'] = $_POST["email"];
						$uri="/".Aplicacion::getDirLocal()."front/confirmacion/index_noinvites.php";
						header("Location: $uri");
					}
					else {
						if (isset($_POST["landingtype"]) && $_POST["landingtype"]=="registro/index.html") {
							$this->updateUserToHorphan($this->objUsuarios);
							$this->updateLanding2($this->objUsuarios, $_POST["landingtype"]);
							$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
							$_SESSION['inviteEmail'] = $_POST["email"];
							$uri="/".Aplicacion::getDirLocal()."front/registro/step2_no_invites.php";
							header("Location: $uri");
						}
						else {
							$this->updatePadrino2($this->objUsuarios, "default1");
							$this->updateLanding2($this->objUsuarios, $_POST["landingtype"]);
							$this->enviarActivacion();
							$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
							$_SESSION['inviteEmail'] = $_POST["email"];
							$uri="/".Aplicacion::getDirLocal()."front/confirmacion/index_noinvites.php";
							header("Location: $uri");
						}
					}
					exit;
				}
			}
			else {
				// Usuario inexistente.
				// Datos: email, password, firstname, lastname, gender, birthdayday, birthdaymonth, birthdayyear
				$this->insertUser($_POST["email"], $_POST["password"], $_POST["firstname"], $_POST["lastname"], $_POST["gender"], $_POST["birthdayday"], $_POST["birthdaymonth"], $_POST["birthdayyear"]);
				// Usuario inexistente con codigo.
				$this->objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($_POST["email"]);
				$this->updateLanding2($this->objUsuarios, $_POST["landingtype"]);
				$this->updateUserToInactive($this->objUsuarios);
				$this->updatePassword($this->objUsuarios, $_POST["password"]);
				if ($this->isValidCode($_POST["activationcode"])) {
					$this->updatePadrino2($this->objUsuarios, $_POST["activationcode"]);
//					$IdActivationCode = dmUsuario::getIdByNombreUsuario($_POST["activationcode"]);
//					$this->registrarAhijado2($this->objUsuarios, $_POST["activationcode"], $IdActivationCode);
					$_POST['IdUsuario'] = $this->objUsuarios->getIdUsuario();
					$activado = $this->enviarActivacion();
					$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
					$_SESSION['inviteEmail'] = $_POST["email"];
					if($activado == 2) {
  					    Aplicacion::CargarIncludes(Aplicacion::getIncludes("login"));
						$login = new LoginController();
						$login->loguearByUserPass($_POST["email"], $_POST["password"]);
						$NewHash = Hash::getHash("5TvUmtcN".$this->objUsuarios->getIdUsuario().$_POST['email']."x2cPMuoW");
						setCookie("gavc",$NewHash,time() + 3600*24*365, "/");
						$uri="/".Aplicacion::getDirLocal()."front/vidriera/index.php";
					} else
						$uri="/".Aplicacion::getDirLocal()."front/confirmacion/index_noinvites.php";
					
					header("Location: $uri");
				}
				else {
				    if (isset($_POST["landingtype"]) && ($_POST["landingtype"]=="registro/index.html" || $_POST["landingtype"]=="registro/index.php" || $_POST["landingtype"]=="registro/")) {
						$this->updateUserToHorphan($this->objUsuarios);
						$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
						$_SESSION['inviteEmail'] = $_POST["email"];
						$uri="/".Aplicacion::getDirLocal()."front/registro/step2_no_invites.php";
						header("Location: $uri");
					}
					else {
						$this->updatePadrino2($this->objUsuarios, "default1");
						$this->updateLanding2($this->objUsuarios, $_POST["landingtype"]);
						$this->enviarActivacion();
						$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
						$_SESSION['inviteEmail'] = $_POST["email"];
						$uri="/".Aplicacion::getDirLocal()."front/confirmacion/index_noinvites.php";
						header("Location: $uri");
					}
				}
				exit;
			}
		}
		catch(Exception $e){
			$uri="/".Aplicacion::getDirLocal()."registro/index.html";
			header("Location: $uri");
			exit;
		}

	}

	protected function setEstadoInicial(){
		$_SESSION['inviteEmail'] = $_POST["txtEmail"];
		$_SESSION['inviteFName'] = $_POST["txtNombre"];
		$_SESSION['inviteLName'] = $_POST["txtApellido"];
		$this->estadoInicial = dmReferenciados::estadoInicial($_POST["txtEmail"]);
		
		$this->estadoFinal = $this->estadoInicial;

		if(!empty($this->estadoInicial["IdUsuario"])) $_SESSION['IdRegistro'][0]["Id"] = $this->estadoInicial["IdUsuario"];

		$this->objUsuarios = dmUsuarios::getObjUsuarioById($_POST["txtEmail"],"NombreUsuario");
		
		if(!empty($_POST["txtPadrino"])){
			$this->objUsuarioPadrino = dmUsuarios::getObjUsuarioById($_POST["txtPadrino"],"NombreUsuario");
		}
		
		if((!$this->objUsuarioPadrino || $this->objUsuarioPadrino->getes_Provisoria() == 1) && !empty($this->estadoInicial["Padrino"])){
			$this->objUsuarioPadrino = dmUsuarios::getObjUsuarioById($this->estadoInicial["Padrino"],"NombreUsuario");
		}
					
		//var_dump($this->objUsuarios);
		
		//var_dump($this->objUsuarioPadrino);
		
		
		if($this->objUsuarioPadrino && esCero($this->objUsuarioPadrino->getes_Provisoria())){
			$this->estadoFinal["Padrino"] = $this->objUsuarioPadrino->getNombreUsuario();
			$this->estadoFinal["IdPadrino"] = $this->objUsuarioPadrino->getIdUsuario();
		}

		if(!$this->estadoFinal["IdPadrino"] || empty($this->estadoFinal["IdPadrino"])){
			$this->estadoFinal["IdPadrino"] = 1;
			$this->estadoFinal["Padrino"] = "Huerfano";
			$this->objUsuarioPadrino = dmUsuarios::getObjUsuarioById(1);
		}
	}

	public function cargarUsuarios($Usuario, $Padrino){
		$this->objUsuarioPadrino = dmUsuarios::getObjUsuarioById($Padrino,"NombreUsuario");
		$this->objUsuarios = dmUsuarios::getObjUsuarioById($Usuario);
		//var_dump($this->objUsuarios);
		//var_dump($this->objUsuarioPadrino);
		return ($this->objUsuarios && $this->objUsuarioPadrino);
	}
	
	public function registrar(){
		try{
			Conexion::nuevo()->Abrir_Trans();
			$this->setEstadoInicial();

			//Caso 1 Usuario Inexistente sin padrino
			if(empty($this->estadoInicial["IdUsuario"]) && !$this->objUsuarios && $this->estadoFinal["IdPadrino"] <= 1){
				//echo generarJs("window.parent.alert","caso 1");
				$this->registroBasico();
				$this->registrarAhijado();
				echo generarJS("window.parent.Redirect",$this->objUrl->step2(),"_self");
			}

			//Caso 2 Usuario Inexistente con Invitacion
			elseif(empty($this->estadoInicial["IdUsuario"]) && !$this->objUsuarios && $this->estadoFinal["IdPadrino"] >1){
				//echo generarJs("window.parent.alert","caso 2");
				$this->registroBasico();
				$this->registrarAhijado();
				$this->enviarActivacion();
				$_SESSION['afterInvites'] = $this->objUrl->confirmacion();
				echo generarJS("window.parent.Redirigir",$this->objUrl->sendInvitesUrl(),"_self");
			}

			//Caso 3 Usuario Huerfano
			elseif($this->objUsuarios && $this->objUsuarios->getes_Activa() == 1 && $this->estadoFinal["IdPadrino"] == 1){
				//Redirect Paso2.
				$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
				//echo generarJs("window.parent.alert","caso 3");
				echo generarJS("window.parent.Redirect",$this->objUrl->step2(),"_self");
			}

			//Caso 4 Usuario Huerfano con padrino en hidden
			elseif($this->estadoInicial["IdPadrino"] == 1 && $this->estadoFinal["IdPadrino"] > 1){
				//echo generarJs("window.parent.alert","caso 4");
				$this->registrarAhijado();
				$this->updatePadrino();
				$this->enviarActivacion();
				$_SESSION['afterInvites'] = $this->objUrl->confirmacion();
				echo generarJS("window.parent.Redirigir",$this->objUrl->sendInvitesUrl(),"_self");
			}

			//Caso 5 Usuario Inactivo
			elseif($this->estadoInicial["es_activa"] == 1 && $this->estadoInicial["IdPadrino"] > 1){
				//echo generarJs("window.parent.alert","caso 5");
				$this->enviarActivacion();
				echo generarJS("window.parent.Redirigir",$this->objUrl->confirmacion(),"_self");
			}

			//Caso 6 Usuario Activo
			elseif(esCero($this->estadoInicial["es_activa"]) || ($this->objUsuarios && esCero(($this->objUsuarios->getes_Activa())))){
				//echo generarJs("window.parent.alert","caso 6");
				echo generarJS("window.parent.ErrorMSJ","Error de registro.", "USUARIO_EXISTENTE");
			}
			else{
				//echo generarJs("window.parent.alert","caso contrario");
				echo generarJS("window.parent.ErrorMSJ","Error de registro.", "0");
			}
			Conexion::nuevo()->Cerrar_Trans();
		}catch(MySQLException $e){
			//echo "<pre>";
			//var_dump($e);
			//echo "</pre>";
			echo generarJS("window.parent.ErrorMSJ","Error de registro.", "0");
			Conexion::nuevo()->RollBack();
		}

	}
	
	public function registrarPaso2(){
		try{
			Conexion::nuevo()->Abrir_Trans();
			$this->cargarUsuarios($_POST["IdUsuario"],$_POST["txtPadrino"]);
			
			if($_POST["hash"] != generar_hash_md5($_POST["IdUsuario"])){
				//echo generarJs("window.parent.alert","caso 1");
				echo generarJS("window.parent.ErrorMSJ","Error de registro.", "ERROR_EN_HASH");
			}
			elseif(!$this->objUsuarios){
				//echo generarJs("window.parent.alert","caso 2");
				echo generarJS("window.parent.ErrorMSJ","Error de registro.", "USUARIO_INEXISTENTE");
			}
			elseif(!$this->objUsuarioPadrino || $_POST["txtPadrino"] == "Huerfano"){
				//echo generarJs("window.parent.alert","caso 3");
				echo generarJS("window.parent.ErrorMSJ","Error de registro.", "PADRINO_INCORRECTO");
			}
			elseif(esCero($this->objUsuarios->getes_activa()) && $this->objUsuarios->getPadrino() != "Huerfano"){
				//echo generarJs("window.parent.alert","caso 4");
				echo generarJS("window.parent.msjError", "Su cuenta ya esta activada, Por favor realice un <a href='/".Aplicacion::getDirLocal()."front/login/index.php' class='aqui'>click aqu�</a> para ingresar a Geelbe","Su cuenta ya esta activa");
			}
			elseif($this->objUsuarios->getPadrino() != "Huerfano"){
				//echo generarJs("window.parent.alert","caso 5: No es huerfano");
				$sent = $this->enviarActivacion();
				$_SESSION['afterInvites'] = $this->objUrl->confirmacion();
				
				if($sent == 1)
					echo generarJS("window.parent.Redirigir",$this->objUrl->sendInvitesUrl(),"_self");
				else
					echo generarJS("window.parent.Redirigir",$this->objUrl->confirmacion2(),"_self");
			}
			elseif(esCero($this->objUsuarioPadrino->getes_Provisoria())){
				//echo generarJs("window.parent.alert","caso 6: OK!");
				$this->registrarAhijado();
				$this->updatePadrino();
				$sent = $this->enviarActivacion();
				$_SESSION['afterInvites'] = $this->objUrl->confirmacion();
				
				if($sent == 1)
					echo generarJS("window.parent.Redirigir",$this->objUrl->sendInvitesUrl(),"_self");
				else
					echo generarJS("window.parent.Redirigir",$this->objUrl->confirmacion2(),"_self");

				
			}else{
				//echo generarJs("window.parent.alert","caso 7");
				echo generarJS("window.parent.ErrorMSJ","Error de registro.", "PADRINO_INCORRECTO");
			}
			Conexion::nuevo()->Cerrar_Trans();
		}catch(MySQLException $e){
			//echo generarJs("window.parent.alert","caso 8");
			echo generarJS("window.parent.ErrorMSJ","Error de registro.", "0");
			Conexion::nuevo()->RollBack();
		}
	}	

	/**
	 * Registro basico (tablas: usuarios, dup, dud)
	 *
	 * @param MySql_registro opcional.
	 */
	protected function registroBasico($objUsuario=false){
		if(!$objUsuario){
			$objUsuario = new MySQL_registro();
			$objUsuario->setNombreUsuario($_POST["txtEmail"]);
			$objUsuario->setClave($_POST["txtClave"]);
			$objUsuario->setes_Activa(1);
			$objUsuario->setIdPerfil(1);
			$objUsuario->setes_Provisoria(1);
			$objUsuario->setFechaIngreso(date("Y-m-d H:i:s"));
			$objDatos = new DatosUsuariosPersonales();
			$objDatos->setApellido(addslashes(ucwords(strtolower($_POST["txtApellido"]))));
			$objDatos->setIdTratamiento($_POST["cbxIdTratamiento"]);
			$objDatos->setNombre(addslashes(ucwords(strtolower($_POST["txtNombre"]))));
			$objDatos->setIdTipoUsuario(1);
			$objDatos->setes_EnviarDatosCorreo(0);
			$objDatos->setFechaNacimiento($_POST["cmb_nacimientoAnio"]."-".$_POST["cmb_nacimientoMes"]."-".$_POST["cmb_nacimientoDia"] );
			$objDatos->esCompleto();
			$objUsuario->setDatos($objDatos);
			$objDireccion = new DatosUsuariosDirecciones();
			$objUsuario->setDirecciones(0, $objDireccion);
			$objDireccion = new DatosUsuariosDirecciones();
			$objUsuario->setDirecciones(1, $objDireccion);
			$objUsuario->setPadrino($this->estadoFinal["Padrino"]);
		}
		$this->objUsuarios = dmUsuarios::getObjUsuarioById(dmRegistro::RegistroBasico($objUsuario),"IdUsuario");
	}


	public function enviarActivacion() {
		
		if(isset($_REQUEST["activationcode"]))
			$_POST['txtPadrino'] = $_REQUEST["activationcode"];
		
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir_Trans();
		$oConexion->setQuery("select b.Auto from partners_codigos_base b LEFT JOIN partners_codigos p ON b.idCodigoBase = p.idCodigoBase WHERE p.Codigo = '".mysql_real_escape_string($_POST['txtPadrino'])."'");
		$Fila = $oConexion->DevolverQuery();
		$oConexion->Cerrar_Trans();

		if($Fila[0]['Auto'] == 1) {
			$oConexion = Conexion::nuevo();
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("UPDATE usuarios SET es_activa = 0 WHERE IdUsuario = ".mysql_real_escape_string($_POST['IdUsuario']));
			$a = $oConexion->EjecutarQuery();
			$oConexion->Cerrar_Trans();		
			
					
			
			return 2;
      			
		} else {

			$emailBuilder = new EmailBuilder();
		   $emailData = $emailBuilder->generateActivationEmail($this->objUsuarios, $this->objUsuarios->getNombreUsuario());
					
			$emailSender = new EmailSender();
			$emailSender->sendEmail($emailData);
			return 1;

		}
	}

	public function realizarActivacionUsuario($IdUsuario = null, $CodActivacion = null) {
		$oConexion = Conexion::nuevo();
		$oConexion->Abrir_Trans();
		$oConexion->setQuery("select b.Auto from partners_codigos_base b LEFT JOIN partners_codigos p ON b.idCodigoBase = p.idCodigoBase WHERE p.Codigo = '".mysql_real_escape_string($CodActivacion)."'");
		$Fila = $oConexion->DevolverQuery();
		$oConexion->Cerrar_Trans();

		if($Fila[0]['Auto'] == 1) {
			
			$oConexion = Conexion::nuevo();
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("UPDATE usuarios SET es_activa = 0 WHERE IdUsuario = ".mysql_real_escape_string($IdUsuario));
			$a = $oConexion->EjecutarQuery();
			$oConexion->Cerrar_Trans();				
			return 2;
      			
		} else {
			$emailBuilder = new EmailBuilder();
		    $emailData = $emailBuilder->generateActivationEmail($this->objUsuarios, $this->objUsuarios->getNombreUsuario());
					
			$emailSender = new EmailSender();
			$emailSender->sendEmail($emailData);
			return 1;

		}
	}
	


	protected function registrarAhijado(){
		//echo $this->objUsuarios->getIdUsuario()."-".$this->objUsuarios->getNombreUsuario()."-".$this->objUsuarioPadrino->getIdUsuario();
		dmRegistro::RegistrarAhiajdo($this->objUsuarios->getIdUsuario(),$this->objUsuarios->getNombreUsuario(),$this->objUsuarioPadrino->getIdUsuario());
	}

	protected function updatePadrino(){
		dmRegistro::updatePadrino($this->objUsuarios->getIdUsuario(),$this->objUsuarioPadrino->getNombreUsuario());
	}

}

class LoginController{

	public function __construct(){
		$this->objUrl = new urlVariables($_POST["urlVariables"]);
	}

	protected $objUsuarios;

	public function setGuardarCookie($recordar){
		$this->guardarCookie = $recordar;
	}

	public function setCookie(){
		if($this->guardarCookie){
			Aplicacion::setLoginCookie($this->objUsuarios->getIdUsuario());
		}
	}


	public function setobjUsuarios($objUsuarios){
		$this->objUsuarios = $objUsuarios;
	}

	public function getobjUsuarios(){
		return $this->objUsuarios;
	}

	public function loguearById($IdUsuario){
		$this->setobjUsuarios(dmUsuarios::getObjUsuarioById($IdUsuario,"IdUsuario"));
		return $this->login();
	}

	public function loguearByUserPass($user,$password){
		$this->setobjUsuarios(dmUsuarios::Login($user,$password));
		return $this->login();
	}

	public function loguearByUserPassPaginasEMS($user,$password){
		$this->setobjUsuarios(dmUsuarios::Login($user,$password));
		return $this->loginPaginasEMS();
	}

	protected function loginPaginasEMS(){

		//Inexistente
		//  Show Error
		if($this->getobjUsuarios() === false){
			echo generarJs("window.parent.ErrorMSJ","Error de login",1000);
		}

		//No Activo Sin Codigo
		//  Redirect Poner Codigo
		elseif($this->getobjUsuarios()->getPadrino() == "Huerfano"){
			$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
			echo generarJS("window.parent.msjError","Su cuenta a�n no se encuentra apadrinada.","Su cuenta no se encuentra apadrinada");
			return false;
		}

		//No Activo con C�digo
		//  Redirect Activa tu cuenta
		elseif($this->getobjUsuarios()->getes_Activa() == 1 && $this->getobjUsuarios()->getPadrino() != "Huerfano"){
			echo generarJS("window.parent.msjError","Su cuenta a�n no se encuentra activada, Por favor realice un <a href='/".Aplicacion::getDirLocal()."front/login/activar2.php' class='aqui'>click aqu�</a> para comenzar el proceso de activaci�n para ser miembro Geelbe","Su e-mail a�n no est� verificado");
			return false;
		}

		elseif($this->getobjUsuarios()->getes_Activa() == 0){
			$this->guardarHistorico();
			Aplicacion::loginUser($this->getobjUsuarios());
			$this->setCookie();
			echo generarJS("window.parent.Redirect","/".Aplicacion::getDirLocal()."front/ems/desuscribir.php","_self");
			return true;
		}else{
			echo generarJS("window.parent.msjError","Error en login","Error en login");
		}
		
		return false;
		
	}
	
	protected function login(){

		//Inexistente
		//  Show Error
		if($this->getobjUsuarios() === false){
			echo generarJs("window.parent.ErrorMSJ","Error de login",1000);
		}

		//No Activo Sin Codigo
		//  Redirect Poner Codigo
		elseif($this->getobjUsuarios()->getPadrino() == "Huerfano"){
			$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
			echo generarJS("window.parent.Redirect",$this->objUrl->step2(),"_self");
			return false;
		}

		//No Activo con C�digo
		//  Redirect Activa tu cuenta
		elseif($this->getobjUsuarios()->getes_Activa() == 1 && $this->getobjUsuarios()->getPadrino() != "Huerfano"){
			echo generarJS("window.parent.msjError","Su cuenta a�n no se encuentra activada, Por favor realice un <a href='/".Aplicacion::getDirLocal()."front/login/activar2.php' class='aqui'>click aqu�</a> para comenzar el proceso de activaci�n para ser miembro Geelbe","Su e-mail a�n no est� verificado");
			return false;
		}

		//Activo 1er Login
		elseif($this->getobjUsuarios()->getes_Provisoria() == 1 && $this->getobjUsuarios()->getes_Activa() == 0){
			$this->guardarHistorico(); //Guardar Hora FALTA!.
			$this->aplicarReglaPrimerLogin();
			Aplicacion::loginUser($this->getobjUsuarios());
			$this->setCookie();
			if ($this->objUsuarios->getDatos()->getNombre()==null || $this->objUsuarios->getDatos()->getApellido()==null) {
				echo generarJS("window.parent.Redirect","/".Aplicacion::getDirLocal()."front/micuenta/mi-perfil.php  ","_self");
			}
			else {
				echo generarJS("window.parent.Redirect","/".Aplicacion::getDirLocal()."front/vidriera/index.php","_self");
			}
			return true;
		}

		//Activo 2do Login
		elseif($this->getobjUsuarios()->getes_Provisoria() == 0 && $this->getobjUsuarios()->getes_Activa() == 0){
			$this->guardarHistorico();
			Aplicacion::loginUser($this->getobjUsuarios());
			$this->setCookie();
			if ($this->objUsuarios->getDatos()->getNombre()==null || $this->objUsuarios->getDatos()->getApellido()==null) {
				echo generarJS("window.parent.Redirect","/".Aplicacion::getDirLocal()."front/micuenta/mi-perfil.php  ","_self");
			}
			else {	
			//Referenciados para usuarios Hotmail x unica vez
			$Email = $_POST["txtUser"];
			$splitEmail = split("@",$Email);
			$emailDomain = $splitEmail[1];
			switch ($emailDomain) {
				case 'hotmail.com': case 'msn.com': case 'live.com': $emailProvider = "hotmail"; break;
			}
			if ($emailProvider == "hotmail"){
				$primerLogin = false;
				
				$oConexion = Conexion::nuevo();
				try {
					$oConexion->setQuery("SELECT hotmail_primer_login FROM usuarios WHERE NombreUsuario='".$Email."'");                
					$result = $oConexion->DevolverQuery();
					$primerLogin = $result[0]["hotmail_primer_login"];
				} catch (MySQLException $e) {
					throw $e;
				}
				if(!$primerLogin) {
					$oConexion = Conexion::nuevo();
					try {
						$oConexion->setQuery("UPDATE usuarios SET hotmail_primer_login = 1 WHERE NombreUsuario='".$Email."'");   	
						$oConexion->EjecutarQuery();
					} catch (MySQLException $e) {
						throw $e;
					}			
					echo generarJS("window.parent.Redirect","/".Aplicacion::getDirLocal()."front/referenciados/index.php","_self");
					exit;
				}
			}
			//Fin
				$campaign = $_POST["auto"];

				if ($campaign != null){
					echo generarJS("window.parent.Redirect","/".Aplicacion::getDirLocal()."logica/autologin/login.php?".$campaign,"_self");
				} else if(isset($_POST['redirectCamp'])) {
					echo generarJS("window.parent.Redirect","/".Aplicacion::getDirLocal()."front/catalogo/index.php?IdCategoria=-1&IdCampania=".$_POST['redirectCamp'],"_self");
				} else {
					echo generarJS("window.parent.Redirect","/".Aplicacion::getDirLocal()."front/vidriera/index.php","_self");
				}
			}
			return true;
		}else{
			echo generarJS("window.parent.msjError","Error en login","Error en login");
		}
		
		return false;
		
		
	}

	public function guardarHistorico(){

		if(empty($this->objUsuarios)){
			return false;
		}

        $oLoginH = new clsLoginH();
        $oLoginH->setApellido(addslashes($this->objUsuarios->getDatos()->getApellido()));
        $oLoginH->setNombre(addslashes($this->objUsuarios->getDatos()->getNombre()));
        $oLoginH->setNombreUsuario($this->objUsuarios->getNombreUsuario());
        $oLoginH->setFecha(date("Y/m/d H:i"));
        dmLoginH::Guardar($oLoginH);
	}

	protected function aplicarReglaPrimerLogin(){
		//verificar que mande mail.
		global $objReglaNegocio;
//	old	$objReglaNegocio->NuevoRegistroUsuario($this->getobjUsuarios()->getIdUsuario());
		$objReglaNegocio->NuevoRegistroUsuario($this->getobjUsuarios()->getIdUsuario(),$this->getobjUsuarios()->getPadrino());
		dmUsuarios::ActivaPrimerLogin($this->getobjUsuarios()->getIdUsuario());
	}
}

class LoginAutomaticoController extends LoginController{
		protected function login(){
		//Inexistente
		//  Show Error
		if($this->getobjUsuarios() === false){
			echo generarJs("window.ErrorMSJ","Error de login",1000);
			return false;
		}

		//No Activo Sin Codigo
		//  Redirect Poner Codigo
		elseif($this->getobjUsuarios()->getPadrino() == "Huerfano"){
			$_SESSION['IdRegistro'][0]["Id"] = $this->objUsuarios->getIdUsuario();
			echo generarJS("window.Redirect",$this->objUrl->step2(),"_self");
			return false;
		}

		//No Activo con C�digo
		//  Redirect Activa tu cuenta
		elseif($this->getobjUsuarios()->getes_Activa() == 1 && $this->getobjUsuarios()->getPadrino() != "Huerfano"){
			//Lo mando a activar su cuenta.
			//Mandar Mail de activacion FALTA!
			echo generarJS("window.msjError","Su cuenta a�n no se encuentra activada, Por favor realice un <a href='/".Aplicacion::getDirLocal()."front/login/activar2.php' class='aqui'>click aqu�</a> para comenzar el proceso de activaci�n para ser miembro Geelbe","Su e-mail a�n no est� verificado");
			return false;
		}

		//Activo 1er Login
		elseif($this->getobjUsuarios()->getes_Provisoria() == 1 && $this->getobjUsuarios()->getes_Activa() == 0){
			$this->guardarHistorico(); //Guardar Hora FALTA!.
			$this->aplicarReglaPrimerLogin();
			Aplicacion::loginUser($this->getobjUsuarios());
			$this->setCookie();
			echo generarJS("window.Redirect","/".Aplicacion::getDirLocal()."front/vidriera/index.php","_self");
			return false;
		}

		//Activo 2do Login
		elseif($this->getobjUsuarios()->getes_Provisoria() == 0 && $this->getobjUsuarios()->getes_Activa() == 0){
			$this->guardarHistorico();
			Aplicacion::loginUser($this->getobjUsuarios());
			$this->setCookie();
			return true;
		}else{
			echo generarJS("window.msjError","Error en login","Error en login");
			return false;
		}
	}
}
?>
