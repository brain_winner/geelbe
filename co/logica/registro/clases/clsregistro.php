<?php
    class MySQL_registro extends usuarios
    {
        public function getInsert()
        {
            $strCampos="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad != "_secciones")
                {
                    if($propiedad != "_idusuario")
                    {   
                        $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                        $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    }
                }                
            }
            return "INSERT INTO usuarios (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).")";
        }
        public function getInsertDatosPersonales()
        {
            $strCampos="";
            $strValores="";
            $Me = $this->getDatos()->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
            }
            return "INSERT INTO datosusuariospersonales (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).")";
        }
        public function getInsertDatosDirecciones($index)
        {
            $strCampos="";
            $strValores="";
            $Me = $this->getDirecciones($index)->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                if($propiedad == "_idpais" || $propiedad == "_idprovincia")
                {
                    $strValores .=($valor == 0 || $valor == "")?"NULL, ":chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
                else
                {
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
            }
            return "INSERT INTO datosusuariosdirecciones (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).")";
        }
        public function getInsertReferenciador()
        {
	        $oConexion = Conexion::nuevo();
	        $oConexion->setQuery("SELECT SUM(Aplica1) AS Importe FROM reglasnegocio WHERE IdTipoRegla = 6;");
	        $DataBono = $oConexion->DevolverQuery();
	        $ValorBono = $DataBono[0]["Importe"];
            return "INSERT INTO ahijados(IdUsuario, Email, Valor) VALUES(".$this->_referenciador[0].",".$this->_referenciador[1].", ".$ValorBono.")";
        }
    }
?>