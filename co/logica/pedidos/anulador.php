<?

	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
 	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php"; 
 	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php"; 
	
	$oConexion = Conexion::nuevo();
	
	$oConexion->Abrir_Trans();
	$oConexion->setQuery("
	SELECT p.IdPedido, p.IdUsuario, c.Nombre, u.NombreUsuario, c.IdCampania FROM pedidos p, campanias c, usuarios u WHERE p.IdEstadoPedidos = 0 AND p.IdCampania = c.IdCampania AND u.IdUsuario = p.IdUsuario AND DATEDIFF(NOW(), c.FechaFin) = 2");
	$p = $oConexion->DevolverQuery();
	$oConexion->Cerrar_Trans();
	
	foreach($p as $i => $pedido) {
		$oConexion->Abrir_Trans();
		$oConexion->setQuery("
		SELECT IdPedido FROM pedidos WHERE IdEstadoPedidos IN (2,3,6) AND IdCampania = ".$pedido['IdCampania']." AND IdUsuario =".$pedido['IdUsuario']);
		$res = $oConexion->DevolverQuery();
		$oConexion->Cerrar_Trans();

		if(count($res) == 0) {
			dmPedidos::restituirBonos($pedido['IdPedido']);
			
            $emailBuilder = new EmailBuilder();
	        $emailData = $emailBuilder->generatePedidoAnuladoEmail($pedido['IdPedido'], $pedido['Nombre'], $pedido['IdUsuario'], $pedido['NombreUsuario']);
	
			$emailSender = new EmailSender();
			$emailSender->sendEmail($emailData);
		}
		
		$oConexion->Abrir_Trans();
		$oConexion->setQuery("
		UPDATE pedidos SET IdEstadoPedidos = 5 WHERE IdPedido = ".$pedido['IdPedido']);
		$p = $oConexion->DevolverQuery();
		$oConexion->Cerrar_Trans();
		
	
	}
                
	

?>