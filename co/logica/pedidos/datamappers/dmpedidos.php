<?php
    class dmPedidos
    {
        public static function save(MySQL_Pedidos $objPedido, $BonoImporte, MySQL_PedidosDirecciones $objDireccionPedidos)
        {
            $oConexion = new Conexion();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objPedido->getInsert());
                $oConexion->EjecutarQuery();
                if($objPedido->getIdPedido() == 0)
                {
                    $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                    $ID = $oConexion->DevolverQuery();
                    $objPedido->setIdPedido($ID[0]["Id"]);
                }
                $objDireccionPedidos->setIdPedido($objPedido->getIdPedido());
                $oConexion->setQuery($objDireccionPedidos->getInsert());
                $oConexion->EjecutarQuery();
                if ($BonoImporte>0)
                {
                    $oConexion->setQuery("SELECT * FROM( SELECT A.*, IF(SUM(BP.Importe) IS NULL,0,SUM(BP.Importe)) AS Importe FROM ( SELECT BU.IdBono, CONCAT( 'Bono obtenido por ', ' ', TR.Descripcion ) AS Descripcion, UNIX_TIMESTAMP( BU.Fecha ) + RN.Caducidad * ( 24 *60 *60 ) AS FechaCaducidad, Aplica1 AS Valor, IdEstadoBono FROM bonos_x_usuarios BU INNER JOIN reglas_aplicadas RA ON BU.IdReglaAplicada = RA.IdReglaAplicada INNER JOIN reglasnegocio RN ON RA.IdRegla = RN.IdRegla INNER JOIN tiposbeneficios TB ON TB.IdBeneficio = RN.Beneficio1 INNER JOIN tiposreglas TR ON TR.IdTipoRegla = RN.IdTipoRegla INNER JOIN usuarios U ON BU.IdUsuario = U.IdUsuario WHERE BU.IdEstadoBono = 0 AND U.IdUsuario = ".$objPedido->getIdUsuario()." AND RN.Beneficio1 =1 AND BU.IdEstadoBono = 0 AND ( UNIX_TIMESTAMP( BU.Fecha ) + RN.Caducidad * ( 24 *60 *60 ) > UNIX_TIMESTAMP(now())  ))A  LEFT JOIN bonos_x_pedidos BP ON BP.IdBono = A.IdBono GROUP BY A.IdBono, A.Descripcion, A.FechaCaducidad, A.Valor, A.IdEstadoBono)B WHERE NOT Importe = Valor AND IdEstadoBono =0");
                    $Tabla = $oConexion->DevolverQuery();
                    $i=0;
                    while($BonoImporte > 0 && $i<count($Tabla))
                    {
                        $Importe = $Tabla[$i]["Valor"] - $Tabla[$i]["Importe"];
                        if($BonoImporte <= $Importe)
                        {
                            $Importe = $BonoImporte;                        
                        }
                        $BonoImporte = abs($Importe - $BonoImporte);
                        $oConexion->setQuery("INSERT INTO bonos_x_pedidos(IdPedido, IdBono, Importe) VALUES (".$objPedido->getIdPedido().", ".$Tabla[$i]["IdBono"].", ".$Importe.")");
                        $oConexion->EjecutarQuery();
                        $i++;
                    }                
                }
                
                
                foreach ($objPedido->getProductos() as $Producto)
                {
                    $Producto->setIdPedido($objPedido->getIdPedido());
                    $oConexion->setQuery($Producto->getInsert());
                    $oConexion->EjecutarQuery();
                }
                $oConexion->Cerrar_Trans();
                return $objPedido->getIdPedido();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
    
   	public static function getDatosMailByIdPedido($idPedido){

    	$oConexion = Conexion::nuevo();
    	$oConexion->Abrir();
   		$oConexion->setQuery("SELECT ep.Descripcion AS estadoPedido, u.NombreUsuario AS emailUsuario,dup.Nombre AS nombreUsuario, dup.Apellido AS apellidoUsuario, up.IdUsuario AS IdPadrino, up.NombreUsuario AS emailPadrino, dupp.Nombre AS nombrePadrino, dupp.Apellido AS apellidoPadrino, c.Nombre AS nombreVidriera, p.IdPedido AS numeroPedido, p.NroEnvio as numeroTrackingDHL, c.TiempoEntregaIn, c.TiempoEntregaFn, codbar.codigodebarra, p.Total AS total, codbar.fecha_vencimiento AS fechaVencimiento, u.IdUsuario
								FROM pedidos AS p JOIN usuarios AS u ON p.IdUsuario = u.IdUsuario
								JOIN datosusuariospersonales AS dup ON u.IdUsuario = dup.IdUsuario
								JOIN campanias AS c ON c.IdCampania = p.IdCampania
								JOIN estadospedidos AS ep ON p.IdEstadoPedidos = ep.IdEstadoPedido
								LEFT JOIN usuarios AS up ON u.Padrino = up.NombreUsuario
								LEFT JOIN datosusuariospersonales AS dupp ON dupp.IdUsuario = up.IdUsuario
								LEFT JOIN formaspagos_codigosdebarra AS codbar ON codbar.Pedido_Id = p.IdPedido
								WHERE IdPedido = ".mysql_real_escape_string($idPedido)." LIMIT 1");

   		$tabla = $oConexion->DevolverQuery();
   		if(empty($tabla[0])){
   			throw new MySQLException("no existe este pedido($idPedido)");
   		}
   		return $tabla[0];
   	}
        public static function SistemaPagoProcesando($IdPedido, $IdUsuario)
        {
            $oConexion = new Conexion();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdPedido FROM pedidos WHERE IdPedido = ".mysql_real_escape_string($IdPedido)." AND IdUsuario = ".mysql_real_escape_string($IdUsuario)." AND (IdEstadoPedidos = 0 OR IdEstadoPedidos = 4)");
                $Tabla = $oConexion->DevolverQuery();
                if(count($Tabla) != 1)
                    throw new MySQLException("PEDIDO_INCORRECTO");
                $oConexion->setQuery("UPDATE pedidos  SET IdEstadoPedidos = 1 WHERE IdPedido = ".mysql_real_escape_string($IdPedido)." AND IdUsuario = ".mysql_real_escape_string($IdUsuario)." AND (IdEstadoPedidos = 0 OR IdEstadoPedidos = 4)");
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function SistemaPagoRechazado($IdPedido, $IdUsuario)
        {
            $oConexion = new Conexion();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdPedido FROM pedidos WHERE IdPedido = ".mysql_real_escape_string($IdPedido)." AND IdUsuario = ".mysql_real_escape_string($IdUsuario)." AND (IdEstadoPedidos = 0 OR IdEstadoPedidos = 4)");
                $Tabla = $oConexion->DevolverQuery();
                if(count($Tabla) != 1)
                    throw new MySQLException("PEDIDO_INCORRECTO");
                $oConexion->setQuery("UPDATE pedidos  SET IdEstadoPedidos = 4 WHERE IdPedido = ".mysql_real_escape_string($IdPedido)." AND IdUsuario = ".mysql_real_escape_string($IdUsuario)." AND (IdEstadoPedidos = 0 OR IdEstadoPedidos = 4)");
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function PagoAceptado($IdPedido, $IdUsuario = false)
        {
            $oConexion = new Conexion();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdPedido FROM pedidos WHERE IdPedido = ".mysql_real_escape_string($IdPedido)." AND (IdEstadoPedidos = 0 OR IdEstadoPedidos = 4 OR IdEstadoPedidos = 1)");
                $Tabla = $oConexion->DevolverQuery();
     
                if(count($Tabla) != 1)
                    //throw new MySQLException("PEDIDO_INCORRECTO");
                    return false;
                $oConexion->setQuery("SELECT COUNT(*) AS Cantidad FROM pedidos WHERE IdPedido = ".mysql_real_escape_string($IdPedido)." AND IdEstadoPedidos = 2");
                $Tabla = $oConexion->DevolverQuery();
                if($Tabla[0]["Cantidad"] >= 1)
                    //throw new Exception("El estado actual del pedido es 'Pago aceptado'.", 1);
                    return false;
                $oConexion->setQuery("UPDATE pedidos SET IdEstadoPedidos = 2 WHERE IdPedido=" . mysql_real_escape_string($IdPedido));
                $oConexion->EjecutarQuery();
                $oConexion->setQuery("SELECT productos_x_proveedores.IdCodigoProdInterno, Stock, Cantidad FROM productos_x_proveedores INNER JOIN  productos_x_pedidos ON productos_x_pedidos.IdCodigoProdInterno = productos_x_proveedores.IdCodigoProdInterno WHERE IdPedido = ".mysql_real_escape_string($IdPedido));
                $Tabla = $oConexion->DevolverQuery();
                foreach($Tabla as $fila)
                {
                    if($fila["Stock"] < $fila["Cantidad"])
                    {	/* Si no hay stock para armar el pedido lo marco como Stock Insuficiente */
                        $oConexion->setQuery("UPDATE pedidos SET IdEstadoPedidos = 10 WHERE IdPedido=" . $IdPedido);
                        $oConexion->EjecutarQuery();
                        self::enviarEmailStockInsuficiente($IdPedido);
                    }
                    else
                    {
                         $oConexion->setQuery("UPDATE productos_x_proveedores SET Stock = Stock - ".$fila["Cantidad"]." WHERE IdCodigoProdInterno = ".$fila["IdCodigoProdInterno"]);
                         $oConexion->EjecutarQuery();
                    }
                }
                $oConexion->Cerrar_Trans();
                
                return true;
            }
            catch(MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
	    protected static function enviarEmailStockInsuficiente($IdPedido = 0){
	    
	  		$oConexion = new Conexion();
			$oConexion->setQuery("
			SELECT * FROM usuarios WHERE IdPerfil = 7");
			$usuarios = $oConexion->DevolverQuery();
		    
			foreach($usuarios as $i => $p) {
				$emailBuilder = new EmailBuilder();
				$emailData = $emailBuilder->generatePedidoInsuficiente($IdPedido, $p['NombreUsuario']);
				
				$emailSender = new EmailSender();
				$emailSender->sendEmail($emailData);
			}
	   }        

        public static function PedidoAnulado($IdPedido, $Motivo="")
        {
            $oConexion = new Conexion();
            try
            {  
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT IdPedido FROM pedidos WHERE IdPedido = ".mysql_real_escape_string($IdPedido));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->setQuery("UPDATE pedidos SET IdEstadoPedidos = 5, MotivoAnulacion = '". htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($Motivo)), ENT_QUOTES) ."' WHERE IdPedido = ".mysql_real_escape_string($IdPedido));
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        /**
        * @desc Dice si hay stock para ese pedido
        * @return bool
        */
        public static function HayStock($IdPedido)
        {
            $oConexion = new Conexion();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT productos_x_proveedores.IdCodigoProdInterno, Stock, Cantidad FROM productos_x_proveedores INNER JOIN  productos_x_pedidos ON productos_x_pedidos.IdCodigoProdInterno = productos_x_proveedores.IdCodigoProdInterno WHERE IdPedido = ".mysql_real_escape_string($IdPedido));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                foreach($Tabla as $fila)
                {
                    if($fila["Stock"] < $fila["Cantidad"])
                    {
                        return false;
                    }
                }
                return true;
            }
            catch(MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        /**
        * @desc Verifica si hay stock y escribe en la base de datos anulando el pedido
        */
        public static function VerificarStock($IdPedido)
        {
            $oConexion = new Conexion();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT productos_x_proveedores.IdCodigoProdInterno, Stock, Cantidad FROM productos_x_proveedores INNER JOIN  productos_x_pedidos ON productos_x_pedidos.IdCodigoProdInterno = productos_x_proveedores.IdCodigoProdInterno WHERE IdPedido = ".mysql_real_escape_string($IdPedido));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                foreach($Tabla as $fila)
                {
                    if($fila["Stock"] < $fila["Cantidad"])
                    {
                        dmPedidos::PedidoAnulado($IdPedido, "Falta de stock.");
                    }
                }
            }
            catch(MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getPedidoById($IdPedido)
        {
            $oConexion = new Conexion();
            try
            {  
                $objPedidos = new MySQL_Pedidos();
                $objPedidos->setIdPedido($IdPedido);
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objPedidos->getById());
                $Tabla = $oConexion->DevolverQuery();
                if(isset($Tabla[0]))
                {   
                    $objPedidos->__setByArray($Tabla[0]);
                    $objDireccion = new MySQL_PedidosDirecciones();
                    $objDireccion->setIdPedido($objPedidos->getIdPedido());
                    $objDireccion->setIdUsuario($objPedidos->getIdUsuario());
                    $oConexion->setQuery($objDireccion->getById());
                    $Tabla = $oConexion->DevolverQuery();
                    if(isset($Tabla[0]))
                        $objDireccion->__setByArray($Tabla[0]);
                    $objPedidos->setDireccion($objDireccion);
                    
                    $oConexion->setQuery("SELECT IdPedido, IdCodigoProdInterno, Cantidad, Precio FROM productos_x_pedidos WHERE IdPedido = " .mysql_real_escape_string($objPedidos->getIdPedido()));
                    $Tabla = $oConexion->DevolverQuery();
                    foreach($Tabla as $fila)
                    {
                        $objProducto = new productos_x_pedidos();
                        $objProducto->__setByArray($fila);
                        $objPedidos->setProducto($objProducto);
                    }
                }
                $oConexion->Cerrar_Trans();
                return $objPedidos;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getIdCategoriaByIdPedido($IdPedido)
        {
            $oConexion = new Conexion();
            try
            {  
                $objPedidos = new MySQL_Pedidos();
                $objPedidos->setIdPedido($IdPedido);
                $oConexion->Abrir_Trans();
                $oConexion->setQuery($objPedidos->getById());
                $Tabla = $oConexion->DevolverQuery();
                $IdCampania = 0;
                if(isset($Tabla[0]))
                {   
                	 $IdCampania = $Tabla[0]['IdCampania'];
                }
                $oConexion->Cerrar_Trans();
                return $IdCampania;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        public static function getInfoPedido($IdPedido)
        {
            $oConexion = new Conexion();
            try
            {  
                $T = array();
                $oConexion->Abrir_Trans();
                /*$oConexion->setQuery("SELECT 'FormaPago' as Id, Descripcion FROM formaspagos WHERE IdFormaPago = (SELECT IdFormaPago FROM pedidos WHERE IdPedido =".$IdPedido.")");
                $Tabla = $oConexion->DevolverQuery();
                $T["FormaPago"]=$Tabla[0];*/
                $T["FormaPago"]["Descripcion"] ="Dineromail";
                $oConexion->setQuery("SELECT 'Estado' as Id, Descripcion FROM estadospedidos WHERE IdEstadoPedido = (SELECT IdEstadoPedidos FROM pedidos WHERE IdPedido =".mysql_real_escape_string($IdPedido).")");
                $Tabla = $oConexion->DevolverQuery();
                $T["Estado"]=$Tabla[0];
                $oConexion->Cerrar_Trans();
                return $T;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public static function ActualizarTarjeta($id, $card) {
	    	$oConexion = new Conexion();
	    	$oConexion->Abrir_Trans();
	        $oConexion->setQuery("UPDATE pedidos SET IdTarjeta = '".$card."' WHERE IdPedido =".mysql_real_escape_string($id));
	        $oConexion->EjecutarQuery();
	        $oConexion->Cerrar_Trans();
	    }
    }
?> 