<?php
$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));

class GeelbeCashDAO {
	
	public static function Guardar($transaction) {
		$transaction->generarEncriptados();
		$oConexion = Conexion::nuevo();
        try {
        	$oConexion->Abrir_Trans();
            $sql = "INSERT INTO geelbecash(".(($transaction->getId()!=null)?"id, ":"")."idUsuario, monto, motivo, fecha, estado, crc) 
                    VALUES(".(($transaction->getId()!=null)?$transaction->getId().", ":"").
                            $transaction->getIdUsuario().", '".
                            htmlspecialchars(mysql_real_escape_string($transaction->getMontoEncriptado()), ENT_QUOTES)."', '".
                            htmlspecialchars(mysql_real_escape_string($transaction->getMotivoEncriptado()), ENT_QUOTES)."', '".
                            $transaction->getFecha()."', ".
                            $transaction->getEstado().", '".
                            $transaction->getCrc()."') ".
                   "ON DUPLICATE KEY UPDATE idUsuario = ".$transaction->getIdUsuario().", ".
                            "monto = '".htmlspecialchars(mysql_real_escape_string($transaction->getMontoEncriptado()), ENT_QUOTES)."', ".
                            "motivo = '".htmlspecialchars(mysql_real_escape_string($transaction->getMotivoEncriptado()), ENT_QUOTES)."', ".
                            "fecha = '".$transaction->getFecha()."', ".
                            "estado = ".$transaction->getEstado().", ".
                            "crc = '".$transaction->getCrc()."'";
                                 
            $oConexion->setQuery($sql);
            $oConexion->EjecutarQuery();
            $id = mysql_insert_id();
            $oConexion->Cerrar_Trans();
            
            return $id;
        }
        catch(exception $e) {
        	$oConexion->RollBack();
            throw $e;
        }
	}

	public static function GuardarAnulacion($transaction, $motivo = "", $idUsuarioBO = null) {
		$oConexion = Conexion::nuevo();
        $sql = "INSERT INTO geelbecash_trx_anuladas (trx_id, idUsuarioBO, motivo, fecha) 
                VALUES(".mysql_real_escape_string($transaction->getId()).", ".
                         mysql_real_escape_string($idUsuarioBO).", '".
                         mysql_real_escape_string($motivo)."', NOW()) ";
                                 
        $oConexion->setQuery($sql);
        $oConexion->EjecutarQuery();
	}

	public static function GuardarCargaCredito($trx_id, $idUsuarioBO = null) {
		$oConexion = Conexion::nuevo();
        $sql = "INSERT INTO geelbecash_cargas_credito_bo (trx_id, idUsuarioBO) 
                VALUES(".mysql_real_escape_string($trx_id).", ".
                         mysql_real_escape_string($idUsuarioBO).") ";
                                 
        $oConexion->setQuery($sql);
        $oConexion->EjecutarQuery();
	}
	
	public static function ObtenerPorId($id) {
		$oConexion = Conexion::nuevo();
       	$oConexion->Abrir_Trans();
        $sql = "SELECT GC.id, GC.idUsuario, GC.monto, GC.motivo, GC.fecha, GC.estado, GC.crc
          		FROM geelbecash GC 
           		WHERE GC.id = ".mysql_real_escape_string($id);
        $oConexion->setQuery($sql);
            
        $array = $oConexion->DevolverQuery();
        $oConexion->Cerrar_Trans();
            
		$transaction = new GeelbeCashTransaction();
		$transaction->setId($array[0]["id"]);
		$transaction->setIdUsuario($array[0]["idUsuario"]);
		$transaction->setMontoEncriptado($array[0]["monto"]);
		$transaction->setMotivoEncriptado($array[0]["motivo"]);
		$transaction->setFecha($array[0]["fecha"]);
		$transaction->setEstado($array[0]["estado"]);
		$transaction->setCrc($array[0]["crc"]);
		
		$transaction->generarDesencriptados();
		
		if(!$transaction->validarCrc()){	   	
			throw new Exception("Error al Validar CRC.");
		}   
		
        return $transaction;
	}

	
	public static function ObtenerPorIdUsuario($idUsuario) {
		$oConexion = Conexion::nuevo();
       	$oConexion->Abrir_Trans();
        $sql = "SELECT GC.id, GC.idUsuario, GC.monto, GC.motivo, GC.fecha, GC.estado, GC.crc
          		FROM geelbecash GC 
           		WHERE GC.idUsuario = ".mysql_real_escape_string($idUsuario);
        $oConexion->setQuery($sql);
            
        $array = $oConexion->DevolverQuery();
        $oConexion->Cerrar_Trans();
        
        $lista = array();
        
        foreach ($array as $valor) {
        	$transaction = new GeelbeCashTransaction();
			$transaction->setId($valor["id"]);
			$transaction->setIdUsuario($valor["idUsuario"]);
			$transaction->setMontoEncriptado($valor["monto"]);
			$transaction->setMotivoEncriptado($valor["motivo"]);
			$transaction->setFecha($valor["fecha"]);
			$transaction->setEstado($valor["estado"]);
			$transaction->setCrc($valor["crc"]);
		
			$transaction->generarDesencriptados();
			
			if(!$transaction->validarCrc()){			    	
				throw new Exception("Error al Validar CRC.");
			}   
			
			$lista[] = $transaction;
        }
        
        return $lista;
	}


	
	public static function ObtenerPorIdUsuarioConEstado($idUsuario, $estado) {
		$oConexion = Conexion::nuevo();
       	$oConexion->Abrir_Trans();
        $sql = "SELECT GC.id, GC.idUsuario, GC.monto, GC.motivo, GC.fecha, GC.estado, GC.crc
          		FROM geelbecash GC 
           		WHERE GC.idUsuario = ".mysql_real_escape_string($idUsuario).
              " AND GC.estado = ".mysql_real_escape_string($estado);
        $oConexion->setQuery($sql);
            
        $array = $oConexion->DevolverQuery();
        $oConexion->Cerrar_Trans();
        
        $lista = array();
        
        foreach ($array as $valor) {
        	$transaction = new GeelbeCashTransaction();
			$transaction->setId($valor["id"]);
			$transaction->setIdUsuario($valor["idUsuario"]);
			$transaction->setMontoEncriptado($valor["monto"]);
			$transaction->setMotivoEncriptado($valor["motivo"]);
			$transaction->setFecha($valor["fecha"]);
			$transaction->setEstado($valor["estado"]);
			$transaction->setCrc($valor["crc"]);
		
			$transaction->generarDesencriptados();
			
			if(!$transaction->validarCrc()){			    	
				throw new Exception("Error al Validar CRC.");
			}   
			
			$lista[] = $transaction;
        }
        
        return $lista;
	}
	
	public static function GuardarDN(GeelbeCashDenormalizacion $denormalizacion) {
		$denormalizacion->generarEncriptados();
		$oConexion = Conexion::nuevo();
        try {
        	$oConexion->Abrir_Trans();
        	
            $sql = "INSERT INTO geelbecash_dn(".(($denormalizacion->getIdUsuario()!=null)?"idUsuario, ":"")."montoUltimoGasto, montoGastosHistoricos, montoCargasHistoricas, montoSaldoActual, crc) 
                    VALUES(".(($denormalizacion->getIdUsuario()!=null)?$denormalizacion->getIdUsuario().", '":"'").
                            htmlspecialchars(mysql_real_escape_string($denormalizacion->getMontoUltimoGastoEncriptado()), ENT_QUOTES)."', '".
                            htmlspecialchars(mysql_real_escape_string($denormalizacion->getMontoGastosHistoricosEncriptados()), ENT_QUOTES)."', '".
                            htmlspecialchars(mysql_real_escape_string($denormalizacion->getMontoCargasHistoricasEncriptados()), ENT_QUOTES)."', '".
                            htmlspecialchars(mysql_real_escape_string($denormalizacion->getMontoSaldoActualEncriptado()), ENT_QUOTES)."', '".
                            $denormalizacion->getCrc()."') ".
                   "ON DUPLICATE KEY UPDATE ".
                            "montoUltimoGasto = '".htmlspecialchars(mysql_real_escape_string($denormalizacion->getMontoUltimoGastoEncriptado()), ENT_QUOTES)."', ".
                            "montoGastosHistoricos = '".htmlspecialchars(mysql_real_escape_string($denormalizacion->getMontoGastosHistoricosEncriptados()), ENT_QUOTES)."', ".
                            "montoCargasHistoricas = '".htmlspecialchars(mysql_real_escape_string($denormalizacion->getMontoCargasHistoricasEncriptados()), ENT_QUOTES)."', ".
                            "montoSaldoActual = '".htmlspecialchars(mysql_real_escape_string($denormalizacion->getMontoSaldoActualEncriptado()), ENT_QUOTES)."', ".
                            "crc = '".$denormalizacion->getCrc()."'";
                                 
            $oConexion->setQuery($sql);
            $oConexion->EjecutarQuery();
            $oConexion->Cerrar_Trans();
        }
        catch(exception $e) {
        	$oConexion->RollBack();
            throw $e;
        }
	}
	

	public static function ObtenerDNPorIdUsuario($idUsuario) {
		$oConexion = Conexion::nuevo();
       	$oConexion->Abrir_Trans();
        $sql = "SELECT GCN.idUsuario, GCN.montoUltimoGasto, GCN.montoGastosHistoricos, GCN.montoCargasHistoricas, GCN.montoSaldoActual, GCN.crc
          		FROM geelbecash_dn GCN 
           		WHERE GCN.idUsuario = ".mysql_real_escape_string($idUsuario);
        $oConexion->setQuery($sql);
            
        $valor = $oConexion->DevolverQuery();
        $oConexion->Cerrar_Trans();
        
        $denormalizacion = new GeelbeCashDenormalizacion();
		$denormalizacion->setIdUsuario($valor[0]["idUsuario"]);
		$denormalizacion->setMontoUltimoGastoEncriptado($valor[0]["montoUltimoGasto"]);
		$denormalizacion->setMontoGastosHistoricosEncriptados($valor[0]["montoGastosHistoricos"]);
		$denormalizacion->setMontoCargasHistoricasEncriptados($valor[0]["montoCargasHistoricas"]);
		$denormalizacion->setMontoSaldoActualEncriptado($valor[0]["montoSaldoActual"]);
		$denormalizacion->setCrc($valor[0]["crc"]);
		

		if (isset($valor[0]["idUsuario"])) {
			$denormalizacion->generarDesencriptados();
			if(!$denormalizacion->validarCrc()){	
				throw new Exception("Error al Validar CRC.");
			}   
		} else {
			$denormalizacion->setIdUsuario($idUsuario);
		}
			
		return $denormalizacion;
	}
	
	public static function ObtenerCantidadTransacciones($paramArray = array()) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			
	    $oConexion = Conexion::nuevo();
        $oConexion->setQuery("SELECT count(1) as total  
        					  FROM geelbecash gc 
        					  inner join usuarios u on gc.idUsuario = u.IdUsuario
        					  inner join geelbecash_trx_status gts on gc.estado = gts.id
        					  left join geelbecash_cargas_credito_bo gcc on gc.id = gcc.trx_id
        					  left join usuarios bou on gcc.idUsuarioBO = bou.IdUsuario
							  WHERE ($filtersSQL)");
        $Tabla = $oConexion->DevolverQuery();
        return $Tabla[0]['total'];
	}

	public static function ObtenerTransaccionesPaginadas($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
		$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
		$oConexion = Conexion::nuevo();
		$oConexion->setQuery(
			"SELECT gc.id as 'gc.id', 
				 u.NombreUsuario as 'u.NombreUsuario', 
				 DATE_FORMAT( gc.fecha, '%d/%m/%Y %H:%i' ) AS 'DATE_FORMAT(gc.fecha,\'%d/%m/%Y %H:%i\')',
				 gc.monto,
				 gc.motivo,
				 gc.estado as 'gc.estado',
				 gc.fecha,
				 gc.crc,
				 gts.descripcion as 'gts.descripcion',
				 bou.NombreUsuario as 'bou.NombreUsuario'
			FROM geelbecash gc
			inner join usuarios u on gc.idUsuario = u.IdUsuario
			inner join geelbecash_trx_status gts on gc.estado = gts.id
			left join geelbecash_cargas_credito_bo gcc on gc.id = gcc.trx_id
			left join usuarios bou on gcc.idUsuarioBO = bou.IdUsuario
			WHERE ($filtersSQL) 
			ORDER BY $sortSQL 
			LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
	                	
		$Tabla = $oConexion->DevolverQuery();
		
	    $lista = array();
        
        foreach ($Tabla as $valor) {
        	$transaction = new GeelbeCashTransaction();
			$transaction->setId($valor["id"]);
			$transaction->setIdUsuario($valor["idUsuario"]);
			$transaction->setMontoEncriptado($valor["monto"]);
			$transaction->setMotivoEncriptado($valor["motivo"]);
			$transaction->setFecha($valor["fecha"]);
			$transaction->setEstado($valor["estado"]);
			$transaction->setCrc($valor["crc"]);
		
			$transaction->generarDesencriptados();
			
			if(!$transaction->validarCrc()){			    	
				throw new Exception("Error al Validar CRC.");
			}   
			
			$lista[] = $transaction;
        }
        
        foreach ($lista as $idx => $trx) {
        	$Tabla[$idx]["gc.monto"] = $trx->getMonto();
        	$Tabla[$idx]["gc.motivo"] = $trx->getMotivo();
        }
        
		ColumnaMoneda(&$Tabla, "gc.monto");
		return $Tabla;
	}
	
	public static function ObtenerCantidadTransaccionesAnuladas($paramArray = array()) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			
	    $oConexion = Conexion::nuevo();
        $oConexion->setQuery("SELECT count(1) as total  
        					  FROM geelbecash_trx_anuladas gca
        					  inner join geelbecash gc on gca.trx_id = gc.id 
        					  inner join usuarios u on gc.idUsuario = u.IdUsuario
        					  inner join usuarios ubo on gca.idUsuarioBO = ubo.IdUsuario
							  WHERE ($filtersSQL)");
        $Tabla = $oConexion->DevolverQuery();
        return $Tabla[0]['total'];
	}

	public static function ObtenerTransaccionesAnuladasPaginadas($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
		$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
		$oConexion = Conexion::nuevo();
		$oConexion->setQuery(
			"SELECT gc.id as 'gc.id', 
				 u.NombreUsuario as 'u.NombreUsuario', 
				 DATE_FORMAT( gca.fecha, '%d/%m/%Y %H:%i' ) AS 'DATE_FORMAT(gca.fecha,\'%d/%m/%Y %H:%i\')',
				 gca.motivo as 'gca.motivo',
				 gca.fecha as 'gca.fecha',
				 ubo.NombreUsuario as 'ubo.NombreUsuario'
			FROM geelbecash_trx_anuladas gca
			inner join geelbecash gc on gca.trx_id = gc.id
			inner join usuarios u on gc.idUsuario = u.IdUsuario
			inner join usuarios ubo on gca.idUsuarioBO = ubo.IdUsuario
			WHERE ($filtersSQL) 
			ORDER BY $sortSQL 
			LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
	                	
		return $oConexion->DevolverQuery();
	}

	public static function ObtenerPromocionParaImporte($monto_trx = 0) {
		$oConexion = Conexion::nuevo();
		$oConexion->setQuery("
			SELECT id, monto_bonificacion, porcentaje_bonificacion
			FROM geelbecash_promociones
			WHERE activa = TRUE and monto_trx = ".mysql_real_escape_string($monto_trx)." 
			and (promocion_temporal = FALSE or (promocion_temporal = TRUE and NOW() BETWEEN fecha_desde AND fecha_hasta)) 
		");
	                	
		return $oConexion->DevolverQuery();
	}
}
        