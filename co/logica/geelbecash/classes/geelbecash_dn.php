<?

$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));

require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";

class GeelbeCashDenormalizacion {

	private $idUsuario;
	private $montoUltimoGasto;
	private $montoGastosHistoricos;
	private $montoCargasHistoricas;
	private $montoSaldoActual;
	private $montoUltimoGastoEncriptado;
	private $montoGastosHistoricosEncriptados;
	private $montoCargasHistoricasEncriptados;
	private $montoSaldoActualEncriptado;
	private $crc;
	
	public function generarCrc() {
		$stringToHash = "&".(string)$this->getIdUsuario().(string)$this->getMontoUltimoGasto().(string)$this->getMontoGastosHistoricos().(string)$this->getMontoCargasHistoricas().(string)$this->getMontoSaldoActual()."&";
		return Hash::getHash($stringToHash);
	}
	
	public function generarEncriptados() {
		$this->setCrc($this->generarCrc());
		$this->setMontoUltimoGastoEncriptado(trim(Hash::encriptar($this->getMontoUltimoGasto(), $this->getCrc())));
		$this->setMontoGastosHistoricosEncriptados(trim(Hash::encriptar($this->getMontoGastosHistoricos(), $this->getCrc())));
		$this->setMontoCargasHistoricasEncriptados(trim(Hash::encriptar($this->getMontoCargasHistoricas(), $this->getCrc())));
		$this->setMontoSaldoActualEncriptado(trim(Hash::encriptar($this->getMontoSaldoActual(), $this->getCrc())));
	}
	
	public function generarDesencriptados() {
		$this->setMontoUltimoGasto(trim(Hash::desencriptar($this->getMontoUltimoGastoEncriptado(), $this->getCrc())));
		$this->setMontoGastosHistoricos(trim(Hash::desencriptar($this->getMontoGastosHistoricosEncriptados(), $this->getCrc())));
		$this->setMontoCargasHistoricas(trim(Hash::desencriptar($this->getMontoCargasHistoricasEncriptados(), $this->getCrc())));
		$this->setMontoSaldoActual(trim(Hash::desencriptar($this->getMontoSaldoActualEncriptado(), $this->getCrc())));
	}
	
	public function validarCrc() {
		if (Hash::compareHash($this->generarCrc(),$this->getCrc())) {
			return true;
		}
		return false;
	}
	
	
	// Getters & Setters
	public function setIdUsuario($unIdUsuario) {
		$this->idUsuario = $unIdUsuario;
	}
	public function getIdUsuario() {
		return $this->idUsuario;
	}

	public function setMontoUltimoGasto($unMonto) {
		$this->montoUltimoGasto = $unMonto;
	}
	public function getMontoUltimoGasto() {
		return $this->montoUltimoGasto;
	}

	public function setMontoUltimoGastoEncriptado($unMontoEncriptado) {
		$this->montoUltimoGastoEncriptado = $unMontoEncriptado;
	}
	public function getMontoUltimoGastoEncriptado() {
		return $this->montoUltimoGastoEncriptado;
	}
	
	public function setMontoGastosHistoricos($unMonto) {
		$this->montoGastosHistoricos = $unMonto;
	}
	public function getMontoGastosHistoricos() {
		return $this->montoGastosHistoricos;
	}

	public function setMontoGastosHistoricosEncriptados($unMontoEncriptado) {
		$this->montoGastosHistoricosEncriptados = $unMontoEncriptado;
	}
	public function getMontoGastosHistoricosEncriptados() {
		return $this->montoGastosHistoricosEncriptados;
	}
	
	public function setMontoCargasHistoricas($unMonto) {
		$this->montoCargasHistoricas = $unMonto;
	}
	public function getMontoCargasHistoricas() {
		return $this->montoCargasHistoricas;
	}

	public function setMontoCargasHistoricasEncriptados($unMontoEncriptado) {
		$this->montoCargasHistoricasEncriptados = $unMontoEncriptado;
	}
	public function getMontoCargasHistoricasEncriptados() {
		return $this->montoCargasHistoricasEncriptados;
	}
	
	public function setMontoSaldoActual($unMonto) {
		$this->montoSaldoActual = $unMonto;
	}
	public function getMontoSaldoActual() {
		return $this->montoSaldoActual;
	}

	public function setMontoSaldoActualEncriptado($unMontoEncriptado) {
		$this->montoSaldoActualEncriptado = $unMontoEncriptado;
	}
	public function getMontoSaldoActualEncriptado() {
		return $this->montoSaldoActualEncriptado;
	}

	public function setCrc($unCrc) {
		$this->crc = $unCrc;
	}
	public function getCrc() {
		return $this->crc;
	}
	
}
?>
