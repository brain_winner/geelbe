<?php

$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";

class GeelbeCashTransaction {

	private $id;
	private $idUsuario;
	private $monto;
	private $montoEncriptado;
	private $motivo;
	private $motivoEncriptado;
	private $fecha;
	private $estado;
	private $crc;
	
	public static $PENDIENTE = 1;
	public static $ACEPTADO = 2;
	public static $RECHAZADO = 3;
	public static $ANULADO = 4;
	public static $CANCELADO = 5;
	
	public static $TRX_50000_ID = 1;
	public static $TRX_85000_ID = 2;
	public static $TRX_100000_ID = 3;
	public static $TRX_150000_ID = 4;
	public static $TRX_200000_ID = 5;
	public static $TRX_300000_ID = 6;
	public static $TRX_10000_ID = 7;
	public static $TRX_25000_ID = 8;

//  Bonos mayores
//	public static $TRX_400000_ID = 7;
//	public static $TRX_500000_ID = 8;

	
	public function generarCrc() {
		$stringToHash = "#".(string)$this->getIdUsuario().(string)$this->getMonto().(string)$this->getMotivo().(string)$this->getFecha().(string)$this->getEstado()."#";
		return Hash::getHash($stringToHash);
	}
	
	public function generarEncriptados() {
		$this->setCrc($this->generarCrc());
		$this->setMontoEncriptado(trim(Hash::encriptar($this->getMonto(), $this->getCrc())));
		$this->setMotivoEncriptado(trim(Hash::encriptar($this->getMotivo(), $this->getCrc())));
	}
	
	public function generarDesencriptados() {
		$this->setMonto(trim(Hash::desencriptar($this->getMontoEncriptado(), $this->getCrc())));
		$this->setMotivo(trim(Hash::desencriptar($this->getMotivoEncriptado(), $this->getCrc())));
	}
	
	public function validarCrc() {
		if (Hash::compareHash($this->generarCrc(),$this->getCrc())) {
			return "true";
		}
		return "false";
	}
	
	
	// Getters & Setters
	public function setId($unId) {
		$this->id = $unId;
	}
	public function getId() {
		return $this->id;
	}

	public function setIdUsuario($unIdUsuario) {
		$this->idUsuario = $unIdUsuario;
	}
	public function getIdUsuario() {
		return $this->idUsuario;
	}

	public function setMonto($unMonto) {
		$this->monto = $unMonto;
	}
	public function getMonto() {
		return $this->monto;
	}

	public function setMontoEncriptado($unMontoEncriptado) {
		$this->montoEncriptado = $unMontoEncriptado;
	}
	public function getMontoEncriptado() {
		return $this->montoEncriptado;
	}

	public function setMotivo($unMotivo) {
		$this->motivo = $unMotivo;
	}
	public function getMotivo() {
		return $this->motivo;
	}

	public function setMotivoEncriptado($unMotivoEncriptado) {
		$this->motivoEncriptado = $unMotivoEncriptado;
	}
	public function getMotivoEncriptado() {
		return $this->motivoEncriptado;
	}

	public function setFecha($unaFecha) {
		$this->fecha = $unaFecha;
	}
	public function getFecha() {
		return $this->fecha;
	}

	public function setEstado($unEstado) {
		$this->estado = $unEstado;
	}
	public function getEstado() {
		return $this->estado;
	}

	public function setCrc($unCrc) {
		$this->crc = $unCrc;
	}
	public function getCrc() {
		return $this->crc;
	}
	
}
?>
