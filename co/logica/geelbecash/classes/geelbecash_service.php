<?php

require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_transaction.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_dn.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/invalid_trx_id_exception.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/invalid_user_email_exception.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/already_revoke_trx_exception.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/datamappers/dmgeelbecash.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php");
require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/clases/clsusuarios.php");

class GeelbeCashService {
	
	public static function getEstadoGeelbeCash($userId) {
		return GeelbeCashDAO::ObtenerDNPorIdUsuario($userId);
	}
	
	public static function getCargasPendientes($userId = null) {
		return GeelbeCashDAO::ObtenerPorIdUsuarioConEstado($userId, GeelbeCashTransaction::$PENDIENTE);
	}
	
	public static function generarTransaccion($userId = null, $monto = 0, $motivo = "") {
		$transaction = new GeelbeCashTransaction();
		
		$transaction->setIdUsuario($userId);
		$transaction->setMonto($monto);
		$transaction->setMotivo($motivo);
		$transaction->setFecha(date("Y-m-d H:i:s"));
		$transaction->setEstado(GeelbeCashTransaction::$PENDIENTE);
		
		return GeelbeCashDAO::Guardar($transaction);
	}
	
	public static function aceptarTransaccion($trx_id = null) {
		$transaccion = GeelbeCashDAO::ObtenerPorId($trx_id);
		$dn = GeelbeCashDAO::ObtenerDNPorIdUsuario($transaccion->getIdUsuario());

		if ($transaccion->getEstado() != GeelbeCashTransaction::$ACEPTADO) {
			$DataPromocion = GeelbeCashDAO::ObtenerPromocionParaImporte($transaccion->getMonto());
			$montoBonificado = 0;

			if ($DataPromocion != null) {
				if ($DataPromocion[0]["monto_bonificacion"] != null && $DataPromocion[0]["monto_bonificacion"] > 0) {
					$montoBonificado = $DataPromocion[0]["monto_bonificacion"];
				} else if ($DataPromocion[0]["porcentaje_bonificacion"] != null && $DataPromocion[0]["porcentaje_bonificacion"] > 0) {
					$montoBonificado = $transaccion->getMonto() * $DataPromocion[0]["porcentaje_bonificacion"];
				}
			}
			
			$objUsuario = dmUsuario::getByIdUsuario($transaccion->getIdUsuario());
			
			// Actualizo estado de la transaccion
			$transaccion->setEstado(GeelbeCashTransaction::$ACEPTADO);
			GeelbeCashDAO::Guardar($transaccion);
			
			if ($montoBonificado > 0) {
				$motivoBonificacion = "Bonificacion PROMO ID ".$DataPromocion[0]["id"];
				self::cargarCredito($objUsuario->getNombreUsuario(), $montoBonificado, $motivoBonificacion, null);
			}
			
			// Actualizo desnormalizaciones
			$montoCargasHistoricas = ($dn->getMontoCargasHistoricas())?$dn->getMontoCargasHistoricas():0;
			$montoSaldoActual = ($dn->getMontoSaldoActual())?$dn->getMontoSaldoActual():0;
			
			$dn->setMontoCargasHistoricas($montoCargasHistoricas + $transaccion->getMonto() + $montoBonificado);
			$dn->setMontoSaldoActual($montoSaldoActual + $transaccion->getMonto() + $montoBonificado);
			
			GeelbeCashDAO::GuardarDN($dn);
			
			// Envio email de transaccion aceptada
	        /*$emailBuilder = new EmailBuilder();
	        $emailData = $emailBuilder->generateGeelbeCashTrxAcceptedEmail($objUsuario, $transaccion->getMonto());
	            
		    $emailSender = new EmailSender();
		    $emailSender->sendEmail($emailData);*/
		    
		    //Notificar administradores
		     $oConexion = new Conexion();
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("SELECT * FROM usuarios WHERE IdPerfil IN (SELECT IdPerfil FROM perfiles WHERE notificacion_gcash = 1)");
			$usuarios = $oConexion->DevolverQuery();
			$oConexion->Cerrar_Trans();
		    
			foreach($usuarios as $i => $p) {
			
		        $emailBuilder = new EmailBuilder();
		        $emailData = $emailBuilder->generateGeelbeCashTrxAcceptedEmailAdmin($p['NombreUsuario'], $objUsuario, $transaccion->getMonto());
		            
			    $emailSender = new EmailSender();
			    $emailSender->sendEmail($emailData);
				
			}
		    
		}
	}
	
	public static function cargarCredito($email = null, $monto = null, $motivo = null, $idUsuarioBO = null) {
		$userId = dmUsuario::getIdByNombreUsuario($email);

		if(!isset($userId)) {
			throw new InvalidUserEmailException();
		}
		
		$dn = GeelbeCashDAO::ObtenerDNPorIdUsuario($userId);
		
		$transaction = new GeelbeCashTransaction();
		$transaction->setIdUsuario($userId);
		$transaction->setMonto($monto);
		$transaction->setMotivo($motivo);
		$transaction->setFecha(date("Y-m-d H:i:s"));
		$transaction->setEstado(GeelbeCashTransaction::$ACEPTADO);
		
		$trx_id = GeelbeCashDAO::Guardar($transaction);
		
		// Actualizo desnormalizaciones
		$montoCargasHistoricas = ($dn->getMontoCargasHistoricas())?$dn->getMontoCargasHistoricas():0;
		$montoSaldoActual = ($dn->getMontoSaldoActual())?$dn->getMontoSaldoActual():0;
		$dn->setMontoCargasHistoricas($montoCargasHistoricas + $monto);
		$dn->setMontoSaldoActual($montoSaldoActual + $monto);
		GeelbeCashDAO::GuardarDN($dn);
		
		if (isset($idUsuarioBO)) {
			GeelbeCashDAO::GuardarCargaCredito($trx_id, $idUsuarioBO);
		}
		
		return $trx_id;
	}
	
	public static function cancelarTransaccion($trx_id = null) {
		$transaccion = GeelbeCashDAO::ObtenerPorId($trx_id);
		
		if($transaccion->getEstado() != GeelbeCashTransaction::$ACEPTADO) {
			$transaccion->setEstado(GeelbeCashTransaction::$CANCELADO);
			GeelbeCashDAO::Guardar($transaccion);
			
			// Envio email de transaccion cancelada
			$objUsuario = dmUsuario::getByIdUsuario($transaccion->getIdUsuario());
			
	        $emailBuilder = new EmailBuilder();
	        $emailData = $emailBuilder->generateGeelbeCashTrxCancelledEmail($objUsuario, $transaccion->getMonto());
	            
		    $emailSender = new EmailSender();
		    $emailSender->sendEmail($emailData);
		}
		
	}

	public static function anularTransaccion($trx_id = null, $motivo = "", $idUsuarioBO = null) {
		$transaccion = GeelbeCashDAO::ObtenerPorId($trx_id);
		
		if ($transaccion->getId() == null) {
			throw new InvalidTrxIdException();
		}
		
		$trxStatus = $transaccion->getEstado();
		
		if ($trxStatus == GeelbeCashTransaction::$ACEPTADO) {
			$dn = GeelbeCashDAO::ObtenerDNPorIdUsuario($transaccion->getIdUsuario());
			
			$montoCargasHistoricas = ($dn->getMontoCargasHistoricas())?$dn->getMontoCargasHistoricas():0;
			$montoSaldoActual = ($dn->getMontoSaldoActual())?$dn->getMontoSaldoActual():0;
			
			$dn->setMontoCargasHistoricas($montoCargasHistoricas - $transaccion->getMonto());
			$dn->setMontoSaldoActual($montoSaldoActual - $transaccion->getMonto());
			
			GeelbeCashDAO::GuardarDN($dn);
			
		} else if ($trxStatus == GeelbeCashTransaction::$ANULADO ||	$trxStatus == GeelbeCashTransaction::$CANCELADO) {
			throw new AlreadyRevokeException();
		}

		$transaccion->setEstado(GeelbeCashTransaction::$ANULADO);
		GeelbeCashDAO::Guardar($transaccion);
		
		GeelbeCashDAO::GuardarAnulacion($transaccion, $motivo, $idUsuarioBO);
	}
	
	public static function utilizarGeelbeCash($user_id = null, $monto = 0) {
		$dn = GeelbeCashDAO::ObtenerDNPorIdUsuario($user_id);

		$montoGastosHistoricos = ($dn->getMontoGastosHistoricos())?$dn->getMontoGastosHistoricos():0;
		$montoSaldoActual = ($dn->getMontoSaldoActual())?$dn->getMontoSaldoActual():0;
		
		$dn->setMontoGastosHistoricos($montoGastosHistoricos + $monto);
		$dn->setMontoSaldoActual($montoSaldoActual - $monto);
		$dn->setMontoUltimoGasto($monto);
		
		GeelbeCashDAO::GuardarDN($dn);
	}

}

?>