<?php

	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_transaction.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_dn.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_service.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/datamappers/dmgeelbecash.php";
	
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: http://'.$_SERVER['HTTP_HOST']."/".$confRoot[1]);
		exit;
    }
	
	$userId = Aplicacion::Decrypter($_SESSION["User"]["id"]);
	$CreditCharge = $_REQUEST["CreditCharge"];
	
	if(!isset($CreditCharge)) {
		header('Location: /'.$confRoot[1]."/ssl/geelbecash/index.php?error_type=ammount_not_selected");
		exit;
	} else {
		switch ($CreditCharge) {
		    case GeelbeCashTransaction::$TRX_10000_ID:
		        $Monto = 10000;
		        break;
		    case GeelbeCashTransaction::$TRX_25000_ID:
		        $Monto = 25000;
		        break;
			case GeelbeCashTransaction::$TRX_50000_ID:
		        $Monto = 50000;
		        break;
		    case GeelbeCashTransaction::$TRX_85000_ID:
		        $Monto = 85000;
		        break;
		    case GeelbeCashTransaction::$TRX_100000_ID:
		        $Monto = 100000;
		        break;
		    case GeelbeCashTransaction::$TRX_150000_ID:
		        $Monto = 150000;
		        break;
		    case GeelbeCashTransaction::$TRX_200000_ID:
		        $Monto = 200000;
		        break;
		    case GeelbeCashTransaction::$TRX_300000_ID:
		        $Monto = 300000;
		        break;
		    case GeelbeCashTransaction::$TRX_400000_ID:
		        $Monto = 400000;
		        break;
		    case GeelbeCashTransaction::$TRX_500000_ID:
		        $Monto = 500000;
		        break;
			default:
				header('Location: /'.$confRoot[1]."/ssl/geelbecash/index.php?error_type=ammount_not_selected");
				exit;
		}
	}
	
	
	// Guardar la transaccion
	$TRX_ID = "gc_".GeelbeCashService::generarTransaccion($userId, $Monto, "Compra GCash");
	
	$useStub = Aplicacion::getParametros("environment_configuration", "use_payment_stub");
	$key = '13220797fc3';
	$usr_id = '74286';
	$mediosDePago = "2,4,5,7,8";
	$mediosDePago2 = $mediosDePago;
	
	if ($useStub=="true") {
		$urlPagosOnline = "https://gateway2.pagosonline.net/apps/gateway/index.html";
	} else {
		$urlPagosOnline = "https://gateway.pagosonline.net/apps/gateway/index.html";
	}
	
    $clave = md5($key.'~'.$usr_id.'~'.$TRX_ID.'~'.$Monto.'~COP');
	$urlRespuesta = "https://".$_SERVER['HTTP_HOST']."/".$confRoot[1]."/ssl/geelbecash/post-pago.php";
	$urlConfirmacion = "http://".$_SERVER['HTTP_HOST']."/".$confRoot[1]."/logica/geelbecash/ipn_pagosonline.php";
	
	?>
    <html>
    	<body>
        	<form method="post" action="<?=$urlPagosOnline?>">
            	<input name="usuarioId" type="hidden" value="<?=$usr_id?>">
            	<input name="descripcion" type="hidden" value="Compra GCash - Geelbe Colombia">
            	<input name="refVenta" type="hidden" value="<?=$TRX_ID?>">
            	<input name="valor" type="hidden" value="<?=$Monto?>">
            	<input name="moneda" type="hidden" value="COP">
            	<input name="firma" type="hidden" value="<?=$clave?>">
            
            	<input name="emailComprador" type="hidden" value="<?=Aplicacion::Decrypter($_SESSION['User']['email']);?>">
            	<input name="url_respuesta" type="hidden" value="<?=$urlRespuesta?>">
            	<input name="url_confirmacion" type="hidden" value="<?=$urlConfirmacion?>">
				<input type="hidden" name="tipo_medio_pago" value="<?=$mediosDePago?>" />
				<input type="hidden" name="tiposMediosDePago" value="<?=$mediosDePago2?>" />
				<input type="hidden" name="extra1" value="-" />
				<input type="hidden" name="extra2" value="-" />
	<?
		if ($useStub=="true") {
	?>
        		<input name="prueba" type="hidden" value="1">
	<?
		}
	?>
            </form>
            <script>
            	document.forms[0].submit();
            </script>
		</body>
	</html>