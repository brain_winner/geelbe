<?php
    class clsLoginH
    {
        private $_idLogin = 0;
        private $_NombreUsuario = "";
        private $_Nombre = "";
        private $_Apellido = "";
        private $_Fecha = "";
        
        public function setNombreUsuario($valor)
        {
            $this->_NombreUsuario = $valor;
        }
        public function setNombre($valor)
        {
            $this->_Nombre = $valor;
        }
        public function setApellido($valor)
        {
            $this->_Apellido = $valor;
        }
        public function setFecha($valor)
        {
            $this->_Fecha = $valor;
        }
        
        public function getIdLogin()
        {
            return $this->_idLogin;
        }
        public function getNombreUsuario()
        {
            return $this->_NombreUsuario;
        }
        public function getNombre()
        {
            return $this->_Nombre;
        }
        public function getApellido()
        {
            return $this->_Apellido;
        }
        public function getFecha()
        {
            return $this->_Fecha;
        }
    }
    
?>
