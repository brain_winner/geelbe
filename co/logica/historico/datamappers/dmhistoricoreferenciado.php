<?php
    class dmReferenciadoH
    {
        public static function Guardar(clsReferenciadoH $oRefH)
        {
            $oConexion = ConexionHistorico::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $sql = "INSERT INTO ".Aplicacion::getParametros("conexionH","base").".referenciados(Nombre, Apellido, Fecha, MailAhijado, NombreUsuario) VALUES('".htmlspecialchars(mysql_real_escape_string($oRefH->getNombre()), ENT_QUOTES)."', '".htmlspecialchars(mysql_real_escape_string($oRefH->getApellido()), ENT_QUOTES)."', '".$oRefH->getFecha()."', '".htmlspecialchars(mysql_real_escape_string($oRefH->getMailAhijado()), ENT_QUOTES)."', '".htmlspecialchars(mysql_real_escape_string($oRefH->getNombreUsuario()), ENT_QUOTES)."')";
                $oConexion->setQuery($sql);
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch(exception $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
		/* DEPRECATED
        public static function ObtenerTabla($columna, $sentido)
        {
            $oConexion = ConexionHistorico::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT NombreUsuario as 'Nombre de usuario', CONCAT(CONCAT(Apellido, ', '), Nombre) AS 'Apellido y nombre', MailAhijado as 'Mail ahijado', Fecha FROM ".Aplicacion::getParametros("conexionH","base").".referenciados ORDER BY $columna $sentido");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
*/
		public static function ObtenerReferenciadosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
            $filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
			$oConexion = ConexionHistorico::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT R.NombreUsuario as 'R.NombreUsuario', 
				                             R.Apellido as 'R.Apellido', 
											 R.Nombre as 'R.Nombre', 
											 R.MailAhijado as 'R.MailAhijado', 
											 R.Fecha as 'R.Fecha' 
				                      FROM ".Aplicacion::getParametros("conexionH","base").".referenciados R 
									  WHERE ($filtersSQL)
									  ORDER BY $sortSQL 
									  LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }

		public static function ObtenerReferenciadosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);

            $oConexion = ConexionHistorico::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(*) as total 
				                      FROM ".Aplicacion::getParametros("conexionH","base").".referenciados R 
									  WHERE (".$filtersSQL.")");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
	
    }
?>
