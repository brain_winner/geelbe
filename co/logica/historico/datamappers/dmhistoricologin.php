<?php
	class dmLoginH
    {
        public static function Guardar(clsLoginH $oLoginH)
        {
            $oConexion = ConexionHistorico::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $sql = "INSERT INTO ".Aplicacion::getParametros("conexionH","base").".logins(NombreUsuario, Nombre, Apellido, Fecha) VALUES('".htmlspecialchars(mysql_real_escape_string($oLoginH->getNombreUsuario()), ENT_QUOTES)."', '".htmlspecialchars(mysql_real_escape_string($oLoginH->getNombre()), ENT_QUOTES)."', '".htmlspecialchars(mysql_real_escape_string($oLoginH->getApellido()), ENT_QUOTES)."', '".$oLoginH->getFecha()."')";
                $oConexion->setQuery($sql);
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch(exception $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }
		        public static function ObtenerLogueadosActualmente($segundosDiferencia)
        {
            $oConexion = ConexionHistorico::nuevo();
            try
            {
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT DISTINCT NombreUsuario, CONCAT(CONCAT(Apellido, ', '), Nombre) AS 'Apellido y nombre', Fecha FROM ".Aplicacion::getParametros("conexionH","base").".logins WHERE NOW() <= DATE_ADD(Fecha, INTERVAL $segundosDiferencia SECOND) ORDER BY Fecha DESC");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                return $Tabla;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
		
		
		public static function getLoginHistoricoCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);

            $oConexion = Conexion::nuevo();
            try
            {
				$oConexion->Abrir_Trans();
				$oConexion->setQuery("	SELECT 	count(*) as total 
										FROM ".Aplicacion::getParametros("conexionH","base").".logins L 
										WHERE  ($filtersSQL) ");
                $Tabla = $oConexion->DevolverQuery();
				$oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }		
		
		public static function getLoginHistoricoPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
		    $filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
            $oConexion = Conexion::nuevo();
            try
            {
			
                $oConexion->Abrir_Trans();
				$oConexion->setQuery("	SELECT 	L.NombreUsuario as 'L.NombreUsuario', 
												L.Apellido as 'L.Apellido', 
												L.Nombre as 'L.Nombre', 
												L.Fecha as 'L.Fecha' 
										FROM ".Aplicacion::getParametros("conexionH","base").".logins L
										WHERE ($filtersSQL)  
										ORDER BY $sortSQL 
										LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla;
            }
            catch (MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
		
        public static function ObtenerUltimoLogin($NombreUsuario)
        {
            $oConexion = ConexionHistorico::nuevo();
            try
            {
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT * FROM ".Aplicacion::getParametros("conexionH","base").".logins WHERE NombreUsuario = \"".$NombreUsuario."\" ORDER BY FECHA DESC LIMIT 0,1");
                $Fila = $oConexion->DevolverQuery();
                $oConexion->Cerrar();
                return $Fila[0]["Fecha"];
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
    }
?>