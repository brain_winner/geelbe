<?

$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));

require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";

	abstract class clsMailer{

    	public $arrEmail;
    	public $oTexto;
    	protected $id;
    	protected $arrDatos;
    	protected $nombreEmail;


     	public function __construct($email, $id){
     		$nombreEmail = $email;
     		$this->id = $id;
     		$this->nombreMail($email);
			$this->obtenerDatosUsuario();
			$this->reemplazarString();
     	}

     	protected function nombreMail($email){
			$this->arrEmail = Aplicacion::getEmail($email);
     	}
		abstract protected function obtenerDatosUsuario();
		abstract function reemplazarString();

		abstract protected function enviarMail();

		 public function preview(){
			return $this->oTexto->getText();
    	}


     	public function enviar(){
			$this->enviarMail();
    	}

    }

	class clsMailerPedidos extends clsMailer{
		protected function obtenerDatosUsuario(){
			$this->arrDatos = dmPedidos::getDatosMailByIdPedido($this->id);
		}

		
		function reemplazarString(){
			
			$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
	        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
	        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
	        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
	        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/datamappers/dmdescuentos.php");
	        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/clases/clsdescuentos.php");
	        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
	        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
			
			$oTexto = new Texto(Aplicacion::getRoot() . $this->arrEmail["url"]);
			$oTexto->Read();
			$oTexto->ReemplazarString("%NombreVidriera%",$this->arrDatos["nombreVidriera"]);
			$oTexto->ReemplazarString("%NumeroPedido%",str_pad($this->arrDatos["numeroPedido"], 8, 0, STR_PAD_LEFT));
			$oTexto->ReemplazarString("%EstadoPedido%",$this->arrDatos["estadoPedido"]);
			$oTexto->ReemplazarString("%numerotrackingdhl%",$this->arrDatos["numeroTrackingDHL"]);
			$oTexto->ReemplazarString("%IdUsuario%",$this->arrDatos["IdUsuario"]);
			$oTexto->ReemplazarString("%HashIdUsuario%",generar_hash_md5($this->arrDatos["IdUsuario"]));
			$oTexto->ReemplazarString("%Hash%", Hash::getHash($this->arrDatos["IdUsuario"].$this->arrDatos["emailUsuario"]));
			$oTexto->ReemplazarString("%NombreUsuario%",$this->arrDatos["nombreUsuario"]);
			$oTexto->ReemplazarString("%ApellidoUsuario%",$this->arrDatos["apellidoUsuario"]);
			$oTexto->ReemplazarString("%EmailUsuario%",$this->arrDatos["emailUsuario"]);
			$oTexto->ReemplazarString("%IdPadrino%",$this->arrDatos["IdPadrino"]);
			$oTexto->ReemplazarString("%HashIdPadrino%",generar_hash_md5($this->arrDatos["IdPadrino"]));
			$oTexto->ReemplazarString("%NombrePadrino%",$this->arrDatos["nombrePadrino"]);
			$oTexto->ReemplazarString("%ApellidoPadrino%",$this->arrDatos["apellidoPadrino"]);
			$oTexto->ReemplazarString("%EmailPadrino%",$this->arrDatos["emailPadrino"]);
			$oTexto->ReemplazarString("%Direccion%", $this->arrDatos["direccion"]);
			$oTexto->ReemplazarString("%Departamento%", $this->arrDatos["departamento"]);
			$oTexto->ReemplazarString("%Ciudad%", $this->arrDatos["ciudad"]);
			$oTexto->ReemplazarString("%FormaPago%", $this->arrDatos["formapago"]);
			
			$in = strtotime($this->arrDatos["TiempoEntregaIn"]);
			$fn = strtotime($this->arrDatos["TiempoEntregaFn"]);
			$oTexto->ReemplazarString("%TiempoEntregaIn%", date("d", $in).' '.mes(date("n", $in)));
			$oTexto->ReemplazarString("%TiempoEntregaFn%", date("d", $fn).' '.mes(date("n", $fn)));
			$oTexto->ReemplazarString("%CodigoDeBarras%",$this->arrDatos["codigodebarra"]);
			$oTexto->ReemplazarString("%Vencimiento%",$this->arrDatos["fechaVencimiento"]);
			$oTexto->ReemplazarString("%CostoEnvio%",number_format($this->arrDatos["envio"], 0, ',', '.'));
			$oTexto->ReemplazarString("%Total%",number_format($this->arrDatos["total"], 0, ',', '.'));
			$oTexto->ReemplazarString("%HashPedido%",generar_hash_md5($this->arrDatos["numeroPedido"]));
			$oTexto->ReemplazarString("%IdPedido%",$this->arrDatos["numeroPedido"]);
			$oTexto->ReemplazarString("%Ruta URL%",Aplicacion::getRootUrl());
			$oTexto->ReemplazarString("%year%",date("Y"));
			$oTexto->ReemplazarString("%Productos%",$this->tablaProductos($this->id));
			$this->oTexto = $oTexto;
			

			$this->arrEmail["asunto"] = str_ireplace("%NombreVidriera%",$this->arrDatos["nombreVidriera"],$this->arrEmail["asunto"]);
			$this->arrEmail["asunto"] = str_ireplace("%NombreUsuario%",$this->arrDatos["nombreUsuario"],$this->arrEmail["asunto"]);
			$this->arrEmail["asunto"] = str_ireplace("%NombrePadrino%",$this->arrDatos["nombrePadrino"],$this->arrEmail["asunto"]);
			$this->arrEmail["asunto"] = str_ireplace("%NumeroPedido%",str_pad($this->arrDatos["numeroPedido"], 8, 0, STR_PAD_LEFT),$this->arrEmail["asunto"]);
	     }
	     
	     function tablaProductos($id) {
		     
		     $pedido = dmPedidos::getPedidoById($id);
		     $html = '';
		     foreach($pedido->getProductos() as $prod) {
			     
			     $producto = dmProductos::getByIdCodigoProdInterno($prod->getIdCodigoProdInterno());
			     $producto = dmProductos::getById($producto->getIdProducto());

			     $arrImg = @DirectorytoArray(Aplicacion::getRoot() . "front/productos/productos_".$producto->getIdProducto()."/", 3, array("jpg", "jpeg", "gif", "png"));

			     $html .= '<tr>
			     		<td style="border-left:3px solid #dadada;padding:20px 10px 20px 0;font-family:Arial;font-size:18px">
			     			<img src="'.UrlResolver::getImgBaseUrl("front/productos/productos_".$producto->getIdProducto()."/producto_1.".get_extension($arrImg["producto_1"])).'" width="80" style="float:left;margin-right:10px;" />
			     		</td>
						<td style="padding:20px;font-family:Arial;font-size:18px;border-right:3px solid #dadada">
							'.$producto->getNombre().'
						</td>
						<td style="border-right:3px solid #dadada;font-family:Arial;font-size:18px;text-align:center">
							'.$prod->getCantidad().'
						</td>
						<td style="font-family:Arial;font-size:14px;padding-left:10px;">
							Subtotal
						</td>
						<td style="font-family:Arial;font-size:18px;text-align:right;padding-right:10px">
							$'.number_format($prod->getPrecio(), 0, ',', '.').'
						</td>
					</tr>';
			     
		     }

		     return $html;
		     
	     }


	    protected function enviarMail(){
			$emailBuilder = new EmailBuilder();
	        $emailData = $emailBuilder->generateAnEmail($this->arrDatos["emailUsuario"], $this->oTexto->getText(), $this->nombreEmail, $this->arrEmail["asunto"]);
			$emailSender = new EmailSender();
			$emailSender->sendEmail($emailData);
	    	
			return true;
		}

		public function enviarPadrino($email){
			$this->nombreMail($email);
			$this->reemplazarString();

			$emailBuilder = new EmailBuilder();
	        $emailData = $emailBuilder->generateAnEmail($this->arrDatos["emailPadrino"], $this->oTexto->getText(), $this->nombreEmail, $this->arrEmail["asunto"]);
	        
			$emailSender = new EmailSender();
			$emailSender->sendEmail($emailData);
	    	
			return true;
		}
	}
	
	function mes($mes) {
		$meses = array(1 => 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		return $meses[$mes];
	}

	?>