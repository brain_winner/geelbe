<?
class ConexionHistorico extends Conexion
{
	/*
	//private static $instance;

    function __construct()
    {
        $this->usuario = Aplicacion::getParametros("conexion", "usuario");
        $this->password = Aplicacion::Decrypter(Aplicacion::getParametros("conexion", "password"));
        $this->server = Aplicacion::getParametros("conexion", "server");
        $this->base = Aplicacion::getParametros("conexionH","base");
        $this->vlink="";
    }


    public static function nuevo(){
		if(empty(self::$instance))
		{
			self::$instance = new ConexionHistorico();
		}

		return self::$instance;
	}*/
}

class Conexion
{
	protected $usuario;
	protected $password;
	protected $server;
	protected $base;
	protected $vlink;
	protected $query;
	public static $instance;
	protected $log;

	function __construct()
	{
        $this->usuario = Aplicacion::getParametros("conexion", "usuario");
        $this->password = Aplicacion::Decrypter(Aplicacion::getParametros("conexion", "password"));
        $this->server = Aplicacion::getParametros("conexion", "server");
        $this->base = Aplicacion::getParametros("conexion","base");
        $this->vlink="";
        $this->AbrirDefault();
        $this->log = array();
	}

	public static function nuevo(){
		if(empty(self::$instance))
		{
			self::$instance = new Conexion();
			self::$instance->Abrir();
		}

		return self::$instance;
	}


	protected function AbrirDefault()
	{
		try
		{
			if(empty($this->vlink)){
			$this->vlink = mysql_connect($this->server, $this->usuario,$this->password);
			if (mysql_errno() == 1203) {
				die('Por razones de seguridad, hemos limitado la cantidad de transacciones simultáneas, realizá tu compra nuevamente en unos minutos');
			}

			if (!$this->vlink)
				throw new MySQLException("Error al conectar.", mysql_errno(), mysql_error());
			else
				mysql_select_db($this->base, $this->vlink);
			}
		}
		catch(MySQLException $e)
		{
			$e->setMensajeSQLERROR(mysql_error());
            $e->setNumeroSQLERROR(mysql_errno());
            throw $e;
		}
	}
	public function Abrir(){}
	public function Cerrar()
	{
		try
		{
			//mysql_close($this->vlink);
		}
		catch(MySQLException $e)
		{
			$e->setMensajeSQLERROR(mysql_error());
            $e->setNumeroSQLERROR(mysql_errno());
            throw $e;
		}
	}
	public function Abrir_Trans()
	{
		try
		{
			if(empty($this->vlink)){
				$this->vlink = mysql_connect($this->server, $this->usuario,$this->password);
				if (mysql_errno() == 1203) {
					die('Por razones de seguridad, hemos limitado la cantidad de transacciones simultáneas, realizá tu compra nuevamente en unos minutos');
				}

				if (!$this->vlink){
					throw new MySQLException("Error de conexion", mysql_errno(), mysql_error());
				}else				{
					mysql_select_db($this->base, $this->vlink);
				}
			}
			
			$this->setQuery("BEGIN");
			$this->EjecutarQuery();

		}
		catch(MySQLException $e)
		{
			throw $e;
		}
	}
	public function Cerrar_Trans()
	{
		try
		{
			$this->setQuery("COMMIT");
			$this->EjecutarQuery();
			//mysql_close($this->vlink);
		}
		catch(MySQLException $e)
		{
			$e->setMensajeSQLERROR(mysql_error());
            $e->setNumeroSQLERROR(mysql_errno());
            throw $e;
		}
	}
	public function setQuery($query)
	{
		$this->query = $query;
	}
	public function getQuery()
	{
		return $this->query;
	}
	public function RollBack()
	{
		try
        {
            $this->setQuery("ROLLBACK");
            $this->EjecutarQuery();
        }
        catch(exception $e)
        {
            $e->setMensajeSQLERROR(mysql_error());
            $e->setNumeroSQLERROR(mysql_errno());
            throw $e;
        }
	}
	public function EjecutarQuery()
	{
		try
		{
			if (!($result=mysql_query($this->query, $this->vlink)))
				throw new MySQLException("Error en EjecutarQuery - " . $this->getQuery(), mysql_errno(), mysql_error());
			$this->log[] = array("query" => $this->query, "info" => "EjecutarQuery");
			return $result;
		}
		catch(MySQLException $e)
		{
			throw $e;
		}
	}
	public function DevolverLog(){
		echo "<div class='debug'>\r\n";
		echo "<pre>\r\n";
		var_dump($this->log);
		echo "</pre>\r\n";
	}
	public function DevolverQuery()
	{
		try
		{
			if(!($result=mysql_query($this->query, $this->vlink)))
				throw new MySQLException("Error en DevolverQuery - " . $this->getQuery(), mysql_errno(), mysql_error());
			$Coleccion = array();
			while ($fila = mysql_fetch_assoc($result))
				array_push($Coleccion, $fila);
				$cant = count($Coleccion);
			if(count($Coleccion)>100){
				$this->log[] = array("query" => $this->query, "info" => "DevolverQuery", "count" => $cant);
			}else{
				$this->log[] = array("query" => $this->query, "info" => "DevolverQuery", "count" => $cant, "filas" => $Coleccion);
			}
			return $Coleccion;
		}
		catch(MySQLException $e)
		{
			throw $e;
		}
	}

	public function LastId(){
		return mysql_insert_id($this->vlink);
	}



}
?>