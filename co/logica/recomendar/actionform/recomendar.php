<?
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("scripts"));
    
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
    require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
?>
<html>
    <head>
        <?=Includes::Scripts();?>
    </head>
    <body>
<?php
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("catalogo"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
    $MAIL = Aplicacion::getIncludes("onlyget", "htmlRecomendarProducto");
    if ($_POST)
    {
        try
        {
            extract($_POST);
			try {
		$isPageOk = true;
		$pageException = "Exception";
		
		// Validaciones de parametros basicas
		if(!isset($_POST["IdCampania"]) && !isset($_POST["IdProducto"])) { // IdCampania y IdProducto Seteado
            $isPageOk = false;
			$pageException = "IdCampania or IdProducto not setted";
			echo "IdCampania or IdProducto not setted";
		}
		else if (!ValidatorUtils::validateNumber($_POST["IdCampania"])) { // IdCampania Numerico
			$isPageOk = false;
			$pageException = "IdCampania is not a number";
		}
		else if (!ValidatorUtils::validateNumber($_POST["IdProducto"])) { // IdProducto Numerico
			$isPageOk = false;
			$pageException = "IdProducto is not a number";
		}
		else if (!dmProductos::isCampaignProduct($_POST["IdProducto"], $_POST["IdCampania"])) { // IdProducto Correspondiente con IdCampania
			$isPageOk = false;
			$pageException = "IdProducto no corresponde con IdCampania";
		}
		else if (!dmCampania::EsCampaniaAccesible($_POST["IdCampania"])) { // IdCampania Accesible
			$isPageOk = false;
			$pageException = "Campa�a not accesible";
		}
		else if (isset($_POST["IdCategoria"])) { 
			if (!ValidatorUtils::validateNumber($_POST["IdCategoria"])) { // IdCategoria Numerico
				$isPageOk = false;
				$pageException = "IdCategoria is not a number";
			}
			else if (!dmCampania::isCampaignCategory($_POST["IdCampania"], $_POST["IdCategoria"])) { // IdCategoria Correspondiente con IdCampania
				$isPageOk = false;
				$pageException = "IdCampania no corresponde con IdCategoria";
			}
		}
		else if (!isset($_POST["txtEmailAmigo"]) || empty($_POST["txtEmailAmigo"])) { // Email vacio
			echo generarJS("window.parent.ErrorMSJ","Error de registro.", "MAIL_INCORRECTO");
			exit;
		} 
		else if (!ValidatorUtils::validateEmail($_POST["txtEmailAmigo"])) { // Formato del email incorrecto
			echo generarJS("window.parent.ErrorMSJ","Error de registro.", "MAIL_INCORRECTO");
			exit;
		} 
		if (!$isPageOk) {
			throw new Exception($pageException);
		}

	}
    catch(Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
		header('HTTP/1.1 404 Not Found');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }
            $objProducto = dmProductos::getById($IdProducto);
            $objCampania = dmCampania::getByIdCampania($IdCampania);
            $ApeNombreUsuario = dmContacto::ObtenerApenomByEmail($txtEmailAmigo);
            $idAmigo = dmUsuario::getIdByNombreUsuario($txtEmailAmigo);
			
            if (dmUsuario::EsUsuarioRegistrado(dmUsuario::getIdByNombreUsuario($txtEmailAmigo))) {
                $emailBuilder = new EmailBuilder();
	            $emailData = $emailBuilder->generateRecomendacionProductoRegistradoEmail($txtEmailAmigo, $_SESSION["User"]["nombre"], $_SESSION["User"]["apellido"], $objCampania, $objProducto, $idAmigo, $_SESSION["User"]["email"], $IdCampania, $IdCategoria, $IdProducto);
	        }
            else {
            	$emailBuilder = new EmailBuilder();
	            $emailData = $emailBuilder->generateRecomendacionProductoNoRegistradoEmail($txtEmailAmigo, $_SESSION["User"]["nombre"], $_SESSION["User"]["apellido"], $objCampania, $objProducto, $idAmigo, $_SESSION["User"]["email"], $IdCampania, $IdCategoria, $IdProducto);
	        }
            
			$emailSender = new EmailSender();
			$emailSender->sendEmail($emailData);
            
            ?>
                <script>
                    window.parent.msjInfo("Se ha enviado la recomendaci&oacute;n", "Recomendar");
                </script>
            <?
        }
        catch(Exception $e)
        {        	
            ?>
                <script>
                    window.parent.msjInfo("Error: no se ha enviado la recomendaci&oacute;n", "Recomendar");
                </script>
            <?
        }
    }
?>
    </body>
</html>
