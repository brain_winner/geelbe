<?

require_once "nusoap/nusoap.php";

define ("MWS_URL", "http://201.131.4.28/webclubunico/service.asmx");
define ("MWS_ID_EMPRESA", "5");
define ("MWS_SUCURSAL", "Mexico");

define ("MWS_APLICA_NAMESPACE", "http://tempuri.org/");
define ("MWS_APLICA_SOAP_ACTION", "http://tempuri.org/Redimir_Folio");
define ("MWS_APLICA_CONTAINER", "Redimir_Folio");

define ("MWS_CONSULTA_NAMESPACE", "http://tempuri.org/");
define ("MWS_CONSULTA_SOAP_ACTION", "http://tempuri.org/Redimir");
define ("MWS_CONSULTA_CONTAINER", "Redimir");

class MovistarWSProxy {

	
	public function sendConsultaMessage($userCode) {
		$client = new nusoap_client(MWS_URL, false, '', '', '', '');
		$client->soap_defencoding = "utf-8";
		$params = array(
		    'DN' => $userCode,
		    'CAMPANA' => MWS_ID_EMPRESA,
		    'SUCURSAL' => MWS_SUCURSAL,
		);
	
		$result = $client->call(MWS_CONSULTA_CONTAINER, $params, MWS_CONSULTA_NAMESPACE, MWS_CONSULTA_SOAP_ACTION, false, null, 'rpc', 'literal');

		if ($client->fault) {
			throw new Exception('Problemas en la invocacion del ws: '.MWS_CONSULTA_CONTAINER.' El resultado de la consulta es: '.$result);
		} else {
			$err = $client->getError();
			if ($err) {
				throw new Exception('Problemas en la invocacion del ws: '.MWS_CONSULTA_CONTAINER.' El error recibido es: '.$err);
			} else {
				return $result;
			}
		}
	}
	
	public function sendAplicaMessage($userCode) {
		$client = new nusoap_client(MWS_URL, false,	'', '', '', '');
		$client->soap_defencoding = "utf-8";
		$params = array(
		    'DN' => $userCode,
		    'CAMPANA' => MWS_ID_EMPRESA,
		);
		
		$result = $client->call(MWS_APLICA_CONTAINER, $params, MWS_APLICA_NAMESPACE, MWS_APLICA_SOAP_ACTION, false, null, 'rpc', 'literal');
		
		if ($client->fault) {
			throw new Exception('Problemas en la invocacion del ws: '.MWS_APLICA_CONTAINER.' El resultado de la consulta es: '.$result);
		} else {
			$err = $client->getError();
			if ($err) {
				throw new Exception('Problemas en la invocacion del ws: '.MWS_APLICA_CONTAINER.' El error recibido es: '.$err);
			} else {
				return $result;
			}
		}
	}
	
}

?>