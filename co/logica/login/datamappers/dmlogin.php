<?
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
	class dmUsuarios
    {
    
    	/**
    	 * Dice si un usuario es activo.
    	 *
    	 * @param integer $idUsuario
    	 * @return bool true si es logueable.
    	 */
    	public static function esLogueable($idUsuario){
    	
			intval($cookie["user"]);
			$idUsuario = intval($idUsuario);
			$oConexion = Conexion::nuevo();
			$oConexion->Abrir();
			$oConexion->setQuery("SELECT COUNT(*) as Cant FROM usuarios WHERE es_activa = 0 AND IdUsuario = '".mysql_real_escape_string($idUsuario)."'");
			
			$cant = $oConexion->DevolverQuery();			    	
			if($cant[0]["Cant"]>0){			    	
				return true;
			}       
			return false;
    	}
    	
    	
        /**
        * @desc Obtiene los datos personales del usuario
        * @param int. IdUsuario
        * @return array.
        */
        public function getDatosPersonales($IdUsuario)
        {
            try
            {
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT * FROM datosusuariospersonales WHERE IdUsuario = '".mysql_real_escape_string($idUsuario)."'");
                $Tabla = $oConexion->DevolverQuery();
               // if (count($Tabla != 1))
               //     throw new MySQLException("Email no existente en la base de datos");
                
               
                $oConexion->Cerrar();
                return $Tabla[0];                
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        /**
        * @desc Obtiene todos los datos del usuario a partir del nombre de usuario
        * @return Array
        */
        public function getDatosUsuarios($NombreUsuario)
        {
            try
            {
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT * FROM usuarios WHERE NombreUsuario = '".mysql_real_escape_string($NombreUsuario)."'");
                $Tabla = $oConexion->DevolverQuery();
               // if (count($Tabla != 1))
               //     throw new MySQLException("Email no existente en la base de datos");
                
               
                $oConexion->Cerrar();
                return $Tabla[0];
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }

        public static function changeBase64ToMd5($IdUsuario, $email) {
        	try {
	        	$oConexion = Conexion::nuevo();
	        	$oConexion->Abrir();
	        	$oConexion->setQuery("SELECT Clave as 'pass' FROM usuarios WHERE IdUsuario = '".mysql_real_escape_string($idUsuario)."'");
	            $Tabla = $oConexion->DevolverQuery();
	            
	            $passBase64 = $Tabla[0]["pass"];
	            $passRAW = Aplicacion::Decrypter($passBase64);
	            $passMd5 = Hash::getHash($passRAW);
	        	
	            $oConexion->setQuery("update usuarios set Clave='".mysql_real_escape_string($passMd5)."' where IdUsuario = ".mysql_real_escape_string($idUsuario));
				$oConexion->EjecutarQuery();
	            
	            echo "ID: ".$IdUsuario." EMAIL: ".$email." B64: ".$passBase64." - RAW: ".$passRAW." - MD5: ".$passMd5."<br/>";
	            
	        	$oConexion->Cerrar();
        	}
            catch(MySQLException $e) {
                throw $e;
            }
			
        }

         /**
        * @desc Verifica que el usuario exista en la base junto con su contraseņa
        * @return objUsuario
        */
        public function Login($Email, $Clave)
        {
        	$oConexion = Conexion::nuevo();
            $oConexion->Abrir();
            $oConexion->setQuery("SELECT IdUsuario, NombreUsuario, Clave, es_Provisoria, Padrino, FechaIngreso, IdPerfil, es_Activa FROM usuarios WHERE NombreUsuario =\"".mysql_real_escape_string($Email)."\"" );
            $Tabla = $oConexion->DevolverQuery();
            if(count($Tabla) >= 1) {
	        	if($Tabla[0]['Clave'] == "HD25h253jlk6jsjfhhkas9HJKSDFG") {
	        		$passMd5 = Hash::getHash($Clave);
	        		$oConexion = Conexion::nuevo();
	                $oConexion->Abrir_Trans();
	                $oConexion->setQuery("UPDATE usuarios SET Clave = \"". mysql_real_escape_string($passMd5) ."\" where NombreUsuario=\"".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($Email)), ENT_QUOTES)."\"");
	                $oConexion->EjecutarQuery();
					$oConexion->Cerrar_Trans();
	        	}
            }
        	
            try
            {   
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT IdUsuario, NombreUsuario, Clave, es_Provisoria, Padrino, FechaIngreso, IdPerfil, es_Activa FROM usuarios WHERE NombreUsuario =\"". mysql_real_escape_string($Email) ."\"" );
                $Tabla = $oConexion->DevolverQuery();
                if(count($Tabla) != 1)
                    return false;
                $objUsuario = new usuarios();
                foreach ($Tabla[0] as $atributo => $valor)
                {
                    $objUsuario->__set($valor, $atributo);
                }
                
                if(Hash::compareHash(Hash::getHash($Clave), $objUsuario->getClave()) != 1) {
                	return false;
                }
                
                $oConexion->setQuery("SELECT IdUsuario, FechaNacimiento, Nombre, Apellido, Apellido2, TipoDOc, NroDoc, Telefono, Email, CIF, IdTipoUsuario, es_EnviarDatosCorreo, idTratamiento, es_Completo FROM datosusuariospersonales  WHERE IdUsuario = '". mysql_real_escape_string($objUsuario->getIdUsuario())."'");
                $Datos = $oConexion->DevolverQuery();
                if(count($Datos)>0)
                {
                    $objDatos = new DatosUsuariosPersonales();
                    foreach ($Datos[0] as $atributo => $valor)
                    {
                        $objDatos->__set($valor, $atributo);
                    }
                    $objUsuario->setDatos($objDatos);
                }                
                $oConexion->setQuery("SELECT IdUsuario, TipoDireccion, Domicilio, Numero, Escalera, Piso, Puerta, CP, IdProvincia, IdPais, TipoCalle, Poblacion, Telefono, Completo FROM datosusuariosdirecciones WHERE IdUsuario = '". mysql_real_escape_string($objUsuario->getIdUsuario())."' ORDER BY TipoDireccion");
                $Direcciones = $oConexion->DevolverQuery();
                if($Direcciones[0])
                {
                    $objDireccion = new DatosUsuariosDirecciones();
                    foreach ($Direcciones[0] as $atributo => $valor)
                    {
                        $objDireccion->__set($valor, $atributo);
                    }
                    $objUsuario->setDirecciones(0,$objDireccion);
                }
                if($Direcciones[1])
                {
                    $objDireccion = new DatosUsuariosDirecciones();
                    foreach ($Direcciones[1] as $atributo => $valor)
                    {
                        $objDireccion->__set($valor, $atributo);
                    }
                    $objUsuario->setDirecciones(1,$objDireccion);
                }
                $oConexion->Cerrar();
                return $objUsuario;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }    

        
        public function isUsuarioActivo($Email)
        {
        	
            $oConexion = Conexion::nuevo();
            try {   
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(*) as total 
				                      FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE U.es_activa = 0 AND U.IdPerfil = 1 
									  AND U.NombreUsuario =\"". mysql_real_escape_string($Email) ."\"" );
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch(MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        } 
        
        public function isUsuarioInactivo($Email)
        {
        	
            $oConexion = Conexion::nuevo();
            try {   
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(*) as total 
				                      FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE 
									  U.es_activa = 1 AND U.es_Provisoria = 1 AND U.Padrino <> 'Huerfano' AND U.IdPerfil = 1 
									  AND U.NombreUsuario =\"". mysql_real_escape_string($Email) ."\"" );		  
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch(MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        } 
        
        public function isUsuarioHuerfano($Email)
        {
        	
            $oConexion = Conexion::nuevo();
            try {   
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(*) as total 
									  FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE U.es_activa = 1 AND U.es_Provisoria = 1  AND U.Padrino = 'Huerfano' AND U.IdPerfil = 1
									  AND U.NombreUsuario =\"". mysql_real_escape_string($Email) ."\"" );		  
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch(MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public function isUsuarioDesuscripto($Email)
        {
        	
            $oConexion = Conexion::nuevo();
            try {   
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(*) as total 
				                      FROM usuarios U 
									  LEFT JOIN datosusuariospersonales DUP ON U.IdUsuario = DUP.IdUsuario 
									  WHERE U.es_activa = 1 AND U.es_Provisoria = 0 AND U.IdPerfil = 1
									  AND U.NombreUsuario =\"". mysql_real_escape_string($Email) ."\"" );		  
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return $Tabla[0]['total'];
            }
            catch(MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        } 
        
        public function isValidCode($activationcode)
        {
        	
            $oConexion = Conexion::nuevo();
            try {   
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT count(*) as total 
				                      FROM usuarios U 
									  WHERE U.NombreUsuario =\"". mysql_real_escape_string($activationcode) ."\"" );		  
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                return ($Tabla[0]['total'] > 0);
            }
            catch(MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        } 
        
        public function updatePassword($idUsuario, $hashPassword)
        {
        	
            $oConexion = Conexion::nuevo();
            try {   
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE usuarios 
                                      SET Clave = '$hashPassword' 
                                      WHERE IdUsuario = $idUsuario");
                $Tabla = $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch(MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }  
        
		// Datos: email, password, firstname, lastname, gender, birthdayday, birthdaymonth, birthdayyear
        public function insertUser($email, $hashPassword, $firstname, $lastname, $gender, $birthdayday, $birthdaymonth, $birthdayyear)
    	{
    		
    		if ($firstname==null || $firstname == "") {
    			$firstname = 'Socio Geelbe';
    		}
    		if ($lastname==null) {
    			$lastname = '';
    		}
    		$fechaNacimiento = "'".mysql_real_escape_string($birthdayyear)."-".mysql_real_escape_string($birthdaymonth)."-".mysql_real_escape_string($birthdayday)."'";
    		if ($birthdayyear==null || $birthdaymonth=null || $birthdayday=null) {
    			$fechaNacimiento = "null";
    		}
    		if ($gender==null || $gender=="") {
    			$gender = "null";
    		}
    		
            $oConexion = Conexion::nuevo();
            try {   
            	$email = htmlspecialchars(mysql_real_escape_string($email), ENT_QUOTES);
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("INSERT INTO usuarios (NombreUsuario, Clave, es_Provisoria, Padrino, FechaIngreso, IdPerfil, es_activa) 
                                      VALUES ('$email', '$hashPassword', 1, 'Huerfano', now(), 1, 1)");
                $Tabla = $oConexion->EjecutarQuery();
                $oConexion->setQuery("SELECT IdUsuario as IdUsuario   
				                      FROM usuarios U 
									  WHERE U.NombreUsuario = '".mysql_real_escape_string($email)."'" );		  
                $Tabla = $oConexion->DevolverQuery();
                $IdUsuario = $Tabla[0]['IdUsuario'];
                $oConexion->setQuery("INSERT INTO datosusuariospersonales (IdUsuario, Nombre, Apellido, idTratamiento, FechaNacimiento, codtelefono, celuempresa, celucodigo, celunumero) 
                                      VALUES ($IdUsuario, '".htmlentities(mysql_real_escape_string($firstname))."', '".htmlentities(mysql_real_escape_string($lastname))."', ".htmlentities(mysql_real_escape_string($gender)).", ".$fechaNacimiento.",'', '', '', '')");
                $Tabla = $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch(MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }
        
        public function updatePadrino($idUsuario, $activationcode)
        {
        	$activationcode = htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($activationcode)), ENT_QUOTES);
            $oConexion = Conexion::nuevo();
            try {   
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE usuarios 
                                      SET Padrino = '$activationcode'
                                      WHERE IdUsuario = $idUsuario");
                $Tabla = $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch(MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        } 
        
        public function updateLanding($idUsuario, $landingUrl)
        {
        	
            $oConexion = Conexion::nuevo();
            try {   
            	$landingUrl = htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($landingUrl)), ENT_QUOTES);
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE usuarios 
                                      SET LandingUrl = '$landingUrl'
                                      WHERE IdUsuario = $idUsuario");
                $Tabla = $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch(MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        } 
        
        public function updateUserToInactive($idUsuario)
        {
        	
            $oConexion = Conexion::nuevo();
            try {   
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE usuarios 
                                      SET es_activa = 1, es_Provisoria = 1
                                      WHERE IdUsuario = $idUsuario");
                $Tabla = $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch(MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        }  
        
        public function updateUserToHorphan($idUsuario)
        {
        	
            $oConexion = Conexion::nuevo();
            try {   
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE usuarios 
                                      SET es_activa = 1, es_Provisoria = 1, Padrino = 'Huerfano'
                                      WHERE IdUsuario = $idUsuario");
                $Tabla = $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
            }
            catch(MySQLException $e) {
                $oConexion->RollBack();
                throw $e;
            }
        } 
        
        public function ObtenerUsuarioCompleto($Email)
        {
            try
            {   
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT IdUsuario, NombreUsuario, Clave, es_Provisoria, Padrino, FechaIngreso, IdPerfil, es_Activa FROM usuarios WHERE NombreUsuario =\"". mysql_real_escape_string($Email) ."\"" );
                $Tabla = $oConexion->DevolverQuery();
                if(count($Tabla) != 1)
                    return false;
                $objUsuario = new usuarios();
                foreach ($Tabla[0] as $atributo => $valor)
                {
                    $objUsuario->__set($valor, $atributo);
                }
                
                $oConexion->setQuery("SELECT IdUsuario, FechaNacimiento, Nombre, Apellido, Apellido2, TipoDOc, NroDoc, Telefono, Email, CIF, IdTipoUsuario, es_EnviarDatosCorreo, idTratamiento, es_Completo FROM datosusuariospersonales  WHERE IdUsuario = '". mysql_real_escape_string($objUsuario->getIdUsuario())."'");
                $Datos = $oConexion->DevolverQuery();
                if(count($Datos)>0)
                {
                    $objDatos = new DatosUsuariosPersonales();
                    foreach ($Datos[0] as $atributo => $valor)
                    {
                        $objDatos->__set($valor, $atributo);
                    }
                    $objUsuario->setDatos($objDatos);
                }                
                $oConexion->setQuery("SELECT IdUsuario, TipoDireccion, Domicilio, Numero, Escalera, Piso, Puerta, CP, IdProvincia, IdPais, TipoCalle, Poblacion, Telefono, Completo FROM datosusuariosdirecciones WHERE IdUsuario = '". mysql_real_escape_string($objUsuario->getIdUsuario())."' ORDER BY TipoDireccion");
                $Direcciones = $oConexion->DevolverQuery();
                if($Direcciones[0])
                {
                    $objDireccion = new DatosUsuariosDirecciones();
                    foreach ($Direcciones[0] as $atributo => $valor)
                    {
                        $objDireccion->__set($valor, $atributo);
                    }
                    $objUsuario->setDirecciones(0,$objDireccion);
                }
                if($Direcciones[1])
                {
                    $objDireccion = new DatosUsuariosDirecciones();
                    foreach ($Direcciones[1] as $atributo => $valor)
                    {
                        $objDireccion->__set($valor, $atributo);
                    }
                    $objUsuario->setDirecciones(1,$objDireccion);
                }
                $oConexion->Cerrar();
                return $objUsuario;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }    

        /**
         * Genera objeto usuarios
         *
         * @param integer|string $IdUsuario IdUsuario o NombreUsuario 
         * @return usuarios Objeto.
         */
        public function getObjUsuarioById($IdUsuario,$forzar=false)
        {   
        	
        	//un email     	
	       	if(checkemail($IdUsuario) && (!$forzar || $forzar == "NombreUsuario" || $forzar == "Email" )){
				$campo = "NombreUsuario";
			}
			//un id
			elseif(intval($IdUsuario)>0 && (!$forzar || $forzar == "IdUsuario")){
				$campo = "IdUsuario";
				$IdUsuario = intval($IdUsuario);
			}
			//un codigo de invitacion
			elseif(checkAlfa($IdUsuario) && (!$forzar || $forzar == "NombreUsuario" || $forzar == "CodigoInvitacion")){
				$campo = "NombreUsuario";
			}
			else{
				return false;	
			}
        	
            try
            {   
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT IdUsuario, NombreUsuario, Clave, es_Provisoria, Padrino, FechaIngreso, IdPerfil, es_Activa FROM usuarios WHERE ".$campo." =\"". mysql_real_escape_string($IdUsuario) ."\"");
                $Tabla = $oConexion->DevolverQuery();
                if(count($Tabla) != 1)
                   return false;
                $objUsuario = new usuarios();
                foreach ($Tabla[0] as $atributo => $valor)
                {
                    $objUsuario->__set($valor, $atributo);
                }
                $oConexion->setQuery("SELECT IdUsuario, FechaNacimiento, Nombre, Apellido, Apellido2, TipoDOc, NroDoc, Telefono, Email, CIF, IdTipoUsuario, es_EnviarDatosCorreo, idTratamiento, es_Completo FROM datosusuariospersonales  WHERE IdUsuario = '". mysql_real_escape_string($objUsuario->getIdUsuario())."'");
                $Datos = $oConexion->DevolverQuery();
                if(count($Datos)>0)
                {
                    $objDatos = new DatosUsuariosPersonales();
                    foreach ($Datos[0] as $atributo => $valor)
                    {
                        $objDatos->__set($valor, $atributo);
                    }
                    $objUsuario->setDatos($objDatos);
                }                
                $oConexion->setQuery("SELECT IdUsuario, TipoDireccion, Domicilio, Numero, Escalera, Piso, Puerta, CP, IdProvincia, IdPais, TipoCalle, Poblacion, Telefono, Completo FROM datosusuariosdirecciones WHERE IdUsuario = '". mysql_real_escape_string($objUsuario->getIdUsuario())."' ORDER BY TipoDireccion");
                $Direcciones = $oConexion->DevolverQuery();
                if($Direcciones[0])
                {
                    $objDireccion = new DatosUsuariosDirecciones();
                    foreach ($Direcciones[0] as $atributo => $valor)
                    {
                        $objDireccion->__set($valor, $atributo);
                    }
                    $objUsuario->setDirecciones(0,$objDireccion);
                }
                if($Direcciones[1])
                {
                    $objDireccion = new DatosUsuariosDirecciones();
                    foreach ($Direcciones[1] as $atributo => $valor)
                    {
                        $objDireccion->__set($valor, $atributo);
                    }
                    $objUsuario->setDirecciones(1,$objDireccion);
                }
                $oConexion->Cerrar();
                return $objUsuario;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
         /**
        * @desc Verifica que el usuario exista en la base junto con su contraseņa
        * @return objUsuario
        */        
        public function ActivaUsuario($idUsuario)
        {
            try
            {   
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE usuarios SET es_activa = 0 WHERE idUsuario =\"". mysql_real_escape_string($idUsuario) ."\"");
                $oConexion->EjecutarQuery();
		/*		$oConexion->setQuery("UPDATE usuarios SET es_Provisoria = 0 WHERE idUsuario =\"". $idUsuario ."\"");
                $oConexion->EjecutarQuery();*/
				$oConexion->Cerrar_Trans();
            }
            catch(MySQLException $e)
            {
                throw $e;
            }                                                                                                
        
        }

        public function ActivaPrimerLogin($idUsuario)
        {
            try
            {   
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("UPDATE usuarios SET es_Provisoria = 0 WHERE idUsuario = '" . mysql_real_escape_string($idUsuario)."'");
                $oConexion->EjecutarQuery();
				$oConexion->Cerrar_Trans();
            }
            catch(MySQLException $e)
            {
                throw $e;
            }                                                                                                
        
        }
        public function getCampaniaCategoria($producto){
    	
        	$oConexion = Conexion::nuevo();
            $oConexion->Abrir();
            
            $oConexion->setQuery("SELECT p.IdProducto,c.IdCampania,cat.IdCategoria
			FROM campanias AS c
			JOIN categorias AS cat ON cat.IdCampania = c.IdCampania
			JOIN productos_x_categorias AS pxc ON pxc.IdCategoria = cat.IdCategoria
			JOIN productos AS p ON p.IdProducto = pxc.IdProducto
			WHERE p.IdProducto = \"".mysql_real_escape_string($producto)."\" LIMIT 1");
                     
            $Tabla = $oConexion->DevolverQuery();
            return $Tabla[0];
        }
       
    }
?>