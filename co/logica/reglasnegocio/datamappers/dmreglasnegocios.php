<?
    class dmReglasNegocio
    {
        public static function getReglaByTipo($IdTipo)
        {        
            $cn = Conexion::nuevo();
            $colRN = array();        
            try
            {
                $cn->Abrir();
                $cn->setQuery("SELECT RN.*, TR.Descripcion AS NombreRegla FROM reglasnegocio RN LEFT JOIN tiposreglas TR ON TR.IdTipoRegla = RN.IdTipoRegla WHERE Desde <= NOW() AND Hasta > NOW() AND RN.idTipoRegla = " . $IdTipo);
                $Tabla = $cn->DevolverQuery();
                foreach ($Tabla as $value)
                {
                    $oreglasnegocio = new AplicarReglas();
                    $oreglasnegocio->__setByArray($value);
                    array_push($colRN, $oreglasnegocio);
                }
            }
            catch(MySQLException $e)
            {            
                throw $e;
            }
            return $colRN;
        }        
        public static function getCantidadReglasAplicadasByIdTipo($IdTipo, $IdUsuario, $IdAhijado)
        {
            try
            {
                $cn = Conexion::nuevo();
                $cn->Abrir();
                $cn->setQuery("SELECT IF(COUNT(*) IS NULL,0,COUNT(*)) AS Cantidad FROM reglas_aplicadas RA INNER JOIN reglasnegocio RN ON RA.IdRegla = RN.IdRegla WHERE RN.IdTipoRegla = ".mysql_real_escape_string($IdTipo)." AND RA.IdUsuario = ".mysql_real_escape_string($IdUsuario)." AND RA.IdAhijado = " . mysql_real_escape_string($IdAhijado));
                $Tabla = $cn->DevolverQuery();
                $cn->Cerrar();
                return $Tabla;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public static function getReglaByTipoUnicaVez($IdTipo, $IdUsuario, $IdUsuarioAhijado = null)
        {        
            $cn = Conexion::nuevo();
            $colRN = array();        
            try
            {
                $cn->Abrir();
                $IdUsuarioAhijado = ($IdUsuarioAhijado == null)?" IS NULL":" = ".mysql_real_escape_string($IdUsuarioAhijado);
                //$cn->setQuery("SELECT RN1.*, TR.Descripcion AS NombreRegla FROM reglasnegocio RN1 LEFT JOIN tiposreglas TR ON TR.IdTipoRegla = RN1.IdTipoRegla WHERE NOW( ) >= Desde AND NOW( ) < Hasta AND RN1.idTipoRegla = ".$IdTipo." AND NOT RN1.idRegla IN (SELECT RN.idRegla FROM reglasnegocio RN INNER JOIN bonos_x_usuarios BU ON BU.IdRegla = RN.IdRegla WHERE idUsuario = ".$IdUsuario." AND idTipoRegla = ".$IdTipo." UNION SELECT RN.idRegla FROM reglasnegocio RN INNER JOIN regalos_x_usuarios RU ON RU.IdRegla = RN.IdRegla WHERE idUsuario = ".$IdUsuario." AND idTipoRegla = ".$IdTipo." UNION SELECT RN.idRegla FROM reglasnegocio RN INNER JOIN gastosenvios_x_usuarios GU ON GU.IdRegla = RN.IdRegla WHERE idUsuario = ".$IdUsuario." AND idTipoRegla = ".$IdTipo.")");
                $cn->setQuery("SELECT RN.* , TR.Descripcion AS NombreRegla FROM reglasnegocio RN INNER JOIN tiposreglas TR ON TR.IdTipoRegla = RN.IdTipoRegla
WHERE NOT IdRegla IN(SELECT IdRegla FROM reglas_aplicadas RAU WHERE IdUsuario = ".mysql_real_escape_string($IdUsuario)." AND IdAhijado ".$IdUsuarioAhijado.") AND TR.IdTipoRegla = ".mysql_real_escape_string($IdTipo));
                $Tabla = $cn->DevolverQuery();
                foreach ($Tabla as $value)
                {
                    $oreglasnegocio = new AplicarReglas();
                    $oreglasnegocio->__setByArray($value);
                    array_push($colRN, $oreglasnegocio);
                }
                $cn->Cerrar();
                return $colRN;
            }
            catch(MySQLException $e)
            {            
                throw $e;
            }
            return $colRN;
        }
        public static function AplicarReglas(array $collReglas, $idUsuario, $reglaext, $idcampania, $idproductos, $IdUsuarioAhijado=null)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                //$oConexion->Abrir_Trans();                
                foreach($collReglas as $value)
                {
                    if ($value->getVerificar($reglaext, $idcampania, $idproductos))
                    {
                        $IdUsuarioAhijado = ($IdUsuarioAhijado == NUll)?"NULL":$IdUsuarioAhijado;
                        $oConexion->setQuery("INSERT INTO reglas_aplicadas(IdRegla, IdUsuario, IdAhijado) VALUES (". $value->getIdRegla() .", ". mysql_real_escape_string($idUsuario) .", ".mysql_real_escape_string($IdUsuarioAhijado).")");
                        $oConexion->EjecutarQuery();
                        $oConexion->setQuery("SELECT LAST_INSERT_ID() AS Id");
                        $Id=$oConexion->DevolverQuery();
                        switch($value->getBeneficio1())
                        {
                            case 1://Creditos
                                $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdReglaAplicada, Fecha) VALUES (". $idUsuario .", ". $Id[0]["Id"] .", NOW())");
                                $oConexion->EjecutarQuery();
                            break;
                            case 2://Gastos bonificados
                                $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdReglaAplicada, Fecha) VALUES (". $idUsuario .", ". $Id[0]["Id"] .", NOW())");
                                $oConexion->EjecutarQuery();
                            break;
                            case 3://Regalos usuarios
                                $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdReglaAplicada, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $Id[0]["Id"] ." ,". $idUsuario .")");
                                $oConexion->EjecutarQuery();
                                
                                $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
                                $Pro = $oConexion->DevolverQuery();
                                if ($Pro[0]["Regalo"] > 0)
                                {
                                    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
                                    $oConexion->EjecutarQuery();
                                    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
                                    $oConexion->EjecutarQuery();
                                }
                            break;
                        }
                    }
                    else
                    {
                        throw new Exception("No se pudo aplicar la regla de negocio porque no tiene los atributos necesarios.");
                    }
                }
                $oConexion->Cerrar_Trans();
            }
            catch(MySQLException $e)
            {
                throw $e;
                
            }
        }
        public function getPadrinoByIdUsuario($IdUsuario)
        {
            try
            {   
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT * FROM usuarios WHERE NombreUsuario = (SELECT Padrino FROM usuarios WHERE IdUsuario = ".mysql_real_escape_string($IdUsuario).")");
                $Tabla = $oConexion->DevolverQuery();
                $objUsuario = new usuarios();
                if(isset($Tabla[0]))
                    $objUsuario->__setByArray($Tabla[0]);
                return $objUsuario;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        public function getCantidadCompras($IdUsuario)
        {
            try
            {   
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir();
                $oConexion->setQuery("SELECT IF(COUNT(*) IS NULL, 0, COUNT(*)) AS CantidadCompras FROM pedidos WHERE IdUsuario = ".mysql_real_escape_string($IdUsuario));
                $Tabla = $oConexion->DevolverQuery();
                return $Tabla;
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
    }
    
 //    class dmReglasNegocio_
//    {
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 1:		
//	    * Disponer de una clave privada El usuario introduce una clave privada en Existir� una tabla de claves vinculada a la regla, de un solo uso. Cuando una clave ha sido usada no desaparece, queda registrada como �CONSUMIDA�.
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    */
//	    public static function Aplicar_01($idUsuario)
//	    {
//		    $colRegla = dmReglasNegocio::ByTipo(2);
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla) > 0)
//			    {
//				    foreach($colRegla as $value)
//				    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }
//				    }	
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 2:		
//	    * 1� compra (par�metro: importe m�nimo de compra).
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    * @param reglaext = total de la compra efectuada
//	    * @param idcampania = campania en que fue efectuado el pedido
//	    * @param idproductos tipo Array = articulo(s) que se adquirieron en el pedido
//	    */
//	    public static function Aplicar_02($idUsuario, $reglaext, $idcampania, $idproductos)
//	    {
//		    $colRegla = dmReglasNegocio::ByTipoUNICAVEZ(2, $idUsuario);
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla) > 0)
//			    {
//				    foreach($colRegla as $value)
//				    {
//					    if ($value->getVerificar($reglaext, $idcampania, $idproductos)==true)
//					    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }						
//					    }
//				    }	
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 3:		
//	    * 2� compra y sucesivas (par�metro: importe m�nimo de compra)
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    * @param reglaext = total de la compra efectuada
//	    * @param idcampania = campania en que fue efectuado el pedido
//	    * @param idproductos tipo Array = articulo(s) que se adquirieron en el pedido
//	    */
//	    public static function Aplicar_03($idUsuario, $reglaext, $idcampania, $idproductos)
//	    {
//		    $colRegla = dmReglasNegocio::ByTipo(3);
//		    $cantidad = dmReglasNegocio::CantidadPedidos($idUsuario);
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla) > 0 && $cantidad >= 2)
//			    {
//				    foreach($colRegla as $value)
//				    {
//					    if ($value->getVerificar($reglaext, $idcampania, $idproductos)==true)
//					    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }
//					    }
//				    }	
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }	
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 4:		
//	    * Nuevo registro de usuario Se produce en el momento de la activaci�n de su cuenta de usuario (al registrarse, se le env�a un email de activaci�n a su cuenta de correo). Se aplica UNA sola vez. 
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    */
//	    public static function Aplicar_04($idUsuario)
//	    {
//		    $colRegla = dmReglasNegocio::ByTipoUNICAVEZ(4, $idUsuario);
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla) > 0)
//			    {
//				    foreach($colRegla as $value)
//				    {
//					    switch($value->getBeneficio1())
//					    {
//						    case 1:
//							    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//							    $oConexion->EjecutarQuery();
//							    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//						    break;
//						    case 2:
//							    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//							    $oConexion->EjecutarQuery();
//							    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//						    break;
//						    case 3:
//							    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//							    $oConexion->EjecutarQuery();
//							    
//							    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//							    $Pro = $oConexion->DevolverQuery();
//							    if ($Pro[0]["Regalo"] > 0)
//							    {
//								    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//								    $oConexion->EjecutarQuery();
//								    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//							    }
//						    break;
//					    }
//				    }
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 6:		
//	    * Apadrinamiento = El apadrinamiento consiste en que un usuario invita a otro, el cual se registra en VipVenta y realiza su primera compra. El bono ,se aplica en el momento de la primera compra.
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    */
//	    public static function Aplicar_06($idUsuario)
//	    {
//		    $CantidadPedidos = dmReglasNegocio::CantidadPedidos($idUsuario);
//		    $IdPadrino=0;
//		    $colRegla = dmReglasNegocio::ByTipo(6);
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla) > 0 && $CantidadPedidos == 1 && $IdPadrino > 0)
//			    {
//				    foreach($colRegla as $value)
//				    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }
//				    }	
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 7:		
//	    * Apadrinamiento masivo (par�metro: n�mero m�nimo de nuevos registros). Cuando un usuario consigue que otros se registren en VipVenta denot�ndolo como padrino. Se aplica UNA vez.
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    */
//	    public static function Aplicar_07($idUsuario)
//	    {
//		    $IdPadrino = dmReglasNegocio::BuscarIdUsuario_Padrino($idUsuario);
//		    $CantidadApadrinados = dmReglasNegocio::CantidadApadrinados($IdPadrino);		
//		    $colRegla = dmReglasNegocio::ByTipoUNICAVEZ(7, $IdPadrino);
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla))
//			    {
//				    foreach($colRegla as $value)
//				    {
//					    if($value->getVerificar($CantidadApadrinados, -1, -1) == true)
//					    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }
//					    }
//				    }	
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 8:		
//	    * Invitaci�n Por haber invitado a m�s de X contactos. Esta regla s�lo puede aplicarse UNA vez por cada usuario.
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    * @param reglaext = Cantidad de Contactos invitados
//	    */
//	    public static function Aplicar_08($idUsuario, $reglaext)
//	    {
//		    $colRegla = dmReglasNegocio::ByTipoUNICAVEZ(8, $idUsuario);
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla))
//			    {
//				    foreach($colRegla as $value)
//				    {
//					    if($value->getVerificar($reglaext, -1, -1) == true)
//					    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }
//					    }
//				    }	
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }	
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 8:		
//	    * Invitaci�n masiva Cuando un usuario hace uso POR PRIMERA VEZ de la utilidad de importaci�n masiva de contactos y realiza una invitaci�n m�ltiple. (tiene que llevar a cabo la invitaci�n, y el n�mero m�nimo de emails enviados debe ser 10).
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    * @param reglaext = cantidad de Contactos invitados. Como minimo 10
//	    */
//	    public static function Aplicar_09($idUsuario, $reglaext)
//	    {
//		    $colRegla = dmReglasNegocio::ByTipoUNICAVEZ(9, $idUsuario);
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla) && $reglaext >= 10)
//			    {
//				    foreach($colRegla as $value)
//				    {
//					    if($value->getVerificar($reglaext, -1, -1) == true)
//					    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }
//					    }
//				    }	
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 8:		
//	    * Volumen de pedidos realizados: (par�metro: n�mero m�nimo de pedidos). Cuando un usuario supera la cantidad de pedidos. Se aplica UNA vez.
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    */
//	    public static function Aplicar_10($idUsuario)
//	    {
//		    $CantidadPedidos = dmReglasNegocio::CantidadPedidos($idUsuario);
//		    $colRegla = dmReglasNegocio::ByTipoUNICAVEZ(10, $idUsuario);
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla))
//			    {
//				    foreach($colRegla as $value)
//				    {
//					    if($value->getVerificar($CantidadPedidos, -1, -1) == true)
//					    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }
//					    }
//				    }	
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }	
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 11:		
//	    * Importe total gastado: (par�metro: importe m�nimo) Seg�n dinero gastado en general desde el principio. Se aplica UNA vez.
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    * @param reglaext = total de la compra efectuada
//	    * @param idcampania = campania en que fue efectuado el pedido
//	    * @param idproductos tipo Array = articulo(s) que se adquirieron en el pedido
//	    */
//	    public static function Aplicar_11($idUsuario, $reglaext, $idcampania, $idproductos)
//	    {
//		    $colRegla = dmReglasNegocio::ByTipoUNICAVEZ(11, $idUsuario);		
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla) > 0)
//			    {
//				    foreach($colRegla as $value)
//				    {
//					    if ($value->getVerificar($reglaext, $idcampania, $idproductos)==true)
//					    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }
//					    }
//				    }	
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 12:		
//	    * N�mero de art�culos comprados en un mismo pedido: (par�metro: n�mero m�nimo de art�culos).
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    * @param reglaext = total de la compra efectuada
//	    * @param idcampania = campania en que fue efectuado el pedido
//	    * @param idproductos tipo Array = articulo(s) que se adquirieron en el pedido
//	    */
//	    public static function Aplicar_12($idUsuario, $reglaext, $idcampania, $idproductos)
//	    {
//		    $colRegla = dmReglasNegocio::ByTipo(12);		
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla) > 0)
//			    {
//				    foreach($colRegla as $value)
//				    {
//					    if ($value->getVerificar($reglaext, $idcampania, $idproductos)==true)
//					    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }
//					    }
//				    }	
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 13:		
//	    * Cliente golden El administrador los selecciona manualmente. OJO: este tipo de reglas se aplican UNA sola vez por cliente.
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    */
//	    public static function Aplicar_13($idUsuario)
//	    {
//		    $colRegla = dmReglasNegocio::ByTipoUNICAVEZ(13, $idUsuario);
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla))
//			    {
//				    foreach($colRegla as $value)
//				    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }
//				    }	
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//		    }
//	    }
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 14:		
//	    * Cliente silver El administrador los selecciona manualmente. OJO: este tipo de reglas se aplican UNA sola vez por cliente
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    */
//	    public static function Aplicar_14($idUsuario)
//	    {
//		    $colRegla = dmReglasNegocio::ByTipoUNICAVEZ(14, $idUsuario);
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    $oConexion->Abrir_Trans();
//			    if (count($colRegla))
//			    {
//				    foreach($colRegla as $value)
//				    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }
//				    }	
//			    }
//			    $oConexion->Cerrar_Trans();
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }
//	    /**
//	    * Funcion que aplica la Regla de Negocio Nro 15:
//	    * Registro completo = Cuando el usuario tiene cumplimentado todos los campos del registro. Se aplica UNA sola vez por cliente.
//	    * @param idUsuario = Id del usuario a aplicar la regla.
//	    */
//	    public static function Aplicar_15($idUsuario)
//	    {
//		    $colRegla = dmReglasNegocio::ByTipoUNICAVEZ(15, $idUsuario);
//		    $oConexion = new Conexion();
//		    try
//		    {
//			    $estado = array();
//			    if (count($colRegla) > 0)
//			    {
//				    $oConexion->Abrir_Trans();			
//				    $oConexion->setQuery("SELECT SUM(Cantidad) AS Cantidad FROM (SELECT COUNT(*) AS Cantidad FROM datosusuariosdirecciones WHERE Tipo IN (1,2) AND Completo = 1 AND IdUsuario = " . $idUsuario . " UNION SELECT COUNT(*) AS Cantidad FROM datosusuariospersonales WHERE  Completo = 1 AND IdUsuario = " . $idUsuario .") A");	
//				    $Tabla = $oConexion->DevolverQuery();
//				    if ($Tabla[0]["Cantidad"] == 3)
//				    {
//					    foreach($colRegla as $value)
//					    {
//						    switch($value->getBeneficio1())
//						    {
//							    case 1:
//								    $oConexion->setQuery("INSERT INTO bonos_x_usuarios(IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "<a href='micuenta3.php'>Bono obtenido por ".$value->getTipoReglaDescripcion()."</a>");
//							    break;
//							    case 2:
//								    $oConexion->setQuery("INSERT INTO gastosenvios_x_usuarios (IdUsuario, IdRegla, Fecha) VALUES (". $idUsuario .", ". $value->getIdRegla() .", NOW())");
//								    $oConexion->EjecutarQuery();
//								    array_push($estado, "Gasto de envio bonificado obtenido por ".$value->getTipoReglaDescripcion());
//							    break;
//							    case 3:
//								    $oConexion->setQuery("INSERT INTO regalos_x_usuarios (IdCodigoProdInterno, Fecha, IdRegla, IdUsuario) VALUES (". $value->getAplica1() .", NOW(), ". $value->getIdRegla() ." ,". $idUsuario .")");
//								    $oConexion->EjecutarQuery();
//								    
//								    $oConexion->setQuery("SELECT * FROM productos_x_pedidos PP WHERE IdCodigoProdInterno = " . $value->getAplica1());
//								    $Pro = $oConexion->DevolverQuery();
//								    if ($Pro[0]["Regalo"] > 0)
//								    {
//									    $oConexion->setQuery("INSERT INTO pedidos (Fecha, FechaEnvio, IdUsuario, IdMensajeria, IdFormaPago, IdFormaEnvio, IdCampania) VALUES (NOW(), NULL, ". $idUsuario .", NULL, NULL, NULL, NULL)");
//									    $oConexion->EjecutarQuery();
//									    $oConexion->setQuery("INSERT INTO productos_x_pedidos (IdPedido, IdCodigoProdInterno, Cantidad, Precio) VALUES (LAST_INSERT_ID(), ". $value->getAplica1() .", 1 ,0)");
//									    $oConexion->EjecutarQuery();
//									    array_push($estado, "<a href='micuenta2.php'>Regalo obtenido por ".$value->getTipoReglaDescripcion()."</a>");	
//								    }
//							    break;
//						    }
//					    }	
//				    }
//			    $oConexion->Cerrar_Trans();
//			    }			
//			    return $estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//			    
//		    }
//	    }
//	    /**
//	    * Obtiene las reglas del tipo de regla
//	    * @param Tipo = id tipo de regla.
//	    */
//	    public static function ByTipo($Tipo)
//	    {		
//		    $cn = new Conexion();
//		    $colRN = array();		
//		    try
//		    {
//			    $cn->Abrir_Trans();
//			    $cn->setQuery("SELECT RN.*, TR.Descripcion AS NombreRegla FROM reglasnegocio RN LEFT JOIN tiposreglas TR ON TR.IdTipoRegla = RN.IdTipoRegla WHERE Desde <= NOW() AND Hasta > NOW() AND RN.idTipoRegla = " . $Tipo);
//			    $Tabla = $cn->DevolverQuery();
//			    foreach ($Tabla as $value)
//			    {
//				    $oreglasnegocio = new AplicarReglas();
//				    $oreglasnegocio->setIdRegla($value["IdRegla"]);
//				    $oreglasnegocio->setNombre($value["Nombre"]);
//				    $oreglasnegocio->setDescripcion($value["Descripcion"]);
//				    $oreglasnegocio->setCaducidad($value["Caducidad"]);
//				    $oreglasnegocio->setDesde($value["Desde"]);
//				    $oreglasnegocio->setHasta($value["Hasta"]);
//				    $oreglasnegocio->setIdTipoRegla($value["IdTipoRegla"]);
//				    $oreglasnegocio->setReglaEXT($value["ReglaEXT"]);
//				    $oreglasnegocio->setIdCampania($value["IdCampania"]);
//				    $oreglasnegocio->setIdProducto($value["IdProducto"]);
//				    $oreglasnegocio->setBeneficio1($value["Beneficio1"]);
//				    $oreglasnegocio->setAplica1($value["Aplica1"]);
//				    $oreglasnegocio->setBeneficio2($value["Beneficio2"]);
//				    $oreglasnegocio->setAplica2($value["Aplica2"]);
//				    $oreglasnegocio->setTipoReglaDescripcion($value["NombreRegla"]);
//				    array_push($colRN, $oreglasnegocio);
//			    }
//			    $cn->Cerrar_Trans();			
//		    }
//		    catch(MySQLException $e)
//		    {			
//			    throw $e;
//		    }
//		    return $colRN;
//	    }
//		/**
//	    * Obtiene el total en euros por apadrinar a un usuario que luego se registre.	
//	    */
//	    public static function ObtenerValor_x_Apadrinamiento()
//	    {
//		    try
//		    {
//			    $cn = new Conexion();
//			    $cn->Abrir();
//			    $SQL = "SELECT if( SUM( CAST( RN.Aplica1 AS DECIMAL( 10, 2 ) ) ) IS NULL , 0, SUM( CAST( RN.Aplica1 AS DECIMAL( 10, 2 ) ) ) ) AS Total
//    FROM reglasnegocio RN
//    WHERE RN.Beneficio1 =1
//    AND RN.IdTipoRegla =4
//    AND Desde <= NOW( ) 
//    AND Hasta > NOW( )";
//			    $cn->setQuery($SQL);
//			    $Tabla = $cn->DevolverQuery($cn);
//			    $cn->Cerrar();
//			    return $Tabla[0]["Total"];
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;
//		    }
//	    }
//	    /**
//	    * Obtiene las reglas del tipo de regla que nunca fueron usadas por el usuario
//	    * @param Tipo = id tipo de regla.
//	    * @param idusuario = id de usuario.	
//	    */
//	    public static function ByTipoUNICAVEZ($IdTipo, $IdUsuario)
//	    {		
//		    $cn = new Conexion();
//		    $colRN = array();		
//		    try
//		    {
//			    $cn->Abrir_Trans();
//			    $cn->setQuery("SELECT RN1.*, TR.Descripcion AS NombreRegla FROM reglasnegocio RN1 LEFT JOIN tiposreglas TR ON TR.IdTipoRegla = RN1.IdTipoRegla WHERE RN1.idTipoRegla = ".$IdTipo." AND NOT RN1.idRegla IN (SELECT RN.idRegla FROM reglasnegocio RN INNER JOIN bonos_x_usuarios BU ON BU.IdRegla = RN.IdRegla WHERE idUsuario = ".$IdUsuario." AND idTipoRegla = ".$IdTipo."
//			    UNION SELECT RN.idRegla FROM reglasnegocio RN INNER JOIN regalos_x_usuarios RU ON RU.IdRegla = RN.IdRegla WHERE idUsuario = ".$IdUsuario." AND idTipoRegla = ".$IdTipo."	
//			    UNION SELECT RN.idRegla FROM reglasnegocio RN INNER JOIN gastosenvios_x_usuarios GU ON GU.IdRegla = RN.IdRegla WHERE idUsuario = ".$IdUsuario." AND idTipoRegla = ".$IdTipo.") AND Desde <= NOW() AND Hasta > NOW()");
//			    $Tabla = $cn->DevolverQuery();
//			    foreach ($Tabla as $value)
//			    {
//				    $oreglasnegocio = new AplicarReglas();
//				    $oreglasnegocio->setIdRegla($value["IdRegla"]);
//				    $oreglasnegocio->setNombre($value["Nombre"]);
//				    $oreglasnegocio->setDescripcion($value["Descripcion"]);
//				    $oreglasnegocio->setCaducidad($value["Caducidad"]);
//				    $oreglasnegocio->setDesde($value["Desde"]);
//				    $oreglasnegocio->setHasta($value["Hasta"]);
//				    $oreglasnegocio->setIdTipoRegla($value["IdTipoRegla"]);
//				    $oreglasnegocio->setReglaEXT($value["ReglaEXT"]);
//				    $oreglasnegocio->setIdCampania($value["IdCampania"]);
//				    $oreglasnegocio->setIdProducto($value["IdProducto"]);
//				    $oreglasnegocio->setBeneficio1($value["Beneficio1"]);
//				    $oreglasnegocio->setAplica1($value["Aplica1"]);
//				    $oreglasnegocio->setBeneficio2($value["Beneficio2"]);
//				    $oreglasnegocio->setAplica2($value["Aplica2"]);
//				    $oreglasnegocio->setTipoReglaDescripcion($value["NombreRegla"]);
//				    array_push($colRN, $oreglasnegocio);
//			    }
//			    $cn->Cerrar_Trans();			
//		    }
//		    catch(MySQLException $e)
//		    {			
//			    throw $e;
//		    }
//		    return $colRN;
//	    }
//	    /**
//	    * Cantidad de pedidos realizados por el usuario.
//	    * @param idUsuario = id del usuario.
//	    */
//	    public static function CantidadPedidos($idUsuario)
//	    {		
//		    $cn = new Conexion();
//		    try
//		    {
//			    $cn->Abrir_Trans();				
//			    $cn->setQuery("SELECT COUNT( * ) AS Cantidad FROM pedidos P WHERE NOT IdCampania IS NULL AND idUsuario = " . $idUsuario);
//			    $Tabla = $cn->DevolverQuery();
//			    $cn->Cerrar_Trans();
//			    return $Tabla[0]->Cantidad;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;			
//		    }
//	    }
//	    /**
//	    * Busca el padrino del usuario
//	    * @param IdUsuario = id del usuario
//	    *Devuelve el idUsuario del padrino buscado. Si no lo encuentra devuelve 0.
//	    */
//	    public static function BuscarIdUsuario_Padrino($idUsuario)
//	    {		
//		    $cn = new Conexion();
//		    $Estado=0;
//		    try
//		    {
//			    $cn->Abrir_Trans();				
//			    $cn->setQuery("SELECT Padrino FROM usuarios WHERE idUsuario = " . $idUsuario);
//			    $Tabla = $cn->DevolverQuery();
//			    if(count($Tabla) > 0)
//			    {
//				    $cn->setQuery("SELECT IdUsuario FROM usuarios WHERE NombreUsuario = '" . $Tabla[0]->Padrino ."'");
//				    $Tabla = $cn->DevolverQuery();
//				    if(count($Tabla)>0)
//					    $Estado=$Tabla[0]->IdUsuario;
//			    }
//			    $cn->Cerrar_Trans();
//			    return $Estado;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;			
//		    }
//	    }
//	    /**
//	    * Busca la cantidad de apadrinamientos registrados de este usuario
//	    * @param IdUsuario = id del usuario
//	    * Devuelve el total de apadrinamientos registrados. Si no encuentra ninguno devuelve 0
//	    */
//	    public static function CantidadApadrinados($idUsuario)
//	    {		
//		    $cn = new Conexion();
//		    try
//		    {
//			    $cn->Abrir_Trans();				
//			    $cn->setQuery("SELECT COUNT(*) AS Cantidad FROM ahijados A INNER JOIN usuarios U ON A.Email = U.NombreUsuario WHERE U.IdUsuario = " . $idUsuario);
//			    $Tabla = $cn->DevolverQuery();
//			    $cn->Cerrar_Trans();
//			    return $Tabla[0]->Cantidad;
//		    }
//		    catch(MySQLException $e)
//		    {
//			    throw $e;			
//		    }
//	    }
//    }
?>