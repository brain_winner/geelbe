<?php
    class AplicarReglas extends reglasnegocio
    {
        private $_tiporegladescripcion;
        public function getTipoReglaDescripcion()
        {
            return $this->_tiporegladescripcion;
        }
        public function setTipoReglaDescripcion($tiporegladescripcion)
        {
            $this->_tiporegladescripcion = $tiporegladescripcion;
        }
        /**
        * Funcion para validar datos extras de las reglas de negocio
        * @param reglaext = parametro a comprar
        * @param idcampania = id de campania donde se realizo la accion
        * @param idproductos tipo Array = array de productos que se adquirieron en la accion
        *Devuelve true si hay coincidencia, sino false
        */
        function getVerificar($reglaext, $idcampania, $idproductos)
        {
            try
            {
                if ($this->getReglaEXT() <= $reglaext)
                {
                    if ($this->getIdCampania() == 0 && $this->getIdCodigoProdInterno() == 0)
                        return true;
                    else if($this->getIdCampania() == $idcampania && $this->getIdCodigoProdInterno() == 0)
                        return true;
                    else if($this->getIdCampania() == 0 && $this->getIdCodigoProdInterno() > 0)
                    {
                        $Estado = false;
                        foreach($idproductos as $value)
                        {
                            if($value == $this->getIdCodigoProdInterno())
                                $Estado=true;
                        }
                        return $Estado;
                    }                    
                    else if($this->getIdCampania() == $idcampania && $this->getIdCodigoProdInterno() > 0)
                    {
                        $Estado = false;
                        foreach($idproductos as $value)
                        {
                            if($value == $this->getIdCodigoProdInterno())
                                $Estado=true;
                        }
                        return $Estado;
                    }                    
                    else
                        return false;
                }
                else
                    return false;            
            }
            catch(exception $e)
            {
                return false;
            }
        }
    }
?>
