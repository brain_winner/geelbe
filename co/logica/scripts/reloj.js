  /*
      Author:        Robert Hashemian (http://www.hashemian.com/)
      Modified by:    Munsifali Rashid (http://www.munit.co.uk/)
      Modified by:    Tilesh Khatri
  */
  
  function IniciarReloj(divname, fechafin, fechahoy, vidriera)
  {
    var dfechafin    = new Date(fechafin);
    var dhoy    = new Date(fechahoy);
    ddiff        = new Date(dfechafin-dhoy);
    gsecs        = Math.floor(ddiff.valueOf()/1000);
    days		 = Math.floor(ddiff.valueOf()/(1000*60*60*24));
    
    if(days > 0) {
      if(vidriera)
       	var html = 'Queda'+(days != 1 ? 'n' : '')+' ';
      else
        var html = '';
       
       html += days+' d&iacute;a'+(days != 1 ? 's' : '');
    	
 	   document.getElementById(divname).innerHTML = html;
    } else
    	CountBack(divname,gsecs,vidriera);
  }
  
  function Calcage(secs, num1, num2)
  {
    s = ((Math.floor(secs/num1))%num2).toString();
    if (s.length < 2) 
    {    
      s = "0" + s;
    }
    return (s);
  }
  
  function CountBack(myDiv, secs, vidriera)
  {
    var dias = Calcage(secs,86400,100000);
    if(dias == 0) {
    	dias = '';
    } else {
    	dias = dias+' d&iacute;as y';
    }
  
    var DisplayStr;
    var DisplayFormat = "%%D%% %%H%%:%%M%%:%%S%% hs";
    
    if(vidriera)
    	DisplayFormat = "Quedan "+DisplayFormat;
    
    DisplayStr = DisplayFormat.replace(/%%D%%/g,    dias);
    DisplayStr = DisplayStr.replace(/%%H%%/g,        Calcage(secs,3600,24));
    DisplayStr = DisplayStr.replace(/%%M%%/g,        Calcage(secs,60,60));
    DisplayStr = DisplayStr.replace(/%%S%%/g,        Calcage(secs,1,60));
    if(secs > 0)
    {    
      document.getElementById(myDiv).innerHTML = DisplayStr;
      setTimeout("CountBack('" + myDiv + "'," + (secs-1) + ", "+vidriera+");", 990);
    }
  }