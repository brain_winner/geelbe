<html><body>
<?php
  try
  {
      ob_start();
      $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
      Aplicacion::CargarIncludes(Aplicacion::getIncludes("recordar"));
      $arrayHTML = Aplicacion::getIncludes("onlyget", "recuperarclave");
      
	  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
	  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
      require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
      require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
	  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";

      Includes::Scripts();
      $arrDatos = array();
      $idUsuario = dmUsuario::getIdByNombreUsuario($_POST["txtEmailActivar"]);
	  $idUsuarioCHK = $_POST["txtEmailActivar"];
		$isPageOk = true;
		$pageException = "Exception";
		// Validaciones de parametros basicas
        if (!isset($idUsuarioCHK)) { // idUsuarioCHK Seteado
            $isPageOk = false;
			$pageException = "idUsuario not setted";
		}
		else if (!ValidatorUtils::validateEmail($idUsuarioCHK)) { // validar idUsuarioCHK
			$isPageOk = false;
			$pageException = "idUsuario is not a mail";
		}
		else if (!ValidatorUtils::is_valid_email($idUsuarioCHK)) { // idUsuarioCHK es mail valido
			$isPageOk = false;
			$pageException = "idUsuario is not a valid mail";
		}
		if (!$isPageOk) {
			echo generarJS("window.parent.ErrorMSJ","Error de registro.", "MAIL_INCORRECTO");
			exit;
		}
      if ($idUsuario)
      {
          if (dmUsuario::EsUsuarioRegistrado($idUsuario))
          {
            ?>
                <script>window.parent.msjError(arrErrores["RECORDATORIO_ACTIVAR"]["USUARIO_YA_ACTIVO"],arrErrores["RECORDATORIO_ACTIVAR"]["USUARIO_YA_ACTIVO_TITULO"],"<?=Aplicacion::getRootUrl()?>front/login");</script>
            <?
          }
          else
          {
              #Se env�a mail de activaci�n al usuario.
              $objUsuario = dmUsuario::getByIdUsuario($idUsuario);

              $emailBuilder = new EmailBuilder();
              $emailData = $emailBuilder->generateActivationEmail($objUsuario, $objUsuario->getNombreUsuario());
					
			  $emailSender = new EmailSender();
			  $emailSender->sendEmail($emailData);
			?>
            <script>
            var auxMsj = arrErrores["RECORDATORIO_ACTIVAR"]["EXITO"]; auxMsj = auxMsj.replace("[EMAIL]","<? echo $_POST["txtEmailActivar"]; ?>");
            window.parent.msjInfo(auxMsj,arrErrores["RECORDATORIO_ACTIVAR"]["EXITO_TITULO"],"<?=Aplicacion::getRootUrl()?>front/login");</script>
           <?
          }

      }
      else
      {
            ?>
                <script>window.parent.msjError(arrErrores["RECORDATORIO_ACTIVAR"]["FRACASO"],arrErrores["RECORDATORIO_ACTIVAR"]["FRACASO_TITULO"],"<?=Aplicacion::getRootUrl()?>front/login");</script>
            <?
      }
      ob_flush();
  }
  catch(MySQLException $e)
  {
      ?>
        <script>window.parent.showError("+++","---");</script>
      <?
  }
?>
</body></html>