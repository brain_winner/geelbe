<html><body>
<?php
  try
  {
       ob_start();
      $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
      Aplicacion::CargarIncludes(Aplicacion::getIncludes("recordar"));
      $arrayHTML = Aplicacion::getIncludes("onlyget", "recuperarclave");

      require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
	  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
	  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
	  require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/recordar/clases/recoverpasshash.php";
      require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
	  
      Includes::Scripts();
      $arrDatos = array();
      $idUsuario = dmUsuario::getIdByNombreUsuario($_POST["txtEmailRecordar"]);
	  $idUsuarioCHK = $_POST["txtEmailRecordar"];
		$isPageOk = true;
		$pageException = "Exception";
		// Validaciones de parametros basicas
        if (!isset($idUsuarioCHK)) { // idUsuarioCHK Seteado
            $isPageOk = false;
			$pageException = "idUsuario not setted";
		}
		else if (!ValidatorUtils::validateEmail($idUsuarioCHK)) { // validar idUsuarioCHK
			$isPageOk = false;
			$pageException = "idUsuario is not a mail";
		}
		else if (!ValidatorUtils::is_valid_email($idUsuarioCHK)) { // idUsuarioCHK es mail valido
			$isPageOk = false;
			$pageException = "idUsuario is not a valid mail";
		}
		if (!$isPageOk) {
			echo generarJS("window.parent.ErrorMSJ","Error de registro.", "MAIL_INCORRECTO");
			exit;
		}
		
		
      if ($idUsuario)
      {
          if (dmUsuario::EsUsuarioRegistrado($idUsuario))
          {
               #Se env�a clave al usuario.
               $objUsuario = dmUsuario::getByIdUsuario(dmUsuario::getIdByNombreUsuario(mysql_real_escape_string($_POST["txtEmailRecordar"])));

               $recoverPassHash = RecoverPassHash::getHash();
               
          		$oConexion = Conexion::nuevo();
				try {
					$oConexion->Abrir_Trans();
					$oConexion->setQuery("INSERT INTO recuperaciones_contrasenias (recover_date, email, codigo, recover_made) VALUES (NOW(), '".htmlspecialchars(mysql_real_escape_string($objUsuario->getNombreUsuario()), ENT_QUOTES)."', '".$recoverPassHash."', false)");
					$oConexion->EjecutarQuery();
					$oConexion->Cerrar_Trans();
				}
				catch (MySQLException $e) {
					$oConexion->RollBack();
					throw $e;
				}
               
               $emailBuilder = new EmailBuilder();
	           $emailData = $emailBuilder->generateRecuperacionDatosEmail($objUsuario, $recoverPassHash);
	                
			   $emailSender = new EmailSender();
			   $emailSender->sendEmail($emailData);
               
			   ?>
               <script>
                 var auxMsj = arrErrores["RECORDAR"]["EXITO"]; auxMsj = auxMsj.replace("[EMAIL]","<? echo $_POST["txtEmailRecordar"]; ?>");
                 window.parent.msjInfo(auxMsj,arrErrores["RECORDAR"]["EXITO_TITULO"],"<?=Aplicacion::getRootUrl()?>front/login");</script>
               <?
          }
          else
          {
              ?>
                <script>window.parent.msjError(arrErrores["RECORDAR"]["USUARIO_NO_ACTIVO"],arrErrores["RECORDAR"]["USUARIO_NO_ACTIVO_TITULO"],"<?=Aplicacion::getRootUrl()?>front/login");</script>
              <?
          }


      }
      else
      {
            ?>
                <script>window.parent.msjError(arrErrores["RECORDAR"]["FRACASO"],arrErrores["RECORDAR"]["FRACASO_TITULO"]);</script>
            <?
      }
      ob_flush();
  }
  catch(MySQLException $e)
  {
      ?>
        <script>window.parent.showError("+++","---");</script>
      <?
  }
?>
</body></html>