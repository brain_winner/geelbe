<html>
    <head>
        <?
        		if(!isset($_GET['wl']) || !is_numeric($_GET['wl']))
        			die;
        		
             ob_start();
         
            $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
            Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados"));
             Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto", "dm"));
            
             
			require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
			require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
			require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
			require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
			require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/ws/MovistarWSProxy.php");

            Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
            Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));
            Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));

            //Aplicacion::CargarIncludes(Aplicacion::getIncludes("mailer"));
            Aplicacion::CargarIncludes(Aplicacion::getIncludes("emails"));
            $arrayHTML = Aplicacion::getIncludes("onlyget", "htmlBienvenida");
            $arrayXML = Aplicacion::getIncludes("onlyget", "xmlEmails");

            $postURL = Aplicacion::getIncludes("post", "micuenta");
            Aplicacion::CargarIncludes(Aplicacion::getIncludes("login"));
            Includes::Scripts();
            $postURL = Aplicacion::getIncludes("post", "activacion");
			
        ?>

        <script src="js/js.js" type="text/javascript"></script>

    </head>
    <body style="width:100%;height:100%">
        <?
            try {
                $oUsuario = dmUsuario::getByIdUsuario($_GET["wl"]);

                // Bajo los datos a las variables correspondientes
                $nombreUsuario = $oUsuario ->getDatos()->getNombre();
                $apellidoUsuario = $oUsuario ->getDatos()->getApellido();
                $emailUsuario = $oUsuario ->getNombreUsuario();

                if ($oUsuario->getes_Activa() == 1) {

                    $objUsuarios = dmUsuarios::ObtenerUsuarioCompleto($oUsuario->getNombreUsuario());
                    dmUsuarios::ActivaUsuario($objUsuarios->getIdUsuario());
                    Aplicacion::loginUser($objUsuarios);
                    $arrPadrino = dmUsuarios::getDatosUsuarios($objUsuarios->getPadrino());
                    $arrPadrino = dmUsuarios::getDatosPersonales($arrPadrino["IdUsuario"]);
			        
                	$sexoUsuario = "";
			        if($oUsuario->getDatos()->getIdTratamiento() == 1) {
			        	$sexoUsuario = "Hombre";
			        } else if ($oUsuario->getDatos()->getIdTratamiento() == 2) {
			        	$sexoUsuario = "Mujer";
			        }
			        
			        $provinciaUsuario = "";
			        if($oUsuario->getDirecciones(0)->getIdProvincia() > 0) {
		                $oConexion->setQuery("SELECT Nombre FROM dhl_estados WHERE IdEstado = ".$oUsuario->getDirecciones(0)->getIdProvincia());                
		                $DT = $oConexion->DevolverQuery();
		                $oConexion->Cerrar();
		                
		                $provinciaUsuario = $DT[0]["Nombre"];
			        }
                    
                    //Guardar en lista
                    $oConexion = Conexion::nuevo();
			        $oConexion->Abrir();
			        
		            $oConexion->setQuery('INSERT INTO acciones_listas_ems (email, nombre, apellido, tipo_accion, fecha_accion, accion_encolada, accion_commiteada, id_usuario, hash_usuario, fechanacimiento_usuario, sexo_usuario, provincia_usuario) 
		            	VALUES ("'.htmlspecialchars(mysql_real_escape_string($oUsuario->getNombreUsuario()), ENT_QUOTES).'", "'.htmlspecialchars(mysql_real_escape_string($oUsuario->getDatos()->getNombre()), ENT_QUOTES).'", "'.htmlspecialchars(mysql_real_escape_string($oUsuario->getDatos()->getApellido()), ENT_QUOTES).'", "A", NOW(), 0, 0, '.
		            	$oUsuario->getIdUsuario().', "'.Hash::getHash($oUsuario->getIdUsuario().$oUsuario->getNombreUsuario()).'", "'.$oUsuario->getDatos()->getFechaNacimiento().'", "'.
		            	$sexoUsuario.'", "'.$provinciaUsuario.'");');
         		    $oConexion->EjecutarQuery();
					
					/* Si es huerfano, solo por hoy le pongo un codigo de invitado*/
					if($objUsuarios->getPadrino() == "Huerfano" ){
							$objUsuarios->setPadrino('reactivacion1');						
						}
					
                    //Mail al padrino
                    $emailBuilder = new EmailBuilder();
	                $emailData = $emailBuilder->generateAvisoRegistroAhijadoEmail($objUsuarios, $arrPadrino["IdUsuario"], $arrPadrino["Nombre"]);
	                
					$emailSender = new EmailSender();
					$emailSender->sendEmail($emailData);
                    
                    // XML de TAGS
                    $ruta = Aplicacion::getRoot() . $arrayXML['xmlEmails'];
                    $xml = simplexml_load_file($ruta);
        
                    // ENVIO DE MAIL
                    $emailBuilder = new EmailBuilder();
	                $emailData = $emailBuilder->generateBienvenidaEmail($objUsuarios);
	                
					$emailSender = new EmailSender();
					$emailSender->sendEmail($emailData);
                    
                }

                $invitesSql = Conexion::nuevo();
				$invitesSql->Abrir_Trans();
				
			    $invitesSql->setQuery("select  invit.user_id, 
			                                   invit.email, 
			                                   dp.Nombre, 
			                                   dp.Apellido
			                           FROM    geelbe_pending_user_activation_invitations invit, 
			                                   datosusuariospersonales dp, 
			                                   usuarios u 
			                           WHERE invit.invitation_sent = 0 
			                           AND u.IdUsuario = dp.IdUsuario
			                           AND invit.user_id = ".$oUsuario->getIdUsuario()."
			                           AND dp.IdUsuario = invit.user_id 
			                           ORDER BY invit.invitation_date");
				
				$invitesQuery = $invitesSql->DevolverQuery();
				$arrayHTML = Aplicacion::getIncludes("onlyget");
				foreach ($invitesQuery as $i => $row) {
                   
				   $emailBuilder = new EmailBuilder();
				   $emailData = $emailBuilder->generateInviteEmail($oUsuario, $row["email"]);
				   
				   $emailSender = new EmailSender();
				   $emailSender->sendEmail($emailData);
				   
				   $conexion = Conexion::nuevo();
				   $conexion->setQuery("insert geelbe_sent_invitations (invitation_from, invitation_to, invitation_date) values ('".$oUsuario->getNombreUsuario()."','".$row["email"]."', NOW())");
				   $conexion->EjecutarQuery();
				}
				
				$invitesSql = new Conexion();
                $invitesSql->Abrir_Trans();
                //setea flag indicando que las invitaciones corresponden a un usuario activo
                $invitesSql->setQuery("update geelbe_pending_user_activation_invitations set invitation_sent = 1 where user_id = '".$oUsuario->getIdUsuario()."'");
                $invitesQuery = $invitesSql->EjecutarQuery();
                $invitesSql->Cerrar_Trans();
                
        ?>

        <form method="post" action="../../../<?=$postURL["activacion"]?>" id="frmActivacion" name="frmActivacion">
            <input type="hidden" id="ee" name="ee" value ="<?=$oUsuario->getNombreUsuario()?>" />
			<input type="hidden" id="idu" name="idu" value ="<?=$oUsuario->getIdUsuario()?>" />
        </form>
        <script>document.forms["frmActivacion"].submit();</script>

        <?
            }
            catch (Exception $e) {
                //var_dump($e);
                echo $e;
                $e = new ACCESOException("","444","");
        ?>

        <script>window.open('<?=Aplicacion::getRootUrl()?>front/accesodenegado/accesodenegado.php?errNo=<?=Aplicacion::Encrypter($e->getCodigo())?>&r=<?=Aplicacion::Encrypter($e->getTitulo())?>','_self');</script>

        <?
                exit;
            }

            ob_flush();
        ?>

    </body>
</html>