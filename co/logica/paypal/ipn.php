<?php

    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("paypal"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("emails"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));

	set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
	require_once 'Zend/Cache.php'; 
    try
    {        	
    	 
    	if(isset($_POST['custom']) && is_numeric($_POST['custom'])) {
    		
    		$id = $_POST['custom'];
    		$pedido = dmPedidos::getPedidoById($id);
    		$pagado = dmPedidos::PagoAceptado($id, $pedido->__get('idusuario'));
    		
    					    		
	        // Limpio la cache de producto
	        $IdCampania = dmPedidos::getIdCategoriaByIdPedido($id);
	        $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
			$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
			$cache->clean(
			    Zend_Cache::CLEANING_MODE_MATCHING_TAG,
			    array('group_cache_catalogo_campania_'.$IdCampania)
			);
    		
    		if($pagado) {
    		
	    		$camp = dmCampania::getByIdCampania($pedido->getIdCampania());
				$u = dmUsuario::getByIdUsuario($pedido->getIdUsuario());
				$email = dmPedidos::getDatosMailByIdPedido($pedido->getIdPedido());
				
	$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	$mesFn = $meses[date("n", strtotime($email["TiempoEntregaFn"])) - 1];
	$mesIn = $meses[date("n", strtotime($email["TiempoEntregaIn"])) - 1];
	
				$datos = array(
					'NumeroPedido' => $pedido->getIdPedido(),
					'SistemaPago' => 'PayPal',
					'NombreVidriera' => $camp->getNombre(),
					'IdUsuario' => $pedido->getIdUsuario(),
					'HashIdUsuario' => Aplicacion::Encrypter($pedido->getIdUsuario()),
					'NombreUsuario' => $u->getDatos()->getNombre(),
					'ApellidoUsuario' => $u->getDatos()->getApellido(),
					"TiempoEntregaFn" => date("d", strtotime($email["TiempoEntregaFn"]))." de ".$mesFn,
					"TiempoEntregaIn" => date("d", strtotime($email["TiempoEntregaIn"]))." de ".$mesIn
				);
				
				emailPagoAceptado($u->getDatos()->getEmail(), $datos);
				
			}
    		
    	}
    	   	        
    }
    catch(MySQLException $e)
    {
        throw $e;
    }
    
?>
