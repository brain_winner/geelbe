<html>
<body>
<?
     $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
     require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
     Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto"));
     Aplicacion::CargarIncludes(Aplicacion::getIncludes("mensajes"));
	
     require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
	 require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
	 require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
	 require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
	 require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php";
     
     Includes::Scripts();
     if ($_POST) {
        extract($_POST);
         try {
         
         	$fields = array(
				'txtMotivo' => array(
					'exception' => 'Motivo vac&iacute;o',
					'check' => 'isset'
				),
				
				'txtComentario' => array(
					'exception' => 'Comentario vac&iacute;o',
					'check' => 'isset'
				),
				
				'txtEmail' => array(
					'exception' => 'E-mail inv&aacute;lido',
					'check' => 'email'
				)					
			);

 			if (!isset($txtCaptcha) && isset($_SESSION['User']) && isset($_SESSION['User']['id'])) {
                $txtCaptcha = $_SESSION["CAPTCHAString"];
 			}
			else {
				$fields['txtCaptcha'] = array(
					'exception' => 'El c&oacute;digo de verificaci&oacute;n no coincide',
					'check' => 'isset'
				);
			}

			ValidatorUtils::checkFields($fields, $_POST, false);
                
            if ($txtCaptcha == $_SESSION["CAPTCHAString"]) {
                 
            	$oMensaje = new Mensajes();

            	if (!(is_null($_SESSION["User"]["id"]))) {
                    $oUsuario = dmUsuario::getByIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
                    
                    if ($oUsuario->getIdUsuario()==0) {
                    	throw new Exception();
                    }
                    else {
	                    $oMensaje->setidUsuario($oUsuario->getIdUsuario());
	                    $txtEmail = $oUsuario->getNombreUsuario();
                    }
            	}
            	else {
                    $oMensaje->setidUsuario("null");
            	}

                $oMensaje->setidMensajePadre("null");
                $oMensaje->setfecha(date("Y-m-d H:i:s"));
				$oMensaje->setemail($txtEmail);
                $oMensaje->setidmotivo($txtMotivo);
                $oMensaje->setmensaje($txtComentario . ' - ' . $txtNombre . ' ' . $txtApellido);
                $oMensaje->setrespondido(0);
                
                if($isCampaniaSel == 'false') {
                	$txtCampania = -2;
                }
                $oMensaje->setIdCampania($txtCampania);
                $Motivo = dmMensajes::getMotivoById($txtMotivo);
                
                dmMensajes::Insert($oMensaje);
                
                $emailBuilder = new EmailBuilder();
	            $emailData = $emailBuilder->generateMensajeBOEmail($oMensaje, $txtNombre, $txtApellido, $Motivo);
				
				$emailSender = new EmailSender();
				$emailSender->sendEmail($emailData);
				
				
                if ($_SESSION["User"]["id"]) {
                ?>
                     <script>
                     window.parent.msjInfo("Tu mensaje fue enviado correctamente.<br />En breve te responderemos.",  "Muchas Gracias <?= $nombre ?>", "<?=Aplicacion::getRootUrl()?>front/contacto-socio");              
                     </script>
                <?
                }
                else {
                 ?>                  
                     <script>
                     window.parent.msjInfo("Tu mensaje fue enviado correctamente.<br />En breve te responderemos.",  "Muchas Gracias <?= $nombre ?>", "<?=Aplicacion::getRootUrl()?>front/contacto");              
                     </script>
                 <?
                 }
             }
             else {
                 ?>
                    <script>
                        window.parent.msjError("El c&oacute;digo de verificaci&oacute;n no coincide", "Error");
                    </script>
                 <?
             }
         }
         catch(Exception $e) {
         	?>
         	<script>
         		window.parent.msjError("Error inesperado", "Error");
            </script>
            <?     
         }
     }
?>
</body>
</html>