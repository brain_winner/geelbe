<?
    class dmContacto
    {
        /**
        * @desc Obtiene nombre y apellido de la persona de contacto
        * @var string. El IdUsuario del que se obtiene los datos.
        * @return array.
        */
        public static function ObtenerApenom($idUsuario)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT DUP.Apellido, DUP.Nombre, U.NombreUsuario as Email FROM datosusuariospersonales DUP INNER JOIN usuarios U ON U.idUsuario = DUP.idUsuario WHERE U.idUsuario = '" . mysql_real_escape_string($idUsuario) . "'");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                if ($Tabla)
                    return $Tabla[0];
                else
                    return 0;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }


        public static function ObtenerApenomByEmail($Email)
        {
            $oConexion = Conexion::nuevo();
            try
            {
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT DUP.Apellido, DUP.Nombre FROM datosusuariospersonales DUP INNER JOIN usuarios U ON U.idUsuario = DUP.idUsuario WHERE U.NombreUsuario =  '".mysql_real_escape_string($Email)."'");
                $Tabla = $oConexion->DevolverQuery();
                $oConexion->Cerrar_Trans();
                if ($Tabla)
                    return $Tabla[0];
                else
                    return 0;
            }
            catch (MySQLException $e)
            {
                $oConexion->RollBack();
                throw $e;
            }
        }

    }
?>
