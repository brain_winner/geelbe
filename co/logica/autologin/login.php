<?
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/conexion/Conectar.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/usuarios/datamappers/dmusuariosfacebook.php";

	$Hash = $_GET["Hash"];
	$IdUsuario = $_GET["IdUsuario"];
	$Type = $_GET["Type"];
	$pedidoNro = $_GET["p"];
	$IdCampania = $_GET["IdCampania"];
	$IdCategoria = $_GET["IdCategoria"];
	$IdProducto = $_GET["IdProducto"];
	
	if(!is_numeric($IdUsuario)) {
		header("Location: /".$confRoot[1]."/front/login/");
		exit;
	}
	
	$objUsuario = dmUsuario::getByIdUsuario($IdUsuario);
	
	if(($objUsuario->getes_Activa() == "1" && $Type != "ACT") || ($objUsuario->getPadrino() == "Huerfano")) {
		header("Location: /".$confRoot[1]."/front/login/");
		exit;
	}
	
	if($objUsuario->getPadrino() == "visacolombiaoff1" || $objUsuario->getPadrino() == "visacolombiaon1")
			$_SESSION['padrino'] = true;
		else
			$_SESSION['padrino'] = false;
	
	
	if($Type == "FACEBOOKUSER") {
		$FacebookId = dmUsuariosFacebook::getFacebookId($IdUsuario);
		
		if($FacebookId == null || $FacebookId == "") {
			header("Location: /".$confRoot[1]."/front/login/");
			exit;
		}
		
		$FacebookHash = Hash::getHash($IdUsuario.$FacebookId);
		if (Hash::compareHash($Hash, $FacebookHash)) {
			$login = new LoginController();
		    $login->setobjUsuarios($objUsuario);
		    $login->guardarHistorico();
		    Aplicacion::loginUser($objUsuario);
		    $login->setCookie();
		    
	    	foreach($_GET as $key => $value) {
				$campaign .= $key."=".$value."&";
			}	
		    
			if($IdCampania == null && $IdCategoria == null && $IdProducto == null){
				header("Location: /".$confRoot[1]."/front/vidriera/index.php?".$campaign);
				exit;	
			} else if($IdCampania != null && $IdCategoria == null && $IdProducto == null){
				header("Location: /".$confRoot[1]."/front/catalogo/main.php?".$campaign);
				exit;	
			} else if($IdCampania != null && $IdCategoria != null && $IdProducto == null){
				header("Location: /".$confRoot[1]."/front/catalogo/index.php?".$campaign);
				exit;	
			} else if($IdCampania != null && $IdCategoria != null && $IdProducto != null){
				header("Location: /".$confRoot[1]."/front/catalogo/detalle.php?".$campaign);
				exit;
			} else {
				header("Location: /".$confRoot[1]."/front/vidriera/index.php");
				exit;
			} 
		} else {
			header("Location: /".$confRoot[1]."/front/login/");
			exit;
		}
	}
	
	$Email = $objUsuario->getNombreUsuario();
	$NewHash = Hash::getHash($IdUsuario.$Email);
if (Hash::compareHash($Hash, $NewHash)) {
	$login = new LoginController();
    $login->setobjUsuarios($objUsuario);
    $login->guardarHistorico();
    Aplicacion::loginUser($objUsuario);
    $login->setCookie();
	
	if(!isset($_COOKIE["gavc"])){
		foreach($_GET as $key => $value) {
					$campaign .= $key."=".$value."&";
				}	
	}else{
		foreach($_GET as $key => $value) {
				if($key == "Hash" || $key == "IdUsuario" || $key == "Type") {
					continue;
				} else {
					$campaign .= $key."=".$value."&";
				}	
			}
		}	
				
	if (isset($_GET["anchor"])){
	$campaign = $campaign."#".$_GET["anchor"];
	}

	//Autologin cookie
	
	// Desactivado por el momento
	
//	if($Type != "ACT") {
//		$NewHashCookie = Hash::getHash("5TvUmtcN".$IdUsuario.$Email."x2cPMuoW");
//		if(!isset($_COOKIE["gavc"])){
//			header("Location: /".$confRoot[1]."/front/login/index.php?".$campaign."TYPE=".$_GET["Type"]);
//			exit;
//		} else if (!Hash::compareHash($_COOKIE["gavc"], $NewHashCookie)){
//			header("Location: /".$confRoot[1]."/front/login/");
//			exit;
//		}
//	}
	//fin
		
	//Referenciados para usuarios Hotmail x unica vez
	$splitEmail = split("@",$Email);
	$emailDomain = $splitEmail[1];
	switch ($emailDomain) {
		case 'hotmail.com': case 'msn.com': case 'live.com': $emailProvider = "hotmail"; break;
	}
	
	if ($emailProvider == "hotmail" && $Type != "ACT"){
		$primerLogin = false;
		
		$oConexion = Conexion::nuevo();
		try {
			$oConexion->setQuery("SELECT hotmail_primer_login FROM usuarios WHERE NombreUsuario='".$Email."'");                
			$result = $oConexion->DevolverQuery();
			$primerLogin = $result[0]["hotmail_primer_login"];
		} catch (MySQLException $e) {
			throw $e;
		}
		if(!$primerLogin) {
			$oConexion = Conexion::nuevo();
			try {
				$oConexion->setQuery("UPDATE usuarios SET hotmail_primer_login = 1 WHERE NombreUsuario='".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($Email)), ENT_QUOTES)."'");   	
				$oConexion->EjecutarQuery();
			} catch (MySQLException $e) {
				throw $e;
			}	
			header("Location: /".$confRoot[1]."/front/referenciados/index.php?".$campaign);
			exit;
		}
	}
	//Fin
		
		
	if ($Type=="ACT") {
	    header("Location: /".$confRoot[1]."/logica/activacion/actionForm/activar.php?e=".$Email);
		exit;
	} else if($Type=="VIDRIERA") {
	    header("Location: /".$confRoot[1]."/front/vidriera/index.php?".$campaign);
		exit;
	} else if($Type=="MICUENTA") {
	    header("Location: /".$confRoot[1]."/front/micuenta/index.php?".$campaign);
		exit;
	} else if($Type=="REF") {
	    header("Location: /".$confRoot[1]."/front/referenciados/index.php?".$campaign);
		exit;
	} else if($Type=="CONTACTO") {
	    header("Location: /".$confRoot[1]."/front/contacto-socio/index.php?".$campaign);
		exit;
	} else if($Type=="INDEX_NEW") {
	    header("Location: /".$confRoot[1]."/front/referenciados/index.php?".$campaign);
		exit;
	}
	//Corresponde a las secciones de Acerca De
	  else if($Type=="FUNCIONAMIENTO") {
	    header("Location: /".$confRoot[1]."/front/funcionamiento/index.php?".$campaign);
		exit;
	} else if($Type=="ACERCA") {
	    header("Location: /".$confRoot[1]."/front/acerca/index.php?".$campaign);
		exit;
	} else if($Type=="QUIENES") {
	    header("Location: /".$confRoot[1]."/front/quienes/index.php?".$campaign);
		exit;
	} else if($Type=="COMPROMISOS") {
	    header("Location: /".$confRoot[1]."/front/compromisos/compromisos.php?".$campaign);
		exit;
	} else if($Type=="VENTAS") {
	    header("Location: /".$confRoot[1]."/front/ventas/index.php?".$campaign);
		exit;
	} else if($Type=="ENVIO") {
	    header("Location: /".$confRoot[1]."/front/envio/index.php?".$campaign);
		exit;
	} else if($Type=="INVITAR") {
	    header("Location: /".$confRoot[1]."/front/invitar/index.php?".$campaign);
		exit;
	}				   
	//FIN Acerca De.
	
	//Corresponde a MiCuenta
	 else if($Type=="MICREDITO") {
	    header("Location: /".$confRoot[1]."/front/micuenta/mi-credito.php?".$campaign);
		exit;
	} else if($Type=="MIPERFIL") {
	    header("Location: /".$confRoot[1]."/front/micuenta/mi-perfil.php?".$campaign);
		exit;
	} else if($Type=="MISPEDIDOS") {
	    header("Location: /".$confRoot[1]."/front/micuenta/mispedidos.php?".$campaign);
		exit;
	} else if($Type=="MISCONSULTAS") {
	    header("Location: /".$confRoot[1]."/front/micuenta/misconsultas.php?act=6&".$campaign);
		exit;
	} else if($Type=="FBKCONNECT") {
	    header("Location: /".$confRoot[1]."/front/micuenta/connect.php?".$campaign);
		exit;
	} else if($Type=="GEELBECASH") {
	    header("Location: https://".$confRoot[1]."/ssl/geelbecash/index.php?".$campaign);
		exit;
	}					
   	//FIN MiCuenta
	
	 else if($Type=="TERMINOS") {
	    header("Location: /".$confRoot[1]."/front/terminos/index.php?".$campaign);
		exit;
	}						
	
	// Auto-login para reanudar pago
	 else if($Type=="pago") {
        $cartUrlProtocol = "https://";
        if($_SERVER['HTTP_HOST'] == "localhost") {
			$cartUrlProtocol = "http://";
		}
	    header("Location: ".$cartUrlProtocol.$_SERVER['HTTP_HOST']."/".$confRoot[1]."/ssl/carrito/step_2.php?IdPedido=". $pedidoNro);
		exit;
	} 
	
	//Corresponde a Analitycs para campañas
	else if($Type=="CAMPAIGN") {
		if($IdCampania == null && $IdCategoria == null && $IdProducto == null){
		header("Location: /".$confRoot[1]."/front/vidriera/index.php?".$campaign);
		exit;	
		}
		else if($IdCampania != null && $IdCategoria == null && $IdProducto == null){
		header("Location: /".$confRoot[1]."/front/catalogo/main.php?".$campaign);
		exit;	
		}
		else if($IdCampania != null && $IdCategoria != null && $IdProducto == null){
		header("Location: /".$confRoot[1]."/front/catalogo/index.php?".$campaign);
		exit;	
		}
		else if($IdCampania != null && $IdCategoria != null && $IdProducto != null){
		header("Location: /".$confRoot[1]."/front/catalogo/detalle.php?".$campaign);
		exit;
		}
	}
		
	//Corresponde a recomendar producto
	else if($Type=="RECOMENDAR") {
		header("Location: /".$confRoot[1]."/front/catalogo/detalle.php?".$campaign);
		exit;	
	}
	
    header("Location: /".$confRoot[1]."/front/vidriera/index.php?".$campaign);
	exit;
	}
	else {
	header("Location: /".$confRoot[1]."/front/login/");
	exit;
	}
?>
	
   