<?php
    class MySQL_Ahijados extends ahijados
    {
        public function getInsert()
        {
            $strCampos="";
            $strValores="";
            $strCamposU="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if ($propiedad != "_idahijado")
                {
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }                
                if ($propiedad != "_idusuario" && $propiedad != "_idahijado")
                {
                    $strCamposU .=substr($propiedad,1,strlen($propiedad))." = ".chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }
            }
            return "INSERT INTO ahijados (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
    }
?>