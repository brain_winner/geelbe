<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
	
	class dmReferenciados
    {
        /**
        * @desc Indica si el usuario est� registrado.
        * @param NombreUsuario. El nombre de usuario en cuesti�n.
        * @param Activo. Indica si el usuario debe estar activo. 0 - Activo . 1 - Inactivo
        */
        public static function EsUsuarioRegistrado($NombreUsuario, $Activo="noImporta")
        {
         if(empty($NombreUsuario)){
		return false;
	  	 }try
            {
                $cn = Conexion::nuevo();
                $cn->Abrir();
                if($Activo == 0 || $Activo == 1)
                {
                    $SQL = "SELECT COUNT(*) as Usuario FROM usuarios WHERE NombreUsuario = '".mysql_real_escape_string($NombreUsuario)."' AND es_activa = " . mysql_real_escape_string($Activo);
                }
                elseif($Activo == "noImporta")
                {
                	$SQL = "SELECT COUNT(*) as Usuario FROM usuarios WHERE NombreUsuario = '".mysql_real_escape_string($NombreUsuario)."'";
                }
                else
                {
                    throw new Exception("Debe ingresar el segundo parametro opcional 1 para desactivo y 0 para activo");
                }
                $cn->setQuery($SQL);
                $Tabla =  $cn->DevolverQuery();
                return $Tabla[0]["Usuario"];
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
	        }
        /**
        * @desc
        */
        public static function getUsuario_byEmail($email)
        {
            try
            {
                $cn = Conexion::nuevo();
                $cn->Abrir_Trans();
                $SQL = "SELECT  * FROM usuarios WHERE NombreUsuario = '".mysql_real_escape_string($email)."'";
                $cn->setQuery($SQL);
                $Tabla =  $cn->DevolverQuery();
                $cn->Cerrar_Trans();
                return $Tabla[0];
            }
            catch(MySQLException $e)
            {
                throw $e;
            }
        }
        function InvitacionHistorico($collAhijados, $arrDatos)
        {
            foreach($collAhijados as $objAhijados)
            {
                if($arrDatos)
                {
                    if($arrDatos["Email"] != $objAhijados->getEmail())
                    {
                        $oRefH = new clsReferenciadoH();
                        $oRefH->setApellido(addslashes($arrDatos["Apellido"]));
                        $oRefH->setNombre(addslashes($arrDatos["Nombre"]));
                        $oRefH->setNombreUsuario($arrDatos["Email"]);
                        $oRefH->setMailAhijado($objAhijados->getEmail());
                        $oRefH->setFecha(date("Y/m/d H:i"));
                        dmReferenciadoH::Guardar($oRefH);
                    }
                }
            }
        	         
        }

        /**
         * Mete al ahijado en la db y ademas manda mail al mismo.
         *
         * @param array $collAhijados coleccion de $obj ahijado
         * @param unknown_type $mensaje
         * @param unknown_type $arrDatos
         * @param unknown_type $noEnviar True: No envia el mail al ahijado.
         */

        function Invitacion(array $collAhijados, $mensaje, $altMensaje, $arrDatos,$noEnviar=false)
        {
                $cn = Conexion::nuevo();
                $cn->Abrir();
                foreach ($collAhijados as $objAhijados)
                {
                    $cn->setQuery($objAhijados->getInsert());
                    $cn->EjecutarQuery();

                    if(!$noEnviar) {
	                    $para = $objAhijados->getEmail();
	                    $userId = $arrDatos["UserId"];
	                    if ($arrDatos) {
	                        if ($para != $arrDatos["Email"]) {
							   $objUsuario = dmusuario::getByIdUsuario($arrDatos["UserId"]);
	                        	
				   			   $emailBuilder = new EmailBuilder();
				   			   $emailData = $emailBuilder->generateInviteEmail($objUsuario, $para);
				   
							   $emailSender = new EmailSender();
							   $emailSender->sendEmail($emailData);
							   
							   $conexion = Conexion::nuevo();
							   $conexion->setQuery("insert geelbe_sent_invitations (invitation_from, invitation_to, invitation_date) values ('".mysql_real_escape_string($arrDatos["Email"])."','".mysql_real_escape_string($para)."', NOW())");
							   $conexion->EjecutarQuery();
							}
						}
					}
                }
        }      

        public static function estadoInicial($email){
        	$cn = Conexion::nuevo();
        	$cn->Abrir();

            $cn->setQuery("SELECT u.IdUsuario, u.NombreUsuario, u.es_activa, u.es_Provisoria, up.NombreUsuario as Padrino, up.IdUsuario as IdPadrino
							FROM ahijados AS a
							LEFT JOIN usuarios AS up ON up.IdUsuario = a.IdUsuario
							LEFT JOIN usuarios AS u ON u.IdUsuario = a.IdUsuarioReg
							WHERE a.email LIKE \"".mysql_real_escape_string($email)."\"
							AND (u.Padrino LIKE up.NombreUsuario OR u.Padrino IS NULL OR u.Padrino = \"\")
							ORDER BY a.IdAhijado ASC LIMIT 1");            	
            $Tabla = $cn->DevolverQuery();
            return $Tabla[0];

        }
        
        public static function actualizarPadrinoInvitaciones($NombreUsuario,$Padrino){        	
        	if(!checkemail($NombreUsuario) || !checkemail($Padrino)) return false;
        	
        	$cn = Conexion::nuevo();
        	$cn->Abrir();
   	   		$cn->setQuery('UPDATE usuarios SET Padrino = "'.htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($Padrino)), ENT_QUOTES).'" WHERE NombreUsuario = "'.htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($NombreUsuario)), ENT_QUOTES).'" AND Padrino LIKE "Huerfano" LIMIT 1');   	         
   	   		$cn->EjecutarQuery();	 
       }        	                    
   }
?>
