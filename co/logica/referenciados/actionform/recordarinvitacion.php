<?
    if($_POST)
    {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto", "dm"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("mailer"));
        $arrayHTML = Aplicacion::getIncludes("onlyget");
        
        
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
        require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
        
        try
        {
            $objUsuario = dmUsuario::getByIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
            foreach($_POST["frmEmail"] as $email)
            {
                $idUsuario = dmUsuario::getIdByNombreUsuario($email);
               if (!$idUsuario)
               {
				   $emailBuilder = new EmailBuilder();
				   $emailData = $emailBuilder->generateInviteEmail($objUsuario, $email);
				   
				   $emailSender = new EmailSender();
				   $emailSender->sendEmail($emailData);
				   
				   $conexion = Conexion::nuevo();
				   $conexion->setQuery("insert geelbe_sent_invitations (invitation_from, invitation_to, invitation_date) values ('".mysql_real_escape_string($objUsuario->getNombreUsuario())."','".mysql_real_escape_string($email)."', NOW())");
				   $conexion->EjecutarQuery();
               }
               else if(!(dmUsuario::EsUsuarioRegistrado($idUsuario)))
               {
                   $objAhijado = dmUsuario::getByIdUsuario(dmUsuario::getIdByNombreUsuario($email));
                   
               	   $emailBuilder = new EmailBuilder();
               	   $emailData = $emailBuilder->generateActivationEmail($objAhijado, $email);
					
				   $emailSender = new EmailSender();
				   $emailSender->sendEmail($emailData);
               }
               else if(!(dmUsuario::EsUsuarioComprador(dmUsuario::getIdByNombreUsuario($email))))
               {
                   $objAhijado = dmUsuario::getByIdUsuario(dmUsuario::getIdByNombreUsuario($email));
                   $oTexto = new Texto(Aplicacion::getRoot(). $arrayHTML["htmlCompra"]);
                   $oTexto->Read();
                   $oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
                   $oTexto->ReemplazarString("%NombrePadrino%", $objUsuario->getDatos()->getNombre());
                   $oTexto->ReemplazarString("%NombreUsuario%", $objAhijado->getDatos()->getNombre());
                   $oTexto->ReemplazarString("%IdUsuario%", dmUsuario::getIdByNombreUsuario($email));
                   $oTexto->ReemplazarString("%HashIdUsuario%", generar_hash_md5(dmUsuario::getIdByNombreUsuario($email)));
                   $oTexto->ReemplazarString("%Hash%", Hash::getHash(dmUsuario::getIdByNombreUsuario($email).$objAhijado->getNombreUsuario()));
                   
				   $emailData = new EmailData();
				   $emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
				   $emailData->setFromName("Geelbe");
				   $emailData->setTo($email);
				   $emailData->setBody($oTexto->getText());
				   $emailData->setAltBody("No alternative text yet.");
				   $emailData->setEmailType("recordatoriocompra");
				   $emailData->setSubject("Compr� en Geelbe");
				
				   $emailSender = new EmailSender();
				   $emailSender->sendEmail($emailData);
               }
            }
            ?>

                <script>window.parent.msjInfo("Se han enviado las invitaciones","Referenciar", "<?=Aplicacion::getRootUrl()?>front/micuenta/mi-credito.php?act=1");</script>
            <?
        }
        catch(MySQLException $e)
        {
            die($e->getMessage());
        }
        ob_flush();
    }
?>