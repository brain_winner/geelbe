<?php
    class MySQL_micuenta extends usuarios
    {
        public function getInsertUsuario()
        {
            $strCampos="";
            $strValores="";
            $Me = $this->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad != "_secciones")
                {
                    if($propiedad != "_idusuario" && $propiedad != "_fechaingreso")
                    {
                        $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                        $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    }
                }

            }
            return "INSERT INTO usuarios (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).")";
        }
        public function getUpdateUsuarios()
        {
            $strCampos="UPDATE usuarios SET ";
            $strCondicion=" WHERE 1=1";
            $Me = $this->__toArray();
            unset($Me["_fechaingreso"]);
            unset($Me["_padrino"]);
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad != "_secciones")
                {
                    if($propiedad == "_idusuario")
                    {
                        $strCondicion .=" AND ".substr($propiedad,1,strlen($propiedad))." = '".mysql_real_escape_string($valor)."'";
                    }
                    else
                    {
                        $strCampos .=substr($propiedad,1,strlen($propiedad))." = ".chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    }
                }
            }
            return substr($strCampos, 0, strlen($strCampos)-2).$strCondicion;
        }
        public function getDeleteUsuarios()
        {
            $Me = $this->__toArray();
            $strCampos="DELETE FROM usuarios WHERE idusuario = '".mysql_real_escape_string($Me["_idusuario"])."'";
            return $strCampos;
        }
        public function getInsertDatosPersonales()
        {
            $strCampos="";
            $strValores="";
            $strCamposU="";
            $Me = $this->getDatos()->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                //if($propiedad != "_fechanacimiento")
                //{
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    if($propiedad != "_idusuario")
                    {
                        $strCamposU .=substr($propiedad,1,strlen($propiedad))." = ".chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    }
            }
            return "INSERT INTO datosusuariospersonales (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getDeleteDatosPersonales()
        {
            $Me = $this->getDatos()->__toArray();
            $strCampos="DELETE FROM datosusuariospersonales WHERE idusuario = '".mysql_real_escape_string($Me["_idusuario"])."'";
            return $strCampos;
        }
        public function getInsertDatosDirecciones($index)
        {
            $strCampos="";
            $strValores="";
            $strCamposU="";
            $Me = $this->getDirecciones($index)->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                if($propiedad == "_idusuario" || $propiedad == "_idprovincia" || $propiedad == "_idpais")
                {
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=(($valor == 0)?"NULL":chr(34).mysql_real_escape_string($valor).chr(34)).", ";
                    if($propiedad != "_idusuario")
                    {
                        $strCamposU .=substr($propiedad,1,strlen($propiedad))." = ".(($valor == 0)?"NULL":chr(34).mysql_real_escape_string($valor).chr(34)).", ";
                    }
                }
                else
                {
                    if($propiedad != "_tipodireccion")
                        $strCamposU .=substr($propiedad,1,strlen($propiedad))." = ".chr(34).mysql_real_escape_string($valor).chr(34).", ";
                    $strCampos .=substr($propiedad,1,strlen($propiedad)).", ";
                    $strValores .=chr(34).mysql_real_escape_string($valor).chr(34).", ";
                }

            }
            return "INSERT INTO datosusuariosdirecciones (".substr($strCampos, 0, strlen($strCampos)-2).") VALUES (".substr($strValores, 0, strlen($strValores)-2).") ON DUPLICATE KEY UPDATE ".substr($strCamposU, 0, strlen($strCamposU)-2);
        }
        public function getDeleteDatosDirecciones($index)
        {
            $Me = $this->getDirecciones($index)->__toArray();
            $strCampos="DELETE FROM datosusuariosdirecciones WHERE idusuario = '".mysql_real_escape_string($Me["_idusuario"])."' AND idtipodireccion = '".mysql_real_escape_string($Me["_idtipodireccion"])."'";
            return $strCampos;
        }
        public function getUsuario($IdUsuario)
        {
            $strCondicion =" WHERE 1=1 AND IdUsuario = '".mysql_real_escape_string($IdUsuario)."'";

            return "SELECT idusuario, nombreusuario, clave, es_provisoria, padrino, fechaingreso, idperfil, es_activa FROM usuarios ".$strCondicion;
        }
        public function getUsuarioDatosPersonales($IdUsuario)
        {
            $strCampo = "";
            $strCondicion =" WHERE 1=1 AND IdUsuario = '".mysql_real_escape_string($IdUsuario)."'";
            $Me = $this->getDatos()->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampo .=substr($propiedad, 1, strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampo, 0, strlen($strCampo)-2)." FROM datosusuariospersonales ".$strCondicion;
        }
        public function getUsuarioDatosDirecciones($IdUsuario, $index)
        {
            $strCampo = "";
            $strCondicion =" WHERE 1=1 AND IdUsuario = '".mysql_real_escape_string($IdUsuario)."' AND TipoDireccion = '".mysql_real_escape_string($index)."'";
            $Me = $this->getDirecciones($index)->__toArray();
            foreach ($Me as $propiedad => $valor)
            {
                $strCampo .=substr($propiedad, 1, strlen($propiedad)).", ";
            }
            return "SELECT ".substr($strCampo, 0, strlen($strCampo)-2)." FROM datosusuariosdirecciones ".$strCondicion;
        }
        public function setUsuario(array $fila)
        {
            foreach ($fila as $atributo => $valor)
            {
                $this->__set(stripcslashes($valor), $atributo);
            }
        }
        public function setUsuarioDatosPersonales($fila)
        {
            foreach ($fila as $atributo => $valor)
            {
                $this->getDatos()->__set(stripcslashes($valor), $atributo);
            }
        }
        public function setUsuarioDatosDirecciones($index, $fila)
        {
        	if(!is_array($fila)){
        		$fila = array();
        	}
            foreach ($fila as $atributo => $valor)
            {
                $this->getDirecciones($index)->__set( stripcslashes($valor), $atributo);
            }
        }
    }
?>
