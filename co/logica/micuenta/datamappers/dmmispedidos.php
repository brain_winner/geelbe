<?php
    class dmMisPedidos
    {
        public static function getPedidoCampania($idPedido)
        {
            $oConexion = Conexion::nuevo();
            $oConexion->setQuery("SELECT idCampania FROM pedidos WHERE idPedido = " . $idPedido);
            $Tabla = $oConexion->DevolverQuery();
            return $Tabla[0]["idCampania"];
        }
        public static function getPedidoByIdUsuario($IdUsuario, $filro)
        {
            $oConexion = Conexion::nuevo();
            $oConexion->setQuery("SELECT * FROM (SELECT P.IdPedido, DATE_FORMAT(P.Fecha,'%d/%m/%Y') AS Fecha,C.Nombre AS Campania, IF(P.FechaEnvio IS NULL,'Pendiente', DATE_FORMAT(P.FechaEnvio, '%d/%m/%Y')) AS FechaEnvio,'DHL' as Mensajeria, EP.Descripcion AS Estado, Total AS Importe, IdEstadoPedidos FROM pedidos P INNER JOIN productos_x_pedidos PP ON P.IdPedido = PP.IdPedido LEFT JOIN campanias C ON C.IdCampania = P.IdCampania LEFT JOIN mensajerias M ON M.IdMensajeria = P.IdMensajeria LEFT JOIN estadospedidos EP ON EP.IdEstadoPedido = P.IdEstadoPedidos WHERE IdUsuario =". $IdUsuario ." GROUP BY P.IdPedido, P.Fecha,C.Nombre, P.FechaEnvio,M.Nombre, EP.Descripcion)Pedido WHERE (Campania LIKE '%".$filro."%' OR Campania LIKE '%".$filro."%' OR FechaEnvio LIKE '%".$filro."%' OR Estado LIKE '%".$filro."%' OR Importe LIKE '%".$filro."%' OR Fecha LIKE '%".$filro."%' OR IdPedido LIKE '%".$filro."%') ORDER BY IdPedido DESC");
                $Tabla = $oConexion->DevolverQuery();
                return $Tabla;
        }
        public static function getPedidoById($IdPedido)
        {
            $oConexion = Conexion::nuevo();
            $oConexion->setQuery("SELECT P.IdPedido, DATE_FORMAT( P.Fecha, '%d/%m/%Y' ) AS Fecha, P.IdFormaPago, C.Nombre AS Campania, IF( P.FechaEnvio IS NULL , 'Pendiente', DATE_FORMAT( P.FechaEnvio, '%d/%m/%Y' ) ) AS FechaEnvio, 'DHL' AS Mensajeria, EP.Descripcion AS Estado, P.IdEstadoPedidos, Total AS Importe, UNIX_TIMESTAMP(P.Fecha) AS FechaUnix, MotivoAnulacion, FP.codigodebarra AS CuponPago
FROM pedidos P INNER JOIN productos_x_pedidos PP ON P.IdPedido = PP.IdPedido
LEFT JOIN campanias C ON C.IdCampania = P.IdCampania
LEFT JOIN mensajerias M ON M.IdMensajeria = P.IdMensajeria
LEFT JOIN estadospedidos EP ON EP.IdEstadoPedido = P.IdEstadoPedidos
LEFT JOIN formaspagos_codigosdebarra FP ON FP.Pedido_Id = P.IdPedido WHERE P.IdPedido = ". $IdPedido ." GROUP BY P.IdPedido, P.Fecha, C.Nombre, P.FechaEnvio, M.Nombre, EP.Descripcion, P.IdFormaPago ");
            $Tabla = $oConexion->DevolverQuery();
            return $Tabla;
        }
		public static function getUsuariosPedidosPendientes()
        {
            $oConexion = Conexion::nuevo();
            $oConexion->setQuery("SELECT P.IdUsuario,P.IdEstadoPedidos, P.IdPedido, P.Total, C.Nombre FROM pedidos P INNER JOIN campanias C ON P.IdCampania = C.IdCampania WHERE P.IdEstadoPedidos in (0,1,2)");
            $Tabla = $oConexion->DevolverQuery();
            return $Tabla;
        }
        public static function getUsuariosPedidosPendientesById($IdUsuario)
        {
            $oConexion = Conexion::nuevo();
            $oConexion->setQuery("SELECT P.IdUsuario,P.IdEstadoPedidos, P.IdPedido, P.Total, C.Nombre FROM pedidos P INNER JOIN campanias C ON P.IdCampania = C.IdCampania WHERE P.IdEstadoPedidos in (0,1,2) AND P.IdUsuario =" . $IdUsuario);
            $Tabla = $oConexion->DevolverQuery();
            return $Tabla;
        }
        
        public function puedeAnularPedido($IdPedido) {
        
				$oConexion = Conexion::nuevo();
				$oConexion->setQuery("SELECT p.IdPedido FROM pedidos p, campanias c WHERE p.IdCampania = c.IdCampania AND p.IdEstadoPedidos = 0 AND c.FechaFin > NOW() AND timestampadd(hour, 2, p.fecha) < NOW() AND p.IdPedido = ".mysql_real_escape_string($IdPedido)." AND p.IdUsuario =".Aplicacion::Decrypter($_SESSION["User"]["id"]));
				$Tabla = $oConexion->DevolverQuery();
				return count($Tabla) > 0;
        
        }
    }
?>