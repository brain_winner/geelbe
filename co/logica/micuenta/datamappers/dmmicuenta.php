<?php

$confRoot = explode("/", dirname($_SERVER["SCRIPT_NAME"]));
require_once $_SERVER['DOCUMENT_ROOT'] . "/" . $confRoot[1] . "/logica/registro/clases/hash.php";

class dmMicuenta {

    public static function Actualizar(MySQL_micuenta $objUsuario) {
        $oConexion = Conexion::nuevo();
        try {
            $oConexion->Abrir_Trans();
            $oConexion->setQuery($objUsuario->getUpdateUsuarios());
            $oConexion->EjecutarQuery();
            $oConexion->setQuery($objUsuario->getInsertDatosPersonales());
            $oConexion->EjecutarQuery();
            $oConexion->setQuery($objUsuario->getInsertDatosDirecciones(0));
            $oConexion->EjecutarQuery();
            if ($objUsuario->getDirecciones(1)->getIdUsuario() > 0) {
                $oConexion->setQuery($objUsuario->getInsertDatosDirecciones(1));
                $oConexion->EjecutarQuery();
            }
            $oConexion->Cerrar_Trans();

            $sexoUsuario = "";
            if ($objUsuario->getDatos()->getIdTratamiento() == 1) {
                $sexoUsuario = "Hombre";
            } else if ($objUsuario->getDatos()->getIdTratamiento() == 2) {
                $sexoUsuario = "Mujer";
            }

            $provinciaUsuario = "";
            if ($objUsuario->getDirecciones(0)->getIdProvincia() > 0) {
                $oConexion->setQuery("SELECT Nombre FROM provincias WHERE IdProvincia = " . mysql_real_escape_string($objUsuario->getDirecciones(0)->getIdProvincia()));
                $DT = $oConexion->DevolverQuery();
                $oConexion->Cerrar();

                $provinciaUsuario = $DT[0]["Nombre"];
            }

            //Guardar en lista
            $oConexion->Abrir();

            $oConexion->setQuery('INSERT INTO acciones_listas_ems (email, nombre, apellido, tipo_accion, fecha_accion, accion_encolada, accion_commiteada, id_usuario, hash_usuario, fechanacimiento_usuario, sexo_usuario, provincia_usuario) 
            		VALUES ("' . htmlspecialchars(mysql_real_escape_string($objUsuario->getNombreUsuario()), ENT_QUOTES) . '", "' . htmlspecialchars(mysql_real_escape_string($objUsuario->getDatos()->getNombre()), ENT_QUOTES) . '", "' . htmlspecialchars(mysql_real_escape_string($objUsuario->getDatos()->getApellido()), ENT_QUOTES) . '", "M", NOW(), 0, 0, ' .
                    mysql_real_escape_string($objUsuario->getIdUsuario()) . ', "' . Hash::getHash($objUsuario->getIdUsuario() . $objUsuario->getNombreUsuario()) . '", "' . mysql_real_escape_string($objUsuario->getDatos()->getFechaNacimiento()) . '", "' .
                    htmlspecialchars(mysql_real_escape_string($sexoUsuario), ENT_QUOTES) . '", "' . htmlspecialchars(mysql_real_escape_string($provinciaUsuario), ENT_QUOTES) . '");');

            $oConexion->EjecutarQuery();
        } catch (MySQLException $e) {
            $oConexion->RollBack();
            throw $e;
        }
    }

    public static function GetUsuario($IdUsuario) {
        $oConexion = Conexion::nuevo();
        try {
            $objUsuario = new MySQL_micuenta();

            $oConexion->Abrir_Trans();
            $oConexion->setQuery($objUsuario->getUsuario($IdUsuario));
            $DT_Usuarios = $oConexion->DevolverQuery();

            $oConexion->setQuery($objUsuario->getUsuarioDatosPersonales($IdUsuario));
            $DT_UsuariosDatos = $oConexion->DevolverQuery();

            $oConexion->setQuery($objUsuario->getUsuarioDatosDirecciones($IdUsuario, 0));
            $DT_UsuariosDir1 = $oConexion->DevolverQuery();

            $oConexion->setQuery($objUsuario->getUsuarioDatosDirecciones($IdUsuario, 1));
            $DT_UsuariosDir2 = $oConexion->DevolverQuery();

            $oConexion->Cerrar_Trans();

            $objUsuario->setUsuario($DT_Usuarios[0]);
            $objUsuario->setUsuarioDatosPersonales($DT_UsuariosDatos[0]);
            $objUsuario->setUsuarioDatosDirecciones(0, $DT_UsuariosDir1[0]);
            $objUsuario->setUsuarioDatosDirecciones(1, $DT_UsuariosDir2[0]);
            return $objUsuario;
        } catch (MySQLException $e) {
            throw $e;
            $oConexion->RollBack();
        }
    }

    public static function GetFacebookId($IdUsuario) {
        $oConexion = Conexion::nuevo();
        try {
            $oConexion->setQuery("select FacebookId from usuarios_facebook where IdUsuario=" . mysql_real_escape_string($IdUsuario) . " limit 1");
            $fbkId = $oConexion->DevolverQuery();
            return $fbkId[0]["FacebookId"];
        } catch (MySQLException $e) {
            throw $e;
        }
    }

    public static function GetPaises() {
        $oConexion = Conexion::nuevo();
        try {
            $objUsuario = new MySQL_micuenta();
            $oConexion->Abrir();

            $oConexion->setQuery("SELECT * FROM paises ORDER BY Nombre");
            $DT = $oConexion->DevolverQuery();

            $oConexion->Cerrar();
            return $DT;
        } catch (MySQLException $e) {
            $oConexion->RollBack();
            throw $e;
        }
    }

    public static function GetPaisesById($Id) {
        $oConexion = Conexion::nuevo();
        try {
            $objUsuario = new MySQL_micuenta();
            $oConexion->Abrir();

            $oConexion->setQuery("SELECT * FROM paises WHERE IdPais = " . mysql_real_escape_string($Id) . " ORDER BY Nombre");
            $DT = $oConexion->DevolverQuery();

            $oConexion->Cerrar();
            return $DT;
        } catch (MySQLException $e) {
            $oConexion->RollBack();
            throw $e;
        }
    }

    public static function GetProvincias() {
        $oConexion = Conexion::nuevo();
        try {
            $oConexion->Abrir();

            $oConexion->setQuery("SELECT * FROM provincias ORDER BY Nombre");
            $DT = $oConexion->DevolverQuery();
            $oConexion->Cerrar();
            return $DT;
        } catch (MySQLException $e) {
            throw $e;
        }
    }

    public static function GetCiudades($IdDepartamento) {
        $oConexion = Conexion::nuevo();
        try {
            $oConexion->Abrir();

            $oConexion->setQuery("SELECT * FROM ciudades where IdProvincia=" . mysql_real_escape_string($IdDepartamento) . " ORDER BY Descripcion");
            $DT = $oConexion->DevolverQuery();
            $oConexion->Cerrar();
            return $DT;
        } catch (MySQLException $e) {
            throw $e;
        }
    }

    public static function GetCiudadById($IdCiudad) {
        $oConexion = Conexion::nuevo();
        try {
            $oConexion->Abrir();

            $oConexion->setQuery("SELECT * FROM ciudades where IdCiudad=" . mysql_real_escape_string($IdCiudad) . " ORDER BY Descripcion");
            $DT = $oConexion->DevolverQuery();
            $oConexion->Cerrar();
            return $DT;
        } catch (MySQLException $e) {
            throw $e;
        }
    }

    public static function GetProvinciasById($Id) {
        $oConexion = Conexion::nuevo();
        try {
            $oConexion->Abrir();

            $oConexion->setQuery("SELECT * FROM provincias WHERE IdProvincia = " . mysql_real_escape_string($Id) . " ORDER BY Nombre");
            $DT = $oConexion->DevolverQuery();
            $oConexion->Cerrar();
            return $DT;
        } catch (MySQLException $e) {
            throw $e;
        }
    }

    public static function GetProvincias_x_Pais($Pais) {
        $oConexion = Conexion::nuevo();
        try {
            $oConexion->Abrir();

            $oConexion->setQuery("SELECT * FROM provincias PR WHERE PR.IdPais = " . mysql_real_escape_string($Pais) . " ORDER BY PR.Nombre");
            $DT = $oConexion->DevolverQuery();

            $oConexion->Cerrar();
            return $DT;
        } catch (MySQLException $e) {
            $oConexion->RollBack();
            throw $e;
        }
    }

    /* BW */

    /**
     *
     * @param type $numDoc    
     * @return type array $custPersonalInfo
     */
    function validateDoc($numDoc) {
        $sql = 'select * from datosusuariospersonales where NroDoc like "' . $numDoc . '"';
        $result = mysql_query($sql);
        $custPersonalInfo = array();
        while ($res = mysql_fetch_array($result)) {
            $custPersonalInfo = dmMicuenta::setCustomerPersonalInfo($res);
            //array_push($custPersonalInfo, $this->setCustomerPersonalInfo($res));
        }

        return $custPersonalInfo;
    }
    
        
    /*
     * $userId @bigint(20)
     * $numDoc @varchar(20)
     * return array()
     */    
    function setCustomerPersonalInfo($dataInfo) {
        $personalInfo=array();
        
        $personalInfo['IdUsuario']=$dataInfo['IdUsuario'];
        $personalInfo['FechaNacimiento']=$dataInfo['FechaNacimiento'];
        $personalInfo['Nombre']=$dataInfo['Nombre'];
        $personalInfo['Apellido']=$dataInfo['Apellido'];
        $personalInfo['Apellido2']=$dataInfo['Apellido2'];
        $personalInfo['TipoDOc']=$dataInfo['TipoDOc'];
        $personalInfo['NroDoc']=$dataInfo['NroDoc'];
        $personalInfo['codtelefono']=$dataInfo['codtelefono'];
        $personalInfo['Email']=$dataInfo['Email'];
        $personalInfo['CIF']=$dataInfo['CIF'];
        $personalInfo['IdTipoUsuario']=$dataInfo['IdTipoUsuario'];
        $personalInfo['es_EnviarDatosCorreo']=$dataInfo['es_EnviarDatosCorreo'];
        $personalInfo['idTratamiento']=$dataInfo['idTratamiento'];
        $personalInfo['celuempresa']=$dataInfo['celuempresa'];
        $personalInfo['celucodigo']=$dataInfo['celucodigo'];
        $personalInfo['celunumero']=$dataInfo['celunumero'];
        $personalInfo['es_Completo']=$dataInfo['es_Completo'];
        
        return $personalInfo;
        
    }

}

?>