<?php
    class dmMisCreditos
    {
        public static function VIEWgetByIdUsuario($idUsuario)
        {
            try
            {
                $cn = Conexion::nuevo();
                $cn->Abrir();
                $cn->setQuery("SELECT * FROM(
        SELECT A.*, IF(SUM(BP.Importe) IS NULL,0,SUM(BP.Importe)) AS Importe
        FROM (
        SELECT BU.IdBono, CONCAT( 'Pesos obtenidos por ', ' ', TR.Descripcion ) AS Descripcion, UNIX_TIMESTAMP( BU.Fecha ) + RN.Caducidad * ( 24 *60 *60 ) AS FechaCaducidad, Aplica1 AS Valor, IdEstadoBono
        FROM bonos_x_usuarios BU
        INNER JOIN reglas_aplicadas RA ON BU.IdReglaAplicada = RA.IdReglaAplicada
        INNER JOIN reglasnegocio RN ON RA.IdRegla = RN.IdRegla
        INNER JOIN tiposbeneficios TB ON TB.IdBeneficio = RN.Beneficio1
        INNER JOIN tiposreglas TR ON TR.IdTipoRegla = RN.IdTipoRegla
        INNER JOIN usuarios U ON BU.IdUsuario = U.IdUsuario
        WHERE BU.IdEstadoBono = 0 AND U.IdUsuario = ".mysql_real_escape_string($idUsuario)."
        AND RN.Beneficio1 =1 AND BU.IdEstadoBono = 0
        AND (
        UNIX_TIMESTAMP( BU.Fecha ) + RN.Caducidad * ( 24 *60 *60 ) > UNIX_TIMESTAMP(now()) 
        ))A 
        LEFT JOIN bonos_x_pedidos BP ON BP.IdBono = A.IdBono
        GROUP BY A.IdBono, A.Descripcion, A.FechaCaducidad, A.Valor, A.IdEstadoBono)B
        WHERE NOT Importe = Valor AND IdEstadoBono =0");
                $Tabla = $cn->DevolverQuery();
                $cn->Cerrar();
                return $Tabla;
            }
            catch(MySQLException $ex)
            {
                throw $ex;
            }
        }
        public static function CountPotencialesgetByIdUsuario($idUsuario)
        {
            try
            {
                $cn = Conexion::nuevo();
                $cn->Abrir();
                $cn->setQuery("SELECT count(Potenciales.IdAhijado) as Total FROM (
                SELECT IdAhijado, Email, IdUsuario, 
                IF(IFNULL((SELECT IdUsuario FROM usuarios U WHERE A.IdUsuarioReg = U.IdUsuario),0) = 0,'Pendiente', 'Realizado') AS Registrado, 
                IF(IFNULL((SELECT COUNT(*) FROM usuarios U INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario AND P.Total > 50000 WHERE A.IdUsuarioReg = U.IdUsuario),0) = 0,'Pendiente', 'Concretada') AS Compro, 
                IFNULL((SELECT 1 FROM usuarios U WHERE U.IdUsuario = A.IdUsuarioReg AND U.Padrino = \"".Aplicacion::Decrypter($_SESSION["User"]["email"])."\"),0) AS SoyPadrino 
                FROM ahijados A WHERE IdUsuario = " . $idUsuario.") Potenciales");
                $Tabla = $cn->DevolverQuery();
                return $Tabla[0]["Total"];
            }
            catch(MySQLException $ex)
            {
                throw $ex;
            }
        }
        public static function VIEWPotencialesgetByIdUsuario($idUsuario, $Filtro="", $columna=2, $sentido="ASC", $offset=0, $cantidad=10)
        {
            try
            {
                $cn = Conexion::nuevo();
                $cn->Abrir();
                $cn->setQuery("	SELECT * 
								FROM 
								  ( SELECT  IdAhijado, 
								            Email, 
								            IdUsuario,
								            IF(IFNULL((SELECT IdUsuario FROM usuarios U WHERE A.IdUsuarioReg = U.IdUsuario),0) = 0,'Pendiente', 'Realizado') AS Registrado, 
								            IF(IFNULL((SELECT COUNT(*) FROM usuarios U INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario WHERE A.IdUsuarioReg = U.IdUsuario AND P.Total > 50000 AND P.IdEstadoPedidos IN (2, 3, 6)),0) = 0,'Pendiente', 'Concretada') AS Compro, 
								            IFNULL((SELECT 1 FROM usuarios U WHERE U.IdUsuario = A.IdUsuarioReg AND U.Padrino = '".Aplicacion::Decrypter($_SESSION["User"]["email"])."'),0) AS SoyPadrino 
								    FROM ahijados A 
								    WHERE IdUsuario = $idUsuario
								  ) Potenciales  
								ORDER BY $columna $sentido 
								LIMIT $offset, $cantidad");
                $Tabla = $cn->DevolverQuery();
                $Potencial = 0;
                $cn->setQuery("SELECT SUM(Aplica1) AS Importe FROM reglasnegocio WHERE IdTipoRegla = 6");
                $Importe = $cn->DevolverQuery();
                $Importe = $Importe[0]["Importe"];
                
                if(count($Tabla)>0)
                {   
                    foreach($Tabla as $i=>$fila)
                    {
                        if(($fila["Registrado"] == "Pendiente" && $fila["SoyPadrino"] == 0) || ($fila["Registrado"] == "Realizado" && $fila["SoyPadrino"] == 1) && $fila["Compro"] != "Concretada")
                        {
                            $Tabla[$i]["Bono"] = $Importe;
                        }
                        elseif($fila["SoyPadrino"] == 0 && $fila["Registrado"] == "Realizado")
                        {
                            $Tabla[$i]["Bono"] = 0;
                        }
                        elseif($fila["Compro"] == "Concretada" && $fila["SoyPadrino"] == 1)
                        {
                            $cn->setQuery("SELECT IdUsuario FROM usuarios WHERE NombreUsuario = \"".$fila["Email"]."\"");
                            $RA = $cn->DevolverQuery();
                            $cn->setQuery("SELECT IFNULL(COUNT(*),0) AS ReglaAplicada FROM reglas_aplicadas RA INNER JOIN reglasnegocio RN ON RN.IdRegla = RA.IdRegla WHERE RA.IdUsuario = ".mysql_real_escape_string($idUsuario)." AND IdAhijado = ".$RA[0]["IdUsuario"]);
                            $RA = $cn->DevolverQuery();
                            $Tabla[$i]["Regla"]="Aplicada";
                            $Tabla[$i]["Bono"] = 0;
                            $Tabla[$i]["BonoA"] = $Importe;
                        }
                        else
                        {
                            $Tabla[$i]["Bono"] = 0;
                        }
                    }
                }
                $cn->Cerrar();
                return $Tabla;
            }
            catch(MySQLException $ex)
            {
                throw $ex;
            }
        }
        
           public static function getCreditoDisponible($idUsuario) {
            try {
                $cn = Conexion::nuevo();
                $cn->setQuery("SELECT SUM(CD.Valor)-SUM(CD.Importe) AS CreditoDisponible 
								FROM (
								  SELECT Aplica1 AS Valor, IF(SUM(BP.Importe) IS NULL,0,SUM(BP.Importe)) AS Importe
								  FROM bonos_x_usuarios BU
								  INNER JOIN reglas_aplicadas RA ON BU.IdReglaAplicada = RA.IdReglaAplicada
								  INNER JOIN reglasnegocio RN ON RA.IdRegla = RN.IdRegla
								  INNER JOIN usuarios U ON BU.IdUsuario = U.IdUsuario
								  LEFT JOIN bonos_x_pedidos BP ON BP.IdBono = BU.IdBono 
								  WHERE BU.IdEstadoBono = 0 AND U.IdUsuario = ".mysql_real_escape_string($idUsuario)." AND RN.Beneficio1 =1 AND BU.IdEstadoBono = 0 AND ( UNIX_TIMESTAMP( BU.Fecha ) + RN.Caducidad * ( 24 *60 *60 ) > UNIX_TIMESTAMP(now()) )
								  GROUP BY BU.IdBono
								) CD");
                $Tabla = $cn->DevolverQuery();
                return intval($Tabla[0]["CreditoDisponible"]);
            } catch(MySQLException $ex) {
                throw $ex;
            }
        }
        
       public static function getCreditoPotencial($idUsuario, $Email) {
            try {
                $cn = Conexion::nuevo();
                $cn->setQuery("select sum(CP.Valor) as CreditoPotencial
								from (
								  select A.Valor
								  from ahijados A
								  left join usuarios U on A.IdUsuarioReg =  U.IdUsuario
								  left join pedidos P on P.IdUsuario = U.IdUsuario 
								  where A.IdUsuario = ".mysql_real_escape_string($idUsuario)."
								  and (U.Padrino = '".mysql_real_escape_string($Email)."' or U.Padrino IS NULL)
								  and (P.Total is null or P.IdEstadoPedidos NOT IN (2, 3, 6) or (P.Total < 50000 and P.IdEstadoPedidos IN (2, 3, 6)))
								  group by A.IdAhijado
								) CP");
                $Tabla = $cn->DevolverQuery();
                return $Tabla[0]["CreditoPotencial"];
            } catch(MySQLException $ex) {
                throw $ex;
            }
        }
        
        public static function PotencialesByIdUsuario($idUsuario, $email, $Filtro="", $columna=2, $sentido="ASC")
        {
            try
            {
                $cn = Conexion::nuevo();
                $cn->Abrir();
                $cn->setQuery("SELECT * FROM (
                SELECT IdAhijado, Email, IdUsuario, 
                IF(IFNULL((SELECT IdUsuario FROM usuarios U WHERE A.IdUsuarioReg = U.IdUsuario),0) = 0,'Pendiente', 'Realizado') AS Registrado, 
                IF(IFNULL((SELECT COUNT(*) FROM usuarios U INNER JOIN pedidos P ON P.IdUsuario = U.IdUsuario AND P.Total > 50000 WHERE A.IdUsuarioReg = U.IdUsuario),0) = 0,'Pendiente', 'Concretada') AS Compro, 
                IFNULL((SELECT 1 FROM usuarios U WHERE U.IdUsuario = A.IdUsuarioReg AND U.Padrino = \"".$email."\"),0) AS SoyPadrino 
                FROM ahijados A WHERE IdUsuario = " . $idUsuario.")Potenciales 
                WHERE (Registrado LIKE '%".$Filtro."%' OR Compro LIKE '%".$Filtro."%' OR Email LIKE '%".$Filtro."%' ) ORDER BY $columna $sentido");
                $Tabla = $cn->DevolverQuery();
                $Potencial = 0;
                $cn->setQuery("SELECT SUM(Aplica1) AS Importe FROM reglasnegocio WHERE IdTipoRegla = 6");
                $Importe = $cn->DevolverQuery();
                $Importe = $Importe[0]["Importe"];
                
                if(count($Tabla)>0)
                {   
                    foreach($Tabla as $i=>$fila)
                    {
                        if(($fila["Registrado"] == "Pendiente" && $fila["SoyPadrino"] == 0) || ($fila["Registrado"] == "Realizado" && $fila["SoyPadrino"] == 1) && $fila["Compro"] != "Concretada")
                        {
                            $Tabla[$i]["Bono"] = $Importe;
                        }
                        elseif($fila["SoyPadrino"] == 0 && $fila["Registrado"] == "Realizado")
                        {
                            $Tabla[$i]["Bono"] = 0;
                        }
                        elseif($fila["Compro"] == "Concretada" && $fila["SoyPadrino"] == 1)
                        {
                            $cn->setQuery("SELECT IdUsuario FROM usuarios WHERE NombreUsuario = \"".$fila["Email"]."\"");
                            $RA = $cn->DevolverQuery();
                            $cn->setQuery("SELECT IFNULL(COUNT(*),0) AS ReglaAplicada FROM reglas_aplicadas RA INNER JOIN reglasnegocio RN ON RN.IdRegla = RA.IdRegla WHERE RA.IdUsuario = ".mysql_real_escape_string($idUsuario)." AND IdAhijado = ".$RA[0]["IdUsuario"]);
                            $RA = $cn->DevolverQuery();
                            if($RA[0]["ReglaAplicada"] == 1)
                            {
                                $Tabla[$i]["Regla"]="Aplicada";
                                $Tabla[$i]["Bono"] = 0;
                                $Tabla[$i]["BonoA"] = $Importe;
                            }
                            else
                            {
                                $Tabla[$i]["Regla"]="Pendiente";
                                $Tabla[$i]["Bono"] = $Importe;
                            }
                        }
                        else
                        {
                            $Tabla[$i]["Bono"] = 0;
                        }
                    }
                }
                $cn->Cerrar();
                return $Tabla;
            }
            catch(MySQLException $ex)
            {
                throw $ex;
            }
        }
        public static function VIEWgetByIdPedido($IdPedido)
        {
            try
            {
                $cn = Conexion::nuevo();
                $cn->Abrir();
                $cn->setQuery("SELECT IF (SUM(Importe) IS NULL, 0, SUM(Importe)) AS Bono FROM bonos_x_pedidos WHERE IdPedido = ".mysql_real_escape_string($IdPedido));
                $Tabla = $cn->DevolverQuery();
                $cn->Cerrar();
                return $Tabla;
            }
            catch(MySQLException $ex)
            {
                throw $ex;
            }
        }
    }  
?>
