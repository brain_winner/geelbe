<?php
  class dmmisinvitados{

    public static function DMU_Cargar_UsuarioBonosAhijados($IdUsuario)
    {
        try
        {
            $cn = Conexion::nuevo();
            $cn->Abrir();
            $cn->setQuery("SELECT Email FROM ahijados WHERE IdUsuario =" . $IdUsuario);        
            $Tabla = $cn->DevolverQuery();
            $cn->Cerrar();
            return $Tabla;
         }
        catch(MySQLException $ex)
        {
            throw $ex;
        }
}

    public static function DMU_Cargar_UsuarioAhijados($IdUsuario)//traigo los bonos correspondientes a el usuario
    {
        try
        {
            $cn = Conexion::nuevo();
            $cn->Abrir();
            $cn->setQuery("SELECT bonos.descripcion, bonos_x_usuarios.vencimiento, bonos.importe FROM bonos INNER JOIN bonos_x_usuarios ON bonos.IdBono = bonos_x_usuarios.IdBono
            WHERE bonos_x_usuarios.IdUsuario =" . $IdUsuario);
            $Fila = $cn->DevolverQuery();
            $cn->Cerrar();
            return $Fila;
         }
        catch(MySQLException $ex)
        {
            throw $ex;
        }
    }

    public static function DMU_UsuarioRegistrado($email)//se fija en la DB si el email se encuentra registrado
    {
        try
        {
            $cn = Conexion::nuevo();
            $cn->Abrir();
            $cn->setQuery("SELECT NombreUsuario FROM usuarios WHERE NombreUsuario = '" . $email . "'");
            $Fila = $cn->DevolverQuery();
            $cn->Cerrar();
            if (($Fila))
                return 1;
            return -1;
        }
        catch(MySQLException $e)
        {
            throw $e;
        }        
    }

    public static function DMU_AhijadoCompro($email)//se fija en la base de datos si el ahijado realizo una compra
    {       
        try
        {
            $cn = Conexion::nuevo();
            $cn->Abrir();
            $cn->setQuery("SELECT * FROM pedidos INNER JOIN usuarios ON pedidos.IdUsuario = usuarios.IdUsuario
            WHERE usuarios.NombreUsuario = '" . $email . "'");
            $Fila = $cn->DevolverQuery();
            $cn->Cerrar();
            if (($Fila))
            {
                return 1;
                
            }
            return -1;
        }
        catch(MySQLException $e)
        {
            throw $e;
        }        
    }

    
}  

?>
