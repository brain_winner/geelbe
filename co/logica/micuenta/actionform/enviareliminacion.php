<html>
    <body>
<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";

    try {
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("scripts"));
        Includes::Scripts();

        $dcode = time();
        $userid = Aplicacion::Decrypter($_SESSION["User"]["id"]);
        $hashcode = md5("c".$dcode."u".$userid);
        
        
        $oConexion = Conexion::nuevo();
		try {
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("INSERT INTO codigo_desuscripcion (IdUsuario, CodigoDesuscripcion, Fecha) VALUES (".$userid.",'".$dcode."', NOW())");
			$oConexion->EjecutarQuery();
			$oConexion->Cerrar_Trans();
		}
		catch (MySQLException $e) {
			$oConexion->RollBack();
			throw $e;
		}
		
        $emailBuilder = new EmailBuilder();
	    $emailData = $emailBuilder->generateEliminacionUsuarioEmail(Aplicacion::Decrypter($_SESSION["User"]["nombre"]), Aplicacion::Decrypter($_SESSION["User"]["id"]), Aplicacion::Decrypter($_SESSION["User"]["email"]), $hashcode);
		
		$emailSender = new EmailSender();
		$emailSender->sendEmail($emailData);

    }
    catch(exception $e) {
        throw $e;
    }
?>
    <script>
        msjInfo(arrErrores["MICUENTA"]["ENVIO_MAIL_DESACTIVACION"], "Mi cuenta", "<?='/'.$confRoot[1].'/'?>" + "front/micuenta");
    </script>
    </body>
</html>