<html>
    <body>
<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
	
    try {
		$isPageOk = true;
		$pageException = "Exception";
    	
		// Validaciones de parametros basicas
        if (!isset($_GET["id"])) { // IdCampania Seteado
            $isPageOk = false;
			$pageException = "IdUsuario not setted";
		}
		else {
			$IdUsuario = $_GET["id"];
	    	if (!ValidatorUtils::validateNumber($IdUsuario)) { // IdUsuario Numerico
				$isPageOk = false;
				$pageException = "IdUsuario is not a number";
			}
			else if (!isset($_GET["hd"])) { // Hash Seteado
            	$isPageOk = false;
				$pageException = "Hash not setted";
			}
			else {
				$Hash = $_GET["hd"];
				
				$oConexion = Conexion::nuevo();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("SELECT CodigoDesuscripcion FROM codigo_desuscripcion WHERE IdUsuario = ".mysql_real_escape_string($IdUsuario)." AND ( Fecha between now() - INTERVAL 7 DAY and now())");
                
                $Tabla = $oConexion->DevolverQuery();
                
				if (count($Tabla) > 0) {
					
					$dcode = $Tabla[0]["CodigoDesuscripcion"];
        			$userid = $IdUsuario;
        			$hashcode = md5("c".$dcode."u".$userid);
					
        			if ($hashcode != $Hash) {
        				$isPageOk = false;
						$pageException = "Invalid Desuscription - Hash not valid";
        			}
                }
                else {
            		$isPageOk = false;
					$pageException = "Invalid Desuscription - Field in DB not Exist or Expired";
                }
				
			}
			
		}
    }
    catch(exception $e) {
		header('HTTP/1.1 404 Not Found');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }  
    
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
	Includes::Scripts();
	if(!$isPageOk) {
	    ?>
            <script>
                msjInfo("Los datos de eliminaci&oacute;n de cuenta ingresados son incorrectos. Por favor verifique la url ingresada.", "Mi cuenta", DIRECTORIO_URL_JS);
            </script>
        <?
	} else {
		try {
			dmUsuario::DesactivarCuenta($_GET["id"]);
			Aplicacion::logoutUser();
			?>
				<script>
					msjInfo(arrErrores["MICUENTA"]["DESACTIVACION"], "Mi cuenta", DIRECTORIO_URL_JS);
				</script>
			<?
		}
		catch(exception $e)
		{
			throw $e;
		}
	}
?>
    </body>
</html>