<?php
    if($_POST)
    {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));
        require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
        extract($_POST);
        try {
        
			$fields = array(
				'frmU[txtEmail]' => array(
					'exception' => 'Nombre de usuario vac&iacute;o',
					'check' => 'isset'
				),
				
				'frmDaU[txtNombre]' => array(
					'exception' => 'Nombre vac&iacute;o',
					'check' => 'isset'
				),
				
				'frmDaU[txtApellido]' => array(
					'exception' => 'Apellido vac&iacute;o',
					'check' => 'isset'
				),
									
				'cmb_nacimientoAnio' => array(
					'exception' => 'Fecha de nacimiento inv&aacute;lida',
					'check' => 'year'
				),
									
				'cmb_nacimientoMes' => array(
					'exception' => 'Fecha de nacimiento inv&aacute;lida',
					'check' => 'month'
				),
									
				'cmb_nacimientoDia' => array(
					'exception' => 'Fecha de nacimiento inv&aacute;lida',
					'check' => 'day'
				),
									
			);

			ValidatorUtils::checkFields($fields, $_POST, false);
			        
            $frmU["txtClave"] = ($frmU["txtClaveNuevo1"] != "") ? Hash::getHash($frmU["txtClaveNuevo1"]) : $frmU["txtClave"];
            $frmU["txtEmail"] = ($frmU["txtEmailNuevo1"] != "") ? $frmU["txtEmailNuevo1"] : $frmU["txtEmail"];
            $objUsuario = new MySQL_micuenta();
            $objUsuario->setIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
            $objUsuario->setClave(limpiar_tags($frmU["txtClave"]));
            $objUsuario->setNombreUsuario(limpiar_tags($frmU["txtEmail"]));
            $objUsuario->setes_Activa(0);
            $objUsuario->setes_Provisoria(0);
            $objUsuario->setIdPerfil(1);
            $objDatos = new DatosUsuariosPersonales();
            $objDatos->setApellido(addslashes(limpiar_tags($frmDaU["txtApellido"])));
            $objDatos->setApellido2(addslashes(limpiar_tags($frmDaU["txtApellido2"])));
            $objDatos->setCIF(limpiar_tags($frmDaU["txtCif"]));
            $objDatos->setEmail(limpiar_tags($frmU["txtEmail"]));
            $objDatos->setIdTratamiento(limpiar_tags($frmDaU["cbxTratamiento"]));
            $objDatos->setIdUsuario($objDatos->getTelefono());
            $objDatos->setNombre(addslashes(limpiar_tags($frmDaU["txtNombre"])));
            $objDatos->setNroDoc(limpiar_tags($frmDaU["txtNroDoc"]));
            $objDatos->setCodTelefono(limpiar_tags($frmDaU["txtCodTelefono"]));
            $objDatos->setTelefono(limpiar_tags($frmDaU["txtTelefono"]));
            $objDatos->setceluempresa(limpiar_tags($frmDaU["cmb_CelularComp"]));
            $objDatos->setcelucodigo(limpiar_tags($frmDaU["txt_CelularCodigo"]));  
            $objDatos->setcelunumero(limpiar_tags($frmDaU["txt_CelularNumero"]));  
            $objDatos->setTipoDoc(0);
            $objDatos->setIdTipoUsuario(1);
            $objDatos->setIdUsuario($objUsuario->getIdUsuario());
            $objDatos->setes_EnviarDatosCorreo(0);
            $objDatos->setFechaNacimiento(limpiar_tags($_POST["cmb_nacimientoAnio"])."-".limpiar_tags($_POST["cmb_nacimientoMes"])."-".limpiar_tags($_POST["cmb_nacimientoDia"]) );
            $objDatos->esCompleto();
            $objUsuario->setDatos($objDatos);
            $objDireccion = new DatosUsuariosDirecciones();
            $objDireccion->setCP(limpiar_tags($frmDi1["txtCodigoPostal"]));
            $objDireccion->setDomicilio(addslashes(limpiar_tags($frmDi1["txtDomicilio"])));
            $objDireccion->setEscalera(limpiar_tags($frmDi1["txtEscalera"]));
            $objDireccion->setIdPais(limpiar_tags($frmDi1["cbxPais"]));
            $objDireccion->setIdProvincia(limpiar_tags($frmDi1["cbxProvincia"]));
            $objDireccion->setIdCiudad(limpiar_tags($frmDi1["cbxCiudad"]));
            $objDireccion->setTipoDireccion(0);
            $objDireccion->setIdUsuario($objUsuario->getIdUsuario());
            $objDireccion->setNumero(limpiar_tags($frmDi1["txtNumero"]));
            $objDireccion->setPoblacion(addslashes(limpiar_tags($frmDi1["txtPoblacion"])));
            $objDireccion->setPiso(limpiar_tags($frmDi1["txtPiso"]));
            $objDireccion->setPuerta(limpiar_tags($frmDi1["txtPuerta"]));
            $objDireccion->setTelefono($objDatos->getTelefono());
            $objDireccion->setTipoCalle(limpiar_tags($frmDi1["cbxTipoCalle"]));
            $objDireccion->esCompleto();
            $objUsuario->setDirecciones(0, $objDireccion);
            dmMicuenta::Actualizar($objUsuario);
        ?>
                <script>
                    window.parent.ErrorMSJ("Actualizacion de mi cuenta", "EXITO");
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            die($e->getMessage());
            ?>
                <script>
                    window.parent.ErrorMSJ("Actualizacion de mi cuenta", "0");
                </script>
            <?
        }  
        ob_flush();
    }

	function limpiar_tags($tags) { 
        $tags = strip_tags($tags); 
		$tags = stripslashes($tags); 
		$tags = htmlentities($tags); 
		return $tags; 
	} 
?>