<?php
    if($_POST)
    {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));
        require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/registro/clases/hash.php";
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
        extract($_POST);
        try {
       
	        if(strlen($frmU["txtClaveNuevo1"]) < 6) {
		        ?>
                <script>
                    window.parent.ErrorMSJ("Actualizacion de mi cuenta", "0");
                </script>
                <?
		        die;
	        }
       
            $pwd = Hash::getHash($frmU["txtClaveNuevo1"]);
            $id = Aplicacion::Decrypter($_SESSION["User"]["id"]);
            
            $oConexion = Conexion::nuevo();
            $oConexion->Abrir_Trans();
            $oConexion->setQuery("UPDATE usuarios SET Clave = '".$pwd."', es_Provisoria = 1 WHERE IdUsuario = ".$id);
            $Tabla = $oConexion->EjecutarQuery();
            $oConexion->Cerrar_Trans();
            
        ?>
                <script>
                    window.parent.ErrorMSJ("Actualizacion de mi cuenta", "EXITO");
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            die($e->getMessage());
            ?>
                <script>
                    window.parent.ErrorMSJ("Actualizacion de mi cuenta", "0");
                </script>
            <?
        }  
        ob_flush();
    }

	function limpiar_tags($tags) { 
        $tags = strip_tags($tags); 
		$tags = stripslashes($tags); 
		$tags = htmlentities($tags); 
		return $tags; 
	} 
?>