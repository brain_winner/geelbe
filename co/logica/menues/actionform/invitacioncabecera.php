<html>
<head>
<?
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("referenciados"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("contacto", "dm"));
        $arrayHTML = Aplicacion::getIncludes("onlyget", "htmlRecomendacion");
        Includes::Scripts();
?>
</head>
<body>
<?
    if($_POST) {
        ob_start();

        try {
            $arrDatos = dmContacto::ObtenerApenom(Aplicacion::Decrypter($_SESSION["User"]["id"]));
            $collAhijados = array();
            $email = $_POST["frmEmailC"];
            if($email != "" && $email != $arrDatos["Email"]) {
                $objAhijados = new MySQL_Ahijados();
                $objAhijados->setEmail($email);
                $objAhijados->setIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
                array_push($collAhijados, $objAhijados);
            }

	        $objUsuario = dmUsuario::getByIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
		    dmReferenciados::Invitacion($collAhijados, $objUsuario, $arrDatos);
            ?>
                <script>msjInfo("Se han enviado las invitaciones","Referenciar", "<?=Aplicacion::getRootUrl()?>front/referenciados/");</script>
            <?
        }
        catch(MySQLException $e) {
            die($e->getMessage());
        }
        ob_flush();
    }
?>
</body>
</html>