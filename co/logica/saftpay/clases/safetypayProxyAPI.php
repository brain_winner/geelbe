<?php

require_once(dirname(__FILE__).'/safetypayNusoap.php');
require_once(dirname(__FILE__).'/safetypaySha256.inc.php');

class SafetyPayProxy
{
    /* Contructor */
    function SafetyPayProxy()
    {
        // Web Services Credentials: API Key
        $this->ApiKey			= 'acff81a5b0684c77a5a183470a207c9e';	// Using M_TEST Account. Get or Generate your own keys in the MMS, option: Profile > Credentials
        // Web Services Credentials: Signature Key
        $this->SignatureKey		= 'da2ce45865ab4057b11b5f474e7625f7';	// Using M_TEST Account. Get or Generate your own keys in the MMS, option: Profile > Credentials
		$this->Test				= 0;									// Set 1: For Test; 0: For Production
		$this->CurrCodeDefault	= 'MXN';								// Set the Currency Code of Virtual Store (USD, PEn, MXN, EUR, etc.)

		if ($this->Test)			// Sandbox Links v2.2
    	{
			$this->wsdlURL		= 'https://secure.saftpay.com/Prod_QAS/WebServices/v2.2/Merchants/MerchantWS.asmx?WSDL';			
		}
		else						// Production Links v2.2
		{
			$this->wsdlURL		= 'https://secure.saftpay.com/prod/WebServices/v2.2/Merchants/MerchantWS.asmx?WSDL';			
		}

        // Request Headers for WS v2.2
        $this->headers 			= '<RequesterCredentials xmlns="SaftpayMerchant v.2.2"><ApiKey>'.$this->ApiKey.'</ApiKey></RequesterCredentials>';

		// Request Date Time
        $this->RequestDateTime	= substr((string)date(DATE_ATOM, mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"))), 0 , 19);

        // Expiration Time for Transactions
        $this->ExpirationTime	= 5;		// minutes
    }

	function LetKeys($sApiKey = '', $sSignatureKey = '')
	{
		if ($sApiKey!='')
			 $this->ApiKey = $sApiKey;

		if ($sSignatureKey!='')
			 $this->SignatureKey = $sSignatureKey;

		$this->headers = '<RequesterCredentials xmlns="SaftpayMerchant v.2.2"><ApiKey>'.$this->ApiKey.'</ApiKey></RequesterCredentials>';

		return true;
	}

	/* Get Proxy Method */
	function GetProxy()
	{
		$soap = new nusoap_client($this->wsdlURL, 'wsdl');
		$soap->setHeaders($this->headers);

		$err = $soap->getError();
		if ($err)
			echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';

		return $soap;
	}

	/* Communication Test Method */
	function CommunicationTest()
	{
		$param = array('CommunicationTestRequest' => array(
							'RequestDateTime' => $this->RequestDateTime,
							'Signature' => safetypay_sha256($this->RequestDateTime.$this->SignatureKey)));

		$soap = $this->GetProxy();
		$Result = $soap->call('CommunicationTest', $param, '', '', false, true);

		if($error = $soap->getError()){ return false; }

		if  ($Result['CommunicationTestResult']['ErrorManager']['ErrorNumber'] == '0')
			$response = 'Communication Successful';
		else
			$response = 'Error: ' . $Result['CommunicationTestResult']['ErrorManager']['ErrorNumber'] . ' - ' . $Result['CommunicationTestResult']['ErrorManager']['Description'];

		return $response;
	}

	/* Calculation Quote Method */
	function CalculationQuote($CurrencyCode,$Amount,$ToCurrencyCode)
	{
		$param = array('FxCalculationQuoteRequest' => array(
							'RequestDateTime' => $this->RequestDateTime,
							'CurrencyCode' => $CurrencyCode,
							'Amount' => $Amount,
							'ToCurrencyCode' => $ToCurrencyCode,
							'Signature' => safetypay_sha256($this->RequestDateTime.$CurrencyCode.$Amount.$ToCurrencyCode.$this->SignatureKey)));

		$soap = $this->GetProxy();
		$Result = $soap->call('FxCalculationQuote', $param, '', '', false, true);

		if($error = $soap->getError()){ die($error);}

		return $Result['FxCalculationQuoteResult'];
	}

	/* Create Express Token Method */
	function CreateExpressToken($CurrencyCode,$Amount,$MerchantReferenceNo,$Language,$TrackingCode,$TransactionOkURL,$TransactionErrorURL)
	{
		$param = array('CreateExpressTokenRequest' => array(
							'RequestDateTime' => $this->RequestDateTime,
							'ExpressInfo' => array(
									'CurrencyCode' => $CurrencyCode,
									'Amount' => $Amount,
									'MerchantReferenceNo' => $MerchantReferenceNo,
									'Language' => $Language,
									'TrackingCode' => $TrackingCode,
									'ExpirationTime' => $this->ExpirationTime,
									'TransactionOkURL' => $TransactionOkURL,
									'TransactionErrorURL' => $TransactionErrorURL),
							'Signature' => safetypay_sha256($this->RequestDateTime.$CurrencyCode.$Amount.$MerchantReferenceNo.$Language.$TrackingCode.$this->ExpirationTime.$TransactionOkURL.$TransactionErrorURL.$this->SignatureKey)));

		$soap = $this->GetProxy();

		$Result = $soap->call('CreateExpressToken', $param, '', '', false, true);

		if($error = $soap->getError()){ die($error);}
		return $Result['CreateExpressTokenResult'];
	}

	/* Create Transaction Method */
	function CreateTransaction($CurrencyCode,$Amount,$ToCurrencyCode,$MerchantReferenceNo,$Language,$FxCalculationQuoteReferenceNo,$TrackingCode,$BankID,$TransactionOkURL,$TransactionErrorURL)
	{
		$param = array('CreateTransactionRequest' => array(
							'RequestDateTime' => $this->RequestDateTime,
							'CurrencyCode' => $CurrencyCode,
							'Amount' => $Amount,
							'ToCurrencyCode' => $ToCurrencyCode,
							'MerchantReferenceNo' => $MerchantReferenceNo,
							'Language' => $Language,
							'FxCalculationQuoteReferenceNo' => $FxCalculationQuoteReferenceNo,
							'TrackingCode' => $TrackingCode,
							'BankID' => $BankID,
							'TransactionOkURL' => $TransactionOkURL,
							'TransactionErrorURL' => $TransactionErrorURL,
							'Signature' => safetypay_sha256($this->RequestDateTime.$CurrencyCode.$Amount.$ToCurrencyCode.$MerchantReferenceNo.$Language.$FxCalculationQuoteReferenceNo.$TrackingCode.$BankID.$TransactionOkURL.$TransactionErrorURL.$this->SignatureKey)));

		$soap = $this->GetProxy();
		$Result = $soap->call('CreateTransaction', $param, '', '', false, true);
		
		if($error = $soap->getError()){ die($error);}

		return $Result['CreateTransactionResult'];
	}


	/* Get Banks Method */
	function GetBanks($CurrencyCode)
	{
		$param = array('GetBanksRequest' => array(
							'RequestDateTime' => $this->RequestDateTime,
							'CurrencyCode' => $CurrencyCode,
							'Signature' => safetypay_sha256($this->RequestDateTime.$this->SignatureKey)));

		$soap = $this->GetProxy();
		$Result = $soap->call('GetBanks', $param, '', '', false, true);

		if($error = $soap->getError()){ die($error);}

		return $Result['GetBanksResult'];
	}


	/* Get Currencies Method */
	function GetCurrencies($Language, $PreferredToCurrencyCode, $ShopperIP = '')
	{
		$param = array('GetCurrenciesRequest' => array(
							'RequestDateTime' => $this->RequestDateTime,
							'Language' => $Language,
							'PreferredToCurrencyCode' => $PreferredToCurrencyCode,
							'ShopperIP' => $ShopperIP,
							'Signature' => safetypay_sha256($this->RequestDateTime.$this->SignatureKey)));

		$soap = $this->GetProxy();
		$Result = $soap->call('GetCurrencies', $param, '', '', false, true);

		if($error = $soap->getError()){ die($error);}

		return $Result['GetCurrenciesResult'];
	}

	/* Get New Paid Orders Method */
	function GetNewPaidOrders()
	{
		$param = array('GetNewPaidOrdersRequest' => array(
							'RequestDateTime' => $this->RequestDateTime,
							'Signature' => safetypay_sha256($this->RequestDateTime.$this->SignatureKey)));

		$soap = $this->GetProxy();
		$Result = $soap->call('GetNewPaidOrders', $param, '', '', false, true);

		if($error = $soap->getError()){ die($error);}

		return $Result['GetNewPaidOrdersResult'];
	}

	/* Confirm New Paid Orders Method */
	function ConfirmNewPaidOrders($Items)
	{
		$ItemsPlain = '';
		foreach ($Items['Items'] as $key => $value) {
			$ItemsPlain .= $value['ReferenceNo'].$value['MerchantOrderNo'].$value['IssueCode'];
		}
		$param = array('ConfirmNewPaidOrdersRequest' => array(
							'RequestDateTime' => $this->RequestDateTime,
							'ListOfNewPaidOrders' => $Items,
							'Signature' => safetypay_sha256($this->RequestDateTime.$ItemsPlain.$this->SignatureKey)));

		$soap = $this->GetProxy();
		$Result = $soap->call('ConfirmNewPaidOrders', $param, '', '', false, true);
		if($error = $soap->getError()){ die($error);}
		
		return $Result['ConfirmNewPaidOrdersResult'];
	}

	/* Confirm Shipped Orders Method */
	function ConfirmShippedOrders($ShippingDetail)
	{
		$ItemsPlain = '';
		foreach ($ShippingDetail as $value) {	$ItemsPlain .= $value;	}
		$param = array('ConfirmShippedOrderRequest' => array(
							'RequestDateTime' => $this->RequestDateTime,
							'ShippingDetail' => $ShippingDetail,
							'Signature' => safetypay_sha256($this->RequestDateTime.$ItemsPlain.$this->SignatureKey)));

		$soap = $this->GetProxy();
		$Result = $soap->call('ConfirmShippedOrder', $param, '', '', false, true);
		if($error = $soap->getError()){ die($error);}

		return $Result['ConfirmShippedOrderResult'];
	}
}
?>