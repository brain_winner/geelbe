<?php
    ob_start();
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
	require_once 'Zend/Cache.php'; 
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito"));
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1]);
		exit;
    }
	try
    {
        $Estado = 0;
        if(!isset($_SESSION["User"]["DineroMail"]))
            throw new ACCESOException("Error en session dineromails","705_MSJ", "705_TITLE");
        $NroTransaccion = Aplicacion::Decrypter($_SESSION["User"]["DineroMail"]["NroItem"]);
        $Vidriera = Aplicacion::Decrypter($_SESSION["User"]["DineroMail"]["Vidriera"]);
    }
    catch(ACCESOException $e)
    {
    ?>
        <script>window.open('../accesodenegado/accesodenegado.php?errNo=<?=Aplicacion::Encrypter($e->getCodigo())?>&r=<?=Aplicacion::Encrypter($e->getTitulo())?>','_self');</script>
    <?
        exit;
    }
    try
    {

    	try{
    		$actualizador = actualizarPedidos::nuevo($NroTransaccion, 1);
			$actualizador->Actualizar();
            Conexion::nuevo()->Cerrar_Trans();
    	}
    	//lo paso a sistema pago
    	catch(Exception $e){
            	Conexion::nuevo()->RollBack();
            	throw $e;
    	}

    $useStub = Aplicacion::getParametros("environment_configuration", "use_payment_stub");
    	
    if($useStub=="true") {
    	$Estado = 1;
    } else {
        //consulta a dineromail
        $Cuenta = Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "e_comercio"))."2";
        $Email = Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "email_cuenta"));
        $Pin = Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "pin_consultas"));
        $objDMReporte = new ReportePedidosDM($Email,$Cuenta, $Pin);
        $objDMReporte->sendConsulta();
        $arrPago = $objDMReporte->getPayByIdTRX($NroTransaccion);
        $Estado = 1;
   	}
        //si llegue hasta aca es porque el pdido esta acpetado por dineromail por ende cambio esl estado yh mando el mail

        try{
    		$actualizador = actualizarPedidos::nuevo($NroTransaccion, 2);
			$actualizador->Actualizar();
            Conexion::nuevo()->Cerrar_Trans();
            
            //Invalido la cache del credito de usuario
            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
		    $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
		    $cache->remove('cache_credito_usuario_'.Aplicacion::Decrypter($_SESSION["User"]["id"]));
            
		    // Invalido la cache del padrino
		    $objUsuario = dmUsuario::getByIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
    	    $nombrePadrino = $objUsuario->getPadrino();
    	    if($nombrePadrino && $nombrePadrino != "Huerfano") {
	    	  $idPadrino = dmUsuario::getIdByNombreUsuario($nombrePadrino);
		      $cache->remove('cache_credito_usuario_'.$idPadrino);
    	    }
    	}
    	//lo paso a pago aceptado
    	catch(Exception $e){
            	Conexion::nuevo()->RollBack();
            	throw $e;
    	}
	}
    catch(exception $e)
    {
        $Estado =0;
    }
    unset($_SESSION["User"]["DineroMail"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="../geelbe.css" rel="stylesheet" type="text/css" />

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
    <script>
        <?
            if($Estado == 1)
            {
                ?>oCortina.showError(Array(arrErrores["COMPRA"]["DINEROMAIL_ACEPTADO"]),arrErrores["COMPRA"]["TITLE"], "<?=Aplicacion::getRootUrl()?>front/micuenta", 1);<?
            }
            else
            {
                ?>oCortina.showError(Array(arrErrores["COMPRA"]["DINEROMAIL_PROCESANDO"]),arrErrores["COMPRA"]["TITLE"], "<?=Aplicacion::getRootUrl()?>front/micuenta", 1);<?
            }
        ?>
    </script>
</body>
</html>