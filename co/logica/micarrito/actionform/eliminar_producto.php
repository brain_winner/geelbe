<html>
<body>
<?php
    if($_GET)
    {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito"));
        Includes::Scripts();  
        extract($_GET);
        try
        {   
            $objCarrito = Aplicacion::getCarrito(); 
            $objCarrito->removeArticulo($IdCodigoProdInterno);
            
            Aplicacion::setCarrito($objCarrito);
            if(count($objCarrito->getArticulos())==0)
            {
                    Aplicacion::VaciarCarrito();
                    ?>
                    <script>
                        Redirect("<?=Aplicacion::getRootUrl()?>front/micarrito/carrito.php","_self");
                    </script>
                    <?
            }
            else
            {
                ?>
                    <script>
                        Redirect("<?="http://".$_SERVER['HTTP_HOST']."/".$confRoot[1]."/"?>front/micarrito/index.php", "_self");
                    </script>
                <?
            }            
        }
        catch(MySQLException $e)
        {
            ?>
            <script>
                window.parent.ErrorMSJ("Error de micarrito", "<?=$e->getNumeroSQLERROR()?>");
            </script>
            <?
        }
        ob_flush();
    }  
?>
</html>
</body>