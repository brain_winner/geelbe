<?php
    if($_POST)
    {
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito"));
    	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
        ob_start();
        extract($_POST);
		try {
				$isPageOk = true;
				$pageException = "Exception";
//				if (!isset($_POST["txtNombre"]) || empty($_POST["txtNombre"])) { // Nombre vacio
//					$isPageOk = false;
//					$pageException = "txtNombre not setted";
//				} 
//				else if (!isset($_POST["txtApellido"]) || empty($_POST["txtApellido"])) { // Apellido vacio
//					$isPageOk = false;
//					$pageException = "txtApellido not setted";
//				}
//				else if (!isset($_POST["txtDomicilio"]) || empty($_POST["txtDomicilio"])) { // Domicilio vacio
//					$isPageOk = false;
//					$pageException = "txtDomicilio not setted";
//				}
//				else if (!isset($_POST["txtNumero"]) || empty($_POST["txtNumero"])) { // Numero vacio
//					$isPageOk = false;
//					$pageException = "txtNumero not setted";
//				}
//				else if (!isset($_POST["txtPoblacion"]) || empty($_POST["txtPoblacion"])) { // Ciudad vacio
//					$isPageOk = false;
//					$pageException = "txtPoblacion not setted";
//				}
//				else if (!isset($_POST["cbxProvincia"]) || empty($_POST["cbxProvincia"])) { // Provincia vacio
//					$isPageOk = false;
//					$pageException = "cbxProvincia not setted";
//				}
//				
//				else if (!isset($_POST["txtCP"]) || empty($_POST["txtCP"])) { // CP vacio
//					$isPageOk = false;
//					$pageException = "txtCP not setted";
//				}
//				if (!ValidatorUtils::validateNumber($_POST["txtCP"])) { // no es numero
//					$isPageOk = false;
//					$pageException = "IdProducto is not a number";
//				} 
				if (!$isPageOk) {
					throw new Exception($pageException);
				}
			}
			catch(Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
				throw $e;
				exit;
			}  
        try
        {   
         	if(strlen($frmDir["txtDomicilio"]) == 0 || strlen($frmDir["txtNumero"]) == 0 || strlen($frmDir["cbxProvincia"]) == 0 || strlen($frmDir["txtPoblacion"]) == 0 || strlen($frmDir["txtCP"]) == 0) {
        		die;
        	}
        
           $objCarrito = Aplicacion::getCarrito();
            $sinStock = array();
       
            foreach($frmA["cbxCantidad"] as $IdCodigoProdInterno => $cantidad)
            {
                ?>
                    <script>
                        window.parent.document.getElementById("sinStock<?=$IdCodigoProdInterno?>").style.display="none";
                    </script>
                <?
                $objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
                if($objPP->getStock()<$cantidad)  
                    $sinStock[]=array("IdCodigoProdInterno" => $IdCodigoProdInterno, "CantidadDisponible" => $objPP->getStock());
                else
                    $objCarrito->setArticulo($objPP->getIdProducto(), $IdCodigoProdInterno, $cantidad, 1,$objPP->getStock());
            }

            if(isset($frmBorrar))
            {
                foreach($frmBorrar as $chk)
                {                    
                    $objCarrito->removeArticulo($chk);
                }
            }
            if(isset($chkDirPredeterminada))
            {
                $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
                $objDatos = $objUsuario->getDatos();
                $objDatos->setNombre(ucwords(strtolower($frmDU["txtNombre"])));
                $objDatos->setApellido(ucwords(strtolower($frmDU["txtApellido"])));
                $objDatos->setcelucodigo($frmDU["txtCelularCodigo"]);
                $objDatos->setceluempresa($frmDU["cbxCeluEmpresa"]);
                $objDatos->setcelunumero($frmDU["txtCelularNumero"]);
                $objDatos->setCodTelefono($frmDU["txtCodTelefono"]);
                $objDatos->setTelefono($frmDU["txtTelefono"]);
                $objUsuario->setDatos($objDatos);
                $objDireccion = $objUsuario->getDirecciones(0);
                $objDireccion->setCP($frmDir["txtCP"]);
                $objDireccion->setDomicilio($frmDir["txtDomicilio"]);
                $objDireccion->setEscalera($frmDir["txtEscalera"]);
                $objDireccion->setIdPais(15);
                $objDireccion->setIdProvincia($frmDir["cbxProvincia"]);
                $objDireccion->setTipoDireccion(0);
                $objDireccion->setNumero($frmDir["txtNumero"]);
                $objDireccion->setPoblacion($frmDir["txtPoblacion"]);
                $objDireccion->setPiso($frmDir["txtPiso"]);
                $objDireccion->setPuerta($frmDir["txtPuerta"]);
                $objDireccion->setTipoCalle($frmDir["cbxTipoCalle"]);
                $objDireccion->setIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
                $objDireccion->esCompleto();
                $objUsuario->setDirecciones(0, $objDireccion);
                dmMicuenta::Actualizar($objUsuario);
            }
            
            $_SESSION["User"]["DireccionEnvio"] = array();
            $_SESSION["User"]["DireccionEnvio"] = $frmDU;
            $_SESSION["User"]["DireccionEnvio"] = array_merge($_SESSION["User"]["DireccionEnvio"], $frmDir);
                
            Aplicacion::setCarrito($objCarrito);
            if(count($objCarrito->getArticulos())>0)
            {
                if(count($sinStock)>0)
                {
                    foreach($sinStock as $ss)
                    {
                        ?>
                            <script>
                                window.parent.document.getElementById("sinStock<?=$ss["IdCodigoProdInterno"]?>").style.display="block";
                            </script>
                        <?
                    }
                }
                else
                {
                    ?>
                    <script>
                        window.parent.Redirect("<?=Aplicacion::getRootUrl()?>front/micarrito/carrito.php","_self");
                    </script>
                    <?
                }
            }
            else
            {
                Aplicacion::VaciarCarrito();
                ?>
                <script>
                    window.parent.Redirect("<?=Aplicacion::getRootUrl()?>front/vidriera","_self");
                </script>
                <?
            }               
        }
        catch(MySQLException $e)
        {
            ?>
            <script>
                window.parent.ErrorMSJ("Error de micarrito", "<?=$e->getNumeroSQLERROR()?>");
            </script>
            <?
        }
        ob_flush();
    }
    
?>
