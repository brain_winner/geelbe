<?php
    if($_POST)
    {    
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito"));
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("emails"));
		extract($_POST);

        try
        {    
        	if(isset($IdPedido) && is_numeric($IdPedido)) {
        		//Actualizar pedido
        		$edit = true;
        		$objPedido = dmPedidos::getPedidoById($IdPedido);
        		$Articulos = $objPedido->getProductos();
        		$IdCampania = $objPedido->getIdCampania();
        		$IdProvincia = $objPedido->getDireccion()->getIdProvincia();
			} else {
        		//Generar nuevo pedido
        		$edit = false; 
        	  
	            $objPedido = new MySQL_Pedidos();
	            $objCarrito = unserialize($_SESSION["User"]["Carrito"]);
	
	            $objPedido->setFecha(date("Y/m/d", time()));
	            $objPedido->setFechaEnvio(0);
	            $objPedido->setGastosEnvio($txtGastosEnvio);
	            $objPedido->setIdCampania($objCarrito->getIdCampania());
	            $objPedido->setIdEstadoPedidos(0);
	            $objPedido->setIdFormaEnvio(1);//Envio
	            $objPedido->setIdFormaPago($paymentMethod);
	            $objPedido->setIdPedido(0);
	            $objPedido->setIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
	            
	             $objDireccion = new MySQL_PedidosDirecciones();
	            $objDireccion->setCP($_SESSION["User"]["DireccionEnvio"]["txtCP"]);
	            $objDireccion->setDomicilio($_SESSION["User"]["DireccionEnvio"]["txtDomicilio"]);
	            $objDireccion->setEscalera($_SESSION["User"]["DireccionEnvio"]["txtEscalera"]);
	            $objDireccion->setIdPais(15);
	            $objDireccion->setIdProvincia($_SESSION["User"]["DireccionEnvio"]["cbxProvincia"]);
	            $objDireccion->setNumero($_SESSION["User"]["DireccionEnvio"]["txtNumero"]);
	            $objDireccion->setPoblacion($_SESSION["User"]["DireccionEnvio"]["txtPoblacion"]);
	            $objDireccion->setPiso($_SESSION["User"]["DireccionEnvio"]["txtPiso"]);
	            $objDireccion->setPuerta($_SESSION["User"]["DireccionEnvio"]["txtPuerta"]);
	            $objDireccion->setTipoCalle($_SESSION["User"]["DireccionEnvio"]["cbxTipoCalle"]);
	            $objDireccion->setHorarioEntrega($_SESSION["User"]["DireccionEnvio"]["cbxHorarioEntrega"]);
	            $objDireccion->setIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
	            $objDireccion->setNombre($_SESSION["User"]["DireccionEnvio"]["txtNombre"]);
	            $objDireccion->setApellido($_SESSION["User"]["DireccionEnvio"]["txtApellido"]);
	            $objDireccion->setNroDoc($_SESSION["User"]["DireccionEnvio"]["txtDNI"]);
	            $objDireccion->setTelefono($_SESSION["User"]["DireccionEnvio"]["txtTelefono"]);
	            $objDireccion->setCodTelefono($_SESSION["User"]["DireccionEnvio"]["txtCodTelefono"]);
	            $objDireccion->setceluempresa($_SESSION["User"]["DireccionEnvio"]["cbxCeluEmpresa"]);
	            $objDireccion->setcelucodigo($_SESSION["User"]["DireccionEnvio"]["txtCelularCodigo"]);
	            $objDireccion->setcelunumero($_SESSION["User"]["DireccionEnvio"]["txtCelularNumero"]);
	            $objDireccion->setIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
					$objDireccion->setColonia($_SESSION['User']['DireccionEnvio']['txtColonia']);
	            
	            $Articulos = $objCarrito->getArticulos();
	            $IdCampania = $objCarrito->getIdCampania();
	            
	             $IdProvincia = $_SESSION["User"]["DireccionEnvio"]["cbxProvincia"];
	        }
	        
	        $Total = 0;
            $Peso = 0;
            $Productos = array();
	        
	        
            foreach($Articulos as $IdCodigoProdInterno =>$Articulo)
            {
            	//Para reanudar un pedido debo transformar
            	//objeto en array
            	if(!is_array($Articulo)) {
            		$IdCodigoProdInterno = $Articulo->getIdCodigoProdInterno();
            		$Articulo = array('Cantidad' => $Articulo->getCantidad());
            	}

                $objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
                $objProducto = dmProductos::getById($objPP->getIdProducto());
                $Producto = new MySQL_PedidosProductos();
                $Producto->setCantidad($Articulo["Cantidad"]);
                $Producto->setIdCodigoProdInterno($IdCodigoProdInterno);
                $Producto->setIdPedido(0);
                $precio = ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento());
                $Producto->setPrecio($precio);
                $Total += $Producto->getPrecio()*$Producto->getCantidad();
                $objPedido->setProducto($Producto);
                $Productos[] = array($objProducto, $Producto, $precio);
                
                $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
                $Peso += $P * $Articulo["Cantidad"];
                
                $dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($IdCampania);
                $montoEnvioGratis = dmCampania::getMinMontoEnvioGratis($IdCampania);
                
                $free = 1;
                foreach($dtCampaniasPropiedades as $propiedad)
                {
                    if($propiedad["IdPropiedadCampamia"]=='6')
                    {
                       $free = 0;
                    }
                }

                if($montoEnvioGratis != 0 && $Total >= $montoEnvioGratis) {
                	$free = 0;
                }
                
            }

            if($free!=0)
            {
                $oConexion = Conexion::nuevo();
                $oConexion->Abrir_Trans();
                $oConexion->setQuery("
                	(
					SELECT p.Tarifa, p.TarifaKG, ps.Hasta
					FROM `dhl_precios` p
					LEFT JOIN `dhl_zonas_x_estados` zp ON p.IdZona = zp.IdZona
					LEFT JOIN `dhl_pesos` ps ON ps.IdPeso = p.IdPeso
					WHERE zp.IdEstado = ".mysql_real_escape_string($IdProvincia)." AND ps.Desde <= ".$Peso." AND ps.Hasta >= ".$Peso."
					)
					
					UNION
					
					(
					SELECT p.Tarifa, p.TarifaKG, ps.Hasta
					FROM `dhl_precios` p
					LEFT JOIN `dhl_zonas_x_estados` zp ON p.IdZona = zp.IdZona
					LEFT JOIN `dhl_pesos` ps ON ps.IdPeso = p.IdPeso
					WHERE zp.IdEstado = ".mysql_real_escape_string($IdProvincia)."
					ORDER BY p.IdPeso DESC LIMIT 1
					)
                ");

                $Tabla = $oConexion->DevolverQuery();

            	if(isset($Tabla[0])) {    
                	$datos = $Tabla[0];
                	$importe = $Tabla[0]['Tarifa'];
                	
                	//Si supera el m�ximo, contar kilos
                	$Extra = $Peso - $datos['Hasta'];
                	if($Extra > 0) {
                		$importe += $Tabla[0]['TarifaKG'] * $Extra;
                	}
                	
        	        $ValorEnvio = $importe;
        	        
        	    } else {
        	    
        	    	$ValorEnvio = 0;
        	    	
        	    }

            }
            else
            {
                $ValorEnvio = 0;
            }

            
            $Total = $Total + $objPedido->getGastosEnvio() - $txtBonoUtilizado;
            $objPedido->setTotal($Total);
            
            //Si se paga con safetypay, verificar antes de guardar
            if($paymentMethod == 3) {
				
				if(empty($_POST['saftpay']['curr']) || empty($_POST['saftpay']['bank'])) {
					echo 'Debe seleccionar un banco y una moneda. Por favor intente nuevamente.';
					die;
				} else {
					$_POST['saftpay']['bank'] = trim($_POST['saftpay']['bank']);
				}
									
        	}    

            if(!$edit) {		
	
	            $objPedido->setIdPedido(dmPedidos::save($objPedido,$txtBonoUtilizado, $objDireccion));
	            $hayStock = dmPedidos::hayStockReal($objPedido->getIdPedido());
	
	            if(!$hayStock["db"]){
	            	//ACA LE CIERRO EL PEDIDO
					$actualizador = actualizarPedidos::nuevo($objPedido->getIdPedido(), 10);
					$actualizador->Actualizar();
		            Conexion::nuevo()->Cerrar_Trans();
		            exit();
	            }elseif(!$hayStock["temporal"]){
	            	//ACA LE TIRO UN CARTEL DE ERROR Y QUE SI QUIERE QUE VUELVA A INTENTAR
	
	            	exit("El ultimo articulo en stock esta siendo abonado por otro usuario. Intente nuevamente mas tarde.");
	            }else{
	            	//ACA SIGO ADELANTE CON TODO
	
	
		            dmPedidos::setFechaEntradaSistemaPago($objPedido->getIdPedido());
		            $objCampania = dmCampania::getByIdCampania($objPedido->getIdCampania());
		            $NombreCampania = $objCampania->getNombre();
		            
		            $email = dmPedidos::getDatosMailByIdPedido($objPedido->getIdPedido());
		
	$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	$mesFn = $meses[date("n", strtotime($email["TiempoEntregaFn"])) - 1];
	$mesIn = $meses[date("n", strtotime($email["TiempoEntregaIn"])) - 1];
	
					//Enviar email
					$datos = array(
						'NumeroPedido' => $objPedido->getIdPedido(),
						"TiempoEntregaFn" => date("d", strtotime($email["TiempoEntregaFn"]))." de ".$mesFn,
						"TiempoEntregaIn" => date("d", strtotime($email["TiempoEntregaIn"]))." de ".$mesIn,
						'NombreVidriera' => $NombreCampania,
						'IdUsuario' => $objPedido->getIdUsuario(),
						'HashIdUsuario' => Aplicacion::Encrypter($objPedido->getIdUsuario())
					);
					
					emailPedidoAceptado(Aplicacion::Decrypter($_SESSION['User']['email']), $datos);
					
					
					Aplicacion::VaciarCarrito();
		            unset($_SESSION["User"]["DireccionEnvio"]);
		            
		        }
			} else {
				$objCampania = dmCampania::getByIdCampania($objPedido->getIdCampania());
				 $NombreCampania = $objCampania->getNombre();
				
				$oConexion = new Conexion();
				$oConexion->Abrir_Trans();

				$oConexion->setQuery("UPDATE pedidos SET IdFormaPago = ".$paymentMethod.", GastosEnvio = ".$objPedido->getGastosEnvio().", Total = ".$objPedido->getTotal()." WHERE IdPedido = ".mysql_real_escape_string($IdPedido));
				
				$oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
              	/*                  
	            //Registro bonificaci�n
	            $oConexion = new Conexion();
				$oConexion->Abrir_Trans();
                $oConexion->setQuery("DELETE FROM pedidos_bonificado WHERE idPedido = ".$objPedido->getIdPedido());
                $oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
				*/
			}

            $TRX_ID = $objPedido->getIdPedido();
			$_SESSION['lastPedidoId'] = $TRX_ID;

            if($paymentMethod == 2 || $paymentMethod == 9) { //Paypal
            
           		$objUsuario = new MySQL_micuenta();
		        $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
           ?>
           <html>
            <body>
            	<form action="<?=((Aplicacion::getParametros("paypal", "sandbox") != 1 || $paymentMethod == 9) ? Aplicacion::getParametros("paypal", "post") : Aplicacion::getParametros("paypal", "sandboxpost"))?>" method="post" id="paypal">

            		<input type="hidden" name="item_name_1" value="<?=$NombreCampania?>" />
					<input type="hidden" name="amount_1" value="<?=$Total;?>" />
					<input type="hidden" name="quantity_1" value="1" />
            		
                    <input type="hidden" name="shopping_url" value="<?=Aplicacion::getRootUrl().Aplicacion::getParametros("paypal", "exito")?>" />
					<input type="hidden" name="upload" value="1" />
					<input type="hidden" name="notify_url" value="<?=Aplicacion::getRootUrl().Aplicacion::getParametros("paypal", "notify")?>" />
					<input type="hidden" name="business" value="<?=Aplicacion::getParametros("paypal", "email_cuenta")?>" />
					<input type="hidden" name="receiver_email" value="<?=Aplicacion::getParametros("paypal", "email_cuenta")?>" />
					<input type="hidden" name="cmd" value="_cart" />
					<input type="hidden" name="charset" value="utf-8" />
					<input type="hidden" name="currency_code" value="MXN" />
					<input type="hidden" name="custom" value="<?=$TRX_ID;?>" />
					
				</form>
				 <script>
                   document.forms[0].submit();
                </script>
             </body>
        </html>
            
            <?php
            
            } elseif($paymentMethod == 8) { //Banamex
            
           		$objUsuario = new MySQL_micuenta();
		        $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
          
          		$SECURE_SECRET = Aplicacion::getParametros("banamex", "hash");
				$vpcURL = Aplicacion::getParametros("banamex", "url") . "?";
				
				$PaymentData = array(
					'vpc_Version' => 1,
					'vpc_Command' => 'pay',
					'vpc_AccessCode' => Aplicacion::getParametros("banamex", "access_code"),
					'vpc_MerchTxnRef' => $TRX_ID,
					'vpc_Merchant' => Aplicacion::getParametros("banamex", "cuenta"),
					'vpc_OrderInfo' => 'Pedido n. '.$TRX_ID,
					'vpc_Amount' => number_format($Total, 2, '', ''),
					'vpc_ReturnURL' => Aplicacion::getParametros("banamex", "return_url"),
					'vpc_Locale' => 'es',
					'AgainLink' => urlencode($HTTP_REFERER)
				);
								
				// Create the request to the Virtual Payment Client which is a URL encoded GET
				// request. Since we are looping through all the data we may as well sort it in
				// case we want to create a secure hash and add it to the VPC data if the
				// merchant secret has been provided.
				$md5HashData = $SECURE_SECRET;
				ksort($PaymentData);
				
				// set a parameter to show the first pair in the URL
				$appendAmp = 0;
				
				foreach($PaymentData as $key => $value) {
				
				    // create the md5 input and URL leaving out any fields that have no value
				    if (strlen($value) > 0) {
				        
				        // this ensures the first paramter of the URL is preceded by the '?' char
				        if ($appendAmp == 0) {
				            $vpcURL .= urlencode($key) . '=' . urlencode($value);
				            $appendAmp = 1;
				        } else {
				            $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
				        }
				        $md5HashData .= $value;
				    }
				}
				
				// Create the secure hash and append it to the Virtual Payment Client Data if
				// the merchant secret has been provided.
				if (strlen($SECURE_SECRET) > 0) {
				    $vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
				}
				
				// FINISH TRANSACTION - Redirect the customers using the Digital Order
				// ===================================================================

				header("Location: ".$vpcURL);
				die;
            
            } else if($paymentMethod == 3) { //SafetyPay
									
				$proxySTP = new SafetyPayProxy();
				$url = 'http://'.$_SERVER['HTTP_HOST'].'/mx/';
				
				if($_POST['saftpay']['refno'] == 0) $_POST['saftpay']['refno'] = null;
				
				//Enviar pedido
				$res = stp_CreateTransaction($proxySTP, 'MXN', $objPedido->getTotal(), $_POST['saftpay']['curr'], $TRX_ID, 'ES', $_POST['saftpay']['refno'], '', $_POST['saftpay']['bank'], $url.Aplicacion::getParametros("saftpay", "exito"), $url.Aplicacion::getParametros("saftpay", "fallo"));
				
				if(!is_array($res)) {
					echo 'Error al procesar el pago. Por favor intente nuevamente.';
					die;
				}
				
				/*Aca lleno las variables a mandar*/
				$IdDelPedido = $objPedido->getIdPedido();
				$nroDeLaReferencia = $res['ReferenceNo'] ;
				$idDeLaTransaccion = $res['TransactionID'] ;
				$nombreDelBanco = $res['SelectedBank']['BankCode'] ;
				$valorDelCurrency = $res['ToCurrency']['Code'] ;
				$montoDelPedido = $res['ToAmount'] ;
				$FechaDeExpiracion = $res['ExpirationTime'] ;
				$urlDelBanco = $res['SelectedBank']['NavigationURL'] ;
				$tipoDelBanco = $res['SelectedBank']['AccessType'] ;
				
				$usuario = Aplicacion::getParametros("conexion", "usuario");
    			$password = Aplicacion::Decrypter(Aplicacion::getParametros("conexion", "password"));
    			$server = Aplicacion::getParametros("conexion", "server");
    			$base = Aplicacion::getParametros("conexion","base");
				
				//Registrar 
            	mysql_connect($server, $usuario, $password);
            	mysql_select_db($base);
				//mysql_query('INSERT INTO `pedidos_saftpay` (IdPedido, refNo, transactionId, bank, currency, amount, expiration, url, bankType) VALUES ('.$objPedido->getIdPedido().', '.$res['ReferenceNo'].", ".$res['TransactionID'].", ".$res['SelectedBank']['BankCode'].", '".$res['ToCurrency']['Code']."', ".$res['ToAmount'].", '".$res['ExpirationTime']."', '".$res['SelectedBank']['NavigationURL']."', ".$res['SelectedBank']['AccessType'].")");
				
				mysql_query('INSERT INTO `pedidos_saftpay` (IdPedido, refNo, transactionId, bank, currency, amount, expiration, url, bankType) VALUES ('. $IdDelPedido .', "'. $nroDeLaReferencia .'", "'. $idDeLaTransaccion .'", "'. $nombreDelBanco .'", "'. $valorDelCurrency  .'", "'. $montoDelPedido .'", "'. $FechaDeExpiracion .'", "'. $urlDelBanco .'", "'. $tipoDelBanco .'") ');
				
                if($res['SelectedBank']['AccessType'] == 2)
                	header("Location: ".$res['SelectedBank']['NavigationURL']);
                else
                	header("Location: ../../../front/micarrito/recibo.php?id=".$objPedido->getIdPedido());
                	
            	die; 
            	
            /* INICIO pagosonline */
            	            	
            } elseif($paymentMethod == 10) {
            
            	$key = '1111111111111111';
            	$usr_id = '2';
            	$clave = md5($key.'~'.$usr_id.'~'.$TRX_ID.'~'.number_format($objPedido->getTotal(), 2, '.', '').'~COP');

            ?>
            <html>
            <body>
	            <form method="post" action="https://gateway2.pagosonline.net/apps/gateway/index.html">
	            	<input name="usuarioId" type="hidden" value="<?=$usr_id;?>">
	            	<input name="descripcion" type="hidden" value="Geelbe Colombia">
	            	<input name="refVenta" type="hidden" value="<?=$TRX_ID;?>">
	            	<input name="valor" type="hidden" value="<?=number_format($objPedido->getTotal(), 2, '.', '')?>">
	            	<input name="moneda" type="hidden" value="COP">
	            	<input name="firma" type="hidden" value="<?=$clave;?>">
	            	
	            	<input name="emailComprador" type="hidden" value="<?=Aplicacion::Decrypter($_SESSION['User']['email']);?>">
	            	<input name="url_respuesta" type="hidden" value="http://www.dev.geelbe.com/mx/front/micarrito/recibo_pagosonline.php">
	            	<input name="url_confirmacion" type="hidden" value="http://www.dev.geelbe.com/mx/logica/framework/pagosonline.php">
	
	            	<input name="prueba" type="hidden" value="1">
	            </form>
	            <script>
	                    document.forms[0].submit();
	                </script>
            
            <?php
            
            /* FIN pagosonline */
            
            } else { //DineroMail
            
            	$_SESSION["User"]["DineroMail"]=array();
	            $_SESSION["User"]["DineroMail"]["NroItem"] =Aplicacion::Encrypter($objPedido->getIdPedido());
    	        $_SESSION["User"]["DineroMail"]["Vidriera"] =Aplicacion::Encrypter($objCampania->getNombre());

           ?>
        <html>
            <body>
                <form id="frmDineroMail" method="post" name="frmDineroMail" action="<?=Aplicacion::getParametros("dineromail", "post")?>">
                    <input type="hidden" id="E_Comercio" name="E_Comercio" value="<?=substr(Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "e_comercio")), 1, 6)?>" />
                    <input type="hidden" id="PrecioItem" name="PrecioItem" value="<?=$objPedido->getTotal()?>" />
                    <input type="hidden" id="NroItem" name="NroItem" value="<?=$TRX_ID?>" />
                    <input type="hidden" id="DireccionExito" name="DireccionExito" value="<?=Aplicacion::getRootUrl().Aplicacion::getParametros("dineromail", "exito")?>" />
                    <input type="hidden" id="DireccionFracaso" name="DireccionFracaso" value="<?=Aplicacion::getRootUrl().Aplicacion::getParametros("dineromail", "fallo")?>" />
                    <input type="hidden" name="TipoMoneda" id="TipoMoneda" value="1">
                    <input type="hidden" name="DireccionEnvio" id="DireccionEnvio" value="0">
                    <input type="hidden" name="Mensaje" id="Mensaje" value="0">
                    <input type="hidden" name="image_url" id="image_url" value="1">
                    <input type="hidden" name="MediosPago" id="MediosPago" value='4,5,6,17,19,20,21,22,7'>
 	<!--		    <input type="hidden" name="MediosPago" id="MediosPago" value="4,5,6,17,19,20,21,22,7"> -->
                    <!--'4,5,6,14,15,16,17,18'--><!--4,5,6,14,15,16,17,2,7,13-->
                    <input type="hidden" name="trx_id" id="trx_id" value="<?=$TRX_ID?>">
                    <input type="hidden" name="NombreItem" id="NombreItem" value="Geelbe - <?=$NombreCampania?>">

                    <input type="hidden" name="usr_mediopago" id="usr_mediopago" value="<?=$objPedido->getIdFormaPago()?>" />
                    <input type="hidden" name="usr_nombre" id="usr_nombre" value="<?=Aplicacion::Decrypter($_SESSION["User"]["nombre"])?>" />
                    <input type="hidden" name="usr_apellido" id="usr_apellido" value="<?=Aplicacion::Decrypter($_SESSION["User"]["apellido"])?>" />
                    <input type="hidden" name="usr_tel_numero" id="usr_tel_numero" value="<?=Aplicacion::Decrypter($_SESSION["User"]["telefono"])?>" />
                    <input type="hidden" name="usr_email" id="usr_email" value="<?=Aplicacion::Decrypter($_SESSION["User"]["email"])?>" />
                </form>
                <script>
                    document.forms[0].submit();
                </script>
 
        <?php } ?>
        
       		</body>
        </html>
        
        <?php        
            
        }
        catch(MySQLException $e)
        {
            throw $e;
        }
	}
?>
