<?php
	ini_set("memory_limit","80M");
	
	define("GEELBE_CASH", 5);
	
    if($_POST)
    {    
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/datamappers/dmdescuentos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/descuentos/clases/clsdescuentos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/dhl/datamappers/dmdhl.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("emails"));
    	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/ssl/carrito/logic/CartUtils.php");
    	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_service.php";
    	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/mailer/clsMailer.php";
		extract($_POST);

        try
        {    
        	
            if ($paymentMethod == GEELBE_CASH) {
            	$userEmail = $_REQUEST["userEmail"];
            	$userPassword = $_REQUEST["userPassword"];
            	
            	// Valid Email y Password del usuario
            	$invalidEmailOrPassword = false;
            	
                $oConexion = Conexion::nuevo();
                $oConexion->setQuery("SELECT Clave FROM usuarios WHERE NombreUsuario =\"". mysql_real_escape_string($userEmail) ."\"" );
                $claveUsuario = $oConexion->DevolverQuery();
                
                if (count($claveUsuario) != 1) {
                	$invalidEmailOrPassword = true;
                }
                
                if (Hash::compareHash(Hash::getHash($userPassword), $claveUsuario[0]["Clave"]) != 1) {
                	$invalidEmailOrPassword = true;
                }
                
                if ($invalidEmailOrPassword) {
                	if(isset($IdPedido) && is_numeric($IdPedido)) {
	                	header("Location: /".$confRoot[1]."/ssl/carrito/step_1.php?IdPedido=".$IdPedido."&error_type=invalid_email_or_password");
                	} else {
	                	header("Location: /".$confRoot[1]."/ssl/carrito/step_1.php?error_type=invalid_email_or_password");
                	}
            		exit;
                }
            }
        	
        	if(isset($IdPedido) && is_numeric($IdPedido)) {
        	    // Verifico stock ..
				$verificoCorrectamenteStock = CartUtils::verifyStockReanudarPago($IdPedido);
				if(!$verificoCorrectamenteStock) {
			       	$_SESSION["ProductosSinStock"] = $productosSinStock;
			       	$Pedido = dmPedidos::getPedidoById($IdPedido);
			        header('Location: /'.$confRoot[1].'/ssl/carrito/no_stock_reanudar_pago.php?IdCampania='.$Pedido->getIdCampania());
					exit;
				}
				
        		//Actualizar pedido
        		$edit = true;
        		$objPedido = dmPedidos::getPedidoById($IdPedido);
        		$Articulos = $objPedido->getProductos();
        		$IdCampania = $objPedido->getIdCampania();
        		$IdProvincia = $objPedido->getDireccion()->getIdProvincia();
        		$objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        		$objDireccion = $objUsuario->getDirecciones(0);
			} else {
		        // Verifico stock ..
				$productosSinStock = CartUtils::verifyStock();
				if(is_array($productosSinStock) && count($productosSinStock) > 0) {
			       	$_SESSION["ProductosSinStock"] = $productosSinStock;
		        	header('Location: /'.$confRoot[1].'/ssl/carrito/no_stock.php');
					exit;
				}
				
        		//Generar nuevo pedido
        		$edit = false; 
        	  
	            $objPedido = new MySQL_Pedidos();
	            $objCarrito = unserialize($_SESSION["User"]["Carrito"]);
	
	            $objPedido->setFecha(date("Y-m-d H:i:s", time()));
	            $objPedido->setFechaEnvio(0);
	            $objPedido->setIdCampania($objCarrito->getIdCampania());
	            $objPedido->setIdEstadoPedidos(0);
	            $objPedido->setIdFormaEnvio(1);//Envio
	            $objPedido->setIdFormaPago($paymentMethod);
	            $objPedido->setIdPedido(0);
	            $objPedido->setIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
	            $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
        		$direccionUsuario = $objUsuario->getDirecciones(0);
	            
        		$objDireccion = new MySQL_PedidosDirecciones();
	            $objDireccion->setCP($direccionUsuario->getCP());
	            $objDireccion->setDomicilio($direccionUsuario->getDomicilio());
	            $objDireccion->setEscalera($direccionUsuario->getEscalera());
	            $objDireccion->setIdPais(1);
	            $objDireccion->setIdProvincia($direccionUsuario->getIdProvincia());
	            $objDireccion->setIdCiudad($direccionUsuario->getIdCiudad());
	            $objDireccion->setNumero($direccionUsuario->getNumero());
	            $objDireccion->setPoblacion($direccionUsuario->getPoblacion());
	            $objDireccion->setPiso($direccionUsuario->getPiso());
	            $objDireccion->setPuerta($direccionUsuario->getPuerta());
	            $objDireccion->setTipoCalle($direccionUsuario->getTipoCalle());
	            $objDireccion->setHorarioEntrega("0");
	            $objDireccion->setIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
	            $objDireccion->setNombre($objUsuario->getDatos()->getNombre());
	            $objDireccion->setApellido($objUsuario->getDatos()->getApellido());
	            $objDireccion->setNroDoc($objUsuario->getDatos()->getNroDoc());
	            $objDireccion->setTelefono($objUsuario->getDatos()->getTelefono());
	            $objDireccion->setCodTelefono($objUsuario->getDatos()->getCodTelefono());
	            $objDireccion->setceluempresa($objUsuario->getDatos()->getceluempresa());
	            $objDireccion->setcelucodigo($objUsuario->getDatos()->getcelucodigo());
	            $objDireccion->setcelunumero($objUsuario->getDatos()->getcelunumero());
	            $objDireccion->setIdUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));

	            $Articulos = $objCarrito->getArticulos();
	            $IdCampania = $objCarrito->getIdCampania();
                $IdProvincia = $direccionUsuario->getIdProvincia();
	        }
	        
            if(isset($_SESSION["CREDITO_APLICADO_CARRITO"]) && $_SESSION["CREDITO_APLICADO_CARRITO"] > 0) {
	    		// Invalido la cache del credito de usuario
	            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
			    $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
			    $cache->remove('cache_credito_usuario_'.$objPedido->getIdUsuario());
            }
	        
	        $Total = 0;
            $Peso = 0;
            $Productos = array();
	        
	        $ultimosProductos = array();
			$CostosCompra = 0;
			$PreciosVenta = 0;
            foreach($Articulos as $IdCodigoProdInterno =>$Articulo)
            {
            	//Para reanudar un pedido debo transformar
            	//objeto en array
            	if(!is_array($Articulo)) {
            		$IdCodigoProdInterno = $Articulo->getIdCodigoProdInterno();
            		$Articulo = array('Cantidad' => $Articulo->getCantidad());
            	}

                $objPP = dmProductos::getByIdCodigoProdInterno($IdCodigoProdInterno);
                $objProducto = dmProductos::getById($objPP->getIdProducto());
                $Producto = new MySQL_PedidosProductos();
                $Producto->setCantidad($Articulo["Cantidad"]);
                $Producto->setIdCodigoProdInterno($IdCodigoProdInterno);
                $Producto->setIdPedido(0);
                $precio = ObtenerPrecioProductos($objProducto->getPVenta(), $objPP->getIncremento());
                $Producto->setPrecio($precio);
                $Total += $Producto->getPrecio()*$Producto->getCantidad();
                $objPedido->setProducto($Producto);
                $Productos[] = array($objProducto, $Producto, $precio);
                
                if($objPP->getStock() == 1) {
                	$ultimosProductos[] = $objProducto->getReferencia();
                }
                
                $P = ($objProducto->getPeso() >= $objProducto->getPesoVolumetrico())?$objProducto->getPeso():$objProducto->getPesoVolumetrico();
                $Peso += $P * $Articulo["Cantidad"];
				
				$CostosCompra += $objProducto->getPCompra()* $Articulo["Cantidad"];
				$PreciosVenta += $objProducto->getPVenta()* $Articulo["Cantidad"];
                
                $dtCampaniasPropiedades = dmCampania::getPropiedadesByIdCampania($IdCampania);
                $montoEnvioGratis = dmCampania::getMinMontoEnvioGratis($IdCampania);
                
                $free = 1;
                foreach($dtCampaniasPropiedades as $propiedad)
                {
                    if($propiedad["IdPropiedadCampamia"]=='6')
                    {
                       $free = 0;
                    }
                }

                if($montoEnvioGratis != 0 && $Total >= $montoEnvioGratis) {
                	$free = 0;
                }
                
            }
            
            /* DESCUENTO */
            if(isset($IdPedido) && is_numeric($IdPedido)) {
            	$Total -= $objPedido->getDescuento();
            	
            	$disc = dmDescuentos::getById($objPedido->getIdDescuento());
	         	if(is_object($disc) && $disc->getEnvioGratis() == 1)
	         		$free = 0;
	         		
            } else {
	            $Descuento = $objCarrito->getDescuento($Total);
				if($objCarrito->getDescuentoId() > 0) {
					$objPedido->setDescuento($Descuento);
					$objPedido->setIdDescuento($objCarrito->getDescuentoId());
					$Total -= $Descuento;
					
					$disc = dmDescuentos::getById($objCarrito->getDescuentoId());
		         	if($disc->getEnvioGratis() == 1)
		         		$free = 0;

				}
			}
			
			/* FIN DESCUENTO */

            if($free!=0)
            {
            	$objDHL = dmDHL::getTarifaByPesoPcia($Peso, $objDireccion->getIdCiudad(), $objCampania);
				$ValorEnvio = $objDHL->getImporte();
				// Si el valor del envio es mayor a cero, le agrego el valor del seguro
				if ($ValorEnvio > 0) {
					$ValorEnvio = $ValorEnvio + 300;
				}

            }
            else
            {
                $ValorEnvio = 0;
            }
            
            $objPedido->setGastosEnvio($ValorEnvio);
			 
			$creditoPermitido = intval(($PreciosVenta - $CostosCompra)/2);
			if(isset($_SESSION["CREDITO_APLICADO_CARRITO"]) && $_SESSION["CREDITO_APLICADO_CARRITO"] > $creditoPermitido) {
				$_SESSION["CREDITO_APLICADO_CARRITO"] = $creditoPermitido;
			}

            /*if($_SESSION["CREDITO_APLICADO_CARRITO"] > 0) {
	            $Total = ($Total/1.16 - $_SESSION["CREDITO_APLICADO_CARRITO"])*1.16 + $objPedido->getGastosEnvio();
            } else {
	        */    $Total = $Total + $objPedido->getGastosEnvio() - $_SESSION["CREDITO_APLICADO_CARRITO"];
            //}
 
            if($paymentMethod == GEELBE_CASH) {

                //validar saldo
                $GeelbeCashSaldo = GeelbeCashService::getEstadoGeelbeCash(Aplicacion::Decrypter($_SESSION['User']['id']))->getMontoSaldoActual();
                if($GeelbeCashSaldo < $Total) {
                	if(isset($IdPedido) && is_numeric($IdPedido)) {
	                	header("Location: /".$confRoot[1]."/ssl/carrito/step_1.php?IdPedido=".$IdPedido."&error_type=no_credit");
                	} else {
	                	header("Location: /".$confRoot[1]."/ssl/carrito/step_1.php?error_type=no_credit");
                	}
                	die;
                }

            }
 
            $objPedido->setTotal($Total);
            
            if(!$edit) {		
	            $objPedido->setIdPedido(dmPedidos::save($objPedido,$_SESSION["CREDITO_APLICADO_CARRITO"], $objDireccion));
	            $hayStock = dmPedidos::hayStockReal($objPedido->getIdPedido());
				
				/* DESCUENTO */
				if($objPedido->getIdDescuento() > 0)
					dmDescuentos::usarDescuento($objPedido->getIdDescuento());
				/* FIN DESCUENTO */
				
	            if(!$hayStock["db"]) {
	            	//ACA LE CIERRO EL PEDIDO
					$actualizador = actualizarPedidos::nuevo($objPedido->getIdPedido(), 10);
					$actualizador->Actualizar();
		            Conexion::nuevo()->Cerrar_Trans();
		            header("Location: "."/".$confRoot[1]."/front/vidriera/index.php");
		            exit();
	            }elseif(!$hayStock["temporal"]){
	            	//ACA LE TIRO UN CARTEL DE ERROR Y QUE SI QUIERE QUE VUELVA A INTENTAR
	
	            	exit("El ultimo articulo en stock esta siendo abonado por otro usuario. Intente nuevamente mas tarde.");
	            }else {
		            dmPedidos::setFechaEntradaSistemaPago($objPedido->getIdPedido());
		            $objCampania = dmCampania::getByIdCampania($objPedido->getIdCampania());
		            $NombreCampania = $objCampania->getNombre();
		            
		            $email = dmPedidos::getDatosMailByIdPedido($objPedido->getIdPedido());
		
		            if ($paymentMethod != GEELBE_CASH) {
						$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
						$mesFn = $meses[date("n", strtotime($email["TiempoEntregaFn"])) - 1];
						$mesIn = $meses[date("n", strtotime($email["TiempoEntregaIn"])) - 1];
		
						//Enviar email
						$datos = array(
							'NumeroPedido' => $objPedido->getIdPedido(),
							"TiempoEntregaFn" => date("d", strtotime($email["TiempoEntregaFn"]))." de ".$mesFn,
							"TiempoEntregaIn" => date("d", strtotime($email["TiempoEntregaIn"]))." de ".$mesIn,
							'NombreVidriera' => $NombreCampania,
							'IdUsuario' => $objPedido->getIdUsuario(),
							'HashIdUsuario' => Aplicacion::Encrypter($objPedido->getIdUsuario())
						);

						emailPedidoAceptado(Aplicacion::Decrypter($_SESSION['User']['email']), $datos);
		            }
					
					Aplicacion::VaciarCarrito();
		            unset($_SESSION["User"]["DireccionEnvio"]);
		            
		        }
			} else {
				$objCampania = dmCampania::getByIdCampania($objPedido->getIdCampania());
				 $NombreCampania = $objCampania->getNombre();
				
				$oConexion = new Conexion();
				$oConexion->Abrir_Trans();

				$oConexion->setQuery("UPDATE pedidos SET IdFormaPago = ".$paymentMethod.", GastosEnvio = ".$objPedido->getGastosEnvio().", Total = ".$objPedido->getTotal()." WHERE IdPedido = ".mysql_real_escape_string($IdPedido));
				
				$oConexion->EjecutarQuery();
                $oConexion->Cerrar_Trans();
			}
			
			//Eliminar pedido temporal
        if(isset($_SESSION['reanudarID']) && is_numeric($_SESSION['reanudarID'])) {
     			$oConexion = new Conexion();
				$oConexion->Abrir_Trans();
				$oConexion->setQuery("DELETE FROM pedidos_temp WHERE IdPedido = ".$_SESSION['reanudarID']);
				$pedido = $oConexion->DevolverQuery();
				$oConexion->Cerrar_Trans();
				unset($_SESSION['reanudarID']);
        }

            $TRX_ID = $objPedido->getIdPedido();
			$_SESSION['lastPedidoId'] = $TRX_ID;
			
			if($paymentMethod >= 1 && $paymentMethod <= 4) {
            	$useStub = Aplicacion::getParametros("environment_configuration", "use_payment_stub");
            	
            	// A tener en cuenta:
            	//     - mediosDePago: medio de pago seleccionado
            	//     - mediosDePago2: opciones dentro del medio de pago seleccionado
            	
                switch($paymentMethod) {
					case 1:
	            		$key = '12923839a68';
	            		$usr_id = '59535';
	            		$mediosDePago = "2,3,4,5";
	            		$mediosDePago2 = $mediosDePago;
						break;
        			case 2:
        				if($objPedido->getTotal() <= 18000) {
	            			$key = '12d0fc1fff8';
	            			$usr_id = '66613';
        				} else {
	            			$key = '12923839a68';
	            			$usr_id = '59535';
        				}
        				$mediosDePago = "2";
        				$mediosDePago2 = $mediosDePago;
        				break;
        			case 3:
                        if($objPedido->getTotal() <= 30000) {
	            			$key = '12d2dac4de6';
	            			$usr_id = '66787';
        				} else {
	            			$key = '12923839a68';
	            			$usr_id = '59535';
        				}
        				$mediosDePago = "4";
        				$mediosDePago2 = $mediosDePago;
	        			break;
 					case 4:
	            		$key = '12923839a68';
	            		$usr_id = '59535';
	            		$mediosDePago = "2";
	            		$mediosDePago2 = $mediosDePago;
						break;
       		}

				if($useStub=="true") {
	            	$urlPagosOnline = "https://gateway2.pagosonline.net/apps/gateway/index.html";
				} else {
	            	$urlPagosOnline = "https://gateway.pagosonline.net/apps/gateway/index.html";
				}
				
	            $clave = md5($key.'~'.$usr_id.'~'.$TRX_ID.'~'.number_format($objPedido->getTotal(), 2, '.', '').'~COP');
				$urlRespuesta = "http://".$_SERVER['HTTP_HOST']."/".$confRoot[1]."/front/micarrito/recibo_pagosonline.php";
				$urlConfirmacion = "http://".$_SERVER['HTTP_HOST']."/".$confRoot[1]."/logica/framework/pagosonline.php";

            ?>
            <html>
            <body>
	            <form method="post" action="<?=$urlPagosOnline?>">
	            	<input name="usuarioId" type="hidden" value="<?=$usr_id;?>">
	            	<input name="descripcion" type="hidden" value="Geelbe Colombia">
	            	<input name="refVenta" type="hidden" value="<?=$TRX_ID;?>">
	            	<input name="valor" type="hidden" value="<?=number_format($objPedido->getTotal(), 2, '.', '')?>">
	            	<input name="moneda" type="hidden" value="COP">
	            	<input name="firma" type="hidden" value="<?=$clave;?>">
	            	
	            	<input name="emailComprador" type="hidden" value="<?=Aplicacion::Decrypter($_SESSION['User']['email']);?>">
	            	<input name="url_respuesta" type="hidden" value="<?=$urlRespuesta?>">
	            	<input name="url_confirmacion" type="hidden" value="<?=$urlConfirmacion?>">
					<input type="hidden" name="tipo_medio_pago" value="<?=$mediosDePago?>" />
					<input type="hidden" name="tiposMediosDePago" value="<?=$mediosDePago2?>" />
					<input type="hidden" name="extra1" value="Fecha maxima de aprobacion <?=$objCampania->getNombre()?> - <?=$objCampania->getFechaFin()?>" />
					
					<?
					  if(count($ultimosProductos) > 0) {
					  	$ultimosProductosString = "";
					  	foreach($ultimosProductos as $idx => $value) {
					  		if($idx == 0) {
					  			$ultimosProductosString = $value;
					  		} else {
					  			$ultimosProductosString .= ",".$value;
					  		}
					  	}
					  	?>
						<input type="hidden" name="extra2" value="Ultimo producto - No. ref: <?=$ultimosProductosString?>" />
					  	<?
					  }
					?>
	
					<?
						if($useStub=="true") {
					?>
	            	<input name="prueba" type="hidden" value="1">
					<?
						}
					?>
	            </form>
	            <script>
	                    document.forms[0].submit();
	                </script>
            
<?
			} else if ($paymentMethod == GEELBE_CASH) {
            	// Descontar cash
            	$user_id = Aplicacion::Decrypter($_SESSION["User"]["id"]);
            	GeelbeCashService::utilizarGeelbeCash($user_id, $Total);
            	
            	// Actualizar pedido como aceptado
            	$actualizador = actualizarPedidos::nuevo($objPedido->getIdPedido(), 2);
	        	$actualizador->Actualizar();
	        	
				$objUsuario = dmUsuario::getByIdUsuario($user_id);
				
				// Envio email pago aceptado
				$mailer = new clsMailerPedidos("pagoaceptado", $objPedido->getIdPedido());
				$mailer->enviar();
	        	
				//Invalido la cache del credito de usuario
				$cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				$cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				$cache->remove('cache_credito_usuario_'.$user_id);
				
				// Invalido la cache del padrino
				$nombrePadrino = $objUsuario->getPadrino();
				if ($nombrePadrino && $nombrePadrino != "Huerfano") {
					$idPadrino = dmUsuario::getIdByNombreUsuario($nombrePadrino);
					$cache->remove('cache_credito_usuario_'.$idPadrino);	
				}
				
            	header("Location: /".$confRoot[1]."/ssl/geelbecash/pagoaceptado.php");
            	exit;
            } else {
            	header("Location: /".$confRoot[1]."/ssl/carrito/step_1.php?error_type=no_payment_method_selected");
            	exit;
            }
            
        } catch(MySQLException $e) {
            throw $e;
        }
	}
?>
