<?php
   if($_POST)
    {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
        
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/clases/clsmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmicuenta.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/clases/clsproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/clases/clscampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/clspedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/datamappers/dmpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/mailer/clsMailer.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/pedidos/clases/actualizarpedidos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/micuenta/datamappers/dmmiscreditos.php");
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/front/includes.php");

        Includes::Scripts();
        extract($_POST);
        try {
		$isPageOk = true;
		$pageException = "Exception";
		
		// Validaciones de parametros basicas
		if(!isset($_POST["IdCampania"]) && !isset($_POST["IdProducto"]) && !isset($_POST["IdCodigoProdInterno"]) && !isset($_POST["Cantidad"]) && !isset($_POST["StockDisp"]) && !isset($_POST["MaxProd"])) { // IdCampania, IdProducto, IdCodigoProdInterno, Cantidad, StockDisp y MaxProd seteado
            $isPageOk = false;
			$pageException = "IdCampania or IdProducto not setted";
			echo "IdCampania or IdProducto not setted";
		}
		else if (!ValidatorUtils::validateNumber($_POST["IdCampania"])) { // IdCampania Numerico
			$isPageOk = false;
			$pageException = "IdCampania is not a number";
		}
		else if (!ValidatorUtils::validateNumber($_POST["IdProducto"])) { // IdProducto Numerico
			$isPageOk = false;
			$pageException = "IdProducto is not a number";
		}
		else if (!dmProductos::isCampaignProduct($_POST["IdProducto"], $_POST["IdCampania"])) { // IdProducto Correspondiente con IdCampania
			$isPageOk = false;
			$pageException = "IdProducto no corresponde con IdCampania";
		}
		else if (!dmCampania::EsCampaniaAccesible($_POST["IdCampania"])) { // IdCampania Accesible
			$isPageOk = false;
			$pageException = "Campa�a not accesible";
		}
		else if (!ValidatorUtils::validateNumber($_POST["IdCodigoProdInterno"])) { // IdCodigoProdInterno Numerico
			$isPageOk = false;
			$pageException = "IdCodigoProdInterno is not a number";
		}
		else if (!ValidatorUtils::validateNumber($_POST["Cantidad"])) { // Cantidad Numerico
			$isPageOk = false;
			$pageException = "Cantidad is not a number";
		}
		else if (!ValidatorUtils::validateNumber($_POST["StockDisp"])) { // StockDisp Numerico
			$isPageOk = false;
			$pageException = "StockDisp is not a number";
		}
		else if (!ValidatorUtils::validateNumber($_POST["MaxProd"])) { // MaxProd Numerico
			$isPageOk = false;
			$pageException = "MaxProd is not a number";
		}
		
		if (!$isPageOk) {
			throw new Exception($pageException);
		}
	}
    catch(Exception $e) { // Aqui se puede loguear el error si es considerado necesario.
		header('HTTP/1.1 404 Not Found');
		include($_SERVER['DOCUMENT_ROOT'].'/'.$confRoot[1].'/front/error/404.php');
		exit;
    }  
		try
        {
        	if(!Aplicacion::VerificarCampaniaCarrito($IdCampania)) {
        		echo '<script>
                        	window.parent.msjInfoJs("Error", "No pueden agregarse al carrito de compras productos de diferentes campa&ntilde;as, por favor finalice la compra actual y vuelva a seleccionar este producto. Muchas Gracias.", null, "location.href = \\\'../../../front/micarrito\\\'");
                        </script>';
        		die;
        	}
        
            Aplicacion::IniciarCarrito($IdCampania);
            $objCarrito = Aplicacion::getCarrito();
            $objCarrito->addArticulo($IdProducto, $IdCodigoProdInterno,$cbxCantidad, 0, $StockDisp);
            Aplicacion::setCarrito($objCarrito, $IdCampania);
	        $cartUrlProtocol = "https://";
			if($_SERVER['HTTP_HOST'] == "localhost") {
				$cartUrlProtocol = "http://";
			}
            ?>
                <script>
                    //var DirAux ="<?=Aplicacion::getRootUrl()?>"+"front/catalogo/index.php?IdCampania=<?=$IdCampania?>";
                    var DirAux = "<?=$cartUrlProtocol.$_SERVER['HTTP_HOST']."/".$confRoot[1]."/ssl/carrito/step_1.php"?>";
                    window.parent.Redirect(DirAux,"_self");
                </script>
            <?
        }
        catch(MySQLException $e)
        {
            throw $e;
        }
        ob_flush();
    }
?>
