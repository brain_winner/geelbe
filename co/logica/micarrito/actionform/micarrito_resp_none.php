<?php
    ob_start();
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("micarrito"));
    try {
        ValidarUsuarioLogueado();
	}
	catch(Exception $e) {
		header('Location: /'.$confRoot[1]);
		exit;
    }
    try
    {
			$NroTransaccion = Aplicacion::Decrypter($_SESSION["User"]["DineroMail"]["NroItem"]);
        $actualizador = actualizarPedidos::nuevo($NroTransaccion, 4);
		$actualizador->Actualizar();
        Conexion::nuevo()->Cerrar_Trans();
    }
    catch(Exception $e)
    {
		Conexion::nuevo()->RollBack();
    	throw $e;
    }
    unset($_SESSION["User"]["DineroMail"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? Includes::Scripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=Aplicacion::getParametros("info", "nombre");?> </title>
<link href="../geelbe.css" rel="stylesheet" type="text/css" />

<?include $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/background/actionform/background.php";?>
</head>
<body>
    <script>
        oCortina.showError(Array(arrErrores["COMPRA"]["DINEROMAIL_RECHAZADO"]),arrErrores["COMPRA"]["TITLE"], "<?=Aplicacion::getRootUrl()?>front/micuenta", 1);
    </script>
</body>
</html>