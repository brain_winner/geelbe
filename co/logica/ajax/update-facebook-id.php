<?
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/conexion/Conectar.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/usuarios/datamappers/dmusuarios.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/usuarios/datamappers/dmusuariosfacebook.php";
	
	if($_REQUEST['user_id'] == null) {
	    $respuesta = array("error" => "true", "error_type" => "user_id_is_required");
	    echo json_encode($respuesta);
		exit;
    }
	
	if($_REQUEST['hash'] == null) {
	    $respuesta = array("error" => "true", "error_type" => "hash_is_required");
	    echo json_encode($respuesta);
		exit;
    }
	
    if(md5($_REQUEST['user_id']."pajarito") != $_REQUEST['hash']) {
	    $respuesta = array("error" => "true", "error_type" => "invalid_hash");
	    echo json_encode($respuesta);
		exit;
    }
	
    try {
		if($_REQUEST['user_data'] == null) {
		    $respuesta = array("error" => "true", "error_type" => "user_data_is_required");
		    echo json_encode($respuesta);
			exit;
	    }
    	
	    $userId = $_REQUEST['user_id'];
		$objUsuario = dmUsuario::getByIdUsuario($userId);
	    $userData = json_decode(stripslashes($_REQUEST['user_data']));

	    if($userData->uid == null || $userData->uid == "") {
		    $respuesta = array("error" => "true", "error_type" => "facebook_id_required");
		    echo json_encode($respuesta);
			exit;
	    }
	    
	    dmUsuariosFacebook::saveOrUpdateFacebookId($userId, $userData->uid);
	    $actualizarUsuario = dmUsuariosFacebook::updateUserInfo($objUsuario, $userData);

	    if ($actualizarUsuario) {
		    dmUsuario::save($objUsuario);
	    }
	    
	    $respuesta = array("error" => "false");
	    echo json_encode($respuesta);

    } catch(Exception $e) {
	    $respuesta = array("error" => "true", "error_type" => "problems_updating_user_data", "error_desc" => $e);
	    echo json_encode($respuesta);
		exit;
    }
?>