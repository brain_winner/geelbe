<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf_bo/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/campanias/datamappers/dmcampanias.php"); 
    
    if(!isset($_REQUEST["idCampania"]) || empty($_REQUEST["idCampania"])) {
    	echo '{"result":"fail", "error":"El Id de Campania es requerido."}';
    	exit;
    }
    
    try {
    	dmCampania::saveVisit($_REQUEST["idCampania"]);
    } catch (Exception $e) {
    	throw $e;
    }
   	echo '{"result":"ok"}';
?>