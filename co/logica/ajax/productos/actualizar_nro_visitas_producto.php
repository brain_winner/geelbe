<?php
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/productos/datamappers/dmproductos.php"); 
    
    if(!isset($_REQUEST["idProducto"]) || empty($_REQUEST["idProducto"])) {
    	echo '{"result":"fail", "error":"El Id de Producto es requerido."}';
    	exit;
    }
    
    try {
    	dmProductos::saveVisit($_REQUEST["idProducto"]);
    } catch (Exception $e) {
    	throw $e;
    }
    
   	echo '{"result":"ok"}';
?>