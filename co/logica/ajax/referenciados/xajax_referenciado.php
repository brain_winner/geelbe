<?	
	require_once("Sistema/Global.php");
	require_once("Sistema/Funciones/FuncionesGlobales.php");
	require_once($dirCon . "Conectar.php");
	require_once("Xajax_Global.php");
	require_once($dirPre . "html_Apadrina.php");
    require_once($dirCla . "msn_contact_grab.class.php");
    require_once($dirCla . "gmail.php");
    require_once($dirCla . "class.GrabYahoo.php");
    require_once($dirCla . "config.php");
    require_once($dirDM . "DMApadrinar.php");
    require_once($dirDM . "DMReglasNegocios.php");

    
	$xajax->registerFunction("CargaInicial");
	$xajax->registerFunction("CargarDirecciones");
	$xajax->registerFunction("ApadrinarM");
	$xajax->registerFunction("Apadrinar");	
	$xajax->processRequests();

	function ApadrinarM($frm,$idUsuario)
	{
		$objResponse = new xajaxResponse("ISO-8859-1");
		
	//	$objResponse->addAlert(print_r($frm));
		$Cantidad = DMA_Invitacion($frm,$idUsuario);
		DMReglasNegocio_Aplicar_09($idUsuario, $Cantidad);//Invitacion Masivo		
		
		$objResponse->addAlert("Las invitaciones Fueron Enviadas con exito");
		return $objResponse;
	}
	function Apadrinar($frm,$idUsuario,$NomApe)
	{
		$objResponse = new xajaxResponse("ISO-8859-1");
		
	//	$objResponse->addAlert(print_r($frm));
		
		$Cantidad = DMA_InvitacionManual($frm,$idUsuario);
		DMReglasNegocio_Aplicar_08($idUsuario, $Cantidad);//Invitacion		

		$objResponse->addAlert("Las invitaciones Fueron Enviadas con exito");
		
		return $objResponse;
	}
	function CargaInicial($idUsuario)	
	{
		$objResponse = new xajaxResponse("ISO-8859-1");
		
		$html="";
		
		$html .=Invitacion();
		$NombreUsuario=DMA_GetNombreApellidoDUP($idUsuario);
		$objResponse->addAssign("lstApadrinarAmigo","innerHTML",$html);
		$mensaje = "Hola,

Aqu� te dejo una p�gina web con descuentos de hasta un 80% en marcas. Espero que te guste.

Un Saludo, " . $NombreUsuario[0]->Nombre . " " . $NombreUsuario[0]->Apellido;
		$objResponse->addAssign("txtMensaje","value",$mensaje);
		$objResponse->addAssign("txtNombreUsuario","value",$NombreUsuario[0]->Nombre . " " . $NombreUsuario[0]->Apellido);
		
		return $objResponse;
	}
	function CargarDirecciones($frm)
	{
		$objResponse = new xajaxResponse("ISO-8859-1");
		$Usuario=$frm["txtUsuario"];
		$Clave=$frm["txtPassword"];
		
		$arroba=strpos($frm['txtUsuario'],"@");
		$Usuario=substr($frm["txtUsuario"],0,$arroba);
	
		switch ($frm["idServicio"])
		{
			case 1:
				$Dominio = "@hotmail.com";
				$msn2 = new msn;
				$returned_emails = $msn2->qGrab($Usuario.$Dominio, $Clave);
				
				if(substr($returned_emails,0,6)!="Array")
				{
					$objResponse->addScript("objCortina.showAlert('".$returned_emails."','Error de Usuario o Password');");
				}
				else
				{
					$html="";
					$html .="<table style='width:100%;height:100%'><tr><td><b>Listado de Contactos de $Usuario$Dominio</b></br></br></td></tr>";
					$html .= Contactos_MSN($returned_emails);
					$html .="</table></br>";
					$objResponse->addAssign("lstApadrinarAmigo","innerHTML",$html);
				}
				break;
			case 2:
				$yahoo  = new GrabYahoo;
				
				$yahoo->service = "addressbook";
				
				$yahoo->isUsingProxy = false;
				
				$yahoo->proxyHost = "";
				
				$yahoo->proxyPort = "";
				
				$yahooList = $yahoo->execService($Usuario, $Clave);
				
				$html="";
				$html .="<table style='width:100%;height:100%'><tr><td><b>Listado de Contactos de $Usuario$Dominio</b></br></br></td></tr>";
				//	$objResponse->addAlert(print_r($yahooList));
				$html .=Contactos_Yahoo($yahooList);
				$html .="</table></br>";
				
				$objResponse->addAssign("lstApadrinarAmigo","innerHTML",$html);
				
				break;
			case 3:
				$Dominio="@gmail.com";
				
				$gmail_acc = $Usuario;
				$gmail_pwd = $Clave;
				
				$my_timezone = 0;
				
				$gmailer = new GMailer();
				
				if ($gmailer->created)
				{
				$gmailer->setLoginInfo($gmail_acc, $gmail_pwd, $my_timezone);
				
				if ($gmailer->connect())
				{				
				$gmailer->fetchBox(GM_CONTACT, "all", "");
				
				$snapshot = $gmailer->getSnapshot(GM_CONTACT);
				
				$html="";
				$html .="<table style='width:100%;height:100%'><tr><td><b>Listado de Contactos de $Usuario$Dominio</b></br></br></td></tr>";
				$html .=Contactos_Gmail($snapshot->contacts);
				$html .="</table></br>";				
				}
				else
				{
				die("Fail to connect because: ".$gmailer->lastActionStatus());
				}
				} 
				else 
				{
				die("Failed to create GMailer because: ".$gmailer->lastActionStatus());
				}
				$gmailer->disconnect();
				$objResponse->addAssign("lstApadrinarAmigo","innerHTML",$html);
				break;
			default:
		}
		return $objResponse;			
	}
?>	