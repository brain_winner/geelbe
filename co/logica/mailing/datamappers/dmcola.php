<?	
	/**
	 * Datamapper de la cola de salida de mails. "amortiguador".
	 *
	 */
	class dmcola
    {    
    	/**
    	 * Toma el objeto de referencia de la coenxion.
    	 *
    	 * @param Conexion $oConexion
    	 */
    	protected  $oConexion = "";
    	    	
    	/**
    	 * Constructor. Opcionalmente, acepta un objeto conexion. Sino lo crea.
    	 *
    	 * @param Conexion $oConexion
    	 */
    	public function __construct(Conexion $oConexion=null){
    		if(isset($oConexion)){
    			$this->oConexion = $oConexion;
    		}else{
    			$this->oConexion = Conexion::nuevo();
    		}
    	}
    	
    	/**
		 * devuelve el objeto oConexion para usar en otro dm
		 *
		 * @return Conexion
		 */
		public function getConexion(){
			return $this->oConexion;
		}
    	
		
		/**
		 * Traer Mensajes de la base para enviar.
		 *
		 * @param integer $cantidad de mensajes a traer
		 * @return array con los mensajes.
		 */
		public function traer_mensaje($cantidad=1){
			
            try
            {  
                if(!is_int($cantidad) && $cantidad<1) throw new Exception("Cantidad de mensajes a traer debe ser >=1");
                
            	$this->oConexion->setQuery("SELECT Email, d.Nombre, d.Apellido, u.NombreUsuario
            	FROM ahijados AS a JOIN datosusuariospersonales AS d ON d.IdUsuario = a.IdUsuario 
            	JOIN usuarios AS u ON a.IdUsuario = u.IdUsuario
            	WHERE fecha_envio_programado IS NOT NULL  
            	ORDER BY fecha_envio_programado ASC LIMIT ".$cantidad);
                
            	return $this->oConexion->DevolverQuery();
            }
            catch (MySQLException $e)
            {                           	
                throw $e;
            }
		}
		
		
		/**
		 * programar_mensajes. Pone fecha de salida a los mensajes
		 *
		 * @param array $mensajes el array que tira traer_mensajes, o si es un unico, un integer del ID.
		 */
	 	public function programar_mensajes($ahijados,$fecha=false){
	 		
	 		$fecha = $this->_armar_fecha($fecha);
	 		
	 		$where = $this->_where_IdAhijado($ahijados);
	 		
	 		$this->oConexion->setQuery("UPDATE ahijados SET fecha_envio_programado = '".$fecha."' WHERE ".$where);
	 		$this->oConexion->EjecutarQuery();
	 					 		
	 	}
	 		
		/**
		 * marcar_mensajes. Pone fecha de salida a los mensajes
		 *
		 * @param array $mensajes el array que tira traer_mensajes, o si es un unico, un integer del ID.
		 */
	 	public function marcar_mensajes($ahijados,$fecha=false){
	 		
	 		$fecha = $this->_armar_fecha($fecha);
	 		
	 		$where = $this->_where_IdAhijado($ahijados);
	 		
	 		$this->oConexion->setQuery("UPDATE ahijados SET fecha_ultimo_envio = '".$fecha."', fecha_envio_programado = NULL WHERE ".$where);
	 		$this->oConexion->EjecutarQuery();
	 					 		
	 	}
	 	
	 	public function traer_ahijados(){
	 		$this->oConexion->setQuery("SELECT * FROM ahijados AS a WHERE IdUsuarioReg IS NULL AND Email NOT LIKE '%hotmail%' AND Email NOT LIKE '%live%'");
	 		
	 	}
	 	
	 	
	 	
	 	/**
	 	 * marcar bounce. Marca los mails en la db.
	 	 *
	 	 * @param string $email 
	 	 * @param string $tipo soft/hard bounce
	 	 * @return bool true si todo esta ok
	 	 */
	 	
	 	public function marcar_bounce($email,$tipo){
	 		if($tipo != "hard" && $tipo !="soft")throw new Exception("el tipo debe ser hard o soft");
	 		if(checkemail($email)) throw new Exception("el email no es v�lido");
	 		
	 		$this->oConexion->setQuery("SELECT IdAhijado, bounce FROM ahijados WHERE Email = '".$email."'");
	 		
	 		$ahijados = $this->oConexion->DevolverQuery();
	 		
	 		
	 		//!!! Si no tengo ningun ahijado con ese mail, corto el proceso.
	 		if(empty($ahijados)) 
	 			return false;	 			
	 		
	 			 		
	 		
	 		foreach($ahijados as $ahijado) 
	 			$bounce = ($ahijado>$bounce)?$ahijado:$bounce;
	 		
	 		if($bounce == 9){
	 			$bounce = false;
	 		}else{
	 			if($bounce >= 8 || $tipo == "hard"){
		 			$bounce = 9;
	 			}elseif($bounce >= 1){
			 			$bounce++;
		 		}else{
			 			$bounce = 1;
		 		}
	 		}
	 		
	 		$where = $this->_where_IdAhijado($ahijado);
	 		
	 		$this->oConexion->setQuery("UPDATE ahijados SET bounce = ".$bounce." WHERE ".$where);
	 		$this->oConexion->EjecutarQuery();
	 		
	 		return true;
	 					 	
	 	}
	 	
	 	
	 	
	 	
	 	
	 	/**
	 	 * genera fecha. wrapper del strtotime
	 	 *
	 	 * @param string $fecha para strtotime
	 	 * @return fecha
	 	 */
	 	protected function _armar_fecha($fecha=false){
	 		if(!$fecha){ 
				$fecha = date('Y-m-d H:i:s');
			}else{
				$fecha = date('Y-m-d H:i:s',strtotime($fecha));
			}
			if($fecha===false) throw new Exception("La fecha es inv�lida");
	 		return $fecha;
	 	}
	 	
	 	protected function _where_IdAhijado($ahijados){
	 		
	 		if(is_integer($ahijados)) $ahijados = array(0 => array("IdAhijado" => $ahijados));
	 		if(!is_arary($ahijados)) throw new Exception("marcar_mensajes acepta int IdAhijado, o array[n][IdAhijado]");
	 		
	 		$where = "";
	 		$i = 0;
	 		
	 		foreach($ahijados as $ahijado){
	 			$where .= $i === 0 ? "" : " OR ";
	 			$where .= "IdAhijado  = ".$ahijado["IdAhijado"];
	 			$i++;
	 		};
	 		
	 		return $where;
	 	}
	 	
	 	
	 	
		
    }
//-para:14 asuto:33 msj:12447 headers:297 -?>