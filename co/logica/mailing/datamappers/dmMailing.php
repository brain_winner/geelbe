<?	
	class dmMailing
    {   
    	/**
    	 * Toma el objeto de referencia de la coenxion.
    	 *
    	 * @param Conexion $oConexion
    	 */
    	private $oConexion = "";
    	    	
    	/**
    	 * Constructor. Opcionalmente, acepta un objeto conexion. Sino lo crea.
    	 *
    	 * @param Conexion $oConexion
    	 */
    	public function __construct(Conexion $oConexion=null){
    		if(isset($oConexion)){
    			$this->oConexion = $oConexion;
    		}else{
    			$this->oConexion = Conexion::nuevo();
    		}
    	}
    	
    	public function addFlag($IdUsuario,$operacion){
            try
            {  
                $this->oConexion->setQuery("INSERT INTO usuarios_mailing (IdUsuario, flag, operacion) VALUES (".mysql_real_escape_string($IdUsuario).", 1, ".$operacion.") ON duplicate KEY UPDATE flag=flag+1, operacion='".$operacion."'");
                $this->oConexion->EjecutarQuery();
            }
            catch (MySQLException $e)
            {
            	//var_dump($e);
               // $this->oConexion->RollBack();
                //throw $e;
               
            }
		}
		
		/**
		 * Remueve el Flag. El mail ya fue procesado correctamente
		 *
		 * @param integer $IdUsuario
		 * @param integer $operacion alta = 1/baja =2
		 */
		public function removeFlag($IdUsuario, $operacion){
            try
            {  
                //$this->oConexion->Abrir_Trans();
                $this->oConexion->setQuery("DELETE FROM usuarios_mailing WHERE IdUsuario = ".mysql_real_escape_string($IdUsuario)." AND operacion = ".$operacion);
                $this->oConexion->EjecutarQuery();
                //$this->oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {
            	//var_dump($e);
            	//echo mysql_error();
                //$this->oConexion->RollBack();
                //throw $e;
            }
		}
		
		/**
		 * Trae listado de usaurios flaggeados
		 *
		 * @return array
		 */
		public function getFlagged(){
            try
            {  
                //$this->oConexion->Abrir_Trans();
                $this->oConexion->setQuery("SELECT IdUsuario, operacion FROM usuarios_mailing ORDER BY flag, id");
                return $this->oConexion->DevolverQuery();
                //$this->oConexion->Cerrar_Trans();
            }
            catch (MySQLException $e)
            {               
            	//var_dump($e); 
                //throw $e;
            }
		}	
		
		/**
		 * devuelve el objeto oConexion para usar en otro dm
		 *
		 * @return Conexion
		 */
		public function getConexion(){
			return $this->oConexion;
		}
    }
?>