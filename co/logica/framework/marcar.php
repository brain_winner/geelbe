<?php
	
	$TRX_ID = $_REQUEST['ref_venta'];
	
	try
	{
	    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mispedidos", "clases"));
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("emails"));
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
	}
	catch(exception $e)
	{
	    die(print_r($e));
	}

	try
	{
            $actualizador = actualizarPedidos::nuevo($TRX_ID, 2);
            $actualizador->Actualizar();
            Conexion::nuevo()->Cerrar_Trans();
     
            $pedido = dmPedidos::getPedidoById($TRX_ID);
       
	    	//Invalido la cache del credito de usuario
                $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
	        $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
	        $cache->remove('cache_credito_usuario_'.$pedido->getIdUsuario());
            
	        // Invalido la cache del padrino
	        $objUsuario = dmUsuario::getByIdUsuario($pedido->getIdUsuario());
            $nombrePadrino = $objUsuario->getPadrino();
            if($nombrePadrino && $nombrePadrino != "Huerfano") {
    	      $idPadrino = dmUsuario::getIdByNombreUsuario($nombrePadrino);
	          $cache->remove('cache_credito_usuario_'.$idPadrino);
            }
            
         $camp = dmCampania::getByIdCampania($pedido->getIdCampania());
			$u = dmUsuario::getByIdUsuario($pedido->getIdUsuario());
			$email = dmPedidos::getDatosMailByIdPedido($pedido->getIdPedido());
			
	$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	$mesFn = $meses[date("n", strtotime($email["TiempoEntregaFn"])) - 1];
	$mesIn = $meses[date("n", strtotime($email["TiempoEntregaIn"])) - 1];
			
			$datos = array(
				'NumeroPedido' => $pedido->getIdPedido(),
				'SistemaPago' => 'PagosOnline',
				'NombreVidriera' => $camp->getNombre(),
				'IdUsuario' => $pedido->getIdUsuario(),
				'HashIdUsuario' => Aplicacion::Encrypter($pedido->getIdUsuario()),
				'NombreUsuario' => $u->getDatos()->getNombre(),
				'ApellidoUsuario' => $u->getDatos()->getApellido(),
				"TiempoEntregaFn" => date("d", strtotime($email["TiempoEntregaFn"]))." de ".$mesFn,
				"TiempoEntregaIn" => date("d", strtotime($email["TiempoEntregaIn"]))." de ".$mesIn
			);
			
			emailPagoAceptado($u->getDatos()->getEmail(), $datos);
                      
	}
	catch(FaltaSockException $e){
		$actualizador = actualizarPedidos::nuevo($TRX_ID, 10);
		$actualizador->Actualizar();
	}                         	
		
		
	   
	
?>