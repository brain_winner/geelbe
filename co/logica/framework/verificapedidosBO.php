<?php
try
{
    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("pedidos", "clases"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("emails"));
    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
	set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
	require_once 'Zend/Cache.php'; 

}
catch(exception $e)
{
    die(print_r($e));
}
	$usuarios = dmPedidos::getUsuariosPedidosPendientes();
	$Cuenta = Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "e_comercio"))."2";
	$Email = Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "email_cuenta"));
	$Pin = Aplicacion::Decrypter(Aplicacion::getParametros("dineromail", "pin_consultas"));
	$objDMReporte = new ReportePedidosDM($Email,$Cuenta, $Pin);
	$objDMReporte->sendConsulta();

foreach($usuarios as $id)
{
    try
    {
        $dtUsuarios = dmPedidos::getPedidoByIdDM($id['IdPedido']);
        $TRX_ID = $dtUsuarios[0]["IdPedido"];
        if($dtUsuarios[0]["IdEstadoPedidos"] == "0" || $dtUsuarios[0]["IdEstadoPedidos"] === 0 || $dtUsuarios[0]["IdEstadoPedidos"] == 1 || $dtUsuarios[0]["IdEstadoPedidos"] == 4)
        {
        	try
            {
            	if($objDMReporte->getPayByIdTRX($TRX_ID)){
        	    	$actualizador = actualizarPedidos::nuevo($TRX_ID, 2);
    	        	$actualizador->Actualizar();
		            Conexion::nuevo()->Cerrar_Trans();

            	
		            $idUsuarioDePedido = dmPedidos::getIdUsuarioByPedido($id['IdPedido']);
	            	
		            //Invalido la cache del credito de usuario
		            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
				    $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
				    $cache->remove('cache_credito_usuario_'.$idUsuarioDePedido[0]["IdUsuario"]);
		            
				    // Invalido la cache del padrino
				    $objUsuario = dmUsuario::getByIdUsuario($idUsuarioDePedido[0]["IdUsuario"]);
		    	    $nombrePadrino = $objUsuario->getPadrino();
		    	    if($nombrePadrino && $nombrePadrino != "Huerfano") {
			    	  $idPadrino = dmUsuario::getIdByNombreUsuario($nombrePadrino);
				      $cache->remove('cache_credito_usuario_'.$idPadrino);
		    	    }		            
		            
		            $pedido = dmPedidos::getPedidoById($TRX_ID);
		            $camp = dmCampania::getByIdCampania($pedido->getIdCampania());
					$u = dmUsuario::getByIdUsuario($pedido->getIdUsuario());
					$email = dmPedidos::getDatosMailByIdPedido($pedido->getIdPedido());
					
	$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	$mesFn = $meses[date("n", strtotime($email["TiempoEntregaFn"])) - 1];
	$mesIn = $meses[date("n", strtotime($email["TiempoEntregaIn"])) - 1];
	
					$datos = array(
						'NumeroPedido' => $pedido->getIdPedido(),
						'SistemaPago' => 'DineroMail',
						'NombreVidriera' => $camp->getNombre(),
						'IdUsuario' => $pedido->getIdUsuario(),
						'HashIdUsuario' => Aplicacion::Encrypter($pedido->getIdUsuario()),
						'NombreUsuario' => $u->getDatos()->getNombre(),
						'ApellidoUsuario' => $u->getDatos()->getApellido(),
						"TiempoEntregaFn" => date("d", strtotime($email["TiempoEntregaFn"]))." de ".$mesFn,
						"TiempoEntregaIn" => date("d", strtotime($email["TiempoEntregaIn"]))." de ".$mesIn
					);
					
					emailPagoAceptado($u->getDatos()->getEmail(), $datos);
					
	            }
            }
            catch (FaltaSockException $e){
   				Conexion::nuevo()->RollBack();

   				//Anulo.
				try
	            {
	            	$actualizador = actualizarPedidos::nuevo($TRX_ID,10);
	            	$actualizador->Actualizar();
	         		Conexion::nuevo()->Cerrar_Trans();
	            }
	            catch(exception $e)
	            {
	             	Conexion::nuevo()->RollBack();
	             	throw $e;
	            }

            }
            catch(Exception $e){
            	Conexion::nuevo()->RollBack();
            }
        }
    }
    


    catch(exception $e)
    {
        die(print_r($e));
    }
}

    
    //####################################################################################################################

    
	$arrayCupones  = $objDMReporte->checkCupones();
	$tablaDM = dmCupones::getIdPedidosbyArray($arrayCupones);	
	$PedidosEsperando = dmPedidos::getUsuariosPedidosPendientes();

	
	foreach($tablaDM as $filaDM){
		/*$actualizadorCupon = actualizarPedidos::nuevo($filaDM["Pedido_Id"],2);
		$actualizadorCupon->Actualizar();
		*/
		
		$do = false;
		foreach($PedidosEsperando as $PedidoEsperando){

			if($PedidoEsperando["IdPedido"] == $filaDM["Pedido_Id"]) $do = true;
		}

		if($do){			
			
			try{            	
            	$actualizador = actualizarPedidos::nuevo($filaDM["Pedido_Id"], 2);
            	$actualizador->Actualizar();
	            Conexion::nuevo()->Cerrar_Trans();
				            
				$idUsuarioDePedido = dmPedidos::getIdUsuarioByPedido($filaDM["Pedido_Id"]);
            	
	            //Invalido la cache del credito de usuario
	            $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
			    $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
			    $cache->remove('cache_credito_usuario_'.$idUsuarioDePedido[0]["IdUsuario"]);
	            
			    // Invalido la cache del padrino
			    $objUsuario = dmUsuario::getByIdUsuario($idUsuarioDePedido[0]["IdUsuario"]);
	    	    $nombrePadrino = $objUsuario->getPadrino();
	    	    if($nombrePadrino && $nombrePadrino != "Huerfano") {
		    	  $idPadrino = dmUsuario::getIdByNombreUsuario($nombrePadrino);
			      $cache->remove('cache_credito_usuario_'.$idPadrino);
	    	    }
	            
            }catch (FaltaSockException $e){
   				Conexion::nuevo()->RollBack();

   				//Anulo.
				try
	            {
	            	$actualizador = actualizarPedidos::nuevo($filaDM["Pedido_Id"],10);
	            	$actualizador->Actualizar();
	         		Conexion::nuevo()->Cerrar_Trans();
	            }
	            catch(exception $e)
	            {
	             	Conexion::nuevo()->RollBack();
	             	throw $e;
	            }
            }
            catch(Exception $e){
	       		Conexion::nuevo()->RollBack();
        	}

		}
	}
    
    
    
    
    
    
    
    
    //####################################################################################################################
 //   echo Conexion::nuevo()->DevolverLog();
?>