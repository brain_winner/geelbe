<?php
    class clsFile
    {
        private $_nombre = "";
        private $_extension = "";
        private $_tamanio = "";
        private $_tipo = "";
        private $_permisos = "";
        private $_puntero = "";
        private $_modo = "";
        
        public function Abrir($filename, $modo)
        {
            $this->_modo = $modo;
            $this->_puntero = fopen($filename, $this->_modo);                        
        }
        public function Cerrar()
        {
            fclose($this->_puntero);
        }
        public function Escribir($dato)
        {
                fwrite($this->_puntero, $dato);            
        }
        public function Leer($cantbytes = null)
        {
                if ($cantbytes)
                {
                    return fread($this->_puntero, $cantbytes);
                }
                else
                {
                    fseek($this->_puntero, 0, 2);
                    $cantbytes = ftell($this->_puntero);
                    fseek($this->_puntero, 0, 0);
                    return fread($this->_puntero, $cantbytes);
                }  
        }
                
        public function getNombre()
        {
            return $this->_nombre;
        }
        public function getExtension()
        {
            return $this->_extension;
        }
        public function getTipo()
        {
            return $this->_tipo;
        }
        public function __construct($Archivo)
        {
            $this->_nombre = clsFile::ObtenerNombre($Archivo);
            $this->_extension = clsFile::ObtenerExtension($Archivo);
            $this->_tipo = @filetype($Archivo);
            $this->_tamanio = @filesize($Archivo);
            $this->_permisos = @fileperms($Archivo);
        }
        /**
        * @desc Obtiene el nombre del archivo sin la extensi�n.
        * @param string. El archivo del que se quiere obtener el nombre.
        * @return string.
        */
        public static function ObtenerNombre($Archivo)
        {
            try
            {
                /*$extension = clsFile::ObtenerExtension($Archivo);
                str_replace(".".$extension, "",$Archivo);
                return $Archivo;*/
                $Nombre = basename($Archivo);
                $Nombre = basename($Archivo, ".".clsFile::ObtenerExtension($Archivo));
                return $Nombre;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        /**
        * @desc Obtiene la extensi�n de un archivo
        * @param string. El archivo que se quiere obtener la extensi�n.
        * @return string
        */
        public static function ObtenerExtension($Archivo)
        {
            try
            {
                $temp1 = explode(".",$Archivo); 
                $temp2 = count($temp1)-1; 
                $ext = $temp1[$temp2]; 
                $ext = strtolower($ext);
                return $ext;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        /**
        * @desc Elimina un archivo, incluso utilizando comodines.
        * @param string. El comod�n Ej: .*, .jpg, etc
        * @return bool
        */
        public static function Delete($Archivo, $wildcard="")
        {
            try
            {
                $File = $Archivo.$wildcard;
                 foreach(glob($File) as $fn) 
                 { 
                    @unlink($fn); 
                 } 
                 return true;
            }
            catch(exception $e)
            {
                return false;
            }
           
        }
    }
?>
