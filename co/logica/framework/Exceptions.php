<?php
    class MySQLException extends Exception 
    {
        private $_numero;        
        private $_mensaje;
        
        public function setNumeroSQLERROR($numero)
        {
            $this->_numero = $numero;
        }
        public function getNumeroSQLERROR()
        {
            return $this->_numero;
        }
        public function setMensajeSQLERROR($mensaje)
        {
            $this->_mensaje = $mensaje;
        }
        public function getMensajeSQLERROR()
        {
            return $this->_mensaje;
        }        
        public function MySQLException($string, $numeroSQLERROR=0, $mensajeSQLERROR="", $codigo=0)
        {
            $this->message =$string."-(".$codigo.")".$mensajeSQLERROR;
            $this->_numero = $numeroSQLERROR;
            $this->_mensaje = $mensajeSQLERROR;
            $this->code = $codigo;
            /*$objLog = new Log(M_ERROR);
            $objLog->setArchivo($this->file);
            $objLog->setDescripcion($string.chr(13).$mensajeSQLERROR);
            $objLog->setDirectorio(Aplicacion::getRoot().Aplicacion::getParametros("log_errores", "archivo"));
            $objLog->setLinea($this->line);
            $objLog->setUsuario((isset($_SESSION["User"]["id"]))?Aplicacion::Decrypter($_SESSION["User"]["email"]):0);
            $objLog->setMovimiento();*/
        }  
    }
    class ACCESOException extends Exception 
    {
        private $_codigo;
        private $_titulo;
        public function ACCESOException($string, $codigo="", $titulo="")
        {
            $this->message = $string;
            $this->_codigo = $codigo;
            $this->_titulo = $titulo;
        }
        public function getCodigo()
        {
            return $this->_codigo;
        }
        public function getTitulo()
        {
            return $this->_titulo;
        }
    }
    class FaltaSockException extends Exception{}
?>
