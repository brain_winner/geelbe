<?php
    class TablaDinamica
    {
        private $_columnas=array();
        private $_nombre;
        private $_botones = array();
        public function TablaDinamica($nombre="tbl")
        {
            $this->_nombre=$nombre;
        }
        public function addColumna($index, $nombre, $visible=true, $Propiedades= array())
        {
            $this->_columnas[$index]["Nombre"] =$nombre;
            $this->_columnas[$index]["Visible"] =$visible;
            $this->_columnas[$index]["Propiedades"] = (is_array($Propiedades)?$Propiedades:array());
        }
        public function addBotones($boton)
        {
            $this->_botones[]= $boton;
        }
        private function setScripts()
        {
            ?>
                <script>
                    var arrStyle = new Array();
                    <?
                        foreach($this->_columnas as $i=> $columna)
                        {
                            ?>arrStyle[<?=$i?>] = new Array();<?
                            foreach($columna["Propiedades"] as $j=> $Propiedad)
                            {
                                ?>
                                arrStyle[<?=$i?>][<?=$j?>] = "<?=str_replace(chr(34),"'", $Propiedad) ?>";
                                <?
                            }
                            ?>
                                arrStyle[<?=$i?>]["Visible"] = "<?=($columna["Visible"]==true)?"":"none"?>";
                            <?
                        }
                    ?>
                    var <?=$this->_nombre?>IndexFilaSeleccionada = -1;
                    function <?=$this->_nombre?>_getFIlaSeleccionada()
                    {
                        return <?=$this->_nombre?>IndexFilaSeleccionada;
                    }
                    function <?=$this->_nombre?>_LimpiarTabla()
                    {
                        try
                        {
                            var Tabla = document.getElementById("<?=$this->_nombre?>");
                            var Cant = Tabla.rows.length;
                            for(i=0; i<Cant; i++)
                            {
                                Tabla.deleteRow(i);
                            }
                        }
                        catch(e)
                        {
                            throw e;
                        }
                    }
                    function <?=$this->_nombre?>_getFila(index)
                    {
                        try
                        {
                            var Tabla = document.getElementById("<?=$this->_nombre?>").rows[index];
                            var arrValores = new Array();
                            var i=0;
                            for(i=0;i<Tabla.cells.length; i++)
                            {
                                var Columna = Tabla.cells[i];
                                arrValores[i] = document.getElementById(Columna.childNodes[0].id).value;
                            }
                            return arrValores;
                        }
                        catch(e)
                        {
                            throw e;
                        }
                    }
                    function <?=$this->_nombre?>_Buscar(valor)
                    {
                        try
                        {
                            var Tabla = document.getElementById("<?=$this->_nombre?>");
                            var i, j;
                            for(i=0; i<Tabla.rows.length;i++)
                            {
                                for(j=0; j<Tabla.rows[i].cells.length;j++)
                                {
                                    if(Tabla.rows[i].cells[j].childNodes.length>1)
                                    {
                                        if(document.getElementById(Tabla.rows[i].cells[j].childNodes[0].id).value == valor)
                                            return true;
                                    }
                                }
                            }
                            return false;
                        }
                        catch(e)
                        {
                            throw e;
                        }
                    }
                    function <?=$this->_nombre?>_ModificarFila()
                    {
                        try
                        {
                            var Argumentos = <?=$this->_nombre?>_ModificarFila.arguments;
                            var Tabla = document.getElementById("<?=$this->_nombre?>");
                            var Fila = Tabla.rows[Argumentos[0]];//index
                            var i=0;
                            for(i=0; i<Argumentos.length; i++)
                            {
                                var Columna = Fila.cells[i];
                                if(Columna.childNodes.length>1)
                                {
                                    document.getElementById(Columna.childNodes[0].id).value = Argumentos[i+1];
                                    document.getElementById(Columna.childNodes[1].id).innerHTML = Argumentos[i+1];
                                }
                            }
                        }
                        catch(e)
                        {
                            throw e;
                        }
                    }
                    function <?=$this->_nombre?>_AgregarFila()
                    {
                        try
                        {
                            var Argumentos = <?=$this->_nombre?>_AgregarFila.arguments;
                            var Tabla = document.getElementById("<?=$this->_nombre?>");
                            var CantidadFilas = Tabla.rows.length;
                            var Fila = Tabla.insertRow(CantidadFilas);
                            Fila.onmouseover=function(){<?=$this->_nombre?>IndexFilaSeleccionada = Fila.rowIndex;};
                            Fila.style.zIndex=9999;
                            var i=0;
                            for(i=0; i<Argumentos.length; i++)
                            {
                                var Columna = Tabla.rows[CantidadFilas].insertCell(i);
                                Columna.style.display = arrStyle[i]["Visible"];
                                var Text = document.createElement("input");
                                Text.type="hidden";
                                Text.id="txt_<?=$this->_nombre?>["+CantidadFilas+"]["+i+"]";
                                Text.name="txt_<?=$this->_nombre?>["+CantidadFilas+"]["+i+"]";
                                Text.value = Argumentos[i];
                                Columna.appendChild(Text);
                                var Span = document.createElement("span");
                                Span.id="span_<?=$this->_nombre?>["+CantidadFilas+"]["+i+"]";
                                Span.innerHTML = Argumentos[i];
                                Columna.appendChild(Span);
                            }
                            <?
                                foreach($this->_botones as $i=>$boton)
                                {
                                    ?>
                                        var Columna = Tabla.rows[CantidadFilas].insertCell(i);
                                        //Columna.innerHTML="<?=str_replace(chr(34),"'", $boton) ?>";
                                        var obj = document.createElement("<?=$boton["tipo"]?>");
                                        obj.id="<?=$boton["nombre"]?>";
                                        obj.title="<?=$boton["title"]?>";
                                        obj.onclick= function(){<?=$boton["funcion"]?>();};
                                        obj.innerHTML="<?=$boton["innerHTML"]?>";
                                        obj.style.cursor="pointer";
                                        Columna.appendChild(obj);
                                        i++;
                                    <?
                                }
                            ?>
                        }
                        catch(e)
                        {
                            throw e;
                        }
                    }
                </script>
            <?
        }
        private function setInicioTabla()
        {
            ?>
                <table border=0 style="border-top:1px solid black;border-bottom:1px solid black;border-left:1px solid black;border-right:1px solid black;" id="<?=$this->_nombre?>">
            <?
            if(count($this->_columnas)>0)
            {
                ?><tr><?
                foreach($this->_columnas as $columna)
                {
                    ?>
                        <td style="display:<?=($columna["Visible"] == true)?"":"none"?>;"  
                        <?
                            foreach($columna["Propiedades"] as $Propiedad)
                            {
                                echo $Propiedad." ";
                            }
                        ?>
                        ><?=$columna["Nombre"]?></td>
                    <?
                }
                ?><td colspan=<?=count($this->_botones)?>>&nbsp;</td></tr><?
            }
            
        }
        private function setFinTabla()
        {
            ?>
                </table>
            <?
        }
        public function show()
        {
            $this->setScripts();
            $this->setInicioTabla();
            $this->setFinTabla();
        }
    }
?>
