<?php
require_once ("../../xajax.inc.php");
$xajax = new xajax();
$xajax->registerFunction("Buscarusuario");
function Buscarusuario($IdUsuario)
{
	require_once("../../../Conexion/Conectar.php");
	$cnn = Conexion::nuevo();
	$cnn->Abrir();
	$cnn->setQuery("SELECT * FROM DatosUsuariosPersonales WHERE IdUsuario = ". $IdUsuario);
	$Tabla = $cnn->DevolverQuery();
	$cnn->Cerrar();
	$objResponse = new xajaxResponse();
	$objResponse->addAssign("txtNombre", "value", $Tabla[0]->Nombre);
	$objResponse->addAssign("txtApellido", "value", $Tabla[0]->Apellido);
	return $objResponse;
}
$xajax->processRequests();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html> 
	<head>
		<title>xajax Multiplier</title> 
		<?php $xajax->printJavascript('../../'); ?> 
	</head> 
	<body> 
		<table>
			<tr>
				<td>IdUsuario</td>
				<td><input type="text" name="txtIdUsuario" id="txtIdUsuario" value="" size="10" /></td>
			</tr>
			<tr>
				<td>Nombre</td>
				<td><input type="text" name="txtNombre" id="txtNombre" value="" size="20" disabled="disabled" /></td>
			</tr>
			<tr>
				<td>Apellido</td>
				<td><input type="text" name="txtApellido" id="txtApellido" value="" size="20" disabled="disabled" /> </td>
			</tr>
			<tr>				
				<td colspan="2"><input type="button" value="Buscar" onclick="xajax_Buscarusuario(document.getElementById('txtIdUsuario').value);return false;" /></td>
			</tr>			
		</table>
	</body> 
</html>