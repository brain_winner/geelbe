<?php
// multiply.php, multiply.common.php, multiply.server.php
// demonstrate a very basic xajax implementation
// using xajax version 0.2
// http://xajaxproject.org
require_once("../../../Clases/Conectar.php");
function BuscarUsuario($IdUsuario)
{
	$cnn = Conexion::nuevo();
	$cnn->Abrir();
	$cnn->setQuery("SELECT Nombre FROM DatosUsuariosPersonales WHERE IdUsuario = ".mysql_real_escape_string($IdUsuario));
	$Tabla = $cnn->DevolverQuery();
	$objResponse = new xajaxResponse();
	$objResponse->addAssign("txtNombre", "value", $Tabla[0]->Nombre);
	return $objResponse;
}

require("multiply.common.php");
$xajax->processRequests();
?>