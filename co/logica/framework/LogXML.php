<?
	/**
	*Log de Movimientos
	*@version 1.0.0.2 	
	*@author ATBK
	*@copyright 2007
	*/
	define("M_ERROR", 0);
	define("M_ALERTA", 1);
	define("M_MOVIMIENTO", 2);
	class Log
	{	
		function __construct($tipo)
		{
			$this->_directorio = null;
			$this->_nombreArchivo = "Logs.xml";
			$this->_usuario=null;
			$this->_descripcion=null;
			$this->_tipo = M_ERROR;
			$this->setTipo($tipo);
		}
		private $_id;
		private $_tipo;
		private $_descripcion;
		private $_usuario;		
		private $_directorio;
		private $_nombreArchivo;
		private $_archivo;
		private $_linea;
		
		/**
		*Version HTML para ver todos los Logs
		*/
		public function getVersionHTML()
		{
			$html = "";
			if ($this->getDirectorio() != "")
			{
				$doc = new DOMDocument("1.0","UTF-8");
				$doc->load($this->getDirectorio() . $this->getNombreArchivo());
				$root = $doc->documentElement;
				$html .="<table border='1' cellpadding='0' cellspacing='0'  style='width:100%'>
					<tr>
						<td colspan='100%' align='center'>Logs XML</td>
					</tr>
					<tr>
						<td>Id</td>
						<td>Fecha</td>
						<td>Usuario</td>
						<td>Archivo</td>
						<td>Descripcion</td>
						<td>Linea</td>
					</tr>";
				$html .="</tr>";
				$i=0;
				foreach($root->childNodes as $node)
				{					
					$at = $node->attributes;
					if ($i==0)
					{
						$html .="<tr bgcolor='#00CCFF'>";
						$i=1;	
					}
					else
					{
						$html .="<tr bgcolor='#FFFFFF'>";
						$i=0;	
					}					
					$html .="
						<td>".$at->getNamedItem("Id")->nodeValue."</td>
						<td>".$at->getNamedItem("Fecha")->nodeValue."</td>";					
					foreach ($node->childNodes as $n)
					{
						$html .="<td>".$n->nodeValue."</td>";
					}
					$html .="</tr>";
				}
				$html .="</table>";
			}
			return $html;			
		}
	 
		/**
		*Crea un movimiento en el Logs XML
		*/
		public function setMovimiento()
		{
			if (is_file($this->getDirectorio()))
			{
				$Id = $this->getUltimoId() + 1;				
				$this->_CrearLog($Id);
			}
			else
			{
				$this->_CrearPrimerLog();
			}
		}
		/**
		*Devuelve el ultimo Id del XML de Logs
		*/
		function getUltimoId()
		{
			$Id = 0;
			if ($this->getDirectorio() != "")
			{
				$doc = new DOMDocument("1.0","UTF-8");
				$doc->load($this->getDirectorio());
				$root = $doc->documentElement;
				foreach($root->childNodes as $node)
				{
					$at = $node->attributes;
					$Id = $at->getNamedItem("Id")->nodeValue;
				}				
			}
			return $Id;
		}
		/**
		*Agrega cada movimiento que se realiza
		*@access private
		*@param IdS  = Id del nuevo movimiento
		*/
		function _CrearLog($IdS)
		{
			$doc = new DOMDocument("1.0","UTF-8");
			$doc->load($this->getDirectorio());
			$Logs = $doc->documentElement;
			$this->_AgregarLog($Logs, $doc, $IdS);
		}
		function _AgregarLog($Logs, $doc, $Ids=1)
		{
			$Log = $doc->createElement("Log");
			$Logs->appendChild($Log);
			
			$Id = $doc->createAttribute("Id");
			$Id_Text=$doc->createTextNode(utf8_encode($Ids));
			$Id->appendChild($Id_Text);
			$Log->appendChild($Id);			
			
			$Fecha = $doc->createAttribute("Fecha");
			$Fecha_Text=$doc->createTextNode(utf8_encode(date("d/m/Y H:i")));
			$Fecha->appendChild($Fecha_Text);			
			$Log->appendChild($Fecha);			
			
			$Tipo = $doc->createAttribute("Tipo");
			switch($this->getTipo())
			{
				case 0:
					$Tipo_Text=$doc->createTextNode(utf8_encode("Error"));
					$Linea = $doc->createElement("Linea", $this->getLinea());
					$Log->appendChild($Linea);	
					$Archivo = $doc->createElement("Archivo", $this->getArchivo());
					$Log->appendChild($Archivo);				
				break;
				case 1:
					$Tipo_Text=$doc->createTextNode(utf8_encode("Alerta"));
				break;
				case 2:
					$Tipo_Text=$doc->createTextNode(utf8_encode("Movimiento"));
				break;
			}
			$Tipo->appendChild($Tipo_Text);			
			$Log->appendChild($Tipo);		
			
			$Usuario = $doc->createElement("Usuario", $this->getUsuario());
			$Log->appendChild($Usuario);
			
			if (strlen($this->getArchivo()) == 0)
				$this->setArchivo(utf8_encode($_SERVER["SCRIPT_FILENAME"]));
				
			$Archivo = $doc->createElement("Archivo", $this->getArchivo());
			$Log->appendChild($Archivo);
			
			$Descripcion = $doc->createElement("Descripcion", $this->getDescripcion());
			$Log->appendChild($Descripcion);			
			
			$doc->save($this->getDirectorio());
		}
		/**
		*Crea el XML si este no existia
		*@access private
		*/
		function _CrearPrimerLog()
		{
			$doc = new DOMDocument("1.0","UTF-8");
			$Logs = $doc->createElement("Logs");
			$doc->appendChild($Logs);			
			$this->_AgregarLog($Logs, $doc);			
		}
		/**
		*Devuelve el directorio en donde se guardara el XML de Logs
		*/
		public function getDirectorio()
		{
			return $this->_directorio;
		}
		/**
		*Setea el directorio en donde se guardara el XML de Logs
		*@param directorio
		*/
		public function setDirectorio($directorio)
		{
			$this->_directorio = $directorio;
		}
		/**
		*Devuelve el Usuario		
		*/
		public function getUsuario()
		{
			return $this->_usuario;
		}
		/**
		*Setea el Usuario
		*@param usuario string
		*/
		public function setUsuario($usuario)
		{
			$this->_usuario = utf8_encode($usuario);
		}
		/**
		*Devuelve la Descripcion		
		*/
		public function getDescripcion()
		{
			return $this->_descripcion;
		}
		/**
		*Setea la Descripcion
		*@param descripcion string
		*/
		public function setDescripcion($descripcion)
		{
			$this->_descripcion = utf8_encode($descripcion);
		}		
		/**
		*Devuelve el Tipo. El tipo sera: M_ERROR = 0, M_ALERTA = 1, M_MOVIMIENTO = 2		
		*/
		public function getTipo()
		{
			return $this->_tipo;
		}
		/**
		*Setea el Tipo . El tipo sera: M_ERROR = 0, M_ALERTA = 1, M_MOVIMIENTO = 2
		*@param tipo integer 0,1,2
		*/
		public function setTipo($tipo)
		{
			if (is_numeric($tipo) && $tipo >=0 && $tipo <= 2)
				$this->_tipo = $tipo;
		}
		/**
		*Devuelve el nombre del Archivo donde se ejecuto la accion		
		*/
		public function getArchivo()
		{
			return $this->_archivo;
		}
		/**
		*Setea el nombre del Archivo donde se ejecuta la accion
		*@param nombre string
		*/
		public function setArchivo($nombre)
		{
			$this->_archivo = utf8_encode($nombre);
		}
		/**
		*Devuelve la Linea		
		*/
		public function getLinea()
		{
			return $this->_linea;
		}
		/**
		*Setea la Linea
		*@param line integer
		*/
		public function setLinea($linea)
		{
			$this->_linea =$linea;
		}	
	}
?>