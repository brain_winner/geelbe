<?
    class XMLArray
    {
        private $_file="";
        private $_stack=array();
        /**
        * @desc Devuelve un XML al tipo Array
        * @var $XMLPath string. Dependiendo del tipo puede ser el Path de un xml o un xml string.
        * @var $Tipo string. Si es FILE en $XMLPath debe ingresarse el Path donde esta el xml. Caso contrario se puede ingresar simplemente un XML del tipo string.
        * @var encoding string.
        * @return array
        */
        public function XML2Array($XMLPath, $Tipo ="FILE", $encoding="UTF-8")
        {
            try
            {
                if($Tipo =="FILE")
                {
                   $this->_file = file_get_contents($XMLPath);
                }
                else
                {
                   $this->_file = $XMLPath;
                }
                $this->_stack=array();
                $xml_parser = xml_parser_create($encoding);
                xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, false );
                xml_set_object($xml_parser, &$this );
                xml_set_element_handler($xml_parser, "startTag", "endTag");
                xml_set_character_data_handler($xml_parser, "cdata");
                $data = xml_parse($xml_parser,$this->_file);
                if(!$data) 
                {
                    $Er = xml_error_string(xml_get_error_code($xml_parser));
                    $Li=xml_get_current_line_number($xml_parser);
                    throw new Exception("XML error: $Er at line $Li");
                }
                xml_parser_free($xml_parser);
                return $this->_stack;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function startTag($parser, $name, $attrs) 
        {
            try
            {
                $tag=array("name"=>$name,"attrs"=>$attrs);   
                array_push($this->_stack, $tag);   
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function cdata($parser, $cdata)
        {
            try
            {
                if(trim($cdata))
                {     
                    $this->_stack[count($this->_stack)-1]['cdata']=$cdata;    
                }
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function endTag($parser, $name) 
        {
            try
            {
                $this->_stack[count($this->_stack)-2]['children'][] = $this->_stack[count($this->_stack)-1];
                array_pop($this->_stack);
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
    }
?>
