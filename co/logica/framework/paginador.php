<?
     class Paginador
     {
         private $_filtro=false;
         private $_palabreclave="";
         private $_tope=20;
         private $_posicion=0;
         private $_paginador = array(10,25,50,100);
         private $_total = 0;
         private $_MAX_X_PAG=20;
         private $_PAG=1;
         private $_CANTPAGINAS=0;
         private $_DataSource = array();                                 
         private $_nombre="Paginador";
         private $_action;
         private $_seleccion=-1;
         
          public function show(){
	         if ($this->_CANTPAGINAS > 1)
	         {
	             $this->_script();
	             ?><table style="width:100%"><tr><?
	             $this->_InicioForm();
	             if($this->_filtro)
	             {
	                 $this->_Filtrar();
	                 ?></tr><tr><?
	             }
	             $this->_ComboPaginador();
	             $this->_FinForm();
	             ?></tr></table><?
	         }
         }
         public function Paginador($Posicion,array $DataSource,$action,$filtro=false,$nombre="Paginador")
         {
             $this->_DataSource=$DataSource;
             $this->_total = count($this->_DataSource);
             $this->_nombre=$nombre;
             $this->_action=$action;
             $this->_posicion = $Posicion;
             $this->_filtro = $filtro;
             $this->_CANTPAGINAS = ceil(count($this->_DataSource) / $this->_MAX_X_PAG);     
         }
         private function _script()
         {
             ?>
                <script>
                    var <?=$this->_nombre?>Tope = <?=$this->_tope?>;
                    function <?=$this->_nombre?>LimpiarBusqueda()
                    {
                        document.getElementById('<?=$this->_nombre?>txtFiltro').value=''; 
                        <?=$this->_nombre?>PalabraClave(this.value);
                        document.getElementById("PAG").value = 1;
                    }
                    function <?=$this->_nombre?>SeleccionarPagina_enComboPaginas(pagina)
                    {
                        document.getElementById("cbxPaginas<?=$this->_nombre?>").value = pagina;
                    }
                    function <?=$this->_nombre?>CambiarPagina(nropagina)
                    {
                        document.getElementById("PAG").value = nropagina;
                        document.getElementById("frm<?=$this->_nombre?>").submit();
                    }
                    function <?=$this->_nombre?>CantidadPaginas(Valor)
                    {
                        document.getElementById("MAX_X_PAG").value = Valor;
                        document.getElementById("PAG").value = 1;
                        document.getElementById("frm<?=$this->_nombre?>").submit();
                    }
                    function <?=$this->_nombre?>Pagina(Valor)
                    {
                        document.getElementById("POSICION").value = parseInt(document.getElementById("POSICION").value) + parseInt(Valor);
                        document.getElementById("PAG").value = 1 + (<?=$this->_nombre?>Tope * parseInt(document.getElementById("POSICION").value));
                        document.getElementById("frm<?=$this->_nombre?>").submit();
                    }
                    function <?=$this->_nombre?>PalabraClave(Valor)
                    {
                        document.getElementById("PALABRA_CLAVE").value = Valor;
                        document.getElementById("PAG").value = 1;
                        document.getElementById("frm<?=$this->_nombre?>").submit();
                    }       
                </script>
             <?
         }
         private function _InicioForm()
         {
             ?>
                <form name="frm<?=$this->_nombre?>" id="frm<?=$this->_nombre?>" action="<?=$this->_action?>" method="post">
                <input type="hidden" id="PAG" name="PAG" value="<?=$this->_PAG?>" />
                <input type="hidden" id="MAX_X_PAG" name="MAX_X_PAG" value="<?=$this->_MAX_X_PAG?>" />
                <input type="hidden" id="POSICION" name="POSICION" value="<?=$this->_posicion?>" />
                <input type="hidden" id="PALABRA_CLAVE" name="PALABRA_CLAVE" value="<?=$this->_posicion?>" />
             <?
         }
         private function _FinForm()
         {
             if($this->_seleccion > -1)
             {
                 ?><script><?=$this->_nombre?>SeleccionarPagina_enComboPaginas(<?=$this->_seleccion?>)</script><?
             }
             ?>         
                </form>
             <?
         }
         private function _ComboPaginador()
         {
             ?>
                 <td width="100%" colspan="2">Ver  <select id="cbxPaginas<?=$this->_nombre?>" name="cbxPaginas<?=$this->_nombre?>" onChange="<?=$this->_nombre?>CantidadPaginas(this.value)">
                 <option value="<?=$this->_total?>">Todos</option>
             <?                   
             foreach($this->_paginador as $pag)
             {
                 if ($this->_MAX_X_PAG == $pag)
                 {
                     ?>
                        <option selected value="<?=$pag?>"><?=$pag?></option>
                     <?
                 }
                 else
                 {
                     ?>                                             
                        <option value="<?=$pag?>"><?=$pag?></option>
                     <?
                 }
             }
             ?>
                 </select>&nbsp;de&nbsp;<?=count($this->_DataSource)?>&nbsp;
             <?
             $this->_Paginas();
             ?></td>
         <?
         }
         private function _Paginas()
         {
             try
             {
                 $newArray = array();
                 if ($this->_MAX_X_PAG)
                 {
                     $this->_CANTPAGINAS = count($this->_DataSource) / $this->_MAX_X_PAG;
                     $this->_CANTPAGINAS = ceil($this->_CANTPAGINAS);
                     $Inicio = 1 + ($this->_posicion * $this->_tope);
                     $Fin = (($this->_CANTPAGINAS < $this->_tope)?$this->_CANTPAGINAS:$this->_tope) + ($this->_posicion * $this->_tope);
                     ?>
                        <!--<a <?=($this->_posicion > 0 && $this->_CANTPAGINAS < $this->_tope)?"href=\"javascript:".$this->_nombre."Pagina(-1)\"":""?> title="Primera pagina">Primera</a>-->
                        <?
                            if($Fin)
                            {
                        ?>
                        <a href="javascript:;" onclick="javascript:<?=$this->_nombre?>CambiarPagina(1)">Primera</a>
                        <?
                            }
                        ?>
                        &nbsp;
                        <?
                            for($i=$Inicio; $i<=$Fin; $i++)
                            {
                                ?><a <?=($i != $this->_PAG)?"href=\"javascript:;\" onclick=\"javascript:".$this->_nombre."CambiarPagina(".$i.")\"":""?>><?=$i?></a> &#160;<?
                            }
                        ?>
                        <!--<a <?=($this->_posicion < $this->_tope-1 && $this->_CANTPAGINAS > $this->_tope)?"href=\"javascript:;\" onclick=\"javascript:".$this->_nombre."Pagina(1)\"":""?> title="Ultima pagina">&Uacute;ltima</a>-->
                        <?
                        if ($Fin > 0)
                        {
                            ?>
                            <a href="javascript:;" onclick="javascript:<?=$this->_nombre?>CambiarPagina(<?=$Fin?>)">&Uacute;ltima</a>
                         <?
                        }
                 }
                 else
                 {
                     $this->_MAX_X_PAG = count($this->_DataSource);
                     $this->_PAG = 1;
                 }
             }
             catch(exception $e)
             {
                 throw $e;
             }
         }
         private function _Filtrar()
         {
             ?>
                <td width="5%">Buscar:</td>
                <td width="95%">
                <input type="text" id="<?=$this->_nombre?>txtFiltro" name="<?=$this->_nombre?>txtFiltro" maxlength="255" style="width:60%" value="<?=$this->_palabreclave?>" />&nbsp;
                <input type="button" id="<?=$this->_nombre?>btnBuscar" name="<?=$this->_nombre?>btnBuscar" onclick="<?=$this->_nombre?>PalabraClave(this.value)" value="Buscar"/>
                <input type="button" id="<?=$this->_nombre?>btnLimpiar" name="<?=$this->_nombre?>btnLimpiar" onclick="<?=$this->_nombre?>LimpiarBusqueda();" value="Ver todo" />
                </td>
             <?
         }
         public function setPagina($pagina)
         {
             $this->_PAG = $pagina;
         }
         public function getPagina()
         {
             return $this->_PAG;
         }
         public function setMaxXPag($maxXpag)
         {
             $this->_MAX_X_PAG = $maxXpag;
         }
         public function getMaxXPag()
         {
             return $this->_MAX_X_PAG;
         }
         public function setSeleccion($valor)
         {
             $this->_seleccion=$valor;
         }
         public function setPalabraClave($valor)
         {
             $this->_palabreclave=$valor;
         }
         public function getArrayPaginado()
         {
             try
             {
                 if (!$this->_DataSource) 
                        $FINFOR=0;
                    else if(count($this->_DataSource)<($this->_MAX_X_PAG* $this->_PAG))
                    {
                        $FINFOR = count($this->_DataSource);
                    }
                    else
                    {
                        $FINFOR = ($this->_MAX_X_PAG * ($this->_PAG-1)) + $this->_MAX_X_PAG;
                    }
                    $newArray = array();
                    for($i=($this->_MAX_X_PAG * ($this->_PAG-1)); $i<$FINFOR; $i++)
                    {
                        array_push($newArray, $this->_DataSource[$i]);
                    }
                    return $newArray;
             }
             catch(exception $e)
             {
                 throw $e;
             }
         }
     } 
?>
