<?
    class Texto
    {
        private $_filename;
        private $_text;
        
        /**
        * @desc Clase que lee y altera un archivo de texto.
        * @param string. Ruta del archivo de texto.
        */
        public function __construct($NombreArchivo)
        {
            try
            {
                $this->_filename = $NombreArchivo;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        /**
        * @desc Asigna el texto del archivo a la propiedad text.
        */
        public function Read()
        {
            try
            {
                $p = fopen($this->_filename, "r");
                $this->_text = fread($p, filesize($this->_filename));
                fclose($p);                
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        /**
        * @desc Reemplaza un string por otro.
        */
        public function ReemplazarString($oldString, $newString)
        {
            try
            {
                $this->_text = str_replace($oldString, $newString, $this->_text);
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
       /**
        * @desc Reemplaza un string por otro.
        */
        public function ReemplazarStringPlano($oldString, $newString)
        {
            try
            {
                $this->_text = str_replace($oldString, $newString, $this->_text);
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        /**
        * @desc Obtiene la propiedad text.
        */
        public function getText()
        {
            try
            {
                return $this->_text;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        
    }  
?>
