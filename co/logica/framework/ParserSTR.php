<?php
	/**
	*Clase ParserSTR
	* @author ATBK
	* @copyright 2007
	*/
	class ParserSTR
	{
		function ParserSTR(){}
		/**
		*Convierte " de un string a \"
		*@param string a cambiar
		*/
		function getString_a_MySQL($string)
		{
			$Aux = $string;
			$AuxSinComillas = str_replace(chr(34), chr(92).chr(34), $Aux);
			return $AuxSinComillas;
		}
		/**
		*Convierte \" de un string a "
		*@param string a cambiar
		*/
		function getString_a_PHP($string)
		{
			$Aux = $string;
			$AuxSinComillas = str_replace(chr(92).chr(34), chr(34), $Aux);
			return $AuxSinComillas;
		}
		/**
		*Transforma una fecha UNIX numerica al formato date del tipo string
		*@param long date a cambiar
		*@param string formato con el formato a devolver
		*/
		function getDateUNIX_a_MySQL($date, $formato="Y/m/d")
		{
			if (is_numeric($date))
				return date($formato,$date);
			else
				return null;
		}
		/**
		*Transforma una fecha STRING con formato DMY al formato date del tipo string
		*@param string date a cambiar
		*@param string formato con el formato a devolver
		*/
		function getDateDMY_a_MySQL($date, $formato="Y/m/d")
		{
			if (str_word_count($date,0,"/") > 0)
				$FechaAux = explode("/",$date);
			elseif (str_word_count($date,0,"-") > 0)
				$FechaAux = explode("-",$date);
			else
				return null;
			$AuxDate = mktime(0,0,0,$FechaAux[1], $FechaAux[0], $FechaAux[2]);
			return date($formato, $AuxDate);
		}
		/**
		*Transforma una fecha STRING con formato YMD al formato date del tipo string
		*@param string date a cambiar
		*@param string formato con el formato a devolver
		*/
		function getDateYMD_a_MySQL($date, $formato="Y/m/d")
		{
			if (str_word_count($date,0,"/") > 0)
				$FechaAux = explode("/",$date);
			elseif (str_word_count($date,0,"-") > 0)
				$FechaAux = explode("-",$date);
			else
				return null;
			$AuxDate = mktime(0,0,0,$FechaAux[1], $FechaAux[2], $FechaAux[0]);
			return date($formato, $AuxDate);
		}
		/**
		*Transforma una fecha STRING con formato MDY al formato date del tipo string
		*@param string date a cambiar
		*@param string formato con el formato a devolver
		*/
		function getDateMDY_a_MySQL($date, $formato="Y/m/d")
		{
			if (str_word_count($date,0,"/") > 0)
				$FechaAux = explode("/",$date);
			elseif (str_word_count($date,0,"-") > 0)
				$FechaAux = explode("-",$date);
			else
				return null;
			$AuxDate = mktime(0,0,0,$FechaAux[0], $FechaAux[1], $FechaAux[2]);
			return date($formato, $AuxDate);
		}
		/**
		*Transforma una fecha con hora STRING con formato DMY h:m al formato date del tipo string
		*@param string date a cambiar
		*@param string formato con el formato a devolver
		*/
		function getTimeDMY_a_MySQL($date, $hora, $formato="Y/m/d h:m:s")
		{
			if (str_word_count($date,0,"/") > 0)
				$FechaAux = explode("/",$date);
			elseif (str_word_count($date,0,"-") > 0)
				$FechaAux = explode("-",$date);
			else
				return null;
			$HoraAux = explode(":",$hora);
			$AuxDate = mktime($HoraAux[0],$HoraAux[1],0,$FechaAux[1], $FechaAux[0], $FechaAux[2]);
			return date($formato, $AuxDate);
		}
		/**
		*Transforma una fecha con hora STRING con formato YMD h:m  al formato date del tipo string
		*@param string date a cambiar
		*@param string formato con el formato a devolver
		*/
		function getTimeYMD_a_MySQL($date, $formato="Y/m/d h:m:s")
		{
			if (str_word_count($date,0,"/") > 0)
				$FechaAux = explode("/",$date);
			elseif (str_word_count($date,0,"-") > 0)
				$FechaAux = explode("-",$date);
			else
				return null;
			$HoraAux = explode(":",$hora);
			$AuxDate = mktime($HoraAux[0],$HoraAux[1],0,$FechaAux[1], $FechaAux[2], $FechaAux[0]);
			return date($formato, $AuxDate);
		}
		/**
        *Transforma una fecha hora STRING con formato MDY h:m al formato date del tipo string
        *@param string date a cambiar
        *@param string formato con el formato a devolver
        */
        function getTimeMDY_a_MySQL($date, $formato="Y/m/d h:m:s")
        {
            if (str_word_count($date,0,"/") > 0)
                $FechaAux = explode("/",$date);
            elseif (str_word_count($date,0,"-") > 0)
                $FechaAux = explode("-",$date);
            else
                return null;
            $HoraAux = explode(":",$hora);
            $AuxDate = mktime($HoraAux[0],$HoraAux[1],0,$FechaAux[0], $FechaAux[1], $FechaAux[2]);
            return date($formato, $AuxDate);
        }
        /**
        *Transforma una fecha con hora STRING con formato DMY h:m al formato UNIX
        *@param string date a cambiar
        *@param string formato con el formato a devolver
        */
        function getTimeDMY_a_UNIX($date, $hora)
        {
            if (str_word_count($date,0,"/") > 0)
                $FechaAux = explode("/",$date);
            elseif (str_word_count($date,0,"-") > 0)
                $FechaAux = explode("-",$date);
            else
                return null;
            $HoraAux = explode(":",$hora);
            $AuxDate = mktime($HoraAux[0],$HoraAux[1],0,$FechaAux[1], $FechaAux[0], $FechaAux[2]);
            return date($formato, $AuxDate);
        }
        /**
        * @desc 
        */
        function Date_to_timestamp($fecha)
        {
            $datetime = explode(" ", $fecha);
            $fecha = explode("/", $datetime[0]);
            $hora = explode(":", $datetime[1]);
             $AuxDate = mktime($hora[0], $hora[1], 0, $fecha[1], $fecha[0], $fecha[2]);        
             return $AuxDate;     
        }
        /**
        * @desc 
        */
        function Timestamp_to_date($timestamp, $formato = "d/m/Y H:i")
        {
            return date($formato, $timestamp);
        }
	}
?>