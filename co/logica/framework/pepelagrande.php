<?php
	
	try
	{
	    $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("mispedidos", "clases"));
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("emails"));
	    Aplicacion::CargarIncludes(Aplicacion::getIncludes("campanias"));
		set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
		require_once 'Zend/Cache.php'; 
	}
	catch(exception $e)
	{
	    die(print_r($e));
	}
	
		try
	{
	$pedidos = array(67449,67451,67454,67455,67464,67467,67471,67470,67473,67474,67475,67479,67483,67484,67485,67486,67487,67488,67489,67490,67491,67493,67494,67497,67500,67502,67480,67506,67507,67508,67509,67510,67512,67516,67522,67524,67526,67528,67529,67530,67532,67534,67536,67537,67538,67539,67540,67543,67545,67550,67552,67553,67554,67555,67556,67559,67560,67562,67563,67566,67567,67568,67569,67572,67573,67577,67579,67580,67581,67583,67584,67585,67587,67596,67598,67600,67602,67603,67605,67606,67611,67613,67615,67617,67619,67621,67541,67623,67551,67625,67626,67610,67627,67628,67629,67632,67634,67635,67636,67637,67642,67643,67644,67647,67557,67648,67650,67651,67653,67657,67661,67663,67664,67665,67666,67668,67447,67453,67476,67608,67612,67624);

	foreach($pedidos as $pedido) {
	$TRX_ID = $pedido;
	echo "procesando pedido nro.: ".$TRX_ID."<br />";
			try
			{
	            $actualizador = actualizarPedidos::nuevo($TRX_ID, 2);
    	        	$actualizador->Actualizar();
	    	        Conexion::nuevo()->Cerrar_Trans();
	    	 
    	        $pedido = dmPedidos::getPedidoById($TRX_ID);
       
			    	//Invalido la cache del credito de usuario
	                $cacheDir = Aplicacion::getParametros("cache_configuration", "cache_directory");
			        $cache = Zend_Cache::factory('Output', 'File', array('lifetime' => null), array('cache_dir' => $cacheDir));
			        $cache->remove('cache_credito_usuario_'.$pedido->getIdUsuario());
	            
			        // Invalido la cache del padrino
			        $objUsuario = dmUsuario::getByIdUsuario($pedido->getIdUsuario());
	    	        $nombrePadrino = $objUsuario->getPadrino();
	    	        if($nombrePadrino && $nombrePadrino != "Huerfano") {
		    	      $idPadrino = dmUsuario::getIdByNombreUsuario($nombrePadrino);
			          $cache->remove('cache_credito_usuario_'.$idPadrino);
	    	        }
	    	        
		         $camp = dmCampania::getByIdCampania($pedido->getIdCampania());
					$u = dmUsuario::getByIdUsuario($pedido->getIdUsuario());
					$email = dmPedidos::getDatosMailByIdPedido($pedido->getIdPedido());
					
	$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	$mesFn = $meses[date("n", strtotime($email["TiempoEntregaFn"])) - 1];
	$mesIn = $meses[date("n", strtotime($email["TiempoEntregaIn"])) - 1];
		
					$datos = array(
						'NumeroPedido' => $pedido->getIdPedido(),
						'SistemaPago' => 'PagosOnline',
						'NombreVidriera' => $camp->getNombre(),
						'IdUsuario' => $pedido->getIdUsuario(),
						'HashIdUsuario' => Aplicacion::Encrypter($pedido->getIdUsuario()),
						'NombreUsuario' => $u->getDatos()->getNombre(),
						'ApellidoUsuario' => $u->getDatos()->getApellido(),
						"TiempoEntregaFn" => date("d", strtotime($email["TiempoEntregaFn"]))." de ".$mesFn,
						"TiempoEntregaIn" => date("d", strtotime($email["TiempoEntregaIn"]))." de ".$mesIn
					);
					
					emailPagoAceptado($u->getDatos()->getEmail(), $datos);
	                      
			}
			catch(FaltaSockException $e){
				$actualizador = actualizarPedidos::nuevo($TRX_ID, 10);
				$actualizador->Actualizar();
			}                         	
		
		
	}
	
		}
	catch(exception $e)
	{
	    die(print_r($e));
	}
	   
	
?>