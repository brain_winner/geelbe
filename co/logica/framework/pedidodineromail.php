<?php
    class ReportePedidosDM
    {
        private $_cuenta="";
        private $_email="";
        private $_pin="";
        private $_fechainicio="";
        private $_fechafin="";
        private $_xml=1;
        private $_url = "https://www.dineromail.com/co/vender/ConsultaPago.asp";
        private $_xmlpath="";
        private $_array_xml = array();
        /**
        * @desc Constructor
        * @var $email string.
        * @var $cuenta string.Sin la barra invertida. Solo los digitos.
        * @var $pin string
        * @var $FechaInicio string.
        * @var $FechaFin string.
        */
        public function ReportePedidosDM($email, $cuenta, $pin)
        {
            try
            {
                $this->_email = $email;
                $this->_cuenta = $cuenta;
                $this->_pin = $pin;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function sendConsulta($FechaInicio=null, $FechaFin=null)
        {
            try
            {
                $this->_fechainicio=($FechaInicio != null)?$FechaInicio:date("Ymd", time()-2505600);
                $this->_fechafin=($FechaFin != null)?$FechaFin:date("Ymd", time());
                $this->_xmlpath = file_get_contents($this->getUrl());

		        if(strlen($this->_xmlpath)<=0)
                    throw new Exception("Ha ocurrido un error. No se pudo leer la url especificada");
                $objXML2Array = new XMLArray();
                
                $this->_array_xml = $objXML2Array->XML2Array($this->_xmlpath, "");

                switch ($this->CheckEstado())
                {
                    case 0:
                        throw new Exception("los parámetros no respetan el formato requerido.");
                    break;
                    case 2:
                        throw new Exception("los valores son incorrectos para realizar la consulta.");
                    break;
                }
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        /**
        * @desc Devuelve una URL de consulta.
        * @return string
        */
        private function getUrl()
        {
            try
            {
                 //= http_build_query(array("Email"=>$this->_email, "Acount"=>$this->_cuenta, "Pin"=>$this->_pin, "StartDate"=>$this->_fechainicio, "EndDate"=>$this->_fechafin, "XML"=>1));
                $Get = $this->_url ."?Email={$this->_email}&Acount={$this->_cuenta}&Pin={$this->_pin}&StartDate={$this->_fechainicio}&EndDate={$this->_fechafin}&XML={$this->_xml}";
                 //exit($Get);
                  return $Get;
               
	//			return "http://www.geelbe.com/".Aplicacion::getDirLocal()."DineroMail/DineroMailXML.xml";
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function CheckEstado()
        {
            return $this->_array_xml[0]["children"][6]["cdata"];
        }
        public function getPayByIdTRX($IdTRX)
        {
            try
            {
                if(!isset($this->_array_xml[0]["children"][7]["children"])){
                    //throw new Exception("No hay pagos disponibles");
                    return false;
                }
                foreach ($this->_array_xml[0]["children"][7]["children"] as $Pay)
                {
                    if(isset($Pay["attrs"]["Trx_id"]) && $Pay["attrs"]["Trx_id"] == $IdTRX)
                    return $Pay;
                }
                return false;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function getPays()
        {
            try
            {
                if(!isset($this->_array_xml[0]["children"][7]["children"]))
                    throw new Exception("No hay pagos disponibles");
                return $this->_array_xml[0]["children"][7]["children"];
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function checkCupones(){
        	$xml = new SimpleXMLElement($this->_xmlpath);
        	$arrayCupones = array();
        	foreach($xml->Collections->Collection as $collection){
        		array_push($arrayCupones, $collection["Trx_id"]);
        	}
        	return $arrayCupones;
        	        	        	
        }        
    }
?>
