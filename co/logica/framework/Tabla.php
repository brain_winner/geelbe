<?php
    define("TABLA_FILA_CURSOR", "default");
    define("TABLA_FILA_IMPAR_COLOR", "#99ccff");
    define("TABLA_FILA_PAR_COLOR", "#ccffff");
    define("TABLA_FILA_SELECCIONADA", "#3399ff");
    define("TABLA_FILA_HOVER", "gray");
    define("TABLA_FILA_ALTO", "30px");
    define("TABLA_FILA_BORDE", "1px solid");
    define("TABLA_FILA_TEXT_ALIGN", "center");
    
    define("TABLA_COLUMNA_CURSOR", "pointer");
    define("TABLA_COLUMNA_COLOR", "#00CCCC");
    define("TABLA_COLUMNA_SELECCIONADA", "#3399ff");
    define("TABLA_COLUMNA_HOVER", "gray");
    define("TABLA_COLUMNA_ALTO", "30px");
    define("TABLA_COLUMNA_BORDE", "1px solid");
    define("TABLA_COLUMNA_TEXT_ALIGN", "center");
    
    define("TABLA_PAGINADO_TEXT_DECORATION", "underline");
    define("TABLA_PAGINADO_CURSOR", "pointer");
    define("TABLA_PAGINADO_SIN_TEXT_DECORATION", "none");
    define("TABLA_PAGINADO_SIN_CURSOR", "default");
    define("TABLA_PAGINADO_COLOR", "#00CCCC");
    
    class Tabla
    {
       
        private $_columnas=array();
        private $_filas=array();
        private $_nombre="dt";
        private $_paginacion=10;
        private $_paginaNro=0;
        private $_columnaOrdenarNro=0;
        private $_columnaOrdenarSentido="DESC";
        private $_totalRegistros=0;
        private $_palabraclave = "";
        private $_dt=array();
        private $_pagina="";
        private $_botones=array();
        private $_propiedades=array();
        private $_parametros=array();
        private $_forma=array();
        private $_formatitulo = array();
        private $_enableFiltro  = true;
        private $_enablePaginacion = true;
        private $_enableTamanioPaginas = true;
		private $_paginaReal = null;
		private $_mostrarFiltros = true;
        
        public function Tabla(array $dt, $paginacion=10, $nombre="dt")
        {
            try
            {
                $this->_dt = $dt;
                $this->_nombre = $nombre;
                $this->_paginacion=$paginacion;
                $this->_totalRegistros = count($dt);
                if(count($this->_dt)>0)
                {
                    foreach($this->_dt[0] as $clave => $valor)
                    {
                        $this->_columnas[] = $clave;
                    }
                    
                    $i=0;
                    foreach($this->_dt as $fila)
                    {
                        $j=0;
                        foreach($fila as $key => $value)
                        {
                            $this->_filas[$i][$j] = $value;
                            $j++;
                        }
                        $i++;
                    }
                }
				
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
		
		public function setStartValue($startValue = 0)
        {
            $this->_paginaReal = ($startValue/$this->_paginacion);
        }
		
		public function setMostrarFiltros($mostrarFiltros = true)
        {
            $this->_mostrarFiltros = $mostrarFiltros;
        }
		
		
		public function setTotalRegistros($totalRegistros)
        {
            $this->_totalRegistros = $totalRegistros;
        }
        public function setEnableTamanioPaginacion($estado)
        {
            $this->_enableTamanioPaginas = $estado;
        }
        public function setEnablePaginacion($estado)
        {
            $this->_enablePaginacion = $estado;
        }
        public function setEnableBuscador($estado)
        {
            $this->_enableFiltro = $estado;
        }
        public function setPalabraClave($valor)
        {
            $this->_palabraclave = $valor;
        }
        public function setFormaTitulo($index, $forma)
        {
            $this->_formatitulo[$index] = $forma;
        }
        public function setFormaHTML($index, $forma)
        {
            $this->_forma[$index] = $forma;
        }
        public function setParametro($parametro)
        {
            $this->_parametros[] = $parametro;
        }
        public function setPropiedadesColumna($index, array $Propiedad)
        {
            $this->_propiedades[$index]=$Propiedad;
        }
        public function addBotones($Boton)
        {
            $this->_botones[]=$Boton;
        }
        public function setPaginaNro($paginaNro)
        {
            try
            {
                $this->_paginaNro = $paginaNro;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function getPaginaNro()
        {
            try
            {
                return $this->_paginaNro;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function setPaginacion($paginacion=10)
        {
            $this->_paginacion = $paginacion;
        }
        public function setPagina($pagina)
        {
            try
            {
                $this->_pagina = $pagina;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function getPagina()
        {
            try
            {
                return $this->_pagina;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function setColumnaNro($columnaOrdenarNro)
        {
            try
            {
                $this->_columnaOrdenarNro = $columnaOrdenarNro;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function getColumnaNro()
        {
            try
            {
                return $this->_columnaOrdenarNro;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function setColumnaSentido($columnaOrdenarSentido)
        {
            try
            {
                $this->_columnaOrdenarSentido = $columnaOrdenarSentido;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function getColumnaSentido()
        {
            try
            {
                return $this->_columnaOrdenarSentido;
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function show()
        {
            try
            {
                $this->style();
                $this->scripts();
                //$this->comienzo_Form();
                $this->comienzo_Tabla();
                if(count($this->_filas)>0)
                {
                    $this->show_Columnas();
                    $this->show_Filas();
                    $this->showPaginacion();
                }   
                else {
                    $this->show_FilasVacias();
				}
                
				if ($this->_mostrarFiltros) {
					$this->showFiltrar();
				}
				
                $this->fin_Tabla();
                //$this->fin_Form();
            }
            catch(exception $e)
            {
                throw $e;
            }
        }

        private function comienzo_Form()
        {
            try
            {
                ?><form id="frm<?=$this->_nombre;?>" name="frm<?=$this->_nombre;?>"><?
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function comienzo_Tabla()
        {
            try
            {
                ?><table class="table_tabla" style="width:100%" cellpadding="0" cellspacing="0"><?
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function fin_Tabla()
        {
            try
            {
                ?></table><?
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function fin_Form()
        {
            try
            {
                ?></form><?
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function show_Columnas()
        {
            try
            {
                ?><tr><?
                foreach($this->_columnas as $key => $columna)
                {
                    $prop="";
                    if(is_array($this->_propiedades[$key]))
                        foreach($this->_propiedades[$key] as $propiedad)
                        {
                            $prop .= $propiedad;
                        }
                    ?>
                    <?
                        if ($this->_formatitulo[$key] != "")
                        {
                            ?>
                            <td class="td_columna" <?=$prop?>>
                            <?
                             echo $this->_formatitulo[$key];?>
                            </td>
                            <?
                        }
                        else
                        {
                    ?>
                        <td class="td_columna" onclick="<?=$this->_nombre;?>_ordenarColumna(<?=$key?>);" <?=$prop?>>
                            <?=$columna?>
                        </td>
                    <?
                        }
                }
                if(count($this->_botones)>0)
                {
                    ?><td class="td_columna" colspan="<?=count($this->_botones)?>">&nbsp;</td><?
                }
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function show_Filas()
        {
            try
            {
                $desde = $this->_paginaNro * $this->_paginacion;
                $hasta = $this->_paginacion * ($this->_paginaNro + 1);
                for($i=$desde ; $i < $hasta ; $i++)
                {
                    ?>
                        <tr id="fila_<?=$i?>"
                    <?
                    $j=0;
                    if(isset($this->_filas[$i]))
                    {
                        ?>onclick="<?=$this->_nombre;?>_filaClick(this.id)" class="tr_<?= $i % 2 ?>"><?
                        foreach($this->_filas[$i] as $tipo => $valor)
                        {
                            $prop="";
                            if(is_array($this->_propiedades[$j]))
                                foreach($this->_propiedades[$j] as $propiedad)
                                {
                                    $prop .= $propiedad;
                                }
                            ?>
                                <td class="td_fila" <?=$prop?>>
                                <?
                                    if($this->_forma[$j][0])
                                    {
                                        $filaNo = $this->_forma[$j][0];
                                        $fila = str_replace("%".$this->_forma[$j][1]."%", ${$this->_forma[$j][1]}, $filaNo);
                                        echo $fila;
                                        ?>
                                            <input class="datos_text" style="display:none" readonly="readonly" type="<?=$tipo?>" id="f[<?=$i?>][<?=$j?>]" name="f[<?=$i?>][<?=$j?>]" value="<?=$valor?>"/>
                                        <?
                                    }
                                    else
                                    {
                                        ?>
                                            <input class="datos_text" readonly="readonly" type="<?=$tipo?>" id="f[<?=$i?>][<?=$j?>]" name="f[<?=$i?>][<?=$j?>]" value="<?=$valor?>" />
                                        <?
                                    }
                                ?>
                                </td>
                            <?                            
                            $j++;
                        }
                        foreach($this->_botones as $boton)
                        {
                            ?><td align="center" class="td_fila"><?=$boton?></td><?
                        }
                        ?>
                            </tr>
                        <?
                    }
                    else
                    {
                        ?>class="tr_<?= $i % 2 ?>"><?
                        for($c=0; $c<count($this->_columnas)+count($this->_botones); $c++)
                        {
                            $prop="";
                            if(is_array($this->_propiedades[$c]))
                                foreach($this->_propiedades[$c] as $propiedad)
                                {
                                    $prop .= $propiedad;
                                }
                            ?><td <?=$prop?> class="td_fila_vacia">&nbsp;</td><?
                        }
                    }
                }   
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function show_FilasVacias()
        {
            try
            {
                $desde = $this->_paginaNro * $this->_paginacion;
                $hasta = $this->_paginacion * ($this->_paginaNro + 1)+2;
                $mitad = $hasta/2;
                for($i=$desde ; $i < $hasta ; $i++)
                { 
                    ?>
                        <tr id="fila_<?=$i?>">
                            <td colspan="100%" class="td_fila_vacia2" valign="middle" align="center"><?=($mitad == $i)?"No hay registros":"&nbsp;"?></td>
                        </tr>
                    <?
                }   
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function getParametersURL()
        {
            for($i=0; $i<count($this->_parametros); $i++)
            {
                $cadena = $cadena . "&" . $this->_parametros[$i];
            }
            return $cadena;
        }
        private function scripts()
        {
            try
            {
                ?>
                  <script>
                        var ultimaFilaSeleccionadaNro = -1;
                        var ultimaFilaClick = "";
                        var paginaActual = "<?=$this->_pagina?>";
						<? if ($this->_paginaReal == null) { ?>
                        var paginaNroActual=<?=$this->_paginaNro?>;
						<? } else { ?>
						var paginaNroActual=<?=$this->_paginaReal?>;
						<? } ?>
                        var columnaSentido="<?=$this->_columnaOrdenarSentido?>";
                        var columnaOrdenar=<?=$this->_columnaOrdenarNro?>;
                        var palabraClave = "<?=$this->_palabraclave?>";
                        var tamanioPagina = "<?=$this->_paginacion?>";
                        
                        function <?=$this->_nombre?>_Paginacion()
                        {
                            tamanioPagina = document.getElementById("<?=$this->_nombre?>_cbxPaginas").value;
                            window.location.href = <?=$this->_nombre?>_Url();
                        }
                        function <?=$this->_nombre?>_Filtrar()
                        {
                            palabraClave = document.getElementById("<?=$this->_nombre?>_txtBuscar").value;
                            window.location.href = <?=$this->_nombre?>_Url();
                        }
                        function <?=$this->_nombre?>_Reset()
                        {
                            palabraClave = "";
                            window.location.href = <?=$this->_nombre?>_Url();
                        }
                        function <?=$this->_nombre;?>_Url()
                        {
                            try
                            {
                            	if(paginaActual.indexOf('?') == -1)
                            		s = '?';
                            	else
                            		s = '';
                            		
                                return paginaActual+s+"&irPagina="+paginaNroActual+"&columna="+columnaOrdenar+"&sentido="+columnaSentido+"&tp="+tamanioPagina+"&q="+palabraClave+"<?=$this->getParametersURL()?>";
                                //return paginaActual+"?irPagina="+paginaNroActual+"&columna="+columnaOrdenar+"&sentido="+columnaSentido;
                            }
                            catch(e)
                            {
                                throw e;
                            }
                        }
                        function <?=$this->_nombre;?>_ordenarColumna(columna)
                        {
                            try
                            {   
                                if(columna == columnaOrdenar)
                                {
                                    if(columnaSentido == "DESC")
                                        columnaSentido = "ASC";
                                    else
                                        columnaSentido = "DESC";
                                }
                                columnaOrdenar=columna;
                                window.location.href=<?=$this->_nombre;?>_Url();                                
                            }
                            catch(e)
                            {
                                throw e;
                            }
                        }
                        function <?=$this->_nombre;?>_filaClick(fila)
                        {
                            try
                            {   
                                if(document.getElementById(fila).className == "tr_seleccionada")
                                {
                                    var id = fila.replace("fila_","");
                                    //document.getElementById(fila).className="tr_"+ (id % 2);
                                    ultimaFilaClick="";
                                    ultimaFilaSeleccionadaNro = -1;
                                }
                                else
                                {
                                    if(ultimaFilaClick != "")
                                    {
                                        var id = ultimaFilaClick.replace("fila_","");
                                        //document.getElementById(ultimaFilaClick).className="tr_"+ (id % 2);
                                    }
                                    //document.getElementById(fila).className="tr_seleccionada";
                                }
                                ultimaFilaClick = fila;
                                ultimaFilaSeleccionadaNro = fila.replace("fila_","");
                            }
                            catch(e)
                            {
                                throw e;
                            }
                        }
                        function <?=$this->_nombre;?>_getCeldaSeleccionada(indexColumna)
                        {
                            try
                            {
                                if(ultimaFilaSeleccionadaNro >-1 )
                                {
                                    if(document.getElementById('f['+ ultimaFilaSeleccionadaNro +']['+ indexColumna +']'))
                                        return document.getElementById('f['+ ultimaFilaSeleccionadaNro +']['+ indexColumna +']').value;
                                    else
                                        throw ("Index de columna fuera del intervalo.");
                                }
                                else
                                    return false;
                            }
                            catch(e)
                            {
                                throw e;
                            }
                        }
                        function <?=$this->_nombre;?>_irPagina(nro)
                        {
                            try
                            {
                                paginaNroActual = nro;
                                window.location.href=<?=$this->_nombre;?>_Url();
                            }
                            catch(e)
                            {
                                throw e;
                            }
                        }
                    </script>
                <?
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function style()
        {
            try
            {
                ?>
                <style type="text/css">
                    .td_columna
                    {
                        /*border-bottom: <?=TABLA_COLUMNA_BORDE?>;*/
                        /*border-left: <?=TABLA_COLUMNA_BORDE?>;*/
                        height: <?=TABLA_COLUMNA_ALTO?>;
                        text-align: <?=TABLA_COLUMNA_TEXT_ALIGN?>;
                        cursor: <?=TABLA_COLUMNA_CURSOR?>;
                        /*background-color: <?=TABLA_COLUMNA_COLOR?>;*/
                        background-image: url("../imagenes/Tabla.gif");
                    }
                    td.td_columna:hover
                    {
                        background-image: url("../imagenes/Tabla_on.gif");
                    }
                    /**/
                    .paginado
                    {
                        text-decoration: <?=TABLA_PAGINADO_TEXT_DECORATION?>;
                        cursor: <?=TABLA_PAGINADO_CURSOR?>;
                    }
                    .paginado_sin
                    {
                        text-decoration: <?=TABLA_PAGINADO_SIN_TEXT_DECORATION?>;
                        cursor: <?=TABLA_PAGINADO_SIN_CURSOR?>;
                    }
                    .tr_paginado
                    {
                        /*background-color: <?=TABLA_PAGINADO_COLOR?>;*/
                        background-image: url("../imagenes/Borde_paginacion.gif");
                        /*font-size: 15px;*/
                    }
                    /**/
                    .td_fila
                    {
                        border-bottom: <?=TABLA_FILA_BORDE?>;
                        /*border-left: <?=TABLA_FILA_BORDE?>;*/
                        height: <?=TABLA_FILA_ALTO?>;
                    }
                    .td_fila_vacia
                    {
                        border-bottom: <?=TABLA_FILA_BORDE?>;
                        /*border-left: <?=TABLA_FILA_BORDE?>;*/
                        height: <?=TABLA_FILA_ALTO?>;
                    }
                    .td_fila_vacia2
                    {
                        /*border-bottom: <?=TABLA_FILA_BORDE?>;*/
                        /*border-left: <?=TABLA_FILA_BORDE?>;*/
                        height: <?=TABLA_FILA_ALTO?>;
                    }
                    .tr_0
                    {
                        /*background-color: <?=TABLA_FILA_PAR_COLOR?>;*/
                        cursor: <?=TABLA_FILA_CURSOR?>;
                    }
                    .tr_1
                    {
                        /*background-color: <?=TABLA_FILA_IMPAR_COLOR?>;*/
                        cursor: <?=TABLA_FILA_CURSOR?>;
                    }
                    /**/
                    .datos_text
                    {
                        text-align: <?=TABLA_FILA_TEXT_ALIGN?>;
                        background-color: transparent;
                        border: 0;
                        width: 100%;
                        cursor: <?=TABLA_FILA_CURSOR?>;
                    } 
                    /**/
                    .table_tabla
                    {
                        /*border:thin	solid;*/
                        border-left: <?=TABLA_FILA_BORDE?>;
                        border-right: <?=TABLA_FILA_BORDE?>;
                        width:100%;
                    }
                </style>
                <?
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function showPaginacion()
        {
            try
            {
                $Adicional = ($this->_totalRegistros % $this->_paginacion);                    
                $Total = 0 + ($this->_totalRegistros / $this->_paginacion);
                if ($Adicional == 0)
                    $Total--;
                $Total = (int) $Total;
				
				if ($this->_paginaReal == null) { 
					$pagUtilizable = $this->_paginaNro;
				} else { 
				    $pagUtilizable = $this->_paginaReal;
				}
				
                if($pagUtilizable < ($Total))
                    $pagSiguiente = $pagUtilizable + 1;
                else
                    $pagSiguiente = -1;
                    
                if($pagUtilizable > 0)
                    $pagAnterior = $pagUtilizable - 1;
                else
                    $pagAnterior = -1;
                    if ($this->_enablePaginacion)
                    {
                ?>
                    <tr class="tr_paginado">
                        <td colspan=<?=count($this->_columnas)+count($this->_botones)?> align="center">
                            <table width=50%>
                            <tr>
                                <td>
                                    <span onclick="<?=($this->_pagina != "" && $pagUtilizable != 0)? $this->_nombre."_irPagina(0)" :"" ?>" class="<?=($this->_pagina != "" && $pagUtilizable != 0)?"paginado":"paginado_sin"?>">Primera</span>
                                </td>
                                <td>
                                    <span onclick="<?=($this->_pagina != "" && $pagAnterior >-1)? $this->_nombre."_irPagina($pagAnterior)" :"" ?>" class="<?=($this->_pagina != "" && $pagAnterior >-1)?"paginado":"paginado_sin"?>">Anterior</span>
                                </td>
                                <td>
                                    <?
                                        /*for($i=0; $i<$Total;$i++)
                                        {
                                            ?>
                                                &nbsp;<span onclick="<?=($this->_pagina != "" && $this->_paginaNro != $i)? $this->_nombre."_irPagina($i)" :"" ?>" class="<?=($this->_pagina != "" && $this->_paginaNro != $i)?"paginado":"paginado_sin"?>"><?=$i+1?></span>&nbsp;
                                            <?
                                        }
                                        $i--;*/
                                    ?>
                                </td>
                                <td>
                                    <span>P&aacute;gina </span>
                                        <input onkeypress="if (ValidarEnter(event)) <?=$this->_nombre?>_irPagina(document.getElementById('txtPagina').value-1);" type="text" name="txtPagina" id="txtPagina" size="3" value="<?=$pagUtilizable+1?>"/>
                                    <span> de <?=$Total+1?></span>
                                &nbsp;
                                <input type="button" value="Ir" onclick="<?=$this->_nombre?>_irPagina(document.getElementById('txtPagina').value-1);" />
                                
                                </td>
                                <td>
                                    <span onclick="<?=($this->_pagina != "" && $pagSiguiente >-1)? $this->_nombre."_irPagina($pagSiguiente)" :"" ?>" class="<?=($this->_pagina != "" && $pagSiguiente >-1)?"paginado":"paginado_sin"?>">Siguiente</span>
                                </td>
                                <td>
                                    <span onclick="<?=($this->_pagina != "" && $pagUtilizable != $Total)? $this->_nombre."_irPagina($Total)" :"" ?>" class="<?=($this->_pagina != "" && $pagUtilizable != $Total)?"paginado":"paginado_sin"?>">Ultima</span>
                                </td>
                                <td>
                            <?
                            }
                                    if($this->_enableTamanioPaginas)
                                    {
                                ?>
                                    <select name="<?=$this->_nombre?>_cbxPaginas" id="<?=$this->_nombre?>_cbxPaginas" onchange="<?=$this->_nombre?>_Paginacion()">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <script>
                                        document.getElementById("<?=$this->_nombre?>_cbxPaginas").value = <?=$this->_paginacion?>;
                                    </script>
                                <?
                                     }
                                ?>
                                </td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                <?
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        private function showFiltrar()
        {
            try
            {
                if($this->_enableFiltro)
                {
                ?>
                    <tr>
                        <td colspan=100% align="center">
                            <table width=90%>
                            <tr>
                                <td width=20%>Buscar</td>
                                <td width=70%><input type="text" id="<?=$this->_nombre?>_txtBuscar" name="<?=$this->_nombre?>_txtBuscar" style="width:90%" value="<?=$this->_palabraclave?>"/></td>
                                <td width=10%><input type="button" value="Buscar" onclick="<?=$this->_nombre?>_Filtrar();" /><input type="button" value="Ver todo" onclick="<?=$this->_nombre?>_Reset();" /></td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                <?
                }
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
    }  
?>