<?
     class PaginadorNew {
         private $_palabreclave="";
         private $_tope=10;
         private $_posicion=0;
         private $_MAX_X_PAG=10;
         private $_PAG=1;
         private $_CANTPAGINAS=0;
         private $_DataSource = array();                                 
         private $_nombre="Paginador";
         private $_action;
         private $_seleccion=-1;
         
         public function PaginadorNew($Posicion, array $DataSource, $action, $nombre = "Paginador") {
             $this->_DataSource = $DataSource;
             $this->_nombre = $nombre;
             $this->_action = $action;
             $this->_posicion = $Posicion;
             $this->_CANTPAGINAS = ceil(count($this->_DataSource) / $this->_MAX_X_PAG);
         }

         public function show() {
         	//$this->_CANTPAGINAS = ceil(count($this->_DataSource) / $this->_MAX_X_PAG);
         	if ($this->_CANTPAGINAS > 1) {
	             $this->_script();
	             echo '<table class="paginador"><tr>';
	             $this->_Paginas();
	             echo '</tr></table>';
	             $this->_Form();
	        }
         }
         
         private function _script() {
             ?>
                <script>
                    var <?=$this->_nombre?>Tope = <?=$this->_tope?>;
                    function <?=$this->_nombre?>SeleccionarPagina_enComboPaginas(pagina) {
                        document.getElementById("cbxPaginas<?=$this->_nombre?>").value = pagina;
                    }
                    function <?=$this->_nombre?>CambiarPagina(nropagina) {
                        document.getElementById("PAG").value = nropagina;
                        document.getElementById("frm<?=$this->_nombre?>").submit();
                    }
                    function <?=$this->_nombre?>CantidadPaginas(Valor) {
                        document.getElementById("MAX_X_PAG").value = Valor;
                        document.getElementById("PAG").value = 1;
                        document.getElementById("frm<?=$this->_nombre?>").submit();
                    }
                    function <?=$this->_nombre?>Pagina(Valor) {
                        document.getElementById("POSICION").value = parseInt(document.getElementById("POSICION").value) + parseInt(Valor);
                        document.getElementById("PAG").value = 1 + (<?=$this->_nombre?>Tope * parseInt(document.getElementById("POSICION").value));
                        document.getElementById("frm<?=$this->_nombre?>").submit();
                    }
                    function <?=$this->_nombre?>PalabraClave(Valor) {
                        document.getElementById("PALABRA_CLAVE").value = Valor;
                        document.getElementById("PAG").value = 1;
                        document.getElementById("frm<?=$this->_nombre?>").submit();
                    }       
                </script>
             <?
         }
         
         private function _Form() {
             ?>
                <table style="width:100%"><tr>
                  <form name="frm<?=$this->_nombre?>" id="frm<?=$this->_nombre?>" action="<?=$this->_action?>" method="post">
                    <input type="hidden" id="PAG" name="PAG" value="<?=$this->_PAG?>" />
                    <input type="hidden" id="MAX_X_PAG" name="MAX_X_PAG" value="<?=$this->_MAX_X_PAG?>" />
                    <input type="hidden" id="POSICION" name="POSICION" value="<?=$this->_posicion?>" />
                    <input type="hidden" id="PALABRA_CLAVE" name="PALABRA_CLAVE" value="<?=$this->_posicion?>" />
                  </form>
                </table>
             <?
         }
         
         private function _Paginas() {
             try {
                 $newArray = array();
                if ($this->_MAX_X_PAG) {
                     //$this->_CANTPAGINAS = count($this->_DataSource) / $this->_MAX_X_PAG;
                     //$this->_CANTPAGINAS = ceil($this->_CANTPAGINAS);
                     $Inicio = 1 + ($this->_posicion * $this->_tope);
                     $Fin = (($this->_CANTPAGINAS < $this->_tope)?$this->_CANTPAGINAS:$this->_tope) + ($this->_posicion * $this->_tope);

                     if($Fin) {
                        ?>
                        <td class="primeraultima"><a href="javascript:;" onclick="javascript:<?=$this->_nombre?>CambiarPagina(1)">Primera</a></td>
                        <?
                      }
                        ?>
                        &nbsp;
                        <?
                      for($i=$Inicio; $i<=$Fin; $i++) {
                                ?><td<?=($i == $this->_PAG)?' class="activa"':''?>><a <?=($i != $this->_PAG)?"href=\"javascript:;\" onclick=\"javascript:".$this->_nombre."CambiarPagina(".$i.")\"":""?>><?=$i?></a></td> &#160;<?
                      }

                      if ($Fin > 0) {
                         ?>
                            <td class="primeraultima"><a href="javascript:;" onclick="javascript:<?=$this->_nombre?>CambiarPagina(<?=$Fin?>)">&Uacute;ltima</a></td>
                         <?
                      }
                 }
                 else {
                     $this->_MAX_X_PAG = count($this->_DataSource);
                     $this->_PAG = 1;
                 }
             } catch(exception $e) {
                 throw $e;
             }
         }
         
         public function getArrayPaginado() {
         	if (!$this->_DataSource) 
            	$FINFOR=0;
            else if(count($this->_DataSource)<($this->_MAX_X_PAG* $this->_PAG)) {
            	$FINFOR = count($this->_DataSource);
            } else {
            	$FINFOR = ($this->_MAX_X_PAG * ($this->_PAG-1)) + $this->_MAX_X_PAG;
            }

            $newArray = array();
            for($i=($this->_MAX_X_PAG * ($this->_PAG-1)); $i<$FINFOR; $i++) {
            	array_push($newArray, $this->_DataSource[$i]);
			}

            return $newArray;
         }

         public function setPagina($pagina) {
             $this->_PAG = $pagina;
         }
         
         public function getPagina() {
             return $this->_PAG;
         }
         
         public function setMaxXPag($maxXpag) {
             $this->_MAX_X_PAG = $maxXpag;
         }
         
         public function setCantPaginas($maxXpag) {
             $this->_CANTPAGINAS = $maxXpag;
         }
         
         public function getMaxXPag() {
             return $this->_MAX_X_PAG;
         }
         
         public function setSeleccion($valor) {
             $this->_seleccion=$valor;
         }
         
         public function setPalabraClave($valor) {
             $this->_palabreclave=$valor;
         }

     } 
?>
