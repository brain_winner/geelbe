<?php
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php";

    class Imagen
    {
        private $_rutaimagen;
        private $_defaultalto;
        private $_defaultancho;
        private $_newalto;
        private $_newancho;
        private $_maxalto;
        private $_maxancho;
        private $_tipo=0;

        public function setRuta($ruta)
        {
            $this->_rutaimagen = $ruta;
        }
        public function setAncho($ancho)
        {
             $this->_newancho = $ancho;
        }
        public function setAlto($alto)
        {
            $this->_newalto = $alto;
        }
        public function getRuta()
        {
            return $this->_rutaimagen;
        }
        public function getAlto()
        {
            return $this->_newalto;
        }
        public function getAncho()
        {
            return $this->_newancho;
        }
        public function getOriginalAlto()
        {
            return $this->_defaultalto;
        }
        public function getOriginalAncho()
        {
           return $this->_defaultancho;
        }
        public function getExtension()
        {
            return get_extension($this->_rutaimagen);
        }
        public function getTipo()
        {
            return $this->_tipo;
        }
        public function __construct($ruta, $anchoDeseado, $altoDeseado)
        {
          $this->_rutaimagen = $ruta;
          $this->_maxancho = $anchoDeseado;
          $this->_maxalto = $altoDeseado;
          $this->Redimensionar();
        }
       private function Redimensionar()
        {
            try
            {
                $image_attribs = getimagesize($this->_rutaimagen);
                $th_max_width = $this->_maxancho;
                $th_max_height = $this->_maxalto;

                $this->_defaultancho = $image_attribs[0];
                $this->_defaultalto = $image_attribs[1];
                $this->_tipo = $image_attribs[2];
                $width = $image_attribs[0];
                $height = $image_attribs[1];
                // si la imagen es cuadrada
                if ($width > $th_max_width || $height >  $th_max_height)
                {
                    if ($width  == $height)
                    {
                        if($th_max_width >= $th_max_height)
                        {
                            $nuevo_ancho = $th_max_height;
                            $nuevo_alto = $th_max_height;
                        }
                        else
                        {
                            $nuevo_ancho = $th_max_width;
                            $nuevo_alto = $th_max_width;
                        }
                    }
                    // si la imagen es mas alta que ancha
                    if ($height > $width)
                    {
                        if($th_max_width >= $th_max_height)
                        {
                            $nuevo_ancho = $th_max_width;
                            $nuevo_alto =  $height*$th_max_width/$width;
                        }
                        else
                        {
                            $nuevo_ancho = $width*$th_max_height/$height;
                            $nuevo_alto = $th_max_height;
                        }
                    }
                    // si la imagen es mas ancha que alta
                    if ($height <= $width)
                    {
                         if($th_max_width < $th_max_height)
                        {
                            if($width*$th_max_height/$height>$th_max_width)
                            {
                                $nuevo_ancho = $th_max_width;
                                $nuevo_alto = $height*$th_max_width/$width;
                            }
                            else
                            {
                                $nuevo_alto = $th_max_height;
                                $nuevo_ancho = $width*$th_max_height/$height;
                            }
                        }
                        else
                        {
                            if($height*$th_max_width/$width>$th_max_height)
                            {
                                $nuevo_alto=$th_max_height;
                                $nuevo_ancho = $width*$th_max_height/$height;
                            }
                            else
                            {
                                $nuevo_ancho = $th_max_width;
                                $nuevo_alto = $height*$th_max_width/$width;
                            }
                        }
                    }
                    $this->_newancho = $nuevo_ancho;
                    $this->_newalto = $nuevo_alto;
                }
                else
                {
                   $this->_newancho = $width;
                   $this->_newalto = $height;
                }
            }
            catch(exception $e)
            {
                throw $e;
            }

        }
    }

    /**
    * @desc Saber si un elemento existe en un vector
    * @param Valor a buscar
    * @param Array donde buscar
    * @return bool
    */
    function ExisteEnArray($aguja, $pajar)
    {
        for($i=0; $i<count($pajar); $i++)
        {
            if($aguja == $pajar[$i])
                return true;
        }
        return false;
    }
/**
* @desc Funci�n que a partir de una ruta obtiene todos los archivos dentro.
* @param string. Ruta de las im�genes.
* @param int. Valor que indica el formato que tendr� el array. 1: �ndices num�ricos, 2: nombre de archivo como �ndice. 3: �dem 2 sin extensiones.
* @param array. Extensiones permitidas.
* @param int. Constante de ordenamiento.
*/
  function DirectorytoArray($dir, $val=1, $arrExt=array(), $sort_flag = SORT_ASC)
  {
        try
        {
            $arrFiles = ArrayfromDirectory($dir, $arrExt);
            sort($arrFiles, $sort_flag);
            if ($val == 1)
                return $arrFiles;
            $arrFiles = ArrayValue_to_Index($arrFiles);
            if ($val == 2)
                return $arrFiles;
            $arrFiles = QuitarExtensiones_arrImagenes($arrFiles);
            if ($val == 3)
            return $arrFiles;
        }
        catch(exception $e)
        {
            throw $e;
        }
  }
  function QuitarExtensiones_arrImagenes($arrImagenes)
  {
      $newArray = array();
      //for($i=0; $i<count($arrImagenes); $i++)
      foreach($arrImagenes as $value)
      {
          $newArray[str_replace(".".get_extension($value), "", $value)] = $value;
      }
      return $newArray;
  }
/**
* @desc Devuelve el archivo sin la extensi�n.
*/
function extract_extension($filename)
{
    return str_replace(".".get_extension($filename), "", $filename);
}
/**
* @desc Los �ndices del vector pasar a ser los valores.
*/
function ArrayValue_to_Index($arr)
{
    $newArr = array();
    for($i=0; $i<count($arr); $i++)
    {
        $newArr[$arr[$i]] = $arr[$i];
    }
    return $newArr;
}
/**
    * @desc Obtiene la extensi�n de un archivo.
    */
    function get_extension($ruta)
    {
        $temp1 = explode(".",$ruta);
        $temp2 = count($temp1)-1;
        $ext = $temp1[$temp2];
        $ext = strtolower($ext);
        return $ext;
    }
/**
    * @desc Transforma los archivos de un directorio en elementos de un array.
    * @param string. Ruta del directorio.
    * @return array.
    */
    function ArrayfromDirectory($dir, $ext=array())
    {
        try
        {
            $directory = opendir($dir);
            $arrFiles = array();
            while($archivo = readdir($directory))
            {
                if (count($ext) > 0)
                {
                    $f = get_extension($archivo);
                    $d = array_search($f, $ext);
                    if (strlen($archivo) > 3 && array_search(get_extension($archivo), $ext) !== FALSE)
                    {
                        array_push($arrFiles, $archivo);
                    }
                }
                else if (strlen($archivo) > 3)
                {
                    array_push($arrFiles, $archivo);
                }
            }
            return $arrFiles;
        }
        catch(exception $e)
        {
            throw $e;
        }
    }

function GetFile_URL($rutaUrl)
{
    try
    {
        $arr = explode("/", $rutaUrl);
        return $arr[(count($arr)-1)];
    }
    catch(exception $e)
    {
        throw $e;
    }
}
function ValidarUsuarioLogueadoBO($idSeccion)
{
    try
    {
        if ($_SESSION["BO"]["User"] && Aplicacion::Decrypter($_SESSION["BO"]["User"]["Dir"]) == Aplicacion::getDirLocal())
        {
            if($idSeccion==0)
            {
                return true;
            }
            else
            {
                $sections=$_SESSION["BO"]["User"]["secciones"];
                foreach($sections as $key)
                {
                    if($key['idSeccion']==$idSeccion)
                    {
                           return true;
                    }
                }
                throw new ACCESOException("Acceso denegado", "404", "login");
            }
        }
        else
        {
            throw new ACCESOException("Acceso denegado", "404", "login");
        }
    }
    catch(exception $e)
    {
        throw $e;
    }
}
function ValidarUsuarioLogueado()
{

	 if(empty($_SESSION["User"]) || Aplicacion::Decrypter($_SESSION["User"]["Dir"]) != Aplicacion::getDirLocal()){

		try
		{
		 	if($idUsuario = Aplicacion::checkLoginCookie()){
		 		$login = new LoginController();
		 		$login->setGuardarCookie(true);
		 		$login->loguearById($idUsuario);

		 		/*	 	//echo "hola";

				$objUsuarios = dmUsuarios::ForzarLogin($idUsuario);
				Aplicacion::setLoginCookie($objUsuarios->getIdUsuario());
				//Hist�rico
					$oLoginH = new clsLoginH();
					$oLoginH->setApellido(addslashes($objUsuarios->getDatos()->getApellido()));
					$oLoginH->setNombre(addslashes($objUsuarios->getDatos()->getNombre()));
					$oLoginH->setNombreUsuario($objUsuarios->getNombreUsuario());
					$oLoginH->setFecha(date("Y/m/d H:i"));
					dmLoginH::Guardar($oLoginH);
				//Fin hist�rico
				if ($objUsuarios->getes_Provisoria()==0)
				{
					Aplicacion::loginUser($objUsuarios);
					global $objReglaNegocio;
					$objReglaNegocio->NuevoRegistroUsuario($objUsuarios->getIdUsuario());
				}
				*/
			}
		}
		catch(MySQLException $e) {
			throw $e;
		}
	 }

	try
    {
        if($_SESSION["User"] && Aplicacion::Decrypter($_SESSION["User"]["Dir"]) == Aplicacion::getDirLocal())
            return true;
        else
        {
            if ($_GET["nl"] && Aplicacion::Decrypter($_GET["nl"]) == 1)
            {
                $_SESSION["whereToGo"] = "http://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
                throw new ACCESOException("Acceso denegado","333", "login");
            }
            else
            {
                throw new ACCESOException("Acceso denegado","304", "login");
            }
        }
    }
    catch(ACCESOException $e) {
        throw $e;
    }
}
function Moneda($numero, $round = false)
{
    $Formato = Aplicacion::getParametros("moneda","formato");
    
    if($round) {
    	$Decimales = 0;
    } else {
	   //$Decimales = Aplicacion::getParametros("moneda","decimales");
	   $Decimales = 0;
	 }
	 
    return str_replace("#importe#", number_format($numero, $Decimales, ',', '.'), $Formato);
}
function MonedaConDecimales($numero, $round = false)
{
    $Formato = Aplicacion::getParametros("moneda","formato");
    
    if($round) {
    	$Decimales = 0;
    } else {
	   $Decimales = Aplicacion::getParametros("moneda","decimales");
	   $Decimales = 0;
	 }
	 
    return str_replace("#importe#", number_format($numero, $Decimales, ',', '.'), $Formato);
}
function ObtenerPrecioProductos($PrecioVenta, $Incremento)
{
    try
    {
        $Operador = substr($Incremento, 0, 1);
        $Incremento = substr($Incremento, 1, strlen($Incremento));
        if($Operador == "*")
        {
            $result = $PrecioVenta * $Incremento;
        }
        else if($Operador == "+")
        {
            $result = $PrecioVenta + $Incremento;
        }
        else
        {
            throw new Exception("EL operador ingresado es incorrecto.");
        }
        return $result;
    }
    catch(exception $e)
    {
        throw $e;
    }
}
function ColumnaTimestamp_to_date(&$Tabla, $Columna, $FormatoFecha = "d/m/Y")
        {
            try
            {
                for($i=0; $i<count($Tabla); $i++)
                {
                    $Tabla[$i][$Columna] = ParserSTR::Timestamp_to_date($Tabla[$i][$Columna], $FormatoFecha);
                }
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
function ColumnaMoneda(&$Tabla, $Columna)
{
    try
    {
        for($i=0; $i<count($Tabla); $i++)
        {
            $Tabla[$i][$Columna] = Moneda($Tabla[$i][$Columna]);
        }
    }
    catch(exception $e)
    {
        throw $e;
    }
}
/**
* @desc Funci�n que se encarga de convertir una im�gen a escala de grises
*/
function IMGtoGrayScale($ImagenOrigen, $ImagenDestino)
{
    try
    {
        $ImagenDestino = strtolower($ImagenDestino);
        $ImagenOrigen = strtolower($ImagenOrigen);
        copy($ImagenOrigen, $ImagenDestino);
        switch(get_extension($ImagenDestino))
        {
            case "jpg":
            case "jpeg":
                $img = imagecreatefromjpeg($ImagenDestino);
                imagefilter($img, IMG_FILTER_GRAYSCALE);
                imagejpeg($img, $ImagenDestino);
            break;

            case "png":
                $img = imagecreatefrompng($ImagenDestino);
                 imagefilter($img, IMG_FILTER_GRAYSCALE);
                 imagepng($img, $ImagenDestino);
            break;

            case "gif":
                $img = imagecreatefromgif($ImagenDestino);
                imagefilter($img, IMG_FILTER_GRAYSCALE);
                imagegif($img, $ImagenDestino);
            break;
        }
    }
    catch(exception $e)
    {
        throw $e;
    }
}
/**
* @desc  Crea un thumbnail de la imagen seleccionada
* @param string. Ruta del archivo de origen.
* @param string. Ruta del archivo de destino.
* @param int. Ancho m�ximo de la imagen.
* @param int. Alto m�ximo de la imagen.
*/
function Thumbnail($archivo, $archivodestino, $anchomaximo, $altomaximo)
{
    try
    {
        $archivo = strtolower($archivo);
        $archivodestino = strtolower($archivodestino);
        $img = new Imagen($archivo, $anchomaximo, $altomaximo);
        //$extension = $img->getExtension();
        switch($img->getTipo())
        {
            case 1://GIF
                $thumb = imagecreatetruecolor($img->getAncho(), $img->getAlto());
                $image = imagecreatefromgif($archivo);
                imagecopyresampled($thumb, $image, 0,0,0,0,$img->getAncho(), $img->getAlto(),$img->getOriginalAncho(), $img->getOriginalAlto());
                imagegif($thumb, $archivodestino);
            break;
            case 2://JPG
                $thumb = imagecreatetruecolor($img->getAncho(), $img->getAlto());
                $image = imagecreatefromjpeg($archivo);
                imagecopyresampled($thumb, $image, 0,0,0,0,$img->getAncho(), $img->getAlto(),$img->getOriginalAncho(), $img->getOriginalAlto());
                imagejpeg($thumb, $archivodestino,100);
            break;
            case 3://PNG
                $thumb = imagecreatetruecolor($img->getAncho(), $img->getAlto());
                $image = imagecreatefrompng($archivo);
                imagecopyresampled($thumb, $image, 0,0,0,0,$img->getAncho(), $img->getAlto(),$img->getOriginalAncho(), $img->getOriginalAlto());
                imagepng($thumb, $archivodestino,0);
            break;
            case "gif":
                $thumb = imagecreatetruecolor($img->getAncho(), $img->getAlto());
                $image = imagecreatefromgif($archivo);
                imagecopyresampled($thumb, $image, 0,0,0,0,$img->getAncho(), $img->getAlto(),$img->getOriginalAncho(), $img->getOriginalAlto());
                imagegif($thumb, $archivodestino,100);
            break;
        }
    }
    catch(exception $e)
    {
        throw $e;
    }
    }

	/**
	* @desc  Guarda logo transparente con fondo gris
	* @param string. Ruta del archivo de origen.
	* @param string. Ruta del archivo de destino.
	*/
	function SaveTransparentLogo($from, $to, $bg = 0) {
	
		$img = imagecreatefrompng($from);
		imagesavealpha($img, true);
		
		$new = imagecreatetruecolor(158, 80);

		if($bg == 0)
			$gray = imagecolorallocate($new, 192, 192, 192);
		else
			$gray = imagecolorallocate($new, 255, 255, 255);
		
		imagefill($new, 0, 0, $gray);
		imagecopyresampled($new, $img, 0,0,0,0, 158, 80, imagesx($img), imagesy($img));
		
		if($bg == 0)
			imagefilter($new, IMG_FILTER_GRAYSCALE);
			
		imagepng($new, $to, 0);
	
	}
    function OrdenarMatriz($Matriz, $Campo, $Orden)
    {
        $Matriz = array_values($Matriz);
        for($i=0; $i<count($Matriz); $i++)
        {
            $Pos = $i;
            for($j=$i+1; $j<count($Matriz); $j++)
            {
                if ($Orden == "ASC")
                    if($Matriz[$j][$Campo] < $Matriz[$Pos][$Campo])
                        $Pos = $j;
                if ($Orden == "DESC")
                    if($Matriz[$j][$Campo] > $Matriz[$Pos][$Campo])
                        $Pos = $j;
            }
            $Aux = $Matriz[$i];
            $Matriz[$i] = $Matriz[$Pos];
            $Matriz[$Pos] = $Aux;
        }
        
        return DesplazarSinStock($Matriz);
    }
    
    function DesplazarSinStock($Matriz) {
	    	$SinStock = array();
	    	$Ultimos = array();
	    	
	    	foreach($Matriz as $i => $prod) {
	    		if(isset($prod['StockMax']) && $prod['StockMax'] == 0) {
	    			$SinStock[] = $prod;
	    			unset($Matriz[$i]);
	    		} else if(isset($prod['StockMax'], $prod['ultimos']) && $prod['ultimos'] != 0 && $prod['ultimos'] >= $prod['StockMax']) {
	    			$Ultimos[] = $prod;
	    			unset($Matriz[$i]);
	    		}
	    	}
	    	
	    	return array_merge($Ultimos, $Matriz, $SinStock);
    }
    
	function generar_hash($id){
		return hash("crc32",$id."a lo hecho pecho");
	}

	function generar_hash_md5($id){
		return hash("md5",$id."CFPcul0AjHpnN1wnS0PZw5QpGRcaJVoHCbklkdZKFdAKWa1lI6uoS05Xfd1E2MCq3PFvz6ZZhM3xVRbVTMd99wrEnhIQqvGXVz3o5hyxCybt6bPn6DFp0SpqzKwmP9pzIPBctqn7Rul3S6fSFkrgqbs75fpHxVYE8EpElqbhtfqRhGgadHeDtvxCJJ4kpx4V0dJCS5NO");
	}

	function checkemail($email) {
		return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
	}

	function checkAlfa($codigo){
		return eregi("^[_a-z0-9-]*$",$codigo);
	}

	function recursive_in_array($needle, $haystack){
		foreach ($haystack as $stalk) {
		    if ($needle === $stalk || (is_array($stalk) && recursive_in_array($needle, $stalk))) {
		        return true;
		    }
		}
		return false;
	}

	//==== Redirect... Try PHP header redirect, then Java redirect, then try http redirect.:
	function redirect($url){
	    if (!headers_sent()){    //If headers not sent yet... then do php redirect
	        header('Location: '.$url); //exit;
	    }else{                    //If headers are sent... do java redirect... if java disabled, do html redirect.
	        echo '<script type="text/javascript">';
	        echo 'window.location.href="'.$url.'";';
	        echo '</script>';
	        echo '<noscript>';
	        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
	        echo '</noscript>'; //exit; 
	    }
	}//==== End -- Redirect

    /**
     * Genera un <script> para usar los iframes de Pato.
     *
     * @param string $function nombre de la funcion
     * @param string $param1 primer parametro (opcional)
     * @param string $param2 segundo parametro (opcional)
     * @param string $param3 tercer parametro (opcional)
     * @return string el script completo
     */
	function generarJs($function,$param1="",$param2="nulo",$param3="nulo")
	{
		$html = "<script>".$function."(\"".addslashes($param1)."\"".($param2!="nulo"?",\"".addslashes($param2)."\"":"").($param3!="nulo"?",\"".addslashes($param3)."\"":"").")</script>";
		return $html;
	}

	/**
	 * Chequea si algo es 0
	 *
	 * @param string|integer $var la variable a testear si es 0
	 * @return bool
	 */
	function esCero($var){
		return ($var == "0" || $var === 0)?true:false;
	}
	
	function exHandler($exception) {
		$emailBuilder = new EmailBuilder();
	    $emailData = $emailBuilder->generateAnEmail("soporte@corp.geelbe.com", 
	                                                "Excepcion en Geelbe: \n".$exception, 
	                                                "exception",
	                                                "Excepcion en Geelbe");
	    $emailSender = new EmailSender();
		$emailSender->sendEmail($emailData);
		echo "</br></br>Error sin considerar. Por favor avise a contacto@geelbe.com para su pronta solucion.</br></br>";
	}
 
	//set_exception_handler('exHandler');

	?> 