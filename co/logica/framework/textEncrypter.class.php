<? 

// You can change the salt you use in your application here
define("APPLICATION_ENCRYPTION_SALT","xyz");

function urlsafe_b64encode($string)
{
    $data = base64_encode($string);
    $data = str_replace(array('+','/'),array('-','_'),$data);
    return $data;
}
function urlsafe_b64decode($string) 
{
    $data = str_replace(array('-','_'),array('+','/'),$string);
    $mod4 = strlen($data) % 4;
    if ($mod4)
    {
        $data .= substr('====', $mod4);
    }
    return base64_decode($data);
}

class textEncrypter
{


	var $salt;
	var $separator;

	/**
	* @return returns value of variable $salt
	* @desc getSalt : Getting value for variable $salt
	*/
	function getSalt ()
	{
		return $this->salt ;
	}

	/**
	* @param param : value to be saved in variable $salt
	* @desc setSalt : Setting value for $salt
	*/
	function setSalt ($value)
	{
		$this->salt  = $value;
	}

	/**
	* @return returns value of variable $separator
	* @desc getSeparator : Getting value for variable $separator
	*/
	function getSeparator()
	{
		return $this->separator;
	}

	/**
	* @param param : value to be saved in variable $separator
	* @desc setSeparator : Setting value for $separator
	*/
	function setSeparator($value)
	{
		$this->separator = $value;
	}

	function textEncrypter()
	{
		$this->setSalt(APPLICATION_ENCRYPTION_SALT);
		$this->setSeparator("|||");
	}


	/**
	* @return encoded string with salt added
	* @param String to be encoded
	* @desc Adds Salt to Data and Encode it before sending back to client
	* @generationDate 2004-10-31
	* @version 1.0
	* @license GNU GPL License
	* @author Nilesh Dosooye <opensource@weboot.com>
	*/
	function encode($string)
	{
		// Write Function Code Here

		$string = $string.$this->getSeparator().$this->getSalt();
		//$string = base64_encode($string);
        $string = urlsafe_b64encode($string);

		return $string;
	}

	/**
	* @return UnEncoded Data
	* @param String to be Decoded
	* @desc Decode Data and Exits if tampering of data is detected
	* @generationDate 2004-10-31
	* @version 1.0
	* @license GNU GPL License
	* @author Nilesh Dosooye <opensource@weboot.com>
	*/
	function decode($string)
	{
		// Write Function Code Here

		//$string = base64_decode($string);
        $string = urlsafe_b64decode($string);
		$tokens = explode($this->getSeparator(),$string);
        

		if ($tokens[1]!=$this->getSalt())
		{
            throw new Exception("Intento de desencriptación no previsto.");
		}
        //$string = urlsafe_b64decode($tokens[0]);
		//return $tokens[0];
        return $tokens[0];

	}


}


?>