<?php
    if($_POST)
    {
        ob_start();
        $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("login"));
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("registro"));
        Aplicacion::CargarIncludes(Aplicacion::getIncludes("usuarios"));
        require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");

		$fields = array(
			'txtUser' => array(
				'exception' => 'Usuario vac&iacute;o',
				'check' => 'isset'
			),
			
			'txtPass' => array(
				'exception' => 'Clave vac&iacute;a',
				'check' => 'isset'
			)
								
		);
		
		ValidatorUtils::checkFields($fields, $_POST, false);

		$login = new LoginController();
		$login->setGuardarCookie($_POST["recordar"]);
		$login->loguearByUserPassPaginasEMS($_POST["txtUser"], $_POST["txtPass"]);

        ob_flush();
    }
?>