<?
ob_start();
try {
	$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
	require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
	Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta"));
} catch(exception $e) {
	die(print_r($e));
}

try {
	ValidarUsuarioLogueado();
} catch(Exception $e) {
	header('Location: /'.$confRoot[1].'/front/ems/pre-desuscribir.php');
	exit;
}

try {
	$objUsuario = new MySQL_micuenta();
	$objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
	
    $sexoUsuario = "";
    if($objUsuario->getDatos()->getIdTratamiento() == 1) {
        $sexoUsuario = "Hombre";
    } else if ($objUsuario->getDatos()->getIdTratamiento() == 2) {
        $sexoUsuario = "Mujer";
    }
        
    $provinciaUsuario = "";
    if($objUsuario->getDirecciones(0)->getIdProvincia() > 0) {
    	$oConexion = Conexion::nuevo();
        $oConexion->setQuery("SELECT Nombre FROM provincias WHERE IdProvincia = ".mysql_real_escape_string($objUsuario->getDirecciones(0)->getIdProvincia()));                
        $DT = $oConexion->DevolverQuery();
        $oConexion->Cerrar();
                
        $provinciaUsuario = $DT[0]["Nombre"];
    }
	 
	$oConexion = Conexion::nuevo();
	$oConexion->setQuery('INSERT INTO acciones_listas_ems (email, nombre, apellido, tipo_accion, fecha_accion, accion_encolada, accion_commiteada, id_usuario, hash_usuario, fechanacimiento_usuario, sexo_usuario, provincia_usuario)
        			          VALUES ("'.htmlspecialchars(mysql_real_escape_string($objUsuario->getNombreUsuario()), ENT_QUOTES).'", "'.htmlspecialchars(mysql_real_escape_string($objUsuario->getDatos()->getNombre()), ENT_QUOTES).'", "'.htmlspecialchars(mysql_real_escape_string($objUsuario->getDatos()->getApellido()), ENT_QUOTES).'", "B", NOW(), 0, 0, '.
	mysql_real_escape_string($objUsuario->getIdUsuario()).', "'.mysql_real_escape_string(generar_hash_md5($objUsuario->getIdUsuario())).'", "'.mysql_real_escape_string($objUsuario->getDatos()->getFechaNacimiento()).'", "'.
	htmlspecialchars(mysql_real_escape_string($sexoUsuario), ENT_QUOTES).'", "'.htmlspecialchars(mysql_real_escape_string($provinciaUsuario), ENT_QUOTES).'");');

	$oConexion->EjecutarQuery();
	
		 
	$oConexion = Conexion::nuevo();
	$oConexion->setQuery('UPDATE usuarios SET mail_suscription = 0 where IdUsuario = '.$objUsuario->getIdUsuario().';');
	$oConexion->EjecutarQuery();
	
	
} catch(Exception $e) {
	echo generarJS("window.parent.msjError","Hubo problemas al intentar desuscribirlo; por favor vuelva a intentarlo.","Hubo problemas al intentar desuscribirlo");
	exit;
}

echo generarJS("window.parent.Redirect","/".Aplicacion::getDirLocal()."front/ems/post-desuscribir.php","_self");

ob_flush();
?>