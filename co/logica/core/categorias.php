<?php
class categorias
{
    function __construct()
    {
        $this->_idcategoria="";
        $this->_idpadre="";
        $this->_nombre="";
        $this->_idcampania="";
        $this->_descripcion="";
        $this->_orden=0;
        $this->_habilitado=1;
        $this->_deep=0;
        $this->_idordenamiento=1;
    }
//DATOS PRIVADOS
     protected $_idcategoria;
     protected $_idpadre;
     protected $_nombre;
     protected $_idcampania;
     protected $_descripcion;
     protected $_orden;
     protected $_habilitado;
     protected $_deep;
     protected $_productos=array();
     protected $_idordenamiento;
//PROPIEDADES PUBLICAS
    function setIdOrdenamiento($idordenamiento)
    {
        $this->_idordenamiento = $idordenamiento;
    }
    function getIdOrdenamiento()
    {
        return $this->_idordenamiento;
    }
    function setIdCategoria($idcategoria)
    {
            $this->_idcategoria = $idcategoria;
    }
    function getIdCategoria()
    {
            return $this->_idcategoria;
    }
    function setIdPadre($idpadre)
    {
            $this->_idpadre = $idpadre;
    }
    function getIdPadre()
    {
            return $this->_idpadre;
    }
    function setNombre($nombre)
    {
            $this->_nombre = $nombre;
    }
    function getNombre()
    {
            return $this->_nombre;
    }
    function setIdCampania($idcampania)
    {
            $this->_idcampania = $idcampania;
    }
    function getIdCampania()
    {
            return $this->_idcampania;
    }
    function setDescripcion($descripcion)
    {
            $this->_descripcion = $descripcion;
    }
    function getDescripcion()
    {
            return $this->_descripcion;
    }
    function setOrden($orden)
    {
            $this->_orden = $orden;
    }
    function getOrden()
    {
            return $this->_orden;
    }
    function setHabilitado($habilitado)
    {
            $this->_habilitado = $habilitado;
    }
    function getHabilitado()
    {
            return $this->_habilitado;
    }
    function addProductos(productos_x_categorias $producto)
    {
        $this->_productos[]=$producto;
    }
    function getProducto($index)
    {
        return $this->_productos[$index];
    }
    function getProductos()
    {
        return $this->_productos;
    }
    function removeProducto($index)
    {
        unset($this->_productos[$index]);
    }
    function setDeep($deep)
    {
        $this->_deep=$deep;
    }
    function getDeep()
    {
        return $this->_deep;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /*Destructor*/
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if($propiedad != "_datos" && $propiedad !="_direcciones")
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
class productos_x_categorias
{
    protected $_idproducto=0;
    protected $_idcategoria=0;
    function setIdProducto($idproducto)
    {
        $this->_idproducto=$idproducto;
    }
    function getIdProducto()
    {
        return $this->_idproducto;
    }
    function setIdCategoria($idcategoria)
    {
        $this->_idcategoria=$idcategoria;
    }
    function getIdCategoria()
    {
        return $this->_idcategoria;
    }
}
?>
