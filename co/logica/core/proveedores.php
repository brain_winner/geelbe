<?
 class proveedores
 {
	function __construct()
	{
		$this->_idproveedor="";
		$this->_nombre="";
		$this->_direccion="";
		$this->_localidad="";
		$this->_cp="";
		$this->_idprovincia="";
		$this->_cif="";
		$this->_telefono="";
		$this->_fax="";
		$this->_website="";
        $this->_habilitado=1;
        $this->_comentarios="";
	}
//DATOS PRIVADOS
	 protected $_idproveedor;
	 protected $_nombre;
	 protected $_direccion;
	 protected $_localidad;
	 protected $_cp;
	 protected $_idprovincia;
	 protected $_cif;
	 protected $_telefono;
	 protected $_fax;
	 protected $_website;
     protected $_habilitado;
     protected $_comentarios;
//PROPIEDADES PUBLICAS
    function setComentarios($comentarios)
    {
        $this->_comentarios = $comentarios;
    }
    function getComentarios()
    {
        return $this->_comentarios;
    }
	function setIdProveedor($idproveedor)
	{
			$this->_idproveedor = $idproveedor;
	}
	function getIdProveedor()
	{
			return $this->_idproveedor;
	}
	function setNombre($nombre)
	{
			$this->_nombre = $nombre;
	}
	function getNombre()
	{
			return $this->_nombre;
	}
	function setDireccion($direccion)
	{
			$this->_direccion = $direccion;
	}
	function getDireccion()
	{
			return $this->_direccion;
	}
	function setLocalidad($localidad)
	{
			$this->_localidad = $localidad;
	}
	function getLocalidad()
	{
			return $this->_localidad;
	}
	function setCP($cp)
	{
			$this->_cp = $cp;
	}
	function getCP()
	{
			return $this->_cp;
	}
	function setIdProvincia($idprovincia)
	{
			$this->_idprovincia = $idprovincia;
	}
	function getIdProvincia()
	{
			return $this->_idprovincia;
	}
	function setCIF($cif)
	{
			$this->_cif = $cif;
	}
	function getCIF()
	{
			return $this->_cif;
	}
	function setTelefono($telefono)
	{
			$this->_telefono = $telefono;
	}
	function getTelefono()
	{
			return $this->_telefono;
	}
	function setFax($fax)
	{
			$this->_fax = $fax;
	}
	function getFax()
	{
			return $this->_fax;
	}
	function setWebSite($website)
    {
            $this->_website = $website;
    }
    function getWebSite()
    {
            return $this->_website;
    }
    function setHabilitado($habilitado)
    {
            $this->_habilitado = $habilitado;
    }
    function getHabilitado()
    {
            return $this->_habilitado;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /*Destructor*/
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if($propiedad != "_datos" && $propiedad !="_direcciones")
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>