<?php
    class CarritoCompras
    {
        private $_idcampania=0;
        private $_tiempo=0;
        private $_misarticulos=array();
        
        public function CarritoCompras($tiempo, $idcampania)
        {
            $this->_tiempo = $tiempo;
            $this->_idcampania = $idcampania;
        }
        public function setIdCampania($idcampania)
        {
            $this->_idcampania = $idcampania;
        }
        public function getIdCampania()
        {
            return $this->_idcampania;
        }
        public function setTiempo($tiempo)
        {
            $this->_tiempo = $tiempo;
        }
        public function getTiempo()
        {
            return $this->_tiempo;
        }
        public function setDescuentoId($id)
		{
			$_SESSION['descuento_id'] = $id;
		}
		public function getDescuentoId()
		{
			return (isset($_SESSION['descuento_id']) && $_SESSION['descuento_id'] > 0 ? $_SESSION['descuento_id'] : null);
		}
		public function getDescuento($totalProds)
		{
			$did = $this->getDescuentoId();
			if(!is_numeric($did))
				return 0;
				
			$d = dmDescuentos::getById($did);
			if(!is_object($d))
				return 0;
				
			if($d->getPorcentaje())
				$desc = round($d->getDescuento()*$totalProds/100, 2);
			else
				$desc = $d->getDescuento();
				
			if($desc > $totalProds)
				$desc = $totalProds;
				
			return $desc;
			
		}
		public function getDescuentoObj() {
			return dmDescuentos::getById($this->getDescuentoId());
		}
        public function addArticulo($IdProducto, $IdCodigoProdInterno, $Cantidad, $Precio, $Stock)
        {
            $Articulo = array("IdProducto" => $IdProducto, "Cantidad" => $Cantidad, "Precio" => $Precio, "Stock" => $Stock);
            $this->_misarticulos[$IdCodigoProdInterno] = $Articulo;
        }
        public function setArticulo($IdProducto, $IdCodigoProdInterno, $Cantidad, $Precio, $Stock)
        {
            $Articulo = array("IdProducto" => $IdProducto, "Cantidad" => $Cantidad, "Precio" => $Precio, "Stock" => $Stock);
            $this->_misarticulos[$IdCodigoProdInterno] = $Articulo;
        }
        public function getArticulo($IdCodigoProdInterno)
        {
            return $this->_misarticulos[$IdCodigoProdInterno];
        }
        public function removeArticulo($IdCodigoProdInterno)
        {
            unset($this->_misarticulos[$IdCodigoProdInterno]);
        }
        public function getArticulos()
        {
            return $this->_misarticulos;
        }
        public function Expiro()
        {
            if($this->_tiempo >= time())
                return false;
            else
                return true;
        }
    }
?>
