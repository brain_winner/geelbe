<?php
    class Roles
    {
        private $_idseccion=0;
        private $_accion=0;//0 denegado, 1 full, 2 read only
        public function setIdSeccion($id_seccion)
        {
            $this->_idseccion=$id_seccion;
        }
        public function getIdSeccion()
        {
            return $this->_idseccion;
        }
        public function setAccion($accion)
        {
            $this->_accion=$accion;
        }
        public function getAccion()
        {
            return $this->_accion;
        }
    }
?>
