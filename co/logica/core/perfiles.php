<?/***Constructor de la clase clsperfiles*/
class perfiles
{
    protected $_idperfil = 0;
    protected $_descripcion = "";
    /**	*Constructor de la clase clsperfiles
    *@var long idperfil
    * *@var string descripcion 	*/
    function __construct($idperfil = 0,$descripcion = "")
    {
        $this->setIdPerfil($idperfil);
        $this->setDescripcion($descripcion);
    }
    /**	* Set IdPerfil 	* @var long IdPerfil	*/
    function setIdPerfil($idperfil)
    {
        $this->_idperfil =$idperfil;
    }	
    /**	* Get IdPerfil 	* @return long IdPerfil	*/
    function getIdPerfil()
    {
        return $this->_idperfil;
    }
    /**	* Set Descripcion 	* @var string Descripcion	*/
    function setDescripcion($descripcion)
    {
        $this->_descripcion =$descripcion;
    }
    /**	* Get Descripcion 	* @return string Descripcion	*/
    function getDescripcion()
    {
        return $this->_descripcion;
    }
    /**	* Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * 
    * * @var String atributo	*/
    function __set($valor,$atributo)
    {
	    if(isset($this->{"_".strtolower($atributo)}))
    		       $this->{"_".strtolower($atributo)} = $valor;
    /*else		   throw new Exception("No existes la variable privada $atributo");*/	
    }
    /**	* Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo	* @return String	*/
    function __get($atributo)
    {
	    if(isset($this->{"_".strtolower($atributo)}))
    	       return $this->{"_".strtolower($atributo)};
    /*else		   throw new Exception("No existes la variable privada $atributo");*/
    }
    /**	* toString devuelve un string con todos los datos de la clase
    * @return String	*/
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
    /**	* @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.	*/
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
}
?>