<?
 class claseproductos
 {
	function __construct()
	{
		$this->_idclase="";
		$this->_nombre="";
        $this->_habilitado=1;
        $this->_parametros=array();
        $this->_impuestos=array();
	}
//DATOS PRIVADOS
	 protected $_idclase;
	 protected $_nombre;
     protected $_habilitado;
     protected $_parametros;
     protected $_impuestos;

//PROPIEDADES PUBLICAS
	function setIdClase($idclase)
	{
			$this->_idclase = $idclase;
	}
	function getIdClase()
	{
			return $this->_idclase;
	}
	function setNombre($nombre)
    {
            $this->_nombre = $nombre;
    }
    function getNombre()
    {
            return $this->_nombre;
    }
    function setHabilitado($habilitado)
    {
            $this->_habilitado = $habilitado;
    }
    function getHabilitado()
    {
            return $this->_habilitado;
    }
    function addParametro(atributosclase $atributo)
    {
        $this->_parametros[] = $atributo;
    }
    function removeParametro($index)
    {
        unset($this->_parametros[$index]);
    }
    function getParametros()
    {
        return $this->_parametros;
    }
    function addImpuesto(impuestosclaseproductos $impuesto)
    {
        $this->_impuestos[] = $impuesto;
    }
    function removeImpuesto($index)
    {
        unset($this->_impuestos[$index]);
    }
    function getImpuestos()
    {
        return $this->_impuestos;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if($propiedad != "_parametros" && $propiedad != "_impuestos")
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }    
}
 class atributosclase
 {
	function __construct()
	{
		$this->_idclase="";
		$this->_idatributo="";
		$this->_idatributoclase="";
        $this->_atributos=array();
	}
//DATOS PRIVADOS
	 protected $_idclase;
	 protected $_idatributo;
	 protected $_idatributoclase;
     protected $_atributos;
//PROPIEDADES PUBLICAS
	function setIdClase($idclase)
	{
			$this->_idclase = $idclase;
	}
	function getIdClase()
	{
			return $this->_idclase;
	}
	function setIdAtributo($idatributo)
	{
			$this->_idatributo = $idatributo;
	}
	function getIdAtributo()
	{
			return $this->_idatributo;
	}
    function setIdAtributoClase($idatributoclase)
	{
			$this->_idatributoclase = $idatributoclase;
	}
	function getIdAtributoClase()
	{
			return $this->_idatributoclase;
	}
    function addAtributos(atributos $atributo)
    {
        $this->_atributos[]=$atributo;
    }
    function removeAtributo($index)
    {
        unset($this->_atributos[$index]);
    }
    function getAtributos()
    {
        return $this->_atributos;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if($propiedad != "_atributos")
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
class atributos
 {
    function __construct()
    {
        $this->_idatributo="";
        $this->_detalle="";
        $this->_nombre="";
        $this->_pregunta="";
    }
//DATOS PRIVADOS
     protected $_idatributo;
     protected $_detalle;
     protected $_nombre;
     protected $_pregunta;
//PROPIEDADES PUBLICAS
    function setIdAtributo($idatributo)
    {
            $this->_idatributo = $idatributo;
    }
    function getIdAtributo()
    {
            return $this->_idatributo;
    }
    function setDetalle($detalle)
    {
            $this->_detalle = $detalle;
    }
    function getDetalle()
    {
            return $this->_detalle;
    }
    function setNombre($nombre)
    {
            $this->_nombre = $nombre;
    }
    function getNombre()
    {
            return $this->_nombre;
    }
    function setPregunta($pregunta)
    {
            $this->_pregunta = $pregunta;
    }
    function getPregunta()
    {
            return $this->_pregunta;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if($propiedad != "_parametros")
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
class impuestosclaseproductos
 {
    function __construct()
    {
        $this->_idclase=0;
        $this->_idimpuesto=0;
    }
//DATOS PRIVADOS
     protected $_idclase;
     protected $_idimpuesto;
//PROPIEDADES PUBLICAS
    function setIdClase($idclase)
    {
            $this->_idclase = $idclase;
    }
    function getIdClase()
    {
            return $this->_idclase;
    }
    function setIdImpuesto($idimpuesto)
    {
            $this->_idimpuesto = $idimpuesto;
    }
    function getIdImpuesto()
    {
            return $this->_idimpuesto;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>