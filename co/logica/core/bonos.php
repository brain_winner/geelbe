<?
/**
*Constructor de la clase clsbonos_x_usuarios
*/
class bonos_x_usuarios
{   
    protected $_idbono = 0;
    protected $_idregla = 0;
    protected $_idusuario = 0;
    protected $_fecha = 0;
    protected $_idestadobono = 0;
    /**
    *Constructor de la clase clsbonos_x_usuarios
    *@var long idbono 
    *@var long idregla 
    *@var long idusuario 
    *@var long fecha 
    *@var long idestadobono 
    */
    function __construct($idbono = 0,$idregla = 0,$idusuario = 0,$fecha = 0,$idestadobono = 0)
    {
        $this->setIdBono($idbono);
        $this->setIdRegla($idregla);
        $this->setIdUsuario($idusuario);
        $this->setFecha($fecha);
        $this->setIdEstadoBono($idestadobono);
    }
    /**
    * Set IdBono 
    * @var long IdBono
    */
    function setIdBono($idbono)
    {
        $this->_idbono =$idbono;
    }
    /**
    * Get IdBono 
    * @return long IdBono
    */
    function getIdBono()
    {
        return $this->_idbono;
    }
    /**
    * Set IdRegla 
    * @var long IdRegla
    */
    function setIdRegla($idregla)
    {
        $this->_idregla =$idregla;
    }
    /**
    * Get IdRegla 
    * @return long IdRegla
    */
    function getIdRegla()
    {
        return $this->_idregla;
    }
    /**
    * Set IdUsuario 
    * @var long IdUsuario
    */
    function setIdUsuario($idusuario)
    {
        $this->_idusuario =$idusuario;
    }
    /**
    * Get IdUsuario 
    * @return long IdUsuario
    */
    function getIdUsuario()
    {
        return $this->_idusuario;
    }
    /**
    * Set Fecha 
    * @var long Fecha
    */
    function setFecha($fecha)
    {
        $this->_fecha =$fecha;
    }
    /**
    * Get Fecha 
    * @return long Fecha
    */
    function getFecha()
    {
        return $this->_fecha;
    }
    /**
    * Set IdEstadoBono 
    * @var long IdEstadoBono
    */
    function setIdEstadoBono($idestadobono)
    {
        $this->_idestadobono =$idestadobono;
    }
    /**
    * Get IdEstadoBono 
    * @return long IdEstadoBono
    */
    function getIdEstadoBono()
    {
        return $this->_idestadobono;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/    
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/    
    }
    /**
    * toString devuelve un string con todos los datos de la clase
    * @return String
    */
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
    /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __getArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>