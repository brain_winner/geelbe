<?php
class campanias
{
    public function  __construct()
    {
	    $this->_idcampania=0;
	    $this->_idcomercial=0;
	    $this->_nombre="";
	    $this->_descripcion="";
	    $this->_fechainicio="";
	    $this->_fechafin="";
	    $this->_visibledesde="";
	    $this->_maxprod=5;
        $this->_propiedades=array();
        $this->_tiempoentregain = "";
        $this->_tiempoentregafn = "";
        $this->_tiempoentregain2 = "";
        $this->_tiempoentregafn2 = "";
        $this->_fechacorte = "";
        $this->_minmontoenviogratis = 0;
        $this->_visitas = 0;
        $this->_welcome = 0;
        $this->_coleccion = 0;
        $this->_idordenamiento = 0;
        $this->_descuento = 0;
        $this->_descuentomax = "";
    }    
    protected $_idcampania;
    protected $_idcomercial;
    protected $_nombre;
    protected $_descripcion;
    protected $_fechainicio;
    protected $_fechafin;
    protected $_visibledesde;
    protected $_maxprod;
    protected $_propiedades;
    protected $_tiempoentregain;
    protected $_tiempoentregafn;
    protected $_tiempoentregain2;
    protected $_tiempoentregafn2;
    protected $_fechacorte;
    protected $_visitas;
    protected $_minmontoenviogratis;
    protected $_welcome;
    protected $_coleccion;
    protected $_idordenamiento;
    protected $_descuento;
    protected $_descuentomax;
    
    public function setIdComercial($min_monto_envio_gratis)
    {
       $this->_idcomercial = $min_monto_envio_gratis;
    }
    public function getIdComercial()
    {
       return $this->_idcomercial;
    }
    public function setMinMontoEnvioGratis($min_monto_envio_gratis)
    {
       $this->_minmontoenviogratis = $min_monto_envio_gratis;
    }
    public function getMinMontoEnvioGratis()
    {
       return $this->_minmontoenviogratis;
    }
    public function  setColeccion($idcampania)
    {
		    $this->_coleccion = $idcampania;
    }
    
    public function  getColeccion()
    {
		    return $this->_coleccion;
    }
    public function  setIdCampania($idcampania)
    {
		    $this->_idcampania = $idcampania;
    }
    
    public function  getIdCampania()
    {
		    return $this->_idcampania;
    }
    public function  setNombre($nombre)
    {
		    $this->_nombre = $nombre;
    }
    public function  getNombre()
    {
		    return $this->_nombre;
    }
    public function  setDescripcion($descripcion)
    {
		    $this->_descripcion = $descripcion;
    }
    public function  getDescripcion()
    {
		    return $this->_descripcion;
    }
    public function  setFechaInicio($fechainicio)
    {
		    $this->_fechainicio = $fechainicio;
    }
    public function  getFechaInicio()
    {
		    return $this->_fechainicio;
    }
    public function getFechaInicioEn() {
	    
	    list($day, $month, $year, $hour, $min) = sscanf($this->_fechainicio, "%d/%d/%d %d:%d");
	    return $month.'/'.$day.'/'.$year.' '.$hour.':'.$min;
	    
    }
    public function  setFechaFin($fechafin)
    {
		    $this->_fechafin = $fechafin;
    }
    public function  getFechaFin()
    {
		    return $this->_fechafin;
    }
    public function getFechaFinEn() {
	    
	    list($day, $month, $year, $hour, $min) = sscanf($this->_fechafin, "%d/%d/%d %d:%d");
	    return $month.'/'.$day.'/'.$year.' '.$hour.':'.$min;
	    
    }
    public function  setVisibleDesde($visibledesde)
    {
		    $this->_visibledesde = $visibledesde;
    }
    public function  getVisibleDesde()
    {
		    return $this->_visibledesde;
    }
    public function  setMaxProd($maxprod)
    {
            $this->_maxprod = $maxprod;
    }
    public function  getMaxProd()
    {
            return $this->_maxprod;
    }    
    
    public function  setTiempoEntregaIn($dias)
    {
		    $this->_tiempoentregain = $dias;
    }
    
    public function getTiempoEntregaIn()
    {
		    return $this->_tiempoentregain;
    }
    
    public function  setTiempoEntregaFn($dias)
    {
		    $this->_tiempoentregafn = $dias;
    }
    
    public function  getTiempoEntregaFn()
    {
		    return $this->_tiempoentregafn;
    }
    
    public function  setTiempoEntregaIn2($dias)
    {
		    $this->_tiempoentregain2 = $dias;
    }
    
    public function getTiempoEntregaIn2()
    {
		    return $this->_tiempoentregain2;
    }
    
    public function  setTiempoEntregaFn2($dias)
    {
		    $this->_tiempoentregafn2 = $dias;
    }
    
    public function  getTiempoEntregaFn2()
    {
		    return $this->_tiempoentregafn2;
    }
    
    public function getTiempoEntregaInCorte($fecha) {
	    
	    if($this->getFechaCorte() == null || $fecha < strtotime($this->getFechaCorte()) + 24*60*60)
	    	return $this->getTiempoEntregaIn();
	    else
	    	return $this->getTiempoEntregaIn2();
	    
    }
    
    public function getTiempoEntregaFnCorte($fecha) {
	    
	    if($this->getFechaCorte() == null || $fecha < strtotime($this->getFechaCorte()) + 24*60*60)
	    	return $this->getTiempoEntregaFn();
	    else
	    	return $this->getTiempoEntregaFn2();
	    
    }
    
    public function  setFechaCorte($dias)
    {
		    $this->_fechacorte = $dias;
    }
    
    public function  getFechaCorte()
    {
		    return $this->_fechacorte;
    }
    
    public function  setDescuento($dias)
    {
		    $this->_descuento = $dias;
    }
    
    public function  getDescuento()
    {
		    return $this->_descuento;
    }
    
     public function  setDescuentoMax($dias)
    {
		    $this->_descuentomax = $dias;
    }
    
    public function  getDescuentoMax()
    {
		    return $this->_descuentomax;
    }
    
    public function addPropiedad($idpropiedad)
    {
        $this->_propiedades[]=$idpropiedad;
    }
    public function getPropiedades()
    {
        return $this->_propiedades;
    }
    public function getPropiedad($index)
    {
        return $this->_propiedades[$index];
    }
    public function hasPropiedad($id) {
    	foreach ($this->_propiedades as $key => $value) {
    		foreach ($value as $k => $v) {
	    		if ($v == $id) {
	    			return true;
	    		}
    		}
		}
    	return false;
    }
    public function removePropiedad($index)
    {
        unset($this->_propiedades[$index]);
    }
    public function setWelcome($imagen_destino)
    {
        $this->_welcome = $imagen_destino;
    }
    public function getWelcome()
     {
         return $this->_welcome;
     }  
    public function setIdOrdenamiento($imagen_destino)
    {
        $this->_idordenamiento = $imagen_destino;
    }
    public function getIdOrdenamiento()
     {
         return $this->_idordenamiento;
     }  
    public function  esCampaniaVisibleTIMESTAMP()
    {
        $fecha_Visible = explode(" ",$this->getVisibleDesde());
        ParserSTR::getTimeDMY_a_UNIX($fecha_Visible[0], $fecha_Visible[1]);
        $fechaInicio = mktime(0,0,0,date("m", $this->getFechaInicio()),date("d", $this->getFechaInicio()),date("y", $this->getFechaInicio()));
        
    }
    public function  getDiasRestantes()
    {	
	    $fechaInicio = mktime(0,0,0,date("m", $this->getFechaInicio()),date("d", $this->getFechaInicio()),date("y", $this->getFechaInicio()));

	    $fechaHoy = mktime(0,0,0,date("m"),date("d"),date("y"));
        $fecha1 = $fechaHoy - $fechaInicio;
        $fecha1 = $fecha1 / (60*60*24);
        $fecha1 = floor($fecha1);

	    $fechaFin = mktime(0,0,0,date("m", $this->getFechaFin()),date("d", $this->getFechaFin()),date("y", $this->getFechaFin()));
	    $fechaHoy = mktime(0,0,0,date("m"),date("d"),date("y"));
	    $fecha2 = $fechaFin - $fechaHoy;
	    $fecha2 = $fecha2 / (60*60*24);
	    $fecha2 = floor($fecha2);
	    
	    
	    if ($fecha1 >= 0 && $fecha2 > 0)
		    return $fecha2;
	    elseif($fecha1 < 0)
		    return abs($fecha1);
	    else
		    return -1;
    }
    public function  getEstadoCampania()
    {		
	    $fechaInicio = mktime(0,0,0,date("m", $this->getFechaInicio()),date("d", $this->getFechaInicio()),date("y", $this->getFechaInicio()));

	    $fechaHoy = mktime(0,0,0,date("m"),date("d"),date("y"));
        $fecha1 = $fechaHoy - $fechaInicio;
        $fecha1 = $fecha1 / (60*60*24);
        $fecha1 = floor($fecha1);
	    
	    $fechaFin = mktime(0,0,0,date("m", $this->getFechaFin()),date("d", $this->getFechaFin()),date("y", $this->getFechaFin()));
	    $fechaHoy = mktime(0,0,0,date("m"),date("d"),date("y"));
	    $fecha2 = $fechaFin - $fechaHoy;
	    $fecha2 = $fecha2 / (60*60*24);
	    $fecha2 = floor($fecha2);
	    
	    
	    if ($fecha1 >= 0 && $fecha2 > 0)
		    return "Actualmente";
	    elseif($fecha2 <= 0)
		    return "Cerrada";
	    elseif($fecha1 < 0)
		    return "Proximamente";
	    else
		    return "Error";		
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /*Destructor*/
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if($propiedad != "_datos" && $propiedad !="_direcciones")
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
/**
*Constructor de la clase clscampanias_propiedades
*/
class campanias_propiedades
{
    
    protected $_idpropiedadcampamia = 0;
    protected $_nombre = "";

    /**
    *Constructor de la clase clscampanias_propiedades
    *@var long idpropiedadcampamia 
    *@var string nombre 
    */
    function __construct($idpropiedadcampamia = 0,$nombre = "")
    {
        $this->setIdPropiedadCampamia($idpropiedadcampamia);
        $this->setNombre($nombre);
    }
    
    /**
    * Set IdPropiedadCampamia 
    * @var long IdPropiedadCampamia
    */
    function setIdPropiedadCampamia($idpropiedadcampamia)
    {
        $this->_idpropiedadcampamia =$idpropiedadcampamia;
    }
    /**
    * Get IdPropiedadCampamia 
    * @return long IdPropiedadCampamia
    */
    function getIdPropiedadCampamia()
    {
        return $this->_idpropiedadcampamia;
    }
    /**
    * Set Nombre 
    * @var string Nombre
    */
    function setNombre($nombre)
    {
        $this->_nombre =$nombre;
    }
    /**
    * Get Nombre 
    * @return string Nombre
    */
    function getNombre()
    {
        return $this->_nombre;
    }

    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           $this->{"_".strtolower($atributo)} = $valor;
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           return $this->{"_".strtolower($atributo)};
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * toString devuelve un string con todos los datos de la clase
    * @return String
    */
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
    /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }

}
/**
*Constructor de la clase clscampanias_x_propiedades
*/
class campanias_x_propiedades
{
    
    protected $_idpropiedadcampamia = 0;
    protected $_idcampania = 0;

    /**
    *Constructor de la clase clscampanias_x_propiedades
    *@var long idpropiedadcampamia 
    *@var long idcampania 
    */
    function __construct($idpropiedadcampamia = 0,$idcampania = 0)
    {
        $this->setIdPropiedadCampamia($idpropiedadcampamia);
        $this->setIdCampania($idcampania);
    }
    
    /**
    * Set IdPropiedadCampamia 
    * @var long IdPropiedadCampamia
    */
    function setIdPropiedadCampamia($idpropiedadcampamia)
    {
        $this->_idpropiedadcampamia =$idpropiedadcampamia;
    }
    /**
    * Get IdPropiedadCampamia 
    * @return long IdPropiedadCampamia
    */
    function getIdPropiedadCampamia()
    {
        return $this->_idpropiedadcampamia;
    }
    /**
    * Set IdCampania 
    * @var long IdCampania
    */
    function setIdCampania($idcampania)
    {
        $this->_idcampania =$idcampania;
    }
    /**
    * Get IdCampania 
    * @return long IdCampania
    */
    function getIdCampania()
    {
        return $this->_idcampania;
    }

    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           $this->{"_".strtolower($atributo)} = $valor;
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           return $this->{"_".strtolower($atributo)};
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * toString devuelve un string con todos los datos de la clase
    * @return String
    */
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
    /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }

}
?>
