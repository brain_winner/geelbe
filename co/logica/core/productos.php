<?
class productos
{
	/*Datos Privados*/
	protected $_idproducto;//Long
	protected $_nombre;//String
	protected $_descripcion;//String
    protected $_descripcionampliada;//String
	protected $_idmarca;//Integer
	protected $_idclase;//Integer
	protected $_pcompra;//Single
	protected $_pventa;//Single
	protected $_pvp;//String
	protected $_peso;//Single
	protected $_referencia;//String
    protected $_ancho;
    protected $_alto;
    protected $_profundidad;
    protected $_habilitado;
    protected $_ultimos;
    protected $_productos_proveedores;
    protected $_guiadetalles; //string
    protected $_oculto; //Integer
    protected $_locales = array();
    
 	/*Constructor*/
	function __construct($idproducto=0, $nombre="", $descripcion="", $idmarca=0, $idclase=0, $pcompra=0, $pventa=0, $pvp="", $peso=0, $referencia="", $ancho=0,$alto=0,$profundidad=0, $guiadetalles = "", $oculto = 0)
	{
		$this->_idproducto=$idproducto;
		$this->_nombre=$nombre;
		$this->_descripcion=$descripcion;
        $this->_descripcionampliada="";
		$this->_idmarca=$idmarca;
		$this->_idclase=$idclase;
		$this->_pcompra=$pcompra;
		$this->_pventa=$pventa;
		$this->_pvp=$pvp;
		$this->_peso=$peso;
		$this->_referencia=$referencia;
        $this->_ancho = $ancho;
        $this->_alto = $alto;
        $this->_profundidad = $profundidad;
        $this->_habilitado=1;
        $this->_ultimos=0;
        $this->_guiadetalles = $guiadetalles;
        $this->_oculto = $oculto;
        $this->_productos_proveedores=array();
	}
	/*Metodos Publicos*/
    function getLocales()
    {
        return $this->_locales;
    }
    function setIdLocal($idlocal)
    {
        $this->_locales[] = $idlocal;
    }
    function getIdLocal($index)
    {
        return $this->_locales[$index];
    }
    function setGuiaDeTalles($guiadetalles)
    {
        $this->_guiadetalles = $guiadetalles;
    }
    function getGuiaDeTalles()
    {
        return $this->_guiadetalles;
    }
	function setIdProducto($idproducto)
	{
		$this->_idproducto = $idproducto;
	}
	function getIdProducto()
	{
		return $this->_idproducto;
	}
	function setNombre($nombre)
	{
		$this->_nombre = $nombre;
	}
	function getNombre()
	{
		return $this->_nombre;
	}
	function setDescripcion($descripcion)
    {
        $this->_descripcion = $descripcion;
    }
    function getDescripcion()
    {
        return $this->_descripcion;
    }
    function setDescripcionAmpliada($descripcionampliada)
    {
        $this->_descripcionampliada = $descripcionampliada;
    }
    function getDescripcionAmpliada()
    {
        return $this->_descripcionampliada;
    }
    function setIdMarca($idmarca)
	{
		$this->_idmarca = $idmarca;
	}
	function getIdMarca()
	{
		return $this->_idmarca;
	}
	function setIdClase($idclase)
	{
		$this->_idclase = $idclase;
	}
	function getIdClase()
	{
		return $this->_idclase;
	}
	function setPCompra($pcompra)
	{
		$this->_pcompra = $pcompra;
	}
	function getPCompra()
	{
		return $this->_pcompra;
	}
	function setPVenta($pventa)
	{
		$this->_pventa = $pventa;
	}
	function getPVenta()
	{
		return $this->_pventa;
	}
	function setPVP($pvp)
	{
		$this->_pvp = $pvp;
	}
	function getPVP()
	{
		return $this->_pvp;
	}
	function setPeso($peso)
	{
		$this->_peso = $peso;
	}
	function getPeso()
	{
		return $this->_peso;
	}
	function setReferencia($referencia)
	{
		$this->_referencia = $referencia;
	}
	function getReferencia()
	{
		return $this->_referencia;
	}
    function setAlto($alto)
    {
        $this->_alto = $alto;
    }
    function getAlto()
    {
        return $this->_alto;
    }
    function setAncho($ancho)
    {
        $this->_ancho = $ancho;
    }
    function getAncho()
    {
        return $this->_ancho;
    }
    function setProfundidad($profundidad)
    {
        $this->_profundidad = $profundidad;
    }
    function getProfundidad()
    {
        return $this->_profundidad;
    }
    function setHabilitado($habilitado)
    {
        $this->_habilitado = $habilitado;
    }
    function getHabilitado()
    {
        return $this->_habilitado;
    }
    function setUltimos($habilitado)
    {
        $this->_ultimos = $habilitado;
    }
    function getUltimos()
    {
        return $this->_ultimos;
    }
    function setOculto($habilitado)
    {
        $this->_oculto = $habilitado;
    }
    function getOculto()
    {
        return $this->_oculto;
    }
    function getPesoVolumetrico()
    {
        try
        {
            $Vol = $this->_alto * $this->_profundidad * $this->_ancho * 222;
            $PV = $Vol / 1000000;
        }
        catch(exception $e)
        {
            $PV =0;
        }
        return $PV;
    }
    function addProductosProveedores(productos_x_proveedores $producto)
    {
        $this->_productos_proveedores[]=$producto;
    }
    function getProductosProveedores()
    {
        return $this->_productos_proveedores;
    }
    function getProductoProveedor($index)
    {
        return $this->_productos_proveedores[$index];
    }
    function removeProductoProveedor($index)
    {
        unset($this->_productos_proveedores[$index]);
    }
	/**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/    
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/    
    }
    /**
    * toString devuelve un string con todos los datos de la clase
    * @return String
    */
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
    /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if(!is_array($valor))
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
	
	/*Destructor*/
	function __destruct()
	{
	}
}
class productos_x_proveedores
{
	/*Datos Privados*/
	protected $_idproducto;//Integer
	protected $_idproveedor;//Integer
	protected $_idcodigoprodinterno;//Long
	protected $_stock;//Integer
	protected $_regalo;//Integer
	protected $_incremento;//String
	protected $_comentarios;//String
    protected $_habilitado;
    protected $_atributos=array();
    /*Constructor*/
	function __construct($idproducto=0, $idproveedor=0, $idcodigoprodinterno=0, $stock=0, $regalo=0, $incremento="", $comentarios="")
	{
		$this->_idproducto=$idproducto;
		$this->_idproveedor=$idproveedor;
		$this->_idcodigoprodinterno=$idcodigoprodinterno;
		$this->_stock=$stock;
		$this->_regalo=$regalo;
		$this->_incremento=$incremento;
		$this->_comentarios=$comentarios; 
        $this->_habilitado=1;
	}
	/*Metodos Publicos*/
	function setIdProducto($idproducto)
	{
		$this->_idproducto = $idproducto;
	}
	function getIdProducto()
	{
		return $this->_idproducto;
	}
	function setIdProveedor($idproveedor)
	{
		$this->_idproveedor = $idproveedor;
	}
	function getIdProveedor()
	{
		return $this->_idproveedor;
	}
	function setIdCodigoProdInterno($idcodigoprodinterno)
	{
		$this->_idcodigoprodinterno = $idcodigoprodinterno;
	}
	function getIdCodigoProdInterno()
	{
		return $this->_idcodigoprodinterno;
	}
	function setStock($stock)
	{
		$this->_stock = $stock;
	}
	function getStock()
	{
		return $this->_stock;
	}
	function setRegalo($regalo)
	{
		$this->_regalo = $regalo;
	}
	function getRegalo()
	{
		return $this->_regalo;
	}
	function setIncremento($incremento)
	{
		$this->_incremento = $incremento;
	}
	function getIncremento()
	{
		return $this->_incremento;
	}
	function setComentarios($comentarios)
    {
        $this->_comentarios = $comentarios;
    }
    function getComentarios()
    {
        return $this->_comentarios;
    }
    function setHabilitado($habilitado)
    {
        $this->_habilitado = $habilitado;
    }
    function getHabilitado()
    {
        return $this->_habilitado;
    }
    function addAtributo(productos_x_atributos $atributo)
    {
        $this->_atributos[]=$atributo;
    }
    function getAtributos()
    {
        return $this->_atributos;
    }
    function getAtributo($index)
    {
        return $this->_atributos[$index];
    }
    function removeAtributo($index)
    {
        unset($this->_atributos[$index]);
    }
	/**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/    
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/    
    }
    /**
    * toString devuelve un string con todos los datos de la clase
    * @return String
    */
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
    /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if(!is_array($valor))
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
	/*Destructor*/
	function __destruct()
	{
	}
}
class productos_x_atributos
{
	/*Datos Privados*/
	protected $_idatributoclase;//Integer
	protected $_idcodigoprodinterno;//Integer
	protected $_valor;//String
 	/*Constructor*/
	function __construct($idatributoclase=0, $idcodigoprodinterno=0, $valor="")
	{
		$this->_idatributoclase=$idatributoclase;
		$this->_idcodigoprodinterno=$idcodigoprodinterno;
		$this->_valor=$valor; 
	}
	/*Metodos Publicos*/
	function setidAtributoClase($idatributoclase)
	{
		$this->_idatributoclase = $idatributoclase;
	}
	function getidAtributoClase()
	{
		return $this->_idatributoclase;
	}
	function setIdCodigoProdInterno($idcodigoprodinterno)
	{
		$this->_idcodigoprodinterno = $idcodigoprodinterno;
	}
	function getIdCodigoProdInterno()
	{
		return $this->_idcodigoprodinterno;
	}
	function setValor($valor)
	{
		$this->_valor = $valor;
	}
	function getValor()
	{
		return $this->_valor;
	}
	/**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/    
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/    
    }
    /**
    * toString devuelve un string con todos los datos de la clase
    * @return String
    */
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
    /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if(!is_array($valor))
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
	/*Destructor*/
	function __destruct()
	{
	}
}
?>