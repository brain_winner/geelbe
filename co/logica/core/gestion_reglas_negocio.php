<?php
    class ReglasNegocioGestion
    {
        private $_idusuario;
        private $_reglaext=null;
        private $_idcampania=null;
        private $_idproductos=null;

//	old	public function NuevoRegistroUsuario($IdUsuario)
        public function NuevoRegistroUsuario($IdUsuario,$Padrino)
        {
            try
            {
				$idTipoRegla = ($Padrino == "clubmovistar1")? 16 : 4 ;
            	$collReglas = dmReglasNegocio::getReglaByTipoUnicaVez($idTipoRegla, $IdUsuario);
                dmReglasNegocio::AplicarReglas($collReglas, $IdUsuario, null, null, null);
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
        public function Apadrinamiento($IdUsuario, $Importe)
        {
            try
            {
                $objUsuarioPadrino = dmReglasNegocio::getPadrinoByIdUsuario($IdUsuario);
                $CantidadVecesReglaAplicada = dmReglasNegocio::getCantidadReglasAplicadasByIdTipo(6,$objUsuarioPadrino->getIdUsuario(),$IdUsuario);
                if($CantidadVecesReglaAplicada[0]["Cantidad"] <=0 && $objUsuarioPadrino->getIdUsuario() >0)
                {
                	$idTipoRegla = ($objUsuarioPadrino->getPadrino() == "clubmovistar1")? 17 : 6 ;
                    $collReglas = dmReglasNegocio::getReglaByTipoUnicaVez($idTipoRegla, $objUsuarioPadrino->getIdUsuario(), $IdUsuario);
                    dmReglasNegocio::AplicarReglas($collReglas, $objUsuarioPadrino->getIdUsuario(), $Importe, null, null, $IdUsuario);
                }
                else
                {
                    throw new Exception("El padrino no existe o ya tiene el credito aplicado.");
                }
                return $objUsuarioPadrino->getNombreUsuario();
            }
            catch(exception $e)
            {
                throw $e;
            }
        }
    }
?>
