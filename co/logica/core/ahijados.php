<?
/**
   *Constructor de la clase clsahijados
   */
	class ahijados{		
		protected $_idahijado = 0;	
		protected $_idusuario = 0;	
		protected $_email = "";	
//		protected $_fecha_entrada = "";
		
		
		
		/**	
		 *Constructor de la clase clsahijados	
		 * *@var long idahijado 	
		 * *@var long idusuario 	
		 * *@var string email 	
		 * 
		 **/	
		function __construct($idahijado = 0,$idusuario = 0,$email = "")	
		{		
			$this->setIdAhijado($idahijado);		
			$this->setIdUsuario($idusuario);		
			$this->setEmail($email);	
			//$this->setfecha_entrada(date("Y/m/d H:i"));
		}		
		/**	* Set fecha_entrada
		 * @var datetime fecha_entrada	
		 */	
/*		function setfecha_entrada($fecha)	
		{		
			$this->_fecha_entrada = $fecha;	
		}
*/
		/**	
		 * Get fecha_entrada 	
		 * @return datetime fecha_entrada	
		 **/	
/*
		function getfecha_entrada()	
		{		
			return $this->_fecha_entrada;	
		}	
*/		
		/** Set IdAhijado 	
		 *@var long IdAhijado	
		 */	
		function setIdAhijado($idahijado)	
		{		
			$this->_idahijado = $idahijado;	
		}

		/**	
		 * Get IdAhijado 	
		 * @return long IdAhijado	
		 **/	
		function getIdAhijado()	
		{		
			return $this->_idahijado;	
		}	
				
		
		/** Set IdUsuario 	
		 * @var long IdUsuario	
		 **/	
		function setIdUsuario($idusuario)	
		{		
			$this->_idusuario = $idusuario;	
		}	
		
		/** Get IdUsuario 	
		 * @return long IdUsuario	
		 **/	
		function getIdUsuario()	
		{		
			return $this->_idusuario;	
		}	
		
		/**	
		 * Set Email 	
		 * @var string Email	
		 **/	
		function setEmail($email)	
		{		
			$this->_email = $email;	
		}	
		
		/**	
		 * Get Email 	
		 * @return string Email	
		 **/	
		function getEmail()	
		{		
			return $this->_email;	
		}	
		
		/**	
		 * Set para llamar de forma dinamica a las propiedades	
		 * @var Valor	
		 * @var String atributo	
		 */	
		function __set($valor,$atributo)	
		{		
			if(isset($this->{"_".strtolower($atributo)}))		   
				$this->{"_".strtolower($atributo)} = $valor;		
				/*else		   throw new Exception("No existes la variable privada $atributo");*/	
		}	
		
		/**	
		 * Get para devolver de forma dinamica los valores de las propiedades	
		 * @var String atributo	* @return String	
		 */	
		function __get($atributo)	
		{		
			if(isset($this->{"_".strtolower($atributo)}))		   
				return $this->{"_".strtolower($atributo)};		
				/*else		   throw new Exception("No existes la variable privada $atributo");*/	
		}	
		
		/**	
		 * toString devuelve un string con todos los datos de la clase	
		 * @return String	
		 */	
		function __toString()	
		{	    
			$Codigo = "";	    
			foreach ($this as $propiedad => $valor)	    
			{	        
				$Codigo .=$propiedad ."=".$valor."\n";	    
			}	    
				return $Codigo;	
		}	
		
		/**	
		 * @desc Devuelve un array con las propiedades y valores de la clase.	
		 * @return array.	
		 */	
		function __toArray()	
		{	    
			$Ar = array();	    
			foreach ($this as $propiedad => $valor)	    
			{	        
				$Ar[$propiedad] = $valor;	    
			}	    
			return $Ar;	
		}
	}
	?>