<?
class DatosUsuariosDirecciones
{
    function __construct()
    {
        $this->_idusuario="";
        $this->_tipodireccion=0;
        $this->_domicilio="";
        $this->_numero="";
        $this->_escalera="";
        $this->_piso="";
        $this->_puerta="";
        $this->_cp="";
        $this->_idprovincia=0;
        $this->_idciudad=0;
        $this->_idpais=0;
        $this->_poblacion="";
        $this->_tipocalle="";
        $this->_telefono="";
        $this->_completo=0;
    }
//DATOS PRIVADOS
        protected $_idusuario;
        protected $_tipodireccion;
        protected $_domicilio;
        protected $_numero;
        protected $_escalera;
        protected $_piso;
        protected $_puerta;
        protected $_cp;
        protected $_idprovincia;
        protected $_idciudad;
        protected $_tipocalle;
        protected $_idpais;
        protected $_poblacion;
        protected $_telefono;
        protected $_completo;
//PROPIEDADES PUBLICAS
    function setIdUsuario($idusuario)
    {
            $this->_idusuario = $idusuario;
    }
    function getIdUsuario()
    {
            return $this->_idusuario;
    }
    function setTipoDireccion($tipodireccion)
    {
            $this->_tipodireccion = $tipodireccion;
    }
    function getTipoDireccion()
    {
            return $this->_tipodireccion;
    }
    function setIdPais($idpais)
    {
            $this->_idpais = $idpais;
    }
    function getIdPais()
    {
            return $this->_idpais;
    }
    function setPoblacion($poblacion)
    {
            $this->_poblacion = $poblacion;
    }
    function getPoblacion()
    {
            return stripslashes($this->_poblacion);
    }
    function setTipoCalle($tipocalle)
    {
            $this->_tipocalle = $tipocalle;
    }
    function getTipoCalle()
    {
            return $this->_tipocalle;
    }
    function setDomicilio($domicilio)
    {
            $this->_domicilio = $domicilio;
    }
    function getDomicilio()
    {
            return stripslashes($this->_domicilio);
    }
    function setNumero($numero)
    {
        $this->_numero = $numero;
    }
    function getNumero()
    {
        return $this->_numero;
    }
    function setEscalera($escalera)
    {
            $this->_escalera = $escalera;
    }
    function getEscalera()
    {
            return $this->_escalera;
    }
    function setPiso($piso)
    {
            $this->_piso = $piso;
    }
    function getPiso()
    {
            return $this->_piso;
    }
    function setPuerta($puerta)
    {
            $this->_puerta = $puerta;
    }
    function getPuerta()
    {
            return $this->_puerta;
    }    
    function setCP($cp)
    {
            $this->_cp = $cp;
    }
    function getCP()
    {
            return $this->_cp;
    }
    function setIdProvincia($idprovincia)
    {
            $this->_idprovincia = $idprovincia;
    }
    function getIdProvincia()
    {
            return $this->_idprovincia;
    }
    function setIdCiudad($idciudad)
    {
            $this->_idciudad = $idciudad;
    }
    function getIdCiudad()
    {
            return $this->_idciudad;
    }
    function setTelefono($telefono)
    {
            $this->_telefono = $telefono;
    }
    function getTelefono()
    {
            return $this->_telefono;
    }    
    function esCompleto()
    {
        foreach($this as $key => $valor)
        {
            if($valor == "")
            {
                $this->_completo=0;
                return 0;
            }
        }
        $this->_completo=1;
        return 1;
    }
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
}
class DatosUsuariosPersonales
{
    function __construct()
    {
        $this->_idusuario="";
        $this->_fechanacimiento="";
        $this->_nombre="";
        $this->_apellido="";
        $this->_apellido2="";
        $this->_tipodoc="";
        $this->_nrodoc="";
        $this->_codtelefono="";
        $this->_telefono="";
        $this->_email="";
        $this->_cif="";
        $this->_idtipousuario="";
        $this->_es_enviardatoscorreo=0;
        $this->_idtratamiento="";
        $this->_celuempresa="";
        $this->_celucodigo="";
        $this->_celunumero="";
        $this->_es_completo=0;
    }
//DATOS PRIVADOS
     protected $_idusuario;
     protected $_fechanacimiento;
     protected $_nombre;
     protected $_apellido;
     protected $_apellido2;
     protected $_tipodoc;
     protected $_nrodoc;
     protected $_codtelefono;
     protected $_telefono;
     protected $_email;
     protected $_cif;
     protected $_idtipousuario;
     protected $_es_enviardatoscorreo;
     protected $_idtratamiento;
     protected $_celuempresa;
     protected $_celucodigo;
     protected $_celunumero; 
     protected $_es_completo; 

//PROPIEDADES PUBLICAS
    function setIdUsuario($idusuario)
    {
            $this->_idusuario = $idusuario;
    }
    function getIdUsuario()
    {
            return $this->_idusuario;
    }
    function setFechaNacimiento($fechanacimiento)
    {
            $this->_fechanacimiento = $fechanacimiento;
    }
    function getFechaNacimiento()
    {
            return $this->_fechanacimiento;
    }
    function setNombre($nombre)
    {
            $this->_nombre = $nombre;
    }
    function getNombre()
    {
            return stripslashes($this->_nombre);
    }
    function setApellido($apellido)
    {
            $this->_apellido = $apellido;
    }
    function getApellido()
    {
            return stripslashes($this->_apellido);
    }
    function setApellido2($apellido)
    {
            $this->_apellido2 = $apellido;
    }
    function getApellido2()
    {
            return stripslashes($this->_apellido2);
    }
    function setTipoDoc($tipodoc)
    {
            $this->_tipodoc = $tipodoc;
    }
    function getTipoDoc()
    {
            return $this->_tipodoc;
    }
    function setNroDoc($nrodoc)
    {
            $this->_nrodoc = $nrodoc;
    }
    function getNroDoc()
    {
            return $this->_nrodoc;
    }   
    function setCodTelefono($codtelefono)
    {
            $this->_codtelefono = $codtelefono;
    }
    function getCodTelefono()
    {
            return $this->_codtelefono;
    }
    function setTelefono($telefono)
    {
            $this->_telefono = $telefono;
    }
    function getTelefono()
    {
            return $this->_telefono;
    }
    function setEmail($email)
    {
            $this->_email = $email;
    }
    function getEmail()
    {
            return $this->_email;
    }
    function setCIF($cif)
    {
            $this->_cif = $cif;
    }
    function getCIF()
    {
            return $this->_cif;
    }
    function setIdTipoUsuario($idtipousuario)
    {
            $this->_idtipousuario = $idtipousuario;
    }
    function getIdTipoUsuario()
    {
            return $this->_idtipousuario;
    }
    function setes_EnviarDatosCorreo($es_enviardatoscorreo)
    {
            $this->_es_enviardatoscorreo = $es_enviardatoscorreo;
    }
    function getes_EnviarDatosCorreo()
    {
            return $this->_es_enviardatoscorreo;
    }
    function setIdTratamiento($idtratamiento)
    {
            $this->_idtratamiento = $idtratamiento;
    }
    function getIdTratamiento()
    {
            return $this->_idtratamiento;
    }
    function setceluempresa($celuempresa)
    {
            $this->_celuempresa = $celuempresa;
    }
    function getceluempresa()
    {
            return $this->_celuempresa;
    }
    function setcelucodigo($celucodigo)
    {
            $this->_celucodigo = $celucodigo;
    }
    function getcelucodigo()
    {
            return $this->_celucodigo;
    }

    function setcelunumero($celunumero)
    {
            $this->_celunumero = $celunumero;
    }
    function getcelunumero()
    {
            return $this->_celunumero;
    }
    
    function esCompleto()
    {
        foreach($this as $key => $valor)
        {
            if($valor == "")
            {
                $this->_es_completo=0;
                return 0;
            }
        }
        $this->_es_completo=1;
        return 1;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return stripslashes($this->{"_".strtolower($atributo)});
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
class usuarios
{
    /*Datos Privados*/
    protected $_idusuario;//Long
    protected $_nombreusuario;//String
    protected $_clave;//String
    protected $_es_provisoria;//Integer
    protected $_padrino;//String
    protected $_fechaingreso;//Date
    protected $_idperfil;//Integer
    protected $_es_activa;//Integer
    protected $_motivobaja;//Integer
    protected $_usuariobaja;//Integer
    protected $_datos=null;//Integer
    protected $_direcciones=array();//Integer
    protected $_secciones=array();//array
    protected $_mail_suscription;
    /*Constructor*/
    function __construct($idusuario=0, $nombreusuario="", $clave="", $es_provisoria=0, $padrino="", $fechaingreso="", $idperfil=0, $es_activa=0)
    {
        $this->_idusuario=$idusuario;
        $this->_nombreusuario=$nombreusuario;
        $this->_clave=$clave;
        $this->_es_provisoria=$es_provisoria;
        $this->_padrino=$padrino;
        $this->_fechaingreso=$fechaingreso;
        $this->_idperfil=$idperfil;
        $this->_es_activa=$es_activa;
        $this->_datos= new DatosUsuariosPersonales();
        $this->_direcciones[0] = new DatosUsuariosDirecciones();
        $this->_direcciones[1] = new DatosUsuariosDirecciones();
        $this->_secciones = $secciones;
        $this->_motivobaja = '';
        $this->_usuariobaja = '';
        $this->_mail_suscription = 0;
    }
    /*Metodos Publicos*/
    /*function setReferenciador($index, $valor)
    {
        $this->_referenciador[$index] = $valor;
    }*/
    function setIdUsuario($idusuario)
    {
        $this->_idusuario = $idusuario;
    }
    function getIdUsuario()
    {
        return $this->_idusuario;
    }
    function setNombreUsuario($nombreusuario)
    {
        $this->_nombreusuario = $nombreusuario;
    }
    function getNombreUsuario()
    {
        return $this->_nombreusuario;
    }
    function setClave($clave)
    {
        $this->_clave = $clave;
    }
    function getClave()
    {
        return $this->_clave;
    }
    function setes_Provisoria($es_provisoria)
    {
        $this->_es_provisoria = $es_provisoria;
    }
    function getes_Provisoria()
    {
        return $this->_es_provisoria;
    }
    function setPadrino($padrino)
    {
        $this->_padrino = $padrino;
    }
    function getPadrino()
    {
        return $this->_padrino;
    }
    function setFechaIngreso($fechaingreso)
    {
        $this->_fechaingreso = $fechaingreso;
    }
    function getFechaIngreso()
    {
        return $this->_fechaingreso;
    }
    function setIdPerfil($idperfil)
    {
        $this->_idperfil = $idperfil;
    }
    function getIdPerfil()
    {
        return $this->_idperfil;
    }
    function setes_Activa($es_activa)
    {
        $this->_es_activa = $es_activa;
    }
    function getes_Activa()
    {
        return $this->_es_activa;
    }
    function setMotivoBaja($es_activa)
    {
        $this->_motivobaja = $es_activa;
    }
    function getMotivoBaja()
    {
        return $this->_motivobaja;
    }
    function setUsuarioBaja($es_activa)
    {
        $this->_usuariobaja = $es_activa;
    }
    function getUsuarioBaja()
    {
        return $this->_usuariobaja;
    }
    function setMailSuscription($es_activa)
    {
        $this->_mail_suscription = $es_activa;
    }
    function getMailSuscription()
    {
        return $this->_mail_suscription;
    }
    function setDatos($datos)
    {
        $this->_datos = $datos;
    }
    function getDatos()
    {
        return $this->_datos;
    }
    function setSecciones($secciones)
    {
        $this->_secciones = $secciones;
    }
    function getSecciones()
    {
        return $this->_secciones;
    }    
    function setDirecciones($index, $direccion)
    {
        $this->_direcciones[$index] = $direccion;
    }
    function getDirecciones($index)
    {
        return $this->_direcciones[$index];
    }
    function getAllDirecciones()
    {
        return $this->_direcciones;
    }
    /*function getReferenciador($index)
    {
        return $this->_referenciador[$index] ;
    }*/
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return stripslashes($this->{"_".strtolower($atributo)});
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /*Destructor*/
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if($propiedad != "_datos" && $propiedad !="_direcciones")
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>
