<?
 class impuestos
 {
	function __construct()
	{
        $this->_nombre="";
        $this->_idimpuesto=0;
		$this->_tipo=0;
		$this->_operador="";
        $this->_habilitado=0;
        $this->_impuestos_x_provincias=array();
	}
//DATOS PRIVADOS
	 protected $_idimpuesto;
	 protected $_tipo;
	 protected $_operador;
     protected $_nombre;
     protected $_habilitado;
     protected $_impuestos_x_provincias;
//PROPIEDADES PUBLICAS
    function setNombre($nombre)
    {
        $this->_nombre = $nombre;
    }
    function getNombre()
    {
        return $this->_nombre;
    }
	function setIdImpuesto($idimpuesto)
	{
			$this->_idimpuesto = $idimpuesto;
	}
	function getIdImpuesto()
	{
			return $this->_idimpuesto;
	}
	function setTipo($tipo)
	{
			$this->_tipo = $tipo;
	}
	function getTipo()
	{
			return $this->_tipo;
	}
	function setOperador($operador)
    {
            $this->_operador = $operador;
    }
    function getOperador()
    {
            return $this->_operador;
    }
    function setHabilitado($habilitado)
    {
            $this->_habilitado = $habilitado;
    }
    function getHabilitado()
    {
            return $this->_habilitado;
    }
    function addImpuestoXProvincia(impuestos_x_provincias $impuesto)
    {
        $this->_impuestos_x_provincias[] = $impuesto;
    }
    function removeImpuestoXProvincia($index)
    {
        unset($this->_impuestos_x_provincias[$index]);
    }
    function getImpuestosXProvincias()
    {
        return $this->_impuestos_x_provincias;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /*Destructor*/
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if($propiedad != "_impuestos_x_provincias")
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
 class impuestos_x_provincias
 {
    function __construct()
    {
        $this->_idimpuesto="";
        $this->_idprovincia="";
        $this->_valor="";
    }
//DATOS PRIVADOS
     protected $_idimpuesto;
     protected $_idprovincia;
     protected $_valor;
//PROPIEDADES PUBLICAS
    function setIdImpuesto($idimpuesto)
    {
            $this->_idimpuesto = $idimpuesto;
    }
    function getIdImpuesto()
    {
            return $this->_idimpuesto;
    }
    function setIdProvincia($idprovincia)
    {
            $this->_idprovincia = $idprovincia;
    }
    function getIdProvincia()
    {
            return $this->_idprovincia;
    }
    function setValor($valor)
    {
            $this->_valor = $valor;
    }
    function getValor()
    {
            return $this->_valor;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /*Destructor*/
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>
