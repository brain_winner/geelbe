<?
/**
*Constructor de la clase clsreglasnegocio
*/
class reglasnegocio
{
    
    protected $_idregla = 0;
    protected $_nombre = "";
    protected $_descripcion = "";
    protected $_caducidad = 0;
    protected $_desde = 0;
    protected $_hasta = 0;
    protected $_idtiporegla = 0;
    protected $_reglaext = "";
    protected $_idcampania = 0;
    protected $_idproducto = 0;
    protected $_beneficio1 = 0;
    protected $_aplica1 = "";
    protected $_beneficio2 = 0;
    protected $_aplica2 = "";

    /**
    *Constructor de la clase clsreglasnegocio
    *@var long idregla 
    *@var string nombre 
    *@var string descripcion 
    *@var int caducidad 
    *@var long desde 
    *@var long hasta 
    *@var long idtiporegla 
    *@var string reglaext 
    *@var long idcampania 
    *@var long idproducto 
    *@var long beneficio1 
    *@var string aplica1 
    *@var long beneficio2 
    *@var string aplica2 
    */
    function __construct($idregla = 0,$nombre = "",$descripcion = "",$caducidad = 0,$desde = 0,$hasta = 0,$idtiporegla = 0,$reglaext = "",$idcampania = 0,$idproducto = 0,$beneficio1 = 0,$aplica1 = "",$beneficio2 = 0,$aplica2 = "")
    {
        $this->setIdRegla($idregla);
        $this->setNombre($nombre);
        $this->setDescripcion($descripcion);
        $this->setCaducidad($caducidad);
        $this->setDesde($desde);
        $this->setHasta($hasta);
        $this->setIdTipoRegla($idtiporegla);
        $this->setReglaEXT($reglaext);
        $this->setIdCampania($idcampania);
        $this->setIdCodigoProdInterno($idproducto);
        $this->setBeneficio1($beneficio1);
        $this->setAplica1($aplica1);
        $this->setBeneficio2($beneficio2);
        $this->setAplica2($aplica2);
    }
    
    /**
    * Set IdRegla 
    * @var long IdRegla
    */
    function setIdRegla($idregla)
    {
        $this->_idregla =$idregla;
    }
    /**
    * Get IdRegla 
    * @return long IdRegla
    */
    function getIdRegla()
    {
        return $this->_idregla;
    }
    /**
    * Set Nombre 
    * @var string Nombre
    */
    function setNombre($nombre)
    {
        $this->_nombre =$nombre;
    }
    /**
    * Get Nombre 
    * @return string Nombre
    */
    function getNombre()
    {
        return $this->_nombre;
    }
    /**
    * Set Descripcion 
    * @var string Descripcion
    */
    function setDescripcion($descripcion)
    {
        $this->_descripcion =$descripcion;
    }
    /**
    * Get Descripcion 
    * @return string Descripcion
    */
    function getDescripcion()
    {
        return $this->_descripcion;
    }
    /**
    * Set Caducidad 
    * @var int Caducidad
    */
    function setCaducidad($caducidad)
    {
        $this->_caducidad =$caducidad;
    }
    /**
    * Get Caducidad 
    * @return int Caducidad
    */
    function getCaducidad()
    {
        return $this->_caducidad;
    }
    /**
    * Set Desde 
    * @var long Desde
    */
    function setDesde($desde)
    {
        $this->_desde =$desde;
    }
    /**
    * Get Desde 
    * @return long Desde
    */
    function getDesde()
    {
        return $this->_desde;
    }
    /**
    * Set Hasta 
    * @var long Hasta
    */
    function setHasta($hasta)
    {
        $this->_hasta =$hasta;
    }
    /**
    * Get Hasta 
    * @return long Hasta
    */
    function getHasta()
    {
        return $this->_hasta;
    }
    /**
    * Set IdTipoRegla 
    * @var long IdTipoRegla
    */
    function setIdTipoRegla($idtiporegla)
    {
        $this->_idtiporegla =$idtiporegla;
    }
    /**
    * Get IdTipoRegla 
    * @return long IdTipoRegla
    */
    function getIdTipoRegla()
    {
        return $this->_idtiporegla;
    }
    /**
    * Set ReglaEXT 
    * @var string ReglaEXT
    */
    function setReglaEXT($reglaext)
    {
        $this->_reglaext =$reglaext;
    }
    /**
    * Get ReglaEXT 
    * @return string ReglaEXT
    */
    function getReglaEXT()
    {
        return $this->_reglaext;
    }
    /**
    * Set IdCampania 
    * @var long IdCampania
    */
    function setIdCampania($idcampania)
    {
        $this->_idcampania =$idcampania;
    }
    /**
    * Get IdCampania 
    * @return long IdCampania
    */
    function getIdCampania()
    {
        return $this->_idcampania;
    }
    /**
    * Set IdCodigoProdInterno 
    * @var long IdCodigoProdInterno
    */
    function setIdCodigoProdInterno($idproducto)
    {
        $this->_idproducto =$idproducto;
    }
    /**
    * Get IdCodigoProdInterno 
    * @return long IdCodigoProdInterno
    */
    function getIdCodigoProdInterno()
    {
        return $this->_idproducto;
    }
    /**
    * Set Beneficio1 
    * @var long Beneficio1
    */
    function setBeneficio1($beneficio1)
    {
        $this->_beneficio1 =$beneficio1;
    }
    /**
    * Get Beneficio1 
    * @return long Beneficio1
    */
    function getBeneficio1()
    {
        return $this->_beneficio1;
    }
    /**
    * Set Aplica1 
    * @var string Aplica1
    */
    function setAplica1($aplica1)
    {
        $this->_aplica1 =$aplica1;
    }
    /**
    * Get Aplica1 
    * @return string Aplica1
    */
    function getAplica1()
    {
        return $this->_aplica1;
    }
    /**
    * Set Beneficio2 
    * @var long Beneficio2
    */
    function setBeneficio2($beneficio2)
    {
        $this->_beneficio2 =$beneficio2;
    }
    /**
    * Get Beneficio2 
    * @return long Beneficio2
    */
    function getBeneficio2()
    {
        return $this->_beneficio2;
    }
    /**
    * Set Aplica2 
    * @var string Aplica2
    */
    function setAplica2($aplica2)
    {
        $this->_aplica2 =$aplica2;
    }
    /**
    * Get Aplica2 
    * @return string Aplica2
    */
    function getAplica2()
    {
        return $this->_aplica2;
    }

    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           $this->{"_".strtolower($atributo)} = $valor;
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           return $this->{"_".strtolower($atributo)};
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * toString devuelve un string con todos los datos de la clase
    * @return String
    */
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
    /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>