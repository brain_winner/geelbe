<?
 class pedidos
 {
    function __construct()
    {
        $this->_idpedido=0;
        $this->_fecha = date("Y-m-d");
        $this->_fechaenvio = date("Y-m-d");
        $this->_idusuario = 0;
        $this->_idformapago = 0;
        $this->_idformaenvio = 0;
        $this->_idestadopedidos = 0;
        $this->_idcampania = 0;
        $this->_iddescuento = 0;
        $this->_descuento = 0;
        $this->_gastosenvio = 0;
        $this->_total=0;
        $this->_nroenvio=0;
        $this->_motivoanulacion="";
        $this->_productos = array();
        $this->_direccion = new pedidos_direcciones();
    }
//DATOS PRIVADOS
     protected $_idpedido;
     protected $_fecha;
     protected $_fechaenvio;
     protected $_idusuario;
     protected $_idformapago;
     protected $_idformaenvio;
     protected $_idestadopedidos;
     protected $_idcampania;
     protected $_iddescuento;
     protected $_descuento;
     protected $_gastosenvio;
     protected $_productos;
     protected $_direccion;
     protected $_total;
     protected $_nroenvio;
     protected $_motivoanulacion;
     protected $_idtarjeta;
//PROPIEDADES PUBLICAS
    function setIdPedido($idpedido)
    {
            $this->_idpedido = $idpedido;
    }
    function getIdPedido()
    {
            return $this->_idpedido;
    }
    function setFecha($fecha)
    {
            $this->_fecha = $fecha;
    }
    function getFecha()
    {
            return $this->_fecha;
    }
    function setFechaEnvio($fechaenvio)
    {
            $this->_fechaenvio = $fechaenvio;
    }
    function getFechaEnvio()
    {
            return $this->_fechaenvio;
    }
    function setIdUsuario($idusuario)
    {
            $this->_idusuario = $idusuario;
    }
    function getIdUsuario()
    {
            return $this->_idusuario;
    }
    function setIdFormaPago($idformapago)
    {
            $this->_idformapago = $idformapago;
    }
    function getIdFormaPago()
    {
            return $this->_idformapago;
    }
    function setIdFormaEnvio($idformaenvio)
    {
            $this->_idformaenvio = $idformaenvio;
    }
    function getIdFormaEnvio()
    {
            return $this->_idformaenvio;
    }
    function setIdEstadoPedidos($idestadopedidos)
    {
            $this->_idestadopedidos = $idestadopedidos;
    }
    function getIdEstadoPedidos()
    {
            return $this->_idestadopedidos;
    }
    function setIdCampania($idcampania)
    {
            $this->_idcampania = $idcampania;
    }
    function getIdCampania()
    {
            return $this->_idcampania;
    }    
    function setIdDescuento($idcampania)
    {
            $this->_iddescuento = $idcampania;
    }
    function getIdDescuento()
    {
            return $this->_iddescuento;
    }    
    function setDescuento($idcampania)
    {
            $this->_descuento = $idcampania;
    }
    function getDescuento()
    {
            return $this->_descuento;
    }    
    function setGastosEnvio($gastosenvio)
    {
            $this->_gastosenvio = $gastosenvio;
    }
    function getGastosEnvio()
    {
    	if($this->_idpedido != '' && is_numeric($this->_idpedido)) {
    		$oConexion = Conexion::nuevo();
	        $oConexion->Abrir();
	        $oConexion->SetQuery('SELECT envio FROM pedidos_bonificado WHERE IdPedido ='.$this->_idpedido);
	        $data = $oConexion->DevolverQuery();
	        if(count($data) > 0)
	        	return $data[0]['envio'];
	    }
    
            return $this->_gastosenvio;
    }
    function setTotal($total)
    {
            $this->_total = $total;
    }
    function getTotal()
    {
            return $this->_total;
    }
    function setProducto(productos_x_pedidos $objProducto)
    {
            $this->_productos[] = $objProducto;
    }
    function removeProducto($index)
    {
            unset($this->_productos[$index]);
    }
    function getProducto($index)
    {
            return $this->_productos[$index];
    }
    function getProductos()
    {
            return $this->_productos;
    }
    function setDireccion(pedidos_direcciones $direccion)
    {
            $this->_direccion = $direccion;
    }
    function getDireccion()
    {
            return $this->_direccion;
    }
    public function setNroEnvio($nroenvio)
    {
        $this->_nroenvio=$nroenvio;
    }
    public function getNroEnvio()
    {
        return $this->_nroenvio;
    }
    public function setMotivoAnulacion($motivoanulacion)
    {
        $this->_motivoanulacion=$motivoanulacion;
    }
    public function getMotivoAnulacion()
    {
        return $this->_motivoanulacion;
    }
    public function setIdTarjeta($motivoanulacion)
    {
        $this->_idtarjeta=$motivoanulacion;
    }
    public function getIdTarjeta()
    {
        return $this->_idtarjeta;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if(!is_array($valor) && $propiedad != "_direccion")
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
 class productos_x_pedidos
 {
    function __construct()
    {
        $this->_idpedido = 0;
        $this->_idcodigoprodinterno = 0;
        $this->_cantidad = 0;
        $this->_precio = 0;
    }
//DATOS PRIVADOS
     protected $_idpedido;
     protected $_idcodigoprodinterno;
     protected $_cantidad;
     protected $_precio;
//PROPIEDADES PUBLICAS
    function setIdPedido($idpedido)
    {
            $this->_idpedido = $idpedido;
    }
    function getIdPedido()
    {
            return $this->_idpedido;
    }
    function setIdCodigoProdInterno($idcodigoprodinterno)
    {
            $this->_idcodigoprodinterno = $idcodigoprodinterno;
    }
    function getIdCodigoProdInterno()
    {
            return $this->_idcodigoprodinterno;
    }
    function setCantidad($cantidad)
    {
            $this->_cantidad = $cantidad;
    }
    function getCantidad()
    {
            return $this->_cantidad;
    }
    function setPrecio($precio)
    {
            $this->_precio = $precio;
    }
    function getPrecio()
    {
            return $this->_precio;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if(!is_array(($valor)))
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
/**
*Constructor de la clase clspedidos_direcciones
*/
class pedidos_direcciones
{
    
    protected $_idpedido = 0;
    protected $_nombre;
    protected $_apellido;
    protected $_nrodoc;
    protected $_codtelefono;
    protected $_celuempresa;
    protected $_celucodigo;
    protected $_celunumero; 
    protected $_idusuario = 0;
    protected $_domicilio = "";
    protected $_numero = "";
    protected $_escalera = "";
    protected $_piso = "";
    protected $_puerta = "";
    protected $_cp = "";
    protected $_idprovincia = 0;
    protected $_idpais = 0;
    protected $_tipocalle = "";
    protected $_poblacion = "";
    protected $_telefono = "";
    protected $_completo = 0;
    protected $_horarioentrega="";
    protected $_idciudad="";

    /**
    *Constructor de la clase clspedidos_direcciones
    *@var long idpedido 
    *@var long idusuario 
    *@var string domicilio 
    *@var string numero 
    *@var string escalera 
    *@var string piso 
    *@var string puerta 
    *@var string cp 
    *@var long idprovincia 
    *@var long idpais 
    *@var string tipocalle 
    *@var string poblacion 
    *@var string telefono 
    *@var int completo 
    */
    function __construct($idpedido = 0,$idusuario = 0,$domicilio = "",$numero = "",$escalera = "",$piso = "",$puerta = "",$cp = "",$idprovincia = 0,$idpais = 0,$tipocalle = "",$poblacion = "",$telefono = "",$completo = 0)
    {
        $this->setIdPedido($idpedido);
        $this->setIdUsuario($idusuario);
        $this->setDomicilio($domicilio);
        $this->setNumero($numero);
        $this->setEscalera($escalera);
        $this->setPiso($piso);
        $this->setPuerta($puerta);
        $this->setCP($cp);
        $this->setIdProvincia($idprovincia);
        $this->setIdPais($idpais);
        $this->setTipoCalle($tipocalle);
        $this->setPoblacion($poblacion);
        $this->setTelefono($telefono);
        $this->setCompleto($completo);
    }
    
    /**
    * Set IdPedido 
    * @var long IdPedido
    */
    function setIdPedido($idpedido)
    {
        $this->_idpedido =$idpedido;
    }
    /**
    * Get IdPedido 
    * @return long IdPedido
    */
    function getIdPedido()
    {
        return $this->_idpedido;
    }
    function setNombre($nombre)
    {
            $this->_nombre = $nombre;
    }
    function getNombre()
    {
            return $this->_nombre;
    }
    function setApellido($apellido)
    {
            $this->_apellido = $apellido;
    }
    function getApellido()
    {
            return $this->_apellido;
    }
    function setNroDoc($nrodoc)
    {
            $this->_nrodoc = $nrodoc;
    }
    function getNroDoc()
    {
            return $this->_nrodoc;
    }   
    function setCodTelefono($codtelefono)
    {
            $this->_codtelefono = $codtelefono;
    }
    function getCodTelefono()
    {
            return $this->_codtelefono;
    }
    function setceluempresa($celuempresa)
    {
            $this->_celuempresa = $celuempresa;
    }
    function getceluempresa()
    {
            return $this->_celuempresa;
    }
    function setcelucodigo($celucodigo)
    {
            $this->_celucodigo = $celucodigo;
    }
    function getcelucodigo()
    {
            return $this->_celucodigo;
    }
    function setcelunumero($celunumero)
    {
            $this->_celunumero = $celunumero;
    }
    function getcelunumero()
    {
            return $this->_celunumero;
    }
    /**
    * Set IdUsuario 
    * @var long IdUsuario
    */
    function setIdUsuario($idusuario)
    {
        $this->_idusuario =$idusuario;
    }
    /**
    * Get IdUsuario 
    * @return long IdUsuario
    */
    function getIdUsuario()
    {
        return $this->_idusuario;
    }
    function setDomicilio($domicilio)
    {
        $this->_domicilio =$domicilio;
    }
    /**
    * Get Domicilio 
    * @return string Domicilio
    */
    function getDomicilio()
    {
        return $this->_domicilio;
    }
    /**
    * Set Numero 
    * @var string Numero
    */
    function setNumero($numero)
    {
        $this->_numero =$numero;
    }
    /**
    * Get Numero 
    * @return string Numero
    */
    function getNumero()
    {
        return $this->_numero;
    }
    /**
    * Set Escalera 
    * @var string Escalera
    */
    function setEscalera($escalera)
    {
        $this->_escalera =$escalera;
    }
    /**
    * Get Escalera 
    * @return string Escalera
    */
    function getEscalera()
    {
        return $this->_escalera;
    }
    /**
    * Set Piso 
    * @var string Piso
    */
    function setPiso($piso)
    {
        $this->_piso =$piso;
    }
    /**
    * Get Piso 
    * @return string Piso
    */
    function getPiso()
    {
        return $this->_piso;
    }
    /**
    * Set Puerta 
    * @var string Puerta
    */
    function setPuerta($puerta)
    {
        $this->_puerta =$puerta;
    }
    /**
    * Get Puerta 
    * @return string Puerta
    */
    function getPuerta()
    {
        return $this->_puerta;
    }
    /**
    * Set CP 
    * @var string CP
    */
    function setCP($cp)
    {
        $this->_cp =$cp;
    }
    /**
    * Get CP 
    * @return string CP
    */
    function getCP()
    {
        return $this->_cp;
    }
    /**
    * Set IdProvincia 
    * @var long IdProvincia
    */
    function setIdProvincia($idprovincia)
    {
        $this->_idprovincia =$idprovincia;
    }
    /**
    * Get IdProvincia 
    * @return long IdProvincia
    */
    function getIdProvincia()
    {
        return $this->_idprovincia;
    }

    function setIdCiudad($idciudad)
    {
        $this->_idciudad =$idciudad;
    }

    function getIdCiudad()
    {
        return $this->_idciudad;
    }
    /**
    * Set IdPais 
    * @var long IdPais
    */
    function setIdPais($idpais)
    {
        $this->_idpais =$idpais;
    }
    /**
    * Get IdPais 
    * @return long IdPais
    */
    function getIdPais()
    {
        return $this->_idpais;
    }
    /**
    * Set TipoCalle 
    * @var string TipoCalle
    */
    function setTipoCalle($tipocalle)
    {
        $this->_tipocalle =$tipocalle;
    }
    /**
    * Get TipoCalle 
    * @return string TipoCalle
    */
    function getTipoCalle()
    {
        return $this->_tipocalle;
    }
    /**
    * Set Poblacion 
    * @var string Poblacion
    */
    function setPoblacion($poblacion)
    {
        $this->_poblacion =$poblacion;
    }
    /**
    * Get Poblacion 
    * @return string Poblacion
    */
    function getPoblacion()
    {
        return $this->_poblacion;
    }
    /**
    * Set Telefono 
    * @var string Telefono
    */
    function setTelefono($telefono)
    {
        $this->_telefono =$telefono;
    }
    /**
    * Get Telefono 
    * @return string Telefono
    */
    function getTelefono()
    {
        return $this->_telefono;
    }
    /**
    * Set Completo 
    * @var int Completo
    */
    function setCompleto($completo)
    {
        $this->_completo =$completo;
    }
    /**
    * Get Completo 
    * @return int Completo
    */
    function getCompleto()
    {
        return $this->_completo;
    }
    public function setHorarioEntrega($horarioentrega)
    {
        $this->_horarioentrega = $horarioentrega;
    }
    public function getHorarioEntrega()
    {
        return $this->_horarioentrega;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           $this->{"_".strtolower($atributo)} = $valor;
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           return $this->{"_".strtolower($atributo)};
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * toString devuelve un string con todos los datos de la clase
    * @return String
    */
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
    /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if(!is_array($valor) && $propiedad != "_direccion")
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>