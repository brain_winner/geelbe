<?
 class marcas
 {
	function __construct()
	{
		$this->_idmarca="";
		$this->_nombre="";
        $this->_descripcion="";
        $this->_habilitado=1;
	}
//DATOS PRIVADOS
	 protected $_idmarca;
	 protected $_nombre;
     protected $_descripcion;
     protected $_habilitado;
//PROPIEDADES PUBLICAS
	function setIdMarca($idmarca)
	{
			$this->_idmarca = $idmarca;
	}
	function getIdMarca()
	{
			return $this->_idmarca;
	}
	function setNombre($nombre)
	{
			$this->_nombre = $nombre;
	}
	function getNombre()
	{
			return $this->_nombre;
	}
    function setDescripcion($descripcion)
    {
            $this->_descripcion = $descripcion;
    }
    function getDescripcion()
    {
            return $this->_descripcion;
    }
    function setHabilitado($habilitado)
    {
            $this->_habilitado = $habilitado;
    }
    function getHabilitado()
    {
            return $this->_habilitado;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
            $this->{"_".strtolower($atributo)} = $valor;
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {        
        if(isset($this->{"_".strtolower($atributo)}))
            return $this->{"_".strtolower($atributo)};
        /*else
            throw new Exception("No existes la variable privada $atributo");*/
    }
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>