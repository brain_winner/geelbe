<?
class DHL {

	private $_importe = 0;
	
    public function DHL($importeToSet = 0) {
    	$this->_importe = $importeToSet;
    }

    function getImporte() {
        return $this->_importe;
    }

}
/**
*Constructor de la clase dhl_zonas_x_provincias
*/
class dhl_zonas_x_provincias
{
    
    protected $_idzonapcia = 0;
    protected $_idprovincia = 0;
    protected $_idzona = 0;

    /**
    *Constructor de la clase dhl_zonas_x_provincias
    *@var long idzonapcia 
    *@var long idprovincia 
    *@var long idzona 
    */
    function __construct($idzonapcia = 0,$idprovincia = 0,$idzona = 0)
    {
        $this->setIdZonaPcia($idzonapcia);
        $this->setIdProvincia($idprovincia);
        $this->setIdZona($idzona);
    }
    
    /**
    * Set IdZonaPcia 
    * @var long IdZonaPcia
    */
    function setIdZonaPcia($idzonapcia)
    {
        $this->_idzonapcia =$idzonapcia;
    }
    /**
    * Get IdZonaPcia 
    * @return long IdZonaPcia
    */
    function getIdZonaPcia()
    {
        return $this->_idzonapcia;
    }
    /**
    * Set IdProvincia 
    * @var long IdProvincia
    */
    function setIdProvincia($idprovincia)
    {
        $this->_idprovincia =$idprovincia;
    }
    /**
    * Get IdProvincia 
    * @return long IdProvincia
    */
    function getIdProvincia()
    {
        return $this->_idprovincia;
    }
    /**
    * Set IdZona 
    * @var long IdZona
    */
    function setIdZona($idzona)
    {
        $this->_idzona =$idzona;
    }
    /**
    * Get IdZona 
    * @return long IdZona
    */
    function getIdZona()
    {
        return $this->_idzona;
    }

    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           $this->{"_".strtolower($atributo)} = $valor;
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           return $this->{"_".strtolower($atributo)};
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * toString devuelve un string con todos los datos de la clase
    * @return String
    */
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
         /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if(!is_array($valor))
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>
<?
/**
*Constructor de la clase dhl_pesos
*/
class dhl_pesos
{
    
    protected $_idpeso = 0;
    protected $_desde = 0;
    protected $_hasta = 0;
    protected $_tarifas = array();

    /**
    *Constructor de la clase dhl_pesos
    *@var long idpeso 
    *@var  desde 
    *@var  hasta 
    */
    function __construct($idpeso = 0,$desde = 0,$hasta = 0)
    {
        $this->setIdPeso($idpeso);
        $this->setDesde($desde);
        $this->setHasta($hasta);
    }
    
    /**
    * Set IdPeso 
    * @var long IdPeso
    */
    function setIdPeso($idpeso)
    {
        $this->_idpeso =$idpeso;
    }
    /**
    * Get IdPeso 
    * @return long IdPeso
    */
    function getIdPeso()
    {
        return $this->_idpeso;
    }
    /**
    * Set Desde 
    * @var  Desde
    */
    function setDesde($desde)
    {
        $this->_desde =$desde;
    }
    /**
    * Get Desde 
    * @return  Desde
    */
    function getDesde()
    {
        return $this->_desde;
    }
    /**
    * Set Hasta 
    * @var  Hasta
    */
    function setHasta($hasta)
    {
        $this->_hasta =$hasta;
    }
    /**
    * Get Hasta 
    * @return  Hasta
    */
    function getHasta()
    {
        return $this->_hasta;
    }
    public function addTarifa(dhl_precios $dhl_precios)
    {
        $this->_tarifas[] = $dhl_precios;
    }
    public function getTarifa($index)
    {
        return $this->_tarifas[$index];
    }
    public function removeTarifa($index)
    {
        unset($this->_tarifas[$index]);
    }
    public function getTarifas()
    {
        return $this->_tarifas;
    }
    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           $this->{"_".strtolower($atributo)} = $valor;
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           return $this->{"_".strtolower($atributo)};
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * toString devuelve un string con todos los datos de la clase
    * @return String
    */
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
    /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if(!is_array($valor))
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>
<?
/**
*Constructor de la clase dhl_zonas
*/
class dhl_zonas
{
    
    protected $_idzona = 0;
    protected $_nombre = "";

    /**
    *Constructor de la clase dhl_zonas
    *@var long idzona 
    *@var string nombre 
    */
    function __construct($idzona = 0,$nombre = "")
    {
        $this->setIdZona($idzona);
        $this->setNombre($nombre);
    }
    
    /**
    * Set IdZona 
    * @var long IdZona
    */
    function setIdZona($idzona)
    {
        $this->_idzona =$idzona;
    }
    /**
    * Get IdZona 
    * @return long IdZona
    */
    function getIdZona()
    {
        return $this->_idzona;
    }
    /**
    * Set Nombre 
    * @var string Nombre
    */
    function setNombre($nombre)
    {
        $this->_nombre =$nombre;
    }
    /**
    * Get Nombre 
    * @return string Nombre
    */
    function getNombre()
    {
        return $this->_nombre;
    }

    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           $this->{"_".strtolower($atributo)} = $valor;
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           return $this->{"_".strtolower($atributo)};
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * toString devuelve un string con todos los datos de la clase
    * @return String
    */
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
         /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if(!is_array($valor))
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>
<?
/**
*Constructor de la clase dhl_precios
*/
class dhl_precios
{
    
    protected $_idpeso = 0;
    protected $_idzona = 0;
    protected $_tarifa = 0;
    protected $_tarifakg = 0;

    /**
    *Constructor de la clase dhl_precios
    *@var long idpeso 
    *@var long idzona 
    *@var  tarifa 
    *@var  tarifakg 
    */
    function __construct($idpeso = 0,$idzona = 0,$tarifa = 0,$tarifakg = 0)
    {
        $this->setIdPeso($idpeso);
        $this->setIdZona($idzona);
        $this->setTarifa($tarifa);
        $this->setTarifaKG($tarifakg);
    }
    
    /**
    * Set IdPeso 
    * @var long IdPeso
    */
    function setIdPeso($idpeso)
    {
        $this->_idpeso =$idpeso;
    }
    /**
    * Get IdPeso 
    * @return long IdPeso
    */
    function getIdPeso()
    {
        return $this->_idpeso;
    }
    /**
    * Set IdZona 
    * @var long IdZona
    */
    function setIdZona($idzona)
    {
        $this->_idzona =$idzona;
    }
    /**
    * Get IdZona 
    * @return long IdZona
    */
    function getIdZona()
    {
        return $this->_idzona;
    }
    /**
    * Set Tarifa 
    * @var  Tarifa
    */
    function setTarifa($tarifa)
    {
        $this->_tarifa =$tarifa;
    }
    /**
    * Get Tarifa 
    * @return  Tarifa
    */
    function getTarifa()
    {
        return $this->_tarifa;
    }
    /**
    * Set TarifaKG 
    * @var  TarifaKG
    */
    function setTarifaKG($tarifakg)
    {
        $this->_tarifakg =$tarifakg;
    }
    /**
    * Get TarifaKG 
    * @return  TarifaKG
    */
    function getTarifaKG()
    {
        return $this->_tarifakg;
    }

    /**
    * Set para llamar de forma dinamica a las propiedades
    * @var Valor
    * @var String atributo
    */
    function __set($valor,$atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           $this->{"_".strtolower($atributo)} = $valor;
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * Get para devolver de forma dinamica los valores de las propiedades
    * @var String atributo
    * @return String
    */
    function __get($atributo)
    {
        if(isset($this->{"_".strtolower($atributo)}))
           return $this->{"_".strtolower($atributo)};
        /*else
           throw new Exception("No existes la variable privada $atributo");*/
    }
    /**
    * toString devuelve un string con todos los datos de la clase
    * @return String
    */
    function __toString()
    {
        $Codigo = "";
        foreach ($this as $propiedad => $valor)
        {
            $Codigo .=$propiedad ."=".$valor."\n";
        }
        return $Codigo;
    }
    /**
    * @desc Devuelve un array con las propiedades y valores de la clase.
    * @return array.
    */
    function __toArray()
    {
        $Ar = array();
        foreach ($this as $propiedad => $valor)
        {
            if(!is_array($valor))
                $Ar[$propiedad] = $valor;
        }
        return $Ar;
    }
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>