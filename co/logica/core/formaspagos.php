<?
/**
*Constructor de la clase clsformaspagos
*/
class formaspagos
{
	
	protected $_idformapago = 0;
	protected $_descripcion = "";
	protected $_inmediato = 0;

	/**
	*Constructor de la clase clsformaspagos
	*@var long idformapago 
	*@var string descripcion 
	*@var int inmediato 
	*/
	function __construct($idformapago = 0,$descripcion = "",$inmediato = 0)
	{
		$this->setIdFormaPago($idformapago);
		$this->setDescripcion($descripcion);
		$this->setInmediato($inmediato);
	}
	
	/**
	* Set IdFormaPago 
	* @var long IdFormaPago
	*/
	function setIdFormaPago($idformapago)
	{
		$this->_idformapago =$idformapago;
	}
	/**
	* Get IdFormaPago 
	* @return long IdFormaPago
	*/
	function getIdFormaPago()
	{
		return $this->_idformapago;
	}
	/**
	* Set Descripcion 
	* @var string Descripcion
	*/
	function setDescripcion($descripcion)
	{
		$this->_descripcion =$descripcion;
	}
	/**
	* Get Descripcion 
	* @return string Descripcion
	*/
	function getDescripcion()
	{
		return $this->_descripcion;
	}
	/**
	* Set Inmediato 
	* @var int Inmediato
	*/
	function setInmediato($inmediato)
	{
		$this->_inmediato =$inmediato;
	}
	/**
	* Get Inmediato 
	* @return int Inmediato
	*/
	function getInmediato()
	{
		return $this->_inmediato;
	}

	/**
	* Set para llamar de forma dinamica a las propiedades
	* @var Valor
	* @var String atributo
	*/
	function __set($valor,$atributo)
	{
		if(isset($this->{"_".strtolower($atributo)}))
		   $this->{"_".strtolower($atributo)} = $valor;
		/*else
		   throw new Exception("No existes la variable privada $atributo");*/
	}
	/**
	* Get para devolver de forma dinamica los valores de las propiedades
	* @var String atributo
	* @return String
	*/
	function __get($atributo)
	{
		if(isset($this->{"_".strtolower($atributo)}))
		   return $this->{"_".strtolower($atributo)};
		/*else
		   throw new Exception("No existes la variable privada $atributo");*/
	}
	/**
	* toString devuelve un string con todos los datos de la clase
	* @return String
	*/
	function __toString()
	{
	    $Codigo = "";
	    foreach ($this as $propiedad => $valor)
	    {
	        $Codigo .=$propiedad ."=".$valor."\n";
	    }
	    return $Codigo;
	}
	/**
	* @desc Devuelve un array con las propiedades y valores de la clase.
	* @return array.
	*/
	function __toArray()
	{
	    $Ar = array();
	    foreach ($this as $propiedad => $valor)
	    {
	        $Ar[$propiedad] = $valor;
	    }
	    return $Ar;
	}
    /**
    * @desc Setea un array con las propiedades y valores a la clase.
    * @var array.
    */
    function __setByArray(array $fila)
    {
        foreach ($fila as $atributo => $valor)
            $this->__set($valor, $atributo);
    }
}
?>