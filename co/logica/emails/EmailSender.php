<?

class EmailSender {

	public static function sendEmail($emailData) {
	
		//Envío instantáneo
		require_once dirname(__FILE__).'/phpmailer/class.phpmailer.php';
		$m = new PHPMailer;
		
		if($emailData->getReplyTo() != null)
			$m->AddReplyTo($emailData->getReplyTo());
			
		if($emailData->getTextOnlyMail() != null && $emailData->getTextOnlyMail() == true) {
			$m->IsHTML(false);
		} else {
			$m->IsHTML(true);
			$m->AltBody = $emailData->getAltBody();
		}
		
		$m->From = $emailData->getFromEmail();
		$m->FromName = $emailData->getFromName();
		$m->AddAddress($emailData->getTo());
		$m->Subject = $emailData->getSubject();		
		$m->Body = $emailData->getBody();
		
		$m->IsSMTP(true);
		$m->Host = 'smtp.mandrillapp.com';
		$m->SMTPAuth = true;
		
		if($emailData->getEmailType() == 'invitacion') {
			$m->Username = 'invitaciones@corp.geelbe.com';
			$m->Password = 'AgnhxEoNZLLarrz2ZFQD-w';
		} else {
			$m->Username = 'transaccional@corp.geelbe.com';
			$m->Password = 'Pjv1XhT9tE_pJB5JEWUu9w';
		}
		
		return $m->Send();
				
		//Método viejo usando cola
		$replyToEmail = "";
		if($emailData->getReplyTo() != null) {
			$replyToEmail = "<replyTo>".self::xmlCharacterEncode($emailData->getReplyTo())."</replyTo>";
		}
		
		$altBody = "";
		if($emailData->getTextOnlyMail() != null && $emailData->getTextOnlyMail() == true) {
			$altBody = "<onlyTextMail>true</onlyTextMail>";
		} else {
			$altBody = "<altBody>".self::xmlCharacterEncode($emailData->getAltBody())."</altBody>";
		}
		
		$email = 
			"<email><fromEmail>".self::xmlCharacterEncode($emailData->getFromEmail())."</fromEmail><fromName>".self::xmlCharacterEncode($emailData->getFromName()).
			"</fromName><to>".self::xmlCharacterEncode($emailData->getTo())."</to><subject>".self::xmlCharacterEncode($emailData->getSubject()).
			"</subject><body>".self::xmlCharacterEncode($emailData->getBody())."</body>".$altBody."<emailType>".
			$emailData->getEmailType()."</emailType>".$replyToEmail."</email>";
	
		$oConexion = Conexion::nuevo();
		try {
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("INSERT INTO geelbe_emails_send_queue (email_send_date, email_data, email_type, email_sent) VALUES (NOW(), '".$email."', '".$emailData->getEmailType()."', 0)");
			$oConexion->EjecutarQuery();
			$oConexion->Cerrar_Trans();
		}
		catch (MySQLException $e) {
			$oConexion->RollBack();
			throw $e;
		}
	}
	
	/*public static function queueNewsletter($asunto, $cuerpo, $date, $newsId) {
	
		//Envío instantáneo
		require_once dirname(__FILE__).'/phpmailer/class.phpmailer.php';
		
		$emails = array(
			array('IdUsuario' => 1, 'NombreUsuario' => 'ignacio@corp.geelbe.com'),
			array('IdUsuario' => 2, 'NombreUsuario' => 'nachioalvarez@gmail.com'),
			array('IdUsuario' => 3, 'NombreUsuario' => 'fede@federicog.com.ar'),
			array('IdUsuario' => 4, 'NombreUsuario' => 'juanjose@corp.geelbe.com'),
			array('IdUsuario' => 5, 'NombreUsuario' => 'mercadeo@corp.geelbe.com'),
		);
		
		$oConexion = Conexion::nuevo();
		
		foreach($emails as $i => $user) {
		
			$search = array('[[[idco]]]', '[[[hashco]]]');
			$replace = array($user['IdUsuario'], Hash::getHash($user['IdUsuario'].$user['NombreUsuario']));
			$cuerpoNuevo = str_replace($search, $replace, $cuerpo);
			
			try {
				$oConexion->Abrir_Trans();
				$oConexion->setQuery("INSERT INTO newsletters_queue VALUES (0, '".$user['NombreUsuario']."', '".mysql_real_escape_string($asunto)."', '".mysql_real_escape_string($cuerpoNuevo)."', '".$date."', ".$newsId.")");
				$oConexion->EjecutarQuery();
				$oConexion->Cerrar_Trans();
			}
			catch (MySQLException $e) {
				$oConexion->RollBack();
				throw $e;
			}
			
		}				

	}

	public static function sendQueuedNewsletter() {
	
		//Envío instantáneo
		require_once dirname(__FILE__).'/phpmailer/class.phpmailer.php';

		$oConexion = Conexion::nuevo();
		try {
			$oConexion->Abrir_Trans();
			$oConexion->setQuery("SELECT * FROM newsletters_queue LIMIT 50");
			$emails = $oConexion->DevolverQuery();
			$oConexion->Cerrar_Trans();
		}
		catch (MySQLException $e) {
			$oConexion->RollBack();
			throw $e;
		}

		foreach($emails as $i => $user) {
				
			$m = new PHPMailer;
			$m->From = "geelbe@geelbe.com";
			$m->FromName = "Geelbe Colombia";
			$m->AddAddress($user['to']);
			$m->Subject = $user['subject'];		
			$m->Body = $user['body'];
			$m->IsHTML(true);			
			$m->IsSMTP(true);
			$m->Host = 'smtp.mandrillapp.com';
			$m->SMTPAuth = true;
			$m->Username = 'ignacio@corp.geelbe.com';
			$m->Password = 'U5xMZGFZZAvrl7zAJPywsg';
			
			if($m->Send()) {
				
				try {
					$oConexion->Abrir_Trans();
					$oConexion->setQuery("DELETE FROM newsletters_queue WHERE id = ".$user['id']);
					$oConexion->EjecutarQuery();
					$oConexion->Cerrar_Trans();
				}
				catch (MySQLException $e) {
					$oConexion->RollBack();
					throw $e;
				}
				
			}
			
		}				

	}*/
	
	public static function sendNewsletter($news) {

		//destinatarios por email		
		$batchSize = 900;
		//emails por ejecución
		$emailsQuantity = 10;
		
		$asunto = $news->getAsunto();
		$cuerpo = $news->getCuerpo();
		$date = $news->getFechaEnvio();

		if($news->getTest() == 0) {
				
			if(is_numeric($news->getMaxUserId()))
				$max = ' AND IdUsuario < '.$news->getMaxUserId();
			else
				$max = '';
				
			if($news->getHuerfanos() == 1)
				$max .= ' AND Padrino = "Huerfano"';
				
			if($news->getInactivos() == 1)
				$max .= ' AND es_activa = 1';
			
			$oConexion = Conexion::nuevo();
			try {
				$oConexion->Abrir_Trans();
				$oConexion->setQuery("SELECT IdUsuario, NombreUsuario FROM usuarios WHERE mail_suscription = 1 AND NombreUsuario LIKE '%@%' ".$max." ORDER BY IdUsuario DESC LIMIT ".$batchSize*$emailsQuantity);
				$emails = $oConexion->DevolverQuery();
				$oConexion->Cerrar_Trans();
			}
			catch (MySQLException $e) {
				$oConexion->RollBack();
				throw $e;
			}
	
		} else {
		
			//Datos de prueba
			$emails = array(
				array('IdUsuario' => 1, 'NombreUsuario' => 'ignacio@corp.geelbe.com'),
				array('IdUsuario' => 2248, 'NombreUsuario' => 'nachioalvarez@gmail.com'),
				array('IdUsuario' => 3, 'NombreUsuario' => 'fede@federicog.com.ar'),
				array('IdUsuario' => 9, 'NombreUsuario' => 'juanjose@corp.geelbe.com'),
				array('IdUsuario' => 5, 'NombreUsuario' => 'mercadeo@corp.geelbe.com'),
			);
		
		}
		
		//No hay más usuarios, el newsletter está enviado
		if(count($emails) == 0 || $news->getTest() == 1) {
			$news->setEnviado(1);
			dmNewsletters::save($news);
			if($news->getTest() == 0)
				return false;
		}
		
		//Envío instantáneo
		require_once dirname(__FILE__).'/mandrill/Mandrill.php';
		$mandrill = new Mandrill(MANDRILL_API_KEY);
		$emails = array_chunk($emails, $batchSize);	
		$oConexion = Conexion::nuevo();
		$lastUserId = null;
		
		foreach($emails as $i => $users) {
			$start = microtime(true);
			
			$to = array();
			$tags = array();
			foreach($users as $u) {
				$to[] = array(
	                'email' => $u['NombreUsuario'],
	                'type' => 'bcc'
	            );
	            
	            $tags[] = array(
					'rcpt' => $u['NombreUsuario'],
					'vars' => array(
						array(
							'name' => 'IDCO',
							'content' => $u['IdUsuario']
						),
						array(
							'name' => 'HASHCO',
							'content' => Hash::getHash($u['IdUsuario'].$u['NombreUsuario'])
						)
					)
				);
				
				//guardar último usuario
				if($lastUserId == null || $lastUserId > $u['IdUsuario'])
					$lastUserId = $u['IdUsuario'];
			}
			
			$message = array(
		        'html' => utf8_encode($cuerpo),
		        'auto_text' => true,
		        'subject' => utf8_encode($asunto),
		        'from_email' => "geelbe@geelbe.com",
		        'from_name' => 'Geelbe Colombia',
		        'to' => $to,
		        'merge' => true,
		        'merge_vars' => $tags
		    );
		   		    
		    $async = false;
		    
		    //El envío tarda 1.5s aprox
		    $result = $mandrill->messages->send($message, $async);
		}
		
		//actualizar último usuario enviado
		$news->setMaxUserId($lastUserId);
		dmNewsletters::save($news);
		
		return true;				

	}
	
	private static function xmlCharacterEncode($string, $trans='') { 
  		$trans = (is_array($trans)) ? $trans : get_html_translation_table(HTML_ENTITIES, ENT_QUOTES); 
  		foreach ($trans as $k=>$v) 
    		$trans[$k]= "&#".ord($k).";"; 

		return strtr($string, $trans); 
	} 

}

?>