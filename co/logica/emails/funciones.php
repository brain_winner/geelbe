<?
 $confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
 Aplicacion::CargarIncludes(Aplicacion::getIncludes("micuenta", "dm"));      

 require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailBuilder.php";
 require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailSender.php";
 require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/emails/EmailData.php"; 
                                  
 function reemplazarTags($texto, $xml)
{   
    $texto = str_replace("%Ruta URL%",Aplicacion::getRootUrl(), $texto);
    if ($_SESSION['IdRegistro'][0]["Id"])
        $objUsuario = dmMicuenta::GetUsuario($_SESSION['IdRegistro'][0]["Id"]);
    else                                                        
        $objUsuario = dmMicuenta::GetUsuario(Aplicacion::Decrypter($_SESSION["User"]["id"]));
           

    foreach($xml->children() as $child)
    {
     foreach($child->attributes() as $a => $b)
      {
        if ($a == 'id')
            $tag = ("%".$b."%");
        if ($a == 'dato')
            if($b == 'clave')
            {
                $dato = $objUsuario->getClave();
            }
            else
            {
                if($b == 'usuario')
                    $dato = $objUsuario->getNombreUsuario();
                else
                    $dato = $objUsuario->getDatos()->__get($b);
            }
       }                                           
    $texto = str_replace($tag, htmlentities($dato), $texto);         
    }
    return $texto; 
}

	function emailPagoAceptado($to, $data) {
		$emailBuilder = new EmailBuilder();
	    $emailData = $emailBuilder->generatePagoAceptadoEmail($to, $data);
	                
		$emailSender = new EmailSender();
		$emailSender->sendEmail($emailData);
			
	}
	
	function emailPedidoAceptado($to, $data) {
		$emailBuilder = new EmailBuilder();
	    $emailData = $emailBuilder->generatePedidoAceptadoEmail($to, $data);
	    
		$emailSender = new EmailSender();
		$emailSender->sendEmail($emailData);

	}

?>