<?

class EmailData {
	
	public $fromEmail;
	public $fromName;
	public $to;
	public $emailType;
	public $subject;
	public $body;
	public $altBody;
	public $replyTo;
	public $textOnlyMail;

	public function setFromEmail($fromEmailToSet) {
		$this->fromEmail = $fromEmailToSet;
	}
	public function setFromName($fromNameToSet) {
		$this->fromName = $fromNameToSet;
	}
	public function setTo($toToSet) {
		$this->to = $toToSet;
	}
	public function setEmailType($emailTypeToSet) {
		$this->emailType = $emailTypeToSet;
	}
	public function setSubject($subjectToSet) {
		$this->subject = $subjectToSet;
	}
	public function setBody($bodyToSet) {
		$this->body = $bodyToSet;
	}
	public function setAltBody($altBodyToSet) {
		$this->altBody = $altBodyToSet;
	}
	public function setReplyTo($replyToToSet) {
		$this->replyTo = $replyToToSet;
	}
	public function setTextOnlyMail($textOnlyMailToSet) {
		$this->textOnlyMail = $textOnlyMailToSet;
	}
	
	public function getFromEmail() {
		return $this->fromEmail;
	}
	public function getFromName() {
		return $this->fromName;
	}
	public function getTo() {
		return $this->to;
	}
	public function getEmailType() {
		return $this->emailType;
	}
	public function getSubject() {
		return $this->subject;
	}
	public function getBody() {
		return $this->body;
	}
	public function getAltBody() {
		return $this->altBody;
	}
	public function getReplyTo() {
		return $this->replyTo;
	}
	public function getTextOnlyMail() {
		return $this->textOnlyMail;
	}
	
}

?>