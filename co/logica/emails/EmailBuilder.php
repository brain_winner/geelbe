<?php 

class EmailBuilder {
	
	public static function getAnalyticsGif($mailtype = 'generic', $type = 'generic') {
		$var_utmac='UA-6895743-8'; //enter the new urchin code
		$var_utmhn='www.geelbe.com'; //enter your domain
		$var_utmn=rand(1000000000,9999999999);//random request number
		$var_cookie=rand(10000000,99999999);//random cookie number
		$var_random=rand(1000000000,2147483647); //number under 2147483647
		$var_today=time(); //today
		$var_referer="http://www.geelbe.com/co/$type/referer.html"; //referer url
		
		$var_uservar='-'; //enter your own user defined variable
		$var_utmp="http://www.geelbe.com/co/$type/$mailtype.html"; //this example adds a fake page request to the (fake) rss directory (the viewer IP to check for absolute unique RSS readers)
			
		$urchinUrl='http://www.google-analytics.com/__utm.gif?utmwv=1&utmn='.$var_utmn.'&utmsr=-&utmsc=-&utmul=-&utmje=0&utmfl=-&utmdt=-&utmhn='.$var_utmhn.'&utmr='.$var_referer.'&utmp='.$var_utmp.'&utmac='.$var_utmac.'&utmcc=__utma%3D'.$var_cookie.'.'.$var_random.'.'.$var_today.'.'.$var_today.'.'.$var_today.'.2%3B%2B__utmb%3D'.$var_cookie.'%3B%2B__utmc%3D'.$var_cookie.'%3B%2B__utmz%3D'.$var_cookie.'.'.$var_today.'.2.2.utmccn%3D(direct)%7Cutmcsr%3D(direct)%7Cutmcmd%3D(none)%3B%2B__utmv%3D'.$var_cookie.'.'.$var_uservar.'%3B';
			 
		return $urchinUrl;
    }
    
	public static function getMailType($mail = '') {
		$default = 'generic';
		$splitEmail = split("@",$mail);
		$emailDomain = $splitEmail[1];
		$secondSplit = split("\.",$emailDomain);
		$emailProvider = $secondSplit[0];
		$domainValue = 'generic';
		
		switch ($emailProvider) {
			case 'gmail': $domainValue = 'gmail'; break;
			case 'hotmail': case 'msn': case 'live': $domainValue = 'hotmail-msn-live'; break;
			case 'yahoo': $domainValue = 'yahoo'; break;
		}
		return $domainValue;
    }
	
	public function generateInviteEmail($usuario, $receiver) {
		$oTexto = new Texto(Aplicacion::getRoot() . "front/emails/invitacion.html");
        $oTexto->Read();
        $oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
        $oTexto->ReemplazarString("%Destino%", $receiver);
        $oTexto->ReemplazarString("%EmailSender%",$usuario->getNombreUsuario());
        $oTexto->ReemplazarString("%NombrePadrino%", $usuario->getDatos()->getNombre());
        $oTexto->ReemplazarString("%ApellidoPadrino%", $usuario->getDatos()->getApellido());
        
		$oTexto->ReemplazarString("%AnalyticsImage%", self::getAnalyticsGif(self::getMailType($receiver), 'invitacion'));
		
	    $oTextoAlt = new Texto(Aplicacion::getRoot() . "front/emails/invitacion.txt");
		$oTextoAlt->Read();
        $oTextoAlt->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
        $oTextoAlt->ReemplazarString("%Destino%", $receiver);
        $oTextoAlt->ReemplazarString("%EmailSender%",$usuario->getNombreUsuario());
        $oTextoAlt->ReemplazarString("%NombrePadrino%", $usuario->getDatos()->getNombre());
        $oTextoAlt->ReemplazarString("%ApellidoPadrino%", $usuario->getDatos()->getApellido());
        
		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_invitacion"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($receiver);
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody($oTextoAlt->getText());
		$emailData->setEmailType("invitacion");
		$emailData->setSubject("Invitaci�n exclusiva");
		
		return $emailData;
	}
	
	public function generateActivationEmail($ahijado, $receiver) {
		$oTexto = new Texto(Aplicacion::getRoot() . "front/emails/activacion.html");
        $oTexto->Read();
        $oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
        $oTexto->ReemplazarString("%Clave%", $ahijado->getClave());
        $oTexto->ReemplazarString("%Email%", $ahijado->getNombreUsuario());
        $oTexto->ReemplazarString("%NombreUsuario%", $ahijado->getDatos()->getNombre());
        $oTexto->ReemplazarString("%ApellidoUsuario%", $ahijado->getDatos()->getApellido());
        $oTexto->ReemplazarString("%IdUsuario%", dmUsuario::getIdByNombreUsuario($receiver));
        $oTexto->ReemplazarString("%HashIdUsuario%", generar_hash_md5(dmUsuario::getIdByNombreUsuario($receiver)));
        $oTexto->ReemplazarString("%Hash%", Hash::getHash($ahijado->getIdUsuario().$ahijado->getNombreUsuario()));
		$oTexto->ReemplazarString("%Type%", "ACT");
		
		$oTexto->ReemplazarString("%AnalyticsImage%", self::getAnalyticsGif(self::getMailType($receiver), 'activacion'));
		
		$oTextoAlt = new Texto(Aplicacion::getRoot() . "front/emails/activacion.txt");
		$oTextoAlt->Read();
		$oTextoAlt->ReemplazarStringPlano("%NombreUsuario%", $ahijado->getDatos()->getNombre());
		$oTextoAlt->ReemplazarStringPlano("%ApellidoUsuario%", $ahijado->getDatos()->getApellido());
		$oTextoAlt->ReemplazarStringPlano("%Ruta URL%", Aplicacion::getRootUrl());
		$oTextoAlt->ReemplazarStringPlano("%IdUsuario%", $ahijado->getIdUsuario());
		$oTextoAlt->ReemplazarStringPlano("%HashIdUsuario%", generar_hash_md5($ahijado->getIdUsuario()));
		$oTextoAlt->ReemplazarStringPlano("%Hash%", Hash::getHash($ahijado->getIdUsuario().$ahijado->getNombreUsuario()));
		$oTexto->ReemplazarString("%Type%", "ACT");
			  
		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_registro"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($receiver);
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody($oTextoAlt->getText());
		$emailData->setEmailType("activacion");
		$emailData->setSubject("Geelbe - Mail de Activaci�n");
		
		return $emailData;
	}
	
	public function generateCompraEmail($usuario, $ahijado, $receiver) {
		$oTexto = new Texto(Aplicacion::getRoot() . "front/emails/recordatoriocompra.html");
        $oTexto->Read();
        $oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
        $oTexto->ReemplazarString("%NombrePadrino%", $usuario->getDatos()->getNombre());
        $oTexto->ReemplazarString("%NombreUsuario%", $ahijado->getDatos()->getNombre());
        $oTexto->ReemplazarString("%IdUsuario%", dmUsuario::getIdByNombreUsuario($receiver));
        $oTexto->ReemplazarString("%HashIdUsuario%", generar_hash_md5(dmUsuario::getIdByNombreUsuario($receiver)));
        $oTexto->ReemplazarString("%Hash%", Hash::getHash(dmUsuario::getIdByNombreUsuario($receiver).$ahijado->getNombreUsuario()));
        
		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($receiver);
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("recordatoriocompra");
		$emailData->setSubject("Compr� en Geelbe");
		
		return $emailData;
	}
	

	
	public function generateCreditoPotencialAcreditadoEmail($usuario, $nroPedido, $nombreVidriera) {
		$oTexto = new Texto(Aplicacion::getRoot() . "front/emails/creditoareferido.html");
        $oTexto->Read();
        $oTexto->ReemplazarString("%NombreUsuario%",$usuario);
        $oTexto->ReemplazarString("%NumeroPedido%", $nroPedido);
        $oTexto->ReemplazarString("%NombreVidriera%", $nombreVidriera);
        $oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
        
		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($usuario);
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("creditopotencialacreditado");
		$emailData->setSubject("Tu credito potencial fue acreditado");
		
		return $emailData;
	}
	
	public function generateAvisoRegistroAhijadoEmail($usuario, $idPadrino, $nombrePadrino) {
		$oTexto = new Texto(Aplicacion::getRoot() . "front/emails/mailpadrino.html");
		$oTexto->Read();
        if($usuario->getDatos()->getNombre() == "Socio Geelbe"){
        	$oTexto->ReemplazarString("%NombreAhijado%", $usuario->getNombreUsuario());
        }
        else{
        	$oTexto->ReemplazarString("%NombreAhijado%", $usuario->getDatos()->getNombre());
        }
        $oTexto->ReemplazarString("%ApellidoAhijado%", $usuario->getDatos()->getApellido());
        $oTexto->ReemplazarString("%IdUsuario%", $idPadrino);
        $oTexto->ReemplazarString("%HashIdUsuario%", generar_hash_md5($idPadrino));
        $oTexto->ReemplazarString("%Hash%", Hash::getHash($usuario->getIdUsuario().$usuario->getNombreUsuario()));
        $oTexto->ReemplazarString("%NombreUsuario%", $nombrePadrino);
        $oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
                    
		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($usuario->getPadrino());
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("avisoregistroahijado");
        if($usuario->getDatos()->getNombre() == "Socio Geelbe") {
        	$emailData->setSubject($usuario->getNombreUsuario() . " se registr� a Geelbe");
        }
        else {
        	$emailData->setSubject($usuario->getDatos()->getNombre() . " se registr� a Geelbe");
        }
		return $emailData;
	}
	
	public function generateBienvenidaEmail($usuario) {
		$oTexto = new Texto(Aplicacion::getRoot() . "front/emails/bienvenida.html");
        $oTexto->Read();
        $txtNombre = $usuario->getDatos()->getNombre();
        $txtEmail = $usuario->getNombreUsuario();
        $subject = "Hola " . $txtNombre;
        $oTexto->ReemplazarString("%IdUsuario%", $usuario->getIdUsuario());
        $oTexto->ReemplazarString("%Email%", $usuario->getNombreUsuario());
        $oTexto->ReemplazarString("%HashIdUsuario%", generar_hash_md5($usuario->getIdUsuario()));
        $oTexto->ReemplazarString("%Hash%", Hash::getHash($usuario->getIdUsuario().$usuario->getNombreUsuario()));
        $oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
        $emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($txtEmail);
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("bienvenida");
		$emailData->setSubject($subject);
        
		return $emailData;
	}
	
	public function generateMensajeBOEmail($oMensaje, $txtNombre, $txtApellido, $Motivo) {
		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo(Aplicacion::getParametros("info", "email_contacto"));
		$emailData->setBody("Mensaje de: ".$txtNombre . " " . $txtApellido."\n\n".$oMensaje->getmensaje());
		$emailData->setTextOnlyMail(true);
		$emailData->setEmailType("contacto");
		$emailData->setSubject("[BO] Consulta con motivo ".$Motivo);
		$emailData->setReplyTo($oMensaje->getemail());
		
		return $emailData;
	}

	
	public function generatePagoAceptadoEmail($to, $data) {
		$data = array_merge($data, array('Ruta URL' => Aplicacion::getRootUrl()));
		$message = file_get_contents(Aplicacion::getRoot()."front/emails/pagoaceptadoAuto.html");
		foreach($data as $search => $replace) {
			$message = str_replace('%'.$search.'%', htmlentities($replace), $message);
		}
			
		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($to);
		$emailData->setBody($message);
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("pagoaceptado");
		$emailData->setSubject("Hemos recibido el pago de tu pedido");
		
		return $emailData;
	}

	
	public function generatePedidoAceptadoEmail($to, $data) {
		$data = array_merge($data, array('Ruta URL' => Aplicacion::getRootUrl()));
		
		if(isset($data['HTML'])) {
			$html = $data['HTML'];
		} 
		else {
			$html = 'esperandosistemapago';
		}
		
		$message = file_get_contents(Aplicacion::getRoot().'front/emails/'.$html.'.html');
		foreach($data as $search => $replace)
			$message = str_replace('%'.$search.'%', $replace, $message);

		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($to);
		$emailData->setBody($message);
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("pedidoaceptado");
		$emailData->setSubject("Hemos recibido tu pedido");
		
		return $emailData;
	}

	
	public function generateAnEmail($email, $body, $type, $subject) {
		$emailData = new EmailData();
		$emailData->setFromEmail("geelbe@geelbemail.com");
		$emailData->setFromName("Geelbe");
		$emailData->setTo($email);
		$emailData->setBody($body);
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType($type);
		$emailData->setSubject($subject);
			
		return $emailData;
	}

	public function generateEliminacionUsuarioEmail($NombreUsuario, $id, $email, $hashcode) {
		$oTexto = new Texto(Aplicacion::getRoot() . "front/emails/eliminarcuenta.html");
		$oTexto->Read();
        $oTexto->ReemplazarString("%NombreUsuario%", $NombreUsuario);
        $oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
        $oTexto->ReemplazarString("%IdUsuario%", $id);
        $oTexto->ReemplazarString("%HashIdUsuario%", generar_hash_md5($id));
        $oTexto->ReemplazarString("%Hash%", Hash::getHash($id.$email));
        $oTexto->ReemplazarString("%HashDes%", $hashcode);
        
		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($email);
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("eliminacionusuario");
		$emailData->setSubject("Eliminaci�n de cuenta Geelbe");
			
		return $emailData;
	}

	
	public function generatePedidoAnuladoEmail($idPedido, $nombrePedido, $idUsuario, $nombreUsuario) {
		$data = array(
			'NumeroPedido' => $idPedido,
			'NombreVidriera' => $nombrePedido,
			'IdUsuario' => $idUsuario,
			'Hash' => generar_hash_md5($idUsuario),
			'Ruta URL' => Aplicacion::getRootUrl()
		);
			
		$message = file_get_contents(Aplicacion::getRoot()."front/emails/pedidoanuladofin.html");
		foreach($data as $search => $replace) {
			$message = str_replace('%'.$search.'%', htmlentities($replace), $message);
		}
				
		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($nombreUsuario);
		$emailData->setBody($message);
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("pedidoanulado");
		$emailData->setSubject("Tu pedido fue anulado");
		
		return $emailData;
	}

	public function generateRecordatorioActivacionEmail($NombreUsuario, $id, $Nombre) {
		$MAIL = Aplicacion::getEmail("reactivacion");
		$oTexto = new Texto(Aplicacion::getRoot() . "front/emails/activacionrecordatorio.html");
        $oTexto->Read();
        $oTexto->ReemplazarString("%NombreUsuario%", $NombreUsuario);
        $oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
        $oTexto->ReemplazarString("%IdUsuario%", $id);
        $oTexto->ReemplazarString("%Hash%", Hash::getHash($id.$NombreUsuario));
        $MAIL["asunto"] = str_replace("%NombreUsuario%", $Nombre, $MAIL["asunto"]);
        
		$emailData = new EmailData();
		$emailData->setFromEmail("geelbe@geelbemail.com");
		$emailData->setFromName("Geelbe");
		$emailData->setTo($NombreUsuario);
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("recordatorioactivacion");
		$emailData->setSubject($MAIL["asunto"]);
			
		return $emailData;
	}

	
	public function generateRecordarPagoEmail($idPedido, $nombrePedido, $idUsuario, $nombreUsuario) {
		$data = array(
			'NumeroPedido' => $idPedido,
			'NombreUsuario' => $nombreUsuario,
			'NombreVidriera' => $nombrePedido,
			'IdUsuario' => $idUsuario,
			//'HashIdUsuario' => Aplicacion::Encrypter($objPedido->getIdUsuario())
			'Hash' => generar_hash_md5($idUsuario),
			'Ruta URL' => Aplicacion::getRootUrl()
		);
		
		$message = file_get_contents(Aplicacion::getRoot()."front/emails/recordarpago.html");
		foreach($data as $search => $replace) {
			$message = str_replace('%'.$search.'%', htmlentities($replace), $message);
		}
					
		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($nombreUsuario);
		$emailData->setBody($message);
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("recordarpago");
		$emailData->setSubject("Tu pedido a�n no ha sido abonado");
		
		return $emailData;
	}

	public function generateRecordarPedidoEmail($idPedido, $idUsuario, $nombreUsuario) {
		$data = array(
			'NumeroPedido' => $idPedido,
			'NombreUsuario' => $nombreUsuario,
			'IdUsuario' => $idUsuario,
			//'HashIdUsuario' => Aplicacion::Encrypter($objPedido->getIdUsuario())
			'Hash' => generar_hash_md5($idUsuario),
			'Ruta URL' => Aplicacion::getRootUrl()
		);
					
		$message = file_get_contents(Aplicacion::getRoot()."front/emails/recordarpedido.html");
		foreach($data as $search => $replace) {
			$message = str_replace('%'.$search.'%', htmlentities($replace), $message);
		}

		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($nombreUsuario);
		$emailData->setBody($message);
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("recordarpedido");
		$emailData->setSubject("Tu pedido a�n no ha sido completado");
		
		return $emailData;
	}
	
	public function generatePedidoInsuficiente($idPedido, $to) {
		$data = array(
			'NumeroPedido' => $idPedido,
			//'HashIdUsuario' => Aplicacion::Encrypter($objPedido->getIdUsuario())
			'Hash' => generar_hash_md5($idUsuario),
			'Ruta URL' => Aplicacion::getRootUrl()
		);
					
		$message = file_get_contents(Aplicacion::getRoot()."front/emails/pedidoinsuficiente.html");
		foreach($data as $search => $replace) {
			$message = str_replace('%'.$search.'%', htmlentities($replace), $message);
		}

		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($to);
		$emailData->setBody($message);
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("recordarpedido");
		$emailData->setSubject("Tu pedido a�n no ha sido completado");
		
		return $emailData;
	}

	public function generateBajoStock($to, $nombre, $url) {
		$data = array(
			'Nombre' => $nombre,
			'URL' => $url,
			'Ruta URL' => Aplicacion::getRootUrl()
		);
					
		$message = file_get_contents(Aplicacion::getRoot()."front/emails/bajostock.html");
		foreach($data as $search => $replace) {
			$message = str_replace('%'.$search.'%', htmlentities($replace), $message);
		}

		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($to);
		$emailData->setBody($message);
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("bajostock");
		$emailData->setSubject("Recordatorio bajo stock");
		
		return $emailData;
	}

	public function generateRecuperacionDatosEmail($objUsuario, $recoverPassHash) {
		
		$oTexto = new Texto(Aplicacion::getRoot() . "front/emails/recuperarclave.html");
        $oTexto->Read();
        $oTexto->ReemplazarString("%NombreUsuario%", $objUsuario->getDatos()->getNombre());
        $oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
        $oTexto->ReemplazarString("%Email%", $objUsuario->getNombreUsuario());
        $oTexto->ReemplazarString("%TimeOut%", Aplicacion::Encrypter(time() + Aplicacion::getParametros("recuperar_clave", "tiempo")));
        $oTexto->ReemplazarString("%Codigo%", $recoverPassHash);

		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($objUsuario->getNombreUsuario());
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("recuperaciondatos");
		$emailData->setSubject("Recuperacion de datos Geelbe");
		
		return $emailData;
	}

	public function generateRecomendacionProductoRegistradoEmail($mailDestino, $nombrePadrino, $apellidoPadrino, $objCampania, $objProducto, $idAmigo, $nombreUsuario, $IdCampania, $IdCategoria, $IdProducto) {
		
		$oTexto = new Texto(Aplicacion::getRoot()."front/emails/vistazo.html");
		$oTexto->Read();
	    $oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
        $oTexto->ReemplazarString("%Destino%", $mailDestino);
        $oTexto->ReemplazarString("%NombrePadrino%", Aplicacion::Decrypter($nombrePadrino));
        $oTexto->ReemplazarString("%ApellidoPadrino%", Aplicacion::Decrypter($apellidoPadrino));
        $oTexto->ReemplazarString("%NombreVidriera%", $objCampania->getNombre());
        $oTexto->ReemplazarString("%FinVidriera%", $objCampania->getFechaFin());
        $oTexto->ReemplazarString("%NombreProducto%", $objProducto->getNombre());
        $oTexto->ReemplazarString("%PrecioLista%", $objProducto->getPVP());
        $oTexto->ReemplazarString("%PrecioGeelbe%", $objProducto->getPVenta());
        $oTexto->ReemplazarString("%IdUsuario%", $idAmigo);
        $oTexto->ReemplazarString("%HashIdUsuario%", generar_hash_md5($idAmigo));
        $oTexto->ReemplazarString("%Hash%", Hash::getHash($idAmigo.$mailDestino));
        $oTexto->ReemplazarString("%NombreUsuario%", $nombreUsuario);
           
        $oTexto->ReemplazarString("%DestinoLink%", Aplicacion::getRootUrl()."logica/autologin/login.php?IdUsuario=".$idAmigo."&Hash=".Hash::getHash($idAmigo.$mailDestino)."&Type=RECOMENDAR&IdCampania=".$IdCampania."&IdCategoria=".$IdCategoria."&IdProducto=".$IdProducto);
          
        $oTexto->ReemplazarString("%DestinoLinkCOMPROMISOS%", Aplicacion::getRootUrl()."logica/autologin/login.php?IdUsuario=".$idAmigo."&Hash=".Hash::getHash($idAmigo.$mailDestino)."&Type=COMPROMISOS");
        $oTexto->ReemplazarString("%DestinoLinkVIDRIERA%", Aplicacion::getRootUrl()."logica/autologin/login.php?IdUsuario=".$idAmigo."&Hash=".Hash::getHash($idAmigo.$mailDestino)."&Type=VIDRIERA");
        $oTexto->ReemplazarString("%DestinoLinkMICUENTA%", Aplicacion::getRootUrl()."logica/autologin/login.php?IdUsuario=".$idAmigo."&Hash=".Hash::getHash($idAmigo.$mailDestino)."&Type=MICUENTA");
        $oTexto->ReemplazarString("%DestinoLinkTERMINOS%", Aplicacion::getRootUrl()."logica/autologin/login.php?IdUsuario=".$idAmigo."&Hash=".Hash::getHash($idAmigo.$mailDestino)."&Type=TERMINOS");
        $oTexto->ReemplazarString("%DestinoLinkCONTACTO%", Aplicacion::getRootUrl()."logica/autologin/login.php?IdUsuario=".$idAmigo."&Hash=".Hash::getHash($idAmigo.$mailDestino)."&Type=CONTACTO");
        
            
        $oTexto->ReemplazarString("%Descripcion%", $objProducto->getDescripcion());
        $oTexto->ReemplazarString("%Descripcion Ampliada%", $objProducto->getDescripcionAmpliada());
        $IMGS = DirectorytoArray(Aplicacion::getRoot() . "front/productos/productos_". $objProducto->getIdProducto(). "/", 3,array("jpg","png", "gif"));

        if (count($IMGS["producto_1"]))
            $oTexto->ReemplazarString("%ImagenProducto%", Aplicacion::getRootUrl() . "front/productos/productos_". $objProducto->getIdProducto(). "/".$IMGS["producto_1"]);
        else
        $oTexto->ReemplazarString("%ImagenProducto%", Aplicacion::getRootUrl() . "front/productos/noimagen.jpg");

		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_recomendacion"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($mailDestino);
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("recomendacionproducto");
		$emailData->setSubject(Aplicacion::Decrypter($nombrePadrino)." ".Aplicacion::Decrypter($apellidoPadrino)." te recomienda este producto");
		
		return $emailData;
	}

	public function generateRecomendacionProductoNoRegistradoEmail($mailDestino, $nombrePadrino, $apellidoPadrino, $objCampania, $objProducto, $idAmigo, $nombreUsuario, $IdCampania, $IdCategoria, $IdProducto) {
		
		$oTexto = new Texto(Aplicacion::getRoot()."front/emails/vistazo-sinprecio.html");
		
        $oTexto->Read();
        $oTexto->ReemplazarString("%Ruta URL%", Aplicacion::getRootUrl());
        $oTexto->ReemplazarString("%Destino%", $mailDestino);
        $oTexto->ReemplazarString("%NombrePadrino%", Aplicacion::Decrypter($nombrePadrino));
        $oTexto->ReemplazarString("%ApellidoPadrino%", Aplicacion::Decrypter($apellidoPadrino));
        $oTexto->ReemplazarString("%NombreVidriera%", $objCampania->getNombre());
        $oTexto->ReemplazarString("%FinVidriera%", $objCampania->getFechaFin());
        $oTexto->ReemplazarString("%NombreProducto%", $objProducto->getNombre());
        $oTexto->ReemplazarString("%IdUsuario%", $idAmigo);
	    $oTexto->ReemplazarString("%HashIdUsuario%", generar_hash_md5($idAmigo));
	    $oTexto->ReemplazarString("%Hash%", Hash::getHash($idAmigo.$mailDestino));
	    $oTexto->ReemplazarString("%NombreUsuario%", $nombreUsuario);
           
        $oTexto->ReemplazarString("%DestinoLink%", Aplicacion::getRootUrl()."logica/autologin/login.php?IdUsuario=".$idAmigo."&Hash=".Hash::getHash($idAmigo.$mailDestino)."&Type=RECOMENDAR&IdCampania=".$IdCampania."&IdCategoria=".$IdCategoria."&IdProducto=".$IdProducto);
          
            
        $oTexto->ReemplazarString("%Descripcion%", $objProducto->getDescripcion());
        $oTexto->ReemplazarString("%Descripcion Ampliada%", $objProducto->getDescripcionAmpliada());
        $IMGS = DirectorytoArray(Aplicacion::getRoot() . "front/productos/productos_". $objProducto->getIdProducto(). "/", 3,array("jpg","png", "gif"));

        if (count($IMGS["producto_1"]))
            $oTexto->ReemplazarString("%ImagenProducto%", Aplicacion::getRootUrl() . "front/productos/productos_". $objProducto->getIdProducto(). "/".$IMGS["producto_1"]);
        else
        $oTexto->ReemplazarString("%ImagenProducto%", Aplicacion::getRootUrl() . "front/productos/noimagen.jpg");

		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_recomendacion"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($mailDestino);
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("recomendacionproducto");
		$emailData->setSubject(Aplicacion::Decrypter($nombrePadrino)." ".Aplicacion::Decrypter($apellidoPadrino)." te recomienda este producto");
		
		return $emailData;
	}

	public function generateRespuestaBOEmail($oMensaje, $oMensajeAnt, $strMensajeText) {
		if ($oMensajeAnt->getidUsuario()!=null && $oMensajeAnt->getidUsuario()>0) {
        	$hashu = $NewHash = Hash::getHash($oMensajeAnt->getidUsuario().$oMensajeAnt->getemail());
        	$idu = $oMensajeAnt->getidUsuario();
       		$mailUrl = "http://www.geelbe.com/co/logica/autologin/login.php?IdUsuario=$idu&Hash=$hashu&Type=CONTACTO";
		} 
		else {
			$mailUrl = "http://www.geelbe.com/co/front/contacto/";
		}
        
        $strMensaje = htmlentities($oMensaje->getmensaje());
        $strMensaje .= "<br><br>--<br><br>";
        $strMensaje .= "Mensaje original escrito el " . $oMensaje->getfecha() . ": <br><i>".htmlentities($oMensajeAnt->getmensaje())."</i>";
        $strMensaje .= "<br><br>--<br><br>";
        $strMensajeText .= "Para contestar este mensaje debes ingresar a: $mailUrl";
        $strMensaje .= "<br><br>--<br>";
        $strMensajeText .= "Geelbe - www.geelbe.com";
        
                    
        $strMensajeText = stripslashes($oMensaje->getmensaje());
        $strMensajeText .= "\n\n--\n\n";
        $strMensajeText .= "Mensaje original escrito el " . $oMensaje->getfecha() . ": \n".stripslashes($oMensajeAnt->getmensaje());
        $strMensajeText .= "\n\n--\n\n";
        $strMensajeText .= "Para contestar este mensaje debes ingresar a: $mailUrl";
        $strMensajeText .= "\n\n--\n";
        $strMensajeText .= "Geelbe - www.geelbe.com";
        
        
		$emailData = new EmailData();
		$emailData->setFromEmail("geelbe@geelbemail.com");
		$emailData->setFromName("Geelbe");
		$emailData->setTo($oMensajeAnt->getemail());
		//$emailData->setBody($strMensaje);
		//$emailData->setAltBody($strMensajeText);

		$emailData->setBody($strMensajeText);
		$emailData->setTextOnlyMail(true);
				
		$emailData->setEmailType("respuestabo");
		$emailData->setSubject("Esta es una respuesta al mensaje #".$oMensajeAnt->getidMensaje());
		$emailData->setReplyTo("no-reply@geelbe.com");
				
			
		return $emailData;
	}

	public function generateGeelbeCashTrxAcceptedEmail($objUsuario, $monto) {
		
		$oTexto = new Texto(Aplicacion::getRoot() . "front/emails/geelbecash_accepted.html");
        $oTexto->Read();
        $oTexto->ReemplazarString("%Nombre%", $objUsuario->getDatos()->getNombre());
        $oTexto->ReemplazarString("%ContextRoot%", Aplicacion::getRootUrl());
        $oTexto->ReemplazarString("%ServerImgs%", Aplicacion::getParametros("static_urls", "img_url"));
        $oTexto->ReemplazarString("%Email%", $objUsuario->getNombreUsuario());
        $oTexto->ReemplazarString("%IdUsuario%", $objUsuario->getIdUsuario());
        $oTexto->ReemplazarString("%Hash%", Hash::getHash($objUsuario->getIdUsuario().$objUsuario->getNombreUsuario()));
        $oTexto->ReemplazarString("%MontoFormateado%", number_format($monto, 0, ',', '.'));

		$emailData = new EmailData();
		$emailData->setFromEmail("geelbe@geelbemail.com");
		$emailData->setFromName("Geelbe");
		$emailData->setTo($objUsuario->getNombreUsuario());
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("geelbecashacceptedtrx");
		$emailData->setSubject("Se ha acreditado tu carga de GCash");
		
		return $emailData;
	}
	
	public function generateGeelbeCashTrxAcceptedEmailAdmin($to, $objUsuario, $monto) {
		
		$oTexto = new Texto(Aplicacion::getRoot() . "front/emails/geelbecash_accepted_admin.html");
        $oTexto->Read();
        $oTexto->ReemplazarString("%NombreUsuario%", $objUsuario->getNombreUsuario());
        $oTexto->ReemplazarString("%ContextRoot%", Aplicacion::getRootUrl());
        $oTexto->ReemplazarString("%ServerImgs%", Aplicacion::getParametros("static_urls", "img_url"));
        $oTexto->ReemplazarString("%Email%", $objUsuario->getNombreUsuario());
        $oTexto->ReemplazarString("%IdUsuario%", $objUsuario->getIdUsuario());
        $oTexto->ReemplazarString("%Hash%", Hash::getHash($objUsuario->getIdUsuario().$objUsuario->getNombreUsuario()));
        $oTexto->ReemplazarString("%MontoFormateado%", number_format($monto, 0, ',', '.'));

		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($to);
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("geelbecashacceptedtrx");
		$emailData->setSubject("Se ha acreditado tu carga de GCash");
		
		return $emailData;
	}
	
	public function generateGeelbeCashTrxCancelledEmail($objUsuario, $monto) {
		
		$oTexto = new Texto(Aplicacion::getRoot() . "front/emails/geelbecash_cancelled.html");
        $oTexto->Read();
        $oTexto->ReemplazarString("%Nombre%", $objUsuario->getDatos()->getNombre());
        $oTexto->ReemplazarString("%ContextRoot%", Aplicacion::getRootUrl());
        $oTexto->ReemplazarString("%ServerImgs%", Aplicacion::getParametros("static_urls", "img_url"));
        $oTexto->ReemplazarString("%Email%", $objUsuario->getNombreUsuario());
        $oTexto->ReemplazarString("%IdUsuario%", $objUsuario->getIdUsuario());
        $oTexto->ReemplazarString("%Hash%", Hash::getHash($objUsuario->getIdUsuario().$objUsuario->getNombreUsuario()));
        $oTexto->ReemplazarString("%MontoFormateado%", number_format($monto, 0, ',', '.'));

		$emailData = new EmailData();
		$emailData->setFromEmail(Aplicacion::getParametros("info", "email_envios"));
		$emailData->setFromName("Geelbe");
		$emailData->setTo($objUsuario->getNombreUsuario());
		$emailData->setBody($oTexto->getText());
		$emailData->setAltBody("No alternative text yet.");
		$emailData->setEmailType("geelbecashcancelledtrx");
		$emailData->setSubject("No se ha podido realizar tu carga de GCash");
		
		return $emailData;
	}
}

?>