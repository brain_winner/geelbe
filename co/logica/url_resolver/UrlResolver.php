<?
    class UrlResolver {

        public static function getImgBaseUrl($imagePath = "") {
            return Aplicacion::getParametros("static_urls", "img_url").$imagePath;
        }

        public static function getCssBaseUrl($imagePath = "") {
            return Aplicacion::getParametros("static_urls", "css_url").$imagePath;
        }

        public static function getJsBaseUrl($imagePath = "") {
            return Aplicacion::getParametros("static_urls", "js_url").$imagePath;
        }

        public static function getBaseUrl($imagePath = "", $ssl = false) {
        	$url = Aplicacion::getParametros("urls", "base_url");
        	if($ssl) $url = str_replace('http://', 'https://', $url);
            return $url.$imagePath;
        }
    }
?>