<?   
	class dmUsuariosFacebook  {
			public static function saveOrUpdateFacebookId($userId = null, $facebookId = null) {
			$oConexion = Conexion::nuevo();
	        $oConexion->Abrir();
	        $oConexion->setQuery('SELECT * FROM usuarios_facebook WHERE IdUsuario = '.mysql_real_escape_string($userId));
			$DataFacebook = $oConexion->DevolverQuery();
			if(empty($DataFacebook)) {
				$oConexion = Conexion::nuevo();
		        $oConexion->Abrir();
		        $oConexion->setQuery('SELECT * FROM usuarios_facebook WHERE FacebookId = '.mysql_real_escape_string($facebookId));
				$DataFacebook = $oConexion->DevolverQuery();
			    
				if(empty($DataFacebook)) {
					$oConexion = Conexion::nuevo();
					$oConexion->setQuery("INSERT INTO usuarios_facebook (FacebookId, IdUsuario) VALUES (".mysql_real_escape_string($facebookId).",".mysql_real_escape_string($userId).")");
					$oConexion->EjecutarQuery();
				} else {
					if($DataFacebook[0]["IdUsuario"] != $userId) {
						$oConexion = Conexion::nuevo();
						$oConexion->setQuery("UPDATE usuarios_facebook SET IdUsuario = ".mysql_real_escape_string($userId)." WHERE FacebookId = ".mysql_real_escape_string($facebookId));
						$oConexion->EjecutarQuery();
					}
				}
				
			} else {
				if($DataFacebook[0]["FacebookId"] != $facebookId) {
					$oConexion = Conexion::nuevo();
					$oConexion->setQuery("UPDATE usuarios_facebook SET FacebookId = ".mysql_real_escape_string($facebookId)." WHERE IdUsuario = ".mysql_real_escape_string($userId));
					$oConexion->EjecutarQuery();
				}
			}
        }
        
        public static function updateUserInfo($objUsuario = null, $userData = null) {
        	$actualizarUsuario = false;
        	
        	$nombre = $objUsuario->getDatos()->getNombre();
		    if (($nombre == null || $nombre == "" || $nombre == "Socio Geelbe") && $userData->first_name != null && $userData->first_name != "") {
		    	$actualizarUsuario = true;
		    	$objUsuario->getDatos()->setNombre(utf8_decode($userData->first_name));
		    }
		
		    $apellido = $objUsuario->getDatos()->getApellido();
		    if (($apellido == null || $apellido == "") && $userData->last_name != null && $userData->last_name != "") {
		    	$actualizarUsuario = true;
		    	$objUsuario->getDatos()->setApellido(utf8_decode($userData->last_name));
		    }
		
		    $fechaNacimiento = $objUsuario->getDatos()->getFechaNacimiento();
		    if (($fechaNacimiento == null || $fechaNacimiento == "" || $fechaNacimiento == "0000-00-00") && (($userData->birthday_date != null && $userData->birthday_date != "") ||($userData->birthday != null && $userData->birthday != ""))) {
				if(isset($userData->birthday_date)) {
			    	$pieces = explode("/", $userData->birthday_date);
				} else {
			    	$pieces = explode("/", $userData->birthday);
				}
				$d = $pieces[1];
				$m = $pieces[0];
				$y = $pieces[2];
		    	$actualizarUsuario = true;
		    	$objUsuario->getDatos()->setFechaNacimiento($y."-".$m."-".$d);
		    }
		
		    $genero = $objUsuario->getDatos()->getIdTratamiento();
		    if (($genero == null || $genero == "" || $genero == 0) && $userData->sex != null && $userData->sex != "") {
		    	$actualizarUsuario = true;
		    	if($userData->sex == "male" || $userData->sex == "hombre") {
			    	$objUsuario->getDatos()->setIdTratamiento(1);
		    	} else {
			    	$objUsuario->getDatos()->setIdTratamiento(2);
		    	}
		    }
		    
		    return $actualizarUsuario;
        }
        
		public static function getFacebookId($userId = null) {
			$oConexion = Conexion::nuevo();
            $oConexion->setQuery("SELECT FacebookId FROM usuarios_facebook WHERE IdUsuario = ".mysql_real_escape_string($userId));
            $result = $oConexion->DevolverQuery();
            return $result[0]["FacebookId"];
        }
        
   }
?>      