<?php
	define("GCASH_PER_LIKE", 200);
	
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("catalogo"));
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_transaction.php";
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_dn.php";
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_service.php";
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/datamappers/dmgeelbecash.php";
	} catch (Exception $e) {
		echo '{"error":"true","error_description":"Problemas al cargar los includes."}';
		die;
	}
	
	try {
		ValidarUsuarioLogueado();
	} catch (Exception $e) {
		echo '{"error":"true","error_description":"Usuario no logueado."}';
		die;
	}
	
	$IdUsuario = Aplicacion::Decrypter($_SESSION["User"]["id"]);
	$EmailUsuario = Aplicacion::Decrypter($_SESSION["User"]["email"]);

	try {
		$isPageOk = true;
		$pageException = "";
		
		// Validaciones de parametros basicas
		if(!isset($_GET["IdCampania"]) && !isset($_GET["IdProducto"])) { // IdCampania y IdProducto Seteado
            $isPageOk = false;
			$pageException = "IdCampania or IdProducto not setted";
		}
		else if (!ValidatorUtils::validateNumber($_GET["IdCampania"])) { // IdCampania Numerico
			$isPageOk = false;
			$pageException = "IdCampania is not a number";
		}
		else if (!ValidatorUtils::validateNumber($_GET["IdProducto"])) { // IdProducto Numerico
			$isPageOk = false;
			$pageException = "IdProducto is not a number";
		}
		else if (!dmProductos::isCampaignProduct($_GET["IdProducto"], $_GET["IdCampania"])) { // IdProducto Correspondiente con IdCampania
			$isPageOk = false;
			$pageException = "IdProducto no corresponde con IdCampania";
		}
		else if (!dmCampania::EsCampaniaAccesible($_GET["IdCampania"])) { // IdCampania Accesible
			$isPageOk = false;
			$pageException = "Campaña not accesible";
		}
		else if (isset($_GET["IdCategoria"])) { 
			if (!ValidatorUtils::validateNumber($_GET["IdCategoria"])) { // IdCategoria Numerico
				$isPageOk = false;
				$pageException = "IdCategoria is not a number";
			}
			else if (!dmCampania::isCampaignCategory($_GET["IdCampania"], $_GET["IdCategoria"])) { // IdCategoria Correspondiente con IdCampania
				$isPageOk = false;
				$pageException = "IdCampania no corresponde con IdCategoria";
			}
		}
		
		if (!$isPageOk) {
			echo '{"error":"true","error_description":"'.$pageException.'"}';
			die;
		}


	} catch(Exception $e) {
		echo '{"error":"true","error_description":"Problemas al procesar los parametros."}';
		die;
    }
    
    $IdProducto = $_GET["IdProducto"];
    $IdCampania = $_GET["IdCampania"];
    $IdCategoria = $_GET["IdCategoria"];
	
	try {
	    $oConexion = Conexion::nuevo();
		$oConexion->setQuery("SELECT * FROM usuarios_facebook WHERE IdUsuario = ".mysql_real_escape_string($IdUsuario));                
		$result = $oConexion->DevolverQuery();
		
		if ($result != null && $result[0] != null && $result[0]["IdUsuario"] != null) {
			$oConexion->setQuery("SELECT count(id) as likes_del_mes FROM fb_likes_productos WHERE IdUsuario = ".mysql_real_escape_string($IdUsuario)." and date(FechaCreacion) BETWEEN DATE_SUB(CURDATE(),INTERVAL 30 DAY) AND CURDATE()");                
			$result = $oConexion->DevolverQuery();
			
			if ($result[0]["likes_del_mes"] <= 120) {
				$gcash_trx_id = GeelbeCashService::cargarCredito($EmailUsuario, GCASH_PER_LIKE, "Like Prod. " + $IdProducto);
				
				$oConexion->setQuery('INSERT INTO fb_likes_productos (IdUsuario, IdProducto, GCashTrxId, FechaCreacion) VALUES ('.mysql_real_escape_string($IdUsuario).', '.mysql_real_escape_string($IdProducto).', '.mysql_real_escape_string($gcash_trx_id).', NOW())');
				$oConexion->EjecutarQuery();
			} else {
				echo '{"error":"true","error_description":"Se supera los 120 likes por mes.","error_type":"likes_per_month_already_reach"}';
				die;
			}
		} else {
			echo '{"error":"true","error_description":"Debe estar conectar su cuenta de Facebook con la de Geelbe.","error_type":"account_not_conected"}';
			die;
		}
	} catch (Exception $e) {
		echo '{"error":"true","error_description":"Problemas al procesar la transaccion."}';
		die;
	}
	
	echo '{"error":"false","error_description":""}';
	die;
?>