<?php
	try {
		$confRoot = explode("/",dirname($_SERVER["SCRIPT_NAME"]));
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/conf/configuracion.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica_bo/validators/ValidatorUtils.php");
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("login", "clases"));
		Aplicacion::CargarIncludes(Aplicacion::getIncludes("catalogo"));
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_transaction.php";
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_dn.php";
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/classes/geelbecash_service.php";
		require_once $_SERVER['DOCUMENT_ROOT']."/".$confRoot[1]."/logica/geelbecash/datamappers/dmgeelbecash.php";
	} catch (Exception $e) {
		echo '{"error":"true","error_description":"Problemas al cargar los includes."}';
		die;
	}
	
	try {
		ValidarUsuarioLogueado();
	} catch (Exception $e) {
		echo '{"error":"true","error_description":"Usuario no logueado."}';
		die;
	}
	
	$IdUsuario = Aplicacion::Decrypter($_SESSION["User"]["id"]);
	$EmailUsuario = Aplicacion::Decrypter($_SESSION["User"]["email"]);

	try {
		$isPageOk = true;
		$pageException = "";
		
		// Validaciones de parametros basicas
		if(!isset($_GET["IdCampania"]) && !isset($_GET["IdProducto"])) { // IdCampania y IdProducto Seteado
            $isPageOk = false;
			$pageException = "IdCampania or IdProducto not setted";
		}
		else if (!ValidatorUtils::validateNumber($_GET["IdCampania"])) { // IdCampania Numerico
			$isPageOk = false;
			$pageException = "IdCampania is not a number";
		}
		else if (!ValidatorUtils::validateNumber($_GET["IdProducto"])) { // IdProducto Numerico
			$isPageOk = false;
			$pageException = "IdProducto is not a number";
		}
		else if (!dmProductos::isCampaignProduct($_GET["IdProducto"], $_GET["IdCampania"])) { // IdProducto Correspondiente con IdCampania
			$isPageOk = false;
			$pageException = "IdProducto no corresponde con IdCampania";
		}
		else if (!dmCampania::EsCampaniaAccesible($_GET["IdCampania"])) { // IdCampania Accesible
			$isPageOk = false;
			$pageException = "Campaña not accesible";
		}
		else if (isset($_GET["IdCategoria"])) { 
			if (!ValidatorUtils::validateNumber($_GET["IdCategoria"])) { // IdCategoria Numerico
				$isPageOk = false;
				$pageException = "IdCategoria is not a number";
			}
			else if (!dmCampania::isCampaignCategory($_GET["IdCampania"], $_GET["IdCategoria"])) { // IdCategoria Correspondiente con IdCampania
				$isPageOk = false;
				$pageException = "IdCampania no corresponde con IdCategoria";
			}
		}
		
		if (!$isPageOk) {
			echo '{"error":"true","error_description":"'.$pageException.'"}';
			die;
		}


	} catch(Exception $e) {
		echo '{"error":"true","error_description":"Problemas al procesar los parametros."}';
		die;
    }
    
    $IdProducto = $_GET["IdProducto"];
    $IdCampania = $_GET["IdCampania"];
    $IdCategoria = $_GET["IdCategoria"];
	
	try {
	    $oConexion = Conexion::nuevo();
		$oConexion->setQuery("SELECT * FROM fb_likes_productos WHERE IdUsuario = ".mysql_real_escape_string($IdUsuario). " and IdProducto = ".mysql_real_escape_string($IdProducto));                
		$result = $oConexion->DevolverQuery();
		
		if ($result != null && $result[0] != null && $result[0]["IdUsuario"] != null) {
			GeelbeCashService::anularTransaccion($result[0]["GCashTrxId"], "Unlike Prod. ".$IdProducto, "null");
			
			$oConexion->setQuery('DELETE FROM fb_likes_productos WHERE IdUsuario = '.mysql_real_escape_string($IdUsuario).' and IdProducto = '.mysql_real_escape_string($IdProducto));
			$oConexion->EjecutarQuery();
		} else {
			echo '{"error":"true","error_description":"Llamada a servicio incorrecta."}';
			die;
		}
	} catch (Exception $e) {
		echo $e;
		echo '{"error":"true","error_description":"Problemas al procesar la transaccion."}';
		die;
	}
	
	echo '{"error":"false","error_description":""}';
	die;
?>