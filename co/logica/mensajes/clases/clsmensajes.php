<?
class Mensajes {
	/*Datos Privados*/
	private $_idmensaje;
	private $_idusuario;
	private $_idmensajepadre;
	private $_fecha;
	private $_email;
	private $_mensaje;
	private $_respondido;
	private $_asunto;
	private $_idmotivo;
	private $_IdCampania;
	private $_cerrado;
	private $_customercare;
	private $_boUserId;

	/*Constructor*/
	/**
	 * @var idmensaje
	 * @var idusuario
	 * @var idmensajepadre
	 * @var fecha
	 * @var email
	 * @var mensaje
	 * @var respondido
	 * @var asunto
	 * @var idmotivo
	 * @var IdCampania
	 * @var cerrado
	 * @var customercare
	 * @var boUserId
	 */
	function __construct($idmensaje=0, $idusuario=0, $idmensajepadre=0, $fecha=0, $email='', 
	                     $mensaje='', $respondido='false', $asunto='', $idmotivo=0, $IdCampania=-2, 
	                     $cerrado='false', $customercare='false', $boUserId='null') {
		 
		$this->_idmensaje=$idmensaje;
		$this->_idusuario=$idusuario;
		$this->_idmensajepadre=$idmensajepadre;
		$this->_fecha=$fecha;
		$this->_email=$email;
		$this->_mensaje=$mensaje;
		$this->_respondido=$respondido;
		$this->_asunto=$asunto;
		$this->_idmotivo=$idmotivo;
		$this->_IdCampania=$IdCampania;
		$this->_cerrado=$cerrado;
		$this->_customercare=$customercare;
		$this->_boUserId=$boUserId;
	}
	
	/*Metodos Publicos*/
	/**
	 * Set boUserId
	 * @var boUserId
	 */
	public function setboUserId($boUserId) {
		$this->_boUserId=$boUserId;
	}
	/**
	 * Get boUserId
	 * @return boUserId
	 */
	public function getboUserId() {
		return $this->_boUserId;
	}
	/**
	 * Set customercare
	 * @var customercare
	 */
	function setcustomercare($customercare) {
		$this->_customercare=$customercare;
	}
	/**
	 * Get customercare
	 * @return customercare
	 */
	function getcustomercare() {
		return $this->_customercare;
	}
	/**
	 * Set cerrado
	 * @var cerrado
	 */
	function setcerrado($cerrado) {
		$this->_cerrado=$cerrado;
	}
	/**
	 * Get cerrado
	 * @return cerrado
	 */
	function getcerrado() {
		return $this->_cerrado;
	}
	/**
	 * Set idMensaje
	 * @var idMensaje
	 */
	function setIdCampania($IdCampania) {
		$this->_IdCampania=$IdCampania;
	}
	/**
	 * Get idMensaje
	 * @return idMensaje
	 */
	function getIdCampania() {
		return $this->_IdCampania;
	}
	/**
	 * Set idMensaje
	 * @var idMensaje
	 */
	function setidMensaje($idmensaje) {
		$this->_idmensaje =$idmensaje;
	}
	/**
	 * Get idMensaje
	 * @return idMensaje
	 */
	function getidMensaje() {
		return $this->_idmensaje;
	}
	/**
	 * Set idUsuario
	 * @var idUsuario
	 */
	function setidUsuario($idusuario) {
		$this->_idusuario =$idusuario;
	}
	/**
	 * Get idUsuario
	 * @return idUsuario
	 */
	function getidUsuario() {
		if (!is_null($this->_idusuario))
		return $this->_idusuario;
		else
		return "null";
	}
	/**
	 * Set idMensajePadre
	 * @var idMensajePadre
	 */
	function setidMensajePadre($idmensajepadre) {
		$this->_idmensajepadre =$idmensajepadre;
	}
	/**
	 * Get idMensajePadre
	 * @return idMensajePadre
	 */
	function getidMensajePadre() {
		if (!is_null($this->_idmensajepadre))
		return $this->_idmensajepadre;
		else
		return "null";
	}
	/**
	 * Set fecha
	 * @var fecha
	 */
	function setfecha($fecha) {
		$this->_fecha =$fecha;
	}
	/**
	 * Get fecha
	 * @return fecha
	 */
	function getfecha() {
		return $this->_fecha;
	}
	/**
	 * Set email
	 * @var email
	 */
	function setemail($email) {
		$this->_email =$email;
	}
	/**
	 * Get email
	 * @return email
	 */
	function getemail() {
		return $this->_email;
	}
	/**
	 * Set mensaje
	 * @var mensaje
	 */
	function setmensaje($mensaje) {
		$this->_mensaje =$mensaje;
	}
	/**
	 * Get mensaje
	 * @return mensaje
	 */
	function getmensaje() {
		return $this->_mensaje;
	}
	/**
	 * Set respondido
	 * @var respondido
	 */
	function setrespondido($respondido) {
		$this->_respondido =$respondido;
	}
	/**
	 * Get respondido
	 * @return respondido
	 */
	function getrespondido() {
		return $this->_respondido;
	}
	/**
	 * Set asunto
	 * @var asunto
	 */
	function setasunto($asunto) {
		$this->_asunto =$asunto;
	}
	/**
	 * Get asunto
	 * @return asunto
	 */
	function getasunto() {
		return $this->_asunto;
	}
	/**
	 * Set idmotivo
	 * @var idmotivo
	 */
	function setidmotivo($idmotivo) {
		$this->_idmotivo =$idmotivo;
	}
	/**
	 * Get idmotivo
	 * @return idmotivo
	 */
	function getidmotivo() {
		return $this->_idmotivo;
	}

	/*Destructor*/
	function __destruct() {}
}
?>
