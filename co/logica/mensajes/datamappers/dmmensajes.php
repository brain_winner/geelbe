<?
    class dmMensajes {
	    /**
	    *Realiza el INSERT en la base de datos
	    *@param class omensajes
	    *@return 1 si ejecuto con exito, 0 si ocurrio un error.
	    */
	    public static function Insert($omensajes) {		
		    $cn = Conexion::nuevo();
		    try {
			    $cn->Abrir_Trans();
			    $cn->setQuery("INSERT INTO mensajes (idUsuario, 
			                                         idMensajePadre, 
			                                         fecha, 
			                                         email, 
			                                         mensaje, 
			                                         respondido, 
			                                         asunto, 
			                                         idmotivo, 
			                                         IdCampania, 
			                                         cerrado, 
			                                         customercare, 
			                                         boUserId) 
			                   VALUES (".mysql_real_escape_string($omensajes->getidUsuario()).", "
			                            .mysql_real_escape_string($omensajes->getidMensajePadre()).", '"
			                            .mysql_real_escape_string($omensajes->getfecha())."', '"
			                            .htmlspecialchars(mysql_real_escape_string($omensajes->getemail()), ENT_QUOTES)."', '"
			                            .htmlspecialchars(mysql_real_escape_string($omensajes->getmensaje()), ENT_QUOTES)."', "
			                            .mysql_real_escape_string($omensajes->getrespondido()).", '"
			                            .htmlspecialchars(mysql_real_escape_string($omensajes->getasunto()), ENT_QUOTES)."', "
			                            .mysql_real_escape_string($omensajes->getidmotivo()).", "
			                            .mysql_real_escape_string($omensajes->getIdCampania()).", "
			                            .mysql_real_escape_string($omensajes->getcerrado()).", "
			                            .mysql_real_escape_string($omensajes->getcustomercare()).", "
			                            .mysql_real_escape_string($omensajes->getboUserId()).")");
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return true;
		    }
		    catch(exception $e) {
			   $cn->RollBack();
			   throw $e;
		    }		
	    }
	    /**
	    *Realiza el UPDATE en la base de datos
	    *@param class omensajes
	    *@return 1 si ejecuto con exito, 0 si ocurrio un error.
	    */
	    public static function Update($omensajes)
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir_Trans();
			    $cn->setQuery("UPDATE mensajes SET 
				                   idUsuario = ". mysql_real_escape_string($omensajes->getidUsuario()) .", 
				                   idMensajePadre = ". mysql_real_escape_string($omensajes->getidMensajePadre()) .", 
				                   fecha = '". mysql_real_escape_string($omensajes->getfecha()) ."', 
				                   email = '". htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($omensajes->getemail())), ENT_QUOTES) ."', 
				                   mensaje = '". htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($omensajes->getmensaje())), ENT_QUOTES) ."', 
				                   respondido = ". mysql_real_escape_string($omensajes->getrespondido()) .", 
				                   asunto = '". htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($omensajes->getasunto())), ENT_QUOTES) ."', 
				                   idmotivo = ". mysql_real_escape_string($omensajes->getidmotivo()) .", 
				                   IdCampania = ". mysql_real_escape_string($omensajes->getIdCampania()) .", 
				                   cerrado = ". mysql_real_escape_string($omensajes->getcerrado()) .", 
				                   customercare = ". mysql_real_escape_string($omensajes->getcustomercare()) .", 
				                   boUserId = ". mysql_real_escape_string($omensajes->getboUserId()) ." 
			                   WHERE idMensaje = ". mysql_real_escape_string($omensajes->getidMensaje()) ."");
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return 1;
		    }
		    catch(exception $e)
		    {
			    $cn->RollBack();
			    throw $e;
		    }		
	    }
	    /**
	    *Realiza el DELETE en la base de datos
	    *@param object omensajes
	    *@return 1 si ejecuto con exito, 0 si ocurrio un error.
	    */
	    public static function Delete($omensajes)
	    {		
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir_Trans();
			    $cn->setQuery("DELETE FROM mensajes WHERE idMensaje = ". mysql_real_escape_string($omensajes->getidMensaje()) ."");
			    $cn->EjecutarQuery();
			    $cn->Cerrar_Trans();
			    return 1;
		    }
		    catch(exception $e)
		    {
			    $cn->RollBack();
			    throw $e;
		    }		
	    }
        /**
        *Realiza un SELECT que devuelve todos los registros de mensajes
        *@return array Assoc, si ocurrio un error devuelve array()
        */
        public static function getConversacion($idMensaje)
        {        
            $cn = Conexion::nuevo();
            try
            {
                $cn->Abrir();
                $cn->setQuery("SELECT idMensaje, idMensajePadre, idUsuario, Fecha, email as 'Nombre de usuario', mensaje, Asunto, If(respondido=1,'Si','No') as Respondido, idmotivo, cerrado, customercare, boUserId FROM mensajes WHERE idMensaje=".mysql_real_escape_string($idMensaje)." OR idMensajePadre = " . mysql_real_escape_string($idMensaje));
                $Tabla = $cn->DevolverQuery();
                $cn->Cerrar();
                return $Tabla;
            }
            catch(exception $e)
            {            
                throw $e;                   
            }        
        }
        /**
        *Realiza un SELECT que devuelve todos los registros de mensajes
        *@return array Assoc, si ocurrio un error devuelve array()
        */
        public static function getMensajesRespondidos_ByIdMensaje($idMensaje)
        {        
            $cn = Conexion::nuevo();
            try
            {
                $cn->Abrir();
                $cn->setQuery("SELECT idMensaje, idMensajePadre, idUsuario, Fecha, email as 'Nombre de usuario', mensaje, Asunto, If(respondido=1,'Si','No') as Respondido, idmotivo FROM mensajes WHERE idMensajePadre = " . mysql_real_escape_string($idMensaje));
                $Tabla = $cn->DevolverQuery();
                $cn->Cerrar();
                return $Tabla;
            }
            catch(exception $e)
            {            
                throw $e;                   
            }        
        }
        /**
        *Realiza un SELECT que devuelve todos los registros de mensajes
        *@return array Assoc, si ocurrio un error devuelve array()
        */
        public static function getMensajes_ByIdUsuario($idUsuario, $filtro) {        
            $cn = Conexion::nuevo();
            try {
                $cn->Abrir();
                $cn->setQuery("SELECT idMensaje, 
                                      idMensajePadre, 
                                      idUsuario, 
                                      Fecha, 
                                      email as 'Nombre de usuario', 
                                      Mensaje, 
                                      Asunto, 
                                      If(respondido=1,'Si','No') as Respondido, 
                                      idmotivo as IdMotivo 
                               FROM mensajes 
                               WHERE (mensaje LIKE '%".mysql_real_escape_string($filtro)."%' OR Asunto LIKE '%".mysql_real_escape_string($filtro)."%') 
                               AND idUsuario = " . mysql_real_escape_string($idUsuario) . " ORDER BY Fecha DESC");                
                
                $Tabla = $cn->DevolverQuery();
                $cn->Cerrar();
                return $Tabla;
            }
            catch(exception $e)
            {            
                throw $e;                   
            }        
        }
        
        public static function getMensajes_ByEmail($email, $filtro) {        
            $cn = Conexion::nuevo();
            try {
                $cn->Abrir();
                $cn->setQuery("SELECT idMensaje, 
                                      idMensajePadre, 
                                      idUsuario, 
                                      Fecha, 
                                      email as 'Nombre de usuario', 
                                      Mensaje, 
                                      Asunto, 
                                      If(respondido=1,'Si','No') as Respondido, 
                                      idmotivo as IdMotivo 
                               FROM mensajes 
                               WHERE (mensaje LIKE '%".mysql_real_escape_string($filtro)."%' OR Asunto LIKE '%".mysql_real_escape_string($filtro)."%') 
                               AND email = '" . mysql_real_escape_string($email) . "' ORDER BY Fecha DESC");                
                
                $Tabla = $cn->DevolverQuery();
                $cn->Cerrar();
                return $Tabla;
            }
            catch(exception $e)
            {            
                throw $e;                   
            }        
        }
        
        public static function getMensajesUsuarios_ById($IdUsuario, $columnaNro, $columnaSentido, $filter = "")
        {        
            $cn = Conexion::nuevo();
            try
            {
                $cn->Abrir();
                if (!($columnaNro && $columnaSentido)) {
                    $cn->setQuery("SELECT idMensaje, idMensajePadre, idUsuario, Fecha, email as 'Nombre de usuario', mensaje, Asunto, If(respondido=1,'Si','No') as Respondido, idmotivo FROM mensajes WHERE idUsuario = ".mysql_real_escape_string($IdUsuario)." AND idMensajePadre IS NULL AND (Fecha LIKE \"%".mysql_real_escape_string($filter)."%\" OR email LIKE \"%".mysql_real_escape_string($filter)."%\" OR mensaje LIKE \"%".mysql_real_escape_string($filter)."%\" OR Asunto LIKE \"%".mysql_real_escape_string($filter)."%\") ORDER BY respondido, fecha ASC");
				}
                else {
                    $cn->setQuery("SELECT idMensaje, idMensajePadre, idUsuario, Fecha, email as 'Nombre de usuario', mensaje, Asunto, If(respondido=1,'Si','No') as Respondido, idmotivo FROM mensajes WHERE idUsuario = ".mysql_real_escape_string($IdUsuario)." AND idMensajePadre IS NULL AND (Fecha LIKE \"%".mysql_real_escape_string($filter)."%\" OR email LIKE \"%".mysql_real_escape_string($filter)."%\" OR mensaje LIKE \"%".mysql_real_escape_string($filter)."%\" OR Asunto LIKE \"%".mysql_real_escape_string($filter)."%\") ORDER BY " . mysql_real_escape_string($columnaNro) . " " . mysql_real_escape_string($columnaSentido));
				}
                $Tabla = $cn->DevolverQuery();
                $cn->Cerrar();
                return $Tabla;
            }
            catch(exception $e)
            {            
                throw $e;                   
            }        
        }
        
    
	    
	    public static function getSelectString() {
	    	return " SELECT ME.idMensaje as 'ME.idMensaje', 
					 		ME.idMensajePadre as 'ME.idMensajePadre', 
							ME.idUsuario as 'ME.idUsuario', 
							ME.fecha as 'ME.fecha', 
							ME.email as 'ME.email', 
							CONCAT(LEFT(CONCAT(CONCAT(ME.asunto, ' '), ME.mensaje), 25),'...') as 'ME.mensaje',
							If(ME.respondido=1,'Si','No') as 'Respondido', 
							MO.Descripcion as 'MO.Descripcion',  
							IF(ME.IdCampania=-2, 'Sin Campa&ntilde;a', IF(ME.IdCampania=-1, 'Otra Campa&ntilde;a', CA.Nombre)) as 'CA.Nombre', 
							ME.IdCampania as 'ME.IdCampania',
							ME.cerrado as 'ME.cerrado', 
							ME.customercare as 'ME.customercare', 
							ME.asignedTo as 'ME.asignedTo', 
                            U.NombreUsuario as 'U.NombreUsuario', 
							ME.boUserId as 'ME.boUserId' ";
	    }
	    
	    public static function getFromString() {
	    	return " FROM mensajes ME 
					 LEFT JOIN motivos MO ON MO.idmotivo = ME.idmotivo 
					 LEFT JOIN campanias CA ON CA.IdCampania = ME.IdCampania 
					 LEFT JOIN usuarios U ON U.IdUsuario = ME.asignedTo ";
	    }
	    
	    public static function getOrderAndLimit($sortSQL, $offset, $itemsPerPage) {
	    	return " ORDER BY $sortSQL 
					 LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage);
	    }
        
	    
	    public static function getMensajesUsuariosBaseCount($whereClause = "") {
			$cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
                $cn->setQuery("SELECT count(*) as total "
							  .dmMensajes::getFromString()
							  .$whereClause);
				$Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();
			    return $Tabla[0]['total'];
		    }
		    catch(exception $e) {			
			    throw $e;                   
		    }		
	    }
	    
		public static function getMensajesUsuariosBasePaginados($sortArray = array(), $whereClause="", $offset=0, $itemsPerPage=10) {
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
		    $cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
                $cn->setQuery(dmMensajes::getSelectString()
                			 .dmMensajes::getFromString()
                			 .$whereClause
                             .dmMensajes::getOrderAndLimit($sortSQL, $offset, $itemsPerPage));
				$Tabla = $cn->DevolverQuery();
				$cn->Cerrar();
			    return $Tabla;
		    }
		    catch(exception $e) {			
			    throw $e;                   
		    }
		    
	    }	
	    
    	public static function getCampañasDeMensajesBase($whereClause="") {
			$cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
			    $cn->setQuery("SELECT ME.IdCampania as id,  
									  IF(ME.IdCampania=-2, 'Sin Campa&ntilde;a', IF(ME.IdCampania=-1, 'Otra Campa&ntilde;a', CA.Nombre)) as val 
							   FROM mensajes ME 
							   LEFT JOIN campanias CA ON CA.IdCampania = ME.IdCampania 
							   LEFT JOIN usuarios U ON U.IdUsuario = ME.asignedTo 
							   $whereClause  
							   GROUP BY ME.IdCampania 
							   ORDER BY val");

			    $Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();
			    return $Tabla;
		    }
		    catch(exception $e) {			
			    throw $e;                   
		    }		
	    }
	    
        public static function getMotivosDeMensajes() {
		    $cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
                $cn->setQuery("SELECT 	MO.idmotivo as id,
										MO.Descripcion as val
							  FROM motivos MO
							  ORDER BY val");
				$Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();
			    return $Tabla;
		    }
		    catch(exception $e) {			
			    throw $e;                   
		    }		
	    }
	    
    
	    //////////////////////////////////
		/// Mensajes Usuarios Cerrados ///
    	//////////////////////////////////
    			
	    public static function getMensajesUsuariosWhere($filtersSQL) {
	    	return " WHERE ME.idMensajePadre IS NULL 
					 AND ($filtersSQL) ";
	    }
	    
 		public static function getMensajesUsuariosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBaseCount($whereClause);	
	    }
	    
		public static function getMensajesUsuariosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBasePaginados($sortArray, $whereClause, $offset, $itemsPerPage);	
	    }	
	    
    	public static function getCampañasDeMensajes($paramArray = array()) {
    		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosWhere($filtersSQL);
			return dmMensajes::getCampañasDeMensajesBase($whereClause);	
	    }
	    
    
	    //////////////////////////////////
		/// Mensajes Usuarios Cerrados ///
    	//////////////////////////////////
    			
	    public static function getMensajesUsuariosCerradosWhere($filtersSQL) {
	    	return " WHERE ME.idMensajePadre IS NULL 
					 AND ME.cerrado = 1 
					 AND ($filtersSQL) ";
	    }
	    
 		public static function getMensajesUsuariosCerradosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosCerradosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBaseCount($whereClause);	
	    }
	    
		public static function getMensajesUsuariosCerradosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosCerradosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBasePaginados($sortArray, $whereClause, $offset, $itemsPerPage);	
	    }	
	    
    	public static function getCampañasDeMensajesCerrados($paramArray = array()) {
    		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosCerradosWhere($filtersSQL);
			return dmMensajes::getCampañasDeMensajesBase($whereClause);	
	    }
	    
	    
	    /////////////////////////////////////
		/// Mensajes Usuarios Respondidos ///
    	/////////////////////////////////////
    			
	    public static function getMensajesUsuariosRespondidosWhere($filtersSQL) {
	    	return " WHERE ME.idMensajePadre IS NULL 
					 AND ME.respondido = 1  
					 AND ($filtersSQL) ";
	    }
	    
 		public static function getMensajesUsuariosRespondidosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosRespondidosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBaseCount($whereClause);	
	    }
	    
		public static function getMensajesUsuariosRespondidosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosRespondidosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBasePaginados($sortArray, $whereClause, $offset, $itemsPerPage);	
	    }	
	    
    	public static function getCampañasDeMensajesRespondidos($paramArray = array()) {
    		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosRespondidosWhere($filtersSQL);
			return dmMensajes::getCampañasDeMensajesBase($whereClause);	
	    }
	    
	    
	    
	    /////////////////////////////////////
		/// Mensajes Pendientes ///
    	/////////////////////////////////////
    			
	    public static function getMensajesPendientesWhere($filtersSQL) {
	    	return " WHERE ME.idMensajePadre IS NULL 
					 AND ME.respondido = 1  
					 AND ME.cerrado = 0
					 AND ($filtersSQL) ";
	    }
	    
 		public static function getMensajesPendientesCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesPendientesWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBaseCount($whereClause);	
	    }
	    
		public static function getMensajesPendientesPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesPendientesWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBasePaginados($sortArray, $whereClause, $offset, $itemsPerPage);	
	    }	
	    
    	public static function getCampañasDeMensajesPendientes($paramArray = array()) {
    		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesPendientesWhere($filtersSQL);
			return dmMensajes::getCampañasDeMensajesBase($whereClause);	
	    }
	    
        //////////////////////////////////
		/// Mensajes Usuarios Abiertos ///
    	//////////////////////////////////
    			
	    public static function getMensajesUsuariosAbiertosWhere($filtersSQL) {
	    	return " WHERE ME.idMensajePadre IS NULL 
	    	         AND ME.cerrado = 0 
					 AND ($filtersSQL) ";
	    }
	    
 		public static function getMensajesUsuariosAbiertosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosAbiertosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBaseCount($whereClause);	
	    }
	    
		public static function getMensajesUsuariosAbiertosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosAbiertosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBasePaginados($sortArray, $whereClause, $offset, $itemsPerPage);	
	    }	
	    
    	public static function getCampañasDeMensajesAbiertos($paramArray = array()) {
    		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosAbiertosWhere($filtersSQL);
			return dmMensajes::getCampañasDeMensajesBase($whereClause);	
	    }
	    

    	//////////////////////////////////////////////
		/// Mensajes Usuarios Abiertos Sin Asignar ///
    	//////////////////////////////////////////////
    			
	    public static function getMensajesUsuariosAbiertosSinAsignarWhere($filtersSQL) {
	    	return " WHERE ME.idMensajePadre IS NULL 
					 AND ME.cerrado = 0 
					 AND ME.asignedTo is null 
					 AND ($filtersSQL) ";
	    }
	    
 		public static function getMensajesUsuariosAbiertosSinAsignarCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosAbiertosSinAsignarWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBaseCount($whereClause);	
	    }
	    
		public static function getMensajesUsuariosAbiertosSinAsignarPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosAbiertosSinAsignarWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBasePaginados($sortArray, $whereClause, $offset, $itemsPerPage);	
	    }	
	    
    	public static function getCampañasDeMensajesAbiertosSinAsignar($paramArray = array()) {
    		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosAbiertosSinAsignarWhere($filtersSQL);
			return dmMensajes::getCampañasDeMensajesBase($whereClause);	
	    }
	    

    	////////////////////////////////////////////
		/// Mensajes Usuarios Abiertos Asignados ///
    	////////////////////////////////////////////
    			
	    public static function getMensajesUsuariosAbiertosAsignadosWhere($filtersSQL) {
	    	return " WHERE ME.idMensajePadre IS NULL 
					 AND ME.cerrado = 0 
					 AND ME.asignedTo is not null 
					 AND ($filtersSQL) ";
	    }
	    
 		public static function getMensajesUsuariosAbiertosAsignadosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosAbiertosAsignadosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBaseCount($whereClause);	
	    }
	    
		public static function getMensajesUsuariosAbiertosAsignadosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosAbiertosAsignadosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBasePaginados($sortArray, $whereClause, $offset, $itemsPerPage);	
	    }	
	    
    	public static function getCampañasDeMensajesAbiertosAsignados($paramArray = array()) {
    		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosAbiertosAsignadosWhere($filtersSQL);
			return dmMensajes::getCampañasDeMensajesBase($whereClause);	
	    }
	    
	    
        ////////////////////////////////////////
		/// Mensajes Usuarios No Respondidos ///
    	////////////////////////////////////////
    			
	    public static function getMensajesUsuariosNoRespondidosWhere($filtersSQL) {
	    	return " WHERE ME.idMensajePadre IS NULL 
	    	         AND ME.respondido = 0 
					 AND ($filtersSQL) ";
	    }
	    
 		public static function getMensajesUsuariosNoRespondidosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosNoRespondidosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBaseCount($whereClause);	
	    }
	    
		public static function getMensajesUsuariosNoRespondidosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosNoRespondidosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBasePaginados($sortArray, $whereClause, $offset, $itemsPerPage);	
	    }	
	    
    	public static function getCampañasDeMensajesNoRespondidos($paramArray = array()) {
    		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosNoRespondidosWhere($filtersSQL);
			return dmMensajes::getCampañasDeMensajesBase($whereClause);	
	    }
	    
	    
        //////////////////////////////////////////////
		/// Mensajes Usuarios Respondidos Cerrados ///
    	//////////////////////////////////////////////
    			
	    public static function getMensajesUsuariosRespondidosCerradosWhere($filtersSQL) {
	    	return " WHERE ME.idMensajePadre IS NULL 
					 AND ME.cerrado = 1 
					 AND ME.respondido = 1 
					 AND ($filtersSQL) ";
	    }
	    
 		public static function getMensajesUsuariosRespondidosCerradosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosRespondidosCerradosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBaseCount($whereClause);	
	    }
	    
		public static function getMensajesUsuariosRespondidosCerradosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosRespondidosCerradosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBasePaginados($sortArray, $whereClause, $offset, $itemsPerPage);	
	    }	
	    
    	public static function getCampañasDeMensajesRespondidosCerrados($paramArray = array()) {
    		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosRespondidosCerradosWhere($filtersSQL);
			return dmMensajes::getCampañasDeMensajesBase($whereClause);	
	    }
	    
	    
        /////////////////////////////////////////////////
		/// Mensajes Usuarios No Respondidos Cerrados ///
    	/////////////////////////////////////////////////
    			
	    public static function getMensajesUsuariosNoRespondidosCerradosWhere($filtersSQL) {
	    	return " WHERE ME.idMensajePadre IS NULL 
					 AND ME.cerrado = 1 
					 AND ME.respondido = 0 
					 AND ($filtersSQL) ";
	    }
	    
 		public static function getMensajesUsuariosNoRespondidosCerradosCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosNoRespondidosCerradosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBaseCount($whereClause);	
	    }
	    
		public static function getMensajesUsuariosNoRespondidosCerradosPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosNoRespondidosCerradosWhere($filtersSQL);
			return dmMensajes::getMensajesUsuariosBasePaginados($sortArray, $whereClause, $offset, $itemsPerPage);	
	    }	
	    
    	public static function getCampañasDeMensajesNoRespondidosCerrados($paramArray = array()) {
    		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosNoRespondidosCerradosWhere($filtersSQL);
			return dmMensajes::getCampañasDeMensajesBase($whereClause);	
	    }
	    
	    
    	////////////////////////////////////
		/// Mensajes Usuarios Asigned To ///
    	////////////////////////////////////
    			
	    public static function getMensajesUsuariosAsignedToWhere($userId, $filtersSQL) {
	    	return " WHERE ME.idMensajePadre IS NULL 
					 AND ME.cerrado = 0 
					 AND ME.asignedTo is not null 
					 AND U.IdUsuario = $userId 
					 AND ($filtersSQL) ";
	    }
	    
 		public static function getMensajesUsuariosAsignedToCount($userId, $paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosAsignedToWhere($userId, $filtersSQL);
			return dmMensajes::getMensajesUsuariosBaseCount($whereClause);	
	    }
	    
		public static function getMensajesUsuariosAsignedToPaginados($userId, $paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosAsignedToWhere($userId, $filtersSQL);
			return dmMensajes::getMensajesUsuariosBasePaginados($sortArray, $whereClause, $offset, $itemsPerPage);	
	    }	
	    
    	public static function getCampañasDeMensajesAsignedTo($userId, $paramArray = array()) {
    		$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$whereClause = dmMensajes::getMensajesUsuariosAsignedToWhere($userId, $filtersSQL);
			return dmMensajes::getCampañasDeMensajesBase($whereClause);	
	    }
	    
	    
        /**
        *Realiza un SELECT que devuelve todos los registros de mensajes
        *@return array Assoc, si ocurrio un error devuelve array()
        */
        public static function getAllObj() {        
            $cn = Conexion::nuevo();
            try
            {
                $cn->Abrir();
                $cn->setQuery("SELECT * FROM mensajes");
                $Tabla = $cn->DevolverQuery();
                $omensajes[] = array();
                foreach($Tabla as $Fila) {
                    $omensajes[] = new Mensajes();
                    $omensajes[count($oMensajes)-1]->setidMensaje($Fila['idMensaje']);
				    $omensajes[count($oMensajes)-1]->setidUsuario($Fila['idUsuario']);
				    $omensajes[count($oMensajes)-1]->setidMensajePadre($Fila['idMensajePadre']);
				    $omensajes[count($oMensajes)-1]->setfecha($Fila['fecha']);
				    $omensajes[count($oMensajes)-1]->setemail($Fila['email']);
				    $omensajes[count($oMensajes)-1]->setmensaje($Fila['mensaje']);
				    $omensajes[count($oMensajes)-1]->setrespondido($Fila['respondido']);
				    $omensajes[count($oMensajes)-1]->setasunto($Fila['asunto']);
				    $omensajes[count($oMensajes)-1]->setidmotivo($Fila['idmotivo']);
				    $omensajes[count($oMensajes)-1]->setIdCampania($Fila['IdCampania']);
				    $omensajes[count($oMensajes)-1]->setcerrado($Fila['cerrado']);
				    $omensajes[count($oMensajes)-1]->setcustomercare($Fila['customercare']);
				    $omensajes[count($oMensajes)-1]->setboUserId($Fila['boUserId']);
                }
                $cn->Cerrar();
                return $omensajes;
            }
            catch(exception $e)
            {            
                throw $e;                  
            }        
        }
	    /**
	    *Realiza un SELECT que devuelve UN registros de mensajes por $idMensaje
	    *@param $idMensaje condicion a buscar
	    *@return objeto
	    */
	    public static function getById($idMensaje) {		
		    $cn = Conexion::nuevo();
		    $omensajes = new Mensajes();
		    try {
			    $cn->Abrir();
			    $cn->setQuery("SELECT * FROM mensajes WHERE idMensaje = ". mysql_real_escape_string($idMensaje) ." ");
			    $Tabla = $cn->DevolverQuery();
			    if ($Tabla) {
    				$omensajes->setidMensaje($Tabla[0]['idMensaje']);
				    $omensajes->setidUsuario($Tabla[0]['idUsuario']);
				    $omensajes->setidMensajePadre($Tabla[0]['idMensajePadre']);
				    $omensajes->setfecha($Tabla[0]['fecha']);
				    $omensajes->setemail($Tabla[0]['email']);
				    $omensajes->setmensaje($Tabla[0]['mensaje']);
				    $omensajes->setrespondido($Tabla[0]['respondido']);
				    $omensajes->setasunto($Tabla[0]['asunto']);
				    $omensajes->setidmotivo($Tabla[0]['idmotivo']);		
				    $omensajes->setIdCampania($Tabla[0]['IdCampania']);	
				    $omensajes->setcerrado($Tabla[0]['cerrado']);			
				    $omensajes->setcustomercare($Tabla[0]['customercare']);			
				    $omensajes->setboUserId($Tabla[0]['boUserId']);			
			    }
			    $cn->Cerrar();			
		    }
		    catch(exception $e) {			
			    throw $e;
		    }
		    return $omensajes;
	    }
	    
	    
    	public static function getNextMensajeId($idUltimoMensaje, $userId) {		
		    $cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
			    $cn->setQuery("SELECT ME.idMensaje   
			                   FROM mensajes ME
					 		   LEFT JOIN usuarios U ON U.IdUsuario = ME.asignedTo 
			                   WHERE ME.idMensaje > $idUltimoMensaje   
							   AND ME.cerrado = 0 
							   AND ME.asignedTo is not null 
							   AND U.IdUsuario = $userId 
			                   ORDER BY ME.idMensaje 
			                   LIMIT 1");
			    $Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();	
			    return $Tabla[0]["idMensaje"];
		    }
		    catch(exception $e) {			
			    throw $e;
		    }
	    }
	    
    	public static function getMotivoById($idMotivo) {		
		    $cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
			    $cn->setQuery("SELECT * FROM motivos WHERE IdMotivo = ".mysql_real_escape_string($idMotivo));
			    $Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();	
			    return $Tabla[0]['Descripcion'];		
		    }
		    catch(exception $e) {			
			    throw $e;
		    }
	    }
	    
    	public static function getMotivos() {		
		    $cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
			    $cn->setQuery("SELECT * FROM motivos");
			    $Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();	
			    return $Tabla;		
		    }
		    catch(exception $e) {			
			    throw $e;
		    }
	    }
	    
   		public static function getTemplates($idMotivo = 1) {		
		    $cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
			    $cn->setQuery("SELECT id_template, title, message, type  
			                   FROM message_templates
			                   WHERE type = $idMotivo");
			    $Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();	
			    return $Tabla;		
		    }
		    catch(exception $e) {			
			    throw $e;
		    }
	    }
	    
   		public static function getTemplate($idTemplate) {		
		    $cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
			    $cn->setQuery("SELECT id_template, title, message, type  
			                   FROM message_templates
			                   WHERE id_template = $idTemplate");
			    $Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();	
			    return $Tabla[0];
		    }
		    catch(exception $e) {			
			    throw $e;
		    }
	    }
	    
	    public static function getTemplatesMensajesCount($paramArray = array()) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			
		    $cn = Conexion::nuevo();
		    try
		    {
			    $cn->Abrir();
                $cn->setQuery("SELECT count(*) as total 
							   FROM message_templates
							   WHERE ($filtersSQL) ");
                $Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();
			    return $Tabla[0]['total'];
		    }
		    catch(exception $e)
		    {			
			    throw $e;                   
		    }		
	    }
	    
	    public static function getTemplatesMensajesPaginados($paramArray = array(), $sortArray = array(), $offset=0, $itemsPerPage=10) {
			$filtersSQL = ListadoUtils::generateFiltersSQL($paramArray);
			$sortSQL = ListadoUtils::generateSortSQL($sortArray);
			
		    $cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
                $cn->setQuery("SELECT id_template, 
                                      title, 
                                      CONCAT(LEFT(message, 25),'...') as 'messagel',
                                      message, 
                                      type,
                                      IF(type=1, 'Introducci&oacute;n', IF(type=2, 'Cuerpo', IF(type=3, 'Pie', 'Nada'))) as 'typed' 
			                   FROM message_templates
			                   WHERE ($filtersSQL) 
							   ORDER BY $sortSQL 
							   LIMIT ".mysql_real_escape_string($offset).", ".mysql_real_escape_string($itemsPerPage));
			    $Tabla = $cn->DevolverQuery();
			    $cn->Cerrar();
			    return $Tabla;
		    }
		    catch(exception $e)
		    {			
			    throw $e;                   
		    }		
	    }	
	    
	    public static function getAddTemplate($txtTitle="", $txtMensaje="", $txtTipo=1) {
			
		    $cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
                $cn->setQuery("INSERT INTO message_templates (title, message, type) 
                               VALUES ('".htmlspecialchars(mysql_real_escape_string($txtTitle), ENT_QUOTES)."', '".htmlspecialchars(mysql_real_escape_string($txtMensaje), ENT_QUOTES)."', ".mysql_real_escape_string($txtTipo).")");
			    $cn->EjecutarQuery();
			    $cn->Cerrar();
			    return $Tabla;
		    }
		    catch(exception $e)
		    {			
			    throw $e;                   
		    }		
	    }	
	    
	    public static function updateTemplate($idTemplate, $txtTitle="", $txtMensaje="", $txtTipo=1) {
			
		    $cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
                $cn->setQuery("UPDATE message_templates SET 
				                   title = '".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($txtTitle)), ENT_QUOTES)."', 
				                   message = '".htmlspecialchars(htmlspecialchars_decode(mysql_real_escape_string($txtMensaje)), ENT_QUOTES)."', 
				                   type = ".mysql_real_escape_string($txtTipo)." 
			                   WHERE id_template = ".mysql_real_escape_string($idTemplate));
			    $cn->EjecutarQuery();
			    $cn->Cerrar();
			    return $Tabla;
		    }
		    catch(exception $e) {			
			    throw $e;                   
		    }		
	    }	
	    
	    public static function deleteTemplate($idTemplate) {
			$cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
                $cn->setQuery("DELETE FROM message_templates WHERE id_template = ". mysql_real_escape_string($idTemplate));
			    $cn->EjecutarQuery();
			    $cn->Cerrar();
			    return $Tabla;
		    }
		    catch(exception $e) {			
			    throw $e;                   
		    }		
	    }	
	    
	    public static function asignUser($idMensaje, $idUsuario) {
	    	$cn = Conexion::nuevo();
		    try {
			    $cn->Abrir();
                $cn->setQuery("UPDATE mensajes SET 
				                      asignedTo = ".mysql_real_escape_string($idUsuario)." 
			                   WHERE idMensaje = ".mysql_real_escape_string($idMensaje));
			    $cn->EjecutarQuery();
			    $cn->Cerrar();
			    return $Tabla;
		    }
		    catch(exception $e) {			
			    throw $e;                   
		    }	
	    }
	    
     public static function getSaludosMensajes($userId) {
	    	$cn = Conexion::nuevo();
		    try {
				$cn->Abrir();
				$cn->setQuery('SELECT * FROM mensajes_saludo WHERE IdUsuario = '.mysql_real_escape_string($userId));
				$Tabla = $cn->DevolverQuery();
                $cn->Cerrar();
                return $Tabla[0]['MensajeSaludo'];
		    }
		    catch(exception $e) {			
			    throw $e;                   
		    }	
	    }
		
		 public static function getTotalMensajes($idUsuario) {
	    	$cn = Conexion::nuevo();
		    try {
				$cn->Abrir();
				$cn->setQuery("SELECT count(*) as totales FROM mensajes WHERE idUsuario = ".mysql_real_escape_string($idUsuario));
				$Tabla = $cn->DevolverQuery();
                $cn->Cerrar();
                return $Tabla[0]['totales'];
		    }
		    catch(exception $e) {			
			    throw $e;                   
		    }	
	    }
		
		public static function getMensajesAbiertos($idUsuario) {
	    	$cn = Conexion::nuevo();
		    try {
				$cn->Abrir();
				$cn->setQuery("SELECT count(cerrado) as abiertos, cerrado as cerrado FROM mensajes WHERE cerrado  = 0 AND idUsuario = ".mysql_real_escape_string($idUsuario));
				$Tabla = $cn->DevolverQuery();
                $cn->Cerrar();
                return $Tabla[0]['abiertos'];
		    }
		    catch(exception $e) {			
			    throw $e;                   
		    }	
	    }
    }
?>
