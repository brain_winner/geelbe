<?php 
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();} 

include $_SERVER['DOCUMENT_ROOT']."/corp/conf/conf_db.php";
include $_SERVER['DOCUMENT_ROOT']."/corp/lib/utils.php";

$perfil = Aplicacion::Decrypter($_SESSION["BO"]["User"]["perfil"]);
if($perfil == 2) {
	$isAdminUser = true;
} else {
	$isAdminUser = false;
}

$db = mysql_connect(SERVER, DB_USERNAME, DB_PASSWORD, true);
mysql_select_db(CORP_DB, $db);

$result = mysql_query('SELECT contenido_informe from informes_sistemas where id = '.mysql_real_escape_string($_GET["id_informe"]), $db);
$contenido_informe = mysql_fetch_assoc($result)
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Carta semanal del CEO</title>
		<link href="http://css.static.geelbe.com/<?=$_GET['country']?>/css/listado_bo.css" rel="stylesheet" type="text/css" />
	</head>

	<body>

		<div id="container" class="listadocontainer blogstyle">
			
			<h1>Informe &aacute;rea sistemas</h1>
			
			<?php 
			if($isAdminUser) {
				if(isset($_REQUEST["id_informe"])) { ?>
				<form style="text-align:center" action="/<?=$_GET["country"]?>/bo/corp/sistemas/modificar_informe.php" id="modificarInformeForm" method="post">
				  <input type="hidden" name="country" id="country" value="<?=$_GET["country"]?>" />
				  <input type="hidden" name="id_informe" id="id_informe" value="<?=$_GET["id_informe"]?>" />
				  <input type="text" size="80" maxlength="150" name="titulo" id="titulo" value="<?=stripcslashes(html_entity_decode($contenido_informe["titulo"]))?>" /><br /><br />
				  <textarea style="width:600px;" name="contenidoInforme" id="contenidoInforme"><?=stripcslashes(html_entity_decode($contenido_informe["contenido_informe"]))?></textarea>
				  <br />
				  <input style="margin-top:10px;" type="submit" name="modificacionInformeSubmitButton" id="modificacionInformeSubmitButton" value="Modificar"></input>
				</form>
				<?php } else { ?>
				<form style="text-align:center" action="/<?=$_GET["country"]?>/bo/corp/sistemas/alta_informe.php" id="altaInformeForm" method="post">
				  <input type="hidden" name="country" id="country" value="<?=$_GET["country"]?>" />
  				  <input type="text" size="80" maxlength="150" name="titulo" id="titulo" /><br /><br />
				  <textarea style="width:600px;" name="contenidoInforme" id="contenidoInforme"></textarea>
				  <br />
				  <input style="margin-top:10px;" type="submit" name="altaInformeSubmitButton" id="altaInformeSubmitButton" value="Agregar"></input>
				</form>
				<?php } ?>
			
			<?php
			}
			$result = mysql_query('SELECT * from informes_sistemas order by fecha desc limit 50', $db);
			
			while ($carta = mysql_fetch_assoc($result)) {
				$fecha = FechaFormateada(strtotime($carta["fecha"]));
				echo "<div style=\"margin:20px auto; width:85%\" class=\"informes_sistemas\">";
				echo "<h3 style=\"margin-bottom:5px\">".stripcslashes($carta["titulo"])."</h3>";
				echo "<h4 style=\"margin-bottom:5px;font-weight:normal;font-style:italic\">".$fecha."</h4>";
				echo "<p style=\"text-align:justify\">".stripcslashes(html_entity_decode($carta["contenido_informe"]))."</p>";
				if($isAdminUser) {
					echo "<a href=\"/".$_GET["country"]."/bo/corp/sistemas/eliminar_informe.php?id_informe=".$carta["id"]."\">Eliminar entrada</a>&nbsp;-&nbsp;<a href=\"/".$_GET["country"]."/bo/corp/sistemas.php?id_informe=".$carta["id"]."\">Modificar entrada</a>";
				}
				echo "<hr style=\"color:#CCC;margin:10px auto\" />";
				echo "</div>";
			}
			?>
		</div>
	</body>
</html>