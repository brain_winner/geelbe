<?php 
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();} 
include $_SERVER['DOCUMENT_ROOT']."/corp/conf/conf.php";
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Comercial</title>
		<link href="http://<?=WEB_HOST?>/corp/css/listado_bo.css" rel="stylesheet" type="text/css" />
		<script src="http://<?=JS_HOST?>/corp/js/jquery-1.4.4.min.js" type="text/javascript"></script>
		<script src="http://<?=JS_HOST?>/corp/js/jquery.galleria.js" type="text/javascript"></script>
		
			<script type="text/javascript"> 
	
	jQuery(function($) {
		
		$('.gallery_demo_unstyled').addClass('gallery_demo'); // adds new class name to maintain degradability
		
		$('ul.gallery_demo').galleria({
			history   : true, // activates the history object for bookmarking, back-button etc.
			clickNext : true, // helper for making the image clickable
			insert    : '#main_image', // the containing selector for our main image
			onImage   : function(image,caption,thumb) { // let's add some image effects for demonstration purposes
				
				// fade in the image & caption
				if(! ($.browser.mozilla && navigator.appVersion.indexOf("Win")!=-1) ) { // FF/Win fades large images terribly slow
					image.css('display','none').fadeIn(1000);
				}
				//caption.css('display','none').fadeIn(1000);
				
				// fetch the thumbnail container
				var _li = thumb.parents('li');
				
				// fade out inactive thumbnail
				_li.siblings().children('img.selected').fadeTo(500,0.3);
				
				// fade in active thumbnail
				thumb.fadeTo('fast',1).addClass('selected');
				
				// add a title for the clickable image
				image.attr('title','Siguiente >>');
			},
			onThumb : function(thumb) { // thumbnail effects goes here
				
				// fetch the thumbnail container
				var _li = thumb.parents('li');
				
				// if thumbnail is active, fade all the way.
				var _fadeTo = _li.is('.active') ? '1' : '0.3';
				
				// fade in the thumbnail when finnished loading
				thumb.css({display:'none',opacity:_fadeTo}).fadeIn(1500);
				
				// hover effects
				thumb.hover(
					function() { thumb.fadeTo('fast',1); },
					function() { _li.not('.active').children('img').fadeTo('fast',0.3); } // don't fade out if the parent is active
				)
			}
		});
	});
	
	</script> 
	<style media="screen,projection" type="text/css"> 
		body{background:#EFEFEF;}
		.galleria{list-style:none;width:200px}
		.galleria li{display:block;width:80px;height:80px;overflow:hidden;float:left;margin:0 10px 10px 0}
		.galleria li a{display:none}
		.galleria li div{position:absolute;display:none;top:0;left:180px}
		.galleria li div img{cursor:pointer}
		.galleria li.active div img,.galleria li.active div{display:block}
		.galleria li img.thumb{cursor:pointer;top:auto;left:auto;display:block;width:auto;height:auto}
		.galleria li .caption{display:none}
		* html .galleria li div span{width:400px} /* MSIE bug */
		
		a{color:#348;text-decoration:none;outline:none;}
		a:hover{color:#67a;}
		.caption{display:none}
		.demo{position:relative;margin-top:2em;}
		.gallery_demo{width:702px;margin:0 auto;}
		.gallery_demo li{width:68px;height:50px;border:3px double #111;margin: 0 2px;background:#000;}
		.gallery_demo li div{left:240px}
		.gallery_demo li div .caption{font:italic 0.7em/1.4 georgia,serif;}
		
		#main_image{margin:0 auto 60px auto;height:438px;width:700px;background:black;}
		#main_image img{margin-bottom:10px;}
		
		.nav{padding-top:15px;clear:both;font:80% 'helvetica neue',sans-serif;letter-spacing:3px;text-align:center;margin-top:20px;}
		
		.info{text-align:left;width:700px;margin:30px auto;border-top:1px dotted #221;padding-top:30px;}
		.info p{margin-top:1.6em;}
		
		.gallery_demo_unstyled {
			display:none;
		}
    </style> 
	</head>
	<body>
	<div class="demo"> 
		<div id="main_image"></div> 
		<ul class="gallery_demo_unstyled"> 
		    <li class="active"><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva01.jpg" alt="Diapositiva 1" title="Diapositiva 1"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva02.jpg" alt="Diapositiva 2" title="Diapositiva 2"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva03.jpg" alt="Diapositiva 3" title="Diapositiva 3"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva04.jpg" alt="Diapositiva 4" title="Diapositiva 4"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva05.jpg" alt="Diapositiva 5" title="Diapositiva 5"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva06.jpg" alt="Diapositiva 6" title="Diapositiva 6"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva07.jpg" alt="Diapositiva 7" title="Diapositiva 7"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva08.jpg" alt="Diapositiva 8" title="Diapositiva 8"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva09.jpg" alt="Diapositiva 9" title="Diapositiva 9"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva10.jpg" alt="Diapositiva 10" title="Diapositiva 10"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva11.jpg" alt="Diapositiva 11" title="Diapositiva 11"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva12.jpg" alt="Diapositiva 12" title="Diapositiva 12"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva13.jpg" alt="Diapositiva 13" title="Diapositiva 13"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva14.jpg" alt="Diapositiva 14" title="Diapositiva 14"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva15.jpg" alt="Diapositiva 15" title="Diapositiva 15"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva16.jpg" alt="Diapositiva 16" title="Diapositiva 16"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva17.jpg" alt="Diapositiva 17" title="Diapositiva 17"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva18.jpg" alt="Diapositiva 18" title="Diapositiva 18"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva19.jpg" alt="Diapositiva 19" title="Diapositiva 19"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva20.jpg" alt="Diapositiva 20" title="Diapositiva 20"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva21.jpg" alt="Diapositiva 21" title="Diapositiva 21"></li> 
		    <li><img src="http://<?=IMG_HOST?>/corp/comercial/img/Diapositiva22.jpg" alt="Diapositiva 22" title="Diapositiva 22"></li> 
		</ul> 
		<br />
		<p class="nav"><a href="#" onclick="$.galleria.prev(); return false;">&laquo; anterior</a> | <a href="#" onclick="$.galleria.next(); return false;">siguiente &raquo;</a></p> 
	</div> 
	</body>
</html>